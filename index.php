<?php
        require_once("src/libs/clases/Utils.php");
        require_once("src/libs/clases/Session.php");
        Session::init();
        // Máxima duración de sesión activa en 30 minutos
        define( 'MAX_SESSION_TIEMPO', 1800 * 1 );

        // Controla cuando se ha creado y cuando tiempo ha recorrido
        if ( isset( $_SESSION[ 'ULTIMA_ACTIVIDAD' ] ) &&
        ( time() - $_SESSION[ 'ULTIMA_ACTIVIDAD' ] > MAX_SESSION_TIEMPO ) ) {

        // Si ha pasado el tiempo sobre el limite destruye la session
        destruir_session();
        }

        $_SESSION[ 'ULTIMA_ACTIVIDAD' ] = time();

        // Función para destruir y resetear los parámetros de sesión
        function destruir_session() {

        $_SESSION = array();
        if ( ini_get( 'session.use_cookies' ) ) {
                $params = session_get_cookie_params();
                setcookie(
                session_name(),
                '',
                time() - MAX_SESSION_TIEMPO,
                $params[ 'path' ],
                $params[ 'domain' ],
                $params[ 'secure' ],
                $params[ 'httponly' ] );
        }
        Session::destroy();
        }

        const MAESTRIA = 3;
        const MATERIA = 4;
        const VERSION = 4;
        const FACULTAD = 2;
        const MALLA = 3;
        const MAESCON = 6;
        const CICLO = 5;
        const GRUPO = 3;
        const COHORTE = 5;
        const ESTUDIANTE = 5;
        const PLANANA = 6;
        const MATRICULA = 6;
        const USUARIO = 4;
        const PERFIL = 3;
        const ASISPER= 7;
        const HOSPITAL =3;
        //const LINKSVR = "[10.87.117.112].DB_RRHH.dbo.";
        const LINKSVR = "DB_RRHH.dbo.";

        require_once('src/libs/clases/Controllers.php');        
        $Controllers = new Controllers((isset($_GET['url']) ? strtoupper($_GET['url']) : "CMD_LOGIN"));
        $View = $Controllers->View();
        if ($View[0])
        {   require_once('src/libs/clases/DateControl.php');
            require_once ('src/libs/plugins/xajax/xajax_core/xajax.inc.php');    
            $xajax = new xajax();
            $xajax->configure('javascript URI','src/libs/plugins/xajax/');
            require_once($View[1]);
        }
        else
        require_once($View[1]);
?>


       
