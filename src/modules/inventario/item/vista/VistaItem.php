<?php
         $Sufijo = '_Item';
         require_once('src/modules/inventario/item/controlador/ControlItem.php');
         $ControlItem = new ControlItem($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlItem,'GuardaItem'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlItem,'CargaModalGrid'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlItem,'ConsultaModalGridByTX'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlItem,'MuestraItemByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlItem,'MuestraItemByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlItem,'EliminaItem'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript"> 
                 var btnItem = "btctamercaderia:btctaventas:btctacostos:btctadevolucion";
                 function ButtonClick_Item(Sufijo,Operacion)
                 {        var objItem = "id:descripcion:estado:categoria:codigo:idctamercaderia:txctamercaderia:idctaventas:txctaventas:idctacostos:txctacostos:idctadevolucion:txctadevolucion";
                          var frmItem = "descripcion:estado:categoria:codigo:idctamercaderia:txctamercaderia:idctaventas:txctaventas:idctacostos:txctacostos:idctadevolucion:txctadevolucion";
                          var btnItem = "btctamercaderia:btctaventas:btctacostos:btctadevolucion";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              BarButtonStateEnabled(Sufijo, btnItem);
                              ElementStatus(Sufijo,objItem,"id:estado");
                              ElementClear(Sufijo,objItem);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               BarButtonStateEnabled(Sufijo, btnItem);
                               ElementStatus(Sufijo,objItem,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_Item(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"descripcion:categoria:idctamercaderia:txctamercaderia"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objItem);
                                       xajax_Guarda_Item(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               BarButtonStateDisabled(Sufijo, btnItem);
                               ElementStatus(Sufijo,"id",frmItem);
                               ElementClear(Sufijo,objItem);
                          }
                          else
                          {    xajax_CargaModal_Item(Operacion);
                          }
                          return false;
                 }

                function SearchByElement_Item(Sufijo,Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    xajax_MuestraByID_Item(Elemento.value);
                                }
                            }
                            else
                            BarButtonState(Sufijo,"Default");
                        }
                        else if (Elemento.id === "FindCtamercaderiaTextBx" + Sufijo)
                        {   xajax_BuscaModalByTX_Item("btctamercaderia", Elemento.value);
                        }
                        else if (Elemento.id === "FindCtaventasTextBx" + Sufijo)
                        {   xajax_BuscaModalByTX_Item("btctaventas", Elemento.value);
                        }
                        else if (Elemento.id === "FindCtacostosTextBx" + Sufijo)
                        {   xajax_BuscaModalByTX_Item("btctacostos", Elemento.value);
                        }
                        else if (Elemento.id === "FindCtadevolucionTextBx" + Sufijo)
                        {   xajax_BuscaModalByTX_Item("btctadevolucion", Elemento.value);
                        }
                        else
                        xajax_MuestraByTX_Item(Elemento.value);    
                            
                        return false;
                }
                 
                function SearchGetData_Item(Sufijo,Grilla)
                {       if (IsDisabled(Sufijo,"addSav")) {
                                BarButtonState(Sufijo,"Active");
                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                                document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                                document.getElementById("descripcion"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                                document.getElementById("categoria"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                                document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue; 

                                var Mercaderia = Grilla.cells[9].childNodes[0].nodeValue;
                                var Ventas = Grilla.cells[11].childNodes[0].nodeValue;
                                var Costos = Grilla.cells[13].childNodes[0].nodeValue;
                                var Devolucion = Grilla.cells[15].childNodes[0].nodeValue;

                                document.getElementById("idctamercaderia"+Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue; 
                                document.getElementById("txctamercaderia"+Sufijo).value = (Mercaderia == "_" ? "" : Mercaderia); 
                                document.getElementById("idctaventas"+Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue; 
                                document.getElementById("txctaventas"+Sufijo).value = (Ventas == "_" ? "" : Ventas); 
                                document.getElementById("idctacostos"+Sufijo).value = Grilla.cells[12].childNodes[0].nodeValue; 
                                document.getElementById("txctacostos"+Sufijo).value = (Costos == "_" ? "" : Costos); 
                                document.getElementById("idctadevolucion"+Sufijo).value = Grilla.cells[14].childNodes[0].nodeValue; 
                                document.getElementById("txctadevolucion"+Sufijo).value = (Devolucion == "_" ? "" : Devolucion); 
                        }
                        return false;
                }
                 
                function XAJAXResponse_Item(Sufijo,Mensaje,Datos)
                {       var objItem = "descripcion:estado:categoria:codigo:idctamercaderia:txctamercaderia:idctaventas:txctaventas:idctacostos:txctacostos:idctadevolucion:txctadevolucion";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,btnItem);
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objItem);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                }

                function SearchCtamercaderiaGetData_Item(Sufijo, DatosGrid)
                {       document.getElementById("idctamercaderia" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txctamercaderia" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }

                function SearchCtaventasGetData_Item(Sufijo, DatosGrid)
                {       document.getElementById("idctaventas" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txctaventas" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }

                function SearchCtacostosGetData_Item(Sufijo, DatosGrid)
                {       document.getElementById("idctacostos" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txctacostos" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }

                function SearchCtadevolucionGetData_Item(Sufijo, DatosGrid)
                {       document.getElementById("idctadevolucion" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txctadevolucion" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }

         </script>
         </head>
<body>
         <div class="FormBasic" style="width:600px">
               <div class="FormSectionMenu">              
              <?php  $ControlItem->CargaItemBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlItem->CargaItemMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlItem->CargaItemSearchGrid();  ?>
              </div>  
         </div> 
        </body>
</html> 
              

