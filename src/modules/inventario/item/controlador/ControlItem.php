<?php
        require_once("src/rules/inventario/servicio/ServicioItem.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
        require_once("src/modules/inventario/item/render/RenderItem.php");
        require_once("src/rules/general/servicio/ServicioPlanCuentas.php");    
        require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");

         class ControlItem
         {     private $Sufijo; 
               private $ServicioItem;
               private $RenderItem;
               private $ServicioPlanCuentas;
               private $RenderPlanCuentas;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioItem = new ServicioItem();
                        $this->RenderItem = new RenderItem($Sufijo);
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
                        
               }

               function CargaItemBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaItemMantenimiento()
               {        echo $this->RenderItem->CreaItemMantenimiento();
               }

               function CargaItemSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoItem = $this->ServicioItem->BuscarItem($prepareDQL);
                        echo $this->RenderItem->CreaItemSearchGrid($datoItem);
               }         
               
               function MuestraItemByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('id' => intval($id));
                        $datoItem = $this->ServicioItem->BuscarItem($prepareDQL);
                        return $this->RenderItem->MuestraItem($ajaxRespon,$datoItem);
               }

               function MuestraItemByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoItem = $this->ServicioItem->BuscarItem($prepareDQL);
                        return $this->RenderItem->MuestraItemGrid($ajaxRespon,$datoItem);
               }

               function GuardaItem($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Item  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioItem->GuardaDBItem($Item);
                        if (is_numeric($id)){
                            $function = (empty($Item->id) ? "MuestraItemGuardado" : "MuestraItemEditado");
                            $prepareDQL = array('id' => intval($id),'limite' => 50,'inicio' => 0);
                            $Datos = $this->ServicioItem->BuscarItem($prepareDQL);
                            return $this->RenderItem->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderItem->MuestraItemExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaItem($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioItem->DesactivaItem(intval($id));
                        $Datos = $this->ServicioItem->BuscarItem($prepareDQL);
                        return $this->RenderItem->MuestraItemEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');

                        if ($Operacion==="btctamercaderia" || $Operacion==="btctaventas" || $Operacion==="btctacostos" || $Operacion==="btctadevolucion")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '');
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Buscar Cuenta";
                            $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                            $jsonModal['Ancho'] = "750";

                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
            
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        if ($Operacion==="btctamercaderia" || $Operacion==="btctaventas" || $Operacion==="btctacostos" || $Operacion==="btctadevolucion")                            
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            return $this->RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);                                        
                        }
                }
                
         }

?>

