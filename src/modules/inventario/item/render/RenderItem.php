<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderItem
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              private $Search;
                      
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Search = new SearchInput($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaItemMantenimiento()
              {        
                       $Item = '<table border="0" class="Form-Frame" cellpadding="0">';
                       $Item.= '       <tr height="30">';
                       $Item.= '           <td class="Form-Label" style="width:20%">Id</td>';
                       $Item.= '           <td class="Form-Label" style="width:45%">';
                       $Item.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'codigo:descripcion:idctamercaderia:txctamercaderia:idctaventas:txctaventas:idctacostos:txctacostos:idctadevolucion:txctadevolucion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $Item.= '           </td>';
                       $Item.= '           <td class="Form-Label" style="width:15%">Estado</td>';
                       $Item.= '           <td class="Form-Label" style="width:20%">';
                       $Item.=                 $this->CreaComboEstado();
                       $Item.= '           </td>';
                       $Item.= '       </tr>';
                       
                       $Item.= '       <tr height="30">';
                       $Item.= '           <td class="Form-Label">C&oacute;digo</td>';
                       $Item.= '           <td class="Form-Label">';
                       $Item.= '               <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['codigo'].'" onKeyDown="return soloNumerosLetras(event); "; disabled/>';
                       $Item.= '           </td>';
                       $Item.= '           <td class="Form-Label">Categor&iacute;a</td>';
                       $Item.= '           <td class="Form-Label">';
                       $Item.= '               <select id="categoria'.$this->Sufijo.'" name="categoria'.$this->Sufijo.'" class="sel-input s04" disabled="">';
                       $Item.= '               <option value="B">BIEN</option>';
                       $Item.= '               <option value="S">SERVICIO</option>';
                       $Item.= '               </select>';
                       $Item.= '           </td>';
                       $Item.= '       </tr>';

                       $Item.= '       <tr height="30">';
                       $Item.= '           <td class="Form-Label">Descripci&oacute;n</td>';
                       $Item.= '           <td class="Form-Label">';
                       $Item.= '                 <input type="text" class="txt-upper t09" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Item.= '           </td>';
                       $Item.= '           <td class="Form-Label"></td>';
                       $Item.= '           <td class="Form-Label">';
                       $Item.= '           </td>';
                       $Item.= '       </tr>';
                       $Item.= '       <tr height="30">';
                       $Item.= '            <td class="Form-Label">Cta. Mercadería</td>';
                       $Item.= '            <td class="Form-Label">';
                       $Item.=                  $this->Search->TextSch("ctamercaderia","","")->Enabled(false)->Create("t08");
                       $Item.= '            </td>';
                       $Item.= '           <td class="Form-Label"></td>';
                       $Item.= '           <td class="Form-Label">';
                       $Item.= '           </td>';
                       $Item.= '       </tr>';

                       $Item.= '       <tr height="30">';
                       $Item.= '            <td class="Form-Label">Cta. Ventas</td>';
                       $Item.= '            <td class="Form-Label">';
                       $Item.=                  $this->Search->TextSch("ctaventas","","")->Enabled(false)->Create("t08");
                       $Item.= '            </td>';
                       $Item.= '           <td class="Form-Label"></td>';
                       $Item.= '           <td class="Form-Label">';
                       $Item.= '           </td>';
                       $Item.= '       </tr>';

                       $Item.= '       <tr height="30">';
                       $Item.= '            <td class="Form-Label">Cta. Costos</td>';
                       $Item.= '            <td class="Form-Label">';
                       $Item.=                  $this->Search->TextSch("ctacostos","","")->Enabled(false)->Create("t08");
                       $Item.= '            </td>';
                       $Item.= '           <td class="Form-Label"></td>';
                       $Item.= '           <td class="Form-Label">';
                       $Item.= '           </td>';
                       $Item.= '       </tr>';

                       $Item.= '       <tr height="30">';
                       $Item.= '            <td class="Form-Label">Cta. Devolución</td>';
                       $Item.= '            <td class="Form-Label">';
                       $Item.=                  $this->Search->TextSch("ctadevolucion","","")->Enabled(false)->Create("t08");
                       $Item.= '            </td>';
                       $Item.= '           <td class="Form-Label"></td>';
                       $Item.= '           <td class="Form-Label">';
                       $Item.= '           </td>';
                       $Item.= '       </tr>';
                       $Item.= '</table>';
                       return $Item;
              }
              
              private function SearchGridValues()
              {        $Columns['Id']       = array('30px','center','');                       
                       $Columns['C&oacute;digo'] = array('75px','left','');
		       $Columns['Item'] = array('100px','left','');
                       $Columns['Categoria']   = array('80px','left','');
                       $Columns['Creacion']   = array('120px','center','');
                       $Columns['Estado']   = array('70px','left',''); 
                       $Columns['idEstado']   = array('0px','left','none');
                       $Columns['idItem']   = array('0px','left','none'); 

                       $Columns['IdMercaderia'] = array('0px','left','none'); 
                       $Columns['TxMercaderia'] = array('0px','left','none'); 
                       $Columns['IdVentas'] = array('0px','left','none'); 
                       $Columns['TxVentas'] = array('0px','left','none'); 
                       $Columns['IdCosto']  = array('0px','left','none'); 
                       $Columns['TxCosto']  = array('0px','left','none'); 
                       $Columns['IdDevol']  = array('0px','left','none'); 
                       $Columns['TxDevol']  = array('0px','left','none'); 
                       return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
              }

              function CreaItemSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->ItemGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function ItemGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $item)
                      {        $fecreacion = $this->Fechas->changeFormatDate($item['fecreacion'],"DMY");
                               $id = str_pad($item['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['codigo']));
			       $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['descripcion']));
                               $idCategoria=trim($item['categoria']);
                               if($idCategoria=='B'){
                                   $categoria="BIEN";
                               }else{
                                   $categoria="SERVICIO";
                               } 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($categoria));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['idestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$idCategoria);

                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['ctamercaderia']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['mercaderia']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['ctaventas']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['ventas']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['ctacostos']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['costos']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['ctadevolucion']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['devolucion']));
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($item['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {        $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraItem($Ajax,$Datos)
              {     foreach ($Datos as $Dato)   
                    {       $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("categoria".$this->Sufijo,"value", trim($Dato['categoria']));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Dato['codigo']));  
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));					   
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);

                            
                            $mercaderia = trim($Dato['mercaderia']);
                            $ventas = trim($Dato['ventas']);
                            $costos = trim($Dato['costos']);
                            $devolucion = trim($Dato['devolucion']);

                            $Ajax->Assign("idctamercaderia".$this->Sufijo,"value", $Dato['ctamercaderia']);					   
                            $Ajax->Assign("txctamercaderia".$this->Sufijo,"value", ($mercaderia=="_" ? "": $mercaderia));					   
                            $Ajax->Assign("idctaventas".$this->Sufijo,"value", $Dato['ctaventas']);					   
                            $Ajax->Assign("txctaventas".$this->Sufijo,"value", ($ventas=="_" ? "": $ventas));					   
                            $Ajax->Assign("idctacostos".$this->Sufijo,"value", $Dato['ctacostos']);					   
                            $Ajax->Assign("txctacostos".$this->Sufijo,"value", ($costos=="_" ? "": $costos));					   
                            $Ajax->Assign("idctadevolucion".$this->Sufijo,"value", $Dato['ctadevolucion']);					   
                            $Ajax->Assign("txctadevolucion".$this->Sufijo,"value", ($devolucion=="_" ? "": $devolucion));					   

                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                    }   
                    return $this->RespuestaItem($Ajax,"NOEXISTEID","No existe un Item con el ID ingresado.");
              }
              
              function MuestraItemGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->ItemGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }  
               
              function MuestraItemGuardado($Ajax,$Item)
              {       if (count($Item)>0)
                        {   $id = str_pad($Item[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraItemGrid($Ajax,$Item);
                            return $this->RespuestaItem($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaItem($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraItemEditado($Ajax,$Item)
              {       foreach ($Item as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraItemRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaItem($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaItem($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraItemEliminado($Ajax,$Item)
              {        foreach ($Item as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraItemRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaItem($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaTipoPrecio($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraItemRowGrid($Ajax,$Item,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Item['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($Item['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Item['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Item['codigo']));
			$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Item['descripcion']));
                        if(trim($Item['categoria'])=='B'){  $categoria="BIEN"; }else{ $categoria="SERVICIO"; }
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($categoria));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Item['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($Item['idestado']));
			$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($Item['categoria']));
                        
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",$Item['ctamercaderia']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",trim($Item['mercaderia']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",$Item['ctaventas']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],11),"innerHTML",trim($Item['ventas']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],12),"innerHTML",$Item['ctacostos']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],13),"innerHTML",trim($Item['costos']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],14),"innerHTML",$Item['ctadevolucion']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],15),"innerHTML",trim($Item['devolucion']));
                        
                        return $Ajax;
                }

              function MuestraItemExcepcion($Ajax,$Msg)
                {       return $this->RespuestaItem($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaItem($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
                /***** Modal ******/
                
                private function SearchGridModalConfig()
                {       $Columns['Id']   = array('40px','center','');                       
                        $Columns['Item'] = array('250px','left','');
                        $Columns['Categoria'] = array('80px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '120px','AltoRow' => '20px');
                }

                function CreaModalGridItem($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $ObjHTML = $this->ModalGridDataHTMLItem($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }
                
                function MuestraModalGridItem($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $ObjHTML = $this->ModalGridDataHTMLItem($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }        

                private function ModalGridDataHTMLItem($Grid,$Datos)
                {       foreach ($Datos as $Item)
                        {       $id = str_pad($Item['id'],2,'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Item['descripcion']));
                                if (trim($Item['categoria'])=="B"){
                                    $Grid->CreaSearchCellsDetalle($id,'',"BIEN");
                                }else{
                                    $Grid->CreaSearchCellsDetalle($id,'',"SERVICIO");
                                }
                                $Grid->CreaSearchRowsDetalle ($id,($Item['idestado']==0 ? "red" : ""));
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
        }
?>

