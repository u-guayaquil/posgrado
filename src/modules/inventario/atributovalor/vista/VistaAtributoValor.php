<?php
         $Sufijo = '_AtributoValor';

         require_once('src/modules/inventario/atributovalor/controlador/ControlAtributoValor.php');
         $ControlAtributoValor = new ControlAtributoValor($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlAtributoValor,'GuardaAtributoValor'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlAtributoValor,'MuestraAtributoValorByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlAtributoValor,'MuestraAtributoValorByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlAtributoValor,'EliminaAtributoValor'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript"> 
                 function ButtonClick_AtributoValor(Sufijo,Operacion)
                 {        var objAtributoValor = "id:descripcion:estado:atributo:codigo";
                          var frmAtributoValor = "descripcion:estado:atributo:codigo";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,objAtributoValor,"id:estado");
                              ElementClear(Sufijo,objAtributoValor);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objAtributoValor,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_AtributoValor(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"descripcion:atributo"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objAtributoValor);
                                       xajax_Guarda_AtributoValor(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmAtributoValor);
                               ElementClear(Sufijo,objAtributoValor);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_AtributoValor(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_AtributoValor(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_AtributoValor(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_AtributoValor(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")) {
                            BarButtonState(Sufijo,"Active");

                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                            document.getElementById("descripcion"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                            document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                                                    document.getElementById("atributo"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;

                            document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue; 
                          }
                          return false;
                 }
                 
                 function XAJAXResponse_AtributoValor(Sufijo,Mensaje,Datos)
                 {       
                        var objAtributoValor = "descripcion:estado:atributo:codigo";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objAtributoValor);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
<body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlAtributoValor->CargaAtributoValorBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlAtributoValor->CargaAtributoValorMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlAtributoValor->CargaAtributoValorSearchGrid();  ?>
              </div>  
         </div> 
        </body>
</html> 
              

