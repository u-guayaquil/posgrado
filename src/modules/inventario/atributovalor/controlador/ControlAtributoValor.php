<?php
         require_once("src/rules/inventario/servicio/ServicioAtributoValor.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/inventario/atributovalor/render/RenderAtributoValor.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlAtributoValor
         {     private  $Sufijo; 
               private  $ServicioAtributoValor;
               private  $RenderAtributoValor;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioAtributoValor = new ServicioAtributoValor();
                        $this->RenderAtributoValor = new RenderAtributoValor($Sufijo);
               }

               function CargaAtributoValorBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaAtributoValorMantenimiento()
               {        echo $this->RenderAtributoValor->CreaAtributoValorMantenimiento();
               }

               function CargaAtributoValorSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoAtributoValor = $this->ServicioAtributoValor->BuscarAtributoValor($prepareDQL);
                        echo $this->RenderAtributoValor->CreaAtributoValorSearchGrid($datoAtributoValor);
               }         
               
               function MuestraAtributoValorByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoAtributoValor = $this->ServicioAtributoValor->BuscarAtributoValor($prepareDQL);
                        return $this->RenderAtributoValor->MuestraAtributoValor($ajaxRespon,$datoAtributoValor);
               }

               function MuestraAtributoValorByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoAtributoValor = $this->ServicioAtributoValor->BuscarAtributoValor($prepareDQL);
                        return $this->RenderAtributoValor->MuestraAtributoValorGrid($ajaxRespon,$datoAtributoValor);
               }

               function GuardaAtributoValor($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $AtributoValor  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioAtributoValor->GuardaDBAtributoValor($AtributoValor);
                        if (is_numeric($id)){
                            $function = (empty($AtributoValor->id) ? "MuestraAtributoValorGuardado" : "MuestraAtributoValorEditado");
                            $prepareDQL = array('id' => intval($id),'limite' => 50,'inicio' => 0);
                            $Datos = $this->ServicioAtributoValor->BuscarAtributoValor($prepareDQL);
                            return $this->RenderAtributoValor->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderAtributoValor->MuestraAtributoValorExcepcion($ajaxRespon,intval($id));
                        }  
               }
               
               function EliminaAtributoValor($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioAtributoValor->DesactivaAtributoValor($id);
                        $Datos = $this->ServicioAtributoValor->BuscarAtributoValor($prepareDQL);
                        return $this->RenderAtributoValor->MuestraAtributoValorEliminado($ajaxRespon,$Datos);
                }
         }

?>

