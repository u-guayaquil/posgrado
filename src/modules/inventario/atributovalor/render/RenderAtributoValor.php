<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/inventario/servicio/ServicioAtributo.php");
        require_once("src/rules/general/entidad/Estado.php"); 
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderAtributoValor
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaAtributoValorMantenimiento()
              {        
                       $AtributoValor = '<table class="Form-Frame" cellpadding="0">';
                       $AtributoValor.= '       <tr height="30">';
                       $AtributoValor.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $AtributoValor.= '             <td class="Form-Label" style="width:75%">';
                       $AtributoValor.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $AtributoValor.= '             </td>';
                       $AtributoValor.= '         </tr>';
					   $AtributoValor.= '         <tr height="28">';
                       $AtributoValor.= '              <td class="Form-Label">C&oacute;digo</td>';
                       $AtributoValor.= '              <td class="Form-Label">';
                       $AtributoValor.= '               <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['codigo'].'" onKeyDown="return soloNumerosLetras(event); "; disabled/>';
                       $AtributoValor.= '              </td>';
                       $AtributoValor.= '         </tr>';  
                       $AtributoValor.= '         <tr height="30">';
                       $AtributoValor.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $AtributoValor.= '             <td class="Form-Label" style="width:75%">';
                       $AtributoValor.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $AtributoValor.= '             </td>';
                       $AtributoValor.= '         </tr>';
                       $AtributoValor.= '         </tr>';
                       $AtributoValor.= '         <tr height="30">';
                       $AtributoValor.= '             <td class="Form-Label">Atributo</td>';
                       $AtributoValor.= '             <td class="Form-Label">';
                                                   $AtributoValor.= $this->CreaComboAtributos();
                       $AtributoValor.= '             </td>';
                       $AtributoValor.= '         </tr>';                       
                       $AtributoValor.= '         <tr height="30">';
                       $AtributoValor.= '             <td class="Form-Label">Estado</td>';
                       $AtributoValor.= '             <td class="Form-Label">';
                                                   $AtributoValor.= $this->CreaComboEstado();
                       $AtributoValor.= '             </td>';
                       $AtributoValor.= '         </tr>';
                       $AtributoValor.= '</table>';
                       return $AtributoValor;
              }
              
              private function SearchGridValues()
              {        $Columns['Id']       = array('30px','center','');                       
                       $Columns['C&oacute;digo'] = array('75px','left','');
					   $Columns['Descripci&oacute;n'] = array('130px','left','');
                       $Columns['Atributo']   = array('80px','left','');
                       $Columns['Creacion']   = array('85px','center','');
                       $Columns['Estado']   = array('70px','left',''); 
                       $Columns['idEstado']   = array('0px','left','none');
                       $Columns['idAtributo']   = array('0px','left','none'); 
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaAtributoValorSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->AtributoValorGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function AtributoValorGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $item)
                      {        $fecreacion = $this->Fechas->changeFormatDate($item['fecreacion'],"DMY")[1];
                               $id = str_pad($item['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['codigo']));
							   $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['descripcion']));                                
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($item['atributo']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['idestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$item['idatributo']);
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($item['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function TraeDatosAtributos($IdArray)
              {        
                       $ServicioAtributo = new ServicioAtributo();
                       $datoAtributos = $ServicioAtributo->BuscarAtributo($IdArray);
                       return $datoAtributos;
              }
              
              private function CreaComboAtributos()
              {       
                       $datosAtributos= $this->TraeDatosAtributos(array(1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("atributo",$datosAtributos)->Selected(1)->Create("s04");
              }
              
              function MuestraAtributoValor($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("atributo".$this->Sufijo,"value", trim($Datos[0]['idatributo']));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo']));  
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                   
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaAtributoValor($Ajax,"NOEXISTEID","No existe una Combinacion con ese ID.");
                       }
              }
              function MuestraAtributoValorGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->AtributoValorGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }  
               
              function MuestraAtributoValorGuardado($Ajax,$AtributoValor)
              {       if (count($AtributoValor)>0)
                        {   $id = str_pad($AtributoValor[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraAtributoValorGrid($Ajax,$AtributoValor);
                            return $this->RespuestaAtributoValor($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaAtributoValor($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraAtributoValorEditado($Ajax,$AtributoValor)
              {       foreach ($AtributoValor as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraAtributoValorRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaAtributoValor($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaAtributoValor($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraAtributoValorEliminado($Ajax,$AtributoValor)
              {        foreach ($AtributoValor as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraAtributoValorRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaAtributoValor($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaAtributoValor($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraAtributoValorRowGrid($Ajax,$AtributoValor,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($AtributoValor['id'],$estado); 
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",trim($AtributoValor['id']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($AtributoValor['codigo']));
						$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($AtributoValor['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($AtributoValor['atributo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($this->Fechas->changeFormatDate($AtributoValor['fecreacion'],"DMY")[1]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($AtributoValor['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($AtributoValor['idestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($AtributoValor['idatributo']));
                        return $Ajax;
                }

              function MuestraAtributoValorExcepcion($Ajax,$Msg)
                {       return $this->RespuestaAtributoValor($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaAtributoValor($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

