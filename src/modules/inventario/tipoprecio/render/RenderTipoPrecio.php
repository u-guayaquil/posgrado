<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderTipoPrecio
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaTipoPrecioMantenimiento()
              {                          
                       $TipoPrecio = '<table class="Form-Frame" cellpadding="0">';
                       $TipoPrecio.= '       <tr height="30">';
                       $TipoPrecio.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $TipoPrecio.= '             <td class="Form-Label" style="width:75%">';
                       $TipoPrecio.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $TipoPrecio.= '             </td>';
                       $TipoPrecio.= '         </tr>';
                       $TipoPrecio.= '         <tr height="30">';
                       $TipoPrecio.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $TipoPrecio.= '             <td class="Form-Label" style="width:75%">';
                       $TipoPrecio.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $TipoPrecio.= '             </td>';
                       $TipoPrecio.= '         </tr>';
                       $TipoPrecio.= '         </tr>';                       
                       $TipoPrecio.= '         <tr height="30">';
                       $TipoPrecio.= '             <td class="Form-Label">Estado</td>';
                       $TipoPrecio.= '             <td class="Form-Label">';
                                                   $TipoPrecio.= $this->CreaComboEstado();
                       $TipoPrecio.= '             </td>';
                       $TipoPrecio.= '         </tr>';
                       $TipoPrecio.= '</table>';
                       return $TipoPrecio;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Descripci&oacute;n'] = array('180px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaTipoPrecioSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoPrecioGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function TipoPrecioGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $atributo)
                      {        $fecreacion = $this->Fechas->changeFormatDate($atributo['fecreacion'],"DMY");
                               $id = str_pad($atributo['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($atributo['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$atributo['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$atributo['idestado']);                                
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($atributo['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {       
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraTipoPrecio($Ajax,$Datos)
              {        $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                       $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                       $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                       $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                       return $Ajax;
              }
              function MuestraTipoPrecioGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoPrecioGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }        
              
              function MuestraTipoPrecioGuardado($Ajax,$TipoPrecio)
              {       if (count($TipoPrecio)>0)
                        {   $id = str_pad($TipoPrecio[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraTipoPrecioGrid($Ajax,$TipoPrecio);
                            return $this->RespuestaTipoPrecio($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaTipoPrecio($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraTipoPrecioEditado($Ajax,$TipoPrecio)
              {       foreach ($TipoPrecio as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoPrecioRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoPrecio($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaTipoPrecio($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraTipoPrecioEliminado($Ajax,$TipoPrecio)
              {        foreach ($TipoPrecio as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoPrecioRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoPrecio($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaTipoPrecio($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraTipoPrecioRowGrid($Ajax,$TipoPrecio,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($TipoPrecio['id'],$estado); 
                        $fecreacion = $this->Fechas->changeFormatDate($TipoPrecio['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",trim($TipoPrecio['id']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($TipoPrecio['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($TipoPrecio['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($TipoPrecio['idestado']));
                        return $Ajax;
                }

              function MuestraTipoPrecioExcepcion($Ajax,$Msg)
                {       return $this->RespuestaTipoPrecio($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaTipoPrecio($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

