<?php
         require_once("src/rules/inventario/servicio/ServicioTipoPrecio.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/inventario/tipoprecio/render/RenderTipoPrecio.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlTipoPrecio
         {     private  $Sufijo; 
               private  $ServicioTipoPrecio;
               private  $RenderTipoPrecio;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioTipoPrecio = new ServicioTipoPrecio();
                        $this->RenderTipoPrecio = new RenderTipoPrecio($Sufijo);
               }

               function CargaTipoPrecioBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaTipoPrecioMantenimiento()
               {        echo $this->RenderTipoPrecio->CreaTipoPrecioMantenimiento();
               }

               function CargaTipoPrecioSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoTipoPrecio = $this->ServicioTipoPrecio->BuscarTipoPrecio($prepareDQL);
                        echo $this->RenderTipoPrecio->CreaTipoPrecioSearchGrid($datoTipoPrecio);
               }         
               
               function MuestraTipoPrecioByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('id' => intval($id));
                        $datoTipoPrecio = $this->ServicioTipoPrecio->BuscarTipoPrecio($prepareDQL);
                        return $this->RenderTipoPrecio->MuestraTipoPrecio($ajaxRespon,$datoTipoPrecio);
               }

               function MuestraTipoPrecioByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoTipoPrecio = $this->ServicioTipoPrecio->BuscarTipoPrecio($prepareDQL);
                        return $this->RenderTipoPrecio->MuestraTipoPrecioGrid($ajaxRespon,$datoTipoPrecio);
               }

               function GuardaTipoPrecio($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $TipoPrecio  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioTipoPrecio->GuardaDBTipoPrecio($TipoPrecio);
                        if (is_numeric($id)){
                            $function = (empty($TipoPrecio->id) ? "MuestraTipoPrecioGuardado" : "MuestraTipoPrecioEditado");
                            $prepareDQL = array('id' => intval($id),'limite' => 50,'inicio' => 0);
                            $Datos = $this->ServicioTipoPrecio->BuscarTipoPrecio($prepareDQL);
                            return $this->RenderTipoPrecio->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderTipoPrecio->MuestraTipoPrecioExcepcion($ajaxRespon,intval($id));
                        }                    
               }
               
               function EliminaTipoPrecio($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioTipoPrecio->DesactivaTipoPrecio(intval($id));
                        $Datos = $this->ServicioTipoPrecio->BuscarTipoPrecio($prepareDQL);
                        return $this->RenderTipoPrecio->MuestraTipoPrecioEliminado($ajaxRespon,$Datos);
                }
         }

?>

