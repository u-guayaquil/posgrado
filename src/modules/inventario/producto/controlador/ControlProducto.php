<?php   

    require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
    require_once("src/rules/inventario/servicio/ServicioProducto.php");
    require_once("src/modules/inventario/producto/render/RenderProducto.php");
    require_once("src/rules/inventario/servicio/ServicioItem.php");    
    require_once("src/modules/inventario/item/render/RenderItem.php");
    require_once("src/rules/general/servicio/ServicioAcreedor.php");    
    require_once("src/modules/general/acreedor/render/RenderAcreedor.php");
    require_once("src/rules/general/servicio/ServicioPlanCuentas.php");    
    require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
    require_once('src/rules/inventario/servicio/ServicioItemAtributo.php');
    require_once('src/rules/inventario/servicio/ServicioAtributoValor.php');
    require_once('src/rules/inventario/servicio/ServicioProductoAtributoValor.php');
    require_once('src/rules/general/servicio/ServicioUnidad.php');
    require_once('src/rules/impuesto/servicio/ServicioIva.php');
    

    
    class ControlProducto 
    {       private $ServicioProducto;
            private $ServicioItem;
            private $ServicioAcreedor;
            private $ServicioPlanCuentas;
            private $ServicioProductoAtributoValor;
            
            private $RenderProducto;
            private $RenderItem;
            private $RenderAcreedor;
            private $RenderPlanCuentas;
            
            function __construct($Sufijo = "")
            {       $this->ServicioProducto = new ServicioProducto();
                    $this->RenderProducto = new RenderProducto($Sufijo);
                    $this->ServicioItem = new ServicioItem();
                    $this->RenderItem = new RenderItem($Sufijo);
                    $this->ServicioAcreedor = new ServicioAcreedor();
                    $this->RenderAcreedor = new RenderAcreedor($Sufijo);
                    $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                    $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
                    $this->ServicioProductoAtributoValor = new ServicioProductoAtributoValor();
            }
            
            function CargaProductoBarButton($Opcion)
            {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                    $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                    echo $this->RenderProducto->CreaBarButtonProducto($Datos);
            }
            
            function CargaProductoMantenimiento()
            {       echo $this->RenderProducto->CreaManteminientoProducto();
            }

            function CargaAtributo($Tipo,$Id)
            {       $ajaxResp = new xajaxResponse();    
                    if ($Tipo=="ITEM")
                    {   $ServicioItemAtributo = new ServicioItemAtributo();
                        $datoItem = $ServicioItemAtributo->BuscarAtributosxItem(intval($Id));                    
                        return $this->RenderProducto->CreaProductoAtributoValor($ajaxResp,$datoItem);
                    }
                    elseif ($Tipo=="PRODUCTO")
                    {   if (intval($Id)!=0)
                        {   return $this->RenderProducto->MuestraProductoAtributoValor($ajaxResp,intval($Id));
                        }
                    }    
            }

            function CargaAtributoModifica($Producto,$Item)
            {       $ajaxResp = new xajaxResponse();    
                    $ServicioItemAtributo = new ServicioItemAtributo();
                    $datoItem = $ServicioItemAtributo->BuscarAtributosxItem(intval($Item));                    
                    return $this->RenderProducto->ModificaProductoAtributoValor($ajaxResp,$datoItem,$Producto);
            }

            function ConsultaProductoByID($id)
            {       $ajaxResp = new xajaxResponse();
                    $Datos = $this->ServicioProducto->BuscarProductoByID($id);
                    return $this->RenderProducto->MuestraProductoForm($ajaxResp,$Datos);
            }
            
            function GuardaProducto($Form)
            {       $ajaxResp = new xajaxResponse();
                    $Producto = json_decode($Form)->Forma; 
                    if (empty($Producto->ProductoID)){
                        $Producto->ProductoID = $this->ServicioProducto->CreaProducto($Producto);
                        $this->ServicioProductoAtributoValor->CreaProductoAtributoValor($Producto);
                    }else{
                        $Producto->ProductoID = $this->ServicioProducto->EditaProducto($Producto);
                        $this->ServicioProductoAtributoValor->EditaProductoAtributoValor($Producto);
                    }     
                    return $this->RenderProducto->MuestraProductoActualizado($ajaxResp,$Producto->ProductoID);
            }

            function EliminaProducto($id)
            {       $ajaxResp = new xajaxResponse();
                    $this->ServicioProducto->DesactivaProducto($id);
                    return $this->RenderProducto->MuestraProductoEliminado($ajaxResp,$id);
            }
            
            function CargaModalGrid($Operacion)
            {       $ajaxResp = new xajaxResponse();    
                    $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');

                    if ($Operacion==="btitem")    
                    {   $Datos = $this->ServicioItem->BuscarItem($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Buscar Item";
                        $jsonModal['Carga'] = $this->RenderItem->CreaModalGridItem($Operacion,$Datos);
                        $jsonModal['Ancho'] = "396";
                    }   
                    elseif ($Operacion==="btproveedor")    
                    {   $Datos = $this->ServicioAcreedor->BuscarAcreedor($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Buscar Proveedor";
                        $jsonModal['Carga'] = $this->RenderAcreedor->CreaModalGridAcreedor($Operacion,$Datos);
                        $jsonModal['Ancho'] = "550";
                    }   
                    elseif ($Operacion==="btctamercaderia" || $Operacion==="btctaventas" || $Operacion==="btctacostos" || $Operacion==="btctadevolucion")    
                    {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '');
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Buscar Cuenta";
                        $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                        $jsonModal['Ancho'] = "750";
                        
                    }   
                    $ajaxResp->call("CreaModal", json_encode($jsonModal));
                    return $ajaxResp; 
            }
            
            function ConsultaModalGridByTX($Operacion,$Texto = "")
            {       $ajaxResp = new xajaxResponse();    
                    $texto = trim($Texto);
                    $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                    if ($Operacion==="btitem")    
                    {   $Datos = $this->ServicioItem->BuscarItemByDescripcion($prepareDQL);
                        return $this->RenderItem->MuestraModalGridItem($ajaxResp,$Operacion,$Datos);                                        
                    }    
                    if ($Operacion==="btproveedor")    
                    {   $Datos = $this->ServicioAcreedor->BuscarAcreedor($prepareDQL);
                        return $this->RenderAcreedor->MuestraModalGridAcreedor($ajaxResp,$Operacion,$Datos);                                        
                    }
                    if ($Operacion==="btctamercaderia" || $Operacion==="btctaventas" || $Operacion==="btctacostos" || $Operacion==="btctadevolucion")                            
                    {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        return $this->RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);                                        
                    }
            }
    }

?>
