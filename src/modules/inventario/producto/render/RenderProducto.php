<?php

require_once("src/libs/clases/SearchGrid.php");
require_once("src/libs/clases/BarButton.php");
require_once('src/libs/clases/ComboBox.php');
require_once('src/libs/clases/SearchInput.php');

class RenderProducto 
{           private $Sufijo;
            private $Select;
            private $Search;
            private $Maxlen;
            
            function __construct($Sufijo = "") 
            {       $this->Sufijo = $Sufijo;
                    $this->Select = new ComboBox($Sufijo);
                    $this->Search = new SearchInput($Sufijo);
                    $this->Maxlen = 10;
            }

            function CreaBarButtonProducto($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaManteminientoProducto()
            {       $ServicioUnidad = new ServicioUnidad();
                    $ServicioIva = new ServicioIva();
                    $datoUnidad = $ServicioUnidad->BuscarUnidadByArrayID(array('estado' => 1));
                    $datoIvaCom = $ServicioIva->BuscarIvaByArrayImpuestoID(array('impuesto' => '4,5,7,9'));
                    $datoIvaVen = $ServicioIva->BuscarIvaByArrayImpuestoID(array('impuesto' => '6,8,10'));

                    $Producto = '<table border="0" class="Form-Frame" cellpadding="0">';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label"   style="width:20%">Id</td>';
                    $Producto.= '            <td class="Form-Label"   style="width:33%">';
                    $Producto.= '                <input type="text"   class="txt-input t03" id="ProductoID'.$this->Sufijo.'" name="ProductoID'.$this->Sufijo.'" value="" maxlength="10" onKeyDown="return KeyDown'.$this->Sufijo.'(event,\''.$this->Sufijo.'\',this); "  onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Producto.= '                <input type="hidden" id="valatrib'.$this->Sufijo.'" name="valatrib'.$this->Sufijo.'" value="" />';
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label" style="width:18%">Código</td>';
                    $Producto.= '            <td class="Form-Label" style="width:29%">';
                    $Producto.= '                <input type="text" class="txt-upper t04" id="Codigo'.$this->Sufijo.'" name="Codigo'.$this->Sufijo.'" value="" maxlength="15" onKeyDown="return soloNumerosLetras(event); " disabled/>&nbsp;&nbsp;&nbsp;&nbsp;';
                    $Producto.=                  $this->CreaComboEstadoByArray(array(0,1));
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label">Item</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Search->TextSch("item","","")->Enabled(false)->Create("t08");
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label">Unidad de Medida</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Select->Combo('idunidad',$datoUnidad)->Fields('id', 'descripcion')->Create("s08");
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr>';
                    $Producto.= '            <td id="Form_Atrib'.$this->Sufijo.'" class="Form-Label" colspan="4">';
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label">Proveedor</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Search->TextSch("proveedor","","")->Enabled(false)->Create("t08");
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label">Decímales Cantidad</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.= '                <input type="text" class="txt-right t01" id="Cant'.$this->Sufijo.'" name="Cant'.$this->Sufijo.'" value="2" maxlength="1" onKeyDown="return soloNumeros(event); " disabled/>&nbsp;';
                    $Producto.= '                Costo&nbsp;&nbsp<input type="text" class="txt-right t01" id="Cost'.$this->Sufijo.'" name="Cost'.$this->Sufijo.'" value="2" maxlength="1" onKeyDown="return soloNumeros(event);" disabled/>&nbsp;';
                    $Producto.= '                Precio&nbsp;&nbsp<input type="text" class="txt-right t01" id="Prec'.$this->Sufijo.'" name="Prec'.$this->Sufijo.'" value="2" maxlength="1" onKeyDown="return soloNumeros(event);" disabled/>';
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label">Descripción</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.= '                <input type="text" class="txt-upper t09" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="100" disabled/>';
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label">Paga Comisión</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.= '                <input type="text" class="txt-right t02" id="Comis'.$this->Sufijo.'" name="Comis'.$this->Sufijo.'" value="1" maxlength="4" onKeyDown="return soloFloat(event,this,2); " disabled/>';
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label">Stock Minimo</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.= '                <input type="text" class="txt-right t03" id="Stkmin'.$this->Sufijo.'" name="Stkmin'.$this->Sufijo.'" value="" maxlength="12" onKeyDown="return soloFloat(event,this,2); " disabled/>&nbsp&nbsp';                    
                    $Producto.= '                Máximo&nbsp&nbsp<input type="text" class="txt-right t03" id="Stkmax'.$this->Sufijo.'" name="Stkmax'.$this->Sufijo.'" value="" maxlength="12" onKeyDown="return soloFloat(event,this,2); " disabled/>';                                                            
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label">% Dscto Máximo</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.= '                <input type="text" class="txt-right t02" id="Dscto'.$this->Sufijo.'" name="Dscto'.$this->Sufijo.'" value="" maxlength="5" onKeyDown="return soloFloat(event,this,2); " disabled/>&nbsp&nbsp';                                        
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label">Cta. Mercadería</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Search->TextSch("ctamercaderia","","")->Enabled(false)->Create("t08");
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label">IVA Compra</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Select->Combo('idivacompra',$datoIvaCom)->Fields('id', 'descripcion')->Create("s08");
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label">Cta. Ventas</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Search->TextSch("ctaventas","","")->Enabled(false)->Create("t08");
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label">IVA Venta</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Select->Combo('idivaventa',$datoIvaVen)->Fields('id', 'descripcion')->Create("s08");
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label">Cta. Costos</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Search->TextSch("ctacostos","","")->Enabled(false)->Create("t08");
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label">ICE</td>';
                    $Producto.= '            <td class="Form-Label">';
                  //$Producto.=                  $this->Select->Combo('idice',$datoContador)->Fields('id', 'identificacion')->Selected($Datos->getTipoIdentContador())->Create("s04");                    
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    $Producto.= '        <tr height="30">';
                    $Producto.= '            <td class="Form-Label">Cta. Devolución</td>';
                    $Producto.= '            <td class="Form-Label">';
                    $Producto.=                  $this->Search->TextSch("ctadevolucion","","")->Enabled(false)->Create("t08");
                    $Producto.= '            </td>';
                    $Producto.= '            <td class="Form-Label">Servicios</td>';
                    $Producto.= '            <td class="Form-Label">';
                  //$Producto.=                  $this->Select->Combo('idservicio',$datoContador)->Fields('id', 'identificacion')->Selected($Datos->getTipoIdentContador())->Create("s04");                    
                    $Producto.= '            </td>';
                    $Producto.= '        </tr>';
                    return $Producto.'</table>';
            }
            
            function CreaProductoAtributoValor($xAjax,$datoItemAt)
            {       $ServicioAtributoValor = new ServicioAtributoValor();        
                    $index = 0;
                    $Objatrib = '';
                    $Controls = 0;
                    $Producto = '<table border="0" class="Form-Frame" cellpadding="0">';
                    foreach($datoItemAt as $ItemAtrib)
                    {       $Atributo = trim(strtolower($ItemAtrib['descripcion']));
                            $datoAtv = $ServicioAtributoValor->BuscarAtributoValorByAtribID($ItemAtrib['idatributo']);
                            if (count($datoAtv)>0)
                            {   $Objatrib.= ($index==0 ? "" : ":").'id'.$Atributo;
                                $Controls++;
                                if ($Controls==1)
                                {   $Producto.= '<tr height="30">';
                                    $Producto.= '    <td class="Form-Label" style="width:20%">'. ucwords($Atributo).'</td>';
                                    $Producto.= '    <td class="Form-Label" style="width:33%">';
                                    $Producto.=          $this->Select->Combo('id'.$Atributo,$datoAtv)->Enabled(true)->Fields('id', 'descripcion')->Create("s09");
                                    $Producto.= '    </td>';
                                }
                                else
                                {   $Controls = 0;
                                    $Producto.= '    <td class="Form-Label" style="width:18%">'.ucwords($Atributo).'</td>';
                                    $Producto.= '    <td class="Form-Label" style="width:29%">';
                                    $Producto.=          $this->Select->Combo('id'.$Atributo,$datoAtv)->Enabled(true)->Fields('id', 'descripcion')->Create("s08");
                                    $Producto.= '    </td>';
                                    $Producto.= '</tr>';                                
                                }    
                                $index++;
                            }
                    }
                    if ($Controls==1)
                    {   $Producto.= '    <td class="Form-Label" colspan="2"></td>';
                        $Producto.= '</tr>';
                    }
                    $xAjax->Assign("valatrib".$this->Sufijo,"value",$Objatrib);
                    $xAjax->Assign("Form_Atrib".$this->Sufijo,"innerHTML",$Producto.'</table>');
                    return $xAjax;
            }

            function MuestraProductoForm($Ajax,$Datos)
            {       foreach($Datos as $Dato)
                    {   $id = str_pad($Dato['id'], $this->Maxlen,"0",STR_PAD_LEFT);    
                        $Ajax->Assign("ProductoID".$this->Sufijo,"value",$id);                            
                        $Ajax->Assign("iditem".$this->Sufijo,"value",$Dato['iditem']);
                        $Ajax->Assign("txitem".$this->Sufijo,"value",trim($Dato['item']));
                        $Ajax->Assign("Codigo".$this->Sufijo,"value",trim($Dato['codigo']));
                        $Ajax->Assign("descripcion".$this->Sufijo,"value",trim($Dato['descripcion']));
                        $Ajax->Assign("idunidad".$this->Sufijo,"value",$Dato['idunidad']);  
                        $Ajax->Assign("idproveedor".$this->Sufijo,"value",$Dato['idproveedor']);
                        $Ajax->Assign("txproveedor".$this->Sufijo,"value",trim($Dato['proveedor']));
                        $Ajax->Assign("estado".$this->Sufijo,"value",trim($Dato['idestado']));
                        
                        if ($Datos[0]['ctamercaderia']!=0){
                            $Ajax->Assign("idctamercaderia".$this->Sufijo,"value",trim($Dato['ctamercaderia']));
                            $Ajax->Assign("txctamercaderia".$this->Sufijo,"value",trim($Dato['mercaderia']));
                        }else{
                            $Ajax->Assign("idctamercaderia".$this->Sufijo,"value",'');
                            $Ajax->Assign("txctamercaderia".$this->Sufijo,"value",'');
                        }

                        if ($Datos[0]['ctaventas']!=0){
                            $Ajax->Assign("idctaventas".$this->Sufijo,"value",trim($Dato['ctaventas']));
                            $Ajax->Assign("txctaventas".$this->Sufijo,"value",trim($Dato['ventas']));
                        }else{
                            $Ajax->Assign("idctaventas".$this->Sufijo,"value",'');
                            $Ajax->Assign("txctaventas".$this->Sufijo,"value",'');
                        }
                        
                        if ($Datos[0]['ctacostos']!=0){
                            $Ajax->Assign("idctacostos".$this->Sufijo,"value",trim($Dato['ctacostos']));
                            $Ajax->Assign("txctacostos".$this->Sufijo,"value",trim($Dato['costos']));
                        }else{
                            $Ajax->Assign("idctacostos".$this->Sufijo,"value",'');
                            $Ajax->Assign("txctacostos".$this->Sufijo,"value",'');
                        }
                        
                        if ($Datos[0]['ctadevolucion']!=0){
                            $Ajax->Assign("idctadevolucion".$this->Sufijo,"value",trim($Dato['ctadevolucion']));
                            $Ajax->Assign("txctadevolucion".$this->Sufijo,"value",trim($Dato['devolucion']));
                        }else{
                            $Ajax->Assign("idctadevolucion".$this->Sufijo,"value",'');
                            $Ajax->Assign("txctadevolucion".$this->Sufijo,"value",'');
                        }
                    
                        $Ajax->Assign("idivacompra".$this->Sufijo,"value",$Dato['ivacompras']);
                        $Ajax->Assign("idivaventa".$this->Sufijo,"value",$Dato['ivaventas']);

                        $decimales = $Datos[0]['factcantidad'];                         
                        $Ajax->Assign("Cant".$this->Sufijo,"value",$decimales);
                        $Ajax->Assign("Cost".$this->Sufijo,"value",$Dato['factcosto']);
                        $Ajax->Assign("Prec".$this->Sufijo,"value",$Dato['factprecio']);
                        $Ajax->Assign("Comis".$this->Sufijo,"value", number_format($Dato['pagacoms'],2,".",","));
                        $Ajax->Assign("Dscto".$this->Sufijo,"value", number_format($Dato['dsctomax'],2,".",","));
                        $Ajax->Assign("Stkmin".$this->Sufijo,"value",number_format($Dato['stockmin'],$decimales,".",","));
                        $Ajax->Assign("Stkmax".$this->Sufijo,"value",number_format($Dato['stockmax'],$decimales,".",","));
                        $Ajax = $this->MuestraProductoAtributoValor($Ajax,trim($Dato['id']));
                        
                        $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                        return $Ajax;
                    }
                    return $this->RespuestaProducto($Ajax,"NOEXISTEID","No existe un producto con el ID ingresado.");
            }

            function MuestraProductoAtributoValor($xAjax,$Producto)
            {       $ServicioProductoAtributoValor = new ServicioProductoAtributoValor();        
                    $DatoItemProductoAtributoValor = $ServicioProductoAtributoValor->BuscarProductoAtributoValorByProducto($Producto);
                    $Controls = 0;
                    $Producto = '<table border="0" class="Form-Frame" cellpadding="0">';
                    foreach($DatoItemProductoAtributoValor as $ItemAtrib)
                    {       $Controls++;
                            $Atributo = trim(strtolower($ItemAtrib['txatrib']));
                            if ($Controls==1)
                            {   $Producto.= '<tr height="30">';
                                $Producto.= '    <td class="Form-Label" style="width:20%">'.ucwords($Atributo).'</td>';
                                $Producto.= '    <td class="Form-Label" style="width:33%">';
                                $Producto.= '        <input type="text" class="txt-upper t09" value="'.trim($ItemAtrib['txatribv']).'" disabled/>';   
                                $Producto.= '    </td>';
                            }    
                            else
                            {   $Controls = 0;
                                $Producto.= '    <td class="Form-Label" style="width:18%">'.ucwords($Atributo).'</td>';
                                $Producto.= '    <td class="Form-Label" style="width:29%">';
                                $Producto.= '        <input type="text" class="txt-upper t08" value="'.trim($ItemAtrib['txatribv']).'" disabled/>';
                                $Producto.= '    </td>';
                                $Producto.= '</tr>';
                            }
                    }
                    if ($Controls==1)
                    {   $Producto.= '    <td class="Form-Label" colspan="2"></td>';
                        $Producto.= '</tr>';
                    }
                    $Producto.= '</table>';
                    $xAjax->Assign("Form_Atrib".$this->Sufijo,"innerHTML",$Producto);
                    return $xAjax;
            }


            function ModificaProductoAtributoValor($xAjax,$datoItemAt,$Idproducto)
            {       $ServicioAtributoValor = new ServicioAtributoValor();        
                    $index = 0;
                    $Objatrib = '';
                    $Controls = 0;
                    $Producto = '<table border="0" class="Form-Frame" cellpadding="0">';
                    foreach($datoItemAt as $ItemAtrib)
                    {       $Atributo = trim(strtolower($ItemAtrib['descripcion']));
                            $DatoAtrb = $ServicioAtributoValor->BuscarAtributoValorByProductoAtributoID($Idproducto,$ItemAtrib['idatributo']);
                            if (count($DatoAtrb)>0)
                            {   $Objatrib.= ($index==0 ? "" : ":").'id'.$Atributo;
                                $Controls++;
                                if ($Controls==1)
                                {   $Producto.= '<tr height="30">';
                                    $Producto.= '    <td class="Form-Label" style="width:20%">'. ucwords($Atributo).'</td>';
                                    $Producto.= '    <td class="Form-Label" style="width:33%">';
                                    $Producto.=          $this->Select->Combo('id'.$Atributo,$DatoAtrb)->Enabled(true)->Selected("idatributo_valor")->Fields('id', 'descripcion')->Create("s09");
                                    $Producto.= '    </td>';
                                }
                                else
                                {   $Controls = 0;
                                    $Producto.= '    <td class="Form-Label" style="width:18%">'.ucwords($Atributo).'</td>';
                                    $Producto.= '    <td class="Form-Label" style="width:29%">';
                                    $Producto.=          $this->Select->Combo('id'.$Atributo,$DatoAtrb)->Enabled(true)->Selected("idatributo_valor")->Fields('id', 'descripcion')->Create("s08");
                                    $Producto.= '    </td>';
                                    $Producto.= '</tr>';                                
                                }    
                                $index++;
                            }
                    }
                    
                    if ($Controls==1)
                    {   $Producto.= '    <td class="Form-Label" colspan="2"></td>';
                        $Producto.= '</tr>';
                    }
                    $xAjax->Assign("valatrib".$this->Sufijo,"value",$Objatrib);
                    $xAjax->Assign("Form_Atrib".$this->Sufijo,"innerHTML",$Producto.'</table>');
                    return $xAjax;
            }

            function MuestraProductoGuardado($Ajax,$Producto)
            {       if (count($Producto)>0)
                    {   $id = str_pad($Producto[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        return $this->RespuestaProducto($Ajax,"GUARDADO",$id);
                    }
                    return $this->RespuestaProducto($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraProductoEditado($Ajax,$Producto)
            {       foreach ($Producto as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            return $this->RespuestaProducto($Ajax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaProducto($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraProductoEliminado($Ajax,$Respuesta)
            {       if (is_bool($Respuesta)){
                        return $this->RespuestaProducto($Ajax,"EXCEPCION","No se pudo desactivar el registro.");
                    }else{
                        return $this->RespuestaProducto($Ajax,"ELIMINADO",0);
                    }
            }

            function MuestraProductoExcepcion($Ajax,$Msg)
            {       return $this->RespuestaProducto($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaProducto($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox($this->Sufijo);
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Selected(1)->Create("s03");
            }        
            
            function MuestraProductoActualizado($Ajax,$Respuesta)
            {       if (is_bool($Respuesta))
                        return $this->RespuestaProducto($Ajax,"EXCEPCION",$Respuesta);
                    else
                    {   $Ajax = $this->MuestraProductoAtributoValor($Ajax,$Respuesta);
                        return $this->RespuestaProducto($Ajax,"GUARDADO",str_pad($Respuesta, $this->Maxlen,"0",STR_PAD_LEFT));
                    }
            }
            

}
