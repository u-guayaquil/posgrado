<?php   $Sufijo = '_producto';

        require_once('src/modules/inventario/producto/controlador/ControlProducto.php');
        $ControlProducto = new ControlProducto($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlProducto,'GuardaProducto'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlProducto,'EliminaProducto'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlProducto,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('CargaAtributo'.$Sufijo, $ControlProducto,'CargaAtributo'));
        $xajax->register(XAJAX_FUNCTION,array('CargaAtributoModifica'.$Sufijo, $ControlProducto,'CargaAtributoModifica'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlProducto,'ConsultaProductoByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlProducto,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript" src="src/modules/inventario/producto/js/JSProducto.js"></script>
    </head>
        <body>
        <div class="FormBasic" style="width:750px">
            <div class="FormSectionMenu">              
            <?php   $ControlProducto->CargaProductoBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlProducto->CargaProductoMantenimiento();  ?>
                    </form>
            </div>    
        </div>
        </body>
    </html>
            