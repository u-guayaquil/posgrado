    var objproducto = "Codigo:iditem:txitem:idunidad:idproveedor:txproveedor:Cant:Cost:Prec:Comis:descripcion:idctamercaderia:";
        objproducto = objproducto + "txctamercaderia:idctaventas:txctaventas:idctacostos:txctacostos:idctadevolucion:txctadevolucion:";
        objproducto = objproducto + "idivacompra:idivaventa:Stkmin:Stkmax:Dscto";
    var savproducto = objproducto + ":ProductoID:estado:valatrib";
    var valproducto = "Codigo:iditem:idproveedor:Cant:Cost:Prec:Comis:descripcion:"; //idctamercaderia:idctaventas:valatrib
        valproducto = valproducto + "Stkmin:Stkmax:Dscto"; //idctacostos:idctadevolucion:
    var btnproducto = "btitem:btproveedor:btctamercaderia:btctaventas:btctacostos:btctadevolucion";

    function ButtonClick_producto(Sufijo, Operacion)
    {       if (Operacion == "addNew")
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateEnabled(Sufijo, btnproducto);
                ElementStatus(Sufijo, objproducto, "ProductoID");
                ElementClear(Sufijo, "Form_Atrib:ProductoID:" + objproducto);
                ElementSetValue(Sufijo, "Cant", "2");
                ElementSetValue(Sufijo, "Cost", "2");
                ElementSetValue(Sufijo, "Prec", "2");
                ElementSetValue(Sufijo, "Comis", "1");
                ElementSetValue(Sufijo, "estado", "1");
            } 
            else if (Operacion == "addMod")
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateEnabled(Sufijo, btnproducto);
                ElementClear(Sufijo, "Form_Atrib");
                if (ElementGetValue(Sufijo, "estado") == 0)
                    ElementStatus(Sufijo, objproducto + ":estado", "ProductoID");
                else
                    ElementStatus(Sufijo, objproducto, "ProductoID");
                xajax_CargaAtributoModifica_producto(ElementGetValue(Sufijo, "ProductoID"), ElementGetValue(Sufijo, "iditem"));
            }
            else if (Operacion === 'addDel')
            {   if (ElementGetValue(Sufijo, "estado") == 1)
                {   if (confirm("Desea inactivar este regsistro?")){
                        xajax_Elimina_producto(ElementGetValue(Sufijo, "ProductoID"));
                    }
                }else
                    alert("El registro se encuentra inactivo");
            } 
            else if (Operacion == "addSav")
            {   var atributos = ElementGetValue(Sufijo, "valatrib");
                if (ElementValidateBeforeSave(Sufijo, valproducto))
                {   if (BarButtonState(Sufijo, "Inactive"))
                    {   var Forma = PrepareElements(Sufijo, savproducto + ":" + atributos);
                        xajax_Guarda_producto(JSON.stringify({Forma}));
                    }
                }
            } 
            else if (Operacion == "addCan")
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateDisabled(Sufijo, btnproducto);
                ElementStatus(Sufijo, "ProductoID", objproducto + ":estado");
                ElementClear(Sufijo, "Form_Atrib:ProductoID:" + objproducto);
            } 
            else{   
                xajax_CargaModal_producto(Operacion);
            }
            return false;
    }

    function XAJAXResponse_producto(Sufijo, Mensaje, Datos)
    {       if (Mensaje === "GUARDADO")
            {   BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, btnproducto);
                ElementSetValue(Sufijo, "ProductoID", Datos);
                ElementStatus(Sufijo, "ProductoID", objproducto + ":estado");
                alert('Los datos se guardaron correctamente');
            }
            else if (Mensaje === 'EXCEPCION')
            {   alert(Datos); //BarButtonState(Sufijo,"addNew");
            }  
            else if (Mensaje === 'NOEXISTEID')
            {   ElementSetValue(Sufijo, "ProductoID", "");
                BarButtonState(Sufijo, "Default");
                alert(Datos);
            }
            else if (Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
            }
    }

    function KeyDown_producto(Evento, Sufijo, Elemento)
    {       if (Elemento.id == "ProductoID" + Sufijo){  
                Params = Dependencia_II(Evento, "NUM"); 
                if (Params[0])
                {   BarButtonState(Sufijo, "Default");
                    ElementClear(Sufijo,"Form_Atrib:" + objproducto);    
                }   
                return Params[2];
            }
    }

    function SearchByElement_producto(Sufijo, Elemento)
    {       Elemento.value = TrimElement(Elemento.value);
            if (Elemento.name == "ProductoID" + Sufijo)
            {   if (Elemento.value.length != 0)
                {   ContentFlag = document.getElementById("descripcion" + Sufijo);
                    if (ContentFlag.value.length == 0)
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        xajax_BuscaByID_producto(Elemento.value);
                    }
                }
            } 
            else
            {   if (Elemento.id === "FindItemTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_producto("btitem", Elemento.value);
                }
                if (Elemento.id === "FindProveedorTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_producto("btproveedor", Elemento.value);
                }
                if (Elemento.id === "FindCtamercaderiaTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_producto("btctamercaderia", Elemento.value);
                }
                if (Elemento.id === "FindCtaventasTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_producto("btctaventas", Elemento.value);
                }
                if (Elemento.id === "FindCtacostosTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_producto("btctacostos", Elemento.value);
                }
                if (Elemento.id === "FindCtadevolucionTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_producto("btctadevolucion", Elemento.value);
                }
            }
            return false;
    }

    function SearchItemGetData_producto(Sufijo, DatosGrid)
    {       document.getElementById("iditem" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txitem" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            cerrar();
            xajax_CargaAtributo_producto("ITEM", DatosGrid.cells[0].childNodes[0].nodeValue);
            return false;
    }
    
    function SearchProveedorGetData_producto(Sufijo, DatosGrid)
    {       document.getElementById("idproveedor" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txproveedor" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCtamercaderiaGetData_producto(Sufijo, DatosGrid)
    {       document.getElementById("idctamercaderia" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txctamercaderia" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCtaventasGetData_producto(Sufijo, DatosGrid)
    {       document.getElementById("idctaventas" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txctaventas" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCtacostosGetData_producto(Sufijo, DatosGrid)
    {       document.getElementById("idctacostos" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txctacostos" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCtadevolucionGetData_producto(Sufijo, DatosGrid)
    {       document.getElementById("idctadevolucion" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txctadevolucion" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }
