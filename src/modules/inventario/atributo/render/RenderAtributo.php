<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/entidad/Estado.php"); 
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderAtributo
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo; 
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaAtributoMantenimiento()
              {                      
                       $Atributo = '<table class="Form-Frame" cellpadding="0">';
                       $Atributo.= '         <tr height="30">';
                       $Atributo.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $Atributo.= '             <td class="Form-Label" style="width:75%">';
                       $Atributo.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion,codigo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $Atributo.= '             </td>';
                       $Atributo.= '         </tr>';
                       $Atributo.= '         <tr height="28">';
                       $Atributo.= '              <td class="Form-Label">C&oacute;digo</td>';
                       $Atributo.= '              <td class="Form-Label">';
                       $Atributo.= '               <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['codigo'].'" onKeyDown="return soloNumerosLetras(event); "; disabled/>';
                       $Atributo.= '              </td>';
                       $Atributo.= '         </tr>';   
                       $Atributo.= '         <tr height="30">';
                       $Atributo.= '             <td class="Form-Label" style="width:25%">Atributo</td>';
                       $Atributo.= '             <td class="Form-Label" style="width:75%">';
                       $Atributo.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Atributo.= '             </td>';
                       $Atributo.= '         </tr>'; 
                       $Atributo.= '         <tr height="30">';
                       $Atributo.= '             <td class="Form-Label">Estado</td>';
                       $Atributo.= '             <td class="Form-Label">';
                                                   $Atributo.= $this->CreaComboEstado();
                       $Atributo.= '             </td>';
                       $Atributo.= '         </tr>';
                       $Atributo.= '</table>';
                       return $Atributo;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('100px','left','');
                        $Columns['Descripci&oacute;n'] = array('150px','left','');
                        $Columns['Creacion']   = array('130px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaAtributoSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->AtributoGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function AtributoGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $atributo)
                      {        $fecreacion = $this->Fechas->changeFormatDate($atributo['fecreacion'],"DMY");
                               $id = str_pad($atributo['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($atributo['codigo']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($atributo['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$atributo['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$atributo['idestado']); 
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($atributo['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {       
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraAtributo($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo'])); 
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));       
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaAtributo($Ajax,"NOEXISTEID","No existe el Atributo con ese ID.");
                       }
              }
              function MuestraAtributoGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->AtributoGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }   
               
              function MuestraAtributoGuardado($Ajax,$Atributo)
              {       if (count($Atributo)>0)
                        {   $id = str_pad($Atributo[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraAtributoGrid($Ajax,$Atributo);
                            return $this->RespuestaAtributo($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaAtributo($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraAtributoEditado($Ajax,$Atributo)
              {       foreach ($Atributo as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraAtributoRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaAtributo($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaAtributo($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraAtributoEliminado($Ajax,$Atributo)
              {        foreach ($Atributo as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraAtributoRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaAtributo($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaAtributo($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraAtributoRowGrid($Ajax,$Atributo,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Atributo['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($Atributo['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Atributo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Atributo['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Atributo['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Atributo['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Atributo['idestado'])); 
                        return $Ajax;
                }

              function MuestraAtributoExcepcion($Ajax,$Msg)
                {       return $this->RespuestaAtributo($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaAtributo($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

