<?php
         require_once("src/rules/inventario/servicio/ServicioAtributo.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/inventario/atributo/render/RenderAtributo.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlAtributo
         {     private  $Sufijo; 
               private  $ServicioAtributo;
               private  $RenderAtributo;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioAtributo = new ServicioAtributo();
                        $this->RenderAtributo = new RenderAtributo($Sufijo);
               }

               function CargaAtributoBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaAtributoMantenimiento()
               {        echo $this->RenderAtributo->CreaAtributoMantenimiento();
               }

               function CargaAtributoSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoAtributo = $this->ServicioAtributo->BuscarAtributo($prepareDQL);
                        echo $this->RenderAtributo->CreaAtributoSearchGrid($datoAtributo);
               }         
               
               function MuestraAtributoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoAtributo = $this->ServicioAtributo->BuscarAtributo($prepareDQL);
                        return $this->RenderAtributo->MuestraAtributo($ajaxRespon,$datoAtributo);
               }

               function MuestraAtributoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoAtributo = $this->ServicioAtributo->BuscarAtributo($prepareDQL);
                        return $this->RenderAtributo->MuestraAtributoGrid($ajaxRespon,$datoAtributo);
               }

               function GuardaAtributo($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Atributo  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioAtributo->GuardaDBAtributo($Atributo);
                        if (is_numeric($id)){
                            $function = (empty($Atributo->id) ? "MuestraAtributoGuardado" : "MuestraAtributoEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioAtributo->BuscarAtributo($prepareDQL);
                            return $this->RenderAtributo->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderAtributo->MuestraAtributoExcepcion($ajaxRespon,intval($id));
                        } 
               }
               
               function EliminaAtributo($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioAtributo->DesactivaAtributo(intval($id));
                        $Datos = $this->ServicioAtributo->BuscarAtributo($prepareDQL);
                        return $this->RenderAtributo->MuestraAtributoEliminado($ajaxRespon,$Datos);
                }
         }

?>

