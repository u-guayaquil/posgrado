<?php
         require_once("src/rules/inventario/servicio/ServicioItemAtributo.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/inventario/itematributo/render/RenderItemAtributo.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlItemAtributo
         {     
               private  $ServicioItemAtributo;
               private  $RenderItemAtributo;
               private  $SufijoPantalla;

               function __construct($Sufijo = "")
               {        $this->SufijoPantalla = $Sufijo;
                        $this->ServicioItemAtributo = new ServicioItemAtributo();
                        $this->RenderItemAtributo = new RenderItemAtributo($Sufijo);
               }

               function CargaItemAtributoBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $this->RenderItemAtributo->CreaOpcionBarButton($Datos);
                }
               
               function CargaItemAtributoMantenimiento()
               {        
                        echo $this->RenderItemAtributo->CreaItemAtributoMantenimiento();
               }
               
               function ControlItemAtributo($item)
               {        $ajaxRespon = new xajaxResponse(); 
                        $tablaDepartamentos = $this->RenderItemAtributo->CreaAtributosporItems($item);
                        $nombre = "SeccionAtributos".$this->SufijoPantalla;
                        $ajaxRespon->Assign($nombre,"innerHTML",$tablaDepartamentos);
                        return $ajaxRespon;
               }

               function CargaItemAtributoSearchGrid()
               {        
                        $datoItemAtributo = $this->ServicioItemAtributo->BuscarItemAtributo();
                        echo $this->RenderItemAtributo->CreaItemAtributoSearchGrid($datoItemAtributo); 
               }         
               
               function MuestraItemAtributoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('id' => intval($id));
                        $datoItemAtributo = $this->ServicioItemAtributo->BuscarItemAtributo($prepareDQL);
                        return $this->RenderItemAtributo->MuestraItemAtributo($ajaxRespon,$datoItemAtributo);
               }

               function MuestraItemAtributoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoItemAtributo = $this->ServicioItemAtributo->BuscarItemAtributo($prepareDQL);
                        return $this->RenderItemAtributo->MuestraItemAtributoGrid($ajaxRespon,$datoItemAtributo);
               }

               function GuardaItemAtributo($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $ItemAtributo  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioItemAtributo->GuardaDBItemAtributo($ItemAtributo);
                        if (is_numeric($id)){
                            $function = (empty($ItemAtributo->id) ? "MuestraItemAtributoGuardado" : "MuestraItemAtributoEditado");
                            $prepareDQL = array('id' => intval($id),'limite' => 50,'inicio' => 0);
                            $Datos = $this->ServicioItemAtributo->BuscarItemAtributo($prepareDQL); 
                            return $this->RenderItemAtributo->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderItemAtributo->MuestraItemAtributoExcepcion($ajaxRespon,intval($id));
                        }                   
               } 
               
               function EliminaItemAtributo($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioItemAtributo->DesactivaItemAtributo(intval($id));
                        $Datos = $this->ServicioItemAtributo->BuscarItemAtributo($prepareDQL);
                        return $this->RenderItemAtributo->MuestraItemAtributoEliminado($ajaxRespon,$Datos);
                }
               
         }

?>

