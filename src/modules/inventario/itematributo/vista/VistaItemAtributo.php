<?php 
         $Sufijo = '_ItemAtributo';

         require_once('src/modules/inventario/itematributo/controlador/ControlItemAtributo.php');
         $ControlItemAtributo = new ControlItemAtributo($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlItemAtributo,'GuardaItemAtributo'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlItemAtributo,'MuestraItemAtributoByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlItemAtributo,'MuestraItemAtributoByTX'));
         $xajax->register(XAJAX_FUNCTION,array('ControlItemAtributo'.$Sufijo, $ControlItemAtributo,'ControlItemAtributo'));
         $xajax->register(XAJAX_FUNCTION,array('DepartamentosPrimeraVez'.$Sufijo, $ControlItemAtributo,'DepartamentosPrimeraVez'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlItemAtributo,'EliminaItemAtributo'));
         $xajax->register(XAJAX_FUNCTION,array('CargaItemAtributoSearchGrid'.$Sufijo, $ControlItemAtributo,'CargaItemAtributoSearchGrid'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); ?>
                <?php require_once('src/utils/links.php'); ?>
         <script type="text/javascript"> 
             
                 function ButtonClick_ItemAtributo(Sufijo,Operacion)
                 {        var objItemAtributo = "id:ItemAtributo";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,objItemAtributo,'id');
                              ElementClear(Sufijo,"id:"+objItemAtributo);  
                              var item = document.getElementById('ItemAtributo'+Sufijo).value;
                              xajax_ControlItemAtributo_ItemAtributo(item);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objItemAtributo,'id');
                               MuestraalGuardar_ItemAtributo(1);
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_ItemAtributo(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"ItemAtributo"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = xajax.getFormValues('FormDatos');
                                       xajax_Guarda_ItemAtributo(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",'ItemAtributo');
                               ElementClear(Sufijo,"id:"+objItemAtributo);
                               document.getElementById('SeccionAtributos'+Sufijo).innerHTML='';
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }
                 
                 function ControlItemAtributo_ItemAtributo(Sufijo,Elemento){
                     var item = Elemento.value;
                     document.getElementById('SeccionAtributos'+Sufijo).innerHTML='';
                     xajax_ControlItemAtributo_ItemAtributo(item);
                 }
                 
                 function SearchGetData_ItemAtributoDetalle(Sufijo,Grilla)
                 {   Sufijo = Sufijo.substring(0, (Sufijo.length)-7);
                     var Boton=document.getElementById('addSav'+Sufijo);
                        if(Boton.disabled===true){
                           if(Grilla.cells[3]){
                                BarButtonState(Sufijo,"Active");
                                var item = Grilla.cells[3].childNodes[0].nodeValue; 
                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                                document.getElementById("ItemAtributo"+Sufijo).value = item;
                                document.getElementById("iditem"+Sufijo).value = item;
                                xajax_ControlItemAtributo_ItemAtributo(item);
                           }
                          return false;
                        }else if(Boton.disabled===false){
                          Grilla.checked=true; 
                        }
                          
                 }
                 
                 function SearchGetData_ItemAtributo(Sufijo,Grilla)
                 {   var Boton=document.getElementById('addSav'+Sufijo);
                        if(Boton.disabled===true){
                           if(Grilla.cells[3]){
                                BarButtonState(Sufijo,"Active");
                                var item = Grilla.cells[3].childNodes[0].nodeValue; 
                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                                document.getElementById("ItemAtributo"+Sufijo).value = item;
                                document.getElementById("iditem"+Sufijo).value = item;
                                xajax_ControlItemAtributo_ItemAtributo(item);
                           }
                          return false;
                        }else if(Boton.disabled===false){
                          Grilla.checked=true; 
                        }
                          
                 }
                 
                 function SearchByElement_ItemAtributo(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {      if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_ItemAtributo(Elemento.value);
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_ItemAtributo(Elemento.value);    
                          }    
                 }
                 
                 function MuestraalGuardar_ItemAtributo(Opcion)
                 {        
                          var input = document.getElementsByTagName("input");
                          for (var i = 0; i < input.length; i++)
                          {  
                             if(input[i].id.substring(0,3)==='chk'){
                                 if(Opcion===0)
                                     input[i].disabled=true;
                                 else
                                     input[i].disabled=false;
                             }
                          }
                 }
                 
                 function XAJAXResponse_ItemAtributo(Sufijo,Mensaje,Datos)
                 {      
                        var objItemAtributo = "ItemAtributo:estado";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objItemAtributo);
                            var item = document.getElementById('ItemAtributo'+Sufijo).value;
                            document.getElementById('id'+Sufijo).value=item;
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
<body>
         <div class="FormContainer" style="width:865px">
            <div class="FormContainerSeccion">
                <div class="FormBasic" style="width:400px" id="FormDatos">
                    <div class="FormSectionMenu">  
                        
                        <?php  $ControlItemAtributo->CargaItemAtributoBarButton($_GET['opcion']);
                               echo '<script type="text/javascript">';
                               echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                               echo '</script>';
                        ?>
                   </div>    
                   <div class="FormSectionData">              
                       <form id="<?php echo 'Form'.$Sufijo; ?>"><input type="hidden" id="<?php echo 'iditem'.$Sufijo; ?>" name ="<?php echo 'iditem'.$Sufijo; ?>">
                             <?php  $ControlItemAtributo->CargaItemAtributoMantenimiento();  ?>
                        </form>
                   </div> 
                </div>               
            </div> 
            <div class="FormContainerSeccion" style="width:400px"> 
                <div class="FormSectionGrid" id="GridItemAtributoDetalle<?php echo $Sufijo; ?>">                  
                        <?php $ControlItemAtributo->CargaItemAtributoSearchGrid();  ?>
                </div>
            </div>
         </div> 
</body>
</html>
              

