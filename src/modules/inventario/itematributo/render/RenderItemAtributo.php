<?php    

        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/inventario/servicio/ServicioItem.php");
        require_once("src/rules/inventario/servicio/ServicioAtributo.php");
        
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderItemAtributo
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;   
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2;                     
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');                       
                        $Columns['Item'] = array('150px','left','');
                        $Columns['Atributo'] = array('150px','left','');
                        $Columns['idItem'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '340px','AltoRow' => '20px');
              }
               
              function CreaItemAtributoMantenimiento()
              {                          
                       $ItemAtributo = '<table class="Form-Frame" cellpadding="0">';
                       $ItemAtributo.= '       <tr height="30" style="display:none">';
                       $ItemAtributo.= '           <td class="Form-Label"  style="width:40%">Id</td>';
                       $ItemAtributo.= '             <td class="Form-Label" style="width:60%">';
                       $ItemAtributo.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $ItemAtributo.= '             </td>';
                       $ItemAtributo.= '         </tr>';                      
                       $ItemAtributo.= '         <tr height="30">';
                       $ItemAtributo.= '             <td class="Form-Label">Item</td>';
                       $ItemAtributo.= '             <td class="Form-Label">';
                                                   $ItemAtributo.= $this->CreaComboItem();
                       $ItemAtributo.= '             </td>';
                       $ItemAtributo.= '         </tr>';
                       $ItemAtributo.= '         <tr height="30">';
                       $ItemAtributo.= '             <td class="Form-Label">Atributos</td>';
                       $ItemAtributo.= '             <td class="Form-Label"><div id="SeccionAtributos'.$this->Sufijo.'"> </div></td>';
                       $ItemAtributo.= '         </tr>';
                       $ItemAtributo.= '</table>';
                       return $ItemAtributo;
              }
              
              private function SearchGridValuesItemAtributos()
              {         $Columns['Id']       = array('0px','center','none');                       
                        $Columns['Chk'] = array('50px','center','');
                        $Columns['Descripci&oacute;n'] = array('150px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '240px','AltoRow' => '20px','FindTxt' =>false);
              }
              
              function CreaAtributosporItems($item)
              {        $SearchGrid = new SearchGrid($this->Sufijo.'Detalle');
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesItemAtributos());
                       $AtributsoconItems = $this->TraeDatosItemAtributos($item);
                       $ObjHTML = $this->CreaAtributosparaGrid($SearchGrid,$AtributsoconItems);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function CreaAtributosparaGrid($ObjSchGrid,$AtributsoconItems)
              {      
                       foreach ($AtributsoconItems as $atribitems)
                       {    $ObjSchGrid->CreaSearchCellsDetalle($atribitems['id'],'',$atribitems['id']);
                            if(trim($atribitems['item'])!=0){ 
                                $ObjSchGrid->CreaSearchCellsDetalle($atribitems['id'],'','<input type="checkbox" value="'.$atribitems['id'].'" id="chk'.$atribitems['id'].'" checked name="chk'.$atribitems['id'].'">');
                            }else{
                                $ObjSchGrid->CreaSearchCellsDetalle($atribitems['id'],'','<input type="checkbox" value="'.$atribitems['id'].'" id="chk'.$atribitems['id'].'"  name="chk'.$atribitems['id'].'">');
                            }                            
                            $ObjSchGrid->CreaSearchCellsDetalle($atribitems['id'],'',trim($atribitems['atributo']));
                            $ObjSchGrid->CreaSearchRowsDetalle ($atribitems['id'],"");
                       }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                       
              }

              function CreaItemAtributoSearchGrid($Datos)
              {                               
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());                       
                       $ObjHTML = $this->ItemAtributoGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
                              
              private function CreaComboItem()
              {       
                        $datosItem= $this->TraeDatosItems(array(1));
                        $Select = new ComboBox($this->Sufijo);
                        return $Select->Combo("ItemAtributo",$datosItem)->Evento()->Selected(1)->Create("s04");
              }
                
                            
              function TraeDatosItems($Id) 
              {        
                       $ServicioItems = new ServicioItem();
                       $datoOrganigrama = $ServicioItems->BuscarItem($Id);
                       return $datoOrganigrama;
              }
              
              function TraeDatosItemAtributos($item)
              {        
                       $ServicioAtributo = new ServicioItemAtributo(); 
                       $datoAtributos = $ServicioAtributo->BuscarItemsconAtributos($item);
                       return $datoAtributos;
              }
              
              
              function MuestraItemAtributoGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->ItemAtributoGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }                  
              
              private function ItemAtributoGridHTML($ObjSchGrid,$Datos)
              {    
                  if(count($Datos)>0){ 
                     foreach ($Datos as $atritem)
                      {        $id = str_pad($atritem['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($atritem['item']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($atritem['atributo']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($atritem['iditem']));
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,"");                            
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }
               
              function MuestraItemAtributoGuardado($Ajax,$ItemAtributo)
              {       if (count($ItemAtributo)>0)
                        {   $id = str_pad($ItemAtributo[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraItemAtributoGrid($Ajax,$ItemAtributo);
                            return $this->RespuestaItemAtributo($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaItemAtributo($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraItemAtributoEditado($Ajax,$ItemAtributo)
              {       foreach ($ItemAtributo as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraItemAtributoRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaItemAtributo($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaItemAtributo($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraItemAtributoRowGrid($Ajax,$ItemAtributo,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($ItemAtributo['id'],$estado); 
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",trim($ItemAtributo['id']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($ItemAtributo['item']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($ItemAtributo['atributo']));
                        return $Ajax;
                }

              function MuestraItemAtributoExcepcion($Ajax,$Msg)
                {       return $this->RespuestaItemAtributo($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaItemAtributo($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

