<?php
         require_once("src/rules/inventario/servicio/ServicioTipoBodega.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/inventario/tipobodega/render/RenderTipoBodega.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlTipoBodega
         {     private  $Sufijo; 
               private  $ServicioTipoBodega;
               private  $RenderTipoBodega;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioTipoBodega = new ServicioTipoBodega();
                        $this->RenderTipoBodega = new RenderTipoBodega($this->Sufijo);
               }

               function CargaTipoBodegaBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaTipoBodegaMantenimiento()
               {        echo $this->RenderTipoBodega->CreaTipoBodegaMantenimiento();
               }

               function CargaTipoBodegaSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => ''); 
                        $datoTipoBodega = $this->ServicioTipoBodega->BuscarTipoBodega($prepareDQL);
                        echo $this->RenderTipoBodega->CreaTipoBodegaSearchGrid($datoTipoBodega);
               }         
               
               function MuestraTipoBodegaByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoTipoBodega = $this->ServicioTipoBodega->BuscarTipoBodega($prepareDQL);
                        return $this->RenderTipoBodega->MuestraTipoBodega($ajaxRespon,$datoTipoBodega);
               }

               function MuestraTipoBodegaByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoTipoBodega = $this->ServicioTipoBodega->BuscarTipoBodega($prepareDQL);
                        return $this->RenderTipoBodega->MuestraTipoBodegaGrid(new SearchGrid($this->Sufijo),$ajaxRespon,$datoTipoBodega);
               }

               function GuardaTipoBodega($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $TipoBodega  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioTipoBodega->GuardaDBTipoBodega($TipoBodega);
                        if (is_numeric($id)){
                            $function = (empty($TipoBodega->id) ? "MuestraTipoBodegaGuardado" : "MuestraTipoBodegaEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioTipoBodega->BuscarTipoBodega($prepareDQL);
                            return $this->RenderTipoBodega->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderTipoBodega->MuestraTipoBodegaExcepcion($ajaxRespon,intval($id));
                        } 
               }
               
               function EliminaTipoBodega($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioTipoBodega->DesactivaTipoBodega(intval($id));
                        $Datos = $this->ServicioTipoBodega->BuscarTipoBodega($prepareDQL);
                        return $this->RenderTipoBodega->MuestraTipoBodegaEliminado($ajaxRespon,$Datos);
                }
         }

?>

