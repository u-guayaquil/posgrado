<?php
         $Sufijo = '_TipoBodega';

         require_once('src/modules/inventario/tipobodega/controlador/ControlTipoBodega.php');
         $ControlTipoBodega = new ControlTipoBodega($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlTipoBodega,'GuardaTipoBodega'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlTipoBodega,'MuestraTipoBodegaByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlTipoBodega,'MuestraTipoBodegaByTX'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript"> 
                 function ButtonClick_TipoBodega(Sufijo,Operacion)
                 {        var objTipoBodega = "id:descripcion:estado";
                          var frmTipoBodega= "descripcion:estado";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,frmTipoBodega,"id:estado");
                              ElementClear(Sufijo,objTipoBodega);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,frmTipoBodega,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"descripcion"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objTipoBodega);
                                        xajax_Guarda_TipoBodega(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmTipoBodega);
                               ElementClear(Sufijo,objTipoBodega);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_TipoBodega(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_TipoBodega(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_TipoBodega(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_TipoBodega(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")) {
                                BarButtonState(Sufijo,"Active");

                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                                document.getElementById("descripcion"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;        
                                document.getElementById("estado"+Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue; 
                          }
                          return false;
                 }
                 
                 function XAJAXResponse_TipoBodega(Sufijo,Mensaje,Datos)
                 {       
                        var objTipoBodega = "descripcion:estado";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objTipoBodega);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
<body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlTipoBodega->CargaTipoBodegaBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlTipoBodega->CargaTipoBodegaMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlTipoBodega->CargaTipoBodegaSearchGrid();  ?>
              </div>  
         </div> 
        </body>
</html> 
              

