<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderTipoBodega
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2;                     
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaTipoBodegaMantenimiento()
              {        
                       $TipoBodega = '<table class="Form-Frame" cellpadding="0">';
                       $TipoBodega.= '       <tr height="30">';
                       $TipoBodega.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $TipoBodega.= '             <td class="Form-Label" style="width:75%">';
                       $TipoBodega.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $TipoBodega.= '             </td>';
                       $TipoBodega.= '         </tr>';
                       $TipoBodega.= '         <tr height="30">';
                       $TipoBodega.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $TipoBodega.= '             <td class="Form-Label" style="width:75%">';
                       $TipoBodega.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $TipoBodega.= '             </td>';
                       $TipoBodega.= '         </tr>';
                       $TipoBodega.= '         </tr>';                       
                       $TipoBodega.= '         <tr height="30">';
                       $TipoBodega.= '             <td class="Form-Label">Estado</td>';
                       $TipoBodega.= '             <td class="Form-Label">';
                                                   $TipoBodega.= $this->CreaComboEstado();
                       $TipoBodega.= '             </td>';
                       $TipoBodega.= '         </tr>';
                       $TipoBodega.= '</table>';
                       return $TipoBodega;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Descripci&oacute;n'] = array('180px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaTipoBodegaSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoBodegaGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function TipoBodegaGridHTML($ObjSchGrid,$Datos)
              {       
                  if(count($Datos)>0){
                     foreach ($Datos as $TipoBodega)
                      {        $fecreacion = $this->Fechas->changeFormatDate($TipoBodega['fecreacion'],"DMY");
                               $id = str_pad($TipoBodega['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($TipoBodega['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$TipoBodega['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$TipoBodega['idestado']); 
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($TipoBodega['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraTipoBodega($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaTipoBodega($Ajax,"NOEXISTEID","No existe un Tipo con ese ID.");
                       }
              }
              
              function MuestraTipoBodegaGrid($Ajax,$Datos)
              {                              
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoBodegaGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }   
               
              function MuestraTipoBodegaGuardado($Ajax,$TipoBodega)
              {       if (count($TipoBodega)>0)
                        {   $id = str_pad($TipoBodega[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraTipoBodegaGrid($Ajax,$TipoBodega);
                            return $this->RespuestaTipoBodega($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaTipoBodega($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraTipoBodegaEditado($Ajax,$TipoBodega)
              {       foreach ($TipoBodega as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoBodegaRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoBodega($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaTipoBodega($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraTipoBodegaEliminado($Ajax,$TipoBodega)
              {        foreach ($TipoBodega as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoBodegaRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoBodega($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaTipoBodega($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraTipoBodegaRowGrid($Ajax,$TipoBodega,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($TipoBodega['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($TipoBodega['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($TipoBodega['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($TipoBodega['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",$fecreacion[1]." ".$fecreacion[2]);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($TipoBodega['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($TipoBodega['idestado']));
                        return $Ajax;
                }

              function MuestraTipoBodegaExcepcion($Ajax,$Msg)
                {       return $this->RespuestaTipoBodega($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaTipoBodega($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

