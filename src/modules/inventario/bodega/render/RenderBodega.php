<?php
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/inventario/servicio/ServicioTipoBodega.php");
        
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");

        class RenderBodega 
        {       private $Sufijo;
                private $Maxlen;
                
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->Maxlen['id'] = 2;        
                        $this->Maxlen['codigo'] = 10;
                        $this->Maxlen['descripcion'] = 20;
                }    
                
                private function SearchGridConfig()
                {       $Columns['Id']        = array('30px','center','');
                        $Columns['Bodega']    = array('128px','left','');
                        $Columns['Sucursal']  = array('100px','left','');
                        $Columns['Tipo']      = array('100px','left','');
                        $Columns['Estado']    = array('65px','left','');
                        $Columns['Codigo']    = array('0px','center','none');
                        $Columns['IdSucursal']= array('0px','center','none');
                        $Columns['IdTipo']    = array('0px','center','none');
                        $Columns['IdEstado']  = array('0px','center','none');
                        $Columns['IdPadre']   = array('0px','center','none');
                        $Columns['TxPadre']   = array('0px','center','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '120px','AltoRow' => '20px');
                }

                function CreaBarButtonBodega($Buttons)
                {       $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaSearchGridBodega($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTMLBodega($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }

                function CreaMantenimientoBodega()
                {       $Search = new SearchInput($this->Sufijo);
                        $Opcion = '<table border=0 class="Form-Frame" cellpadding="0">';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label" style="width:20%">Id</td>';
                        $Opcion.= '           <td class="Form-Label" style="width:80%">';
                        $Opcion.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'codigo:descripcion:idsucursal:txsucursal:idpadre:txpadre\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Código</td>';
                        $Opcion.= '           <td class="Form-Label">';
                        $Opcion.= '               <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['codigo'].'" onKeyDown="return soloNumerosLetras(event); "; disabled/>';
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Descripción</td>';
                        $Opcion.= '           <td class="Form-Label">';
                        $Opcion.= '               <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Sucursal</td>';
                        $Opcion.= '           <td class="Form-Label">';
                                              $Opcion.= $Search->TextSch("sucursal","","")->Enabled(false)->Create("t06");
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';

                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Padre</td>';
                        $Opcion.= '           <td class="Form-Label">';
                                              $Opcion.= $Search->TextSch("padre","","")->Enabled(false)->Create("t06");
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';

                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Tipo</td>';
                        $Opcion.= '           <td class="Form-Label">';
                                              $Opcion.= $this->CreaComboTipoBodega();
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Estado</td>';
                        $Opcion.= '           <td class="Form-Label">';
                                              $Opcion.= $this->CreaComboEstadoByArray(array(0,1));
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        return $Opcion.'</table>';
                }

                function MuestraBodegaForm($Ajax,$Bodegas)
                {       foreach ($Bodegas as $bodega) 
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($bodega['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($bodega['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($bodega['descripcion']));
                            $Ajax->Assign("idsucursal".$this->Sufijo,"value", $bodega['idxsucursal']);
                            $Ajax->Assign("txsucursal".$this->Sufijo,"value", trim($bodega['txtsucursal']));
                            
                            $txpadre = trim($bodega['txpadre']);
                            $Ajax->Assign("idpadre".$this->Sufijo,"value", $bodega['idpadre']);
                            $Ajax->Assign("txpadre".$this->Sufijo,"value", ($txpadre=="_" ? "": $txpadre));
                            
                            $Ajax->Assign("idtipobodega".$this->Sufijo,"value", $bodega['idxtipobog']);
                            $Ajax->Assign("estado".$this->Sufijo,"value", $bodega['idxestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaBodega($Ajax,"NOEXISTEID","No existe una bodega con el ID ingresado.");
                }

                function MuestraBodegaGuardada($Ajax,$Bodega)
                {       if (count($Bodega)>0)
                        {   $id = str_pad($Bodega[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraBodegaGrid($Ajax,$Bodega);
                            return $this->RespuestaBodega($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaBodega($Ajax,"EXCEPCION","No se guardó la información.");
                }   

                function MuestraBodegaEditada($Ajax,$Bodega)
                {       foreach ($Bodega as $bodega) 
                        {       $id = str_pad($bodega['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraBodegaRowGrid($Ajax, $bodega);
                                return $this->RespuestaBodega($xAjax,"GUARDADO",$id);
                        }    
                        return $this->RespuestaBodega($Ajax,"EXCEPCION","No se actualizó la información.");
                }

                function MuestraBodegaEliminada($Ajax,$Bodega)
                {       foreach ($Bodega as $bodega)  
                        {   $xAjax = $this->MuestraBodegaRowGridDelete($Ajax,$bodega);  
                            return $this->RespuestaBodega($xAjax,"ELIMINADO",0);
                        }
                        return $this->RespuestaBodega($Ajax,"ELIMINADO",1);
                }

                function RespuestaBodega($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }

                private function MuestraBodegaRowGridDelete($Ajax,$Bodega)
                {       $Fila = $this->GetRow($Bodega['id'],$Bodega['idxestado']);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->GetCell($Fila['Id'],4),"innerHTML",trim($Bodega['txtestado'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],8),"innerHTML",$Bodega['idxestado']); 
                        return $Ajax;
                }        
                
                private function MuestraBodegaRowGrid($Ajax,$Bodega)
                {       $Fila = $this->GetRow($Bodega['id'],$Bodega['idxestado']);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->GetCell($Fila['Id'],1),"innerHTML",trim($Bodega['descripcion'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],2),"innerHTML",trim($Bodega['txtsucursal'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],3),"innerHTML",trim($Bodega['txttipobog'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],4),"innerHTML",trim($Bodega['txtestado'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],5),"innerHTML",trim($Bodega['codigo'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],6),"innerHTML",$Bodega['idxsucursal']); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],7),"innerHTML",$Bodega['idxtipobog']); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],8),"innerHTML",trim($Bodega['idxestado'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],9),"innerHTML",$Bodega['idpadre']); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],10),"innerHTML",trim($Bodega['txpadre'])); 
                        return $Ajax;
                }

                function MuestraBodegaGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTMLBodega($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLBodega($Grid,$Datos)
                {       foreach ($Datos as $bodega)
                        {       $id = str_pad($bodega['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($bodega['descripcion']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($bodega['txtsucursal']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($bodega['txttipobog']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($bodega['txtestado']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($bodega['codigo']));
                                $Grid->CreaSearchCellsDetalle($id,'',$bodega['idxsucursal']);
                                $Grid->CreaSearchCellsDetalle($id,'',$bodega['idxtipobog']);
                                $Grid->CreaSearchCellsDetalle($id,'',$bodega['idxestado']);
                                $Grid->CreaSearchCellsDetalle($id,'',$bodega['idpadre']);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($bodega['txpadre']));
                                $Grid->CreaSearchRowsDetalle ($id,($bodega['idxestado']==0 ? "red" : ""));
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
                
                private function CreaComboEstadoByArray($IdArray)
                {       $Select = new ComboBox($this->Sufijo);
                        $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $Select->Combo("estado",$Datos)->Selected(1)->Create("s04");
                }        
                
                private function CreaComboTipoBodega()
                {       $Select = new ComboBox($this->Sufijo);
                        $ServicioTipoBodega = new ServicioTipoBodega();
                        $Datos = $ServicioTipoBodega->BuscarTipoBodega(array('estado' => 1));
                        return $Select->Combo("idtipobodega",$Datos)->Selected(1)->Create("s04");
                }        
                
                private function GetRow($id,$estado)
                {       $RowId = $this->Sufijo."_".str_pad($id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $Style = "color: ".($estado==0 ? "red" : "#000");
                        $RowOb = "TR".$RowId;
                        return array('Id' => $RowId,'Name' => $RowOb,'Color' => $Style);
                }
                
                private function GetCell($RowId,$Cell)
                {       return "TD".$RowId."_".str_pad(intval($Cell),3,'0',STR_PAD_LEFT);
                }
                
                /**** Modal ****/
                private function SearchGridModalValues()
                {       $Columns['Id']                 = array('30px','center','');
                        $Columns['Descripci&oacute;n'] = array('180px','left','');
                        $Columns['Creacion']           = array('70px','center','');
                        $Columns['Estado']             = array('70px','left','');
                        $Columns['idEstado']           = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '130px','AltoRow' => '20px');
                }

                function CreaModalGridBodega($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalValues());
                        $ObjHTML = $this->BodegaGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }

                function MuestraModalGridBodega($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalValues());
                        $ObjHTML = $this->BodegaGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }

                private function BodegaGridHTML($ObjSchGrid,$Datos)
                {       $Fechas = new DateControl();
                        foreach ($Datos as $bodega)
                        {       $fecreacion = $Fechas->changeFormatDate($bodega['fecreacion'],"DMY")[1];
                                $id = str_pad($bodega['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($bodega['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$bodega['txtestado']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$bodega['idxestado']); 
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($bodega['idxestado']==0 ? "red" : ""));

                        }
                        return $ObjSchGrid->CreaSearchTableDetalle(); 
                }
                
                
    }      

?>