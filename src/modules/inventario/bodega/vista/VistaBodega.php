<?php   $Sufijo = '_bodega';

        require_once('src/modules/inventario/bodega/controlador/ControlBodega.php');
        $ControlBodega = new ControlBodega($Sufijo);
        
        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlBodega,'GuardaBodega'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlBodega,'EliminaBodega'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlBodega,'ConsultaBodegaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX'.$Sufijo, $ControlBodega,'ConsultaBodegaByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlBodega,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlBodega,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <?php $xajax->printJavascript(); 
                require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                var newbodega = "codigo:descripcion:idtipobodega:idsucursal:txsucursal:idpadre:txpadre";
                var fnsbodega = newbodega+":estado";
                var objbodega = fnsbodega+":id";
                    
                function ButtonClick_bodega(Sufijo,Operacion)
                {       var clsbodega = newbodega+":id";
                        var reqbodega = "codigo:descripcion:idsucursal:txsucursal";
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Sufijo,Operacion);
                            BarButtonStateEnabled(Sufijo,"btsucursal:btpadre");
                            ElementStatus(Sufijo,newbodega,"id");
                            ElementClear(Sufijo,clsbodega);
                            ElementSetValue(Sufijo,"estado",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Sufijo,Operacion);
                                BarButtonStateEnabled(Sufijo,"btsucursal:btpadre");
                                if (ElementGetValue(Sufijo,"estado")==0)
                                    ElementStatus(Sufijo,newbodega+":estado","id");
                                else
                                ElementStatus(Sufijo,newbodega,"id");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este regsistro?"))
                                    xajax_Elimina_bodega(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(Sufijo,reqbodega))
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    {   var Forma = PrepareElements(Sufijo,objbodega);
                                        xajax_Guarda_bodega(JSON.stringify({Forma}));
                                    }
                                }
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Sufijo,Operacion);
                                BarButtonStateDisabled(Sufijo,"btsucursal:btpadre");
                                ElementStatus(Sufijo,"id",fnsbodega);
                                ElementClear(Sufijo,clsbodega);                    
                        }
                        else 
                            xajax_CargaModal_bodega(Operacion);
                        return false;
                }
                
                function XAJAXResponse_bodega(Sufijo,Mensaje,Datos)
                {       if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btsucursal:btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",fnsbodega);
                            alert('Los datos se guardaron correctamente');
                        }
                        else if(Mensaje==='EXCEPCION')
                        {   BarButtonState(Sufijo,"addNew");
                            alert(Datos);
                        }    
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                }
                
                function SearchByElement_bodega(Sufijo,Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    xajax_BuscaByID_bodega(Elemento.value);
                                }
                            } 
                            BarButtonState(Sufijo,"Default"); //OJO
                        }
                        else if (Elemento.id === "FindSucursalTextBx"+Sufijo)
                        {    xajax_BuscaModalByTX_bodega("btsucursal",Elemento.value);
                        }
                        else if (Elemento.id === "FindPadreTextBx"+Sufijo)
                        {    xajax_BuscaModalByTX_bodega("btpadre",Elemento.value);
                        }    
                        else    
                        {    xajax_BuscaByTX_bodega(Elemento.value);
                        }    
                }
                    
                function SearchSucursalGetData_bodega(Sufijo,DatosGrid)
                {       document.getElementById("idsucursal"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txsucursal"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }

                function SearchPadreGetData_bodega(Sufijo,DatosGrid)
                {       document.getElementById("idpadre"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txpadre"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }
                
                function SearchGetData_bodega(Sufijo,Grilla)
                {       if (IsDisabled(Sufijo,"addSav"))
                        {   BarButtonState(Sufijo,"Active");
                            ElementSetValue(Sufijo,"id",Grilla.cells[0].childNodes[0].nodeValue);
                            ElementSetValue(Sufijo,"descripcion",Grilla.cells[1].childNodes[0].nodeValue);                
                            ElementSetValue(Sufijo,"txsucursal",Grilla.cells[2].childNodes[0].nodeValue);                
                            ElementSetValue(Sufijo,"codigo",Grilla.cells[5].childNodes[0].nodeValue);
                            ElementSetValue(Sufijo,"idsucursal",Grilla.cells[6].childNodes[0].nodeValue);
                            ElementSetValue(Sufijo,"idtipobodega",Grilla.cells[7].childNodes[0].nodeValue);
                            ElementSetValue(Sufijo,"estado",Grilla.cells[8].childNodes[0].nodeValue);
                            ElementSetValue(Sufijo,"idpadre",Grilla.cells[9].childNodes[0].nodeValue);
                            if (Grilla.cells[10].childNodes[0].nodeValue=='_'){
                                ElementSetValue(Sufijo,"txpadre","");
                            }else{
                                ElementSetValue(Sufijo,"txpadre",Grilla.cells[10].childNodes[0].nodeValue);
                            }    
                        }
                        return false;
                }
                
        </script>
    </head>
        <body>
        <div class="FormBasic" style="width:475px">
            <div class="FormSectionMenu">              
                <?php   $ControlBodega->CargaBodegaBarButton($_GET['opcion']);
                        echo '<script type="text/javascript">';
                        echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                        echo '</script>';
                ?>
            </div>      
            <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlBodega->CargaBodegaMantenimiento();  ?>
                </form>
            </div>    
            <div class="FormSectionGrid">          
                <?php  $ControlBodega->CargaBodegaSearchGrid();  ?>
            </div>    
        </div>
        </body>
    </html>
                
