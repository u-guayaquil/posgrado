<?php   
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/inventario/servicio/ServicioBodega.php");    
        require_once("src/rules/general/servicio/ServicioSucursal.php");  
        
        require_once("src/modules/inventario/bodega/render/RenderBodega.php");
        require_once("src/modules/general/sucursal/render/RenderSucursal.php");
        
        class ControlBodega 
        {       private  $ServicioBodega;
                private  $RenderBodega;

                function __construct($Sufijo = "")
                {       $this->ServicioBodega = new ServicioBodega();
                        $this->RenderBodega = new RenderBodega($Sufijo);
                        $this->ServicioSucursal = new ServicioSucursal();
                        $this->RenderSucursal = new RenderSucursal($Sufijo);
                }

                function CargaBodegaBarButton($Bodega)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Bodega);
                        echo $this->RenderBodega->CreaBarButtonBodega($Datos);
                }
               
                function CargaBodegaMantenimiento()
                {       echo $this->RenderBodega->CreaMantenimientoBodega();
                }

                function CargaBodegaSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioBodega->BuscarBodegaByDescripcion($prepareDQL);
                        echo $this->RenderBodega->CreaSearchGridBodega($Datos);     
                }         
                
                function ConsultaBodegaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioBodega->BuscarBodegaByID($id);
                        return $this->RenderBodega->MuestraBodegaForm($ajaxRespon,$Datos);
                }

                function ConsultaBodegaByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $texto = Trim($Texto);      
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioBodega->BuscarBodegaByDescripcion($prepareDQL);
                        return $this->RenderBodega->MuestraBodegaGrid($ajaxRespon,$Datos);
                }
                
                function GuardaBodega($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $Bodega = json_decode($Form)->Forma;
                        
                        $funcion = (empty($Bodega->id) ? "MuestraBodegaGuardada" : "MuestraBodegaEditada");
                        $id = $this->ServicioBodega->GuardaDBBodega($Bodega);
                        if (is_numeric($id)){
                            $Datos = $this->ServicioBodega->BuscarBodegaByID($id);
                            return $this->RenderBodega->{$funcion}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderBodega->RespuestaBodega($ajaxRespon,"EXCEPCION",$id);
                        }    
                }

                function EliminaBodega($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioBodega->DesactivaBodega($id);
                        $Datos = $this->ServicioBodega->BuscarBodegaByID($id);
                        return $this->RenderBodega->MuestraBodegaEliminada($ajaxRespon,$Datos);
                }

                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        if ($Operacion==="btsucursal")    
                        {   $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Sucursal";
                            $jsonModal['Carga'] = $this->RenderSucursal->CreaModalGridSucursal($Operacion,$Datos);
                            $jsonModal['Ancho'] = "603";
                        }   
                        if ($Operacion==="btpadre")    
                        {   $Datos = $this->ServicioBodega->BuscarBodegaByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Bodega";
                            $jsonModal['Carga'] = $this->RenderBodega->CreaModalGridBodega($Operacion,$Datos);
                            $jsonModal['Ancho'] = "380";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        if ($Operacion==="btsucursal")    
                        {   $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                            return $this->RenderSucursal->MuestraModalGridSucursal($ajaxResp,$Operacion,$Datos);                                        
                        }    
                        if ($Operacion==="btpadre")    
                        {   $Datos = $this->ServicioBodega->BuscarBodegaByDescripcion($prepareDQL);
                            return $this->RenderBodega->MuestraModalGridBodega($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
        }       
?>

