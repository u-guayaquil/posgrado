<?php   
    require_once('src/modules/seguridad/recovery/controlador/ControlRecovery.php');
    $ControlRecovery = new ControlRecovery();
    
    $xajax->register(XAJAX_FUNCTION,array('AutenticaUsuario', $ControlRecovery,'AutenticaUsuario'));
    $xajax->processRequest();
?>         

    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
        <title>Recuperar Contraseña</title>
 
        <link rel="apple-touch-icon" sizes="144x144" href="src/public/img/logo_ug_small.png">
        <link rel="shortcut icon" href="src/public/img/favicon.png">
        <meta name="theme-color" content="#3063A0">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600">
     
        <link rel="stylesheet" href="vendor/fontawesome/css/all.css">
        <link rel="stylesheet" href="src/libs/css/theme.min.css" data-skin="default">
        <link rel="stylesheet" href="src/libs/css/theme-dark.min.css" data-skin="dark">
        <link rel="stylesheet" href="src/libs/css/custom.css">
        <link rel="stylesheet" type="text/css" href="src/libs/css/modal.css"/>
        <style>
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('src/public/img/loader.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script>
            var skin = localStorage.getItem('skin') || 'default';
            var unusedLink = document.querySelector('link[data-skin]:not([data-skin="' + skin + '"])');
            unusedLink.setAttribute('rel', '');
            unusedLink.setAttribute('disabled', true);
        </script>
        
        <script language="javaScript" type="text/javascript">
                $(window).load(function() 
                {   $(".loader").fadeOut("slow");
                });

                function Autentica()
                {       if (ElementValidateBeforeSave("usuario"))
                        {   $('#loading').show();
                            ElementStatus("","btautentica");
                            xajax_AutenticaUsuario(xajax.getFormValues("Recovery"));
                            return false;
                        }        
                }
                    
                function Ejecuta(CMD)
                {       parent.location.href = CMD;
                        return false;
                }

                function cerrarmodal()
                {   document.getElementById('loading').style.display = 'none';
                    cerrar();      
                }
        </script>        
        <script type="text/javascript" src="src/libs/js/elements.js"></script>
        <script type="text/javascript" src="src/libs/js/modal.js"></script>
        <?php   $xajax->printJavascript(); ?>
    
    </head>
    <body>  
        <main class="auth">
            <form class="auth-form auth-form-reflow" id="Recovery" method="POST">
              <div class="text-center mb-4">
                <div class="mb-4">
                  <img class="rounded" src="src/public/img/logo_ug_small.png" alt="" height="150">
                </div>
                <h1 class="h3"> Recuperar Contraseña </h1>
              </div>
              <div class="loader" id="loading"></div> 
                    <?php   $ControlRecovery->CargaRecovery(); ?>
                    <p class="mb-0 px-3 text-muted text-center">© 2019 Todos los Derechos Reservados Universidad de Guayaquil.</p>
            </form>
            <!--<footer class="auth-footer mt-5">© 2019 Todos los Derechos Reservados Universidad de Guayaquil.</footer>-->
        </main>
    
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/popper.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/particles.js/particles.min.js"></script>
        
        <script>
            $(document).on('theme:init', () =>
            {  /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
               particlesJS.load('announcement', 'src/libs/pages/particles.json');
            })
        </script>
        <script src="src/libs/js/theme.min.js"></script> 
    </body>
    </html>