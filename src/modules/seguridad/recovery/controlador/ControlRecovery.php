<?php
    
    require_once("src/rules/entorno/servicio/ServicioConfig.php"); 
    require_once("src/rules/seguridad/servicio/ServicioRecovery.php");
    require_once("src/modules/seguridad/recovery/render/RenderRecovery.php");

    class ControlRecovery
    {       private $ServicioRecovery;
            private $RenderRecovery;
            
            function __construct() 
            {       $this->ServicioConfig = new  ServicioConfig();
                    $this->ServicioRecovery = new ServicioRecovery();
                    $this->RenderRecovery =  new RenderRecovery();
            }  
            
            function CargaRecovery()
            {       $Datos = $this->ServicioRecovery->CargaRolesModulo();
                        if ($Datos[0])    
                            echo $this->RenderRecovery->CargaCuerpoLogin($Datos[1]);
                        else
                        echo $Datos[1];
            }
             
            function AutenticaUsuario($Form)
            {       $xAjax = new xajaxResponse();
                    $Datos = $this->ServicioRecovery->AutenticaUsuarioSistema($Form);
                    $xAjax->call("ElementStatus","btautentica","");
                    if ($Datos[0])
                    {   $xAjax->call("Ejecuta",$Datos[1]);
                    }
                    else 
                    $xAjax = $this->CargaModalGrid($xAjax,$Datos[1]);
                    return $xAjax;
            }
            
            function CargaModalGrid($xResp,$Datos)
            {       $jsonModal['Modal'] = "ModalMensaje"; 
                    $jsonModal['Title'] = "Mensaje";
                    $jsonModal['Carga'] = $this->RenderRecovery->MsgBoxP($Datos);
                    $jsonModal['Ancho'] = "350";
                    $xResp->call("CreaModalP", json_encode($jsonModal));
                    return $xResp; 
            }
    }

?>

