<?php

    require_once('src/libs/clases/ComboBox.php');
   
    class RenderRecovery
    {       private $Select;
            
            function __construct() 
            {       $this->Select = new ComboBox();
            }

            function CargaCuerpoLogin($Datos)
            {      //$login = '<p class="text-left mb-4"> Dont have a account? <a href="auth-signup.html">Create One</a></p>';
                    $login = '<p class="text-left mb-4">Por favor, ingrese su cédula y seleccione su rol de usuario.</p>';
                    $login.= '<div class="form-group mb-4">';
                    $login.= '    <input type="text" placeholder="Usuario" id="usuario"  name="usuario" class="form-control form-control-lg" required="" maxlength="30" value="" >';
                    $login.= '</div>';
                    $login.= '<div class="form-group mb-4">';
                    $login.=       $this->Select->Combo('rol',$Datos)->Enabled(true)->Fields("ID","DESCRIPCION")->Create("form-control form-control-lg");
                    $login.= '</div>';
                    $login.= '<div class="form-group mb-4">';
                    $login.= '     <button id="btautentica" class="btn btn-lg btn-primary btn-block" onClick=" return Autentica();">Aceptar</button>';
                    $login.= '</div>';
                    $login.= '<div class=" form-group mb-4">';
                    $login.= '<a href="CMD_LOGIN" class="btn btn-block btn-light">Volver al Inicio</a>';
                    $login.= '</div>';
                    $login.= '<p class="text-muted">';
                    $login.= '<small>Le enviaremos un enlace para restablecer la contraseña a su correo electrónico.</small>';
                    $login.= '</p>';
                    return $login;
            }
            
            function MsgBox($Datos)
            {       $HTML = '<table class="Form-Frame" cellpadding="10" border="0" width="100%">';
                    $HTML.= '      <tr height="100px">';
                    $HTML.= '          <td class="Form-Label-Center">';
                    $HTML.= $Datos;
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function MsgBoxP($Datos)
            {       $HTML = '<table class="Form-Frame" cellpadding="10" border="0" width="100%">';
                    $HTML.= '      <tr height="100px">';
                    $HTML.= '          <td class="Form-Label-Center">';
                    $HTML.= $Datos;
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';
                    $HTML.= '      <tr height="100px">';
                    $HTML.= '          <td class="Form-Label-Center" align="center">';
                    $HTML.= '               <button type="button" class="btn btn-success btn-lg" onclick="cerrarmodal();">OK</button>';
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function MsgBoxCerrar()
            {       $HTML = '<table class="Form-Frame" cellpadding="10" border="0" width="100%">';
                    $HTML.= '      <tr height="100px">';
                    $HTML.= '          <td class="Form-Label-Center">';
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }
            
            function MensajeCorreo($CMD,$Recovery)
            {       if ($CMD=="MAIL_RECOVERY_CLAVE")
                        return $this->MensajeRecuperacionClave($Recovery);;
            }        
           
            function MensajeRecuperacionClave($Recovery)
            {       $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                            <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <b>Estimado(a)<br>".$Recovery['nombre']."</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    Usted acaba de solicitar con exito recuperar la contrase&ntilde;a de acceso al sistema
                                    de Posgrado. A continuaci&oacute;n ponemos en su conocimiento lo siguiente:
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <br>
                                    <b>Usuario: </b>".$Recovery['usuario']."<br>
                                    <b>Nuevo Password: </b>".$Recovery['password']."<br>
                                     <p><b>Para poder activar su nueva clave por favor de click en el siguiente enlace:</b></p>
                                    <b>Enlace:</b><a href='https://www.posgrados.ug.edu.ec/CMD_ACTIVACION_PWD?token=".$Recovery['token']."' target='_blank'>https://www.posgrados.ug.edu.ec/</a>
                                    <p><b>Este enlace consta de una duraci&oacute;n de 24 horas.</b></p>
                                    <p><b>Si usted no ingres&oacute; al enlace dentro de las 24 horas deber&aacute; solicitar una nueva clave.</b></p>
                                    <br><br> 
                                    Archive este correo en un lugar seguro. Le recordamos la responsabilidad que usted adquiere al otorgarle el acceso respectivo.
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <br><b>Saludos</b>
                                    <br>Departamento de Posgrado
                                    <br>Universidad de Guayaquil
                                    <br><br><br>
                                    Favor no responda a este correo. Cualquier inquietud pongase en contacto con la Unidad de Posgrado de la Universidad de Guayaquil.
                                </td>
                            </tr>
                            </table>";
                            return $mnj;                 
            }       
    }

?>