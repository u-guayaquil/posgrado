<?php

    require_once('src/libs/clases/ComboBox.php');
   
    class RenderLogin
    {       private $Select;
            private $Utils;
            
            function __construct() 
            {       $this->Select = new ComboBox();
                    $this->Utils = New Utils();       
            }

            function CargaCuerpoLogin($Datos)
            {     //$login = '<p class="text-left mb-4"> Dont have a account? <a href="auth-signup.html">Create One</a></p>';
                    $login = '<div class="form-group mb-4">';
                    $login.= '    <input type="text" placeholder="Usuario" id="usuario"  name="usuario" class="form-control form-control-lg" required="" maxlength="30" value="" >';
                    $login.= '</div>';
                    $login.= '<div class="form-group mb-4">';
                    $login.= '    <input type="password" placeholder="Password" id="password" name="password" class="form-control form-control-lg" required="" maxlength="15" value="">';
                    $login.= '</div>';
                    $login.= '<div class="form-group mb-4">';
                    $login.=       $this->Select->Combo('rol',$Datos)->Enabled(true)->Fields("ID","DESCRIPCION")->Create("form-control form-control-lg");
                    $login.= '</div>';  
                    $login.= '<div class="form-group mb-4">';
                    $login.= '     <button id="btautentica" class="btn btn-lg btn-primary btn-block" onClick=" return Autentica();">Aceptar</button>';
                    $login.= '</div>';
                    $login.= '<div id="loginmsg" class="form-group text-center">';
                   //$login.= '     <div class="custom-control custom-control-inline custom-checkbox">';
                   //$login.= '          <input type="checkbox" class="custom-control-input" id="remember-me">'; 
                   //$login.= '          <label class="custom-control-label" for="remember-me">Keep me sign in</label>';
                   //$login.= '     </div>';
                    $login.= '</div>';
                    $login.= '<p class="py-2">';
                    $login.= '<a href="CMD_RECOVERY" class="link" onClick="">Recuperar Usuario y Contraseña</a>'; //<!--span class="mx-2">kerber_auth(this.href)</span-->
                    $login.= '</p>';
                    //$login.= '<div class="form-group mb-4">';
                    //$login.= '      <a href="https://forms.gle/hHYLtETDzN5XcV6h8" target="_blank" id="btDataAulaVirtual" class="btn btn-danger btn-block">Actualiza tus datos para acceder al aula virtual</a>';
                    //$login.= '</div>';
                    return $login; 
            }
            
            function MsgBox($Datos)
            {       $HTML = '<table class="Form-Frame" cellpadding="10" border="0" width="100%">';
                    $HTML.= '      <tr height="100px">';
                    $HTML.= '          <td class="Form-Label-Center">';
                    $HTML.= $Datos;
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function MsgBoxP($Datos)
            {       $HTML = '<table class="Form-Frame" cellpadding="10" border="0" width="100%">';
                    $HTML.= '      <tr height="100px">';
                    $HTML.= '          <td class="Form-Label-Center">';
                    $HTML.= $Datos;
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';
                    $HTML.= '      <tr height="100px">';
                    $HTML.= '          <td class="Form-Label-Center" align="center">';
                    $HTML.= '               <button type="button" class="btn btn-success btn-lg" onclick="cerrarmodal();">OK</button>';
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function MsgBoxCerrar()
            {       $HTML = '<table class="Form-Frame" cellpadding="10" border="0" width="100%">';
                    $HTML.= '      <tr height="100px">';
                    $HTML.= '          <td class="Form-Label-Center">';
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }
            
            function CargaCuerpoLoginUpdate($Politica)
            {       $Sesion = json_decode(Session::getValue("Sesion"));
                 
                    $login = '<table border="0" cellspacing="0" cellpadding="4" width="350px">';
                    $login.=    '<tr>';
                    $login.=        '<td style="text-align:left" colspan="2">';
                    $login.=        '<input type="text" style="width:295px" placeholder="Correo" required="" id="correo"  name="correo"  value="'.$Sesion->Correo.'" maxlength="80" readonly/>';
                    $login.=        '</td>';
                    $login.=    '</tr>';
                    $login.=    '<tr>';
                    $login.=        '<td style="text-align:left">';
                    $login.=        '<input type="text" placeholder="Palabra clave" required="" id="clave" name="clave" value="" maxlength="10"/>';
                    $login.=        '</td>';
                    $login.=        '<td rowspan="4" style="text-align:left" valign="top">';
                    $login.=             '<b>Contraseña<br>Segura:</b><br><br>'.$Politica;
                    $login.=        '</td>';
                    $login.=    '</tr>';
                    $login.=    '<tr>';
                    $login.=        '<td style="text-align:left">';
                    $login.=        '<input type="password" placeholder="Actual contraseña" required="" id="password" name="password" value="" maxlength="15"/>';
                    $login.=        '</td>';
                    $login.=    '</tr>';
                    $login.=    '<tr>';
                    $login.=        '<td style="text-align:left">';
                    $login.=        '<input type="password" placeholder="Nueva contraseña" required="" id="npassword" name="npassword" value="" maxlength="15"/>';
                    $login.=        '</td>';
                    $login.=    '</tr>';
                    $login.=    '<tr>';
                    $login.=        '<td style="text-align:left">';
                    $login.=        '<input type="password" placeholder="Confirmar contraseña" required="" id="cpassword" name="cpassword" value="" maxlength="15"/>';
                    $login.=        '</td>';
                    $login.=    '</tr>';
                    $login.=    '<tr>';
                    $login.=        '<td colspan="2">';
                    $login.=        '<input type="button" id= "btautentica" value="Cambiar" onClick=" return Autentica();"/>';
                    $login.=        '</td>';
                    $login.=    '</tr>';
                    $login.= '</table>';
                    return $login; 
                
            }
            
            function MensajeCorreo($CMD,$Login)
            {       if ($CMD=="MAIL_CREA_CUENTA")
                    {   if ($Login['rol']==1 || $Login['rol']==2)
                            return $this->MensajeCorreoConfirmado($Login);
                        else 
                            return $this->MensajeCorreoPorConfirmar($Login);    
                    }
                    else if ($CMD=="MAIL_MODF_CUENTA")
                            return $this->MensajeCorreoActualiza($Login);
                    else if ($CMD=="MAIL_CREA_CUENTA_COORD")
                            return $this->MensajeCorreoCoordinador($Login);
            }        
            
            function MensajeCorreoCoordinador($Login)
            {       $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                            <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <b>Bienvenido<br>".utf8_decode($this->Utils->ConvertTildesToUpper($Login['nombrecompleto']))."</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    Para que usted pueda acceder al sistema, le solicitamos completar el proceso de registro 
                                    accediendo con la contrase&ntilde;a provicional que se encuentra en la parte inferior de este correo.
                                    <br><br>
                                    Tiene un plazo de 72 horas para completar esta tarea, posterior a esto, su solicitud
                                    caducar&aacute; y deber&aacute; solicitar una nueva contactandose con personal de la Unidad de Posgrado.
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                <br>
                                    <b>Usuario: </b>".$Login['usuario']."<br>
                                    <b>Password: </b>".$Login['password']."<br>
                                    <b>Enlace:</b><a href='https://www.posgrados.ug.edu.ec/' target='_blank'>Enlace Web Posgrado</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <br><b>Saludos</b>
                                    <br>Departamento de Posgrado
                                    <br>Universidad de Guayaquil
                                    <br><br>
                                    Por favor no responda a esta direcci&oacute;n de correo.
                                </td>
                            </tr>
                            </table>";
                            return $mnj;                 
            }
            
            function MensajeCorreoActualiza($Login)
            {       $Sesion = json_decode(Session::getValue("Sesion"));
                    $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                            <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <b>Estimado(a)<br>".utf8_decode($this->Utils->ConvertTildesToUpper($Sesion->Nombre))."</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    Usted acaba de modificar con exito informaci&oacute;n relacionada a su cuenta y acceso al sistema
                                    de Posgrado. A continuaci&oacute;n ponemos en su conocimiento lo siguiente:
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <br>
                                    <b>Usuario: </b>".$Login['usuario']."<br>
                                    <b>Password: </b>".$Login['npassword']."<br>
                                    <b>Palabra clave: </b>".$Login['clave']."<br>
                                    <b>Enlace:</b><a href='https://www.posgrados.ug.edu.ec/' target='_blank'>Enlace Web Posgrado</a>
                                    <br><br>
                                    Es importante recordarle que la Palabra clave le ser&aacute; de utilidad cuando desee recuperar su contrase&ntilde;a. 
                                    Archive este correo en un lugar seguro. Le recordamos la responsabilidad que usted adquiere al otorgarle el acceso respectivo.
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <br><b>Saludos</b>
                                    <br>Departamento de Posgrado
                                    <br>Universidad de Guayaquil
                                    <br><br><br>
                                    Favor no responda a este correo. Cualquier inquietud pongase en contacto con la Unidad de Posgrado de la Universidad de Guayaquil.
                                </td>
                            </tr>
                            </table>";
                            return $mnj;                 
            }
            
            function MensajeCorreoConfirmado($Login)
            {       $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                            <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <b>Bienvenido<br>".utf8_decode($this->Utils->ConvertTildesToUpper($Login['nombre']))."</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    Para que usted pueda acceder al sistema, le solicitamos completar el proceso de registro 
                                    accediendo con la contrase&ntilde;a provicional que se encuentra en la parte inferior de este correo.
                                    <br><br>
                                    Tiene un plazo de 72 horas para completar esta tarea, posterior a esto, su solicitud
                                    caducar&aacute; y deber&aacute; solicitar una nueva contactandose con personal de la Unidad de Posgrado.
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                <br>
                                    <b>Usuario: </b>".$Login['usuario']."<br>
                                    <b>Password: </b>".$Login['password']."<br>
                                    <b>Enlace:</b><a href='https://www.posgrados.ug.edu.ec/' target='_blank'>Enlace Web Posgrado</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <br><b>Saludos</b>
                                    <br>Departamento de Posgrado
                                    <br>Universidad de Guayaquil
                                    <br><br>
                                    Por favor no responda a esta direcci&oacute;n de correo.
                                </td>
                            </tr>
                            </table>";
                            return $mnj;                 
            }
            
            function MensajeCorreoPorConfirmar($Login)
            {       $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                            <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    <b>Bienvenido<br>".utf8_decode($this->Utils->ConvertTildesToUpper($Login['nombre']))."</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                    Para que usted pueda acceder a la informaci&oacute;n, se requiere de un proceso de verificaci&oacute;n. 
                                    <br><br>
                                    En un plazo no mayor a 24 horas se enviar&aacute;, a su cuenta de correo con la cuenta provisional
                                    con la cual podr&aacute; acceder a las funciones y servicios que ofrece la Unidad de Posgrado de la
                                    Universidad de Guayaquil.
                                </td>
                            </tr>
                            <tr>
                            <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                <br><b>Saludos</b>
                                <br>Departamento de Posgrado
                                <br>Universidad de Guayaquil
                                <br><br>
                                Por favor no responda a esta direcci&oacute;n de correo.
                                </td>
                            </tr>
                            </table>";
                            return $mnj;
            }
    }

?>