<?php
    
  //require_once("src/rules/entorno/servicio/ServicioConfig.php"); 
    require_once("src/rules/seguridad/servicio/ServicioLogin.php");
    require_once("src/modules/seguridad/login/render/RenderLogin.php");

    class ControlLoginUpdate
    {       private $ServicioLogin;
            private $RenderLogin;
            
            function __construct() 
            {       $this->ServicioLogin = new ServicioLogin();
                    $this->RenderLogin =  new RenderLogin();
            }  
            
            function CargaLogin()
            {       $Politica = $this->ServicioLogin->ObtenerPoliticasContrasena();
                    echo $this->RenderLogin->CargaCuerpoLoginUpdate($Politica);
            }
             
            function CambiaUsuario($Form)
            {       $xAjax = new xajaxResponse();
                    $Datos = $this->ServicioLogin->CambiaUsuarioSistema($Form);
                    $xAjax->call("ElementStatus","btautentica","");
                    if ($Datos[0])
                    {   $xAjax->Call("ElementClear","clave:password:npassword:cpassword");
                    }
                    $xAjax = $this->CargaModalGrid($xAjax,$Datos[1]);
                    return $xAjax;
            }
            
            function CargaModalGrid($xResp,$Datos)
            {       $jsonModal['Modal'] = "ModalMensaje"; 
                    $jsonModal['Title'] = "Mensaje";
                    $jsonModal['Carga'] = $this->RenderLogin->MsgBox($Datos);
                    $jsonModal['Ancho'] = "350";
                    $xResp->call("CreaModal", json_encode($jsonModal));
                    return $xResp; 
            }
    }

?>

