<?php
    
    require_once("src/rules/entorno/servicio/ServicioConfig.php"); 
    require_once("src/rules/seguridad/servicio/ServicioLogin.php");
    require_once("src/modules/seguridad/login/render/RenderLogin.php");

    class ControlLogin
    {       private $ServicioLogin;
            private $RenderLogin;
            
            function __construct($Modulo,$Entorno) 
            {       $this->ServicioConfig = new  ServicioConfig($Modulo,$Entorno);
                    $this->ServicioLogin = new ServicioLogin();
                    $this->RenderLogin =  new RenderLogin();
            }  
            
            function CargaLogin()
            {       $Resp = $this->ServicioConfig->EstadoServicio();
                    if ($Resp[0])
                    {   $Datos = $this->ServicioLogin->CargaRolesModulo();
                        if ($Datos[0])    
                            echo $this->RenderLogin->CargaCuerpoLogin($Datos[1]);
                        else
                        echo $Datos[1]; 
                    }
                    else 
                    echo $Resp[1]; 
                  
            }
             
            function AutenticaUsuario($Form)
            {       $xAjax = new xajaxResponse();
                    $Datos = $this->ServicioLogin->AutenticaUsuarioSistema($Form);
                    $xAjax->call("ElementStatus","btautentica","");
                    if ($Datos[0])
                    {   $xAjax->call("Ejecuta",$Datos[1]);
                    }
                    else 
                    $xAjax = $this->CargaModalGrid($xAjax,$Datos[1]);
                    return $xAjax;
            }
            
            function CargaModalGrid($xResp,$Datos)
            {       $jsonModal['Modal'] = "ModalMensaje"; 
                    $jsonModal['Title'] = "Mensaje";
                    $jsonModal['Carga'] = $this->RenderLogin->MsgBoxP($Datos);
                    $jsonModal['Ancho'] = "350";
                    $xResp->call("CreaModalP", json_encode($jsonModal));
                    return $xResp; 
            }
    }

?>

