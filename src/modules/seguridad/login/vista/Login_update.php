<?php   require_once('src/modules/seguridad/login/controlador/ControlLoginUpdate.php');
        $ControlLoginUpdate = new ControlLoginUpdate();
    
        $xajax->register(XAJAX_FUNCTION,array('CambiaUsuario', $ControlLoginUpdate,'CambiaUsuario'));
        $xajax->processRequest();
?>        

        <!doctype html>
        <html xmlns:svg="http://www.w3.org/2000/svg">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <title></title>
                <script type="text/javascript" src="src/libs/js/elements.js"></script>
                <script type="text/javascript" src="src/libs/js/modal.js"></script>
                
                <script language="javaScript" type="text/javascript">
                        function Autentica()
                            {   if (ElementValidateBeforeSave("correo:clave:password:npassword:cpassword"))
                                    {   ElementStatus("","btautentica");
                                        xajax_CambiaUsuario(xajax.getFormValues("Login"));
                                    }
                                else
                                    alert("Debe ingresar toda la información solicitada en el formulario.");
                                return false;
                            }
                </script>
                <link rel="stylesheet" type="text/css" href="src/libs/css/modal.css"/>
                <link rel="stylesheet" type="text/css" href="src/modules/seguridad/login/libs/css/login.css"/>
                
                <?php   $xajax->printJavascript();  ?>
                </head>
        <body>
                <div class="container">
                    <section id="content">
                    <form id="Login" method="POST">
                    <?php  $ControlLoginUpdate->CargaLogin(); ?>  
                    </form>
                    </section>
                </div>
        </body>
        </html>

