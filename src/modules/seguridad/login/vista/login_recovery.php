<?php      require ('formularios/xajax_core/xajax.inc.php');
           $xajax = new xajax();
           $xajax->setCharEncoding('ISO-8859-1');
           $xajax->configure('decodeUTF8Input',true);

           include("adodb5/adodb.inc.php");

           function decripServerName($serverNm)
  	   {        $hasta=strlen($serverNm)/3;
                    $k=0;
                    $decripta="";
                    for($i=0;$i<=$hasta-1;$i++)
                    {   $decripta=$decripta.chr((intval(substr($serverNm,$k,3))/7)+17);
                        $k=$k+3;
                    }
        	    return $decripta;
  	   }

           function busca_cedula($CedulaValue,$IdSignal,$IdRol)
           {        $IdSignal1=decripServerName($IdSignal);
                    $mylocalIP =ADONewConnection('odbc_mssql');
                    $mylocalIP->Connect("Driver={SQL Server};Server=".$IdSignal1.";Database=solulegal;","","");

                    $respuesta=new xajaxResponse();
                    $respuesta->setCharacterEncoding('ISO-8859-1');

                    $sesMensaj="";
                    $sesTpEnti=$mylocalIP->Execute("select Tabla from BRA_TIPOENTIDAD where id<>0 and Id='$IdRol'");
                    While (!$sesTpEnti->EOF)
                    {     $I=0;
                          $TB=trim($sesTpEnti->fields[0]);
                          $sesEntida=$mylocalIP->Execute("select IdUsuario,Estado,Id from ".$TB." Where NumID='$CedulaValue'");
                          While (!$sesEntida->EOF)
                          {     $I=1;
                                $IE=$sesEntida->fields[2];
                                if (trim($sesEntida->fields[1])=="A")
                                {  if ($sesEntida->fields[0]!=0)
                                   {  $Id=$sesEntida->fields[0];
                                      $sesUsuari=$mylocalIP->Execute("Select Estado,DATEDIFF(day,GETDATE(),Caducidad),ERespuesta From BRA_USUARIO Where Id='$Id'");
                                      While (!$sesUsuari->EOF)
                                      {     if (trim($sesUsuari->fields[0])=="A")
                                            {  $respuesta->Assign("Mail","value",trim($sesUsuari->fields[2]));
                                               $respuesta->Assign("IdUs","value",str_pad($Id,4,"0",STR_PAD_LEFT));
                                            }
                                            else
                                            {  if (trim($sesUsuari->fields[0])=="C")
                                               {  if ($sesUsuari->fields[1]>0)
                                                     $sesMensaj="Su cuenta est� pendiente de confirmaci�n. Consulte su correo.";
                                                  else
                                                  {  $sesMensaj="Su cuenta ha caducado: comun�quese con el administrador.";
                                                     $mylocalIP->Execute("Update ".$TB." Set IdUsuario=0 Where Id='$IE'");
                                                     $mylocalIP->Execute("Delete From BRA_USUARIO Where Id='$Id'");
                                                  }
                                               }
                                               else
                                               $sesMensaj="Su cuenta est� inactiva: comun�quese con el administrador.";
                                            }
                                            $sesUsuari->MoveNext();
                                      }
                                      $sesUsuari->close();
                                   }
                                   else
                                   $sesMensaj="Usted no dispone de una cuenta de usuario: comun�quese con el administrador.";
                                }
                                else
                                $sesMensaj="Su registro est� inactivo: comun�quese con el administrador.";
                                $sesEntida->MoveNext();
                          }
                          $sesEntida->close();
                          $sesTpEnti->MoveNext();
                    }
                    $sesTpEnti->close();

                    if ($I==0) $sesMensaj="No existe el identificador ingresado.";

                    $respuesta->Assign("IdRespuesta","value","");
                    if (trim($sesMensaj)!="")
                    {  $respuesta->alert($sesMensaj);
                       $respuesta->Assign("Mail","value","");
                       $respuesta->Assign("IdUs","value","");
                    }

                    $respuesta->Assign("AddSegur","disabled",false);
                    $mylocalIP->close();
                    return $respuesta;
           }

           $xajax->register(XAJAX_FUNCTION,'busca_cedula');
           $xajax->processRequest();
           include("adodb5/conexion.inc.php");
           include("formularios/funciones/cb_selects.php");
           $mylocalP=initDBC(0);
           
           $figf=str_pad(rand(1,50),4,"0",STR_PAD_LEFT);
           $fidf="C".$figf;

?>
<!doctype html>
<html xmlns:svg="http://www.w3.org/2000/svg">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Seguridad</title>
<?php
       $strconex1 = new ParamConexion;
       echo '<script language="javaScript" type="text/javascript"> var IdSignal="'.$strconex1->encripServerName().'"; </script>';
       $xajax->printJavascript("formularios/");
?>
<script type="text/javascript" src="js/validador.js"></script>
<script type="text/javascript" src="js/myajax.js"></script>
<script language="javaScript" type="text/javascript">
        var rqObPas="IdUs:NumID:Mail";

        function BtOperator(IdMain,opBoton,Forma,opOb)
        {
                 var MyId=document.getElementById(IdMain);
                 MyId.value=quita_espacios(MyId.value);
                 if (opBoton=="G")
                 {  if (validateSave(rqObPas)==true)
                    {  Progress_Bar("visible","Espere...");
                       Boton_Evento("","AddSegur");
                       ExecuteAjax("login_session_recovery.php","POST",RecuperaSesion,"IdUs:NumID:Mail:IdPregunta:IdRespuesta:"+opOb);
                    }
                    else
                    alert("Falta ingresar informaci�n.");
                 }
                 if (opBoton=="X")
                 {  var parentObject = parent.document.getElementById("RecovLogin");
                    parentObject.style.visibility="hidden";
                 }
                 return false;
        }
	    function RecuperaSesion()
        {        capa=document.getElementById("xajax_msg");
                 if(PeticionHTTP.readyState==READY_STATE_LOADING)
			     { }
                 else if(PeticionHTTP.readyState==READY_STATE_COMPLETE)
                 {    var Error=0;
                      Progress_Bar("hidden","");
                      if(PeticionHTTP.status==200)
                      {  var resp=PeticionHTTP.responseText;
                         if (resp.trim()=='S')
                         {  alert("Se ha enviado un mensaje a su correo de respaldo.");
                            var parentObject = parent.document.getElementById("RecovLogin");
                            parentObject.style.visibility="hidden";
                         }
                         else
                         {  Error=1;
                            capa.innerHTML=PeticionHTTP.responseText;    }
                      }
                      else if(PeticionHTTP.status==404)
			  	      {    Error=1;
                           capa.innerHTML= "No se encuentra el objeto."; }
    			      else
				      {    Error=1;
                           capa.innerHTML=PeticionHTTP.status;           }
                      if (Error==1) Boton_Evento("AddSegur","");
                 }
        }
</script>
<link rel="stylesheet" type="text/css" href="css/formulario.css"/>
<link rel="stylesheet" type="text/css" href="css/elemento.css"/>
<link rel="stylesheet" type="text/css" href="css/barrabtn.css"/>
</head>
<body style="background-color:transparent">
<div id="contact-form-free" style="padding:10px; width:475px;">
     <h1>�Olvidaste tu Cuenta?</h1>
     <form method="POST" id="Form_Logrecup">
     <table cellPadding="2" cellSpacing="0" border="0">
            <tr>
                <td colspan="4">
                    <p class='barra_spaces'>
                    <span><input type="hidden" id="IdUs" name="IdUs" value=""/></span>
                    <span><input Title="Aceptar" class="barra_botton" id="AddSegur" name="AddSegur" type="button" onclick="return BtOperator('IdUs','G','Form_Logrecup','<?php echo $fidf ?>');" onMouseOver=" return OnBoton(this);" onMouseOut=" return OuBoton(this);"/></span>
                    <span><input Title="Cerrar"  class="barra_botton" id="AddClose" name="AddClose" type="button" onclick="return BtOperator('IdUs','X','Form_Logrecup','<?php echo $fidf ?>');" onMouseOver=" return OnBoton(this);" onMouseOut=" return OuBoton(this);"/></span>
                    </p>
                </td>
            </tr>
            <tr>
                <td class="coletiqueta">Tu Rol</td>
                <td colspan="3">
                    <select style="width:110px" id="IdRol" name="IdRol"/>
                    <?php   select_roltipoentidad($mylocalP);          ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="coletiqueta">CI /RUC/ PASS</td>
                <td colspan="3">
                    <input type="text" id="NumID" name="NumID" value="" class="CodigTexto05" maxlength="15" onKeyDown="return depCodigos(event,'Mail:IdRespuesta:IdUs');" onblur="return BscCedulaRucPass_1(NumID,IdRol);"/>
                </td>
            </tr>
            <tr>
                <td class="coletiqueta">Email. Resp</td>
                <td colspan="3">
                    <input type="text" id="Mail" name="Mail" value="" placeholder="micuenta@dominio.com" maxlength="100" style="width:319px" onblur=" return BscEmail(Mail);" readonly/>
                </td>
            </tr>
            <tr>
                <td class="coletiqueta">Pregunta</td>
                <td colspan="3">
                    <select style="width:350px" id="IdPregunta" name="IdPregunta"/>
                    <?php   select_preguntas($mylocalP);                         ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="coletiqueta">Respuesta</td>
                <td width="130">
                    <input type="text" id="IdRespuesta" name="IdRespuesta" value="" maxlength="100" style="width:120px"/>
                </td>
                <td align="right">
                    <input type="text" id="<?php echo $fidf ?>" name="<?php echo $fidf ?>" placeholder="Codigo" value="" maxlength="4" style="width:66px"/>
                </td>
                <td>
                    <img border="1" style="border-color: #777777" src="images/iveryfy/<?php echo $figf.".gif";?>" height="22px" width="66px">
                </td>
            </tr>
            
            <tr><td height="20" colspan="4" id="xajax_msg"></td></tr>
            <tr>
                <td Id="CaptionBar" class="coletiqueta" align="right"></td>
                <td colspan="3"><img Id="ProgressBar" style="visibility:hidden" src="formularios/images/ajax_wait_4.gif" width="220" height="19" border="0"></td>
            </tr>
     </table>
     </form>
</div>
</body>
</html>
