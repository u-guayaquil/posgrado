<?php   
    require_once('src/modules/seguridad/login/controlador/ControlLogin.php');
    $ControlLogin = new ControlLogin('POSGRADO','PRODUCCION');
    
    $xajax->register(XAJAX_FUNCTION,array('AutenticaUsuario', $ControlLogin,'AutenticaUsuario'));
    $xajax->processRequest();
?>         

    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
        <title>Login</title>
 
        <link rel="apple-touch-icon" sizes="144x144" href="src/public/img/logo_ug_small.png">
        <link rel="shortcut icon" href="src/public/img/favicon.png">
        <meta name="theme-color" content="#3063A0">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600">
     
        <link rel="stylesheet" href="vendor/fontawesome/css/all.css">
        <link rel="stylesheet" href="src/libs/css/theme.min.css" data-skin="default">
        <link rel="stylesheet" href="src/libs/css/theme-dark.min.css" data-skin="dark">
        <link rel="stylesheet" href="src/libs/css/custom.css">
        <link rel="stylesheet" type="text/css" href="src/libs/css/modal.css"/>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">

        <style>
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('src/public/img/loader.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }

            .btn-primary {
                color: #fff !important;
                background-color: #346cb0 !important;
                border-color: #2b5a92 !important;
            }
       
            .h3{
                font-size: 2.75rem !important;
                margin-bottom: .5rem !important;
                font-weight: 600 !important;
                line-height: 1.2 !important;
                font-family: -apple-system,BlinkMacSystemFont,Fira Sans,Helvetica Neue,Apple Color Emoji,sans-serif !important;
            }

            span{
                cursor: pointer;
            }

            .input-group-addon{
                color: #000 !important;
                background-color: #fff !important;
                border: 0px;
                border-bottom: 1px solid #ccc;
            }

            .input-group:not(.input-group-alt) .form-control {
            top: 0px !important;
            height: auto !important;
            }
            
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script>
            var skin = localStorage.getItem('skin') || 'default';
            var unusedLink = document.querySelector('link[data-skin]:not([data-skin="' + skin + '"])');
            unusedLink.setAttribute('rel', '');
            unusedLink.setAttribute('disabled', true);
        </script>

        <script>
            $(document).on('ready', function(){
                $('#show-hide-passwd').on('click', function(e){
                    e.preventDefault();

                    var current = $(this).attr('action');

                    if (current == 'hide'){
                        $(this).prev().attr('type','text');
                        $(this).removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close').attr('action','show');
                    }

                    if(current == 'show'){
                        $(this).prev().attr('type','password');
                        $(this).removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open').attr('action','hide'); 
                    }
                })
            })
        </script>
        
        <script language="javaScript" type="text/javascript">
                $(window).load(function() 
                {   $(".loader").fadeOut("slow");
                });

                function Autentica()
                {       if (ElementValidateBeforeSave("usuario:password"))
                        {   $('#loading').show();
                            ElementStatus("","btautentica");
                            xajax_AutenticaUsuario(xajax.getFormValues("Login"));
                            return false;
                        }        
                }
                    
                function Ejecuta(CMD)
                {       parent.location.href = CMD;
                        return false;
                }

                function cerrarmodal()
                {   document.getElementById('loading').style.display = 'none';
                    cerrar();      
                }
        </script>        
        <script type="text/javascript" src="src/libs/js/elements.js"></script>
        <script type="text/javascript" src="src/libs/js/modal.js"></script>
        <?php   $xajax->printJavascript(); ?>
    
    </head>
    <body>
        <!--[if lt IE 10]>
            <div class="page-message" role="alert">You are using an <strong>outdated</strong> browser. Please <a class="alert-link" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</div>
        <![endif]-->

        <main class="auth auth-floated">
                <div id="announcement" class="auth-announcement" style="background-image: url(src/public/img/banner_ug.png);">
                    <!--div class="announcement-body">
                        <h2 class="announcement-title"> How to Prepare for an Automated Future </h2><a href="#" class="btn btn-warning"><i class="fa fa-fw fa-angle-right"></i> Check Out Now</a>
                    </div-->
                </div>
                <form class="auth-form" id="Login" method="POST">
                    <div class="mb-4">
                        <div class="mb-3">
                            <img class="rounded" src="src/public/img/logo_ug_small.png" alt="" height="72">
                        </div>
                        <h1 class="h3">Inicio de Sesión</h1>
                    </div>
                    <div class="loader" id="loading"></div> 
                    <?php   $ControlLogin->CargaLogin(); ?>  
                    <p class="mb-0 px-3 text-muted text-center">© 2019 Todos los Derechos Reservados Universidad de Guayaquil.</p>
                </form>     
        </main>
    
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/popper.min.js"></script>
        <!-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script> -->
        <script src="vendor/particles.js/particles.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

        <script>
            $(document).on('theme:init', () =>
            {  /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
               particlesJS.load('announcement', 'src/libs/pages/particles.json');
            })
        </script>

        <script src="src/libs/js/theme.min.js"></script> 
    </body>
    </html>