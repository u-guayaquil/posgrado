<?php
        require_once ('src/modules/entorno/config/controlador/ControlConfig.php');
        $ControlConfig = new ControlConfig();   
        
        $xajax->register(XAJAX_FUNCTION,array('GuardarConfiguracion', $ControlConfig,'GuardarConfiguracion'));
        $xajax->processRequest();
?>

        <!doctype html>
        <html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Configuración</title>
                <link rel="stylesheet" type="text/css" href="src/libs/css/body.css"/>
                <link rel="stylesheet" type="text/css" href="src/libs/css/forms.css"/>
                <link rel="stylesheet" type="text/css" href="src/libs/css/elements.css"/>
                <script type="text/javascript" src="src/libs/js/elements.js"></script>
                <script type="text/javascript" src="src/libs/js/validaciones.js"></script>

       
        <script language="javaScript" type="text/javascript">
                function Guardar()
                {       if (ElementValidateBeforeSave("","Db:Usuario:Password:Host:Port"))
                        {   ElementStatusDisabled("","Create");
                            xajax_GuardarConfiguracion(xajax.getFormValues("Config"));
                            ElementStatusEnabled("","Create");
                  
                        }   
                        return false;
                }
        </script>
        <?php   $xajax->printJavascript(); ?>
        </head>

        <body>
            <div class="FormBasicCenter" style="width:480px;">
                <div class="FormSectionTitle">
                    <p class="Form-Title" >Configuración</p>
                </div>     
                <div class="FormSectionData">    
                    <form id="Config" method="POST">
                    <table border="0" class="Form-Frame">
                        <tr height="30">
                            <td class="Form-Label-Regular">Entorno</td>
                            <td class="Form-Label-Regular">
                                <select class="sel-input s05" id="Entorno" name="Entorno">
                                        <option value="PRODUCCION" selected="true">PRODUCCION</option>
                                        <option value="QOS">QoS</option>
                                        <option value="DESARROLLO">DESARROLLO</option>
                                        <option value="WORKGROUP">WORKGROUP</option>
                                </select>
                            </td>
                            <td class="Form-Label-Regular">Módulo</td>
                            <td class="Form-Label-Regular">
                                <select class="sel-input s05" id="Modulo" name="Modulo">
                                        <option value="TITULACION" selected="true">TITULACION</option>
                                        <option value="POSGRADO">POSGRADO</option>
                                </select>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="Form-Label-Regular">Base de datos</td>
                            <td class="Form-Label-Regular">
                                <input type="text" class="txt-input t05" id="Db" name="Db" value="" maxlength="15" onkeydown="return soloNumerosLetras(event);"/>
                            </td>
                            <td class="Form-Label-Regular">Driver</td>
                            <td class="Form-Label-Regular">
                                <select class="sel-input s05" id="Driver" name="Driver">
                                        <option value="SqlProvider" selected="true">SQLSERVER</option>
                                        <!--option value="PgProvider">POSTGRES</option-->
                                </select>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="Form-Label-Regular">Usuario</td>
                            <td class="Form-Label-Regular">
                                <input type="text" class="txt-input t05" id="Usuario"  name="Usuario"  value="" maxlength="10" onkeydown="return soloNumerosLetras(event);"/>
                            </td>
                            <td class="Form-Label-Regular">Password</td>
                            <td class="Form-Label-Regular">
                                <input type="text" class="txt-input t05" id="Password" name="Password" value="" maxlength="20"/>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="Form-Label-Regular">IP / Host</td>
                            <td class="Form-Label-Regular">
                                <input type="text" class="txt-input t05" id="Host" name="Host" value="localhost" maxlength="15"/>
                            </td>
                            <td class="Form-Label-Regular">Port</td>
                            <td class="Form-Label-Regular">
                                <input type="text" class="txt-input t05" id="Port" name="Port" value="1434" maxlength="4" onkeydown="return soloNumeros(event);"/>
                            </td>
                        </tr>
                        <tr height="35">
                            <td colspan="4" class="Form-Label-Regular">
                                <center>
                                <input id="Create" name="Create" type="button" style="height:25px; width:80px" value="Guardar" onclick="return Guardar();"/>    
                                </center>
                            </td>
                        </tr>    
                    </table>
                    </form>    
                </div>                         
                <div id="FormGridDatos" class="FormSectionGrid">
                <?php    echo $ControlConfig->CargaConfiguraciones(); ?>
                </div>
            </div>
        </body>
        </html>