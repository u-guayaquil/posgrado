<?php
        require_once("src/rules/entorno/servicio/ServicioConfig.php"); 
        require_once("src/modules/entorno/config/render/RenderConfig.php"); 

        class ControlConfig
        {       private $ServicioConfig;
                private $RenderConfig;
                
                function __construct() 
                {       $this->ServicioConfig = new ServicioConfig();
                        $this->RenderConfig = new RenderConfig();
                }  

                function CargaConfiguraciones()
                {       $Configuraciones = $this->ServicioConfig->ObtenerConfiguraciones();
                        return $this->RenderConfig->MuestraGrid($Configuraciones);
                }
                
                function GuardarConfiguracion($Form)
                {       $AjaxResponse = new xajaxResponse();   
                        $this->ServicioConfig->CrearConfiguracion($Form);
                        $Resp = $this->ServicioConfig->EstadoServicio();
                        if ($Resp[0])
                        {   $AjaxResponse->Assign("FormGridDatos","innerHTML",$this->CargaConfiguraciones());
                        }
                        $AjaxResponse->alert($Resp[1]);
                        return $AjaxResponse;
                }
        }
?>

