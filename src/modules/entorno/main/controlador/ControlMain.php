<?php       
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/modules/entorno/main/render/RenderMainMenu.php");
    
    class ControlMain
    {       private $ServicioPerfilOpcion;
            private $RenderMainMenu;
 
            function CargarMainMenu()
            {   $this->ServicioPerfilOpcion = new ServicioPerfilOpcion();
                $this->RenderMainMenu = new RenderMainMenu();
                $root = $this->ServicioPerfilOpcion->ObtenerMenu();
                echo $this->RenderMainMenu->CreaMainMenu(json_decode($root));
            }
            
            function CerrarSesion($link,$id,$nombre)
            {       $xAjax = new xajaxResponse();
                    $xAjax = $this->CargaModalGrid($xAjax,$link,$id,$nombre);
                    return $xAjax;
            }
            
            function CargaModalGrid($xResp,$link,$id,$nombre)
            {       $this->RenderMainMenu = new RenderMainMenu();
                    $jsonModal['Modal'] = "ModalMensaje"; 
                    $jsonModal['Title'] = "Cerrar Sesión";
                    $jsonModal['Carga'] = $this->RenderMainMenu->MsgBoxCerrar($link,$id,$nombre);
                    $jsonModal['Ancho'] = "350";
                    $xResp->call("CreaModal", json_encode($jsonModal));
                    return $xResp; 
            }
    }
    
?>