<?php   require_once("src/modules/entorno/main/controlador/ControlMain.php");
        $ControlMain = new ControlMain();
?>
        <html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Universidad de Guayaquil</title>
                <link rel="shortcut icon" href="src/public/img/favicon.png">
                <link rel="stylesheet" type="text/css" href="src/libs/css/body.css"/>
                <link rel="stylesheet" type="text/css" href="src/modules/entorno/main/libs/css/sm-core-css.css"/>
                <link rel="stylesheet" type="text/css" href="src/modules/entorno/main/libs/css/sm-blue/sm-blue.css"/>
                <link rel="stylesheet" type="text/css" href="src/modules/entorno/main/libs/css/main.css"/>
                 
                <script type="text/javascript" src="src/libs/js/jquery.js"></script>
                <script type="text/javascript" src="src/libs/js/solapas.js"></script>
                <script type="text/javascript" src="src/modules/entorno/main/libs/js/jquery.smartmenus.js"></script>
                
                <script type="text/javascript">
                        $(function()
                        {   $('#main-menu').smartmenus({ subMenusSubOffsetX: 1, subMenusSubOffsetY: -8 });
                        });
              
                        function EjecutaOpcion(link,id,nombre)
                        {       if(id==55)
                                {       if(confirm('¿Está seguro que desea salir?'))
                                        {   alert('Usted ha cerrado sesión. Vuelva pronto.');
                                                if (AgregarOptionSolapas(id,nombre))
                                                {       Container = CreaSolapas(id);
                                                        Container.src = "index.php?url="+link+"&opcion="+id;
                                                }
                                                else
                                                alert("Ya se encuentra en ejecución una sesión de esta opción.");
                                                return false;
                                        }
                                        else{   alert('Operacion Cancelada');
                                              return false;  
                                        }
                                }
                                if (AgregarOptionSolapas(id,nombre))
                                {   Container = CreaSolapas(id);
                                    Container.src = "index.php?url="+link+"&opcion="+id;
                                }
                                else
                                alert("Ya se encuentra en ejecución una sesión de esta opción.");
                                return false;
                        }
                         
                </script>
        </head>
        <body>
                <table class="SeccionMain">
                <tr>
                        <td class="SeccionMenu">
                            <?php $ControlMain->CargarMainMenu(); ?>
                        </td>
                </tr>
                <tr>
                        <td class="SeccionSolapa">
                            <label style="font-size: 9pt">Opcion:</label>
                            <select id="solapas" style="width: 150px;" onchange=" return CambiaSolapas(); ">
                                    <option value='0000'>Home</option>
                            </select>
                            <input type="button" value="X" onclick=" return QuitaSolapas();"/>
                        </td>
                </tr>
                <tr>
                        <td id= "SeccionContenido">
                            <iframe class="SeccionContenido" id="div0000" src="Home.php" frameborder="0"></iframe>
                        </td>
                </tr>
                </table>
        </body>
        </html>