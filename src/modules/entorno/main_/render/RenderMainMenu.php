<?php
          class RenderMainMenu
          {     private $Utils;

                function __construct()
                {       $this->Utils = new Utils();  
                }        
              
                function CreaMainMenu($root)
                {        $flag = 0;
                         $menu = '';
                         foreach($root as $sheet)
                         {       $child = $this->CreaMainMenuSgte($sheet[3]);
                                 if ($child != '')
                                 {   if ($flag == 0)
                                     {   $menu.= '<nav id="main-nav" role="navigation">';
                                         $menu.= '<ul id="main-menu" class="sm sm-blue">';
                                         $flag = 1;
                                     }
                                     $Name = $this->NombreMenu($sheet[2]);
                                     $menu.= '<li>';
                                     $menu.= '<a href="">'.$Name.'</a>';
                                     $menu.= '<ul>'.$child.'</ul>';
                                     $menu.= '</li>';
                                 }
                         }
                         if ($flag == 1) $menu.= '</ul></nav>';
                         return $menu;
                }
                function CreaMainMenuSgte($root)
                {       $subm = '';
                        foreach($root as $sheet)
                        {       $Name = $this->NombreMenu($sheet[2]);
                                if ($sheet[0]=='O')
                                {   $even = 'onclick=" return EjecutaOpcion(\''.$this->Utils->Encriptar($sheet[3]).'\',\''.$sheet[1].'\',\''.$Name.'\');"';
                                    $subm.= '<li><a href="'.$sheet[3].'" '.$even.'>'.$Name.'</a></li>';                             
                                }
                                else
                                {   $rslt = $this->CreaMainMenuSgte($sheet[3]);
                                    if ($rslt != '')
                                    {   $subm.= '<li><a href="#">'.$Name.'</a>';
                                        $subm.= '<ul>'.$rslt.'</ul>';
                                        $subm.= '</li>';
                                    }
                                }
                        }
                        return $subm;
                }
                
                function NombreMenu($OpcionName)
                {       $Phars = "";
                        $Nodos = explode(" ", $OpcionName);
                        foreach ($Nodos as $Nodo)
                        {   $Nodo = strtolower(trim($Nodo));
                            if ($Nodo!="")
                            {   if (in_array($Nodo, array('de','x','por','para','y','la','lo','los','que','se','este','esta')))
                                $Phars.=strtolower(trim($Nodo))." ";
                                else
                                $Phars.=ucwords(strtolower(trim($Nodo)))." ";
                            }    
                        }
                        return $Phars;
                }
         }



?>

