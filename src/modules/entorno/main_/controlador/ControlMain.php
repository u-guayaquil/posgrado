<?php
    require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
    require_once("src/modules/entorno/main/render/RenderMainMenu.php");
    
    class ControlMain
    {       private $ServicioPerfilOpcion;
            private $RenderMainMenu;

            function CargarMainMenu()
            {   $this->ServicioPerfilOpcion = new ServicioPerfilOpcion();
                $this->RenderMainMenu = new RenderMainMenu();
                $root = $this->ServicioPerfilOpcion->ObtenerMenu();
                echo $this->RenderMainMenu->CreaMainMenu(json_decode($root));
            }
    }
    
?>
