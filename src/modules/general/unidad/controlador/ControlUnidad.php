<?php
         require_once("src/rules/general/servicio/ServicioUnidad.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/general/unidad/render/RenderUnidad.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlUnidad
         {     private  $Sufijo; 
               private  $ServicioUnidad;
               private  $RenderUnidad;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioUnidad = new ServicioUnidad();
                        $this->RenderUnidad = new RenderUnidad($Sufijo);
               }

               function CargaUnidadBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaUnidadMantenimiento()
               {        echo $this->RenderUnidad->CreaUnidadMantenimiento();
               }

               function CargaUnidadSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoUnidad = $this->ServicioUnidad->BuscarUnidadByDescripcion($prepareDQL);
                        echo $this->RenderUnidad->CreaUnidadSearchGrid($datoUnidad);
               }         
               
               function MuestraUnidadByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoUnidad = $this->ServicioUnidad->BuscarUnidadByID($prepareDQL);
                        return $this->RenderUnidad->MuestraUnidad($ajaxRespon,$datoUnidad);
               }

               function MuestraUnidadByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoUnidad = $this->ServicioUnidad->BuscarUnidadByDescripcion($prepareDQL);
                        return $this->RenderUnidad->MuestraUnidadGrid($ajaxRespon,$datoUnidad);
               }

               function GuardaUnidad($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Unidad  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioUnidad->GuardaDBUnidad($Unidad);
                        if (is_numeric($id)){
                            $function = (empty($Unidad->id) ? "MuestraUnidadGuardado" : "MuestraUnidadEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioUnidad->BuscarUnidadByID($prepareDQL);
                            return $this->RenderUnidad->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderUnidad->MuestraUnidadExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaUnidad($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioUnidad->DesactivaUnidad(intval($id));
                        $Datos = $this->ServicioUnidad->BuscarUnidadByDescripcion($prepareDQL);
                        return $this->RenderUnidad->MuestraUnidadEliminado($ajaxRespon,$Datos);
                }
         }

?>

