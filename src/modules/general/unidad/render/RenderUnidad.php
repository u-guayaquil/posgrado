<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderUnidad
        {     private $Sufijo;        
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
                       $this->Select = new ComboBox($Sufijo);

                       $this->Columns['Id']       = array('30px','center','');
                       $this->Columns['Abreviatura']   = array('80px','left','');
                       $this->Columns['Unidad'] = array('150px','left','');
                       $this->Columns['Creacion']   = array('120px','center','');
                       $this->Columns['Estado']   = array('70px','left',''); 
                       $this->Columns['idEstado']   = array('0px','left','none'); 

                       $this->Configs = array('AltoCab' => '25px','DatoCab' => $this->Columns,'AltoDet' => '150px','AltoRow' => '20px');
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaUnidadMantenimiento()
              {                          
                       $Unidad = '<table class="Form-Frame" cellpadding="0">';
                       $Unidad.= '       <tr height="30">';
                       $Unidad.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $Unidad.= '             <td class="Form-Label" style="width:75%">';
                       $Unidad.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:abreviatura\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $Unidad.= '             </td>';
                       $Unidad.= '         </tr>';
                       $Unidad.= '         <tr height="30">';
                       $Unidad.= '             <td class="Form-Label" style="width:25%">Abreviatura</td>';
                       $Unidad.= '             <td class="Form-Label" style="width:75%">';
                       $Unidad.= '                 <input type="text" class="txt-upper t07" id="abreviatura'.$this->Sufijo.'" name="abreviatura'.$this->Sufijo.'" value="" maxlength="5" disabled/>';
                       $Unidad.= '             </td>';
                       $Unidad.= '         </tr>';
                       $Unidad.= '         <tr height="30">';
                       $Unidad.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $Unidad.= '             <td class="Form-Label" style="width:75%">';
                       $Unidad.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Unidad.= '             </td>';
                       $Unidad.= '         </tr>';
                       $Unidad.= '         <tr height="30">';
                       $Unidad.= '             <td class="Form-Label">Estado</td>';
                       $Unidad.= '             <td class="Form-Label">';
                                                   $Unidad.= $this->CreaComboEstado();
                       $Unidad.= '             </td>';
                       $Unidad.= '         </tr>';
                       $Unidad.= '</table>';
                       return $Unidad;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Abreviatura']   = array('80px','left','');
                        $Columns['Descripci&oacute;n'] = array('180px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaUnidadSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->UnidadGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function UnidadGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $unidad)
                      {        $fecreacion = $this->Fechas->changeFormatDate($unidad['fecreacion'],"DMY");
                               $id = str_pad($unidad['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($unidad['abreviatura']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($unidad['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$unidad['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$unidad['idestado']); 
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($unidad['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraUnidad($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion'])); 
                            $Ajax->Assign("abreviatura".$this->Sufijo,"value", trim($Datos[0]['abreviatura'])); 
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaUnidad($Ajax,"NOEXISTEID","No existe una Unidad con ese ID.");
                       }
              }
              
              function MuestraUnidadGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->UnidadGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }   
               
              function MuestraUnidadGuardado($Ajax,$Unidad)
              {       if (count($Unidad)>0)
                        {   $id = str_pad($Unidad[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraUnidadGrid($Ajax,$Unidad);
                            return $this->RespuestaUnidad($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaUnidad($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraUnidadEditado($Ajax,$Unidad)
              {       foreach ($Unidad as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraUnidadRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaUnidad($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaUnidad($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraUnidadEliminado($Ajax,$Unidad)
              {        foreach ($Unidad as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraUnidadRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaUnidad($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaUnidad($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraUnidadRowGrid($Ajax,$Unidad,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Unidad['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($Unidad['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Unidad['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Unidad['abreviatura']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Unidad['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Unidad['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Unidad['idestado']));
                        return $Ajax;
                }

              function MuestraUnidadExcepcion($Ajax,$Msg)
                {       return $this->RespuestaUnidad($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaUnidad($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

