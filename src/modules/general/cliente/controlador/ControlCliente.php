<?php
        require_once("src/rules/general/servicio/ServicioCliente.php");
        require_once("src/rules/general/servicio/ServicioClienteIdentificacion.php");
        require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
        require_once("src/rules/general/servicio/ServicioCiudad.php");
        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/rules/general/servicio/ServicioParroquia.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/modules/general/cliente/render/RenderCliente.php");
        require_once("src/modules/general/ciudad/render/RenderCiudad.php");   
        require_once("src/modules/general/parroquia/render/RenderParroquia.php");     
        require_once("src/modules/general/empleado/render/RenderEmpleado.php");     
        require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");


        class ControlCliente
        {       private  $Sufijo;
                private  $ServicioCliente;
                private  $ServicioClienteIdentificacion;
                private  $ServicioPlanCuentas;
                private  $ServicioCiudad;
                private  $ServicioParroquia;
                private  $ServicioEmpleado;
                private  $RenderCliente;
                private  $RenderCiudad;
                private  $RenderParroquia;
                private  $RenderEmpleado;
                private  $RenderPlanCuentas;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        
                        $this->ServicioCliente = new ServicioCliente();
                        $this->ServicioClienteIdentificacion = new ServicioClienteIdentificacion();
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        $this->ServicioCiudad = new ServicioCiudad();
                        $this->ServicioParroquia = new ServicioParroquia();
                        $this->ServicioEmpleado = new ServicioEmpleado();
                        $this->RenderCliente = new RenderCliente($Sufijo);
                        $this->RenderCiudad = new RenderCiudad($Sufijo);
                        $this->RenderParroquia = new RenderParroquia($Sufijo);
                        $this->RenderEmpleado = new RenderEmpleado($Sufijo);
                        $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
                }
               
                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaMantenimiento()
                {        echo $this->RenderCliente->CreaMantenimiento();
                }

                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse(); 
                        $prepareDQL = array('id' => intval($id));
                        $datoCliente = $this->ServicioCliente->BuscarCliente($prepareDQL);
                        return $this->RenderCliente->MuestraCliente($ajaxRespon,$datoCliente);
                }

                function Guarda($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $Cliente  = json_decode($Form)->Forma;
                        $id = $this->ServicioCliente->GuardaDB($Cliente);
                        if (is_numeric($id)){
                            return $this->RenderCliente->MuestraActualizado($ajaxRespon,$id);                      
                        }else{    
                            return $this->RenderCliente->MuestraExcepcion($ajaxRespon,$id);
                        }    
                }
                
                function Elimina($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $this->ServicioCliente->DesactivaCliente(intval($id));
                        return $this->RenderCliente->MuestraEliminado($ajaxRespon);
                }
               
                function CargaModalGridCliente($Operacion,$IdRelation)
                {       $ajaxResp = new xajaxResponse(); 
                
                        if ($Operacion=="btCliente" || $Operacion=="btClienteFactura")
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'texto'=>'');
                            $Datos = $this->ServicioCliente->BuscarCliente($prepareDQL);
                            $jsonModal['Carga'] = $this->RenderCliente->CreaModalGridCliente($Operacion,$Datos);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cliente";
                            $jsonModal['Ancho'] = $this->RenderCliente->WidthModalGrid();
                        }                   
                        else if ($Operacion=="btCiudad")
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'texto'=>'','estado'=>1);
                            $Datos = $this->ServicioCiudad->BuscarCiudadParaCombo($prepareDQL);
                            $jsonModal['Carga'] = $this->RenderCiudad->CreaModalGridCiudad($Operacion,$Datos);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Ciudad";
                            $jsonModal['Ancho'] = $this->RenderCiudad->WidthModalGrid();
                        }    
                        else if ($Operacion=="btParroquia")
                        {   $prepareDQL = array('limite' => 50,'orden' => 'r.descripcion','ciudad' => $IdRelation,'estado'=>1);
                            $Datos = $this->ServicioParroquia->BuscarParroquiaByDescripcion($prepareDQL);
                            $jsonModal['Carga'] = $this->RenderParroquia->CreaModalGridParroquia($Operacion,$Datos);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Parroquia";
                            $jsonModal['Ancho'] = $this->RenderParroquia->WidthModalGrid();
                        }
                        else if ($Operacion=="btVendedor")
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '','estado'=>1);
                            $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);
                            $jsonModal['Carga'] = $this->RenderEmpleado->CreaModalGrid($Operacion,$Datos);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Vendedor";
                            $jsonModal['Ancho'] = $this->RenderEmpleado->WidthModalGrid();
                        }    
                        else if ($Operacion=="btCuentaPorCobrar" || $Operacion=="btCuentaAnticipo" || $Operacion=="btCuentaExcedente")
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '');
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Plan Cuentas";
                            $jsonModal['Ancho'] = $this->RenderPlanCuentas->WidthModalGrid();
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                                
                function ConsultaModalGridClienteByTX($Operacion,$Texto = "",$IdRelation=0)
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        
                        switch($Operacion){
                            case "btCuentaPorCobrar" || "btCuentaAnticipo" || "btCuentaExcedente":
                                $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                                $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                                return $this->RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);                                        
                            case "btCiudad":
                                $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                                $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                                return $this->RenderCiudad->MuestraModalGridCiudad($ajaxResp,$Operacion,$Datos);                                        
                            case "btParroquia":
                                $prepareDQL = array('ciudad'=>$IdRelation, 'limite' => 50, 'orden' => 'r.descripcion', 'texto' => strtoupper($texto), 'estado'=>1);
                                $Datos = $this->ServicioParroquia->BuscarParroquiaByDescripcion($prepareDQL);
                                return $this->RenderParroquia->MuestraModalGridParroquia($ajaxResp,$Operacion,$Datos);                                        
                            case "btVendedor":
                                $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto),'estado'=>1);
                                $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);
                                return $this->RenderEmpleado->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                          
                            case "btCliente":
                                $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                                $Datos = $this->ServicioCliente->BuscarCliente($prepareDQL);
                                return $this->RenderCliente->MuestraModalGridCliente($ajaxResp,$Operacion,$Datos);                          
                        }
                        
                }
                
         }

?>

