<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");            
        require_once("src/rules/general/entidad/Estado.php"); 
        require_once("src/rules/general/servicio/ServicioGrupoCliente.php");
        require_once("src/rules/general/servicio/ServicioTerminos.php");
        require_once("src/rules/general/servicio/ServicioEstadoCivil.php");
        require_once("src/rules/general/servicio/ServicioCliente.php");
        require_once("src/rules/impuesto/servicio/ServicioIdentificacion.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderCliente
        {   private $Sufijo;
            private $SearchGrid;
            private $Maxlen;
            private $Fechas;
            private $Search;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Search = new SearchInput($this->Sufijo);                      
                    $this->Fechas = new DateControl();
                    $this->Maxlen['id'] = 7; 
            }
              
            function CreaOpcionBarButton($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);
                    return $BarButton->CreaBarButton($Buttons);
            }
              
            function CreaMantenimiento()
            {       
                    $Cliente = '<table class="Form-Frame" cellpadding="0" border="0" style="width:650px">';
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label" style="width:130px">Id</td>';
                    $Cliente.= '           <td class="Form-Label" style="width:80px">';
                    $Cliente.=                 $this->Search->TextSch("Cliente","","")->Enabled(true)->MaxLength($this->Maxlen['id'])->ReadOnly(false)->Create("t02");
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label" style="width:100px"></td>';
                    $Cliente.= '           <td class="Form-Label" style="width:100px">';
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label" style="width:130px">&nbsp;&nbsp;Estado</td>';
                    $Cliente.= '           <td class="Form-Label" style="width:110px">';
                    $Cliente.=                 $this->CreaComboEstado();
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Codigo</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" onKeyDown="return soloNumerosLetras(event); " maxlength="10" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label"></td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '           </td>'; 
                    $Cliente.= '           <td class="Form-Label" style="width:100px">&nbsp;&nbsp;Fecha Cliente</td>'; 
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <input class="txt-upper t04" name = "fechacliente'.$this->Sufijo.'" id = "fechacliente'.$this->Sufijo.'" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';

                    $Cliente.= '        <tr height="35">';
                    $Cliente.= '            <td class="Form-Label" colspan="6"><u>Datos Personales:</u></td>';
                    $Cliente.= '        </tr>';
                    
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Nombre o  Razón</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.= '               <input type="text" class="txt-upper t11" id="nombre'.$this->Sufijo.'" name="nombre'.$this->Sufijo.'" value="" maxlength="120" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label" style="width:100px">&nbsp;&nbsp;Sexo</td>'; 
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <select id="sexo'.$this->Sufijo.'" name="sexo'.$this->Sufijo.'" class="sel-input s04" disabled>';
                    $Cliente.= '                       <option value="M">MASCULINO</option>';
                    $Cliente.= '                       <option value="F">FEMENINO</option>';
                    $Cliente.= '               </select>';
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    $Cliente.= '       <tr height="28">'; 
                    $Cliente.= '           <td class="Form-Label">Direcci&oacute;n</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.= '               <input type="text" class="txt-upper t11" id="direccion'.$this->Sufijo.'" name="direccion'.$this->Sufijo.'" value="" maxlength="150" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;Estado Civil</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.=                 $this->CreaComboEstadoCivil();                    
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';                       
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Ciudad</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="2">';
                    $Cliente.=                $this->Search->TextSch("Ciudad","","")->Enabled(false)->Create("t06");
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label"></td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;Tel&eacute;fonos</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <input type="text" class="txt-upper t04" id="telefono'.$this->Sufijo.'" name="telefono'.$this->Sufijo.'" onKeyDown="return soloNumeros(event); " value="" maxlength="10" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Parroquia</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="2">';
                    $Cliente.=                 $this->Search->TextSch("Parroquia","","")->Enabled(false)->Create("t06");
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label"></td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;Movil</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <input type="text" class="txt-upper t04" id="movil'.$this->Sufijo.'" name="movil'.$this->Sufijo.'" onKeyDown="return soloNumeros(event); " value="" maxlength="10" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Rep. Legal</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.= '               <input type="text" class="txt-upper t11" id="replegal'.$this->Sufijo.'" name="replegal'.$this->Sufijo.'" value="" maxlength="120" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;Obl. Contabilidad</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <input type="checkbox" id="obligacontabilidad'.$this->Sufijo.'" name="obligacontabilidad'.$this->Sufijo.'" value="1" disabled/>';                    
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    $Cliente.= '        <tr height="35">';
                    $Cliente.= '            <td class="Form-Label" colspan="6"><u>Datos Comerciales:</u></td>';
                    $Cliente.= '        </tr>';
                    
                    $Cliente.= '       <tr height="28">'; 
                    $Cliente.= '           <td class="Form-Label">Email</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.= '               <input type="text" class="txt-upper t11" id="email'.$this->Sufijo.'" name="email'.$this->Sufijo.'" value="" maxlength="200" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;Grupo</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.=                 $this->CreaComboGrupoCliente();                    
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';                       
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Cliente Factura</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.=                 $this->Search->TextSch("ClienteFactura","","")->Enabled(false)->Create("t10");
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;T&eacute;rminos</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.=                 $this->CreaComboTerminos();                    
                    $Cliente.= '           </td>'; 
                    $Cliente.= '       </tr>';
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Vendedor</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.=                 $this->Search->TextSch("Vendedor","","")->Enabled(false)->Create("t10");
                    $Cliente.= '           </td>'; 
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;Cupo $</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <input type="text" class="txt-right t04" id="cupo'.$this->Sufijo.'" name="cupo'.$this->Sufijo.'"  onKeyDown="return soloFloat(event,this,2); " value="" maxlength="20" disabled/>';
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    
                    $Cliente.= '        <tr height="35">';
                    $Cliente.= '            <td class="Form-Label" colspan="6"><u>Datos Contables:</u></td>';
                    $Cliente.= '        </tr>';
                    
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Cta. Cobrar</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.=                 $this->Search->TextSch("CuentaPorCobrar","","")->Enabled(false)->Create("t10");
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;Tipo Cliente</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <select id="tipocliente'.$this->Sufijo.'" onchange="return ControlTipoCliente'.$this->Sufijo.'(\''.$this->Sufijo.'\',this)" name="tipocliente'.$this->Sufijo.'" class="sel-input s04" disabled>';
                    $Cliente.= '                       <option value="N">NATURAL</option>';
                    $Cliente.= '                       <option value="J">JUR&Iacute;DICO</option>';
                    $Cliente.= '               </select>';
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Cta. Anticipo</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.=                 $this->Search->TextSch("CuentaAnticipo","","")->Enabled(false)->Create("t10");
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;Cédula</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <input type="text" class="txt-input t04" id="cedula'.$this->Sufijo.'" name="cedula'.$this->Sufijo.'" value="" maxlength="10" onKeyDown="return soloNumeros(event); " disabled/>';                        
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    $Cliente.= '       <tr height="28">';
                    $Cliente.= '           <td class="Form-Label">Cta. Excedente</td>';
                    $Cliente.= '           <td class="Form-Label" colspan="3">';
                    $Cliente.=                 $this->Search->TextSch("CuentaExcedente","","")->Enabled(false)->Create("t10");
                    $Cliente.= '           </td>';
                    $Cliente.= '           <td class="Form-Label">&nbsp;&nbsp;RUC</td>';
                    $Cliente.= '           <td class="Form-Label">';
                    $Cliente.= '               <input type="text" class="txt-input t04" id="RUC'.$this->Sufijo.'" name="RUC'.$this->Sufijo.'" value="" maxlength="13" onKeyDown="return soloNumeros(event); " disabled/>';                        
                    $Cliente.= '           </td>';
                    $Cliente.= '       </tr>';
                    $Cliente.= '</table>';                     
                    return $Cliente;
                    
                    //
                    //
            }

            private function CreaComboEstado()
            {       $Select = new ComboBox($this->Sufijo);                        
            
                    $ServicioEstado = new ServicioEstado();
                    $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
            }
              
            private function CreaComboGrupoCliente()
            {       $Select = new ComboBox($this->Sufijo);       
            
                    $ServicioGrupoCliente = new ServicioGrupoCliente();
                    $datosGrupoCliente = $ServicioGrupoCliente->BuscarGrupoClienteByArrayID(array(1));
                    return $Select->Combo("grupocliente",$datosGrupoCliente)->Selected(1)->Create("s04");
            }
              
            private function CreaComboTerminos()
            {       $Select = new ComboBox($this->Sufijo);                        
            
                    $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'estado'=>array(1));
                    $ServicioTerminos = new ServicioTerminos();
                    $datosTerminos = $ServicioTerminos->BuscarTerminosByDescripcion($prepareDQL);  
                    return $Select->Combo("terminos",$datosTerminos)->Selected(1)->Create("s04");
            }
              
            private function CreaComboEstadoCivil()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioEstadoCivil = new ServicioEstadoCivil();
                    $datosEstadoCivil = $ServicioEstadoCivil->BuscarEstadoCivil(array());
                    return $Select->Combo("estadocivil",$datosEstadoCivil)->Selected(1)->Create("s04");
            }
              
            function MuestraCliente($Ajax,$Datos)
            {       foreach ($Datos as $dato) 
                    {   $Ajax->Assign("idCliente".$this->Sufijo,"value", str_pad($dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign("nombre".$this->Sufijo,"value", trim($dato['nombre']));                     
                        $Ajax->Assign("codigo".$this->Sufijo,"value", trim($dato['codigo']));  
                        $Ajax->Assign("replegal".$this->Sufijo,"value", trim($dato['representante']));  
                        $Ajax->Assign("cedula".$this->Sufijo,"value", trim($dato['cedula']));  
                        $Ajax->Assign("RUC".$this->Sufijo,"value", trim($dato['ruc']));  
                        $Ajax->Assign("direccion".$this->Sufijo,"value", trim($dato['direccion']));
                        $Ajax->Assign("telefono".$this->Sufijo,"value", $dato['telefono']);
                        $Ajax->Assign("email".$this->Sufijo,"value", trim($dato['email']));
                        $Ajax->Assign("tipocliente".$this->Sufijo,"value", $dato['idtipocliente']);
                        $Ajax->Assign("sexo".$this->Sufijo,"value", $dato['sexo']);
                        $Ajax->Assign("movil".$this->Sufijo,"value", $dato['movil']);
                        $Ajax->Assign("grupocliente".$this->Sufijo,"value", $dato['idgrupo']);
                        $Ajax->Assign("estadocivil".$this->Sufijo,"value", $dato['idestadocivil']);
                        
                        $Obliga=false; 
                        if($dato['oblcontabilidad']==1) $Obliga=true;  
                        $Ajax->Assign("obligacontabilidad".$this->Sufijo,"checked", $Obliga);
                        
                        $Ajax->Assign("idCuentaPorCobrar".$this->Sufijo,"value", $dato['ctaxcobrar']);
                        $Ajax->Assign("txCuentaPorCobrar".$this->Sufijo,"value", trim($dato['cuentaporcobrar']));
                        $Ajax->Assign("idCuentaAnticipo".$this->Sufijo,"value", $dato['ctaanticip']);
                        $Ajax->Assign("txCuentaAnticipo".$this->Sufijo,"value", trim($dato['cuentaanticipo']));                            
                        $Ajax->Assign("idCuentaExcedente".$this->Sufijo,"value", $dato['ctaexcedente']);
                        $Ajax->Assign("txCuentaExcedente".$this->Sufijo,"value", trim($dato['excedente']));
                        $Ajax->Assign("idCiudad".$this->Sufijo,"value", $dato['idciudad']);
                        $Ajax->Assign("txCiudad".$this->Sufijo,"value", trim($dato['ciudad']));
                        $Ajax->Assign("idParroquia".$this->Sufijo,"value", $dato['idparroquia']);
                        $Ajax->Assign("txParroquia".$this->Sufijo,"value", trim($dato['parroquia']));
                        $Ajax->Assign("idClienteFactura".$this->Sufijo,"value", $dato['idclientefactura']);
                        $Ajax->Assign("txClienteFactura".$this->Sufijo,"value", trim($dato['clientefactura']));
                        $Ajax->Assign("cupo".$this->Sufijo,"value", number_format($dato['cupo'],2,".",""));
                        $Ajax->Assign("idVendedor".$this->Sufijo,"value", $dato['idvendedor']);
                        
                        if ($dato['idvendedor']==0)
                            $Ajax->Assign("txVendedor".$this->Sufijo,"value", "");
                        else                            
                            $Ajax->Assign("txVendedor".$this->Sufijo,"value", trim($dato['nomvendedor'])." ".trim($dato['apevendedor'])); 
                        
                        $Ajax->Assign("estado".$this->Sufijo,"value", $dato['idestado']); 
                        $Ajax->Assign("fechacliente".$this->Sufijo,"value", $this->Fechas->changeFormatDate($dato['fechacliente'],"DMY")[1]);                       
                        $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                        return $Ajax;
                    }    
                    return $this->Respuesta($Ajax,"NOEXISTEID","No existe un Cliente con ese ID.");
            }
              
            function MuestraActualizado($Ajax,$Respuesta)
            {       if (is_numeric($Respuesta)){
                        return $this->Respuesta($Ajax,"GUARDADO",str_pad($Respuesta, $this->Maxlen['id'],"0",STR_PAD_LEFT));
                    }else{
                        return $this->Respuesta($Ajax,"EXCEPCION",$Respuesta);
                    }
            }

            function MuestraEliminado($Ajax)
            {       return $this->Respuesta($Ajax,"ELIMINADO",0);
            }

            function MuestraExcepcion($Ajax,$Msg)
            {       return $this->Respuesta($Ajax,"EXCEPCION",$Msg);    
            }

            private function Respuesta($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
                
            /*** MODAL ***/
            private function SearchGridModalConfig()
            {       $Columns['Id']     = array('50px','center','');
                    $Columns['Codigo'] = array('80px','left','');
                    $Columns['Nombre'] = array('250px','left','');
                    $Columns['Estado'] = array('80px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
            }

            function WidthModalGrid()
            {       return 490;
            }
            
            function CreaModalGridCliente($Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->GridDataHTMLModalCliente($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
                
            function MuestraModalGridCliente($Ajax,$Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->GridDataHTMLModalCliente($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }        

            private function GridDataHTMLModalCliente($Grid,$Datos)
            {       foreach ($Datos as $Opcion)
                    {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['nombre']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['txtestado']));
                            $Grid->CreaSearchRowsDetalle ($id,($Opcion['idestado']==0 ? "red" : ""));
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
                
        }
?>

