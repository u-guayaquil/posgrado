    $(document).ready(function()
    {      $('#fechacliente_Cliente').datetimepicker({
            timepicker:false,
            format: 'd/m/Y'
            });
    });

    var clsCliente = "nombre:codigo:direccion:idCiudad:txCiudad:telefono:idParroquia:txParroquia:movil:email:replegal";
        clsCliente = clsCliente + ":idClienteFactura:txClienteFactura:idVendedor:txVendedor:idCuentaPorCobrar:txCuentaPorCobrar";
        clsCliente = clsCliente + ":fechacliente:idCuentaAnticipo:txCuentaAnticipo:idCuentaExcedente:txCuentaExcedente:cupo:cedula:RUC";
    var objCliente = "obligacontabilidad:nombre:codigo:direccion:sexo:idCiudad:txCiudad:telefono:idParroquia:txParroquia:movil:email";
        objCliente = objCliente + ":estadocivil:idClienteFactura:txClienteFactura:tipocliente:idVendedor:txVendedor:grupocliente:idCuentaPorCobrar:txCuentaPorCobrar";
        objCliente = objCliente + ":fechacliente:idCuentaAnticipo:txCuentaAnticipo:terminos:idCuentaExcedente:txCuentaExcedente:cupo:cedula:RUC:replegal";
    var valCliente = "nombre:direccion:idCiudad:txCiudad:telefono:idParroquia:txParroquia:movil:email:fechacliente";   
    var btnCliente = "btCiudad:btParroquia:btClienteFactura:btVendedor:btCuentaPorCobrar:btCuentaAnticipo:btCuentaExcedente";

    function ButtonClick_Cliente(Sufijo, Operacion)
    {       if (Operacion === 'addNew')
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateEnabled(Sufijo, btnCliente);
                ElementStatus(Sufijo, objCliente, "idCliente:btCliente");
                ElementClear(Sufijo, clsCliente+':idCliente');
                ElementSetValue(Sufijo, "estado", 1);
            } 
            else if (Operacion === 'addMod')
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateEnabled(Sufijo, btnCliente);
                if (ElementGetValue(Sufijo,"estado")==0){
                    ElementStatus(Sufijo, objCliente + ":estado", "idCliente:btCliente");
                }else{
                    ElementStatus(Sufijo, objCliente, "idCliente:btCliente");
                }    
            } 
            else if (Operacion === 'addDel')
            {   if (ElementGetValue(Sufijo, "estado")==1)
                {   if (confirm("Desea inactivar este registro?")){
                        xajax_Elimina_Cliente(ElementGetValue(Sufijo, "idCliente"));
                    }
                } 
                else
                alert("El registro se encuentra inactivo");
            } 
            else if (Operacion === 'addSav')
            {   var valAdd = ":cedula";
                if (ElementGetValue(Sufijo,"tipocliente")=="J"){ 
                    valAdd = ":RUC"; 
                    ElementSetValue(Sufijo, "cedula", "");
                    ElementSetValue(Sufijo, "obligacontabilidad", true);
                } 
                else if (ElementGetValue(Sufijo, "cedula")=="")
                {   valAdd = ":RUC"; 
                }   
                
                if (ElementValidateBeforeSave(Sufijo, valCliente + valAdd))
                {   //if (BarButtonState(Sufijo, "Inactive"))
                    {   var Forma = PrepareElements(Sufijo, objCliente + ":idCliente:estado");
                        xajax_Guarda_Cliente(JSON.stringify({Forma}));
                    }        
                }    
            } 
            else if (Operacion === 'addCan')
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateDisabled(Sufijo, btnCliente);
                ElementStatus(Sufijo, "idCliente:btCliente", objCliente);
                ElementClear(Sufijo, clsCliente+':idCliente');
            } 
            else 
            {   if (Operacion=="btParroquia")
                {   var idCiudad=ElementGetValue(Sufijo,"idCiudad");
                    if (idCiudad!="")    
                        xajax_CargaModal_Cliente(Operacion,idCiudad);
                    else
                    alert("Primero debe seleccionar una ciudad.");
                }
                else
                xajax_CargaModal_Cliente(Operacion,0);    
            }    
            return false;    
    }
    
    function ControlTipoCliente_Cliente(Sufijo, Combo)
    {       if (Combo.value=="J"){ 
                ElementSetValue(Sufijo, "cedula", "");
                ElementSetValue(Sufijo, "obligacontabilidad", true);
            }    
    }
    
    function SearchByElement_Cliente(Sufijo, Elemento)
    {       Elemento.value = TrimElement(Elemento.value);
            
            switch (Elemento.id) 
            {   case "idCliente" + Sufijo:
                    if (Elemento.value.length != 0)
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        xajax_BuscaByID_Cliente(Elemento.value);
                    }
                    break;
                case "FindCuentaPorCobrarTextBx" + Sufijo:
                    xajax_BuscaModalByTX_Cliente("btCuentaPorCobrar", Elemento.value);
                    break;
                case "FindCuentaAnticipoTextBx" + Sufijo:
                    xajax_BuscaModalByTX_Cliente("btCuentaAnticipo", Elemento.value);
                    break;
                case "FindCuentaExcedenteTextBx" + Sufijo:
                    xajax_BuscaModalByTX_Cliente("btCuentaExcedente", Elemento.value);
                    break;
                case 'FindCiudadTextBx' + Sufijo:
                    xajax_BuscaModalByTX_Cliente("btCiudad", Elemento.value);
                    break;
                case 'FindVendedorTextBx' + Sufijo:
                    xajax_BuscaModalByTX_Cliente("btVendedor", Elemento.value);
                    break;
                case 'FindClienteTextBx' + Sufijo:
                    xajax_BuscaModalByTX_Cliente("btCliente", Elemento.value);
                    break;
                case 'FindClienteFacturaTextBx' + Sufijo:
                    xajax_BuscaModalByTX_Cliente("btClienteFactura", Elemento.value);
                    break;
                case 'FindParroquiaTextBx' + Sufijo:
                    var idCiudad=ElementGetValue(Sufijo,"idCiudad");
                    if (idCiudad!="")    
                        xajax_BuscaModalByTX_Cliente("btParroquia", Elemento.value,idCiudad);
                    else
                        alert("Primero debe seleccionar una ciudad.");    
                    break;
            }
    }

    function SearchClienteGetData_Cliente(Sufijo, DatosGrid)
    {       var Id = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("idCliente" + Sufijo).value = Id;
            xajax_BuscaByID_Cliente(Id);    
            cerrar();
            return false;
    }

    function SearchCuentaPorCobrarGetData_Cliente(Sufijo, DatosGrid)
    {       document.getElementById("idCuentaPorCobrar" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txCuentaPorCobrar" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCuentaAnticipoGetData_Cliente(Sufijo, DatosGrid)
    {       document.getElementById("idCuentaAnticipo" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txCuentaAnticipo" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCuentaExcedenteGetData_Cliente(Sufijo, DatosGrid)
    {       document.getElementById("idCuentaExcedente" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txCuentaExcedente" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCiudadGetData_Cliente(Sufijo, DatosGrid)
    {       ElementSetValue(Sufijo, "idParroquia", "");
            ElementSetValue(Sufijo, "txParroquia", "");
            document.getElementById("idCiudad" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txCiudad" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchVendedorGetData_Cliente(Sufijo, DatosGrid)
    {       document.getElementById("idVendedor" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txVendedor" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchClienteFacturaGetData_Cliente(Sufijo, DatosGrid)
    {       document.getElementById("idClienteFactura" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txClienteFactura" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchParroquiaGetData_Cliente(Sufijo, DatosGrid)
    {       if (DatosGrid.cells[0].childNodes[0].nodeValue!="->")
            {   document.getElementById("idParroquia" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                document.getElementById("txParroquia" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                cerrar();
            }
            else
            alert("El registro no es correcto.");
            return false;
    }

    function XAJAXResponse_Cliente(Sufijo,Mensaje,Datos)
    {       if (Mensaje === "GUARDADO")
            {   BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, btnCliente);
                ElementSetValue(Sufijo, "idCliente", Datos);
                ElementStatus(Sufijo, "idCliente:btCliente", objCliente+":estado");
                alert('Los datos se guardaron correctamente.');
            } 
            else if (Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
                alert("Los Datos se eliminaron correctamente");
            }
            else if (Mensaje === 'NOEXISTEID')
            {   BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "id");
                ElementClear(Sufijo, "idCliente");
                alert(Datos);
            } 
            else if (Mensaje === 'EXCEPCION')
            {   BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
            return false;
    }          

    function KeyDown_Cliente(Evento, Sufijo, Elemento)
    {       if (Elemento.id == "idCliente" + Sufijo)  
            {   Params = Dependencia_II(Evento,"NUM");
                if (Params[0])
                {   BarButtonState(Sufijo, "Default");
                    ElementClear(Sufijo,clsCliente);    
                }   
                return Params[2];
            }
    }
