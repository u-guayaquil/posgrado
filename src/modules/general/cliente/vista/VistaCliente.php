<?php
        $Sufijo = '_Cliente';
         
        require_once('src/modules/general/cliente/controlador/ControlCliente.php');
        $ControlCliente = new ControlCliente($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlCliente,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlCliente,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlCliente,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlCliente,'CargaModalGridCliente'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlCliente,'ConsultaModalGridClienteByTX'));
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/general/cliente/js/JSCliente.js"></script>
         </head>
         <body>         
                <div class="FormBasic" style="width:670px">
                <div class="FormSectionMenu">              
                <?php  $ControlCliente->CargaBarButton($_GET['opcion']);
                       echo '<script type="text/javascript">';
                       echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                       echo '</script>';
                ?>
                </div>    
                <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                        <input type="hidden" id="Operacion<?php echo $Sufijo; ?>" name="Operacion<?php echo $Sufijo; ?>" value=""/>
                     <?php  $ControlCliente->CargaMantenimiento();  ?>
                     </form>
                </div>
                </div>
        </body>
        </html>