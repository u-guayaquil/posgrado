<?php
         $Sufijo = '_Parroquia';

         require_once('src/modules/general/parroquia/controlador/ControlParroquia.php');
         $ControlParroquia = new ControlParroquia($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlParroquia,'GuardaParroquia'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlParroquia,'ConsultaParroquiaByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlParroquia,'ConsultaParroquiaByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlParroquia,'EliminaParroquia'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlParroquia,'CargaModalGrid'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlParroquia,'ConsultaModalGridByTX')); 
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/general/parroquia/js/JSParroquia.js"></script>
         </head>
         <body>
         <div class="FormBasic" style="width:625px">
               <div class="FormSectionMenu">              
              <?php  $ControlParroquia->CargaParroquiaBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlParroquia->CargaParroquiaMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlParroquia->CargaParroquiaSearchGrid();  ?>
              </div>  
         </div>
         </body>
</html>
              

