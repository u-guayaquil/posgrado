<?php
        require_once("src/rules/general/servicio/ServicioParroquia.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");  
        require_once("src/rules/general/servicio/ServicioCiudad.php");
        require_once("src/modules/general/parroquia/render/RenderParroquia.php"); 
        require_once("src/modules/general/ciudad/render/RenderCiudad.php");

         class ControlParroquia
         {     private  $Sufijo; 
               private  $ServicioParroquia;
               private  $ServicioCiudad;
               private  $RenderParroquia;
               private  $RenderCiudad;
               
               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioParroquia = new ServicioParroquia();
                        $this->ServicioCiudad = new ServicioCiudad();
                        $this->RenderParroquia = new RenderParroquia($Sufijo);
                        $this->RenderCiudad = new RenderCiudad($Sufijo);
               }

               function CargaParroquiaBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaParroquiaMantenimiento()
               {        echo $this->RenderParroquia->CreaParroquiaMantenimiento();
               }

               function CargaParroquiaSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'orden' => 'r.descripcion');
                        $datoParroquia = $this->ServicioParroquia->BuscarParroquiaByDescripcion($prepareDQL);
                        echo $this->RenderParroquia->CreaParroquiaSearchGrid($datoParroquia);
               }         
               
               function ConsultaParroquiaByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $datoParroquia = $this->ServicioParroquia->BuscarParroquiaByID(array('id' => intval($id)));
                        return $this->RenderParroquia->MuestraParroquia($ajaxRespon,$datoParroquia);
               }

               function ConsultaParroquiaByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'orden' => 'r.descripcion');
                        $datoParroquia = $this->ServicioParroquia->BuscarParroquiaByDescripcion($prepareDQL);
                        return $this->RenderParroquia->MuestraParroquiaGrid($ajaxRespon,$datoParroquia);
               }

               function GuardaParroquia($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Parroquia  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioParroquia->GuardaDBParroquia($Parroquia);
                        if (is_numeric($id)){
                            $function = (empty($Parroquia->id) ? "MuestraParroquiaGuardado" : "MuestraParroquiaEditado");
                            $Datos = $this->ServicioParroquia->BuscarParroquiaByID(array('id' => intval($id)));
                            return $this->RenderParroquia->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderParroquia->MuestraParroquiaExcepcion($ajaxRespon,intval($id));
                        }
               }
               
                function EliminaParroquia($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioParroquia->DesactivaParroquia(intval($id));
                        $Datos = $this->ServicioParroquia->BuscarParroquiaByID(array('id' =>intval($id)));
                        return $this->RenderParroquia->MuestraParroquiaEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '','estado' =>1);
                        $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Buscar Ciudad";
                        $jsonModal['Carga'] = $this->RenderCiudad->CreaModalGridCiudad($Operacion,$Datos);
                        $jsonModal['Ancho'] = $this->RenderCiudad->WidthModalGrid();

                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
            
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                        return $this->RenderCiudad->MuestraModalGridCiudad($ajaxResp,$Operacion,$Datos);                                        
                }
         }

?>

