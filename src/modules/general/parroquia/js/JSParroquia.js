        function ButtonClick_Parroquia(Sufijo, Operacion)
        {   var objParroquia = "id:descripcion:estado:idciudad:txciudad:codigo";
            var frmParroquia = "descripcion:estado:idciudad:txciudad:codigo";

            if (Operacion == 'addNew')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, frmParroquia, "id:estado");
                ElementClear(Sufijo, objParroquia);
                BarButtonStateEnabled(Sufijo, "btciudad");
            } 
            else if (Operacion == 'addMod')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, frmParroquia, "id");
                BarButtonStateEnabled(Sufijo, "btciudad");
                if(ElementGetValue(Sufijo,"estado")==='0'){
                    ElementStatus(Sufijo, "estado", "");
                }else{
                    ElementStatus(Sufijo, "", "estado");
                }
            } 
            else if (Operacion == 'addDel')
            {   if (ElementGetValue(Sufijo, "estado") == 1)
                {   if (confirm("Desea inactivar este registro?"))
                    xajax_Elimina_Parroquia(ElementGetValue(Sufijo, "id"));
                } 
                else
                alert("El registro se encuentra inactivo");
            } 
            else if (Operacion == 'addSav')
            {   if (ElementValidateBeforeSave(Sufijo, frmParroquia))
                {   if (BarButtonState(Sufijo, "Inactive"))
                    {   var Forma = PrepareElements(Sufijo, objParroquia);
                        xajax_Guarda_Parroquia(JSON.stringify({Forma}));
                    }
                }
            } 
            else if (Operacion == 'addCan')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, "id", frmParroquia);
                ElementClear(Sufijo, objParroquia);
                BarButtonStateDisabled(Sufijo, "btciudad");
            } 
            else 
            xajax_CargaModal_Parroquia(Operacion);
            return false;
        }

        function SearchByElement_Parroquia(Sufijo, Elemento)
        {   Elemento.value = TrimElement(Elemento.value);
            if (Elemento.name == "id" + Sufijo)
            {   if (Elemento.value.length != 0)
                {   ContentFlag = document.getElementById("descripcion" + Sufijo);
                    if (ContentFlag.value.length == 0)
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        xajax_MuestraByID_Parroquia(Elemento.value);
                    }
                } 
                else
                BarButtonState(Sufijo, "Default");
            } 
            else if (Elemento.id === "FindCiudadTextBx" + Sufijo)
            {   xajax_BuscaModalByTX_Parroquia("btciudad", Elemento.value);
            }
            else
            xajax_MuestraByTX_Parroquia(Elemento.value);
        }

        function SearchGetData_Parroquia(Sufijo, Grilla)
        {   
            if (Grilla.cells[0].childNodes[0].nodeValue!="->")
            {   if (IsDisabled(Sufijo,"addSav"))
                {   BarButtonState(Sufijo, "Active");
                    document.getElementById("id" + Sufijo).value     = Grilla.cells[0].childNodes[0].nodeValue;
                    document.getElementById("codigo" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                    document.getElementById("descripcion" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                    document.getElementById("txciudad" + Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                    document.getElementById("idciudad" + Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
                    document.getElementById("estado" + Sufijo).value   = Grilla.cells[5].childNodes[0].nodeValue;
                }    
            }    
            return false;
        }

        function SearchCiudadGetData_Parroquia(Sufijo, DatosGrid)
        {   document.getElementById("idciudad" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txciudad" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
        }

        function XAJAXResponse_Parroquia(Sufijo, Mensaje, Datos)
        {   var objParroquia = "descripcion:estado:codigo:idciudad:txciudad";
            if (Mensaje === "GUARDADO")
            {   BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, "btciudad");
                ElementSetValue(Sufijo, "id", Datos);
                ElementStatus(Sufijo, "id", objParroquia);
                alert('Los datos se guardaron correctamente.');
            } 
            else if (Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
                alert("Los Datos se eliminaron correctamente");
            } 
            else if (Mensaje === 'NOEXISTEID')
            {   BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "id");
                alert(Datos);
            } 
            else if (Mensaje === 'EXCEPCION')
            {   BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
        }