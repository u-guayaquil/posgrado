<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioParroquia.php");
        require_once("src/rules/general/servicio/ServicioCiudad.php");
        require_once('src/libs/clases/ComboBox.php');        
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php"); 
        require_once('src/libs/clases/SearchInput.php');     
         
        class RenderParroquia
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 4; 
                }
              
                private function SearchGridValues()
                {       $Columns['Id'] = array('40px','center','');
                        $Columns['C&oacute;digo'] = array('55px','left','');
                        $Columns['Parroquia'] = array('300px','left','');
                        $Columns['Ciudad']    = array('180px','left','');
                        $Columns['idCiudad']  = array('0px','left','none');
                        $Columns['idEstado']  = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
                }
              
                function CreaParroquiaMantenimiento()
                {       $Search = new SearchInput($this->Sufijo);        
                        $Parroquia = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $Parroquia.= '       <tr height="30">';
                        $Parroquia.= '           <td class="Form-Label" style="width:15%">Id</td>';
                        $Parroquia.= '           <td class="Form-Label" style="width:56%">';
                        $Parroquia.= '               <input type="text" class="txt-input t02" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo:idciudad:txciudad\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Parroquia.= '           </td>';
                        $Parroquia.= '           <td class="Form-Label" style="width:14%"></td>';
                        $Parroquia.= '           <td class="Form-Label" style="width:15%">';
                        $Parroquia.= '           </td>';
                        $Parroquia.= '       </tr>';
                        $Parroquia.= '         <tr height="30">';
                        $Parroquia.= '             <td class="Form-Label">Parroquia</td>';
                        $Parroquia.= '             <td class="Form-Label">';
                        $Parroquia.= '                 <input type="text" class="txt-upper t13" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="60" disabled/>';
                        $Parroquia.= '             </td>';
                        $Parroquia.= '             <td class="Form-Label" >C&oacute;digo</td>';
                        $Parroquia.= '             <td class="Form-Label" >';
                        $Parroquia.= '                 <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="10" onKeyDown="return soloNumerosLetras(event); " disabled/>';
                        $Parroquia.= '             </td>';
                        $Parroquia.= '         </tr>';
                        $Parroquia.= '         <tr height="30">';
                        $Parroquia.= '             <td class="Form-Label">Ciudad</td>';
                        $Parroquia.= '             <td class="Form-Label">';
                        $Parroquia.=                   $Search->TextSch("ciudad","","")->Enabled(false)->Create("t07");
                        $Parroquia.= '             </td>';
                        $Parroquia.= '           <td class="Form-Label" style="width:14%">Estado</td>';
                        $Parroquia.= '           <td class="Form-Label" style="width:19%">';
                        $Parroquia.=                 $this->CreaComboEstado();
                        $Parroquia.= '           </td>';
                        $Parroquia.= '         </tr>';
                        $Parroquia.= '</table>';
                        return $Parroquia;
                }

                function CreaParroquiaSearchGrid($Datos)
                {      $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->ParroquiaGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
                }

                private function CreaComboEstado()
                {       $Select = new ComboBox($this->Sufijo);                        
                        $ServicioEstado = new ServicioEstado();
                        $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));                        
                        return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
                }
              
                function MuestraParroquia($Ajax,$Datos)
                {       foreach($Datos as $Dato)
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Dato['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));
                            $Ajax->Assign("txciudad".$this->Sufijo,"value", trim($Dato['ciudad']));                       
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                            $Ajax->Assign("idciudad".$this->Sufijo,"value", $Dato['idciudad']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaParroquia($Ajax,"NOEXISTEID","No existe una parroquia con el ID ingresado.");
                }
                
                function MuestraParroquiaGrid($Ajax,$Datos)
                {      $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->ParroquiaGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
                }        

                private function ParroquiaGridHTML($ObjSchGrid,$Datos)
                {       $Quiebre = "";
                        foreach ($Datos as $parroquia)
                        {       $id = str_pad($parroquia['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                if ($Quiebre != trim($parroquia['provincia'])){
                                    $Quiebre = trim($parroquia['provincia']);
                                    $ObjSchGrid->CreaSearchCellsDetalle('Q'.$id,'',"->");
                                    $ObjSchGrid->CreaSearchCellsDetalle('Q'.$id,'',"<b>".$Quiebre."</b>",8);
                                    $ObjSchGrid->CreaSearchRowsDetalle ('Q'.$id,"black");
                                }
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($parroquia['codigo']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($parroquia['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($parroquia['ciudad']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$parroquia['idciudad']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$parroquia['idestado']);
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($parroquia['idestado']==0 ? "red" : ""));
                        }   
                        return $ObjSchGrid->CreaSearchTableDetalle();
                }
               
                function MuestraParroquiaGuardado($Ajax,$Parroquia)
                {       if (count($Parroquia)>0)
                        {   $id = str_pad($Parroquia[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraParroquiaGrid($Ajax,$Parroquia);
                            return $this->RespuestaParroquia($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaParroquia($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraParroquiaEditado($Ajax,$Parroquia)
                {       foreach ($Parroquia as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraParroquiaRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaParroquia($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaParroquia($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
              
                function MuestraParroquiaEliminado($Ajax,$Parroquia)
                {       foreach ($Parroquia as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraParroquiaRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaParroquia($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->RespuestaParroquia($xAjax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraParroquiaRowGrid($Ajax,$Parroquia,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Parroquia['id'],$estado);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Parroquia['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Parroquia['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Parroquia['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Parroquia['ciudad']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$Parroquia['idciudad']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$Parroquia['idestado']);
                        return $Ajax;
                }

                function MuestraParroquiaExcepcion($Ajax,$Msg)
                {       return $this->RespuestaParroquia($Ajax,"EXCEPCION",$Msg);    
                }

                private function RespuestaParroquia($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
            
                /**** Modal ****/
                function CreaModalGridParroquia($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->ParroquiaGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }        
              
                function WidthModalGrid()
                {       return 610;
                }
                
                function MuestraModalGridParroquia($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->ParroquiaGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }        
            
        }
?>

