<?php    
        class RenderEstado
        {     private $Sufijo;

              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
              }
               
              function CreaListaEstado($Datos)
              {        $Lista = '<select class="sel-input s04" id="estado'.$this->Sufijo.'" name="estado'.$this->Sufijo.'" disabled>';
                                   foreach($Datos as $estado)
                                   {       $Lista.= '<option value='.$estado['id'].' '.($estado['id'] ==1 ? "selected" : "").'>'.$estado['descripcion'].'</option>';
                                   }
                       return $Lista.'</select>';
              }
        }
?>

