<?php
         $Sufijo = '_Provincia';

         require_once('src/modules/general/provincia/controlador/ControlProvincia.php');
         $ControlProvincia = new ControlProvincia($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlProvincia,'GuardaProvincia'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlProvincia,'MuestraProvinciaByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlProvincia,'MuestraProvinciaByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlProvincia,'EliminaProvincia'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlProvincia,'CargaModalGridProvincia'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlProvincia,'ConsultaModalGridProvinciaByTX')); 
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/general/provincia/js/JSProvincia.js"></script>
         </head>
         <body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlProvincia->CargaProvinciaBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlProvincia->CargaProvinciaMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlProvincia->CargaProvinciaSearchGrid();  ?>
              </div>  
         </div>
         </body>
</html>
              

