<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioProvincia.php");
        require_once('src/libs/clases/ComboBox.php');        
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderProvincia
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Fechas = new DateControl();
                    $this->Maxlen['id'] = 2; 
            }
              
            function CreaOpcionBarButton($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);
                    return $BarButton->CreaBarButton($Buttons);
            }
               
            function CreaProvinciaMantenimiento()
            {       $Search = new SearchInput($this->Sufijo); 
              
                    $Provincia = '<table class="Form-Frame" cellpadding="0" border="0">';
                    $Provincia.= '      <tr height="28">';
                    $Provincia.= '          <td class="Form-Label" style="width:20%">Id</td>';
                    $Provincia.= '          <td class="Form-Label" style="width:42%">';
                    $Provincia.= '              <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo:idpais:txpais\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Provincia.= '          </td>';
                    $Provincia.= '          <td class="Form-Label" style="width:18%"></td>';
                    $Provincia.= '          <td class="Form-Label" style="width:20%"></td>';
                    $Provincia.= '      </tr>';
                    $Provincia.= '      <tr height="28">';
                    $Provincia.= '           <td class="Form-Label">Provincia</td>';
                    $Provincia.= '           <td class="Form-Label">';                                                                                                              
                    $Provincia.= '               <input type="text" class="txt-upper t08" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="30" disabled/>';
                    $Provincia.= '           </td>';
                    $Provincia.= '           <td class="Form-Label" >C&oacute;digo</td>';
                    $Provincia.= '           <td class="Form-Label" >';
                    $Provincia.= '               <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="10" onKeyDown="return soloNumerosLetras(event); " disabled/>';
                    $Provincia.= '           </td>';
                    $Provincia.= '      </tr>';
                    $Provincia.= '      <tr height="28">';
                    $Provincia.= '          <td class="Form-Label">Pa&iacute;s</td>';
                    $Provincia.= '          <td class="Form-Label">';
                    $Provincia.=                $Search->TextSch("pais","","")->Enabled(false)->Create("t07");
                    $Provincia.= '          </td>';
                    $Provincia.= '      </tr>';
                    $Provincia.= '      <tr height="28">';
                    $Provincia.= '          <td class="Form-Label">Estado</td>';
                    $Provincia.= '          <td class="Form-Label">';
                    $Provincia.=                $this->CreaComboEstado();
                    $Provincia.= '          </td>';
                    $Provincia.= '      </tr>';
                    $Provincia.= '</table>';
                    return $Provincia;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']           = array('40px','center','');
                    $Columns['C&oacute;digo']= array('60px','left','');
                    $Columns['Provincia']    = array('200px','left','');
                    $Columns['Creaci&oacute;n'] = array('120px','center','');
                    $Columns['Estado']       = array('60px','left','');
                    $Columns['idPais']       = array('0px','left','none');
                    $Columns['Pais']         = array('0px','left','none');
                    $Columns['idEstado']     = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }

            function CreaProvinciaSearchGrid($Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->ProvinciaGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }
              
            private function ProvinciaGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $provincia)
                    {       $id = str_pad($provincia['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $fecreacion = $this->Fechas->changeFormatDate($provincia['fecreacion'],"DMY");
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($provincia['codigo']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($provincia['descripcion']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$provincia['txtestado']); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$provincia['idpais']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$provincia['pais']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$provincia['idestado']); 
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,($provincia['idestado']==0 ? "red" : ""));

                    }
                    return $ObjSchGrid->CreaSearchTableDetalle();
            }

            private function CreaComboEstado()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioEstado = new ServicioEstado();
                    $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
            }
              
            function MuestraProvincia($Ajax,$Datos)
            {       foreach ($Datos as $Dato) 
                    {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Dato['codigo']));
                        $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));
                        $Ajax->Assign("idpais".$this->Sufijo,"value", $Dato['idpais']);  
                        $Ajax->Assign("txpais".$this->Sufijo,"value", trim($Dato['pais']));
                        $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                        $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                        return $Ajax;
                    }
                    return $this->RespuestaProvincia($Ajax,"NOEXISTEID","No existe una Provincia con ese ID.");
            }
            
            function MuestraProvinciaGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->ProvinciaGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }   
               
            function MuestraProvinciaGuardado($Ajax,$Provincia)
            {       foreach ($Provincia as $datos) 
                    {   $id = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraProvinciaGrid($Ajax,$Provincia);
                        return $this->RespuestaProvincia($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaProvincia($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraProvinciaEditado($Ajax,$Provincia)
            {       foreach ($Provincia as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraProvinciaRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaProvincia($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaProvincia($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraProvinciaEliminado($Ajax,$Provincia)
            {       foreach ($Provincia as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraProvinciaRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaProvincia($xAjax,"ELIMINADO",$datos['idestado']);
                    }  
                    return $this->RespuestaProvincia($xAjax,"EXCEPCION","No se eliminó la información.");
            }
              
            function MuestraProvinciaRowGrid($Ajax,$Provincia,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($Provincia['id'],$estado);
                    $fecreacion = $this->Fechas->changeFormatDate($Provincia['fecreacion'],"DMY");
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Provincia['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Provincia['codigo']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Provincia['descripcion']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Provincia['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Provincia['idpais']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($Provincia['pais']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($Provincia['idestado']));
                    return $Ajax;
            }

            function MuestraProvinciaExcepcion($Ajax,$Msg)
            {       return $this->RespuestaProvincia($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaProvincia($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
                
            /*** MODAL ***/
            private function SearchGridModalConfig()
            {       $Columns['Id']      = array('40px','center','');
                    $Columns['Codigo']  = array('45px','center','');
                    $Columns['Provincia'] = array('200px','left','');
                    $Columns['Pais']      = array('120px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
            }
            
            function WidthModalGrid()
            {       return 435;
            }
            
            function CreaModalGridProvincia($Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->GridDataHTMLModalProvincia($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
                
            function MuestraModalGridProvincia($Ajax,$Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->GridDataHTMLModalProvincia($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }        

            private function GridDataHTMLModalProvincia($Grid,$Datos)
            {       foreach ($Datos as $Dato)
                    {       $id = str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['codigo']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['descripcion']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['pais']));
                            $Grid->CreaSearchRowsDetalle ($id,"black");
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
        }
?>

