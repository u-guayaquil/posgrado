<?php
         require_once("src/rules/general/servicio/ServicioProvincia.php");
         require_once("src/rules/general/servicio/ServicioPais.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/general/provincia/render/RenderProvincia.php");
         require_once("src/modules/general/pais/render/RenderPais.php");

         class ControlProvincia
         {     private  $Sufijo; 
               private  $ServicioProvincia;
               private  $RenderProvincia;
               private  $ServicioPais;
               private  $RenderPais;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioProvincia = new ServicioProvincia();
                        $this->RenderProvincia = new RenderProvincia($Sufijo);
                        $this->ServicioPais = new ServicioPais();
                        $this->RenderPais = new RenderPais($Sufijo);
               }

               function CargaProvinciaBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaProvinciaMantenimiento()
               {        echo $this->RenderProvincia->CreaProvinciaMantenimiento();
               }

               function CargaProvinciaSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoProvincia = $this->ServicioProvincia->BuscarProvinciaByDescripcion($prepareDQL);
                        echo $this->RenderProvincia->CreaProvinciaSearchGrid($datoProvincia);
               }         
               
               function MuestraProvinciaByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $datoProvincia = $this->ServicioProvincia->BuscarProvinciaByID(array('id' => intval($id)));
                        return $this->RenderProvincia->MuestraProvincia($ajaxRespon,$datoProvincia);
               }

               function MuestraProvinciaByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoProvincia = $this->ServicioProvincia->BuscarProvinciaByDescripcion($prepareDQL);
                        return $this->RenderProvincia->MuestraProvinciaGrid($ajaxRespon,$datoProvincia);
               }

               function GuardaProvincia($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Provincia  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioProvincia->GuardaDBProvincia($Provincia);
                        if (is_numeric($id)){
                            $function = (empty($Provincia->id) ? "MuestraProvinciaGuardado" : "MuestraProvinciaEditado");
                            $Datos = $this->ServicioProvincia->BuscarProvinciaByID(array('id' => intval($id)));
                            return $this->RenderProvincia->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderProvincia->MuestraProvinciaExcepcion($ajaxRespon,intval($id));
                        }
               }
               
                function EliminaProvincia($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioProvincia->DesactivaProvincia(intval($id));
                        $Datos = $this->ServicioProvincia->BuscarProvinciaByID(array('id' => intval($id)));
                        return $this->RenderProvincia->MuestraProvinciaEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridProvincia($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioPais->BuscarByDescripcion($prepareDQL);                            
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Pais";
                        $jsonModal['Carga'] = $this->RenderPais->CreaModalGrid($Operacion,$Datos);                        
                        $jsonModal['Ancho'] = $this->RenderPais->WidthModalGrid();
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalProvinciaGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);                        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioPais->BuscarByDescripcion($prepareDQL);
                        return $this->RenderPais->MuestraModalGrid($ajaxResp,$Operacion,$Datos);     
                }
                
         }

?>

