        function ButtonClick_Provincia(Sufijo, Operacion)
        {       var objProvincia = "id:descripcion:estado:codigo:idpais:txpais";
                var frmProvincia = "descripcion:estado:codigo:idpais:txpais";

                if (Operacion == 'addNew')
                {   BarButtonState(Sufijo, Operacion);
                    ElementStatus(Sufijo, frmProvincia, "id:estado");
                    ElementClear(Sufijo, objProvincia);
                    BarButtonStateEnabled(Sufijo, "btpais");
                } 
                else if (Operacion == 'addMod')
                {   BarButtonState(Sufijo, Operacion);
                    ElementStatus(Sufijo, frmProvincia, "id");
                    BarButtonStateEnabled(Sufijo, "btpais");
                    if(ElementGetValue(Sufijo,"estado")==='0'){
                        ElementStatus(Sufijo, "estado", "");
                    }else{
                        ElementStatus(Sufijo, "", "estado");
                    }
                } 
                else if (Operacion == 'addDel')
                {   if (ElementGetValue(Sufijo, "estado") == 1)
                    {   if (confirm("Desea inactivar este registro?"))
                        xajax_Elimina_Provincia(ElementGetValue(Sufijo, "id"));
                    } 
                    else
                    alert("El registro se encuentra inactivo");
                } 
                else if (Operacion == 'addSav')
                {   if (ElementValidateBeforeSave(Sufijo, frmProvincia))
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        {   var Forma = PrepareElements(Sufijo, objProvincia);
                            xajax_Guarda_Provincia(JSON.stringify({Forma}));
                        }
                    }
                } 
                else if (Operacion == 'addCan')
                {   BarButtonState(Sufijo, Operacion);
                    ElementStatus(Sufijo, "id", frmProvincia);
                    ElementClear(Sufijo, objProvincia);
                    BarButtonStateDisabled(Sufijo, "btpais");
                }
                else
                xajax_CargaModal_Provincia(Operacion);
                return false;
        }

        function SearchByElement_Provincia(Sufijo, Elemento)
        {       Elemento.value = TrimElement(Elemento.value);
                if (Elemento.name == "id" + Sufijo)
                {   if (Elemento.value.length != 0)
                    {   ContentFlag = document.getElementById("descripcion" + Sufijo);
                        if (ContentFlag.value.length == 0)
                        {   if (BarButtonState(Sufijo, "Inactive"))
                            xajax_MuestraByID_Provincia(Elemento.value);
                        }
                    }
                    else
                    BarButtonState(Sufijo, "Default");                        
                } 
                else
                xajax_MuestraByTX_Provincia(Elemento.value);
        }

        function SearchGetData_Provincia(Sufijo, Grilla)
        {       if (IsDisabled(Sufijo,"addSav"))
                {   BarButtonState(Sufijo, "Active");
                    document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                    document.getElementById("codigo" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                    document.getElementById("descripcion" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                    document.getElementById("idpais" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;    
                    document.getElementById("txpais" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                    document.getElementById("estado" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                }    
                return false;
        }

        function SearchPaisGetData_Provincia(Sufijo, DatosGrid)
        {       document.getElementById("idpais" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                document.getElementById("txpais" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                cerrar();
                return false;
        }

        function XAJAXResponse_Provincia(Sufijo, Mensaje, Datos)
        {       var objProvincia = "descripcion:estado:codigo:idpais:txpais";
                if (Mensaje === "GUARDADO")
                {   BarButtonState(Sufijo, "Active");
                    BarButtonStateDisabled(Sufijo, "btpais");
                    ElementSetValue(Sufijo, "id", Datos);
                    ElementStatus(Sufijo, "id", objProvincia);
                    alert('Los datos se guardaron correctamente.');
                } 
                else if (Mensaje === 'ELIMINADO')
                {   ElementSetValue(Sufijo, "estado", Datos);
                    alert("Los Datos se eliminaron correctamente");
                } 
                else if (Mensaje === 'NOEXISTEID')
                {   BarButtonState(Sufijo, 'Default');
                    ElementClear(Sufijo, "id");
                    alert(Datos);
                }
                else if (Mensaje === 'EXCEPCION')
                {   BarButtonState(Sufijo, "addNew");
                    alert(Datos);
                }
        }