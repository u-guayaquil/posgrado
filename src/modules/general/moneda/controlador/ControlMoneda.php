<?php
         require_once("src/rules/general/servicio/ServicioMoneda.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/general/moneda/render/RenderMoneda.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlMoneda
         {     private  $Sufijo; 
               private  $ServicioMoneda;
               private  $RenderMoneda;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioMoneda = new ServicioMoneda();
                        $this->RenderMoneda = new RenderMoneda($Sufijo);
               }

               function CargaMonedaBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaMonedaMantenimiento()
               {        echo $this->RenderMoneda->CreaMonedaMantenimiento();
               }

               function CargaMonedaSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoMoneda = $this->ServicioMoneda->BuscarMonedaByDescripcion($prepareDQL);
                        echo $this->RenderMoneda->CreaMonedaSearchGrid($datoMoneda);
               }         
               
               function MuestraMonedaByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoMoneda = $this->ServicioMoneda->BuscarMonedaByID($prepareDQL);
                        return $this->RenderMoneda->MuestraMoneda($ajaxRespon,$datoMoneda);
               }

               function MuestraMonedaByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoMoneda = $this->ServicioMoneda->BuscarMonedaByDescripcion($prepareDQL);
                        return $this->RenderMoneda->MuestraMonedaGrid($ajaxRespon,$datoMoneda);
               }

               function GuardaMoneda($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Moneda  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioMoneda->GuardaDBMoneda($Moneda);
                        if (is_numeric($id)){
                            $function = (empty($Moneda->id) ? "MuestraMonedaGuardado" : "MuestraMonedaEditado");
                            $prepareDQL = array('id' => intval($id),'limite' => 50,'inicio' => 0);
                            $Datos = $this->ServicioMoneda->BuscarMonedaByID($prepareDQL);
                            return $this->RenderMoneda->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderMoneda->MuestraMonedaExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaMoneda($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioMoneda->DesactivaMoneda(intval($id));
                        $Datos = $this->ServicioMoneda->BuscarMonedaByDescripcion($prepareDQL);
                        return $this->RenderMoneda->MuestraMonedaEliminado($ajaxRespon,$Datos);
                }
         }

?>

