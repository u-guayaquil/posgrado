<?php
         $Sufijo = '_Moneda';

         require_once('src/modules/general/moneda/controlador/ControlMoneda.php');
         $ControlMoneda = new ControlMoneda($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlMoneda,'GuardaMoneda'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlMoneda,'MuestraMonedaByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlMoneda,'MuestraMonedaByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlMoneda,'EliminaMoneda'));
         $xajax->processRequest();

?>      <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript">
                 function ButtonClick_Moneda(Sufijo,Operacion)
                 {        var objMoneda = "id:descripcion:estado:cambio:codigo";
                          var frmMoneda = "descripcion:estado:cambio:codigo";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,frmMoneda,"id:estado");
                              ElementClear(Sufijo,objMoneda);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,frmMoneda,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_Moneda(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,frmMoneda))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objMoneda);
                                       xajax_Guarda_Moneda(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmMoneda);
                               ElementClear(Sufijo,objMoneda);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_Moneda(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_Moneda(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {      xajax_MuestraByTX_Moneda(Elemento.value);  
                          }    
                 }
                 
                 function SearchGetData_Moneda(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")){
                                BarButtonState(Sufijo,"Active");

                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                                document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                                document.getElementById("descripcion"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                                document.getElementById("cambio"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;                          
                                document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue; 
                          }
                          return false;
                 }
                 
                 function XAJAXResponse_Moneda(Sufijo,Mensaje,Datos)
                 {       
                        var objMoneda = "descripcion:estado:cambio:codigo";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objMoneda);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
            </script>
            </head>
            <body>
                <div class="FormBasic" style="width:535px">
                      <div class="FormSectionMenu">              
                     <?php  $ControlMoneda->CargaMonedaBarButton($_GET['opcion']);
                            echo '<script type="text/javascript">';
                            echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                            echo '</script>';
                     ?>
                     </div>    
                     <div class="FormSectionData">              
                         <form id="<?php echo 'Form'.$Sufijo; ?>">
                          <?php  $ControlMoneda->CargaMonedaMantenimiento();  ?>
                          </form>
                     </div>    
                     <div class="FormSectionGrid">          
                     <?php  $ControlMoneda->CargaMonedaSearchGrid();  ?>
                     </div>  
                </div>
            </body>
            </html>
              

