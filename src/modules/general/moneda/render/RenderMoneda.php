<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderMoneda
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
               
              function CreaMonedaMantenimiento()
              {                          
                       $Moneda = '<table class="Form-Frame" cellpadding="0">';
                       $Moneda.= '       <tr height="30">';
                       $Moneda.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $Moneda.= '             <td class="Form-Label" style="width:75%">';
                       $Moneda.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo:cambio\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $Moneda.= '             </td>';
                       $Moneda.= '         </tr>';
                       $Moneda.= '         <tr height="30">';
                       $Moneda.= '             <td class="Form-Label" style="width:25%">C&oacute;digo</td>';
                       $Moneda.= '             <td class="Form-Label" style="width:75%">';
                       $Moneda.= '                 <input type="text" class="txt-upper t07" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                       $Moneda.= '             </td>';
                       $Moneda.= '         </tr>';
                       $Moneda.= '         <tr height="30">';
                       $Moneda.= '             <td class="Form-Label" style="width:25%">Moneda</td>';
                       $Moneda.= '             <td class="Form-Label" style="width:75%">';
                       $Moneda.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Moneda.= '             </td>';
                       $Moneda.= '         </tr>';
                       $Moneda.= '         <tr height="30">';
                       $Moneda.= '             <td class="Form-Label" style="width:25%">Cambio</td>';
                       $Moneda.= '             <td class="Form-Label" style="width:75%">';
                       $Moneda.= '                 <input type="text" class="txt-upper t07" id="cambio'.$this->Sufijo.'" name="cambio'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Moneda.= '             </td>';
                       $Moneda.= '         </tr>';
                       $Moneda.= '         <tr height="30">';
                       $Moneda.= '             <td class="Form-Label">Estado</td>';
                       $Moneda.= '             <td class="Form-Label">';
                                                   $Moneda.= $this->CreaComboEstado();
                       $Moneda.= '             </td>';
                       $Moneda.= '         </tr>';
                       $Moneda.= '</table>';
                       return $Moneda;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo']   = array('60px','left','');
                        $Columns['Descripci&oacute;n'] = array('100px','left','');
                        $Columns['Cambio'] = array('80px','left','');
                        $Columns['Creacion']   = array('130px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaMonedaSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->MonedaGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }        

              private function MonedaGridHTML($ObjSchGrid,$Datos)
              {   
                  foreach ($Datos as $moneda)
                      {        $id = str_pad($moneda['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($moneda['codigo']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($moneda['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($moneda['cambio']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$moneda['fecreacion']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$moneda['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$moneda['idestado']); 
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($moneda['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function CreaListaMoneda($Datos)
              {        $Lista = '<select class="sel-input s04" id="moneda'.$this->Sufijo.'" name="moneda'.$this->Sufijo.'" disabled>';                     
                       foreach($Datos as $moneda)
                                   {       $Lista.= '<option value='.$moneda['id'].' '.($moneda['id'] ==1 ? "selected" : "").'>'.$moneda['descripcion'].'</option>';
                                   }
                       return $Lista.'</select>';
              }
              
              function MuestraMoneda($Ajax,$Datos)
              {         if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));
                            $Ajax->Assign("cambio".$this->Sufijo,"value", trim($Datos[0]['cambio']));                       
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");

                            return $Ajax;
                       }else{
                            return $this->RespuestaMoneda($Ajax,"NOEXISTEID","No existe una Moneda con ese ID.");
                       }
              }
              function MuestraMonedaGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->MonedaGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }
               
              function MuestraMonedaGuardado($Ajax,$Moneda)
              {       if (count($Moneda)>0)
                        {   $id = str_pad($Moneda[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraMonedaGrid($Ajax,$Moneda);
                            return $this->RespuestaMoneda($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaMoneda($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraMonedaEditado($Ajax,$Moneda)
              {       foreach ($Moneda as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraMonedaRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaMoneda($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaMoneda($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraMonedaEliminado($Ajax,$Moneda)
              {        foreach ($Moneda as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraMonedaRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaMoneda($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaMoneda($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraMonedaRowGrid($Ajax,$Moneda,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Moneda['id'],$estado);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Moneda['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Moneda['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Moneda['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Moneda['cambio']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Moneda['fecreacion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Moneda['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($Moneda['idestado']));
                        return $Ajax;
                }

              function MuestraMonedaExcepcion($Ajax,$Msg)
                {       return $this->RespuestaMoneda($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaMoneda($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

