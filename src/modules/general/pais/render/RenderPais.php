<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderPais
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 2; 
                }
              
                function CreaOpcionBarButton($Buttons)
                {       $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
                }
               
                function CreaMantenimiento()
                {        
                        $Pais = '<table class="Form-Frame" cellpadding="0" border="0">';
                        $Pais.= '       <tr height="28">';
                        $Pais.= '           <td class="Form-Label" style="width:20%">Id</td>';
                        $Pais.= '           <td class="Form-Label" style="width:42%">';
                        $Pais.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo:abreviatura\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Pais.= '           </td>';
                        $Pais.= '           <td class="Form-Label" style="width:20%"></td>';
                        $Pais.= '           <td class="Form-Label" style="width:18%"></td>';
                        $Pais.= '         </tr>';
                        $Pais.= '         <tr height="28">';
                        $Pais.= '             <td class="Form-Label">Pais</td>';
                        $Pais.= '             <td class="Form-Label">';
                        $Pais.= '                 <input type="text" class="txt-upper t08" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                        $Pais.= '             </td>';
                        $Pais.= '             <td class="Form-Label">C&oacute;digo</td>';
                        $Pais.= '             <td class="Form-Label">';
                        $Pais.= '                 <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="10" onKeyDown="return soloNumerosLetras(event); " disabled/>';
                        $Pais.= '             </td>';
                        $Pais.= '         </tr>';
                        $Pais.= '         <tr height="28">';
                        $Pais.= '             <td class="Form-Label">Siglas</td>';
                        $Pais.= '             <td class="Form-Label">';
                        $Pais.= '                 <input type="text" class="txt-upper t03" id="abreviatura'.$this->Sufijo.'" name="abreviatura'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                        $Pais.= '             </td>';
                        $Pais.= '         </tr>';
                        $Pais.= '         <tr height="28">';
                        $Pais.= '             <td class="Form-Label">Estado</td>';
                        $Pais.= '             <td class="Form-Label">';
                        $Pais.=                   $this->CreaComboEstado();
                        $Pais.= '             </td>';
                        $Pais.= '         </tr>';
                        $Pais.= '</table>';
                        return $Pais;
                }
              
                private function SearchGridValues()
                {       $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo']   = array('60px','left','');
                        $Columns['Pais'] = array('150px','left','');
                        $Columns['Siglas'] = array('50px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left','');
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
                }

                function CreaSearchGrid($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->GridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }
              
                private function GridHTML($ObjSchGrid,$Datos)
                {       foreach ($Datos as $pais)
                        {       $id = str_pad($pais['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $fecreacion = $this->Fechas->changeFormatDate($pais['fecreacion'],"DMY");
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($pais['codigo']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($pais['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($pais['abreviatura']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$pais['txtestado']);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$pais['idestado']);                               
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($pais['idestado']==0 ? "red" : ""));
                        }
                        return $ObjSchGrid->CreaSearchTableDetalle();
                }

                private function CreaComboEstado()
                {       $Select = new ComboBox($this->Sufijo);                        
                        $ServicioEstado = new ServicioEstado();
                        $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                        return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
                }
              
                function MuestraDatos($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Dato['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));
                            $Ajax->Assign("abreviatura".$this->Sufijo,"value", trim($Dato['abreviatura']));                       
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->Respuesta($Ajax,"NOEXISTEID","No existe un Pais con ese ID.");
                }
                
                function MuestraDatosGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->GridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }                 
              
                function MuestraDatoGuardado($Ajax,$Pais)
                {       foreach ($Pais as $Dato)
                        {   $id = str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraDatosGrid($Ajax,$Pais);
                            return $this->Respuesta($xAjax,"GUARDADO",$id);
                        }
                        return $this->Respuesta($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraDatoEditado($Ajax,$Pais)
                {       foreach ($Pais as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->Respuesta($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->Respuesta($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
              
                function MuestraDatoEliminado($Ajax,$Pais)
                {       foreach ($Pais as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->Respuesta($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->Respuesta($xAjax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraRowGrid($Ajax,$Pais,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Pais['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($Pais['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Pais['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Pais['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Pais['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Pais['abreviatura']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Pais['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($Pais['idestado']));
                        return $Ajax;
                }

                function MuestraExcepcion($Ajax,$Msg)
                {       return $this->Respuesta($Ajax,"EXCEPCION",$Msg);    
                }

                private function Respuesta($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
              /*** MODAL ***/
                
                private function SearchGridModalConfig()
                {       $Columns['Id']      = array('40px','center','');
                        $Columns['Codigo']  = array('45px','center','');
                        $Columns['Descripcion'] = array('150px','left','');
                        $Columns['Siglas']      = array('50px','left','');
                        $Columns['Creacion']    = array('120px','center','');
                        $Columns['Estado']      = array('70px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }
                
                function WidthModalGrid()
                {       return 511;
                }
                
                function CreaModalGrid($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModal($Grid,$Datos)
                {       foreach ($Datos as $Dato)
                        {       $id = str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $fecreacion = $this->Fechas->changeFormatDate($Dato['fecreacion'],"DMY");
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['codigo']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['descripcion']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['abreviatura']));
                                $Grid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                                $Grid->CreaSearchCellsDetalle($id,'',$Dato['txtestado']);
                                $Grid->CreaSearchRowsDetalle ($id,"black");
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
        }
?>

