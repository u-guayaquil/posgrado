<?php
        $Sufijo = '_Pais';
        require_once('src/modules/general/pais/controlador/ControlPais.php');
        $ControlPais = new ControlPais($Sufijo);
         
        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlPais,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlPais,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX'.$Sufijo, $ControlPais,'ConsultaByTX'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlPais,'Elimina'));
        $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript">
                function ButtonClick_Pais(Sufijo,Operacion)
                {       var objPais = "id:descripcion:estado:abreviatura:codigo";
                        var frmPais = "descripcion:estado:abreviatura:codigo";
                     
                        if (Operacion=='addNew')
                        {   BarButtonState(Sufijo,Operacion);
                            ElementStatus(Sufijo,frmPais,"id:estado");
                            ElementClear(Sufijo,objPais);
                        }
                        else if (Operacion=='addMod')
                        {   BarButtonState(Sufijo,Operacion);
                            ElementStatus(Sufijo,frmPais,"id");
                            if(ElementGetValue(Sufijo,"estado")==='0'){
                                ElementStatus(Sufijo, "estado", "");
                            }else{
                                ElementStatus(Sufijo, "", "estado");
                            }
                        }
                        else if (Operacion=='addDel')
                        {   if (ElementGetValue(Sufijo,"estado")==1)
                            {   if (confirm("Desea inactivar este registro?"))
                                xajax_Elimina_Pais(ElementGetValue(Sufijo,"id"));
                            }
                            else
                            alert("El registro se encuentra inactivo");
                        }
                        else if (Operacion=='addSav')
                        {   if (ElementValidateBeforeSave(Sufijo,frmPais))
                            {   if (BarButtonState(Sufijo,"Inactive"))
                                {   var Forma = PrepareElements(Sufijo,objPais);
                                    xajax_Guarda_Pais(JSON.stringify({Forma}));
                                }
                            }
                        }
                        else if (Operacion=='addCan')
                        {   BarButtonState(Sufijo,Operacion);
                            ElementStatus(Sufijo,"id",frmPais);
                            ElementClear(Sufijo,objPais);
                        }
                        return false;
                }

                function SearchByElement_Pais(Sufijo,Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    xajax_BuscaByID_Pais(Elemento.value);
                                }
                            }
                            else
                            BarButtonState(Sufijo,"Default");
                        }
                        else
                        xajax_BuscaByTX_Pais(Elemento.value);
                }
                 
                function SearchGetData_Pais(Sufijo,Grilla)
                {       if (IsDisabled(Sufijo,"addSav"))
                        {   BarButtonState(Sufijo,"Active");
                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("descripcion"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                            document.getElementById("abreviatura"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;                          
                            document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                        }    
                        return false;
                }
                 
                function XAJAXResponse_Pais(Sufijo,Mensaje,Datos)
                {       var objPais = "descripcion:estado:codigo:abreviatura";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objPais);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                }
                 
            </script>
            </head>
            <body>
            <div class="FormBasic" style="width:535px">
                    <div class="FormSectionMenu">              
                    <?php  $ControlPais->CargaBarButton($_GET['opcion']);
                            echo '<script type="text/javascript">';
                            echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                            echo '</script>';
                    ?>
            </div>    
                    <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlPais->CargaMantenimiento();  ?>
                    </form>
            </div>    
                    <div class="FormSectionGrid">          
                    <?php  $ControlPais->CargaSearchGrid();  ?>
                    </div>  
            </div>
            </body>
</html>     

