<?php
        require_once("src/rules/general/servicio/ServicioPais.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
        require_once("src/modules/general/pais/render/RenderPais.php");

         class ControlPais
         {     private  $Sufijo; 
               private  $ServicioPais;
               private  $RenderPais;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioPais = new ServicioPais();
                        $this->RenderPais = new RenderPais($Sufijo);
               }

               function CargaBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaMantenimiento()
               {        echo $this->RenderPais->CreaMantenimiento();
               }

               function CargaSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoPais = $this->ServicioPais->BuscarByDescripcion($prepareDQL);
                        echo $this->RenderPais->CreaSearchGrid($datoPais);
               }         
               
               function ConsultaByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $datoPais = $this->ServicioPais->BuscarByID(array('id' => intval($id)));
                        return $this->RenderPais->MuestraDatos($ajaxRespon,$datoPais);
               }

               function ConsultaByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoPais = $this->ServicioPais->BuscarByDescripcion($prepareDQL);
                        return $this->RenderPais->MuestraDatosGrid($ajaxRespon,$datoPais);
               }

               function Guarda($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Pais  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioPais->GuardaDB($Pais);
                        if (is_numeric($id)){
                            $function = (empty($Pais->id) ? "MuestraDatoGuardado" : "MuestraDatoEditado");
                            $prepareDQL = array('id' => intval($id),'limite' => 50,'inicio' => 0);
                            $Datos = $this->ServicioPais->BuscarByID($prepareDQL);
                            return $this->RenderPais->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderPais->MuestraExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function Elimina($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioPais->Desactiva(intval($id));
                        $Datos = $this->ServicioPais->BuscarByID($prepareDQL);
                        return $this->RenderPais->MuestraDatoEliminado($ajaxRespon,$Datos);
                }
         }

?>

