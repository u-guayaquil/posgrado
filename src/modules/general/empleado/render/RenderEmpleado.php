<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/rules/general/servicio/ServicioEstadoCivil.php');
        require_once('src/rules/nomina/servicio/ServicioProfesion.php');
        require_once('src/rules/general/servicio/ServicioSucursal.php');
        require_once('src/rules/caja/servicio/ServicioMedios.php');
        require_once('src/rules/caja/servicio/ServicioSubTipo.php');
        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderEmpleado
        {       private $Sufijo;
                private $Select;
                private $Search;
                private $Maxlen;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;  
                        $this->Select = new ComboBox($Sufijo);
                        $this->Search = new SearchInput($Sufijo);
                        $this->Maxlen = 4;                     
                }
              
                function CreaOpcionBarButton($Buttons)
                {       $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
                }
               
                function CreaMantenimiento()
                {       $ServicioEstadoCivil = new ServicioEstadoCivil();
                        $datosEstadoCivil = $ServicioEstadoCivil->BuscarEstadoCivil(array());
                        $ServicioProfesion = new ServicioProfesion();
                        $datosProfesion = $ServicioProfesion->BuscarProfesionByDescripcion(array('limite'=>50));
                        $ServicioSucursal = new ServicioSucursal();
                        $datosSucursal = $ServicioSucursal->BuscarSucursalByDescripcion(array('limite' => 50,'inicio' => 0,'texto' => ''));
                        $ServicioMedios = new ServicioMedios();
                        $datosBanco = $ServicioMedios->BuscarMedioByDescripcion(array('oper'=>'1,3','estado'=>1,'tipo'=>'2'));
                        $ServicioSubTipo = new ServicioSubTipo();
                        $datoCtaTipo = $ServicioSubTipo->BuscarSubTipoTodos(array('tipo'=>'2'));
                        
                        $Empleado = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label" style="width:20%">Id</td>';
                        $Empleado.= '            <td class="Form-Label" style="width:33%">';
                        $Empleado.=                  $this->Search->TextSch("empleado","","")->Enabled(true)->MaxLength($this->Maxlen)->ReadOnly(false)->Create("t02");
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label" style="width:18%">Estado</td>';
                        $Empleado.= '            <td class="Form-Label" style="width:29%">';
                        $Empleado.=                  $this->CreaComboEstadoByArray(array(0,1));
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Nombre</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-upper t09" id="nombre'.$this->Sufijo.'" name="nombre'.$this->Sufijo.'" value="" maxlength="30" disabled/>';
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Fecha nacimiento</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-center t03" id="nacio'.$this->Sufijo.'" name="nacio'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Apellido</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-upper t09" id="apellido'.$this->Sufijo.'" name="apellido'.$this->Sufijo.'" value="" maxlength="30" disabled/>';                        
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Estado civil</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Select->Combo('estadocivil',$datosEstadoCivil)->Fields('id', 'descripcion')->Create("s03");
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Sexo</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->CreaComboSexo("s04");
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Cédula</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-input t03" id="cedula'.$this->Sufijo.'" name="cedula'.$this->Sufijo.'" value="" maxlength="10" onKeyDown="return soloNumeros(event); " disabled/>';                        
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label" colspan="4"><u>Datos Generales:</u></td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Dirección</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-upper t09" id="direccion'.$this->Sufijo.'" name="direccion'.$this->Sufijo.'" value="" maxlength="150" disabled/>';
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Ciudad</td>'; 
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Search->TextSch("ciudad","","")->Enabled(false)->Create("t07");
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Email</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-upper t09" id="email'.$this->Sufijo.'" name="email'.$this->Sufijo.'" value="" maxlength="60" disabled/>';                        
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Teléfono fijo</td>';                        
                        $Empleado.= '            <td class="Form-Label">';                        
                        $Empleado.= '                <input type="text" class="txt-input t02_5" id="fijo'.$this->Sufijo.'"  name="fijo'.$this->Sufijo.'"  value="" maxlength="10" onKeyDown="return soloNumeros(event);" disabled/>';
                        $Empleado.= '                <div style="border:0px dotted #000; width:54px  ;display: inline-block">Movil</div>';
                        $Empleado.= '                <input type="text" class="txt-input t02_5" id="movil'.$this->Sufijo.'" name="movil'.$this->Sufijo.'" value="" maxlength="10" onKeyDown="return soloNumeros(event);" disabled/>';
                        $Empleado.= '            </td>';                        
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Beneficiario</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-upper t09" id="beneficia'.$this->Sufijo.'" name="beneficia'.$this->Sufijo.'" value="" maxlength="60" disabled/>';                        
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Profesión</td>';                        
                        $Empleado.= '            <td class="Form-Label">';                        
                        $Empleado.=                  $this->Select->Combo('profesion',$datosProfesion)->Fields('id', 'descripcion')->Create("s08");
                        $Empleado.= '            </td>';                        
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Firma</td>';
                        $Empleado.= '            <td class="Form-Label" colspan="3">';
                        $Empleado.=                  $this->Search->TextSch("firma","","")->Enabled(false)->Create("t08");
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label" colspan="4"><u>Datos Contractuales:</u></td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Contrato</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->CreaComboTipoEmpleado("s04");
                        $Empleado.= '                #';
                        $Empleado.= '                <input type="text" class="txt-input t04" id="contrato'.$this->Sufijo.'" name="contrato'.$this->Sufijo.'" value="" maxlength="20" onKeyDown="return soloNumeros(event); " disabled/>';
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Empleado desde</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-center t02_5" id="ingreso'.$this->Sufijo.'" name="ingreso'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                        $Empleado.= '                <div style="border:0px dotted #000; width:54px  ;display: inline-block">Hasta</div>';
                        $Empleado.= '                <input type="text" class="txt-center t02_5" id="salida'.$this->Sufijo.'"  name="salida'.$this->Sufijo.'"  value="" maxlength="10" disabled/>';
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Departamento</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Search->TextSch("departamento","","")->Enabled(false)->Create("t08");
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Banco</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Select->Combo('banco',$datosBanco)->Fields('id', 'descripcion')->Create("s08");
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Cargo</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Search->TextSch("cargo","","")->Enabled(false)->Create("t08");
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Cta. tipo</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Select->Combo('ctatipo',$datoCtaTipo)->Fields('id', 'descripcion')->Create("s08");
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Jefe inmediato</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Search->TextSch("jefe","","")->Enabled(false)->Create("t08");
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Cta. número</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-input t02_5" id="ctanum'.$this->Sufijo.'" name="ctanum'.$this->Sufijo.'" value="" maxlength="15" onKeyDown="return soloNumeros(event); " disabled/>';                                        
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Plantilla rol</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Search->TextSch("plantilla","","")->Enabled(false)->Create("t08");
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Hora de entrada</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-right t02_5" id="entrada'.$this->Sufijo.'" name="entrada'.$this->Sufijo.'" value="" maxlength="8" onKeyDown="return soloNumeros(event); " disabled/>';                    
                        $Empleado.= '                <div style="border:0px dotted #000; width:54px  ;display: inline-block">Salida</div>';
                        $Empleado.= '                <input type="text" class="txt-right t02_5" id="hsalida'.$this->Sufijo.'" name="hsalida'.$this->Sufijo.'" value="" maxlength="8" onKeyDown="return soloNumeros(event); " disabled/>';                                                            
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Sucursal</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Select->Combo('sucursal',$datosSucursal)->Fields('id', 'descripcion')->Create("s09");                                                                        
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">% multa atraso</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.= '                <input type="text" class="txt-right t02_5" id="multa'.$this->Sufijo.'" name="multa'.$this->Sufijo.'"  value="" maxlength="15" onKeyDown="return soloFloat(event,this,2); " disabled/>';
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label" colspan="4"><u>Datos Contables:</u></td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Cta. sueldo</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Search->TextSch("ctasueldo","","")->Enabled(false)->Create("t08");
                        $Empleado.= '            </td>';
                        $Empleado.= '            <td class="Form-Label">Cta. anticipo</td>';
                        $Empleado.= '            <td class="Form-Label">';
                        $Empleado.=                  $this->Search->TextSch("ctaanticipo","","")->Enabled(false)->Create("t07");
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        $Empleado.= '        <tr height="30">';
                        $Empleado.= '            <td class="Form-Label">Cta. préstamos</td>';
                        $Empleado.= '            <td class="Form-Label" colspan="3">';
                        $Empleado.=                  $this->Search->TextSch("ctaprestamo","","")->Enabled(false)->Create("t08");
                        $Empleado.= '            </td>';
                        $Empleado.= '        </tr>';
                        return $Empleado.'</table>';
                }

                function MuestraForm($Ajax,$Datos)
                {       foreach($Datos as $Dato)
                        {   $Fecha  = new DateControl();
                            $id = str_pad($Dato['id'], $this->Maxlen,"0",STR_PAD_LEFT);    
                            $Ajax->Assign("idempleado".$this->Sufijo,"value",$id);                            
                            $Ajax->Assign("nombre".$this->Sufijo,"value",trim($Dato['nombre']));
                            $Ajax->Assign("apellido".$this->Sufijo,"value",trim($Dato['apellido']));
                            $Ajax->Assign("beneficia".$this->Sufijo,"value",trim($Dato['beneficiario']));
                            $Ajax->Assign("email".$this->Sufijo,"value",trim($Dato['email']));
                            $Ajax->Assign("direccion".$this->Sufijo,"value",trim($Dato['direccion']));
                            $Ajax->Assign("estado".$this->Sufijo,"value",$Dato['idestado']);
                            $Ajax->Assign("sexoemp".$this->Sufijo,"value",trim($Dato['sexo']));
                            $Ajax->Assign("cedula".$this->Sufijo,"value",trim($Dato['numero']));
                            $Ajax->Assign("estadocivil".$this->Sufijo,"value",$Dato['idestcivil']);
                            $Ajax->Assign("profesion".$this->Sufijo,"value",$Dato['idprofesion']);
                            $Ajax->Assign("fijo".$this->Sufijo,"value",trim($Dato['fono_casa']));
                            $Ajax->Assign("movil".$this->Sufijo,"value",trim($Dato['fono_movil']));
                            $Ajax->Assign("sucursal".$this->Sufijo,"value",$Dato['idsucursal']);
                            $Ajax->Assign("tipoemp".$this->Sufijo,"value",$Dato['tipoempleado']);  
                            $Ajax->Assign("contrato".$this->Sufijo,"value",trim($Dato['nucontrato']));  
                            $Ajax->Assign("idciudad".$this->Sufijo,"value",$Dato['idciudad']);
                            $Ajax->Assign("txciudad".$this->Sufijo,"value",trim($Dato['txciudad']));
                            $Ajax->Assign("iddepartamento".$this->Sufijo,"value",$Dato['iddepartamento']);
                            $Ajax->Assign("txdepartamento".$this->Sufijo,"value",trim($Dato['txdepartamento']));
                            $Ajax->Assign("idcargo".$this->Sufijo,"value",$Dato['idcargo']);
                            $Ajax->Assign("txcargo".$this->Sufijo,"value",trim($Dato['txcargo']));
                            $Ajax->Assign("idjefe".$this->Sufijo,"value",$Dato['idjefe']);
                            $Ajax->Assign("txjefe".$this->Sufijo,"value",trim($Dato['txjefe']));
                            $Ajax->Assign("idplantilla".$this->Sufijo,"value",$Dato['idplantilla']);
                            $Ajax->Assign("txplantilla".$this->Sufijo,"value",trim($Dato['txplantilla']));

                            $Ajax->Assign("idctasueldo".$this->Sufijo,"value",$Dato['idctasueldo']);
                            $Ajax->Assign("txctasueldo".$this->Sufijo,"value",trim($Dato['txctasueldo']));
                            $Ajax->Assign("idctaanticipo".$this->Sufijo,"value",$Dato['idctaanticipo']);
                            $Ajax->Assign("txctaanticipo".$this->Sufijo,"value",trim($Dato['txctaanticipo']));
                            $Ajax->Assign("idctaprestamo".$this->Sufijo,"value",$Dato['idctaprestamo']);
                            $Ajax->Assign("txctaprestamo".$this->Sufijo,"value",trim($Dato['txctaprestamo']));
                            $Ajax->Assign("txfirma".$this->Sufijo,"value",trim($Dato['firma']));
                            $Ajax->Assign("banco".$this->Sufijo,"value",$Dato['idbanco']);
                            $Ajax->Assign("ctatipo".$this->Sufijo,"value",$Dato['tipoctabco']);
                            $Ajax->Assign("ctanum".$this->Sufijo,"value",trim($Dato['ctabanco']));
                            $Ajax->Assign("entrada".$this->Sufijo,"value",trim($Dato['horadesde']));
                            $Ajax->Assign("hsalida".$this->Sufijo,"value",trim($Dato['horahasta']));
                            $Ajax->Assign("multa".$this->Sufijo,"value",number_format($Dato['pormulta'],2,".",","));
                            $Ajax->Assign("ingreso".$this->Sufijo,"value",$Fecha->changeFormatDate($Dato['feingreso'], "DMY")[1]);
                            $Ajax->Assign("salida".$this->Sufijo,"value",$Fecha->changeFormatDate($Dato['fesalida'], "DMY")[1]);
                            $Ajax->Assign("nacio".$this->Sufijo,"value",$Fecha->changeFormatDate($Dato['fenacimiento'], "DMY")[1]);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->Respuesta($Ajax,"NOEXISTEID","No existe un empleado con el ID ingresado.");
                        
                }
                
                private function CreaComboEstadoByArray($IdArray)
                {       $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $this->Select->Combo("estado",$Datos)->Selected(1)->Create("s03");
                }        

                private function CreaComboTipoEmpleado($Size)
                {       $Combo = '<select id="tipoemp'.$this->Sufijo.'" name="tipoemp'.$this->Sufijo.'" class="sel-input '.$Size.'" onchange=" return ActivaSalida(\''.$this->Sufijo.'\',\'tipoemp\'); " disabled>';
                        $Combo.= '<option value=1>EVENTUAL</option>';
                        $Combo.= '<option value=2>A PRUEBA</option>';
                        $Combo.= '<option value=3>PERMANENTE</option>';
                        $Combo.= '</select>';
                        return $Combo; 
                }        
                
                private function CreaComboSexo($Size)
                {       $Combo = '<select id="sexoemp'.$this->Sufijo.'" name="sexoemp'.$this->Sufijo.'" class="sel-input '.$Size.'" disabled>';
                        $Combo.= '<option value="M">MASCULINO</option>';
                        $Combo.= '<option value="F">FEMENINO</option>';
                        $Combo.= '</select>';
                        return $Combo; 
                }        

                function MuestraActualizado($Ajax,$Respuesta)
                {       if (is_numeric($Respuesta)){
                            return $this->Respuesta($Ajax,"GUARDADO",str_pad($Respuesta, $this->Maxlen,"0",STR_PAD_LEFT));
                        }else{
                            return $this->Respuesta($Ajax,"EXCEPCION",$Respuesta);
                        }
                }

                function MuestraEliminado($Ajax,$Respuesta)
                {       if (is_bool($Respuesta)){
                            return $this->Respuesta($Ajax,"EXCEPCION","No se pudo desactivar el registro.");
                        }else{
                            return $this->Respuesta($Ajax,"ELIMINADO",0);
                        }
                }
                
                private function Respuesta($Ajax,$Tipo,$Data)
                {       $Ajax->call("XAJAXResponse".$this->Sufijo,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }

                public function RutaFirma($RutaFirma)
                {      $script = '<script language="javaScript" type="text/javascript">';
                       $script.= '        var obj = window.parent.document;';
                       $script.= '        var txt = obj.getElementById("txfirma'.$this->Sufijo.'");';
                       $script.= '        txt.value = "'.$RutaFirma.'";';
                       $script.= '</script>';
                       $script.= '<div style="border:0px dotted #000; width:92%; padding:10px; font-family: tahoma; font-size:9pt">';
                       $script.= '     <b>La firma se subio correctamente.</b>';
                       $script.= '</div>';
                       return $script;
                }
                
                
                
                /**** Modal ****/
                private function SearchGridModalConfig()
                {       $Columns['Id']       = array('50px','center','');
                        $Columns['Empleado'] = array('250px','left','');
                        $Columns['C.I']      = array('80px','center','');
                        $Columns['CREADO']   = array('80px','center','');
                        $Columns['ESTADO']   = array('70px','center','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
                }
        
                function WidthModalGrid()
                {       return 562;
                }

                private function ModalGridHTML($Grid,$Datos)
                {       foreach ($Datos as $empleado)
                        {       $Fechas = new DateControl();
                                $id = str_pad($empleado['id'],$this->Maxlen,'0',STR_PAD_LEFT);
                                $fe = $Fechas->changeFormatDate($empleado['fecreacion'],"DMY")[1];
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($empleado['empleado']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($empleado['numero'])); 
                                $Grid->CreaSearchCellsDetalle($id,'',$fe);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($empleado['descripcion']));
                                $Grid->CreaSearchRowsDetalle($id,"");
                        }   
                        return $Grid->CreaSearchTableDetalle();
                }

                function CreaModalGrid($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->ModalGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }        
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->ModalGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
                
        }
?>

