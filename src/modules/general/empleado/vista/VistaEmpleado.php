<?php   $Sufijo = '_empleado';
         
        require_once('src/modules/general/empleado/controlador/ControlEmpleado.php');
        $ControlEmpleado = new ControlEmpleado($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlEmpleado,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlEmpleado,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlEmpleado,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlEmpleado,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlEmpleado,'ConsultaModalGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('EliminaFile'.$Sufijo, $ControlEmpleadoCargas,'EliminaFileEmpleado'));                 
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/general/empleado/js/JSEmpleado.js"></script>
        </head>
        <body>
        <div class="FormBasic" style="width:750px">
                <div class="FormSectionMenu">              
                <?php   $ControlEmpleado->CargaBarButton($_GET['opcion']);
                        echo '<script type="text/javascript">';
                        echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                        echo '</script>';
                ?>
        </div>    
        <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Sufijo; ?>">
                <?php  $ControlEmpleado->CargaMantenimiento();  ?>
                </form>
        </div>    
        </div> 
        </body>
        </html>