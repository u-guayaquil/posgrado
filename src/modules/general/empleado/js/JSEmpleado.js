    var objempleado = "nombre:sexoemp:apellido:cedula:beneficia:estadocivil:email:profesion:direccion:fijo:movil:tipoemp:contrato:ingreso:salida:sucursal:entrada:hsalida";
        objempleado = objempleado +":idciudad:txciudad:banco:iddepartamento:txdepartamento:ctatipo:idcargo:txcargo:ctanum:idjefe:txjefe:multa:idplantilla:txplantilla:nacio:idctasueldo:txctasueldo:idctaanticipo:txctaanticipo:idctaprestamo:txctaprestamo:txfirma";
    
    var savempleado = objempleado+ ":idempleado:estado";
    
    var valempleado = "nombre:apellido:email:direccion:fijo:movil:ingreso:entrada:hsalida:idciudad:txciudad:iddepartamento:txdepartamento";
        valempleado = valempleado + ":nacio:idcargo:txcargo:idctasueldo:txctasueldo:idctaanticipo:txctaanticipo:idctaprestamo:txctaprestamo";    //:ctanum:idplantilla:txplantilla
        
    var btnempleado = "btciudad:btdepartamento:btcargo:btjefe:btplantilla:btctasueldo:btctaanticipo:btctaprestamo:btfirma";

    $(document).ready(function()
    {      $('#ingreso_empleado').datetimepicker({
                        timepicker:false,
                        format: 'd/m/Y'
           });
           $('#salida_empleado').datetimepicker({
                        timepicker:false,
                        format: 'd/m/Y'
           });
           $('#nacio_empleado').datetimepicker({
                        timepicker:false,
                        format: 'd/m/Y'
           });
                
           $("#entrada_empleado").mask("99:99");
           $("#hsalida_empleado").mask("99:99");
           
    });
    
    function ButtonClick_empleado(Sufijo, Operacion)
    {       if (Operacion == "addNew")
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateEnabled(Sufijo, btnempleado);
                ElementStatus(Sufijo, objempleado, "idempleado:btempleado");
                ElementClear (Sufijo, objempleado + ":idempleado");
                ElementSetValue(Sufijo, "estado", "1");
                ElementSetValue(Sufijo, "tipoemp", "1");
                ElementSetValue(Sufijo, "salida", "01/01/1900");
            } 
            else if (Operacion == "addMod")
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateEnabled(Sufijo, btnempleado);
                if (ElementGetValue(Sufijo, "estado") == 0){
                    ElementStatus(Sufijo, objempleado + ":estado", "idempleado:btempleado");
                }else{
                    ElementStatus(Sufijo, objempleado, "idempleado:btempleado");
                }
                ActivaSalida(Sufijo,"tipoemp");
            }
            else if (Operacion === 'addDel')
            {   if (ElementGetValue(Sufijo, "estado") == 1)
                {   if (confirm("Desea inactivar este regsistro?")){
                        xajax_Elimina_empleado(ElementGetValue(Sufijo, "idempleado"));
                    }
                }else
                alert("El registro se encuentra inactivo");
            } 
            else if (Operacion == "addSav")
            {   if (ElementGetValue(Sufijo, "tipoemp")==3 && ElementGetValue(Sufijo, "contrato")=='')
                    alert('Si el contrato es permanente, ingrese su numero.');
                else{    
                    if (ElementValidateBeforeSave(Sufijo, valempleado)) {
                        if (BarButtonState(Sufijo, "Inactive")){   
                            var Forma = PrepareElements(Sufijo, savempleado);
                            xajax_Guarda_empleado(JSON.stringify({Forma}));
                        }
                    }    
                }
            } 
            else if (Operacion == "addCan")
            {   if (ElementGetValue(Sufijo, "idempleado")=='')
                {   var link = ElementGetValue(Sufijo, "txfirma");
                    if (link!='')
                    xajax_EliminaFile_empleado(link);
                }
                
                BarButtonState(Sufijo, Operacion);
                BarButtonStateDisabled(Sufijo, btnempleado);
                ElementStatus(Sufijo, "idempleado:btempleado", objempleado + ":estado");
                ElementClear (Sufijo, "idempleado:" + objempleado);
            } 
            else{   
                var iddepar = ElementGetValue(Sufijo, "iddepartamento");
                if (Operacion == 'btcargo')
                {   if (iddepar == '')
                    {   alert('Debe ingresar un departamento');
                        return false;
                    }
                }
                xajax_CargaModal_empleado(Operacion,iddepar);
            }
            return false;
    }

    function KeyDown_empleado(Evento, Sufijo, Elemento)
    {       if (Elemento.id == "idempleado" + Sufijo)  
            {   Params = Dependencia_II(Evento,"NUM");
                if (Params[0])
                {   BarButtonState(Sufijo, "Default");
                    ElementClear(Sufijo,objempleado);    
                }   
                return Params[2];
            }
    }

    function SearchByElement_empleado(Sufijo, Elemento)
    {       Elemento.value = TrimElement(Elemento.value);
            if (Elemento.name == "idempleado" + Sufijo)
            {   if (Elemento.value.length != 0)
                {   ContentFlag = document.getElementById("nombre" + Sufijo);
                    if (ContentFlag.value.length == 0)
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        xajax_BuscaByID_empleado(Elemento.value);
                    }
                }
            } 
            else
            {   if (Elemento.id === "FindEmpleadoTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_empleado("btempleado", Elemento.value,0);
                }
                else if (Elemento.id === "FindCiudadTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_empleado("btciudad", Elemento.value,0);
                }
                else if (Elemento.id === "FindDepartamentoTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_empleado("btdepartamento", Elemento.value,0);
                }
                else if (Elemento.id === "FindCargoTextBx" + Sufijo)
                {   
                    var iddepar = ElementGetValue(Sufijo, "iddepartamento");
                    if (iddepar == '')
                        alert('Debe seleccionar un departamento');
                    else
                        xajax_BuscaModalByTX_empleado("btcargo", Elemento.value,iddepar);
                }
                else if (Elemento.id === "FindJefeTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_empleado("btjefe", Elemento.value,0);
                }
                else if (Elemento.id === "FindPlantillaTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_empleado("btplantilla", Elemento.value,0);
                }
                else if (Elemento.id === "FindCtasueldoTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_empleado("btctasueldo", Elemento.value,0);
                }
                else if (Elemento.id === "FindCtaanticipoTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_empleado("btctaanticipo", Elemento.value,0);
                }
                else if (Elemento.id === "FindCtaprestamoTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_empleado("btctaprestamo", Elemento.value,0);
                }
            }
            return false;
    }

    function SearchEmpleadoGetData_empleado(Sufijo, DatosGrid)
    {       var Id = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("idempleado" + Sufijo).value = Id;
            xajax_BuscaByID_empleado(Id);    
            cerrar();
            return false;
    }

    function SearchCiudadGetData_empleado(Sufijo, DatosGrid)
    {       document.getElementById("idciudad" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txciudad" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }
    
    function SearchDepartamentoGetData_empleado(Sufijo, DatosGrid)
    {       document.getElementById("iddepartamento" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txdepartamento" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            document.getElementById("idcargo" + Sufijo).value = "";
            document.getElementById("txcargo" + Sufijo).value = "";
            cerrar();
            return false;
    }

    function SearchCargoGetData_empleado(Sufijo, DatosGrid)
    {       document.getElementById("idcargo" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txcargo" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchJefeGetData_empleado(Sufijo, DatosGrid)
    {       document.getElementById("idjefe" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txjefe" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchPlantillaGetData_empleado(Sufijo, DatosGrid)
    {       document.getElementById("idplantilla" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txplantilla" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCtasueldoGetData_empleado(Sufijo, DatosGrid)
    {       document.getElementById("idctasueldo" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txctasueldo" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCtaanticipoGetData_empleado(Sufijo, DatosGrid)
    {       document.getElementById("idctaanticipo" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txctaanticipo" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchCtaprestamoGetData_empleado(Sufijo, DatosGrid)
    {       document.getElementById("idctaprestamo" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txctaprestamo" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }
    
    function XAJAXResponse_empleado(Sufijo, Mensaje, Datos)
    {       if (Mensaje === "GUARDADO")
            {  BarButtonState(Sufijo, "Active");
               BarButtonStateDisabled(Sufijo, btnempleado);
               ElementSetValue(Sufijo, "idempleado", Datos);
               ElementStatus(Sufijo, "idempleado:btempleado", objempleado + ":estado");
               alert('Los datos se guardaron correctamente');
            }
            else if (Mensaje === 'EXCEPCION')
            {       BarButtonState(Sufijo,"addNew");
                    alert(Datos); 
            }  
            else if (Mensaje === 'NOEXISTEID')
            {       ElementSetValue(Sufijo, "idempleado", "");
                    BarButtonState(Sufijo, "Default");
                    alert(Datos);
            }
            else if (Mensaje === 'ELIMINADO')
            {       ElementSetValue(Sufijo, "estado", Datos);
            }
    }
    
    function ActivaSalida(Sufijo,TipoContrato)
    {       ElementSetValue(Sufijo, "salida","01/01/1900");
            if (ElementGetValue(Sufijo, TipoContrato)==3){
                ElementStatusDisabled(Sufijo,"salida");
            }else{ 
                ElementStatusEnabled(Sufijo,"salida");
            } 
    }
