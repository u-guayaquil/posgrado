<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
        
        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/modules/general/empleado/render/RenderEmpleado.php");      
        require_once("src/rules/general/servicio/ServicioCiudad.php"); 
        require_once("src/modules/general/ciudad/render/RenderCiudad.php");
        require_once("src/rules/nomina/servicio/ServicioDepartamento.php"); 
        require_once("src/modules/nomina/departamento/render/RenderDepartamento.php");
        require_once("src/rules/nomina/servicio/ServicioCargo.php"); 
        require_once("src/modules/nomina/cargo/render/RenderCargo.php");
        require_once("src/rules/nomina/servicio/ServicioPlantillaRol.php"); 
        require_once("src/modules/nomina/plantillarol/render/RenderPlantillaRol.php");
        require_once("src/rules/general/servicio/ServicioPlanCuentas.php");    
        require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
        require_once("src/libs/clases/File.php");
        
        class ControlEmpleado
        {   private $Sufijo; 
            private $ServicioEmpleado;
            private $RenderEmpleado;
            private $Utils;

            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;
                    $this->ServicioEmpleado = new ServicioEmpleado($Sufijo);
                    $this->RenderEmpleado = new RenderEmpleado($Sufijo);
                    $this->Utils = new Utils();
            }

            function CargaBarButton($Opcion)
            {       $BarButton = new BarButton($this->Sufijo);
                    $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                    $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                    echo $BarButton->CreaBarButton($Buttons);
            }
               
            function CargaMantenimiento()
            {        echo $this->RenderEmpleado->CreaMantenimiento();
            }

            function ConsultaByID($id)
            {       $ajaxResp = new xajaxResponse();
                    $Datos = $this->ServicioEmpleado->BuscarByID($id);
                    return $this->RenderEmpleado->MuestraForm($ajaxResp,$Datos);
            }
               
            function Guarda($Form)
            {       $ajaxResp = new xajaxResponse();
                    $Empleado = json_decode($Form)->Forma;
                    if (empty($Empleado->idempleado)){
                        $Empleado->idempleado = $this->ServicioEmpleado->Crea($Empleado);
                    }else{
                        $Empleado->idempleado = $this->ServicioEmpleado->Edita($Empleado);
                    }     
                    return $this->RenderEmpleado->MuestraActualizado($ajaxResp,$Empleado->idempleado);                      
            }

            function Elimina($id)
            {       $ajaxResp = new xajaxResponse();
                    $this->ServicioEmpleado->Desactiva($id);
                    return $this->RenderEmpleado->MuestraEliminado($ajaxResp,$id);
            }

            function CargaModalGrid($Operacion,$RelID)
            {       $ajaxResp = new xajaxResponse();    
                    $prepareDQL = array('limite' => 50,'inicio' => 0, 'texto' => '', 'estado'=>1, 'departamento'=>$RelID);

                    if ($Operacion==="btempleado" || $Operacion==="btjefe")    
                    {   $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Empleado";
                        $jsonModal['Carga'] = $this->RenderEmpleado->CreaModalGrid($Operacion,$Datos);
                        $jsonModal['Ancho'] = $this->RenderEmpleado->WidthModalGrid();
                    }   
                    elseif ($Operacion==="btciudad")    
                    {   $ServicioCiudad = new ServicioCiudad();
                        $RenderCiudad = new RenderCiudad($this->Sufijo);
                        $Datos = $ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Ciudad";
                        $jsonModal['Carga'] = $RenderCiudad->CreaModalGridCiudad($Operacion,$Datos);
                        $jsonModal['Ancho'] = $RenderCiudad->WidthModalGrid();
                    }   
                    elseif ($Operacion==="btdepartamento")    
                    {   $ServicioDepartamento = new ServicioDepartamento();
                        $RenderDepartamento = new RenderDepartamento($this->Sufijo);
                        $Datos = $ServicioDepartamento->BuscarDepartamentoByDescripcion($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Departamento";
                        $jsonModal['Carga'] = $RenderDepartamento->CreaModalGridDepartamento($Operacion,$Datos);
                        $jsonModal['Ancho'] = $RenderDepartamento->WidthModalGrid();
                    }   
                    else if ($Operacion==="btcargo")    
                    {   $ServicioCargo = new ServicioCargo();
                        $RenderCargo = new RenderCargo($this->Sufijo);
                        $Datos = $ServicioCargo->BuscarCargoByDescripcion($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Cargo";
                        $jsonModal['Carga'] = $RenderCargo->CreaModalGridCargo($Operacion,$Datos);
                        $jsonModal['Ancho'] = $RenderCargo->WidthModalGrid();
                    }   
                    elseif ($Operacion==="btplantilla")    
                    {   $ServicioPlantillaRol = new ServicioPlantillaRol();
                        $RenderPlantillaRol = new RenderPlantillaRol($this->Sufijo);
                        $Datos = $ServicioPlantillaRol->BuscarPlantillaRolByDescripcion($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Plantilla Rol";
                        $jsonModal['Carga'] = $RenderPlantillaRol->CreaModalGrid($Operacion,$Datos);
                        $jsonModal['Ancho'] = $RenderPlantillaRol->WidthModalGrid();
                    }   
                    elseif ($Operacion==="btctasueldo" || $Operacion==="btctaanticipo" || $Operacion==="btctaprestamo")    
                    {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '');
                        $ServicioPlanCuentas = new ServicioPlanCuentas();
                        $RenderPlanCuentas = new RenderPlanCuentas($this->Sufijo);
                        $Datos = $ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Buscar Cuenta";
                        $jsonModal['Carga'] = $RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                        $jsonModal['Ancho'] = $RenderPlanCuentas->WidthModalGrid();
                    } 
                    elseif ($Operacion==="btfirma")
                    {   $ruta = $this->Utils->Encriptar("src/utils/carga/carga.php");
                        $path = $this->Utils->Encriptar("src/upload/empleados");
                        $clss = $this->Utils->Encriptar("src/modules/general/empleado/render/RenderEmpleado.php");
                        $type = $this->Utils->Encriptar("[png, jpg, jpeg]");
                        $suff = $this->Utils->Encriptar($this->Sufijo);
                                
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Carga Firma";
                        $jsonModal['Carga'] = "<iframe name='loadfile' src='guns.php?ruta=".$ruta."&clss=".$clss."&type=".$type."&path=".$path."&suff=".$suff."' height='100%' width='100%' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe>";
                        $jsonModal['Ancho'] = "300";
                    }
                    $ajaxResp->call("CreaModal", json_encode($jsonModal));
                    return $ajaxResp; 
            }

            function ConsultaModalGridByTX($Operacion,$Texto = "",$RelID = 0)
            {       $ajaxResp = new xajaxResponse();    
                    $texto = trim($Texto);
                    
                    $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'estado'=>1, 'departamento'=> intval($RelID));

                    if ($Operacion==="btempleado" || $Operacion==="btjefe")    
                    {   $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);
                        return $this->RenderEmpleado->MuestraModalGrid($ajaxResp,$Operacion,$Datos);
                    }   
                    elseif ($Operacion==="btciudad")    
                    {   $ServicioCiudad = new ServicioCiudad();
                        $RenderCiudad = new RenderCiudad($this->Sufijo);
                        $Datos = $ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                        return $RenderCiudad->MuestraModalGridCiudad($ajaxResp,$Operacion,$Datos);                                        
                    }    
                    elseif ($Operacion==="btdepartamento")    
                    {   $ServicioDepartamento = new ServicioDepartamento();
                        $RenderDepartamento = new RenderDepartamento($this->Sufijo);
                        $Datos = $ServicioDepartamento->BuscarDepartamentoByDescripcion($prepareDQL);
                        return $RenderDepartamento->MuestraModalGridDepartamento($ajaxResp,$Operacion,$Datos);                                        
                    }    
                    elseif ($Operacion==="btcargo") 
                    {   $ServicioCargo = new ServicioCargo();
                        $RenderCargo = new RenderCargo($this->Sufijo);
                        $Datos = $ServicioCargo->BuscarCargoByDescripcion($prepareDQL);
                        return $RenderCargo->MuestraModalGridCargo($ajaxResp,$Operacion,$Datos);                                        
                    }
                    elseif ($Operacion==="btplantilla") 
                    {   $ServicioPlantillaRol = new ServicioPlantillaRol();
                        $RenderPlantillaRol = new RenderPlantillaRol($this->Sufijo);
                        $Datos = $ServicioPlantillaRol->BuscarPlantillaRolByDescripcion($prepareDQL);
                        return $RenderPlantillaRol->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                    }
                    
                    if ($Operacion==="btctasueldo" || $Operacion==="btctaanticipo" || $Operacion==="btctaprestamo")                            
                    {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                        $ServicioPlanCuentas = new ServicioPlanCuentas();
                        $RenderPlanCuentas = new RenderPlanCuentas($this->Sufijo);
                        $Datos = $ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        return $RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);                                        
                    }
            }

            function EliminaFileEmpleado($ruta)
            {       $FileManager = new File();                
                    $FileManager->DeleteFile($ruta);
            }
    }

?>

