<?php
         require_once("src/rules/general/servicio/ServicioZonas.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/general/zonas/render/RenderZonas.php");

         class ControlZonas
         {     private  $Sufijo; 
               private  $ServicioZonas;
               private  $RenderZonas;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioZonas = new ServicioZonas();
                        $this->RenderZonas = new RenderZonas($Sufijo);
               }

               function CargaZonasBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaZonasMantenimiento()
               {        echo $this->RenderZonas->CreaZonasMantenimiento();
               }

               function CargaZonasSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoZonas = $this->ServicioZonas->BuscarZonasByDescripcion($prepareDQL);
                        echo $this->RenderZonas->CreaZonasSearchGrid($datoZonas);
               }         
               
               function MuestraZonasByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoZonas = $this->ServicioZonas->BuscarZonasByID($prepareDQL);
                        return $this->RenderZonas->MuestraZonas($ajaxRespon,$datoZonas);
               }

               function MuestraZonasByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoZonas = $this->ServicioZonas->BuscarZonasByDescripcion($prepareDQL);
                        return $this->RenderZonas->MuestraZonasGrid($ajaxRespon,$datoZonas);
               }

               function GuardaZonas($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Zonas  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioZonas->GuardaDBZonas($Zonas);
                        if (is_numeric($id)){
                            $function = (empty($Zonas->id) ? "MuestraZonasGuardado" : "MuestraZonasEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioZonas->BuscarZonasByID($prepareDQL);
                            return $this->RenderZonas->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderZonas->MuestraZonasExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaZonas($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioZonas->DesactivaZonas(intval($id));
                        $Datos = $this->ServicioZonas->BuscarZonasByDescripcion($prepareDQL);
                        return $this->RenderZonas->MuestraZonasEliminado($ajaxRespon,$Datos);
                }
         }

?>

