<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderZonas
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 2; 
                }
               
                function CreaZonasMantenimiento()
                {       $Zonas = '<table class="Form-Frame" cellpadding="0" border="0">';
                        $Zonas.= '       <tr height="28">';
                        $Zonas.= '           <td class="Form-Label" style="width:25%">Id</td>';
                        $Zonas.= '           <td class="Form-Label" style="width:75%">';
                        $Zonas.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Zonas.= '           </td>';
                        $Zonas.= '       </tr>';
                        $Zonas.= '       <tr height="28">';
                        $Zonas.= '             <td class="Form-Label">Zonas</td>';
                        $Zonas.= '             <td class="Form-Label">';
                        $Zonas.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                        $Zonas.= '             </td>';
                        $Zonas.= '         </tr>';
                        $Zonas.= '         <tr height="28">';
                        $Zonas.= '             <td class="Form-Label">Estado</td>';
                        $Zonas.= '             <td class="Form-Label">';
                        $Zonas.=               $this->CreaComboEstado();
                        $Zonas.= '             </td>';
                        $Zonas.= '         </tr>';
                        $Zonas.= '</table>';
                        return $Zonas;
                }
              
                private function SearchGridValues()
                {       $Columns['Id']                 = array('30px','center','');
                        $Columns['Descripci&oacute;n'] = array('150px','left','');
                        $Columns['Creacion']           = array('120px','center','');
                        $Columns['Estado']             = array('70px','left',''); 
                        $Columns['idEstado']           = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
                }

                function CreaZonasSearchGrid($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $GrdDataHTML = $this->ZonasGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
              
                private function ZonasGridHTML($ObjSchGrid,$Datos)
                {       foreach ($Datos as $zonas)
                        {       $fecreacion = $this->Fechas->changeFormatDate($zonas['fecreacion'],"DMY");
                                $id = str_pad($zonas['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($zonas['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$zonas['txtestado']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$zonas['idestado']); 

                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($zonas['idestado']==0 ? "red" : ""));
                        }
                        return $ObjSchGrid->CreaSearchTableDetalle();
                }

                private function CreaComboEstado()
                {       $Select = new ComboBox($this->Sufijo);                        
                        $ServicioEstado = new ServicioEstado();
                        $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                        return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
                }
              
                function MuestraZonas($Ajax,$Datos)
                {       foreach ($Datos as $Dato) 
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaZonas($Ajax,"NOEXISTEID","No existe una Zona con ese ID.");
                }
              
                function MuestraZonasGrid($Ajax,$Datos)
                {       $SearchGrid = $this->SearchGrid;
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $GrdDataHTML = $this->ZonasGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }  
              
                function MuestraZonasGuardado($Ajax,$Zonas)
                {       foreach ($Zonas as $Dato) 
                        {   $id = str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraZonasGrid($Ajax,$Zonas);
                            return $this->RespuestaZonas($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaZonas($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraZonasEditado($Ajax,$Zonas)
                {       foreach ($Zonas as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraZonasRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaZonas($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaZonas($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
              
                function MuestraZonasEliminado($Ajax,$Zonas)
                {       foreach ($Zonas as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraZonasRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaZonas($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->RespuestaZonas($xAjax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraZonasRowGrid($Ajax,$Zonas,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Zonas['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($Zonas['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Zonas['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Zonas['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",$fecreacion[1]." ".$fecreacion[2]);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Zonas['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$Zonas['idestado']);
                        return $Ajax;
                }

                function MuestraZonasExcepcion($Ajax,$Msg)
                {       return $this->RespuestaZonas($Ajax,"EXCEPCION",$Msg);    
                }

                private function RespuestaZonas($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
              
                /*** Modal ***/
                
                function CreaModalGridZonas($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $GrdDataHTML = $this->ZonasGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }

                function MuestraModalGridZonas($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $GrdDataHTML = $this->ZonasGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
        }
?>

