<?php
        require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");

        class ControlPlanCuentas
        {       private $Sufijo; 
                private $ServicioPlanCuentas;
                private $RenderPlanCuentas;
                private $Utils;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
                        $this->Utils = new Utils();
                }

                function CargaCuentaBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaCuentaMantenimiento()
                {       echo $this->RenderPlanCuentas->CreaCuentaMantenimiento();
                }

                function CargaPlanCuentaSearchGrid()
                {       $Datos = $this->ServicioPlanCuentas->BuscarPlanCuenta(0);
                        echo $this->RenderPlanCuentas->CreaPlanCuentaGrid(json_decode($Datos));
                }         
               
                function MuestraCuentaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $datoPlanCuentas = $this->ServicioPlanCuentas->BuscarCuentaByID(intval($id));
                        return $this->RenderPlanCuentas->MuestraCuenta($ajaxRespon,$datoPlanCuentas);
                }

                function GuardaCuenta($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $planCuenta  = json_decode($Form)->Forma;
                        $id = $this->ServicioPlanCuentas->GuardaCuenta($planCuenta);
                        if (is_numeric($id)){
                            $function = (empty($planCuenta->idplancta) ? "MuestraPlanCuentaGuardado" : "MuestraPlanCuentaEditado");
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuenta(0);
                            return $this->RenderPlanCuentas->{$function}($ajaxRespon,$id,json_decode($Datos));
                        }else    
                        return $this->RenderPlanCuentas->MuestraCuentaExcepcion($ajaxRespon,$id);
                }
               
                function EliminaCuenta($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioPlanCuentas->DesactivaCuenta(intval($id));
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuenta(0);
                        return $this->RenderPlanCuentas->MuestraPlanCuentaEliminado($ajaxRespon,$id,json_decode($Datos));
                }
                
                function CargaModalGridCuenta($Operacion)
                {       $ajaxResp = new xajaxResponse();  
                        if ($Operacion==="btpadre")    
                        {   $prepareDQL = array('limite' => 50,'tipo'=>'G','estado'=>'1');
                            $Datos = $this->ServicioPlanCuentas->BuscarCuenta($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cuentas de Grupo";
                            $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridCuentas($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderPlanCuentas->WidthModalGrid();
                        }
                        else if($Operacion==="btplancta")
                        {   $prepareDQL = array('limite' => 50);
                            $Datos = $this->ServicioPlanCuentas->BuscarCuenta($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cuentas";
                            $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridCuentas($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderPlanCuentas->WidthModalGrid();
                        }
                        else if($Operacion==="btplanexe")
                        {   $ruta = $this->Utils->Encriptar("src/utils/carga/carga.php");
                            $path = $this->Utils->Encriptar("src/upload/contabilidad/plan");
                            $clss = $this->Utils->Encriptar("src/modules/general/plancuentas/controlador/ControlPlanCuentas.php");
                            $type = $this->Utils->Encriptar("[xls, xlsx, csv]");
                            $suff = $this->Utils->Encriptar($this->Sufijo);
                                
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Carga Plan Contable";
                            $jsonModal['Carga'] = "<iframe name='loadfile' src='guns.php?ruta=".$ruta."&clss=".$clss."&type=".$type."&path=".$path."&suff=".$suff."' height='100%' width='100%' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe>";
                            $jsonModal['Ancho'] = "300";
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridCuentaByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion==="btplancta")    
                        {   $prepareDQL = array('limite' => 50,'texto' => strtoupper(trim($Texto)));
                            $Datos = $this->ServicioPlanCuentas->BuscarCuenta($prepareDQL);
                            return $this->RenderPlanCuentas->MuestraModalGridCuentas($ajaxResp,$Operacion,$Datos);                                        
                        }    
                        if ($Operacion==="btpadre")    
                        {   $prepareDQL = array('limite' => 50,'estado'=>'1','tipo'=>'G','texto' => strtoupper(trim($Texto)));
                            $Datos = $this->ServicioPlanCuentas->BuscarCuenta($prepareDQL);
                            return $this->RenderPlanCuentas->MuestraModalGridCuentas($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }

                function CargaFilePlanCuenta($Ruta)
                {       $HTMLResp = $this->ServicioPlanCuentas->LeerFileCuentas($Ruta); 
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuenta(0);
                        return $this->RenderPlanCuentas->RespuestaCargaMasiva($HTMLResp,json_decode($Datos));
                }
                
        }
?>

