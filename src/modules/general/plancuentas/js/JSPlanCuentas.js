    var frmplancta = "codigo:descripcion:tipo:clase:idpadre:txpadre";
    var savplancta = "idplancta:codigo:descripcion:idpadre:txpadre:tipo:clase:estado";
    var valplancta = "codigo:descripcion";
    var clsplancta = "codigo:descripcion:idpadre:txpadre";


    /*
    function carga() 
    {   document.getElementById("Path1").value = document.getElementById("arch1").value;
        document.getElementById("nombrePlan").value = document.getElementById("arch1").value;
    }

    function UploadFiles_plancuentas(Forma) {

    var resul = confirm('Desea Guardar los datos..!!!');
    if (resul) {
        var Sufijo = document.getElementById('NomSufijo').value;

        if (Forma.Path1.value.length != 0) {
            var FormaEnvio = Forma;
            xajax_ExtraeExcel_plancuentas(JSON.stringify({FormaEnvio}));
            cerrar();
            return false;
        } else {
            alert("Debe agregar por lo menos un archivo para la carga.");
            return false;
        }
        return true;
    } else {
        return false;
    }
    }*/


    /*
    function cargaExcel(Operacion) {
        xajax_CargaModalExcel_plancuentas(Operacion);
        return false;
    }

    function descargarExcel() {
        xajax_DescargarExcel_plancuentas();
        return false;
    }*/

    /*
    function SearchGetData_plancuentas(Sufijo, Grilla)
    {   if (IsDisabled(Sufijo,"addSav")) 
        {
            BarButtonState(Sufijo, "Active");
            document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
            document.getElementById("codigo" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
            document.getElementById("descripcion" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
            document.getElementById("idpadre" + Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
            document.getElementById("txpadre" + Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
            document.getElementById("tipo" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
            document.getElementById("clase" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
            document.getElementById("estado" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
        }
        return false;
    }*/

    function SearchPadreGetData_plancuentas(Sufijo, DatosGrid)
    {       document.getElementById("idpadre" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txpadre" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            document.getElementById("codigo" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchPlanctaGetData_plancuentas(Sufijo, DatosGrid)
    {       var Id = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("idplancta" + Sufijo).value = Id;
            xajax_BuscarByID_plancuentas(Id);    
            cerrar();
            return false;    
    }

    function KeyDown_plancuentas(Evento, Sufijo, Elemento)
    {       if (Elemento.id == "idplancta" + Sufijo)  
            {   Params = Dependencia_II(Evento,"NUM");
                if (Params[0])
                {   BarButtonState(Sufijo, "Default");
                    ElementClear(Sufijo,clsplancta);    
                }   
                return Params[2];
            }
    }

    function XAJAXResponse_plancuentas(Sufijo, Mensaje, Datos)
    {       if (Mensaje === "GUARDADO")
            {   BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, "btpadre");
                ElementSetValue(Sufijo, "idplancta", Datos);
                ElementStatus(Sufijo, "idplancta:btplancta", frmplancta+":estado");
                alert('Los datos se guardaron correctamente.');
            } 
            else if (Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
                alert("Los datos se eliminaron correctamente");
            } 
            else if (Mensaje === 'NOEXISTEID')
            {   BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "idplancta");
                alert(Datos);
            }
            else if (Mensaje === 'EXCEPCION')
            {   BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
    }

    function ButtonClick_plancuentas(Sufijo, Operacion)
    {       if (Operacion=='addNew')
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateEnabled(Sufijo, "btpadre");
                ElementStatus(Sufijo, frmplancta, "idplancta:btplancta");
                ElementClear (Sufijo, frmplancta + ":idplancta");
                ElementSetValue(Sufijo, "estado", "1");
            }
            else if (Operacion=='addMod')
            {       BarButtonState(Sufijo, Operacion);
                    BarButtonStateEnabled(Sufijo, "btpadre");
                    ElementStatus(Sufijo, frmplancta, "idplancta:btplancta");
                    if (ElementGetValue(Sufijo,"estado")==0)
                        ElementStatus(Sufijo, "estado", "");
                    else
                        ElementStatus(Sufijo, "", "estado");
            } 
            else if (Operacion=='addDel')
            {       if (ElementGetValue(Sufijo, "estado")==1)
                    {   if (confirm("Desea inactivar este registro?"))
                        xajax_Elimina_plancuentas(ElementGetValue(Sufijo, "idplancta"));
                    } 
                    else
                    alert("El registro se encuentra inactivo.");
            } 
            else if (Operacion == 'addSav')
            {       if (EspecialValidate(Sufijo))
                    {   if (ElementValidateBeforeSave(Sufijo, valplancta))
                        {   if (BarButtonState(Sufijo, "Inactive"))
                            {   var Forma = PrepareElements(Sufijo, savplancta);
                                xajax_Guarda_plancuentas(JSON.stringify({Forma}));
                            }
                        }    
                    }
            } 
            else if (Operacion == 'addCan')
            {       BarButtonState(Sufijo, Operacion);
                    BarButtonStateDisabled(Sufijo, "btpadre");
                    ElementStatus(Sufijo, "idplancta:btplancta", frmplancta + ":estado");
                    ElementClear(Sufijo,"idplancta:" + clsplancta);
            } 
            else if (Operacion == 'btplanupl')
                    window.location="src/public/formatos/carga_plan.csv";
            else
            xajax_CargaModal_plancuentas(Operacion);

            return false;
    }

    function EspecialValidate(Sufijo)
    {       if (ElementGetValue(Sufijo, "idplancta")=="")
            {   if (ElementGetValue(Sufijo, "idpadre")=="")
                {   ElementSetValue(Sufijo, "codigo", ElementGetValue(Sufijo, "clase"));
                    ElementSetValue(Sufijo, "descripcion", ElementGetText(Sufijo, "clase"));
                    if (!confirm("¿Desea crear una cuenta raiz?"))
                    {   ElementSetValue(Sufijo, "codigo", "");
                        ElementSetValue(Sufijo, "descripcion", "");
                        return false;
                    }
                }
            }    
            return true;
    }

    function SearchByElement_plancuentas(Sufijo, Elemento)
    {       Elemento.value = TrimElement(Elemento.value);
            if (Elemento.name == "idplancta" + Sufijo)
            {   if (Elemento.value.length != 0)
                {   ContentFlag = document.getElementById("descripcion" + Sufijo);
                    if (ContentFlag.value.length==0)
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        xajax_BuscarByID_plancuentas(Elemento.value);
                    }
                }
                else
                BarButtonState(Sufijo, "Default");
            } 
            else if (Elemento.id === "FindPlanctaTextBx" + Sufijo)
            {    xajax_BuscaModalByTX_plancuentas("btplancta", Elemento.value);
            }
            else if (Elemento.id === "FindPadreTextBx" + Sufijo)
            {    xajax_BuscaModalByTX_plancuentas("btpadre", Elemento.value);
            }
    }


    /*
    function XAJAXResponseExcel_plancuentas(Mensaje)
    {
            if (Mensaje === "GUARDADO")
            {
                alert('Los datos se guardaron correctamente.');
            } 
            else if (Mensaje === 'ERROR')
            {
                alert('Error al guardar las cuentas');
            }
    }*/



