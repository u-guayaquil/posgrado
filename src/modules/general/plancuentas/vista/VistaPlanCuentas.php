<?php
        $Sufijo = '_plancuentas';
        require_once('src/modules/general/plancuentas/controlador/ControlPlanCuentas.php');
        $ControlPlanCuentas = new ControlPlanCuentas($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo,  $ControlPlanCuentas,'GuardaCuenta'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlPlanCuentas,'EliminaCuenta'));
        $xajax->register(XAJAX_FUNCTION,array('BuscarByID'.$Sufijo, $ControlPlanCuentas,'MuestraCuentaByID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlPlanCuentas,'CargaModalGridCuenta'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlPlanCuentas,'ConsultaModalGridCuentaByTX'));
        //$xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlPlanCuentas,'CargaFilePlanCuentaSearchGrid'));
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/general/plancuentas/js/JSPlanCuentas.js"></script>
        </head>
        <body>
        <div class="FormContainer" style="width:1055px">
            <div class="FormContainerSeccion" style="width:443px">
                <div class="FormBasic">
                    <div class="FormSectionMenu"> 
                        <input type="hidden" id="NomSufijo" name="NomSufijo" value="<?php echo $Sufijo; ?>">
                        <?php  $ControlPlanCuentas->CargaCuentaBarButton($_GET['opcion']);
                            echo '<script type="text/javascript">';
                            echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                            echo '</script>';
                        ?>
                    </div>    
                    <div class="FormSectionData">              
                        <form id="<?php echo 'Form'.$Sufijo; ?>">
                        <?php  $ControlPlanCuentas->CargaCuentaMantenimiento();  ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="FormContainerSeccion" style="width:603px">
                <div id="FormSectionGrid" class="FormSectionGrid">          
                <?php  $ControlPlanCuentas->CargaPlanCuentaSearchGrid();  ?>
                </div>  
            </div> 
        </div>
        </body>
        </html> 
              

