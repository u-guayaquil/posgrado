<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");        
        require_once("src/rules/general/servicio/ServicioClaseCuentas.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderPlanCuentas
        {       private $Sufijo;
                private $SearchGrid;
                private $Select;
                private $Maxlen;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Select = new ComboBox($this->Sufijo);
                        $this->Maxlen = 5; 
                }
               
                function CreaCuentaMantenimiento()
                {       $Search = new SearchInput($this->Sufijo);

                        $PlanCuentas = '<table class="Form-Frame" border="0" cellpadding="0">';
                        $PlanCuentas.= '       <tr height="30">';
                        $PlanCuentas.= '           <td class="Form-Label" style="width:20%">Id</td>';
                        $PlanCuentas.= '           <td class="Form-Label" style="width:60%">';
                        $PlanCuentas.=                 $Search->TextSch("plancta","","")->Enabled(true)->MaxLength($this->Maxlen)->ReadOnly(false)->Create("t02");
                        $PlanCuentas.= '           </td>';
                        $PlanCuentas.= '           <td class="Form-Label-Center" style="width:10%">';
                        $PlanCuentas.=                 $Search->TextSch("planupl","","")->Enabled(true)->ReadOnly(false,'txt')->TypeBtn('downloads')->Create("s01");
                        $PlanCuentas.= '           </td>';
                        $PlanCuentas.= '           <td class="Form-Label-Center" style="width:10%">';
                        $PlanCuentas.=                 $Search->TextSch("planexe","","")->Enabled(true)->ReadOnly(false,'txt')->TypeBtn('uploads')->Create("s01");
                        $PlanCuentas.= '           </td>';
                        $PlanCuentas.= '       </tr>';
                        $PlanCuentas.= '       <tr height="30">';
                        $PlanCuentas.= '           <td class="Form-Label">Padre</td>';
                        $PlanCuentas.= '           <td colspan="3" class="Form-Label">';
                        $PlanCuentas.=                 $Search->TextSch("padre","","")->Enabled(false)->Create("t12");
                        $PlanCuentas.= '           </td>';
                        $PlanCuentas.= '       </tr>';
                        $PlanCuentas.= '       <tr height="30">';
                        $PlanCuentas.= '           <td class="Form-Label">C&oacute;digo</td>';
                        $PlanCuentas.= '           <td colspan="3" class="Form-Label">';
                        $PlanCuentas.= '               <input type="text" class="txt-upper t07" onKeyDown="return soloCuenta(event,this); " id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="50" disabled/>';
                        $PlanCuentas.= '           </td>';
                        $PlanCuentas.= '       </tr>';
                        $PlanCuentas.= '       <tr height="30">';
                        $PlanCuentas.= '           <td class="Form-Label">Cuenta</td>';
                        $PlanCuentas.= '           <td colspan="3" class="Form-Label">';
                        $PlanCuentas.= '               <input type="text" class="txt-upper t13" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="100" disabled/>';
                        $PlanCuentas.= '           </td>';
                        $PlanCuentas.= '       </tr>';
                        $PlanCuentas.= '       <tr height="30">';
                        $PlanCuentas.= '           <td class="Form-Label">Tipo</td>';
                        $PlanCuentas.= '           <td colspan="3" class="Form-Label">';
                        $PlanCuentas.= '               <select id="tipo'.$this->Sufijo.'" class="sel-input s04" name="tipo'.$this->Sufijo.'" disabled>';
                        $PlanCuentas.= '                    <option value="G">GRUPO</option>';
                        $PlanCuentas.= '                    <option value="M">MOVIMIENTO</option>';                                 
                        $PlanCuentas.= '               </select>';
                        $PlanCuentas.= '           </td>';
                        $PlanCuentas.= '       </tr>';
                        $PlanCuentas.= '       <tr height="30">';
                        $PlanCuentas.= '            <td class="Form-Label">Clase</td>';
                        $PlanCuentas.= '            <td colspan="3" class="Form-Label">';
                        $PlanCuentas.=              $this->CreaComboClase();
                        $PlanCuentas.= '            </td>';
                        $PlanCuentas.= '       </tr>';                       
                        $PlanCuentas.= '       <tr height="30">';
                        $PlanCuentas.= '            <td class="Form-Label">Estado</td>';
                        $PlanCuentas.= '            <td colspan="3" class="Form-Label">';
                        $PlanCuentas.=              $this->CreaComboEstado();
                        $PlanCuentas.= '            </td>';
                        $PlanCuentas.= '       </tr>';
                        $PlanCuentas.= '</table>';
                        return $PlanCuentas;
                }
              
                private function SearchGridValues()
                {       $Columns['Id']     = array('40px','center','');
                        $Columns['Codigo'] = array('110px','left','');
                        $Columns['Cuenta'] = array('406px','left','');
                        $Columns['Estado'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '400px','AltoRow' => '20px','FindTxt' =>false);
                }

                function CreaPlanCuentaGrid($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->PlanCuentaGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }        

                function PlanCuentaGridHTML($grid,$root)
                {       $entro = false;
                        foreach($root as $sheet)
                        {       $id = str_pad($sheet->id,$this->Maxlen,'0',STR_PAD_LEFT);
                                $desc = $this->FormatGridDataOption($sheet->level,$sheet->type,$sheet->ctas);
                                $grid->CreaSearchCellsDetalle($id,'',$id);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->codigo);
                                $grid->CreaSearchCellsDetalle($id,'',$desc);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->status);
                                $grid->CreaSearchRowsDetalle ($id, ($sheet->status==0 ? "red" : "") ,0); 
                                if ($sheet->type=='G')
                                {   if (count($sheet->hijos)>0){
                                        $this->PlanCuentaGridHTMLSgte($grid,$sheet->hijos);
                                    }
                                }
                                $entro = true;
                        }
                        if ($entro)
                        return $grid->CreaSearchTableDetalle();
                }

                private function PlanCuentaGridHTMLSgte($grid,$root)
                {       foreach($root as $sheet)
                        {       $id = str_pad($sheet->id,$this->Maxlen,'0',STR_PAD_LEFT);
                                $desc = $this->FormatGridDataOption($sheet->level,$sheet->type,$sheet->ctas);
                                $grid->CreaSearchCellsDetalle($id,'',$id);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->codigo);
                                $grid->CreaSearchCellsDetalle($id,'',$desc);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->status);
                                $grid->CreaSearchRowsDetalle ($id, ($sheet->status==0 ? "red" : "") ,0); 
                                if ($sheet->type=='G')
                                {   if (count($sheet->hijos)>0)
                                    $this->PlanCuentaGridHTMLSgte($grid,$sheet->hijos);
                                }
                        }
                }

                private function FormatGridDataOption($level,$types,$description)
                {       $Texto = "<b>".$description."</b>";
                        $Sangria = "<font color='#bbb'>".str_repeat("..", $level*1)."</font>";
                        return $Sangria." ".($types=="G" ? $Texto : $description);
                }

                private function CreaComboEstado()
                {       $ServicioEstado = new ServicioEstado();
                        $datoEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                        return $this->Select->Combo("estado",$datoEstado)->Selected(1)->Create("s04");
                }
              
                private function CreaComboClase()
                {       $ServicioClase = new ServicioClaseCuentas();
                        $datoClase = $ServicioClase->BuscarClaseCuentas(array(0,1));
                        return $this->Select->Combo("clase",$datoClase)->Selected(1)->Create("s04");
                }
              
                function MuestraCuenta($Ajax,$Datos)
                {       foreach($Datos as $Dato)
                        {       $Ajax->Assign("idplancta".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen,'0',STR_PAD_LEFT));
                                $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Dato['codigo']));
                                $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));
                                $Ajax->Assign("clase".$this->Sufijo,"value", trim($Dato['clase']));                       
                                $Ajax->Assign("tipo".$this->Sufijo,"value", trim($Dato['tipo']));
                                $Ajax->Assign("idpadre".$this->Sufijo,"value", trim($Dato['idpadre']));
                                $Ajax->Assign("txpadre".$this->Sufijo,"value", trim($Dato['txpadre']));
                                $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                                $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                                return $Ajax;
                        }
                        return $this->RespuestaCuenta($Ajax,"NOEXISTEID","No existe una cuenta con el ID.");
                }
            
                function MuestraPlanCuentaGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->PlanCuentasGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }
               
                function MuestraPlanCuentaGuardado($Ajax,$IdCuenta,$Plan)
                {       $id = str_pad($IdCuenta,$this->Maxlen,'0',STR_PAD_LEFT);
                        $Ajax->Assign("FormSectionGrid","innerHTML",$this->CreaPlanCuentaGrid($Plan));
                        return $this->RespuestaCuenta($Ajax,"GUARDADO",$id); //return $this->RespuestaCuenta($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraPlanCuentaEditado($Ajax,$IdCuenta,$Plan)
                {       $id = str_pad($IdCuenta,$this->Maxlen,'0',STR_PAD_LEFT);
                        $Ajax->Assign("FormSectionGrid","innerHTML",$this->CreaPlanCuentaGrid($Plan));
                        return $this->RespuestaCuenta($Ajax,"GUARDADO",$id); //return $this->RespuestaCuenta($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
                    
                function MuestraPlanCuentaEliminado($Ajax,$IdCuenta,$Plan)
                {       $id = str_pad($IdCuenta,$this->Maxlen,'0',STR_PAD_LEFT);
                        $Ajax->Assign("idplancta".$this->Sufijo,"value",$id);
                        $Ajax->Assign("FormSectionGrid","innerHTML",$this->CreaPlanCuentaGrid($Plan));
                        return $this->RespuestaCuenta($Ajax,"ELIMINADO",0); //return $this->RespuestaCuenta($Ajax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraCuentaExcepcion($Ajax,$Msg)
                {       return $this->RespuestaCuenta($Ajax,"EXCEPCION",$Msg);    
                }

                private function RespuestaCuenta($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
                /*************************** MODAL ****************************/
                private function SearchGridModalConfig()
                {       $Columns['Id']      = array('40px','center','');
                        $Columns['Codigo']  = array('100px','left','');
                        $Columns['Cuenta'] = array('300px','left','');
                        $Columns['Cuenta Padre']       = array('280px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function WidthModalGrid()
                {       return 750;
                }

                function CreaModalGridCuentas($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalPlanCuentas($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGridCuentas($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalPlanCuentas($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalPlanCuentas($Grid,$Datos)
                {       foreach ($Datos as $Opcion)
                        {       $id = str_pad($Opcion['id'],$this->Maxlen,'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                                $Grid->CreaSearchCellsDetalle($id,'',strtoupper(trim($Opcion['descripcion'])));
                                $Grid->CreaSearchCellsDetalle($id,'',strtoupper(trim($Opcion['txpadre'])));
                                $Grid->CreaSearchRowsDetalle ($id,($Opcion['idestado']==0 ? "red" : ""));
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
                
                function RespuestaCargaMasiva($HTMLResp,$Plan)
                {       $script = '<script language="javaScript" type="text/javascript">';
                        $script.= '        var obj = window.parent.document;';
                        $script.= '        var Frm = obj.getElementById("FormSectionGrid");';
                        $script.= '        Frm.innerHTML = "'.$this->CreaPlanCuentaGrid($Plan).'";';
                        $script.= '</script>';
                        $script.= '<div style="border:0px dotted #000; width:92%; padding:10px; font-family: tahoma; font-size:9pt">';
                        $script.= '     <b>'.$HTMLResp.'</b>';
                        $script.= '</div>';
                        return $script;          
                }
               
                
                /*
                function CreaHojadeExcel($Ajax)
                {   error_reporting(E_ALL);
                    ini_set('display_errors', TRUE);
                    ini_set('display_startup_errors', TRUE);
                    date_default_timezone_set('Europe/London');
                    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
                    
                    //header('Content-type: application/vnd.ms-excel');
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="Plandecuentas.xlsx"');
                    header('Cache-Control: max-age=0');
                    require_once 'src/libs/plugins/excel/Clases/PHPExcel.php';
                    $objPHPExcel = new PHPExcel();

                    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                                 ->setTitle("Plan de Cuentas")
                                                 ->setSubject("Plan de Cuentas")
                                                 ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                                                 ->setKeywords("office PHPExcel php")
                                                 ->setCategory("Test result file");

                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'CODIGO');
                    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'DESCRIPCION');
                    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'TIPO');
                    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'CLASE');
                    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'IDPADRE');
                    
                    $objPHPExcel->getActiveSheet()->setCellValue('A2', '117');
                    $objPHPExcel->getActiveSheet()->setCellValue('B2', 'CAJA LIQUIDA');
                    $objPHPExcel->getActiveSheet()->setCellValue('C2', 'M');
                    $objPHPExcel->getActiveSheet()->setCellValue('D2', '1');
                    $objPHPExcel->getActiveSheet()->setCellValue('E2', '11');
                    
                    $objPHPExcel->getActiveSheet()->setCellValue('A3', '118');
                    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'CAJA AUXILIAR');
                    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'M');
                    $objPHPExcel->getActiveSheet()->setCellValue('D3', '1');
                    $objPHPExcel->getActiveSheet()->setCellValue('E3', '11');

                    $objPHPExcel->getActiveSheet()->setTitle('Plan de Cuentas');
                    $objPHPExcel->setActiveSheetIndex(0);
                    
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save('src/modules/general/plancuentas/Excel/PlandeCuentas.xlsx');
                    $objWriter->save('php://output');                    
                    //$Ajax->Assign($objWriter->save('php://output'));
                    return $Ajax;
                }
                
                function MuestraDespuesdeGuardarExcel($Ajax,$Datos)
                {       $jsFun = "XAJAXResponseExcel".$this->Sufijo;
                        if (count($Datos)>0)
                        {   $xAjax = $this->MuestraPlanCuentasGrid($Ajax,$Datos); 
                            $xAjax->call($jsFun,"GUARDADO"); 
                            return $xAjax;
                        }
                }
                
                function MuestraErroralGuardarExcel($Ajax,$Datos)
                {       $jsFun = "XAJAXResponseExcel".$this->Sufijo;                
                        if (count($Datos)>0)
                        {   $Ajax = $this->MuestraPlanCuentasGrid($Ajax,$Datos);                            
                            $Ajax->call($jsFun,"ERROR");    
                            return $Ajax;
                        }
                        
                }
                */
        }
?>

