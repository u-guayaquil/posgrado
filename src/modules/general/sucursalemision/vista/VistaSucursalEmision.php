<?php
         $Sufijo = '_SucursalEmision';

         require_once('src/modules/general/sucursalemision/controlador/ControlSucursalEmision.php');
         $ControlSucursalEmision = new ControlSucursalEmision($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlSucursalEmision,'GuardaSucursalEmision'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlSucursalEmision,'MuestraSucursalEmisionByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlSucursalEmision,'MuestraSucursalEmisionByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlSucursalEmision,'EliminaSucursalEmision'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlSucursalEmision,'CargaModalGridSucursalEmision'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlSucursalEmision,'ConsultaModalGridSucursalEmisionByTX'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaSucursalByTX'.$Sufijo, $ControlSucursalEmision,'ConsultaModalGridSucursalEmisionByTX'));
         
         $xajax->processRequest();

?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript">
             
                 function CargaFechas_SucursalEmision(Sufijo){
                    $('#fevigencia'+Sufijo).datetimepicker({
                            timepicker:false,
                            format: 'd/m/Y'
                    });
                    $('#fecaducidad'+Sufijo).datetimepicker({
                            timepicker:false,
                            format: 'd/m/Y'
                    });
                 }
             
                 function ButtonClick_SucursalEmision(Sufijo,Operacion)
                 {        var objSucursalEmision = "id:tipoemision:idTipoComprobante:txTipoComprobante:idSucursal:txSucursal"
                                                 + ":serie1:serie2:secinicio:secfinal:autorizacion:fevigencia:fecaducidad"
                                                 + ":ambiente:ejecucion:estado";
                          var frmSucursalEmision = "tipoemision:idTipoComprobante:txTipoComprobante:idSucursal:txSucursal"
                                                 + ":serie1:serie2:secinicio:secfinal:autorizacion:fevigencia:fecaducidad"
                                                 + ":ambiente:ejecucion:estado";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,frmSucursalEmision,"id:estado");
                              BarButtonStateEnabled(Sufijo,"btTipoComprobante:btSucursal");
                              ElementClear(Sufijo,objSucursalEmision);
                              CargaFechas_SucursalEmision(Sufijo);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               BarButtonStateEnabled(Sufijo,"btTipoComprobante:btSucursal");
                               ElementStatus(Sufijo,frmSucursalEmision,"id");
                               CargaFechas_SucursalEmision(Sufijo);
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_SucursalEmision(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,frmSucursalEmision))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objSucursalEmision);
                                       xajax_Guarda_SucursalEmision(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               BarButtonStateDisabled(Sufijo,"btTipoComprobante:btSucursal");
                               ElementStatus(Sufijo,"id",frmSucursalEmision);
                               ElementClear(Sufijo,objSucursalEmision);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                           else { 
                             xajax_CargaModal_SucursalEmision(Operacion);  
                           }
                            
                          return false;
                 }

                 function SearchByElement_SucursalEmision(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          
                          switch(Elemento.id){
                              case "id"+Sufijo:
                                  if (Elemento.value.length != 0)
                                    {      if (BarButtonState(Sufijo,"Inactive"))
                                            xajax_MuestraByID_SucursalEmision(Elemento.value);                                  
                                    }
                                    else
                                    BarButtonState(Sufijo,"Default");
                              break;
                              case "FindTipoComprobanteTextBx"+Sufijo:
                                  xajax_BuscaModalByTX_SucursalEmision("btTipoComprobante",Elemento.value);
                              break;
                              case 'FindTextBx'+Sufijo:
                                  xajax_MuestraByTX_SucursalEmision(Elemento.value);
                              break;
                              case 'FindSucursalTextBx'+Sufijo:
                                  xajax_BuscaSucursalByTX_SucursalEmision("btSucursal",Elemento.value);
                              break;
                          }
                 }
                 
                 function SearchGetData_SucursalEmision(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")){
                                BarButtonState(Sufijo,"Active");

                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                                document.getElementById("tipoemision"+Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
                                document.getElementById("idTipoComprobante"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                                document.getElementById("txTipoComprobante"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                                document.getElementById("idSucursal"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                                document.getElementById("txSucursal"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                                document.getElementById("serie1"+Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
                                document.getElementById("serie2"+Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue;
                                document.getElementById("secinicio"+Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
                                document.getElementById("secfinal"+Sufijo).value = Grilla.cells[11].childNodes[0].nodeValue;
                                document.getElementById("autorizacion"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                                document.getElementById("fevigencia"+Sufijo).value = Grilla.cells[12].childNodes[0].nodeValue;
                                document.getElementById("fecaducidad"+Sufijo).value = Grilla.cells[13].childNodes[0].nodeValue;
                                document.getElementById("ambiente"+Sufijo).value = Grilla.cells[14].childNodes[0].nodeValue;
                                document.getElementById("ejecucion"+Sufijo).value = Grilla.cells[15].childNodes[0].nodeValue;                          
                                document.getElementById("estado"+Sufijo).value = Grilla.cells[16].childNodes[0].nodeValue; 
                          }
                          return false;
                 }
                 
                 function SearchTipoComprobanteGetData_SucursalEmision(Sufijo,DatosGrid)
                 {       document.getElementById("idTipoComprobante"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                         document.getElementById("txTipoComprobante"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                         cerrar();
                         return false;
                 }
                 
                 function SearchSucursalGetData_SucursalEmision(Sufijo,DatosGrid)
                 {       document.getElementById("idSucursal"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                         document.getElementById("txSucursal"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                         cerrar();
                         return false;
                 }
                                 
                 function XAJAXResponse_SucursalEmision(Sufijo,Mensaje,Datos)
                 {       
                        var objSucursalEmision = "tipoemision:idTipoComprobante:txTipoComprobante:idSucursal:txSucursal"
                                               + ":serie1:serie2:secinicio:secfinal:autorizacion:fevigencia:fecaducidad"
                                               + ":ambiente:ejecucion:estado";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btTipoComprobante:btSucursal");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objSucursalEmision);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
                 
         </script>
          </head>
<body>       
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlSucursalEmision->CargaSucursalEmisionBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '    BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlSucursalEmision->CargaSucursalEmisionMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlSucursalEmision->CargaSucursalEmisionSearchGrid();  ?>
              </div>  
         </div> 
        </body>
</html> 
              

