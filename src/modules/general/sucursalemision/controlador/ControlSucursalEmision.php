<?php
         require_once("src/rules/general/servicio/ServicioSucursalEmision.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/rules/impuesto/servicio/ServicioTipoEmision.php");
         require_once("src/rules/impuesto/servicio/ServicioTipoComprobante.php");
         require_once("src/rules/general/servicio/ServicioSucursal.php");
         require_once("src/modules/general/sucursalemision/render/RenderSucursalEmision.php");
         require_once("src/modules/impuesto/tipocomprobante/render/RenderTipoComprobante.php");
         require_once("src/modules/general/sucursal/render/RenderSucursal.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlSucursalEmision
         {     private  $Sufijo; 
               private  $ServicioSucursalEmision;
               private  $ServicioTipoEmision;
               private  $ServicioTipoComprobante;
               private  $ServicioSucursal;
               private  $RenderSucursalEmision;
               private  $RenderSucursal;
               private  $RenderTipoComprobante;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioSucursalEmision = new ServicioSucursalEmision();
                        $this->ServicioTipoEmision = new ServicioTipoEmision();
                        $this->ServicioTipoComprobante = new ServicioTipoComprobante();
                        $this->ServicioSucursal = new ServicioSucursal();
                        $this->RenderSucursal = new RenderSucursal($Sufijo);
                        $this->RenderSucursalEmision = new RenderSucursalEmision($Sufijo);
                        $this->RenderTipoComprobante = new RenderTipoComprobante($Sufijo);
               }

               function CargaSucursalEmisionBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaSucursalEmisionMantenimiento()
               {        echo $this->RenderSucursalEmision->CreaSucursalEmisionMantenimiento();
               }

               function CargaSucursalEmisionSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoSucursalEmision = $this->ServicioSucursalEmision->BuscarSucursalEmision($prepareDQL);
                        echo $this->RenderSucursalEmision->CreaSucursalEmisionSearchGrid($datoSucursalEmision);
               }         
               
               function MuestraSucursalEmisionByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('id' => intval($id));
                        $datoSucursalEmision = $this->ServicioSucursalEmision->BuscarSucursalEmision($prepareDQL);
                        return $this->RenderSucursalEmision->MuestraSucursalEmision($ajaxRespon,$datoSucursalEmision);
               }

               function MuestraSucursalEmisionByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoSucursalEmision = $this->ServicioSucursalEmision->BuscarSucursalEmision($prepareDQL);
                        return $this->RenderSucursalEmision->MuestraSucursalEmisionGrid($ajaxRespon,$datoSucursalEmision);
               }

               function GuardaSucursalEmision($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $SucursalEmision  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioSucursalEmision->GuardaDBSucursalEmision($SucursalEmision);
                        if (is_numeric($id)){
                            $function = (empty($SucursalEmision->id) ? "MuestraSucursalEmisionGuardado" : "MuestraSucursalEmisionEditado");
                            $prepareDQL = array('id' => intval($id),'limite' => 50,'inicio' => 0);
                            $Datos = $this->ServicioSucursalEmision->BuscarSucursalEmision($prepareDQL);
                            return $this->RenderSucursalEmision->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderSucursalEmision->MuestraSucursalEmisionExcepcion($ajaxRespon,($id));
                        }  
               }
               
               function EliminaSucursalEmision($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $this->ServicioSucursalEmision->DesactivaSucursalEmision(intval($id));
                        $Datos = $this->ServicioSucursalEmision->BuscarSucursalEmision($prepareDQL);
                        return $this->RenderSucursalEmision->MuestraSucursalEmisionEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridSucursalEmision($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        if ($Operacion==="btSucursal")    
                        {   $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                            $nombre = " Sucursal";
                        }else{
                            $Datos = $this->ServicioTipoComprobante->BuscarTipoComprobanteByDescripcion($prepareDQL);
                            $nombre = " Comprobantes";
                        }  
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca".$nombre;
                            if ($Operacion==="btSucursal")    
                            {   
                                $jsonModal['Carga'] = $this->RenderSucursal->CreaModalGridSucursal($Operacion,$Datos);
                            }else{
                                $jsonModal['Carga'] = $this->RenderTipoComprobante->CreaModalGridTipoComprobante($Operacion,$Datos);
                            }
                            $jsonModal['Ancho'] = "517";
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                                
                function ConsultaModalGridSucursalEmisionByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        if ($Operacion==="btSucursal")    
                        {   $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                            return $this->RenderSucursal->MuestraModalGridSucursal($ajaxResp,$Operacion,$Datos);
                        }else{
                            $Datos = $this->ServicioTipoComprobante->BuscarTipoComprobanteByDescripcion($prepareDQL);
                            return $this->RenderTipoComprobante->MuestraModalGridTipoComprobante($ajaxResp,$Operacion,$Datos);     
                        }
                }
         }

?>

