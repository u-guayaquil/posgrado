<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/impuesto/servicio/ServicioTipoComprobante.php");
        require_once("src/rules/impuesto/servicio/ServicioTipoEmision.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/DateControl.php");
         
        class RenderSucursalEmision
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
               
              function CreaSucursalEmisionMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);
                       $FrmSucursal = "tipoemision:idTipoComprobante:txTipoComprobante:idSucursal:txSucursal"
                                    . ":serie1:serie2:secinicio:secfinal:autorizacion:fevigencia:fecaducidad"
                                    . ":ambiente:ejecucion:estado:fedesde:fehasta";
                       $SucursalEmision = '<table class="Form-Frame" cellpadding="0">';
                       $SucursalEmision.= '       <tr height="30">';
                       $SucursalEmision.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:25%">';
                       $SucursalEmision.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\''.$FrmSucursal.'\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:14%">Emisi&oacute;n</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:36%">';
                                                    $SucursalEmision.= $this->CreaComboTipoEmision();
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '         </tr>';
                       $SucursalEmision.= '       <tr height="28">';
                       $SucursalEmision.= '           <td class="Form-Label">Comprobante</td>';
                       $SucursalEmision.= '           <td class="Form-Label" colspan="3">';
                                      $SucursalEmision.= $Search->TextSch("TipoComprobante","","")->Enabled(false)->Create("t05");
                       $SucursalEmision.= '           </td>';
                       $SucursalEmision.= '       </tr>';
                       $SucursalEmision.= '       <tr height="28">';
                       $SucursalEmision.= '           <td class="Form-Label">Sucursal</td>';
                       $SucursalEmision.= '           <td class="Form-Label" colspan="3">';
                                      $SucursalEmision.= $Search->TextSch("Sucursal","","")->Enabled(false)->Create("t05");
                       $SucursalEmision.= '           </td>';
                       $SucursalEmision.= '       </tr>';
                       $SucursalEmision.= '         <tr height="28">';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:25%">Serie 1</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:25%">';
                       $SucursalEmision.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t03" id="serie1'.$this->Sufijo.'" name="serie1'.$this->Sufijo.'" maxlength="3" disabled/>';
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:14%">Serie 2</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:36%">';
                       $SucursalEmision.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t03" id="serie2'.$this->Sufijo.'" name="serie2'.$this->Sufijo.'" maxlength="3" disabled/>';
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '         </tr>';
                       $SucursalEmision.= '         <tr height="28">';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:25%">Sec. Inicio</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:25%">';
                       $SucursalEmision.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t03" id="secinicio'.$this->Sufijo.'" name="secinicio'.$this->Sufijo.'" maxlength="3" disabled/>';
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:14%">Sec. Final</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:36%">';
                       $SucursalEmision.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t03" id="secfinal'.$this->Sufijo.'" name="secfinal'.$this->Sufijo.'" maxlength="3" disabled/>';
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '         </tr>';
                       $SucursalEmision.= '         <tr height="28">';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:25%">Autorizaci&oacute;n</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:50%" colspan="3">';
                       $SucursalEmision.= '                 <input type="text" class="txt-upper t06" id="autorizacion'.$this->Sufijo.'" name="autorizacion'.$this->Sufijo.'" maxlength="10" disabled/>';
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '         </tr>';
                       $SucursalEmision.= '         <tr height="28">';
                       $SucursalEmision.= '             <td class="Form-Label">Fecha de Vigencia</td>';
                       $SucursalEmision.= '             <td class="Form-Label"><input class="txt-upper t04" name = "fevigencia'.$this->Sufijo.'" id = "fevigencia'.$this->Sufijo.'" disabled/></td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:25%">Ambiente</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:25%">';
                       $SucursalEmision.= '                 <select id="ambiente'.$this->Sufijo.'" name="ambiente'.$this->Sufijo.'" class="sel-input s04" disabled="">';
                       $SucursalEmision.= '                     <option value="0">NINGUNO</option>';
                       $SucursalEmision.= '                     <option value="1">PRUEBAS</option>';
                       $SucursalEmision.= '                     <option value="2">PRODUCCI&Oacute;N</option>';
                       $SucursalEmision.= '                 </select">';
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '         </tr>';
                       $SucursalEmision.= '         <tr height="28">';
                       $SucursalEmision.= '             <td class="Form-Label">Fecha de Caducidad</td>';
                       $SucursalEmision.= '             <td class="Form-Label"><input class="txt-upper t04" name = "fecaducidad'.$this->Sufijo.'" id = "fecaducidad'.$this->Sufijo.'" disabled/></td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:14%">Ejecuci&oacute;n</td>';
                       $SucursalEmision.= '             <td class="Form-Label" style="width:36%">';
                       $SucursalEmision.= '                 <select id="ejecucion'.$this->Sufijo.'" name="ejecucion'.$this->Sufijo.'" class="sel-input s04" disabled="">';
                       $SucursalEmision.= '                     <option value="0">NINGUNO</option>';
                       $SucursalEmision.= '                     <option value="1">NORMAL</option>';
                       $SucursalEmision.= '                     <option value="2">OFFLINE</option>';
                       $SucursalEmision.= '                 </select">';
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '         </tr>';
                       $SucursalEmision.= '         <tr height="28">';
                       $SucursalEmision.= '             <td class="Form-Label">Estado</td>';
                       $SucursalEmision.= '             <td class="Form-Label">';
                                                    $SucursalEmision.= $this->CreaComboEstado();
                       $SucursalEmision.= '             </td>';
                       $SucursalEmision.= '         </tr>';
                       $SucursalEmision.= '</table>';
                       return $SucursalEmision;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Sucursal']   = array('150px','left','');
                        $Columns['Emisi&oacute;n'] = array('150px','left','');
                        $Columns['Autorizaci&oacute;n'] = array('150px','left','');
                        $Columns['idTipoEmision'] = array('0px','left','none');
                        $Columns['idTipoComp'] = array('0px','left','none');
                        $Columns['Comprobante'] = array('100px','left','');
                        $Columns['idSucursal'] = array('0px','left','none');
                        $Columns['Sucursal'] = array('0px','left','none');
                        $Columns['serie1'] = array('0px','left','none');
                        $Columns['serie2'] = array('0px','left','none');
                        $Columns['secini'] = array('0px','left','none');
                        $Columns['secfin'] = array('0px','left','none');
                        $Columns['autorizacion'] = array('0px','left','none');
                        $Columns['fecvigencia'] = array('0px','left','none');
                        $Columns['feccaducidad'] = array('0px','left','none');
                        $Columns['ambiente'] = array('0px','left','none');
                        $Columns['ejecucion'] = array('0px','left','none');
                        $Columns['estado'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaSucursalEmisionSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->SucursalEmisionGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }        

              private function SucursalEmisionGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $SucursalEmision)
                      {       
                               $id = str_pad($SucursalEmision['id'],2,'0',STR_PAD_LEFT);
                               $fevigencia = $this->Fechas->changeFormatDate($SucursalEmision['fevigencia'],"DMY")[1];
                               $fecaducidad = $this->Fechas->changeFormatDate($SucursalEmision['fecaducidad'],"DMY")[1];
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['sucursal']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['tipoemision']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['autorizacion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['idemidocs']));                           
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['idtipocomp']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['tipocomprobante']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['idsucursal']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['serie1']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['serie2']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['sec_inicio']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['sec_fin']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fevigencia);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecaducidad);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['idambiente']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['idejecucion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($SucursalEmision['idestado']));
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($SucursalEmision['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              private function CreaComboTipoEmision()
              {       
                       $datosTipoEmision= $this->TraeDatosTipoEmision();
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("tipoemision",$datosTipoEmision)->Selected(1)->Create("s04");
              }
              
              function TraeDatosTipoEmision()
              {        
                       $ServicioTipoEmision = new ServicioTipoEmision();
                       $datoTipoEmision = $ServicioTipoEmision->BuscarTipoEmisionCombo();
                       return $datoTipoEmision;
              }
              
              function MuestraSucursalEmision($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){     
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("tipoemision".$this->Sufijo,"value", trim($Datos[0]['idemidocs']));
                            $Ajax->Assign("idTipoComprobante".$this->Sufijo,"value", trim($Datos[0]['idtipocomp']));
                            $Ajax->Assign("txTipoComprobante".$this->Sufijo,"value", trim($Datos[0]['tipocomprobante']));
                            $Ajax->Assign("idSucursal".$this->Sufijo,"value", trim($Datos[0]['idsucursal']));
                            $Ajax->Assign("txSucursal".$this->Sufijo,"value", trim($Datos[0]['sucursal']));
                            $Ajax->Assign("serie1".$this->Sufijo,"value", trim($Datos[0]['serie1']));
                            $Ajax->Assign("serie2".$this->Sufijo,"value", trim($Datos[0]['serie2']));
                            $Ajax->Assign("secinicio".$this->Sufijo,"value", trim($Datos[0]['sec_inicio']));
                            $Ajax->Assign("secfinal".$this->Sufijo,"value", trim($Datos[0]['sec_fin']));
                            $Ajax->Assign("autorizacion".$this->Sufijo,"value", trim($Datos[0]['autorizacion'])); 
                            $Ajax->Assign("fevigencia".$this->Sufijo,"value", trim($this->Fechas->changeFormatDate($Datos[0]['fevigencia'],"DMY")[1]));
                            $Ajax->Assign("fecaducidad".$this->Sufijo,"value", trim($this->Fechas->changeFormatDate($Datos[0]['fecaducidad'],"DMY")[1]));
                            $Ajax->Assign("ambiente".$this->Sufijo,"value", trim($Datos[0]['idambiente']));
                            $Ajax->Assign("ejecucion".$this->Sufijo,"value", trim($Datos[0]['idejecucion']));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                       }else{
                            return $this->RespuestaSucursalEmision($Ajax,"NOEXISTEID","No existe una Emision con ese ID.");
                       }
                       return $Ajax;
              }
              function MuestraSucursalEmisionGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->SucursalEmisionGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }
               
              function MuestraSucursalEmisionGuardado($Ajax,$SucursalEmision)
              {       if (count($SucursalEmision)>0)
                        {   $id = str_pad($SucursalEmision[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraSucursalEmisionGrid($Ajax,$SucursalEmision);
                            return $this->RespuestaSucursalEmision($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaSucursalEmision($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraSucursalEmisionEditado($Ajax,$SucursalEmision)
              {       foreach ($SucursalEmision as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraSucursalEmisionRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaSucursalEmision($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaSucursalEmision($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraSucursalEmisionEliminado($Ajax,$SucursalEmision)
              {        foreach ($SucursalEmision as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraSucursalEmisionRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaSucursalEmision($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaSucursalEmision($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraSucursalEmisionRowGrid($Ajax,$SucursalEmision,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($SucursalEmision['id'],$estado); 
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",trim($SucursalEmision['id']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($SucursalEmision['sucursal']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($SucursalEmision['tipoemision']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($SucursalEmision['autorizacion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($SucursalEmision['idemidocs']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($SucursalEmision['idtipocomp']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($SucursalEmision['tipocomprobante']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($SucursalEmision['idsucursal']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($SucursalEmision['serie1']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",trim($SucursalEmision['serie2']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",trim($SucursalEmision['sec_inicio']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],11),"innerHTML",trim($SucursalEmision['sec_fin']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],12),"innerHTML",trim($this->Fechas->changeFormatDate($SucursalEmision['fevigencia'],"DMY")[1]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],13),"innerHTML",trim($this->Fechas->changeFormatDate($SucursalEmision['fecaducidad'],"DMY")[1]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],14),"innerHTML",trim($SucursalEmision['idambiente']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],15),"innerHTML",trim($SucursalEmision['idejecucion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],16),"innerHTML",trim($SucursalEmision['idestado']));
                        return $Ajax;
                }

              function MuestraSucursalEmisionExcepcion($Ajax,$Msg)
                {       return $this->RespuestaSucursalEmision($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaSucursalEmision($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
              /*** MODAL ***/
                private function SearchGridModalConfig()
                {       $Columns['Id']     = array('40px','center','');
                        $Columns['Ciudad']  = array('100px','center','');
                        $Columns['Descripcion'] = array('150px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGridSucursalEmision($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalSucursalEmision($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGridSucursalEmision($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalSucursalEmision($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalSucursalEmision($Grid,$Datos)
                {       foreach ($Datos as $Opcion)
                        {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['ciudad']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['descripcion']));
                                $Grid->CreaSearchRowsDetalle ($id,"black");
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
                
                function CreaModalExcelSucursalEmision()
                {       $SucursalEmision = '<table class="Form-Frame" cellpadding="0">';
                        $SucursalEmision.= '</table>';
                        return $SucursalEmision;
                }
        }
?>

