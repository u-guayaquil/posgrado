<?php
        $Sufijo = '_Ciudad';

        require_once('src/modules/general/ciudad/controlador/ControlCiudad.php');
        $ControlCiudad = new ControlCiudad($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlCiudad,'GuardaCiudad'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlCiudad,'ConsultaCiudadByID'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlCiudad,'ConsultaCiudadByTX'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlCiudad,'EliminaCiudad'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlCiudad,'CargaModalGridCiudad'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlCiudad,'ConsultaModalGridCiudadByTX')); 
        $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/general/ciudad/js/JSCiudad.js"></script>
         </head>
         <body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlCiudad->CargaCiudadBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlCiudad->CargaCiudadMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlCiudad->CargaCiudadSearchGrid();  ?>
              </div>  
         </div>
         </body>
</html>
              

