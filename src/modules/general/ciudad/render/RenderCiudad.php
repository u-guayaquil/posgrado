<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioCiudad.php");
        require_once("src/rules/general/servicio/ServicioProvincia.php");
        require_once('src/libs/clases/ComboBox.php');        
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php"); 
        require_once('src/libs/clases/SearchInput.php');     
         
        class RenderCiudad
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 3; 
                }
              
                private function SearchGridValues()
                {       $Columns['Id'] = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('50px','left','');
                        $Columns['Ciudad']      = array('210px','left','');
                        $Columns['Provincia']   = array('193px','left','');
                        $Columns['idProvincia'] = array('0px','left','none');
                        $Columns['idEstado']    = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }
              
                function CreaCiudadMantenimiento()
                {       $Search = new SearchInput($this->Sufijo);        
                        $Ciudad = '<table class="Form-Frame" cellpadding="0" border="0">';
                        $Ciudad.= '       <tr height="28">';
                        $Ciudad.= '           <td class="Form-Label" style="width:17%">Id</td>';
                        $Ciudad.= '           <td class="Form-Label" style="width:52%">';
                        $Ciudad.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo:idprovincia:txprovincia\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Ciudad.= '           </td>';
                        $Ciudad.= '           <td class="Form-Label" style="width:14%"></td>';
                        $Ciudad.= '           <td class="Form-Label" style="width:17%"></td>';
                        $Ciudad.= '       </tr>';
                        $Ciudad.= '       <tr height="28">';
                        $Ciudad.= '           <td class="Form-Label">Ciudad</td>';
                        $Ciudad.= '           <td class="Form-Label">';
                        $Ciudad.= '               <input type="text" class="txt-upper t10" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="40" disabled/>';
                        $Ciudad.= '           </td>';
                        $Ciudad.= '           <td class="Form-Label">C&oacute;digo</td>';
                        $Ciudad.= '           <td class="Form-Label">';
                        $Ciudad.= '               <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="10" onKeyDown="return soloNumerosLetras(event); " disabled/>';
                        $Ciudad.= '           </td>';
                        $Ciudad.= '       </tr>';
                        $Ciudad.= '       <tr height="28">';
                        $Ciudad.= '           <td class="Form-Label">Provincia</td>';
                        $Ciudad.= '           <td class="Form-Label">';
                        $Ciudad.=             $Search->TextSch("provincia","","")->Enabled(false)->Create("t09");
                        $Ciudad.= '           </td>';
                        $Ciudad.= '       </tr>';
                        $Ciudad.= '       <tr height="28">';
                        $Ciudad.= '           <td class="Form-Label">Estado</td>';
                        $Ciudad.= '           <td class="Form-Label">'; 
                        $Ciudad.=             $this->CreaComboEstado();
                        $Ciudad.= '           </td>';
                        $Ciudad.= '       </tr>';
                        $Ciudad.= '</table>';
                        return $Ciudad;
                }

                function CreaCiudadSearchGrid($Datos)
                {      $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->CiudadGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
                }

                private function CreaComboEstado()
                {       $Select = new ComboBox($this->Sufijo);                        
                        $ServicioEstado = new ServicioEstado();
                        $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                        return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
                }
              
                function MuestraCiudad($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Dato['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));
                            $Ajax->Assign("idprovincia".$this->Sufijo,"value", $Dato['idprovincia']);  
                            $Ajax->Assign("txprovincia".$this->Sufijo,"value", trim($Dato['provincia']));                       
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaCiudad($Ajax,"NOEXISTEID","No existe una Ciudad con ese ID.");
                }
                
                function MuestraCiudadGrid($Ajax,$Datos)
                {      $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->CiudadGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
                }        

                private function CiudadGridHTML($ObjSchGrid,$Datos)
                {       $Quiebre = "";
                        foreach ($Datos as $ciudad)
                        {       $id = str_pad($ciudad['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                if ($Quiebre != trim($ciudad['pais'])){
                                    $Quiebre = trim($ciudad['pais']);
                                    $ObjSchGrid->CreaSearchCellsDetalle('Q'.$id,'',"->");
                                    $ObjSchGrid->CreaSearchCellsDetalle('Q'.$id,'',"<b>".$Quiebre."</b>",5);
                                    $ObjSchGrid->CreaSearchRowsDetalle ('Q'.$id,"black");
                                }
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ciudad['codigo']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ciudad['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ciudad['provincia'])); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ciudad['idprovincia']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ciudad['idestado']);
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($ciudad['idestado']==0 ? "red" : ""));
                        }   
                        return $ObjSchGrid->CreaSearchTableDetalle();
                }
               
                function MuestraCiudadGuardado($Ajax,$Ciudad)
                {       foreach ($Ciudad as $dato) 
                        {   $id = str_pad($dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraCiudadGrid($Ajax,$Ciudad);
                            return $this->RespuestaCiudad($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaCiudad($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraCiudadEditado($Ajax,$Ciudad)
                {       foreach ($Ciudad as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraCiudadRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaCiudad($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaCiudad($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
              
                function MuestraCiudadEliminado($Ajax,$Ciudad)
                {       foreach ($Ciudad as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraCiudadRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaCiudad($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->RespuestaCiudad($xAjax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraCiudadRowGrid($Ajax,$Ciudad,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Ciudad['id'],$estado);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Ciudad['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Ciudad['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Ciudad['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Ciudad['provincia']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$Ciudad['idprovincia']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$Ciudad['idestado']);
                        return $Ajax;
                }

                function MuestraCiudadExcepcion($Ajax,$Msg)
                {       return $this->RespuestaCiudad($Ajax,"EXCEPCION",$Msg);    
                }

                private function RespuestaCiudad($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
            
                /**** Modal ****/
                private function SearchGridModalValues()
                {       $Columns['Id']            = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('50px','left','');
                        $Columns['Ciudad']        = array('220px','left','');
                        $Columns['Provincia']     = array('168px','left','');
                        $Columns['Pais']          = array('100px','left','');
                        $Columns['idProvincia']   = array('0px','left','none');
                        $Columns['idPais']        = array('0px','left','none');
                        $Columns['idEstado']      = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
                }
                
                function WidthModalGrid()
                {       return 600;
                }
                
                function CreaModalGridCiudad($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalValues());
                        $ObjHTML = $this->CiudadModalGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }       

                function MuestraModalGridCiudad($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalValues());
                        $ObjHTML = $this->CiudadModalGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }        
                
                private function CiudadModalGridHTML($ObjSchGrid,$Datos)
                {       foreach ($Datos as $ciudad)
                        {       $id = str_pad($ciudad['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $fecreacion = $this->Fechas->changeFormatDate($ciudad['fecreacion'],"DMY");
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ciudad['codigo']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ciudad['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ciudad['provincia'])); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ciudad['pais'])); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ciudad['idprovincia']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ciudad['idpais']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ciudad['idestado']);
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($ciudad['idestado']==0 ? "red" : ""));
                        }   
                        return $ObjSchGrid->CreaSearchTableDetalle();
                }
        }
?>

