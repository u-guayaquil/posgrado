<?php
         require_once("src/rules/general/servicio/ServicioCiudad.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");  
         require_once("src/rules/general/servicio/ServicioProvincia.php");
         require_once("src/modules/general/ciudad/render/RenderCiudad.php"); 
         require_once("src/modules/general/provincia/render/RenderProvincia.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlCiudad
         {     private  $Sufijo; 
               private  $ServicioCiudad;
               private  $ServicioProvincia;
               private  $RenderCiudad;
               private  $RenderProvincia;
               
               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioCiudad = new ServicioCiudad();
                        $this->ServicioProvincia = new ServicioProvincia();
                        $this->RenderCiudad = new RenderCiudad($Sufijo);
                        $this->RenderProvincia = new RenderProvincia($Sufijo);
               }

               function CargaCiudadBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaCiudadMantenimiento()
               {        echo $this->RenderCiudad->CreaCiudadMantenimiento();
               }

               function CargaCiudadSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '','orden'=>'pa.descripcion');
                        $datoCiudad = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                        echo $this->RenderCiudad->CreaCiudadSearchGrid($datoCiudad);
               }         
               
               function ConsultaCiudadByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $datoCiudad = $this->ServicioCiudad->BuscarCiudadByDescripcion(array('id' => intval($id)));
                        return $this->RenderCiudad->MuestraCiudad($ajaxRespon,$datoCiudad);
               }

               function ConsultaCiudadByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'orden'=>'pa.descripcion');
                        $datoCiudad = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                        return $this->RenderCiudad->MuestraCiudadGrid($ajaxRespon,$datoCiudad);
               }

               function GuardaCiudad($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Ciudad  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioCiudad->GuardaDBCiudad($Ciudad);
                        if (is_numeric($id)){
                            $function = (empty($Ciudad->id) ? "MuestraCiudadGuardado" : "MuestraCiudadEditado");
                            $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion(array('id' => intval($id)));
                            return $this->RenderCiudad->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderCiudad->MuestraCiudadExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaCiudad($id)
                {       $ajaxRespon = new xajaxResponse();
                        
                        $this->ServicioCiudad->DesactivaCiudad(intval($id));
                        $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion(array('id' => intval($id)));
                        return $this->RenderCiudad->MuestraCiudadEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridCiudad($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '','estado' =>1);
                        $Datos = $this->ServicioProvincia->BuscarProvinciaByDescripcion($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Buscar Provincia";
                        $jsonModal['Carga'] = $this->RenderProvincia->CreaModalGridProvincia($Operacion,$Datos);
                        $jsonModal['Ancho'] = $this->RenderProvincia->WidthModalGrid();
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                         
                function ConsultaModalGridCiudadByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioProvincia->BuscarProvinciaByDescripcion($prepareDQL);
                        return $this->RenderProvincia->MuestraModalGridProvincia($ajaxResp,$Operacion,$Datos);                                        
                }
         }

?>

