        function ButtonClick_Ciudad(Sufijo, Operacion)
        {   var objCiudad = "id:descripcion:estado:idprovincia:txprovincia:codigo";
            var frmCiudad = "descripcion:estado:idprovincia:txprovincia:codigo";

            if (Operacion == 'addNew')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, frmCiudad, "id");
                ElementClear(Sufijo, objCiudad);
                BarButtonStateEnabled(Sufijo, "btprovincia");
            }
            else if (Operacion == 'addMod')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, frmCiudad, "id");
                BarButtonStateEnabled(Sufijo, "btprovincia");
                if(ElementGetValue(Sufijo,"estado")==='0'){
                    ElementStatus(Sufijo, "estado", "");
                }else{
                    ElementStatus(Sufijo, "", "estado");
                }
            }
            else if (Operacion == 'addDel')
            {   if (ElementGetValue(Sufijo, "estado") == 1)
                {   if (confirm("Desea inactivar este registro?"))
                    xajax_Elimina_Ciudad(ElementGetValue(Sufijo, "id"));
                } 
                else
                alert("El registro se encuentra inactivo");
            } 
            else if (Operacion == 'addSav')
            {   if (ElementValidateBeforeSave(Sufijo, frmCiudad))
                {   if (BarButtonState(Sufijo, "Inactive"))
                    {   var Forma = PrepareElements(Sufijo, objCiudad);
                        xajax_Guarda_Ciudad(JSON.stringify({Forma}));
                    }
                }
            } 
            else if (Operacion == 'addCan')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, "id", frmCiudad);
                ElementClear(Sufijo, objCiudad);
                BarButtonStateDisabled(Sufijo, "btprovincia");
            }
            else 
            xajax_CargaModal_Ciudad(Operacion);
            return false;
        }

        function SearchByElement_Ciudad(Sufijo, Elemento)
        {   Elemento.value = TrimElement(Elemento.value);
            if (Elemento.name == "id" + Sufijo)
            {   if (Elemento.value.length != 0)
                {   ContentFlag = document.getElementById("descripcion" + Sufijo);
                    if (ContentFlag.value.length == 0)
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        xajax_MuestraByID_Ciudad(Elemento.value);
                    }
                }
                else
                BarButtonState(Sufijo, "Default");
            } 
            else if (Elemento.id=="FindProvinciaTextBx"+Sufijo)
            {   xajax_BuscaModalByTX_Ciudad("btprovincia",Elemento.value);
            }    
            else    
            xajax_MuestraByTX_Ciudad(Elemento.value);
        }

        function SearchGetData_Ciudad(Sufijo, Grilla)
        {   if (Grilla.cells[0].childNodes[0].nodeValue!="->")
            {   if (IsDisabled(Sufijo,"addSav"))
                {   BarButtonState(Sufijo, "Active");
                    document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                    document.getElementById("codigo" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                    document.getElementById("descripcion" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                    document.getElementById("txprovincia" + Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                    document.getElementById("idprovincia" + Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
                    document.getElementById("estado" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                }    
            }    
            return false;
        }

        function SearchProvinciaGetData_Ciudad(Sufijo, DatosGrid)
        {   document.getElementById("idprovincia" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txprovincia" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
        }

        function XAJAXResponse_Ciudad(Sufijo, Mensaje, Datos)
        {   var objCiudad = "descripcion:estado:codigo:idprovincia:txprovincia";
            if (Mensaje === "GUARDADO")
            {
                BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, "btprovincia");
                ElementSetValue(Sufijo, "id", Datos);
                ElementStatus(Sufijo, "id", objCiudad);
                alert('Los datos se guardaron correctamente.');
            } 
            else if (Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
                alert("Los Datos se eliminaron correctamente");
            } 
            else if (Mensaje === 'NOEXISTEID')
            {   BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "id");
                alert(Datos);
            } 
            else if (Mensaje === 'EXCEPCION')
            {   BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
        }