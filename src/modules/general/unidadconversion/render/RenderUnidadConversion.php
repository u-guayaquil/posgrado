<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/entidad/Estado.php");
        require_once("src/rules/general/servicio/ServicioUnidad.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderUnidadConversion
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Maxlen['id'] = 2; 
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaUnidadConversionMantenimiento()
              {                          
                       $UnidadConversion = '<table class="Form-Frame" cellpadding="0">';
                       $UnidadConversion.= '         <tr height="30">';
                       $UnidadConversion.= '             <td class="Form-Label" style="width:10%">&nbsp;&nbsp;&nbsp;Id</td>';
                       $UnidadConversion.= '             <td class="Form-Label" style="width:30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Origen</td>';
                       $UnidadConversion.= '             <td class="Form-Label" style="width:30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Destino</td>';
                       $UnidadConversion.= '             <td class="Form-Label" style="width:30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Factor</td>';
                       $UnidadConversion.= '         </tr>';
                       $UnidadConversion.= '         <tr height="30">';
                       $UnidadConversion.= '             <td class="Form-Label">';
                       $UnidadConversion.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:abreviatura\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $UnidadConversion.= '             </td>';
                       $UnidadConversion.= '             <td class="Form-Label">';
                                                   $UnidadConversion.= $this->CreaComboOrigen();
                       $UnidadConversion.= '             </td>';
                       $UnidadConversion.= '             <td class="Form-Label">';
                                                   $UnidadConversion.= $this->CreaComboDestino();
                       $UnidadConversion.= '             </td>';
                       $UnidadConversion.= '             <td class="Form-Label">';
                       $UnidadConversion.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t03" id="factor'.$this->Sufijo.'" name="factor'.$this->Sufijo.'" value="" maxlength="5" disabled/>';
                       $UnidadConversion.= '             </td>';
                       $UnidadConversion.= '         </tr>';                       
                       $UnidadConversion.= '</table>';
                       return $UnidadConversion;
              }
              
              private function SearchGridValues()
              {        $Columns['Id']       = array('30px','center','');
                       $Columns['Origen']   = array('100px','left','');
                       $Columns['Destino'] = array('100px','left','');
                       $Columns['Factor'] = array('150px','left','');
                       $Columns['idOrigen']   = array('0px','left','none');
                       $Columns['idDestino']   = array('0px','left','none');

                       return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaUnidadConversionSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->UnidadConversionGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function UnidadConversionGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $UnidadConversion)
                      {        
                               $id = str_pad($UnidadConversion['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($UnidadConversion['origen']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($UnidadConversion['destino']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$UnidadConversion['und_factor']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$UnidadConversion['und_origen']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$UnidadConversion['und_destino']);
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,"");

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }
              
              function TraeDatosUnidadByArray($IdArray)
              {        
                       $ServicioUnidad = new ServicioUnidad();
                       $datoUnidad = $ServicioUnidad->BuscarUnidadByArrayID($IdArray);
                       return $datoUnidad;
              }
              
              private function CreaComboOrigen()
              {       
                       $datosOrigen= $this->TraeDatosUnidadByArray(array('estado'=>1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("Origen",$datosOrigen)->Selected(1)->Create("s04");
              }
              
              private function CreaComboDestino()
              {       
                       $datosDestino= $this->TraeDatosUnidadByArray(array('estado'=>1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("Destino",$datosDestino)->Selected(1)->Create("s04");
              }
              
              function MuestraUnidadConversion($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("Origen".$this->Sufijo,"value", trim($Datos[0]['origen']));
                            $Ajax->Assign("Destino".$this->Sufijo,"value", trim($Datos[0]['destino']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos->getIdestado());
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaUnidadConversion($Ajax,"NOEXISTEID","No existe una Conversion con ese ID.");
                       }
              }
              
              function MuestraUnidadConversionGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->UnidadConversionGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }   
               
              function MuestraUnidadConversionGuardado($Ajax,$UnidadConversion)
              {       if (count($UnidadConversion)>0)
                        {   $id = str_pad($UnidadConversion[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraUnidadConversionGrid($Ajax,$UnidadConversion);
                            return $this->RespuestaUnidadConversion($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaUnidadConversion($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraUnidadConversionEditado($Ajax,$UnidadConversion)
              {       foreach ($UnidadConversion as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraUnidadConversionRowGrid($Ajax, $datos);
                                return $this->RespuestaUnidadConversion($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaUnidadConversion($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraUnidadConversionEliminado($Ajax,$Datos)
              {        $xAjax = $this->MuestraUnidadConversionRowGrid($Ajax,$Datos,0);   
                       return $this->RespuestaUnidadConversion($xAjax,"ELIMINADO","El registro fue eliminado con éxito.");
              }
              
              function MuestraUnidadConversionRowGrid($Ajax,$UnidadConversion,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($UnidadConversion['id'],$estado);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($UnidadConversion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($UnidadConversion['origen']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($UnidadConversion['destino']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($UnidadConversion['und_factor']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($UnidadConversion['und_origen']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($UnidadConversion['und_destino']));
                        return $Ajax;
                }

              function MuestraUnidadConversionExcepcion($Ajax,$Msg)
                {       return $this->RespuestaUnidadConversion($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaUnidadConversion($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

