<?php
         require_once("src/rules/general/servicio/ServicioUnidadConversion.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/general/unidadconversion/render/RenderUnidadConversion.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlUnidadConversion
         {     private  $Sufijo; 
               private  $ServicioUnidadConversion;
               private  $RenderUnidadConversion;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioUnidadConversion = new ServicioUnidadConversion();
                        $this->RenderUnidadConversion = new RenderUnidadConversion($Sufijo);
               }

               function CargaUnidadConversionBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaUnidadConversionMantenimiento()
               {        echo $this->RenderUnidadConversion->CreaUnidadConversionMantenimiento();
               }

               function CargaUnidadConversionSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoUnidadConversion = $this->ServicioUnidadConversion->BuscarUnidadConversionByDescripcion($prepareDQL);
                        echo $this->RenderUnidadConversion->CreaUnidadConversionSearchGrid($datoUnidadConversion);
               }         
               
               function MuestraUnidadConversionByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoUnidadConversion = $this->ServicioUnidadConversion->BuscarUnidadConversionByID($prepareDQL);
                        return $this->RenderUnidadConversion->MuestraUnidadConversion($ajaxRespon,$datoUnidadConversion);
               }

               function MuestraUnidadConversionByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoUnidadConversion = $this->ServicioUnidadConversion->BuscarUnidadConversionByDescripcion($prepareDQL);
                        return $this->RenderUnidadConversion->MuestraUnidadConversionGrid($ajaxRespon,$datoUnidadConversion);
               }

               function GuardaUnidadConversion($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $UnidadConversion  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioUnidadConversion->GuardaDBUnidadConversion($UnidadConversion);
                        if (is_numeric($id)){
                            $function = (empty($UnidadConversion->id) ? "MuestraUnidadConversionGuardado" : "MuestraUnidadConversionEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioUnidadConversion->BuscarUnidadConversionByID($prepareDQL);
                            return $this->RenderUnidadConversion->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderUnidadConversion->MuestraUnidadConversionExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaUnidadConversion($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioUnidadConversion->DesactivaUnidadConversion(intval($id));
                        $Datos = $this->ServicioUnidadConversion->BuscarUnidadConversionByDescripcion($prepareDQL);
                        return $this->RenderUnidadConversion->MuestraDespuesdeEliminar($ajaxRespon,$Datos);
                }
         }

?>

