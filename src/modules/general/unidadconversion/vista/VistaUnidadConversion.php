<?php
         $Sufijo = '_UnidadConversion';

         require_once('src/modules/general/unidadconversion/controlador/ControlUnidadConversion.php');
         $ControlUnidadConversion = new ControlUnidadConversion($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlUnidadConversion,'GuardaUnidadConversion'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlUnidadConversion,'MuestraUnidadConversionByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlUnidadConversion,'MuestraUnidadConversionByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlUnidadConversion,'EliminaUnidadConversion'));
         $xajax->processRequest();

?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript">
                 function ButtonClick_UnidadConversion(Sufijo,Operacion)
                 {        var objUnidadConversion = "id:Origen:Destino:factor";
                          var frmUnidadConversion = "Origen:Destino:factor";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,frmUnidadConversion,"id");
                              ElementClear(Sufijo,objUnidadConversion);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,frmUnidadConversion,"id");
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_UnidadConversion(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,frmUnidadConversion))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objUnidadConversion);
                                       xajax_Guarda_UnidadConversion(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmUnidadConversion);
                               ElementClear(Sufijo,objUnidadConversion);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_UnidadConversion(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_UnidadConversion(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_UnidadConversion(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_UnidadConversion(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")) {
                                BarButtonState(Sufijo,"Active");

                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                                document.getElementById("Origen"+Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
                                document.getElementById("Destino"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                                document.getElementById("factor"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                          }
                          return false;
                 }
                 
                 function XAJAXResponse_UnidadConversion(Sufijo,Mensaje,Datos)
                 {       
                        var objUnidadConversion = "Origen:Destino:factor";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objUnidadConversion);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
<body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlUnidadConversion->CargaUnidadConversionBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlUnidadConversion->CargaUnidadConversionMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlUnidadConversion->CargaUnidadConversionSearchGrid();  ?>
              </div>  
         </div>
        </body>
</html> 
              

