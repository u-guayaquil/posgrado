<?php
         require_once("src/rules/general/servicio/ServicioCliente.php");
         require_once("src/rules/general/servicio/ServicioClienteComentario.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/general/clientecomentario/render/RenderClienteComentario.php");
         require_once("src/modules/general/cliente/render/RenderCliente.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlClienteComentario
         {     private  $Sufijo; 
               private  $ServicioClienteComentario;
               private  $ServicioCliente;
               private  $RenderClienteComentario;
               private  $RenderCliente;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioClienteComentario = new ServicioClienteComentario();
                        $this->ServicioCliente = new ServicioCliente();
                        $this->RenderClienteComentario = new RenderClienteComentario($Sufijo);
                        $this->RenderCliente = new RenderCliente($Sufijo);
               }

               function CargaClienteComentarioBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaClienteComentarioMantenimiento()
               {        echo $this->RenderClienteComentario->CreaClienteComentarioMantenimiento();
               }

               function CargaClienteComentarioSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoClienteComentario = $this->ServicioClienteComentario->BuscarClienteComentarioByDescripcion($prepareDQL);
                        echo $this->RenderClienteComentario->CreaClienteComentarioSearchGrid($datoClienteComentario);
               }         
               
               function MuestraClienteComentarioByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoClienteComentario = $this->ServicioClienteComentario->BuscarClienteComentarioByID($prepareDQL);
                        return $this->RenderClienteComentario->MuestraClienteComentario($ajaxRespon,$datoClienteComentario);
               }

               function MuestraClienteComentarioByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoClienteComentario = $this->ServicioClienteComentario->BuscarClienteComentarioByDescripcion($prepareDQL);
                        return $this->RenderClienteComentario->MuestraClienteComentarioGrid($ajaxRespon,$datoClienteComentario);
               }

               function GuardaClienteComentario($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $ClienteComentario  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioClienteComentario->GuardaDBClienteComentario($ClienteComentario);
                        if (is_numeric($id)){
                            $function = (empty($ClienteComentario->id) ? "MuestraClienteComentarioGuardado" : "MuestraClienteComentarioEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioClienteComentario->BuscarClienteComentarioByID($prepareDQL);
                            return $this->RenderClienteComentario->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderClienteComentario->MuestraClienteComentarioExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaClienteComentario($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioClienteComentario->DesactivaClienteComentario(intval($id));
                        $Datos = $this->ServicioClienteComentario->BuscarClienteComentarioByDescripcion($prepareDQL);
                        return $this->RenderClienteComentario->MuestraDespuesdeEliminar($ajaxRespon,$Datos);
                }
                
               function CargaModalClienteGeneral($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                        
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Clientes ";
                        $jsonModal['Carga'] = $this->RenderCliente->CreaModalClienteGeneral($Operacion);
                        $jsonModal['Ancho'] = "300";
                        
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function MuestraClienteByTX($texto,$Operacion)
                {        $ajaxRespon = new xajaxResponse();                   
                         $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                         $datoCliente = $this->ServicioCliente->BuscarCliente($prepareDQL);
                         return $this->RenderCliente->MuestraModalGridClienteGeneral($ajaxRespon,$Operacion,$datoCliente);
                }
         }

?>

