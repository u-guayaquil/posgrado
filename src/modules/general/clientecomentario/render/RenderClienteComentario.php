<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/DateControl.php");
         
        class RenderClienteComentario
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
               
              function CreaClienteComentarioMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);                 
                       $ClienteComentario = '<table class="Form-Frame" cellpadding="0">';
                       $ClienteComentario.= '       <tr height="30">';
                       $ClienteComentario.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $ClienteComentario.= '             <td class="Form-Label" style="width:75%">';
                       $ClienteComentario.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo:cambio\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $ClienteComentario.= '             </td>';
                       $ClienteComentario.= '         </tr>';
                       $ClienteComentario.= '         <tr height="30">';
                       $ClienteComentario.= '           <td class="Form-Label">Cliente</td>';
                       $ClienteComentario.= '           <td class="Form-Label" colspan="3">';
                                      $ClienteComentario.= $Search->TextSch("Cliente","","")->Enabled(false)->Create("t07");
                       $ClienteComentario.= '           </td>';
                       $ClienteComentario.= '         </tr>';
                       $ClienteComentario.= '         <tr height="30">';
                       $ClienteComentario.= '             <td class="Form-Label">Fecha</td>';
                       $ClienteComentario.= '             <td class="Form-Label"><input class="txt-upper t04" name = "fecha'.$this->Sufijo.'" id = "fecha'.$this->Sufijo.'" disabled/></td>';
                       $ClienteComentario.= '         </tr>';
                       $ClienteComentario.= '         <tr height="30">';
                       $ClienteComentario.= '             <td class="Form-Label" style="width:25%">Comentario</td>';
                       $ClienteComentario.= '             <td class="Form-Label" style="width:75%">';
                       $ClienteComentario.= '                 <textarea id="comentario'.$this->Sufijo.'" name="comentario'.$this->Sufijo.'" rows="2" cols="30" disabled></textarea>';
                       $ClienteComentario.= '             </td>';
                       $ClienteComentario.= '         </tr>';
                       $ClienteComentario.= '         <tr height="30">';
                       $ClienteComentario.= '             <td class="Form-Label">Estado</td>';
                       $ClienteComentario.= '             <td class="Form-Label">';
                                                   $ClienteComentario.= $this->CreaComboEstado();
                       $ClienteComentario.= '             </td>';
                       $ClienteComentario.= '         </tr>';
                       $ClienteComentario.= '</table>';
                       return $ClienteComentario;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Cliente']   = array('170px','left','');
                        $Columns['Comentario'] = array('150px','left','');
                        $Columns['Fecha'] = array('60px','left','');
                        $Columns['Creacion']   = array('120px','center','none');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        $Columns['idCliente']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaClienteComentarioSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->ClienteComentarioGridHTML($SearchGrid,$Datos); 
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }        

              private function ClienteComentarioGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $clientecom)
                      {        $fecreacion = $this->Fechas->changeFormatDate($clientecom['fecreacion'],"DMY")[1];
                               $fecha = $this->Fechas->changeFormatDate($clientecom['fecha'],"DMY")[1];
                               $id = str_pad($clientecom['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($clientecom['cliente']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($clientecom['comentario']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecha);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$clientecom['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$clientecom['idestado']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$clientecom['idcliente']);
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($clientecom['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraClienteComentario($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));
                            $Ajax->Assign("comentario".$this->Sufijo,"value", trim($$Datos[0]['comentario']));                       
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");

                            return $Ajax;
                       }else{
                            return $this->RespuestaClienteComentario($Ajax,"NOEXISTEID","No existe un comentario con ese ID.");
                       }
              }
              function MuestraClienteComentarioGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->ClienteComentarioGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }
               
              function MuestraClienteComentarioGuardado($Ajax,$ClienteComentario)
              {       if (count($ClienteComentario)>0)
                        {   $id = str_pad($ClienteComentario[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraClienteComentarioGrid($Ajax,$ClienteComentario);
                            return $this->RespuestaClienteComentario($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaClienteComentario($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraClienteComentarioEditado($Ajax,$ClienteComentario)
              {       foreach ($ClienteComentario as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraClienteComentarioRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaClienteComentario($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaClienteComentario($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraClienteComentarioEliminado($Ajax,$ClienteComentario)
              {        foreach ($ClienteComentario as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraClienteComentarioRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaClienteComentario($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaClienteComentario($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraClienteComentarioRowGrid($Ajax,$ClienteComentario,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($ClienteComentario['id'],$estado);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($ClienteComentario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($ClienteComentario['cliente']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($ClienteComentario['comentario']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($this->Fechas->changeFormatDate($ClienteComentario['fecha'],"DMY")[1]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($this->Fechas->changeFormatDate($ClienteComentario['fecreacion'],"DMY")[1]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($ClienteComentario['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($ClienteComentario['idestado'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($ClienteComentario['idcliente']));
                        return $Ajax;
                }

              function MuestraClienteComentarioExcepcion($Ajax,$Msg)
                {       return $this->RespuestaClienteComentario($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaClienteComentario($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

