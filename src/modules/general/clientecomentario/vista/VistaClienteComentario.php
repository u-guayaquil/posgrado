<?php
         $Sufijo = '_ClienteComentario';

         require_once('src/modules/general/clientecomentario/controlador/ControlClienteComentario.php');
         $ControlClienteComentario = new ControlClienteComentario($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlClienteComentario,'GuardaClienteComentario'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlClienteComentario,'MuestraClienteComentarioByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlClienteComentario,'MuestraClienteComentarioByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlClienteComentario,'EliminaClienteComentario'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlClienteComentario,'CargaModalClienteGeneral'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlClienteComentario,'ConsultaModalGridByTX'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraClienteByTX'.$Sufijo, $ControlClienteComentario,'MuestraClienteByTX'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript">
             
                 function CargaFechas_ClienteComentario(Sufijo){
                    $('#fecha'+Sufijo).datetimepicker({
                            timepicker:false,
                            format: 'd/m/Y'
                    });
                 }
             
                 function ButtonClick_ClienteComentario(Sufijo,Operacion)
                 {        var objClienteComentario = "id:comentario:estado:fecha:idCliente:txCliente";
                          var frmClienteComentario = "comentario:estado:fecha:idCliente:txCliente";
                          if(document.getElementById('Operacion'+Sufijo))
                                 document.getElementById('Operacion'+Sufijo).value = Operacion;
                             
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,frmClienteComentario,"id:estado");
                              ElementClear(Sufijo,objClienteComentario);
                              BarButtonStateEnabled(Sufijo,"btCliente");
                              CargaFechas_ClienteComentario(Sufijo);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,frmClienteComentario,"id");
                               BarButtonStateEnabled(Sufijo,"btCliente");
                               CargaFechas_ClienteComentario(Sufijo);
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_ClienteComentario(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,frmClienteComentario))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objClienteComentario);
                                       xajax_Guarda_ClienteComentario(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmClienteComentario);
                               ElementClear(Sufijo,objClienteComentario);
                               BarButtonStateDisabled(Sufijo,"btCliente");
                               CargaFechas_ClienteComentario(Sufijo);
                          }
                          else if (Operacion=='addImp')
                          {
                          } else 
                            xajax_CargaModal_ClienteComentario(Operacion);
                          return false;
                 }

                 function SearchByElement_ClienteComentario(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_ClienteComentario(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   var Pop = document.getElementById('FindClienteTextBx'+Sufijo); 
                              var Operacion = document.getElementById('Operacion'+Sufijo).value;
                              if(Pop){    
                                  xajax_MuestraClienteByTX_ClienteComentario(Elemento.value,Operacion);
                              }else{ 
                                  xajax_MuestraByTX_ClienteComentario(Elemento.value);
                              }
                          }    
                 }
                 
                 function SearchGetData_ClienteComentario(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")){
                                BarButtonState(Sufijo,"Active");

                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                                document.getElementById("idCliente"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                                document.getElementById("txCliente"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                                document.getElementById("comentario"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                                document.getElementById("fecha"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;

                                document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue; 
                          }                          
                          return false;
                 }
                 
                 function SearchClienteGetData_ClienteComentario(Sufijo,DatosGrid)
                 {       document.getElementById("idCliente"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                         document.getElementById("txCliente"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                         cerrar();
                         return false;
                 }
                 
                 
                 function XAJAXResponse_ClienteComentario(Sufijo,Mensaje,Datos)
                 {       
                        var objClienteComentario = "comentario:estado:fecha:idCliente:txCliente";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objClienteComentario);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
         <body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlClienteComentario->CargaClienteComentarioBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                      <input type="hidden" id="Operacion<?php echo $Sufijo; ?>" name="Operacion<?php echo $Sufijo; ?>" value=""/>
                   <?php  $ControlClienteComentario->CargaClienteComentarioMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlClienteComentario->CargaClienteComentarioSearchGrid();  ?>
              </div>  
         </div>
         </body>
         </html>
              

