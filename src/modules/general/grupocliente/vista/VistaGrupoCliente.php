<?php
         $Sufijo = '_GrpCliente';

         require_once('src/modules/general/grupocliente/controlador/ControlGrupoCliente.php');
         $ControlGrupoCliente = new ControlGrupoCliente($Sufijo);
         
         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlGrupoCliente,'GuardaGrupoCliente'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlGrupoCliente,'MuestraGrupoClienteByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlGrupoCliente,'MuestraGrupoClienteByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlGrupoCliente,'EliminaGrupoCliente'));
         $xajax->processRequest();

?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript">
                 function ButtonClick_GrpCliente(Sufijo,Operacion)
                 {        var objGrpCliente = "id:descripcion:estado";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,objGrpCliente,"id:estado");
                              ElementClear(Sufijo,objGrpCliente);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objGrpCliente,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_GrpCliente(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"descripcion"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objGrpCliente);
                                       xajax_Guarda_GrpCliente(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",objGrpCliente);
                               ElementStatus(Sufijo,"id",'');
                               ElementClear(Sufijo,objGrpCliente);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_GrpCliente(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_GrpCliente(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_GrpCliente(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_GrpCliente(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")){
                            BarButtonState(Sufijo,"Active");

                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("descripcion"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("estado"+Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue; 
                          }
                          return false;
                 }
                 
                 function XAJAXResponse_GrpCliente(Sufijo,Mensaje,Datos)
                 {       
                        var objGrpCliente = "descripcion:estado";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objGrpCliente);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
        <body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlGrupoCliente->CargaGrupoClienteBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlGrupoCliente->CargaGrupoClienteMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid">          
              <?php  $ControlGrupoCliente->CargaGrupoClienteSearchGrid();  ?>
              </div>  
         </div>
       </body>
</html>  
              

