<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderGrupoCliente
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaGrupoClienteMantenimiento()
              {        
                       $GrupoCliente = '<table class="Form-Frame" cellpadding="0">';
                       $GrupoCliente.= '       <tr height="30">';
                       $GrupoCliente.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $GrupoCliente.= '             <td class="Form-Label" style="width:75%">';
                       $GrupoCliente.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $GrupoCliente.= '             </td>';
                       $GrupoCliente.= '         </tr>';
                       $GrupoCliente.= '         <tr height="30">';
                       $GrupoCliente.= '             <td class="Form-Label" style="width:25%">GrupoCliente</td>';
                       $GrupoCliente.= '             <td class="Form-Label" style="width:75%">';
                       $GrupoCliente.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $GrupoCliente.= '             </td>';
                       $GrupoCliente.= '         </tr>';
                       $GrupoCliente.= '         <tr height="30">';
                       $GrupoCliente.= '             <td class="Form-Label">Estado</td>';
                       $GrupoCliente.= '             <td class="Form-Label">';
                                                   $GrupoCliente.= $this->CreaComboEstado();
                       $GrupoCliente.= '             </td>';
                       $GrupoCliente.= '         </tr>';
                       $GrupoCliente.= '</table>';
                       return $GrupoCliente;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Descripci&oacute;n'] = array('180px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaGrupoClienteSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->GrupoClienteGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function GrupoClienteGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $grupo)
                      {        $fecreacion = $this->Fechas->changeFormatDate($grupo['fecreacion'],"DMY");
                               $id = str_pad($grupo['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($grupo['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$grupo['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$grupo['idestado']);                                
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($grupo['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraGrupoCliente($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaGrupoCliente($Ajax,"NOEXISTEID","No existe un Grupo con ese ID.");
                       }
              }
              
              function MuestraGrupoClienteGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->GrupoClienteGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }
               
              function MuestraGrupoClienteGuardado($Ajax,$GrupoCliente)
              {       if (count($GrupoCliente)>0)
                        {   $id = str_pad($GrupoCliente[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraGrupoClienteGrid($Ajax,$GrupoCliente);
                            return $this->RespuestaGrupoCliente($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaGrupoCliente($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraGrupoClienteEditado($Ajax,$GrupoCliente)
              {       foreach ($GrupoCliente as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraGrupoClienteRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaGrupoCliente($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaGrupoCliente($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraGrupoClienteEliminado($Ajax,$GrupoCliente)
              {        foreach ($GrupoCliente as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraGrupoClienteRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaGrupoCliente($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaGrupoCliente($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraGrupoClienteRowGrid($Ajax,$GrupoCliente,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($GrupoCliente['id'],$estado);                        
                        $fecreacion = $this->Fechas->changeFormatDate($GrupoCliente['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($GrupoCliente['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($GrupoCliente['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($GrupoCliente['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($GrupoCliente['idestado']));
                        return $Ajax;
                }

              function MuestraGrupoClienteExcepcion($Ajax,$Msg)
                {       return $this->RespuestaGrupoCliente($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaGrupoCliente($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

