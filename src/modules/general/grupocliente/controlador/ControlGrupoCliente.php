<?php
         require_once("src/rules/general/servicio/ServicioGrupoCliente.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/general/grupocliente/render/RenderGrupoCliente.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlGrupoCliente
         {     private  $Sufijo; 
               private  $ServicioGrupoCliente;
               private  $RenderGrupoCliente;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioGrupoCliente = new ServicioGrupoCliente();
                        $this->RenderGrupoCliente = new RenderGrupoCliente($Sufijo);
               }

               function CargaGrupoClienteBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaGrupoClienteMantenimiento()
               {        echo $this->RenderGrupoCliente->CreaGrupoClienteMantenimiento();
               }

               function CargaGrupoClienteSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoGrupoCliente = $this->ServicioGrupoCliente->BuscarGrupoClienteByDescripcion($prepareDQL);
                        echo $this->RenderGrupoCliente->CreaGrupoClienteSearchGrid($datoGrupoCliente);
               }         
               
               function MuestraGrupoClienteByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoGrupoCliente = $this->ServicioGrupoCliente->BuscarGrupoClienteByID($prepareDQL);
                        return $this->RenderGrupoCliente->MuestraGrupoCliente($ajaxRespon,$datoGrupoCliente);
               }

               function MuestraGrupoClienteByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoGrupoCliente = $this->ServicioGrupoCliente->BuscarGrupoClienteByDescripcion($prepareDQL);
                        return $this->RenderGrupoCliente->MuestraGrupoClienteGrid($ajaxRespon,$datoGrupoCliente);
               }

               function GuardaGrupoCliente($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $GrupoCliente  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioGrupoCliente->GuardaDBGrupoCliente($GrupoCliente);
                        if (is_numeric($id)){
                            $function = (empty($GrupoCliente->id) ? "MuestraGrupoClienteGuardado" : "MuestraGrupoClienteEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioGrupoCliente->BuscarGrupoClienteByID($prepareDQL);
                            return $this->RenderGrupoCliente->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderGrupoCliente->MuestraGrupoClienteExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaGrupoCliente($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioGrupoCliente->DesactivaGrupoCliente(intval($id));
                        $Datos = $this->ServicioGrupoCliente->BuscarGrupoClienteByDescripcion($prepareDQL);
                        return $this->RenderGrupoCliente->MuestraGrupoClienteEliminado($ajaxRespon,$Datos);
                }
         }

?>

