<?php   $Sufijo = '_terminos';
        require_once('src/modules/general/termino/controlador/ControlTerminos.php');
        $ControlTerminos = new ControlTerminos($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlTerminos,'GuardaTerminos'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlTerminos,'EliminaTerminos'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlTerminos,'ConsultaTerminosByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX'.$Sufijo, $ControlTerminos,'ConsultaTerminosByTX'));
        $xajax->processRequest();
?>  
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php $xajax->printJavascript(); 
                  require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript">
                var objterminos = "id:descripcion:idtiempo:valor:interes:estado";
                var valterminos = "descripcion:idtiempo:valor:interes";   
                function ButtonClick_terminos(Sufijo,Operacion)
                {       if (Operacion==='addNew')
                        {   BarButtonState(Sufijo,Operacion);
                            ElementStatus(Sufijo,valterminos,"id");
                            ElementClear(Sufijo,objterminos);
                            ElementSetValue(Sufijo,"estado",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Sufijo,Operacion);
                                if (ElementGetValue(Sufijo,"estado")==0)
                                    ElementStatus(Sufijo,valterminos+":estado","id");
                                else
                                ElementStatus(Sufijo,valterminos,"id");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este regsistro?"))
                                    xajax_Elimina_terminos(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(Sufijo,valterminos))
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    {   var Forma = PrepareElements(Sufijo,objterminos);
                                        xajax_Guarda_terminos(JSON.stringify({Forma}));
                                    }
                                }
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Sufijo,Operacion);
                                ElementStatus(Sufijo,"id",valterminos+":estado");
                                ElementClear(Sufijo,objterminos);
                        }
                        return false;
                }
                
                function XAJAXResponse_terminos(Sufijo,Mensaje,Datos)
                {       if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",valterminos+":estado");
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='EXCEPCION')
                        {   BarButtonState(Sufijo,"addNew");
                            alert(Datos); 
                        }    
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                        }
                        else if(Mensaje==="NOEXISTEID")
                        {   BarButtonState(Sufijo,"Default");
                            ElementClear(Sufijo,"id");
                            alert(Datos); 
                        }
                }
                
                function SearchByElement_terminos(Sufijo,Elemento)
                {       
                        Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    xajax_BuscaByID_terminos(Elemento.value);
                                }
                            }
                            BarButtonState(Sufijo,"Default"); //OJO AQUI
                        }
                        else
                        xajax_BuscaByTX_terminos(Elemento.value);
                    
                }
                 
                function SearchGetData_terminos(Sufijo,Grilla)
                {       if (IsDisabled(Sufijo,"addSav"))
                        {   BarButtonState(Sufijo,"Active");
                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("descripcion"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("interes"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue; 
                            document.getElementById("valor"+Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue; 
                            document.getElementById("idtiempo"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue; 
                            document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue; 
                        }
                        return false;
                }
                 
        </script>
        </head>
        <body>
        <div class="FormBasic" style="width:480px">
            <div class="FormSectionMenu">              
            <?php   $ControlTerminos->CargaTerminosBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlTerminos->CargaTerminosMantenimiento();  ?>
                    </form>
            </div>    
            <div class="FormSectionGrid">          
            <?php   $ControlTerminos->CargaTerminosSearchGrid();  ?>
            </div>    
        </div>
        </body>
        </html> 
              

