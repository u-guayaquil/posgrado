<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioTiempo.php");
        
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/ComboBox.php');
         
        class RenderTerminos
        {   private $Sufijo;
            private $Maxlen;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;
                    $this->Maxlen['id'] = 2;        
                    $this->Maxlen['descripcion'] = 20;
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']       = array('40px','center','');
                    $Columns['Descripción'] = array('127px','left','');
                    $Columns['Tasa']     = array('60px','right','');
                    $Columns['Creación'] = array('120px','center','');
                    $Columns['Valor']    = array('0px','center','none');
                    $Columns['IdTiempo'] = array('0px','center','none');
                    $Columns['IdEstado'] = array('0px','center','none');
                    $Columns['Estado']   = array('81px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
            }
               
            function CreaMantenimientoTerminos()
            {       $Terminos = '<table border="0" class="Form-Frame" cellpadding="0">';
                    $Terminos.= '       <tr height="28">';
                    $Terminos.= '             <td class="Form-Label" style="width:20%">Id</td>';
                    $Terminos.= '             <td class="Form-Label" style="width:24%">';
                    $Terminos.= '                <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:valor:interes\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Terminos.= '             </td>';
                    $Terminos.= '             <td class="Form-Label" style="width:03%"></td>';
                    $Terminos.= '             <td class="Form-Label" style="width:13%"></td>';
                    $Terminos.= '             <td class="Form-Label" style="width:03%"></td>';
                    $Terminos.= '             <td class="Form-Label" style="width:47%"></td>';
                    $Terminos.= '         </tr>';
                    $Terminos.= '         <tr height="28">';
                    $Terminos.= '             <td class="Form-Label">Descripción</td>';
                    $Terminos.= '             <td class="Form-Label" colspan="5">';
                    $Terminos.= '                 <input type="text" class="txt-upper t04" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $Terminos.= '             </td>';
                    $Terminos.= '         </tr>';
                    $Terminos.= '         <tr height="28">';
                    $Terminos.= '             <td class="Form-Label">Plazo</td>';
                    $Terminos.= '             <td class="Form-Label">';
                    $Terminos.=                   $this->CreaComboTiempoByArray('1,2,3');
                    $Terminos.= '             </td>';
                    $Terminos.= '             <td class="Form-Label-Center">-</td>';
                    $Terminos.= '             <td class="Form-Label">';
                    $Terminos.= '                 <input type="text" class="txt-input t02" id="valor'.$this->Sufijo.'" name="valor'.$this->Sufijo.'" value="" maxlength="3" onKeyDown="return soloNumeros(event);" disabled/>';
                    $Terminos.= '             </td>';
                    $Terminos.= '             <td class="Form-Label-Center">-</td>';
                    $Terminos.= '             <td class="Form-Label">';
                    $Terminos.= '                 <input type="text" class="txt-input t02" id="interes'.$this->Sufijo.'" name="interes'.$this->Sufijo.'" value="" maxlength="5" onKeyDown="return soloFloat(event,this,2);" disabled/> %';
                    $Terminos.= '             </td>';
                    $Terminos.= '         </tr>';
                    $Terminos.= '         <tr height="28">';
                    $Terminos.= '             <td class="Form-Label">Estado</td>';
                    $Terminos.= '             <td class="Form-Label" colspan="5">';
                    $Terminos.=                   $this->CreaComboEstadoByArray(array(0,1));
                    $Terminos.= '             </td>';
                    $Terminos.= '         </tr>';
                    $Terminos.= '</table>';
                    return $Terminos;
            }

            function CreaBarButtonTerminos($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);    
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGridTerminos($Terminos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTMLTerminos($SearchGrid,$Terminos);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }

            private function GridDataHTMLTerminos($Grid,$Datos)
            {       foreach ($Datos as $tiempo)
                    {       $Fecha  = new DateControl();
                            $fecreacion = $Fecha->changeFormatDate($tiempo['fecreacion'],"DMY");                     
                            $tasa = number_format($tiempo['interes'], 2, ".",",");
                            $id = str_pad($tiempo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($tiempo['descripcion']));
                            $Grid->CreaSearchCellsDetalle($id,'',$tasa);
                            $Grid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                            $Grid->CreaSearchCellsDetalle($id,'',$tiempo['valor']);
                            $Grid->CreaSearchCellsDetalle($id,'',$tiempo['idxtiempo']);
                            $Grid->CreaSearchCellsDetalle($id,'',$tiempo['idxestado']);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($tiempo['txtestado']));
                            $Grid->CreaSearchRowsDetalle ($id,($tiempo['idxestado']==0 ? "red" : ""));
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
               
            function MuestraTerminosForm($Ajax,$Terminos)
            {       if ($Terminos)
                    {   if ($Terminos->id>0)
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Terminos->id,$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Terminos->descripcion));
                            $Ajax->Assign("valor".$this->Sufijo,"value", trim($Terminos->valor));
                            $Ajax->Assign("interes".$this->Sufijo,"value", trim($Terminos->interes));
                            $Ajax->Assign("idtiempo".$this->Sufijo,"value", trim($Terminos->idtiempo));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Terminos->idestado);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }    
                    }
                    return $this->RespuestaTerminos($Ajax,"NOEXISTEID","No existe un término con el ID ingresado.");
            }

            function MuestraTerminosGrid($Ajax,$Terminos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTMLTerminos($SearchGrid,$Terminos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }        

            function MuestraTerminosRowGrid($Ajax,$Terminos)
            {       $Fecha  = new DateControl();
                    $Fila = $this->GetRow($Terminos['id'],$Terminos['idxestado']);
                    $fecreacion = $Fecha->changeFormatDate($Terminos['fecreacion'],"DMY");                                         
                    $tasa = number_format($Terminos['interes'], 2, ".",",");                    
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->GetCell($Fila['Id'],1),"innerHTML",trim($Terminos['descripcion'])); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],2),"innerHTML",$tasa); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],3),"innerHTML",$fecreacion[1]." ".$fecreacion[2]); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],4),"innerHTML",$Terminos['valor']);                             
                    $Ajax->Assign($this->GetCell($Fila['Id'],5),"innerHTML",$Terminos['idxtiempo']);                                                         
                    $Ajax->Assign($this->GetCell($Fila['Id'],6),"innerHTML",$Terminos['idxestado']); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],7),"innerHTML",trim($Terminos['txtestado'])); 
                    return $Ajax;
            }        
            
            function MuestraTerminosGuardado($Ajax,$Terminos)
            {       if (count($Terminos)>0)
                    {   $id = str_pad($Terminos[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraTerminosGrid($Ajax,$Terminos);
                        return $this->RespuestaTerminos($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaTerminos($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraTerminosEditado($Ajax,$Terminos)
            {       foreach ($Terminos as $termino) 
                    {       $id = str_pad($termino['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraTerminosRowGrid($Ajax, $termino);
                            return $this->RespuestaTerminos($xAjax,"GUARDADO",$id);
                    }    
                    return $this->RespuestaTerminos($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
            
            function MuestraTerminosEliminado($Ajax,$Terminos)
            {       foreach ($Terminos as $termino)  
                    {   $xAjax = $this->MuestraTerminosRowGridDelete($Ajax,$termino);  
                        return $this->RespuestaTerminos($xAjax,"ELIMINADO",0);
                    }
                    return $this->RespuestaTerminos($Ajax,"ELIMINADO",1);
            }

            function MuestraTerminosRowGridDelete($Ajax,$Terminos)
            {       $Fila = $this->GetRow($Terminos['id'],$Terminos['idxestado']);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->GetCell($Fila['Id'],6),"innerHTML",$Terminos['idxestado']); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],7),"innerHTML",trim($Terminos['txtestado'])); 
                    return $Ajax;
            }        

            private function RespuestaTerminos($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox($this->Sufijo);
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Selected(1)->Create("s04");
            }        
            
            private function CreaComboTiempoByArray($IdArray)
            {       $Select = new ComboBox($this->Sufijo);
                    $ServicioTiempo = new ServicioTiempo();
                    $Datos = $ServicioTiempo->BuscarTiempoByArrayID($IdArray);
                    return $Select->Combo("idtiempo",$Datos)->Selected(1)->Create("s04");
            }        

            private function GetRow($id,$estado)
            {       $RowId = $this->Sufijo."_".str_pad($id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Style = "color: ".($estado==0 ? "red" : "#000");
                    $RowOb = "TR".$RowId;
                    return array('Id' => $RowId,'Name' => $RowOb,'Color' => $Style);
            }
                
            private function GetCell($RowId,$Cell)
            {       return "TD".$RowId."_".str_pad(intval($Cell),3,'0',STR_PAD_LEFT);
            }
            
        }
?>

