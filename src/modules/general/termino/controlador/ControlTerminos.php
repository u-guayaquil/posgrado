<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/general/servicio/ServicioTerminos.php");
        require_once("src/modules/general/termino/render/RenderTerminos.php");

        class ControlTerminos
        {       private  $ServicioTerminos;
                private  $RenderTerminos;

                function __construct($Sufijo = "")
                {       $this->ServicioTerminos = new ServicioTerminos();
                        $this->RenderTerminos = new RenderTerminos($Sufijo);
                }

                function CargaTerminosBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $this->RenderTerminos->CreaBarButtonTerminos($Datos);
                }
               
                function CargaTerminosMantenimiento()
                {       echo $this->RenderTerminos->CreaMantenimientoTerminos();
                }

                function CargaTerminosSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioTerminos->BuscarTerminosByDescripcion($prepareDQL);
                        echo $this->RenderTerminos->CreaSearchGridTerminos($Datos);
                }         
               
                function ConsultaTerminosByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioTerminos->BuscarTerminosByID($id);
                        return $this->RenderTerminos->MuestraTerminosForm($ajaxRespon,$Datos);
                }

                function ConsultaTerminosByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $texto = Trim($Texto);      
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioTerminos->BuscarTerminosByDescripcion($prepareDQL);
                        return $this->RenderTerminos->MuestraTerminosGrid($ajaxRespon,$Datos);
                }

                function GuardaTerminos($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $Terminos = json_decode($Form)->Forma;
                        
                        $function = (empty($Terminos->id) ? "MuestraTerminosGuardado" : "MuestraTerminosEditado");
                        $id = $this->ServicioTerminos->GuardaDBTerminos($Terminos);
                        $Datos = $this->ServicioTerminos->BuscarTerminosEstadoByID($id);
                        return $this->RenderTerminos->{$function}($ajaxRespon,$Datos);
                }

                function EliminaTerminos($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioTerminos->DesactivaTerminos($id);
                        $Datos = $this->ServicioTerminos->BuscarTerminosEstadoByID($id);
                        return $this->RenderTerminos->MuestraTerminosEliminado($ajaxRespon,$Datos);
                }
        }       
?>

