<?php
         $Sufijo = '_Cliente';
         
         require_once('src/modules/general/cliente/controlador/ControlCliente.php');
         $ControlCliente = new ControlCliente($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlCliente,'GuardaCliente'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlCliente,'MuestraClienteByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlCliente,'MuestraClienteByTX'));
         $xajax->register(XAJAX_FUNCTION,array('PresentGridData'.$Sufijo, $ControlCliente,'PresentaGridData'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlCliente,'EliminaCliente'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlCliente,'CargaModalGridCliente'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlCliente,'ConsultaModalGridClienteByTX'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaCiudadByTX'.$Sufijo, $ControlCliente,'ConsultaModalCiudadGridClienteByTX'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaVendedorByTX'.$Sufijo, $ControlCliente,'ConsultaModalVendedorGridClienteByTX'));
         $xajax->register(XAJAX_FUNCTION,array('GuardaIdent'.$Sufijo, $ControlCliente,'GuardaIdentificacion'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModalIdent'.$Sufijo, $ControlCliente,'CargaModalIdentificaciones'));
         
         $xajax->processRequest();
         $xajax->printJavascript();

?>
         <script type="text/javascript"> 
             
                 function CargaFechas_Cliente(Sufijo){
                    $('#fechanacimiento'+Sufijo).dcalendarpicker({
                      format: 'dd/mm/yyyy'      
                      });
                    $('#fechacliente'+Sufijo).dcalendarpicker({
                      format: 'dd/mm/yyyy'      
                      });
                 }
                 
                 function VerificaTipo_Cliente(Sufijo){
                    var tipo = document.getElementById('tipocliente'+Sufijo).value;
                    if(tipo==='J'){
                       document.getElementById('obligacontabilidad'+Sufijo).checked=true; 
                       document.getElementById('obligacontabilidad'+Sufijo).disabled=true;
                       document.getElementById('estadocivil'+Sufijo).disabled=true;
                       document.getElementById('sexo'+Sufijo).disabled=true;
                       document.getElementById('fechanacimiento'+Sufijo).disabled=true;
                    }else{
                       document.getElementById('obligacontabilidad'+Sufijo).checked=false; 
                       document.getElementById('obligacontabilidad'+Sufijo).disabled=false;
                       document.getElementById('estadocivil'+Sufijo).disabled=false;
                       document.getElementById('sexo'+Sufijo).disabled=false;
                       document.getElementById('fechanacimiento'+Sufijo).disabled=false;
                    }
                 }
                 
                 function EncerarCombos_Cliente(Sufijo){
                     document.getElementById('grupocliente'+Sufijo).value=1; 
                     document.getElementById('terminos'+Sufijo).value=1; 
                     document.getElementById('estadocivil'+Sufijo).value=1; 
                     document.getElementById('sexo'+Sufijo).value='M'; 
                     document.getElementById('tipocliente'+Sufijo).value='N';
                 }
                 
                    
                 function ButtonClick_Cliente(Sufijo,Operacion)
                 {        var objCliente = "id:codigo:nombre:tipocliente:obligacontabilidad:grupocliente:direccion"
                                           +":telefono:fechacliente:email:estado:txCuentaPorCobrar:txCuentaAnticipo"
                                           +":idCuentaPorCobrar:idCuentaAnticipo:btCuentaPorCobrar:btCuentaAnticipo"
                                           +":idCiudad:btCiudad:txCiudad:estadocivil:terminos"
                                           +":sexo:movil:fechanacimiento:idVendedor:btVendedor:txVendedor";
                          var buttonPantalla = "btCuentaAnticipo:btCiudad:btCuentaPorCobrar:btVendedor:Operacion";
                          if(document.getElementById('Operacion'+Sufijo))
                            document.getElementById('Operacion'+Sufijo).value = Operacion;
                          
                          if (Operacion==='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,objCliente,"id:estado");
                              ElementClear(Sufijo,objCliente);
                              ElementSetValue(Sufijo,"estado",1);
                              CargaFechas_Cliente(Sufijo);
                              EncerarCombos_Cliente(Sufijo);
                          }
                          else if (Operacion==='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objCliente,"id");
                               CargaFechas_Cliente(Sufijo);
                          }
                          else if (Operacion==='addDel')
                          {    if (ElementGetValue(Sufijo,"estado").trim()==='1')
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_Cliente(ElementGetValue(Sufijo,"id"));
                                    BarButtonStateDisabled(Sufijo,buttonPantalla);
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion==='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"nombre"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   
                                       var Forma = PrepareElements(Sufijo,objCliente);
                                        xajax_Guarda_Cliente(JSON.stringify({Forma}));
                                        BarButtonStateDisabled(Sufijo,buttonPantalla);
                                   }
                               }
                          }
                          else if (Operacion==='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",objCliente);
                               ElementStatus(Sufijo,"id","");
                               ElementClear(Sufijo,objCliente);
                               EncerarCombos_Cliente(Sufijo);
                          }
                          else if (Operacion==='addChildSave')
                          {      if (confirm("Desea guardar los datos ?"))
                                    var FormaI = PrepareElements(Sufijo,'cedula:ruc:pasaporte:id:idcedula:idruc:idpasaporte');
                                        xajax_GuardaIdent_Cliente(JSON.stringify({FormaI}));
                                
                          }
                          else if (Operacion==='addImp')
                          {
                          }else if (Operacion==='btIdent' && document.getElementById('id'+Sufijo).value!=='')
                          {  xajax_CargaModalIdent_Cliente(Operacion,document.getElementById('id'+Sufijo).value);  
                          }else { 
                             xajax_CargaModal_Cliente(Operacion);  
                           }
                          return false;
                 }

                 function SearchByElement_Cliente(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          var tipo = document.getElementById('Operacion'+Sufijo).value;
                          
                          switch(Elemento.id){
                              case "id"+Sufijo:
                                  if (Elemento.value.length != 0)
                                    {      if (BarButtonState(Sufijo,"Inactive"))
                                            xajax_MuestraByID_Cliente(Elemento.value);                              
                                    }
                                    else
                                    BarButtonState(Sufijo,"Default");
                              break;
                              case "FindCuentaCobrarTextBx"+Sufijo:
                                  xajax_BuscaModalByTX_Cliente(tipo,Elemento.value); 
                              break;
                              case "FindCuentaAnticipoTextBx"+Sufijo:
                                  xajax_BuscaModalByTX_Cliente(tipo,Elemento.value); 
                              break;
                              case 'FindTextBx'+Sufijo:
                                  xajax_MuestraByTX_pago(tipo,Elemento.value);
                              break;
                              case 'FindCiudadTextBx'+Sufijo:
                                  xajax_BuscaCiudadByTX_pago(tipo,Elemento.value);
                              break;
                              case 'FindVendedorTextBx'+Sufijo:
                                  xajax_BuscaVendedorByTX_pago(tipo,Elemento.value);
                              break;
                          }
                 }
                 
                 function SearchGetData_Cliente(Sufijo,Grilla)
                 {        BarButtonState(Sufijo,"Active");
                   
                          document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                          document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                          document.getElementById("nombre"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue; 
                          document.getElementById("telefono"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                          document.getElementById("estado"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                          document.getElementById("tipocliente"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                          var Obliga = false;
                          if(Grilla.cells[7].childNodes[0].nodeValue.trim()==='1'){
                              Obliga = true;
                          }else{
                              Obliga = false;
                          } 
                          document.getElementById("obligacontabilidad"+Sufijo).checked = Obliga;
                          document.getElementById("grupocliente"+Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
                          document.getElementById("terminos"+Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue.trim();
                          document.getElementById("direccion"+Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
                          document.getElementById("telefono"+Sufijo).value = Grilla.cells[11].childNodes[0].nodeValue;
                          document.getElementById("email"+Sufijo).value = Grilla.cells[12].childNodes[0].nodeValue;
                          document.getElementById("idCuentaPorCobrar"+Sufijo).value = Grilla.cells[13].childNodes[0].nodeValue;
                          document.getElementById("txCuentaPorCobrar"+Sufijo).value = Grilla.cells[14].childNodes[0].nodeValue;
                          document.getElementById("idCuentaAnticipo"+Sufijo).value = Grilla.cells[15].childNodes[0].nodeValue;
                          document.getElementById("txCuentaAnticipo"+Sufijo).value = Grilla.cells[16].childNodes[0].nodeValue;
                          document.getElementById("idCiudad"+Sufijo).value = Grilla.cells[17].childNodes[0].nodeValue;
                          document.getElementById("txCiudad"+Sufijo).value = Grilla.cells[18].childNodes[0].nodeValue;
                          document.getElementById("idVendedor"+Sufijo).value = Grilla.cells[19].childNodes[0].nodeValue;
                          if(Grilla.cells[19].childNodes[0].nodeValue.trim()==='0'){
                              document.getElementById("txVendedor"+Sufijo).value = '';
                          }else{
                              document.getElementById("txVendedor"+Sufijo).value = Grilla.cells[20].childNodes[0].nodeValue;
                          }                          
                          document.getElementById("estadocivil"+Sufijo).value = Grilla.cells[21].childNodes[0].nodeValue;
                          document.getElementById("sexo"+Sufijo).value = Grilla.cells[22].childNodes[0].nodeValue;
                          document.getElementById("movil"+Sufijo).value = Grilla.cells[23].childNodes[0].nodeValue;
                          document.getElementById("fechanacimiento"+Sufijo).value = Grilla.cells[24].childNodes[0].nodeValue;
                          document.getElementById("fechacliente"+Sufijo).value = Grilla.cells[25].childNodes[0].nodeValue;
                          return false;
                 }
                 
                 function SearchCuentaPorCobrarGetData_Cliente(Sufijo,DatosGrid)
                 {   
                         document.getElementById("idCuentaPorCobrar"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                         document.getElementById("txCuentaPorCobrar"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;                             
                         cerrar();
                         return false;
                 }
                 
                 function SearchCuentaAnticipoGetData_Cliente(Sufijo,DatosGrid)
                 {       
                         document.getElementById("idCuentaAnticipo"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                         document.getElementById("txCuentaAnticipo"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;                             
                         cerrar();
                         return false;
                 }
                 
                 function SearchCiudadGetData_Cliente(Sufijo,DatosGrid)
                 {       
                         document.getElementById("idCiudad"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                         document.getElementById("txCiudad"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;                             
                         cerrar();
                         return false;
                 }
                 
                 function SearchVendedorGetData_Cliente(Sufijo,DatosGrid)
                 {       
                         document.getElementById("idVendedor"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                         document.getElementById("txVendedor"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;                             
                         cerrar();
                         return false;
                 }
                 
                function XAJAXResponse_Cliente(Sufijo,Mensaje,Datos)
                {       var buttonPantalla = "btCuentaAnticipo:btCiudad:btCuentaPorCobrar:btVendedor";
                        var objCliente = "codigo:nombre:tipocliente:obligacontabilidad:grupocliente:direccion"
                                        +":telefono:fechacliente:email:estado:txCuentaPorCobrar:txCuentaAnticipo"
                                        +":idCuentaPorCobrar:idCuentaAnticipo:btCuentaPorCobrar:btCuentaAnticipo"
                                        +":idCiudad:btCiudad:txCiudad:estadocivil:terminos"
                                        +":sexo:movil:fechanacimiento:idVendedor:btVendedor:txVendedor:Operacion";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,buttonPantalla);
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objCliente);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                }                 
         </script>
                  
         <div class="FormContainer" style="width:9800px">
            <div class="FormContainerSeccion">
             <div class="FormBasic" style="width:480px">
                <div class="FormSectionMenu">              
                <?php  $ControlCliente->CargaClienteBarButton($_POST['opcion']);
                       echo '<script type="text/javascript">';
                       echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                       echo '</script>';
                ?>
                </div>    
                <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                        <input type="hidden" id="Operacion<?php echo $Sufijo; ?>" name="Operacion<?php echo $Sufijo; ?>" value=""/>
                     <?php  $ControlCliente->CargaClienteMantenimiento();  ?>
                     </form>
                </div>
             </div>
            </div>
            <div class="FormContainerSeccion" style="width:500px">
              <div class="FormSectionGrid">          
              <?php echo $ControlCliente->CargaClienteSearchGrid();  ?>
              </div>  
            </div> 
         </div>
         
              

