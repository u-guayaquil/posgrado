<?php
         require_once("src/rules/general/servicio/ServicioClienteIdentificacion.php");
         require_once("src/modules/general/clienteIdentificacion/render/RenderClienteIdentificacion.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlCliente
         {     private  $Sufijo;
               private  $ServicioClienteIdentificacion;
               private  $RenderClienteIdentificacion;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioClienteIdentificacion = new ServicioClienteIdentificacion();
                        $this->RenderClienteIdentificacion = new RenderClienteIdentificacion($Sufijo);
               }
               
               public function GuardaIdentificacion($Form)
               {        
                        $Cliente = json_decode($Form)->FormaI;
                        $datosServicioClienteIdentificacion=new ServicioClienteIdentificacion();
                        $infoIdentificacion  = $datosServicioClienteIdentificacion->ObjetoClienteIdentificacionByIDCliente($Cliente->id);
                        if (count($infoIdentificacion)==0){
                           list($estado,$Id) = $this->ServicioClienteIdentificacion->CreaClienteIdentificacion($Cliente);                            
                        }else{
                           list($estado,$Id) = $this->ServicioClienteIdentificacion->EditaClienteIdentificacion($Cliente,$infoIdentificacion);
                        }                       
               }
               
                
                public function CargaModalIdentificaciones($Operacion,$id)
                {       $ajaxResp = new xajaxResponse(); 
                        
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Identificaciones ";
                        $jsonModal['Carga'] = $this->RenderCliente->CreaModalIdentificacion($id);
                        $jsonModal['Ancho'] = "300";
                        
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
               
         }

?>

