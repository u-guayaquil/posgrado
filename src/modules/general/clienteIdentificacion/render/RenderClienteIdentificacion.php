<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");            
        require_once("src/rules/general/entidad/Estado.php"); 
        require_once("src/rules/impuesto/servicio/ServicioIdentificacion.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderClienteIdentificacion
        {     private $Sufijo;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
              }              
              
              function MuestraDespuesdeGuardar($Ajax,$Id,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;
                        if (count($Datos)>0)
                        {   $xAjax = $this->MuestraClienteGrid($Ajax,$Datos); 
                            $xAjax->call($jsFun,$this->Sufijo,"GUARDADO",str_pad($Id,2,'0',STR_PAD_LEFT)); 
                            return $xAjax;
                        }
                }
                
              function MuestraErroralGuardar($Ajax,$Id,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;                
                        if (count($Datos)>0)
                        {   $Ajax = $this->MuestraClienteGrid($Ajax,$Datos);                            
                            $Ajax->call($jsFun,$this->Sufijo,"EXCEPCION",$Id);    
                            return $Ajax;
                        }
                        
                }
                
              function MuestraDespuesdeEliminar($Ajax,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;
                        if (count($Datos)>0)  
                        {   $xAjax = $this->MuestraClienteGrid($Ajax,$Datos);  
                            $xAjax->call($jsFun,$this->Sufijo,"ELIMINADO",0);
                            return $xAjax;
                        }
                }
                
                /*** MODAL ***/
                
                function CreaModalIdentificacion($id)
                {      $datos = $this->ExtraeIdentificaciones($id);
                       
                       $Cliente = '<div class="FormBasicCenter">';               
                       $Cliente .= '<table class="Form-Frame" cellpadding="0">';
                       $Cliente.= '         <tr height="30">';
                       $Cliente.= '             <td class="Form-Label" style="width:25%">C&eacute;dula</td>';
                       $Cliente.= '             <td class="Form-Label" style="width:50%">';
                       $Cliente.= '                 <input type="text" class="txt-upper t05" id="cedula'.$this->Sufijo.'" name="cedula'.$this->Sufijo.'" value="'.trim($datos['cedula']).'" maxlength="10" />';
                       $Cliente.= '                 <input type="hidden" id="idcedula'.$this->Sufijo.'" name="idcedula'.$this->Sufijo.'" value="'.trim($datos['idcedula']).'"/>';
                       $Cliente.= '             </td>';
                       $Cliente.= '             <td class="Form-Label" rowspan="3" style="width:25%" align="center">';
                       $Cliente.= '                 <input id="btnIdent'.$this->Sufijo.'" src="src/public/img/guarda.png" type="image" '
                                                      . 'name="btnIdent'.$this->Sufijo.'" onclick="return ButtonClick'.$this->Sufijo.'(\''.$this->Sufijo.'\',\'addChildSave\');" ';
                       $Cliente.= '             </td>';
                       $Cliente.= '         </tr>';
                       $Cliente.= '         <tr height="30">';
                       $Cliente.= '             <td class="Form-Label" style="width:25%">RUC</td>';
                       $Cliente.= '             <td class="Form-Label" style="width:50%">';
                       $Cliente.= '                 <input type="text" class="txt-upper t05" id="ruc'.$this->Sufijo.'" name="ruc'.$this->Sufijo.'" value="'.trim($datos['ruc']).'" maxlength="13" />';
                       $Cliente.= '                 <input type="hidden" id="idruc'.$this->Sufijo.'" name="idruc'.$this->Sufijo.'" value="'.trim($datos['idruc']).'"/>';
                       $Cliente.= '             </td>';
                       $Cliente.= '         </tr>';
                       $Cliente.= '         <tr height="30">';
                       $Cliente.= '             <td class="Form-Label" style="width:25%">Pasaporte</td>';
                       $Cliente.= '             <td class="Form-Label" style="width:50%">';
                       $Cliente.= '                 <input type="text" class="txt-upper t05" id="pasaporte'.$this->Sufijo.'" name="pasaporte'.$this->Sufijo.'" value="'.trim($datos['pasaporte']).'" maxlength="13" />';
                       $Cliente.= '                 <input type="hidden" id="idpasaporte'.$this->Sufijo.'" name="idpasaporte'.$this->Sufijo.'" value="'.trim($datos['idpasaporte']).'"/>';
                       $Cliente.= '             </td>';
                       $Cliente.= '         </tr>';
                       $Cliente.= '</table>';
                       $Cliente.= '</div>';
                       return $Cliente;
              }
              
              function ExtraeIdentificaciones($id){                        
                       $datosServicioClienteIdentificacion=new ServicioClienteIdentificacion();
                       $infoIdentificacion  = $datosServicioClienteIdentificacion->BuscarClienteIdentificacionByIDCliente($id);
                       
                       $ruc=0;  $idRuc=0;
                       $cedula=0;  $idCedula=0;
                       $pasaporte=0;  $idPasaporte=0;
                       
                       for($i=0;$i<count($infoIdentificacion);$i++){                          
                           switch($infoIdentificacion[$i]['idTipo']){
                               case '1':
                                   $ruc = $infoIdentificacion[$i]['NumeroIdent'];
                                   $idRuc = $infoIdentificacion[$i]['id'];
                                   break;
                               case '2':
                                   $cedula = $infoIdentificacion[$i]['NumeroIdent'];
                                   $idCedula = $infoIdentificacion[$i]['id'];
                                   break;
                               case '3':
                                   $pasaporte = $infoIdentificacion[$i]['NumeroIdent'];
                                   $idPasaporte = $infoIdentificacion[$i]['id'];
                                   break;
                           }
                       }
                            
                       $ObjetosEnvia = array("ruc" => $ruc,"idruc" => $idRuc,"cedula" => $cedula
                                            ,"idcedula" => $idCedula,"pasaporte" => $pasaporte
                                            ,"idpasaporte" => $idPasaporte);
                       return $ObjetosEnvia;
              }
                
        }
?>

