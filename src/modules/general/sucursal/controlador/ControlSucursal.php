<?php   
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/general/servicio/ServicioSucursal.php");    
        require_once("src/rules/general/servicio/ServicioCiudad.php");  
        
        require_once("src/modules/general/sucursal/render/RenderSucursal.php");
        require_once("src/modules/general/ciudad/render/RenderCiudad.php");
        
        require_once('src/libs/clases/DateControl.php');
        
        class ControlSucursal 
        {       private  $ServicioSucursal;
                private  $RenderSucursal;

                function __construct($Sufijo = "")
                {       $this->ServicioSucursal = new ServicioSucursal();
                        $this->RenderSucursal = new RenderSucursal($Sufijo);
                        $this->ServicioCiudad = new ServicioCiudad();
                        $this->RenderCiudad = new RenderCiudad($Sufijo);
                }

                function CargaSucursalBarButton($Sucursal)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Sucursal);
                        echo $this->RenderSucursal->CreaBarButtonSucursal($Datos);
                }
               
                function CargaSucursalMantenimiento()
                {       echo $this->RenderSucursal->CreaMantenimientoSucursal();
                }

                function CargaSucursalSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                        echo $this->RenderSucursal->CreaSearchGridSucursal($Datos);     
                }         
                
                function ConsultaSucursalByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioSucursal->BuscarSucursalByID($id);
                        return $this->RenderSucursal->MuestraSucursalForm($ajaxRespon,$Datos);
                }

                function ConsultaSucursalByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $texto = Trim($Texto);      
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                        return $this->RenderSucursal->MuestraSucursalGrid($ajaxRespon,$Datos);
                }
                
                function GuardaSucursal($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $Sucursal = json_decode($Form)->Forma;
                        $funcion = (empty($Sucursal->id) ? "MuestraSucursalGuardada" : "MuestraSucursalEditada");
                        $id = $this->ServicioSucursal->GuardaDBSucursal($Sucursal);
                        $Datos = $this->ServicioSucursal->BuscarSucursalByID($id); 
                        return $this->RenderSucursal->{$funcion}($ajaxRespon,$Datos);
                }

                function EliminaSucursal($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioSucursal->DesactivaSucursal($id);
                        $Datos = $this->ServicioSucursal->BuscarSucursalByID($id); 
                        return $this->RenderSucursal->MuestraSucursalEliminada($ajaxRespon,$Datos);
                }

                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '','estado'=>'1');
                        if ($Operacion==="btciudad")    
                        {   $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Ciudad";
                            $jsonModal['Carga'] = $this->RenderCiudad->CreaModalGridCiudad($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderCiudad->WidthModalGrid();
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        if ($Operacion==="btciudad")    
                        {   $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                            return $this->RenderCiudad->MuestraModalGridCiudad($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
                
        }       
?>

