<?php   $Sufijo = '_sucursal';
        require_once('src/modules/general/sucursal/controlador/ControlSucursal.php');
        $ControlSucursal = new ControlSucursal($Sufijo);
        
        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlSucursal,'GuardaSucursal'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlSucursal,'EliminaSucursal'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlSucursal,'ConsultaSucursalByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX'.$Sufijo, $ControlSucursal,'ConsultaSucursalByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlSucursal,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlSucursal,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                var newsucursal = "telefono:fax:descripcion:direccion:idciudad:txciudad";
                var fnssucursal = newsucursal+":estado";
                var objsucursal = fnssucursal+":id";
                    
                function ButtonClick_sucursal(Sufijo,Operacion)
                {       var clssucursal = newsucursal+":id";
                        var reqsucursal = "descripcion:direccion:idciudad:txciudad";
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Sufijo,Operacion);
                            BarButtonStateEnabled(Sufijo,"btciudad");
                            ElementStatus(Sufijo,newsucursal,"id");
                            ElementClear(Sufijo,clssucursal);
                            ElementSetValue(Sufijo,"estado",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Sufijo,Operacion);
                                BarButtonStateEnabled(Sufijo,"btciudad");
                                if (ElementGetValue(Sufijo,"estado")==0)
                                    ElementStatus(Sufijo,newsucursal+":estado","id");
                                else
                                ElementStatus(Sufijo,newsucursal,"id");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este regsistro?"))
                                    xajax_Elimina_sucursal(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(Sufijo,reqsucursal))
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    {   var Forma = PrepareElements(Sufijo,objsucursal);
                                        xajax_Guarda_sucursal(JSON.stringify({Forma}));
                                    }
                                }
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Sufijo,Operacion);
                                BarButtonStateDisabled(Sufijo,"btciudad");
                                ElementStatus(Sufijo,"id",fnssucursal);
                                ElementClear(Sufijo,clssucursal);                    
                        }
                        else 
                            xajax_CargaModal_sucursal(Operacion);
                        return false;
                }
                
                function XAJAXResponse_sucursal(Sufijo,Mensaje,Datos)
                {       if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btciudad");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",fnssucursal);
                            alert('Los datos se guardaron correctamente');
                        }
                        else if(Mensaje==='EXCEPCION')
                        {   BarButtonState(Sufijo,"addNew");
                            alert(Datos);
                        }    
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                }
                
                function SearchByElement_sucursal(Sufijo,Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    xajax_BuscaByID_sucursal(Elemento.value);
                                }
                            }
                            BarButtonState(Sufijo,"Default"); //OJO
                        }
                        else if (Elemento.id === "FindCiudadTextBx"+Sufijo)
                        {    xajax_BuscaModalByTX_sucursal("btciudad",Elemento.value);
                        }
                        else
                        {    xajax_BuscaByTX_sucursal(Elemento.value);
                        }    
                }
                    
                function SearchCiudadGetData_sucursal(Sufijo,DatosGrid)
                {       document.getElementById("idciudad"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txciudad"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }
                
                function SearchGetData_sucursal(Sufijo,Grilla)
                {       if (IsDisabled(Sufijo,"addSav"))
                        {   BarButtonState(Sufijo,"Active");
                            id = Grilla.cells[0].childNodes[0].nodeValue;
                            xajax_BuscaByID_sucursal(id);
                        }
                        return false;
                }
                
        </script>
    </head>
        <body>
        <div class="FormBasic" style="width:475px">
            <div class="FormSectionMenu">              
                <?php   $ControlSucursal->CargaSucursalBarButton($_GET['opcion']);
                        echo '<script type="text/javascript">';
                        echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                        echo '</script>';
                ?>
            </div>      
            <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlSucursal->CargaSucursalMantenimiento();  ?>
                </form>
            </div>    
            <div class="FormSectionGrid">          
                <?php  $ControlSucursal->CargaSucursalSearchGrid();  ?>
            </div>    
        </div>
        </body>
    </html>
            
