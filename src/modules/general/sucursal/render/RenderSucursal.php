<?php
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");

        class RenderSucursal 
        {       private $Sufijo;
                private $Maxlen;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->Maxlen['id'] = 2;        
                        $this->Maxlen['descripcion'] = 20;
                        $this->Maxlen['direccion'] = 150;
                        $this->Maxlen['telefono'] = 20;
                        $this->Maxlen['fax'] = 20;
                }    

                private function SearchGridConfig()
                {       $Columns['Id']      = array('40px','center','');
                        $Columns['Sucursal']= array('155px','left','');
                        $Columns['Ciudad']  = array('150px','left','');
                        $Columns['IdEstado']= array('0px','center','none');
                        $Columns['Estado']  = array('81px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '120px','AltoRow' => '20px');
                }

                function CreaBarButtonSucursal($Buttons)
                {       $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaSearchGridSucursal($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $ObjHTML = $this->GridDataHTMLSucursal($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }

                function CreaMantenimientoSucursal()
                {       $Search = new SearchInput($this->Sufijo);
                        $Opcion = '<table border=0 class="Form-Frame" cellpadding="0">';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label" style="width:20%">Id</td>';
                        $Opcion.= '           <td class="Form-Label" style="width:30%">';
                        $Opcion.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:direccion:telefono:fax:idciudad:txciudad\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Opcion.= '           </td>';
                        $Opcion.= '           <td class="Form-Label" style="width:18%"></td>';
                        $Opcion.= '           <td class="Form-Label" style="width:32%"></td>';
                        $Opcion.= '       </tr>';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Descripción</td>';
                        $Opcion.= '           <td class="Form-Label" colspan="2">';
                        $Opcion.= '               <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                        $Opcion.= '           </td>';
                        $Opcion.= '           <td class="Form-Label">';
                        $Opcion.=                 $this->CreaComboEstadoByArray(array(0,1));
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Dirección</td>';
                        $Opcion.= '           <td class="Form-Label" colspan="3">';
                        $Opcion.= '               <input type="text" class="txt-upper t13" id="direccion'.$this->Sufijo.'" name="direccion'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['direccion'].'" disabled/>';
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Ciudad</td>';
                        $Opcion.= '           <td class="Form-Label" colspan="3">';
                        $Opcion.=                 $Search->TextSch("ciudad","","")->Enabled(false)->Create("t12");
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Teléfono</td>';
                        $Opcion.= '           <td class="Form-Label">';
                        $Opcion.= '               <input type="text" class="txt-input t04" id="telefono'.$this->Sufijo.'" name="telefono'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['telefono'].'" onKeyDown="return soloNumeros(event); " disabled/>';
                        $Opcion.= '           </td>';
                        $Opcion.= '           <td class="Form-Label">Fax</td>';
                        $Opcion.= '           <td class="Form-Label">';
                        $Opcion.= '               <input type="text" class="txt-input t04" id="fax'.$this->Sufijo.'" name="fax'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['fax'].'" onKeyDown="return soloNumeros(event); " disabled/>';
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';
                        /*
                        $Opcion.= '       <tr height="28">';
                        $Opcion.= '           <td class="Form-Label">Estado</td>';
                        $Opcion.= '           <td class="Form-Label" colspan="3">';
                                              $Opcion.= $this->CreaComboEstadoByArray(array(0,1));
                        $Opcion.= '           </td>';
                        $Opcion.= '       </tr>';*/
                        return $Opcion.'</table>';
                }

                function MuestraSucursalForm($Ajax,$Sucursales)
                {       foreach ($Sucursales as $Sucursal) 
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Sucursal['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Sucursal['descripcion']));
                            $Ajax->Assign("direccion".$this->Sufijo,"value", trim($Sucursal['direccion']));
                            $Ajax->Assign("telefono".$this->Sufijo,"value", trim($Sucursal['telefono']));
                            $Ajax->Assign("fax".$this->Sufijo,"value", trim($Sucursal['fax']));
                            $Ajax->Assign("idciudad".$this->Sufijo,"value", $Sucursal['idxciudad']);
                            $Ajax->Assign("txciudad".$this->Sufijo,"value", trim($Sucursal['txtciudad']));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Sucursal['idxestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaSucursal($Ajax,"NOEXISTEID","No existe una sucursal con el ID ingresado.");
                }

                function MuestraSucursalGuardada($Ajax,$Sucursal)
                {       if (count($Sucursal)>0)
                        {   $id = str_pad($Sucursal[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraSucursalGrid($Ajax,$Sucursal);
                            return $this->RespuestaSucursal($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaSucursal($Ajax,"EXCEPCION","No se guardó la información.");
                }   

                function MuestraSucursalEditada($Ajax,$Sucursal)
                {       foreach ($Sucursal as $sucursal) 
                        {       $id = str_pad($sucursal['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraSucursalRowGrid($Ajax, $sucursal);
                                return $this->RespuestaSucursal($xAjax,"GUARDADO",$id);
                        }    
                        return $this->RespuestaSucursal($Ajax,"EXCEPCION","No se actualizó la información.");
                }

                function MuestraSucursalEliminada($Ajax,$Sucursal)
                {       foreach ($Sucursal as $sucursal)  
                        {   $xAjax = $this->MuestraSucursalRowGrid($Ajax,$sucursal);  
                            return $this->RespuestaSucursal($xAjax,"ELIMINADO",0);
                        }
                        return $this->RespuestaSucursal($Ajax,"ELIMINADO",1);
                }

                private function RespuestaSucursal($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }

                private function MuestraSucursalRowGrid($Ajax,$Sucursal)
                {       $Fila = $this->GetRow($Sucursal['id'],$Sucursal['idxestado']);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        
                        $Ajax->Assign($this->GetCell($Fila['Id'],1),"innerHTML",trim($Sucursal['descripcion'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],2),"innerHTML",trim($Sucursal['txtciudad'])); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],3),"innerHTML",$Sucursal['idxestado']); 
                        $Ajax->Assign($this->GetCell($Fila['Id'],4),"innerHTML",trim($Sucursal['txtestado'])); 
                        return $Ajax;
                }        

                function MuestraSucursalGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $ObjHTML = $this->GridDataHTMLSucursal($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLSucursal($Grid,$Datos)
                {       foreach ($Datos as $Sucursal)
                        {       $id = str_pad($Sucursal['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Sucursal['descripcion']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Sucursal['txtciudad']));
                                $Grid->CreaSearchCellsDetalle($id,'',$Sucursal['idxestado']);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Sucursal['txtestado']));
                                $Grid->CreaSearchRowsDetalle ($id,($Sucursal['idxestado']==0 ? "red" : ""));
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
                
                private function CreaComboEstadoByArray($IdArray)
                {       $Select = new ComboBox($this->Sufijo);
                        $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $Select->Combo("estado",$Datos)->Selected(1)->Create("s04");
                }        
                
                
                /***** Modal *****/

                private function SearchGridModalConfig()
                {       $Columns['Id']      = array('40px','center','');
                        $Columns['Sucursal']= array('160px','left','');
                        $Columns['Ciudad']  = array('170px','left','');
                        $Columns['Pais']    = array('120px','left','');
                        $Columns['IdEstado']= array('0px','center','none');
                        $Columns['Estado']  = array('81px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '120px','AltoRow' => '20px');
                }

                function CreaModalGridSucursal($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $ObjHTML = $this->ModalGridDataHTMLSucursal($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }
                
                function MuestraModalGridSucursal($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $ObjHTML = $this->ModalGridDataHTMLSucursal($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }        

                private function ModalGridDataHTMLSucursal($Grid,$Datos)
                {       foreach ($Datos as $Sucursal)
                        {       $id = str_pad($Sucursal['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Sucursal['descripcion']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Sucursal['txtciudad']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Sucursal['txtpais']));
                                $Grid->CreaSearchCellsDetalle($id,'',$Sucursal['idxestado']);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Sucursal['txtestado']));
                                $Grid->CreaSearchRowsDetalle ($id,($Sucursal['idxestado']==0 ? "red" : ""));
                        }
                        return $Grid->CreaSearchTableDetalle();
                }

                private function GetRow($id,$estado)
                {       $RowId = $this->Sufijo."_".str_pad($id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $Style = "color: ".($estado==0 ? "red" : "#000");
                        $RowOb = "TR".$RowId;
                        return array('Id' => $RowId,'Name' => $RowOb,'Color' => $Style);
                }
                
                private function GetCell($RowId,$Cell)
                {       return "TD".$RowId."_".str_pad(intval($Cell),3,'0',STR_PAD_LEFT);
                }
                
                
    }      

?>