<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/general/servicio/ServicioZonaCiudad.php");
        require_once("src/modules/general/zonaciudad/render/RenderZonaCiudad.php");
        require_once("src/rules/general/servicio/ServicioZonas.php");
        require_once("src/modules/general/zonas/render/RenderZonas.php");
        require_once("src/rules/general/servicio/ServicioCiudad.php");  
        require_once("src/modules/general/ciudad/render/RenderCiudad.php");

        class ControlZonaCiudad
        {       private $ServicioZonaCiudad;
                private $RenderZonaCiudad;
                private $ServicioZonas;
                private $RenderZonas;

                function __construct($Sufijo = "")
                {       $this->ServicioZonaCiudad = new ServicioZonaCiudad();
                        $this->RenderZonaCiudad = new RenderZonaCiudad($Sufijo);
                        $this->ServicioZonas = new ServicioZonas();
                        $this->RenderZonas = new RenderZonas($Sufijo);
                        $this->ServicioCiudad = new ServicioCiudad();
                        $this->RenderCiudad = new RenderCiudad($Sufijo);
                }

                function CargaZonaCiudadBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $this->RenderZonaCiudad->CreaBarButtonZonaCiudad($Datos);
                }
               
                function CargaZonaCiudadMantenimiento()
                {       echo $this->RenderZonaCiudad->CreaMantenimientoZonaCiudad();
                }

                function CargaZonaCiudadSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioZonaCiudad->BuscarZonaCiudadByDescripcion($prepareDQL);
                        echo $this->RenderZonaCiudad->CreaSearchGridZonaCiudad($Datos);
                }         
               
                function ConsultaZonaCiudadByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $Datos = $this->ServicioZonaCiudad->BuscarZonaCiudadByID($prepareDQL);
                        return $this->RenderZonaCiudad->MuestraZonaCiudadForm($ajaxRespon,$Datos);
                }

                function ConsultaZonaCiudadByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $texto = Trim($Texto);      
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioZonaCiudad->BuscarZonaCiudadByDescripcion($prepareDQL);
                        return $this->RenderZonaCiudad->MuestraZonaCiudadGrid($ajaxRespon,$Datos);
                }

                function GuardaZonaCiudad($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $ZonaCiudad = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioZonaCiudad->GuardaDBZonaCiudad($ZonaCiudad);
                        if (is_numeric($id)){
                            $function = (empty($ZonaCiudad->id) ? "MuestraZonaCiudadGuardado" : "MuestraZonaCiudadEditado");
                            $prepareDQL = array('limite' => 50,'id' => $id);
                            $Datos = $this->ServicioZonaCiudad->BuscarZonaCiudadByID($prepareDQL);
                            return $this->RenderZonaCiudad->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderZonaCiudad->MuestraZonaCiudadExcepcion($ajaxRespon,$id);
                        }
                }

                function EliminaZonaCiudad($id)
                {       $ajaxRespon = new xajaxResponse();
                        if ($this->ServicioZonaCiudad->EliminaZonaCiudad($id))
                        {   $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioZonaCiudad->BuscarZonaCiudadByDescripcion($prepareDQL);
                            return $this->RenderZonaCiudad->MuestraZonaCiudadEliminado($ajaxRespon,$Datos);
                        }else{
                            return $this->RenderZonaCiudad->MuestraZonaCiudadExcepcion($ajaxRespon,"El registro no puedo ser eliminado.");
                        }    
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'estado' => 1 );
                        if ($Operacion==="btzona")    
                        {   $Datos = $this->ServicioZonas->BuscarZonasByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Zonas";
                            $jsonModal['Carga'] = $this->RenderZonas->CreaModalGridZonas($Operacion,$Datos);
                            $jsonModal['Ancho'] = "429";
                        }  
                        else if($Operacion==="btciudad")
                        {   $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Ciudad";
                            $jsonModal['Carga'] = $this->RenderCiudad->CreaModalGridCiudad($Operacion,$Datos);
                            $jsonModal['Ancho'] = "521";
                        }
                        
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'estado' => array(1,2));
                        if ($Operacion==="btzona")    
                        {   $Datos = $this->ServicioZonas->BuscarZonasByDescripcion($prepareDQL);
                            return $this->RenderZonas->MuestraModalGridZonas($ajaxResp,$Operacion,$Datos);                                        
                        }    
                        else if($Operacion==="btciudad")
                        {   $Datos = $this->ServicioCiudad->BuscarCiudadByDescripcion($prepareDQL);
                            return $this->RenderCiudad->MuestraModalGridCiudad($ajaxResp,$Operacion,$Datos);                                        
                        }
                }

        }       
?>

