var sta_zonaciudad = "idzona:txzona:idciudad:txciudad";
var frm_zonaciudad = "id:" + sta_zonaciudad;
var cls_zonaciudad = "id:idzona:txzona:idciudad:txciudad";

function ButtonClick_zonaciudad(Sufijo, Operacion)
{
    var val_zonaciudad = "idzona:txzona:idciudad:txciudad";

    if (Operacion === 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateEnabled(Sufijo, "btzona:btciudad");
        ElementStatus(Sufijo, sta_zonaciudad, "id");
        ElementClear(Sufijo, cls_zonaciudad);
    } else if (Operacion === 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateEnabled(Sufijo, "btzona:btciudad");
        ElementStatus(Sufijo, sta_zonaciudad, "id");
        if(ElementGetValue(Sufijo,"estado")==='0'){
            ElementStatus(Sufijo, "estado", "");
        }else{
            ElementStatus(Sufijo, "", "estado");
        }
    } else if (Operacion === 'addDel')
    {
        if (confirm("Desea inactivar este regsistro?"))
        {
            xajax_Elimina_zonaciudad(ElementGetValue(Sufijo, "id"));
        }
    } else if (Operacion === 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, val_zonaciudad))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {
                var Forma = PrepareElements(Sufijo, frm_zonaciudad);
                xajax_Guarda_zonaciudad(JSON.stringify({Forma}));
            }
        }
    } else if (Operacion === 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateDisabled(Sufijo, "btzona:btciudad");
        ElementStatus(Sufijo, "id", sta_zonaciudad);
        ElementClear(Sufijo, cls_zonaciudad);
    } else
        xajax_CargaModal_zonaciudad(Operacion);
    return false;
}

function XAJAXResponse_zonaciudad(Sufijo, Mensaje, Datos)
{
    if (Mensaje === "GUARDADO")
    {
        BarButtonState(Sufijo, "Active");
        BarButtonStateDisabled(Sufijo, "btzona:btciudad");
        ElementSetValue(Sufijo, "id", Datos);
        ElementStatus(Sufijo, "id", sta_zonaciudad);
        alert('Los datos se guardaron correctamente.');
    } else if (Mensaje === 'EXCEPCION')
    {
        BarButtonState(Sufijo, "addNew");
        alert(Datos);
    } else if (Mensaje === 'ELIMINADO')
    {
        BarButtonState(Sufijo, 'addCan');
        ElementClear(Sufijo, cls_zonaciudad);
        alert(Datos);
    } else if (Mensaje === "NOEXISTEID")
    {
        BarButtonState(Sufijo, "Default");
        ElementClear(Sufijo, "id");
        alert(Datos);
    }
}

function SearchByElement_zonaciudad(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    if (Elemento.name == "id" + Sufijo)
    {
        if (Elemento.value.length != 0)
        {
            ContentFlag = document.getElementById("txzona" + Sufijo);
            if (ContentFlag.value.length == 0)
            {
                if (BarButtonState(Sufijo, "Inactive"))
                    xajax_BuscaByID_zonaciudad(Elemento.value);
            }
        }
        BarButtonState(Sufijo, "Default"); //OJO AQUI
    } else if (Elemento.id === "FindZonaTextBx" + Sufijo)
    {
        xajax_BuscaModalByTX_zonaciudad("btzona", Elemento.value);
    } else if (Elemento.id === "FindCiudadTextBx" + Sufijo)
    {
        xajax_BuscaModalByTX_zonaciudad("btciudad", Elemento.value);
    } else
        xajax_BuscaByTX_zonaciudad(Elemento.value);
}

function SearchGetData_zonaciudad(Sufijo, Grilla)
{
    if (IsDisabled(Sufijo, "addSav"))
    {
        BarButtonState(Sufijo, "Active");
        document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("idzona" + Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
        document.getElementById("txzona" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
        document.getElementById("idciudad" + Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
        document.getElementById("txciudad" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
    }
    return false;
}

function SearchZonaGetData_zonaciudad(Sufijo, DatosGrid)
{
    document.getElementById("idzona" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txzona" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
    cerrar();
    return false;
}

function SearchCiudadGetData_zonaciudad(Sufijo, DatosGrid)
{
    document.getElementById("idciudad" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txciudad" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}