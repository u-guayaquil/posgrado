<?php    
        require_once("src/rules/sistema/servicio/ServicioPerfil.php");
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
        
        class RenderZonaCiudad
        {   private $Sufijo;
            private $Maxlen; 
            private $SearchGrid;   
            
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;
                    $this->Maxlen['id'] = 3;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);      
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']    = array('40px','center','');
                    $Columns['Zonas']  = array('200px','left','');
                    $Columns['Ciudad']= array('150px','left','');
                    $Columns['IdZona']  = array('0px','left','none');
                    $Columns['IdCiudad']= array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
            }
               
            function CreaMantenimientoZonaCiudad()
            {       $Search = new SearchInput($this->Sufijo);
                    $ZonaCiudad = '<table border=0 class="Form-Frame" cellpadding="0">';
                    $ZonaCiudad.= '       <tr height="28">';
                    $ZonaCiudad.= '             <td class="Form-Label" style="width:20%">Id</td>';
                    $ZonaCiudad.= '             <td class="Form-Label" colspan="3">';
                    $ZonaCiudad.= '                <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'idzona:txzona:idciudad:txciudad\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $ZonaCiudad.= '             </td>';
                    $ZonaCiudad.= '         </tr>';
                    $ZonaCiudad.= '         <tr height="28">';
                    $ZonaCiudad.= '             <td class="Form-Label">Zona</td>';
                    $ZonaCiudad.= '             <td class="Form-Label" colspan="3">';
                                                    $ZonaCiudad.= $Search->TextSch("zona","","")->Enabled(false)->Create("t12");
                    $ZonaCiudad.= '             </td>';
                    $ZonaCiudad.= '         </tr>';
                    $ZonaCiudad.= '         <tr height="28">';
                    $ZonaCiudad.= '             <td class="Form-Label">Ciudad</td>';
                    $ZonaCiudad.= '             <td class="Form-Label" colspan="3">';
                                                    $ZonaCiudad.= $Search->TextSch("ciudad","","")->Enabled(false)->Create("t12");
                    $ZonaCiudad.= '             </td>';
                    $ZonaCiudad.= '         </tr>';
                    return $ZonaCiudad.'</table>';
            }

            function CreaBarButtonZonaCiudad($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);  
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGridZonaCiudad($ZonaCiudad)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTMLZonaCiudad($SearchGrid,$ZonaCiudad);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }

            private function GridDataHTMLZonaCiudad($Grid,$Datos)
            {       foreach ($Datos as $zonaciudad)
                    {       $id = str_pad($zonaciudad['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($zonaciudad['zona']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($zonaciudad['ciudad']));
                            $Grid->CreaSearchCellsDetalle($id,'',$zonaciudad['idzona']);
                            $Grid->CreaSearchCellsDetalle($id,'',$zonaciudad['idciudad']);
                            $Grid->CreaSearchRowsDetalle ($id,"");
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
               
            function MuestraZonaCiudadForm($Ajax,$ZonaCiudad)
            {       if (count($ZonaCiudad)>0)
                    {   foreach ($ZonaCiudad as $zonaciudad)
                        {       $id = str_pad($zonaciudad['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Ajax->Assign("id".$this->Sufijo,"value", $id);
                                $Ajax->Assign("idzona".$this->Sufijo,"value", $zonaciudad['idzona']);
                                $Ajax->Assign("txzona".$this->Sufijo,"value", trim($zonaciudad['zona']));
                                $Ajax->Assign("idciudad".$this->Sufijo,"value", $zonaciudad['idciudad']);
                                $Ajax->Assign("txciudad".$this->Sufijo,"value", trim($zonaciudad['ciudad']));
                                $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                                return $Ajax;
                        }        
                    }
                    return $this->RespuestaZonaCiudad($Ajax,"NOEXISTEID","No existe un Usuario Perfil con el ID ingresado.");
            }

            function MuestraZonaCiudadGrid($Ajax,$ZonaCiudad)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTMLZonaCiudad($SearchGrid,$ZonaCiudad);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }        
            
            function MuestraZonaCiudadGuardado($Ajax,$ZonaCiudad)
            {       if (count($ZonaCiudad)>0)
                    {   $id = str_pad($ZonaCiudad[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraZonaCiudadGrid($Ajax,$ZonaCiudad);
                        return $this->RespuestaZonaCiudad($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaZonaCiudad($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraZonaCiudadEditado($Ajax,$ZonaCiudad)
            {       foreach ($ZonaCiudad as $usuario) 
                    {       $usuario['id'] = str_pad($usuario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraZonaCiudadRowGrid($Ajax, $usuario);
                            return $this->RespuestaZonaCiudad($xAjax,"GUARDADO",$usuario['id']);
                    }    
                    return $this->RespuestaZonaCiudad($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
            
            function MuestraZonaCiudadEliminado($Ajax,$ZonaCiudad)
              {        foreach ($ZonaCiudad as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraZonaCiudadRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaZonaCiudad($xAjax,"ELIMINADO",$datos['id']);
                        }  
                       return $this->RespuestaZonaCiudad($xAjax,"EXCEPCION","No se eliminó la información.");
              }

            function MuestraZonaCiudadRowGrid($Ajax,$ZonaCiudad,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($ZonaCiudad['id'],$estado);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);                    

                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($ZonaCiudad['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT)); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($ZonaCiudad['zona'])); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",$ZonaCiudad['ciudad']); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$ZonaCiudad['idzona']); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$ZonaCiudad['idciudad']); 
                    return $Ajax;
            }

            function MuestraZonaCiudadExcepcion($Ajax,$Msg)
            {       return $this->RespuestaZonaCiudad($Ajax,"EXCEPCION",$Msg);    
            }
            
            private function RespuestaZonaCiudad($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
        }
?>

