<?php   $Sufijo = '_zonaciudad';

        require_once('src/modules/general/zonaciudad/controlador/ControlZonaCiudad.php');
        $ControlZonaCiudad = new ControlZonaCiudad($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlZonaCiudad,'GuardaZonaCiudad'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlZonaCiudad,'EliminaZonaCiudad'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlZonaCiudad,'ConsultaZonaCiudadByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX'.$Sufijo, $ControlZonaCiudad,'ConsultaZonaCiudadByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlZonaCiudad,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlZonaCiudad,'ConsultaModalGridByTX'));
        
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/general/zonaciudad/js/JSZonaCiudad.js"></script>
        </head>
        <body>
        <div class="FormBasic" style="width:480px">
            <div class="FormSectionMenu">              
            <?php   $ControlZonaCiudad->CargaZonaCiudadBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlZonaCiudad->CargaZonaCiudadMantenimiento();  ?>
                    </form>
            </div>    
            <div class="FormSectionGrid">          
            <?php   $ControlZonaCiudad->CargaZonaCiudadSearchGrid();  ?>
            </div>    
        </div>
        </body>
        </html>

        