<?php   

    $Sufijo = '_empresa';
    require_once('src/modules/general/empresa/controlador/ControlEmpresa.php');
    $ControlEmpresa = new ControlEmpresa($Sufijo);

    $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlEmpresa,'GuardaEmpresa'));
    $xajax->register(XAJAX_FUNCTION,array('Muestra'.$Sufijo, $ControlEmpresa,'MuestraEmpresa'));
    $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlEmpresa,'CargaModalGrid'));
    $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlEmpresa,'ConsultaModalGridByTX'));
    $xajax->processRequest();
    
?>
    <!doctype html>
    <html>
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

            <?php $xajax->printJavascript(); 
                  require_once('src/utils/links.php');
            ?>
        
    <script type="text/javascript">
            var objempresa = "comercial:razon:idrazon:nurazon:legal:idlegal:nulegal:contador:idcontador:nucontador:obligado:nucontribuyente:idmoneda:txsucursal:idsucursal:idpais";
                
            function ButtonClick_empresa(Sufijo,Operacion)
            {       
                    var valempresa = "comercial:razon:nurazon:legal:nulegal:txsucursal:idsucursal";
                    if (Operacion=="addMod")
                    {   BarButtonState(Sufijo,Operacion);
                        BarButtonStateEnabled(Sufijo,"btsucursal");
                        ElementStatus(Sufijo,objempresa,"");
                    }
                    else if (Operacion=="addSav")
                    {   if (document.getElementById("idcontador"+Sufijo).value!=0)
                            valempresa = valempresa + ":nucontador:contador";
                        else
                            ElementClear(Sufijo,"nucontador:contador");
                            
                        if (ElementValidateBeforeSave(Sufijo,valempresa))
                        {   if (BarButtonState(Sufijo,"Inactive"))
                            {   var Forma = PrepareElements(Sufijo,objempresa);
                                xajax_Guarda_empresa(JSON.stringify({Forma})); 
                            }    
                        }
                    }
                    else if (Operacion=="addCan")
                    {   BarButtonState(Sufijo,"Active");
                        BarButtonStateDisabled(Sufijo,"btsucursal");
                        ElementStatus(Sufijo,"",objempresa);
                        xajax_Muestra_empresa(); 
                    }
                    else 
                    xajax_CargaModal_empresa(Operacion,ElementGetValue(Sufijo, "idpais"));
                    return false;
            }

            function SearchByElement_empresa(Sufijo,Elemento)
            {       if (Elemento.id === "FindSucursalTextBx"+Sufijo)
                    {   xajax_BuscaModalByTX_empresa("btsucursal",Elemento.value);
                    }    
                    return false;
            }

            function SearchSucursalGetData_empresa(Sufijo,DatosGrid)
            {       document.getElementById("idsucursal"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                    document.getElementById("txsucursal"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
                    cerrar();
                    return false;
            }

            function XAJAXResponse_empresa(Sufijo,Mensaje,Datos)
            {       if (Mensaje==="GUARDADO")
                    {   BarButtonState(Sufijo,"Active");
                        BarButtonStateDisabled(Sufijo,"btsucursal");
                        ElementStatus(Sufijo,"",objempresa);
                        alert(Datos);
                    }
                    else if(Mensaje==='EXCEPCION')
                    {   BarButtonState(Sufijo,"addNew");
                        alert(Datos);
                    }    
            }
            
            function ControlIdpais_empresa(Sufijo,ObjPais)
            {       ElementSetValue(Sufijo, "idsucursal", "");
                    ElementSetValue(Sufijo, "txsucursal", "");
                    return false;
            }
            
    </script>
    </head>
            <body>
                <div class="FormBasic" style="width:650px">
                    <div class="FormSectionMenu">              
                    <?php   $ControlEmpresa->CargaEmpresaBarButton($_GET['opcion']);
                            echo '<script type="text/javascript">';
                            echo '        BarButtonState(\''.$Sufijo.'\',"Active"); ';
                            echo '</script>';
                    ?>
                    </div>    
                    <div class="FormSectionData">              
                            <form id="<?php echo 'Form'.$Sufijo; ?>">
                            <?php  $ControlEmpresa->CargaEmpresaMantenimiento();  ?>
                            </form>
                    </div>    
                </div>
            </body>
    </html>
