<?php
    
    require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
    require_once("src/rules/general/servicio/ServicioEmpresa.php");
    require_once("src/modules/general/empresa/render/RenderEmpresa.php");
    
    require_once('src/libs/clases/DateControl.php');
    require_once("src/libs/clases/Utils.php");
    require_once("src/rules/general/servicio/ServicioSucursal.php");    
    require_once("src/modules/general/sucursal/render/RenderSucursal.php");
    
    
    class ControlEmpresa 
    {       private $ServicioEmpresa;
            private $ServicioSucursal;
            private $RenderEmpresa;
            private $RenderSucursal;
            
            function __construct($Sufijo = "")
            {       $this->ServicioEmpresa = new ServicioEmpresa();
                    $this->RenderEmpresa = new RenderEmpresa($Sufijo);
                    $this->ServicioSucursal = new ServicioSucursal();
                    $this->RenderSucursal = new RenderSucursal($Sufijo);
            }
            
            function CargaEmpresaBarButton($Opcion)
            {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                    $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                    echo $this->RenderEmpresa->CreaBarButtonEmpresa($Datos);
            }
            
            function CargaEmpresaMantenimiento()
            {       $Datos = $this->ServicioEmpresa->ObtenerDatosEmpresa();
                    echo $this->RenderEmpresa->CreaManteminientoEmpresa($Datos);
            }

            function GuardaEmpresa($Form)
            {       $ajaxResp = new xajaxResponse();
                    $response = $this->ServicioEmpresa->ActualizaEmpresa(json_decode($Form)->Forma);
                    return $this->RenderEmpresa->MuestraSucursalActualizada($ajaxResp,$response);
            }
            
            function MuestraEmpresa()
            {       $ajaxResp = new xajaxResponse(); 
                    $Datos = $this->ServicioEmpresa->ObtenerDatosEmpresa();
                    return $this->RenderEmpresa->MuestraDatosEmpresaForm($Datos,$ajaxResp);
            }
            
            function CargaModalGrid($Operacion,$RelID)
            {       $ajaxResp = new xajaxResponse();    
                    $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '','pais'=> $RelID);

                    if ($Operacion==="btsucursal")    
                    {   $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Sucursal";
                        $jsonModal['Carga'] = $this->RenderSucursal->CreaModalGridSucursal($Operacion,$Datos);
                        $jsonModal['Ancho'] = "603";
                    }   
                    $ajaxResp->call("CreaModal", json_encode($jsonModal));
                    return $ajaxResp; 
            }
            
            function ConsultaModalGridByTX($Operacion,$Texto = "")
            {       $ajaxResp = new xajaxResponse();    
                    $texto = trim($Texto);
                    $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                    if ($Operacion==="btsucursal")    
                    {   $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                        return $this->RenderSucursal->MuestraModalGridSucursal($ajaxResp,$Operacion,$Datos);                                        
                    }    
            }
    }

?>
