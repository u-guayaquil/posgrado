<?php
require_once('src/rules/impuesto/servicio/ServicioIdentificacion.php');
require_once('src/rules/general/servicio/ServicioMoneda.php');
require_once('src/rules/general/servicio/ServicioPais.php');

require_once("src/libs/clases/SearchGrid.php");
require_once("src/libs/clases/BarButton.php");
require_once('src/libs/clases/ComboBox.php');
require_once('src/libs/clases/SearchInput.php');

class RenderEmpresa 
{           private $Sufijo;
            private $Select;
            private $Search;

            function __construct($Sufijo = "") 
            {       $this->Sufijo = $Sufijo;
                    $this->Select = new ComboBox($Sufijo);
                    $this->Search = new SearchInput($Sufijo);
            }
            
            function CreaManteminientoEmpresa($Datos)
            {       
                    $ServicioIdentificacion = new ServicioIdentificacion();
                    $ServicioPais = new ServicioPais();
                    $ServicioMoneda = new ServicioMoneda();
                    
                    $datoRepresen = $ServicioIdentificacion->BuscarByArrayID(array('tipo' => '1,2'));
                    $datoContador = $ServicioIdentificacion->BuscarByArrayID(array('tipo' => '0,1'));
                    $datoMoneda  = $ServicioMoneda->SeleccionarTodos(array('estado' => 1));
                    $datoPais = $ServicioPais->BuscarByArrayID(array('estado' => '1'));
                    
                    $checked = ($Datos->obl_contabilidad==1 ? 'checked': '');
                    
                    $Empresa = '<table border="0" class="Form-Frame" cellpadding="0">';
                    $Empresa.= '        <tr height="30">';
                    $Empresa.= '            <td class="Form-Label" style="width:26%">Nombre Comercial</td>';
                    $Empresa.= '            <td class="Form-Label" colspan="3">';
                    $Empresa.= '                <input type="text" class="txt-upper t18" id="comercial'.$this->Sufijo.'" name="comercial'.$this->Sufijo.'" value="'.trim($Datos->comercial).'" maxlength="100" disabled/>';
                    $Empresa.= '            </td>';
                    $Empresa.= '        </tr>';
                    
                    $Empresa.= '        <tr height="30">';
                    $Empresa.= '            <td class="Form-Label" style="width:26%">Obligado a Contabilidad</td>';
                    $Empresa.= '            <td class="Form-Label" style="width:38%">';
                    $Empresa.= '                <input type="checkbox" id="obligado'.$this->Sufijo.'" name="obligado'.$this->Sufijo.'" value="1" '.$checked.' disabled/>';
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label-Center" style="width:18%">Documento</td>';
                    $Empresa.= '            <td class="Form-Label-Center" style="width:18%">Número</td>';
                    $Empresa.= '        </tr>';
                    
                    $Empresa.= '        <tr height="30">';
                    $Empresa.= '            <td class="Form-Label">Razón Social</td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.= '                <input type="text" class="txt-upper t09" id="razon'.$this->Sufijo.'" name="razon'.$this->Sufijo.'" value="'.trim($Datos->razonsocial).'" maxlength="100" disabled/>';                    
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.=                  $this->Select->Combo('idrazon',$datoRepresen)->Fields('id', 'identificacion')->Selected($Datos->tipo_ident_razon)->Create("s04");
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.= '                <input type="text" class="txt-input t04" id="nurazon'.$this->Sufijo.'" name="nurazon'.$this->Sufijo.'" value="'.trim($Datos->num_ident_razon).'" maxlength="15" onKeyDown="return soloNumeros(event); " disabled/>';
                    $Empresa.= '            </td>';
                    $Empresa.= '        </tr>';
                    
                    $Empresa.= '        <tr height="30">';
                    $Empresa.= '            <td class="Form-Label">Representante Legal</td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.= '                <input type="text" class="txt-upper t09" id="legal'.$this->Sufijo.'" name="legal'.$this->Sufijo.'" value="'.trim($Datos->representante).'" maxlength="80" disabled/>';
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.=                  $this->Select->Combo('idlegal',$datoRepresen)->Fields('id', 'identificacion')->Selected($Datos->tipo_ident_repres)->Create("s04");                    
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.= '                <input type="text" class="txt-input t04" id="nulegal'.$this->Sufijo.'" name="nulegal'.$this->Sufijo.'" value="'.trim($Datos->num_ident_repres).'" maxlength="15" onKeyDown="return soloNumeros(event); " disabled/>';
                    $Empresa.= '            </td>';
                    $Empresa.= '        </tr>';
                    
                    $Empresa.= '        <tr height="30">';
                    $Empresa.= '            <td class="Form-Label">Contador</td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.= '                <input type="text" class="txt-upper t09" id="contador'.$this->Sufijo.'" name="contador'.$this->Sufijo.'" value="'.trim($Datos->contador).'" maxlength="80" disabled/>';
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.=                  $this->Select->Combo('idcontador',$datoContador)->Fields('id', 'identificacion')->Selected($Datos->tipo_ident_contador)->Create("s04");                    
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.= '                <input type="text" class="txt-input t04" id="nucontador'.$this->Sufijo.'" name="nucontador'.$this->Sufijo.'" value="'.trim($Datos->num_ident_contador).'" maxlength="15" onKeyDown="return soloNumeros(event); " disabled/>';
                    $Empresa.= '            </td>';
                    $Empresa.= '        </tr>';

                    $Empresa.= '        <tr height="30">';
                    $Empresa.= '            <td class="Form-Label">Pais</td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.=                  $this->Select->Combo('idpais',$datoPais)->Fields('id', 'descripcion')->Selected($Datos->idpais)->Evento()->Create("s09");                    
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label">Contrib.Especial</td>';
                    $Empresa.= '            <td class="Form-Label">';
                                                $EspContribuyente = $Datos->esp_contribuyente;
                    $Empresa.= '                <input type="text" class="txt-input t04" id="nucontribuyente'.$this->Sufijo.'" name="nucontribuyente'.$this->Sufijo.'" value="'.($EspContribuyente==0 ? "": $EspContribuyente).'" maxlength="5" onKeyDown="return soloNumeros(event); " disabled/>';
                    $Empresa.= '            </td>';
                    $Empresa.= '        </tr>';

                    $Empresa.= '        <tr height="30">';
                    $Empresa.= '            <td class="Form-Label">Sucursal</td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.=                  $this->Search->TextSch("sucursal",$Datos->idsucursal,$Datos->descripcion)->Enabled(false)->Create("t08");
                    $Empresa.= '            </td>';
                    $Empresa.= '            <td class="Form-Label">Moneda</td>';
                    $Empresa.= '            <td class="Form-Label">';
                    $Empresa.=                  $this->Select->Combo('idmoneda',$datoMoneda)->Selected($Datos->idmoneda)->Create("s04");
                    $Empresa.='             </td>';
                    $Empresa.= '        </tr>';
                    return $Empresa.'</table>';
            }
            
            function CreaBarButtonEmpresa($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function MuestraDatosEmpresaForm($Datos,$Ajax)
            {       $Ajax->Assign("comercial".$this->Sufijo,"value",trim($Datos->comercial));
                    $Ajax->Assign("razon".$this->Sufijo,"value",trim($Datos->razonsocial));
                    $Ajax->Assign("idrazon".$this->Sufijo,"value",$Datos->tipo_ident_razon);
                    $Ajax->Assign("nurazon".$this->Sufijo,"value",trim($Datos->num_ident_razon));
                    $Ajax->Assign("legal".$this->Sufijo,"value",trim($Datos->representante));
                    $Ajax->Assign("idlegal".$this->Sufijo,"value",$Datos->tipo_ident_repres);
                    $Ajax->Assign("nulegal".$this->Sufijo,"value",trim($Datos->num_ident_repres));
                    $Ajax->Assign("contador".$this->Sufijo,"value",trim($Datos->contador));
                    $Ajax->Assign("idcontador".$this->Sufijo,"value",$Datos->tipo_ident_contador);
                    $Ajax->Assign("nucontador".$this->Sufijo,"value",trim($Datos->num_ident_contador));
                    $Ajax->Assign("obligado".$this->Sufijo,"checked", ($Datos->obl_contabilidad==1 ? true: false));
                    $Ajax->Assign("idsucursal".$this->Sufijo,"value",$Datos->idsucursal);
                    $Ajax->Assign("txsucursal".$this->Sufijo,"value",$Datos->descripcion);
                    $Ajax->Assign("nucontribuyente".$this->Sufijo,"value",($Datos->esp_contribuyente==0 ? "": $Datos->esp_contribuyente));
                    $Ajax->Assign("idmoneda".$this->Sufijo,"value",$Datos->idmoneda);
                    return $Ajax;
            }
            
            function MuestraSucursalActualizada($Ajax,$Respuesta)
            {       if (is_bool($Respuesta))
                    {   if ($Respuesta)
                            return $this->RespuestaEmpresa($Ajax,"GUARDADO","La información se actualizó con exito.");
                        else
                        return $this->RespuestaEmpresa($Ajax,"EXCEPCION","No se pudo actualizar la información.");
                    }    
                    else
                    return $this->RespuestaEmpresa($Ajax,"EXCEPCION",$Respuesta);
            }
            
            private function RespuestaEmpresa($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
}
