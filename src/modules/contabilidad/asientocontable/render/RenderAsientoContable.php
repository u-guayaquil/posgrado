<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/contabilidad/servicio/ServicioTipoAsiento.php");
        require_once("src/rules/contabilidad/servicio/ServicioPlantillaContable.php");
        require_once("src/rules/general/entidad/Estado.php"); 
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/DateControl.php");
         
        class RenderAsientoContable
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
               
              function CreaAsientoContableMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);
                       $AsientoContable = '<table class="Form-Frame" cellpadding="0">';
                       $AsientoContable.= '       <tr height="30">';
                       $AsientoContable.= '            <td class="Form-Label" style="width:25%">Id</td>';
                       $AsientoContable.= '             <td class="Form-Label" style="width:75%">';
                       $AsientoContable.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" '
                                                          . 'value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'estado:tipoasiento:numtipoasiento:idsucursal:fecha:glosa\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $AsientoContable.= '                 <input type="hidden" id="idAsiento'.$this->Sufijo.'" name="idAsiento'.$this->Sufijo.'" value="">';
                       $AsientoContable.= '             </td>';
                       $AsientoContable.= '        </tr>';
                       $AsientoContable.= '       <tr height="30">';
                       $AsientoContable.= '             <td class="Form-Label" style="width:25%">Tipo</td>';
                       $AsientoContable.= '             <td class="Form-Label" style="width:25%">';
                                                    $AsientoContable.= $this->CreaComboTipoAsiento();
                       $AsientoContable.= '             </td>';
                       $AsientoContable.= '             <td class="Form-Label" style="width:25%">N&uacute;mero</td>';
                       $AsientoContable.= '             <td class="Form-Label" style="width:25%">';
                       $AsientoContable.= '                 <input type="text" class="txt-upper t04" id="numtipoasiento'.$this->Sufijo.'" name="numtipoasiento'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $AsientoContable.= '             </td>';
                       $AsientoContable.= '         </tr>';
                       $AsientoContable.= '       <tr height="28">';
                       $AsientoContable.= '           <td class="Form-Label">Sucursal</td>';
                       $AsientoContable.= '           <td class="Form-Label">';
                                              $AsientoContable.= $Search->TextSch("sucursal","","")->Enabled(false)->Create("t05");
                       $AsientoContable.= '           </td>';
                       $AsientoContable.= '           <td class="Form-Label">Fecha </td>';
                       $AsientoContable.= '           <td class="Form-Label">'
                                                         . '<input class="txt-upper t04" name = "fecha'.$this->Sufijo.'" id = "fecha'.$this->Sufijo.'" disabled/>'
                                                     . '</td>';
                       $AsientoContable.= '       </tr>';
                       $AsientoContable.= '         <tr height="30">';
                       $AsientoContable.= '             <td class="Form-Label" style="width:25%">Glosa</td>';
                       $AsientoContable.= '             <td class="Form-Label" style="width:75%" colspan="3">';
                       $AsientoContable.= '                 <textarea id="glosa'.$this->Sufijo.'" name="glosa'.$this->Sufijo.'" rows="2" cols="30" disabled></textarea>';
                       $AsientoContable.= '             </td>';
                       $AsientoContable.= '         </tr>';                     
                       $AsientoContable.= '         <tr height="30">';
                       $AsientoContable.= '             <td class="Form-Label">Plantilla</td>';
                       $AsientoContable.= '             <td class="Form-Label">';
                                                    $AsientoContable.= $this->CreaComboPlantilla();
                       $AsientoContable.= '             </td>';
                       $AsientoContable.= '             <td class="Form-Label">Estado</td>';
                       $AsientoContable.= '             <td class="Form-Label">';
                                                    $AsientoContable.= $this->CreaComboEstado();
                       $AsientoContable.= '             </td>';
                       $AsientoContable.= '         </tr>';
                       $AsientoContable.= '         <tr height="30">';
                       $AsientoContable.= '             <td class="Form-Label" style="width:100%" colspan="4">';
                       $AsientoContable.= '                 <div id="SeccionCabIngresoDetalleAsiento'.$this->Sufijo.'" class="SchCab"> ';
                       $AsientoContable.= '                   <table class="SchGrdCab" id="CabIngresoDetalleAsiento'.$this->Sufijo.'" name="CabIngresoDetalleAsiento'.$this->Sufijo.'" cellpadding="0">';
                       $AsientoContable.= '                       <tbody>';                       
                       $AsientoContable.= '                       <tr class="SchGrdCabCell">';
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 150px;">Cuenta</td>';
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 100px;">Tipo</td>';  
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 100px;">Valor</td>';                       
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 100px;">Glosa</td>';
                       $AsientoContable.= '                       </tr>';                       
                       $AsientoContable.= '                       </tbody>';
                       $AsientoContable.= '                   </table>';
                       $AsientoContable.= '                 </div>';
                       $AsientoContable.= '                   <table class="SchGrdCab" id="CabIngresoDetalleAsiento'.$this->Sufijo.'" name="CabIngresoDetalleAsiento'.$this->Sufijo.'" cellpadding="0">';
                       $AsientoContable.= '                       <tbody>';
                       $AsientoContable.= '                       <tr class="Form-Label">';
                       $AsientoContable.= '                          <td class="Form-Label" style="width: 150px; height:50px;" align="center">';
                       $AsientoContable.=                                 $Search->TextSch("Cuenta","","")->Enabled(false)->Create("t04");
                       $AsientoContable.= '                          </td>';
                       $AsientoContable.= '                          <td class="Form-Label" style="width: 100px;" align="center">';
                       $AsientoContable.= '                               <select id="tipomovimiento'.$this->Sufijo.'" name="tipomovimiento'.$this->Sufijo.'" class="sel-input s03" disabled>';
                       $AsientoContable.= '                                   <option value="D">DEBE</option>';
                       $AsientoContable.= '                                   <option value="H">HABER</option>';
                       $AsientoContable.= '                               </select>';
                       $AsientoContable.= '                          </td>';
                       $AsientoContable.= '                          <td class="Form-Label" style="width:21%" align="center">';
                       $AsientoContable.= '                             <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t03" id="valordetalle'.$this->Sufijo.'" name="valordetalle'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $AsientoContable.= '                          </td>';
                       $AsientoContable.= '                          <td class="Form-Label" style="width: 100px; height:50px;" align="center">';
                       $AsientoContable.= '                             <textarea id="glosadetalle'.$this->Sufijo.'" name="glosadetalle'.$this->Sufijo.'" rows="2" cols="10" disabled></textarea>';
                       $AsientoContable.= '                          </td>';
                       $AsientoContable.= '                          <td class="Form-Label" style="width: 10px; height:50px;" align="center">';
                       $AsientoContable.= '                             <input id="btn_addAsiento'.$this->Sufijo.'" src="src/modules/contabilidad/plantillacontable/img/add.png" '
                                                                         . ' type="image" name="addAsiento'.$this->Sufijo.'" onclick="return addFilaAsiento(event,\''.$this->Sufijo.'\');" />';
                       $AsientoContable.= '                          </td>';
                       $AsientoContable.= '                          <td class="Form-Label" style="width: 10px; height:50px;" align="center">';
                       $AsientoContable.= '                             <input id="btn_borraDatos'.$this->Sufijo.'" src="src/modules/nomina/plantillarol/img/clean.png" '
                                                                         . ' type="image" name="borraDatos'.$this->Sufijo.'" style="width: 30px; height:25px;" '
                                                                         . 'onclick="return clean(\''.$this->Sufijo.'\');" />';
                       $AsientoContable.= '                          </td>';
                       $AsientoContable.= '                       </tr>';
                       $AsientoContable.= '                       </tbody>';
                       $AsientoContable.= '                   </table>';
                       $AsientoContable.= '                 <div id="SeccionCabDetalleAsiento'.$this->Sufijo.'" class="SchCab"> ';
                       $AsientoContable.= '                   <table class="SchGrdCab" id="CabDetalleAsiento'.$this->Sufijo.'" name="CabDetalleAsiento'.$this->Sufijo.'" cellpadding="0">';
                       $AsientoContable.= '                       <tbody>';
                       $AsientoContable.= '                       <tr class="SchGrdCabCell">';
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 30px;">&nbsp;&nbsp;&nbsp;</td>';
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 150px;">Cuenta</td>';                       
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="display:none;">idCuenta</td>';    
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="display:none;">Tipo</td>'; 
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="display:none;">idTipo</td>';       
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 80px">Debe</td>';
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="display:none;">idDebe</td>'; 
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 80px">Haber</td>';
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="display:none;">idHaber</td>';   
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 170px;">Glosa</td>';
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="display:none;">idGlosa</td>';    
                       $AsientoContable.= '                       </tr>';
                       $AsientoContable.= '                       </tbody>';
                       $AsientoContable.= '                   </table>';
                       $AsientoContable.= '                 </div>';
                       $AsientoContable.= '                 <div id="SeccionDetalleAsiento'.$this->Sufijo.'" class="SchDet"> ';
                       $AsientoContable.= '                   <table class="SchGrdDet" id="DetSch'.$this->Sufijo.'DetalleAsiento" name="DetSch'.$this->Sufijo.'DetalleAsiento" cellpadding="0">';
                       $AsientoContable.= '                       <tbody>';
                       $AsientoContable.= '                       </tbody>';
                       $AsientoContable.= '                   </table>';
                       $AsientoContable.= '                 </div>';
                       $AsientoContable.= '             </td>';
                       $AsientoContable.= '         </tr>';
                       $AsientoContable.= '         <tr height="30">';
                       $AsientoContable.= '             <td class="Form-Label" style="width:100%" colspan="4">';
                       $AsientoContable.= '                 <div id="SeccionTotalDetalleAsiento'.$this->Sufijo.'" class="SchCab"> ';
                       $AsientoContable.= '                   <table class="SchGrdCab" id="TotalDetalleAsiento'.$this->Sufijo.'" name="TotalDetalleAsiento'.$this->Sufijo.'" cellpadding="0">';
                       $AsientoContable.= '                       <tbody>';                       
                       $AsientoContable.= '                       <tr class="SchGrdCabCell">';
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 180px;">TOTAL</td>';    
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 80px"><span>$</span><span id="totalDebe'.$this->Sufijo.'">0.00</span></td>';
                       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 80px"><span>$</span><span id="totalHaber'.$this->Sufijo.'">0.00</span></td>'; 
		       $AsientoContable.= '                          <td class="SchGrdCabCell" style="width: 170px;">&nbsp;&nbsp;&nbsp;</td>';  
                       $AsientoContable.= '                       </tr>';                       
                       $AsientoContable.= '                       </tbody>';
                       $AsientoContable.= '                   </table>';
                       $AsientoContable.= '                 </div>';
                       $AsientoContable.= '             </td>';
                       $AsientoContable.= '         </tr>';
                       $AsientoContable.= '</table>';
                       return $AsientoContable;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['N&uacute;mero'] = array('75px','left','');
                        $Columns['Tipo Asiento'] = array('100px','left','');
                        $Columns['Sucursal'] = array('85px','left','');
                        $Columns['Creacion'] = array('85px','left','none');
                        $Columns['Estado'] = array('85px','left','');
                        $Columns['tipo'] = array('0px','left','none');
                        $Columns['glosa'] = array('0px','left','none');
                        $Columns['idsucursal'] = array('0px','left','none');
                        $Columns['fecha'] = array('0px','left','none');
                        $Columns['idestado'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '400px','AltoRow' => '20px');
              }

              function CreaAsientoContableSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->AsientoContableGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }        

              private function AsientoContableGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $plantillacontable)
                      {        $fecreacion = $this->Fechas->changeFormatDate($plantillacontable['fecreacion'],"DMY")[1];
                               $fecha= $this->Fechas->changeFormatDate($plantillacontable['fecha'],"DMY")[1];
                               $id = str_pad($plantillacontable['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['nutipo_asiento']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['tipoasiento']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['sucursal']));    
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',($fecreacion));  
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['txtestado']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['idtipo_asiento']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['glosa']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['idsucursal']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecha); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['idestado'])); 
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($plantillacontable['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function TraeDatosTipoAsiento()
              {        
                       $ServicioTipoAsiento = new ServicioTipoAsiento();
                       $datoTipoAsiento = $ServicioTipoAsiento->BuscarTipoAsiento(array('estado' => 1));
                       return $datoTipoAsiento;
              }
              
              private function CreaComboTipoAsiento()
              {       
                       $datosTipoAsiento= $this->TraeDatosTipoAsiento();
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("tipoasiento",$datosTipoAsiento)->Selected(1)->Create("s04");
              }
              
              function TraeDatosPlantillas()
              {        
                       $ServicioPlantillaContable = new ServicioPlantillaContable();
                       $datoPlantillaContable = $ServicioPlantillaContable->BuscarPlantillaContable(array('estado' => 1));
                       return $datoPlantillaContable;
              }
              
              private function CreaComboPlantilla()
              {       
                       $datosTipoAsiento= $this->TraeDatosPlantillas();
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("plantilla",$datosTipoAsiento)->Evento()->Selected(1)->Create("s07");
              }
              
              function MuestraAsientoContable($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $fecha = $this->Fechas->changeFormatDate($Datos[0]['fecha'],"DMY")[1];
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("idPedido".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("numasiento".$this->Sufijo,"value", trim($Datos[0]['nutipo_asiento']));
                            $Ajax->Assign("tipoasiento".$this->Sufijo,"value", trim($Datos[0]['idtipo_asiento']));
                            $Ajax->Assign("idsucursal".$this->Sufijo,"value", trim($Datos[0]['idsucursal']));
                            $Ajax->Assign("txsucursal".$this->Sufijo,"value", trim($Datos[0]['sucursal']));
                            $Ajax->Assign("fecha".$this->Sufijo,"value", $fecha);
                            $Ajax->Assign("glosa".$this->Sufijo,"value", trim($Datos[0]['glosa']));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);

                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaPlantillaRol($Ajax,"NOEXISTEID","No existe un Asiento con ese ID.");
                       }
              }
              
              function MuestraAsientoContableGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->AsientoContableGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }
              
              private function SearchGridValuesDetalle()
              {         $Columns['opcion'] = array('30px','left','');
                        $Columns['Cuenta'] = array('140px','left','');
                        $Columns['idCuenta'] = array('0px','left','none');
                        $Columns['Tipo'] = array('150px','left','none');
                        $Columns['idTipo'] = array('0px','left','none');
                        $Columns['Debe'] = array('80px','right','');
                        $Columns['idDebe'] = array('0px','left','none');
                        $Columns['Haber'] = array('80px','right','');
                        $Columns['idHaber'] = array('0px','left','none');
                        $Columns['Glosa'] = array('170px','center','');
                        $Columns['idGlosa'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
                        
              }
              
              function MuestraDetalleAsientoContable($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo.'DetalleAsiento');
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesDetalle());
                       $ObjHTML = $this->DetalleAsientoContable($SearchGrid,$Datos);
                       $Ajax->Assign('SeccionDetalleAsiento'.$this->Sufijo,"innerHTML", $ObjHTML);
                       $Ajax->call("SumaValoresAsiento",$this->Sufijo);
                       return $Ajax;
              }
              
              private function DetalleAsientoContable($ObjSchGrid,$Datos)
              {       $contador =0;    
                      foreach ($Datos as $plantillacontable)
                      {        $contador++;
                               $id = str_pad($plantillacontable['id'],2,'0',STR_PAD_LEFT);
                               $cuenta = trim($plantillacontable['cuenta']);
                               $idcuenta = trim($plantillacontable['idcuenta']);
                               $tipo = trim($plantillacontable['idmovnto']);
                               $valor = ($plantillacontable['valor']);
                               $glosa = trim($plantillacontable['glosa']);
                               $idGeneral = $this->Sufijo.$idcuenta;
                               $debe='0.00';
                               $haber='0.00';
                               if($tipo=='D'){
                                   $destipo = 'DEBE';
                                   $debe = number_format ($valor,2);
                               }else{
                                   $destipo = 'HABER';
                                   $haber = number_format ($valor,2);
                               }
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input id="btn_eliAsiento'.$this->Sufijo.'" class="borrar"'
                                      .'src="src/modules/contabilidad/plantillacontable/img/delete.png"'
                                      .'type="image" name="eliAsiento'.$this->Sufijo.'"'
                                      .'onclick="return deleteFilaAsiento();" />');
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cuenta);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detcuenta'.$idGeneral.'" name="detcuenta'.$idGeneral.'" value="'.$idcuenta.'">');
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$destipo);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="dettipo'.$idGeneral.'" name="dettipo'.$idGeneral.'" value="'.$tipo.'">');
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$debe);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detdebe'.$idGeneral.'" name="detdebe'.$idGeneral.'" value="'.$debe.'">');
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$haber);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="dethaber'.$idGeneral.'" name="dethaber'.$idGeneral.'" value="'.$haber.'">');
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$glosa);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detglosa'.$idGeneral.'" name="detglosa'.$idGeneral.'" value="'.$glosa.'">');
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,"");
                      }
                     
                      return $ObjSchGrid->CreaSearchTableDetalle(1);
              }
               
              function MuestraAsientoContableGuardado($Ajax,$AsientoContable)
              {       if (count($AsientoContable)>0)
                        {   $id = str_pad($AsientoContable[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraAsientoContableGrid($Ajax,$AsientoContable);
                            return $this->RespuestaAsientoContable($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaAsientoContable($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraAsientoContableEditado($Ajax,$AsientoContable)
              {        foreach ($AsientoContable as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraAsientoContableRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaAsientoContable($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaAsientoContable($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraAsientoContableEliminado($Ajax,$AsientoContable)
              {        foreach ($AsientoContable as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraAsientoContableRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaAsientoContable($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaAsientoContable($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraAsientoContableRowGrid($Ajax,$AsientoContable,$estado=1)
              {         $Fila = $this->SearchGrid->GetRow($AsientoContable['id'],$estado); 
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",trim($AsientoContable['id']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($AsientoContable['nutipo_asiento']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($AsientoContable['tipoasiento']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($AsientoContable['sucursal']));
                        $fecreacion = $this->Fechas->changeFormatDate($AsientoContable['fecreacion'],"DMY")[1];
                        $fecha= $this->Fechas->changeFormatDate($AsientoContable['fecha'],"DMY")[1];
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($fecreacion));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($AsientoContable['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($AsientoContable['idtipo_asiento']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($AsientoContable['glosa']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($AsientoContable['idsucursal']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",$fecha);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",trim($AsientoContable['idestado']));
                        return $Ajax;
              }

              function MuestraAsientoContableExcepcion($Ajax,$Msg)
              {        return $this->RespuestaAsientoContable($Ajax,"EXCEPCION",$Msg);    
              }

              private function RespuestaAsientoContable($Ajax,$Tipo,$Data)
              {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                      $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                      return $Ajax;
              }
             
        }
?>

