<?php
         $Sufijo = '_asientocontable';

         require_once('src/modules/contabilidad/asientocontable/controlador/ControlAsientoContable.php');
         $ControlAsientoContable = new ControlAsientoContable($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlAsientoContable,'GuardaAsientoContable'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlAsientoContable,'MuestraAsientoContableByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlAsientoContable,'MuestraAsientoContableByTX'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraDetalle'.$Sufijo, $ControlAsientoContable,'MuestraDetalleAsientoContable'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlAsientoContable,'EliminaAsientoContable'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlAsientoContable,'CargaModalGridAsientoContable'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlAsientoContable,'ConsultaModalGridAsientoContableByTX'));         
         $xajax->register(XAJAX_FUNCTION,array('ControlPlantilla'.$Sufijo, $ControlAsientoContable,'ControlAsientoContable'));
         $xajax->processRequest();

?>

        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/contabilidad/asientocontable/js/JSAsientoContable.js"></script>
         </head>
         <body>
         <div class="FormContainer" style="width:965px">
            <div class="FormContainerSeccion">
             <div class="FormBasic" style="width:500px" id="FormDatosAsiento">
                <div class="FormSectionMenu"> 
                    <input type="hidden" id="NomSufijo" name="NomSufijo" value="<?php echo $Sufijo; ?>">
                    <input type="hidden" id="FilaTrabajo<?php echo $Sufijo; ?>" name="FilaTrabajo<?php echo $Sufijo; ?>" value="">
                <?php  $ControlAsientoContable->CargaAsientoContableBarButton($_GET['opcion']);
                       echo '<script type="text/javascript">';
                       echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                       echo '</script>';
                ?>
                </div>    
                <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                     <?php  $ControlAsientoContable->CargaAsientoContableMantenimiento();  ?>
                     </form>
                </div>
             </div>
            </div>
            <div class="FormContainerSeccion" style="width:450px">
              <div class="FormSectionGrid">          
              <?php  $ControlAsientoContable->CargaAsientoContableSearchGrid();  ?>
              </div>  
            </div> 
         </div>
        </body>
</html> 
              

