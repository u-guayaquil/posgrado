function CargaFecha_asientocontable(Sufijo) {
    $('#fecha'+Sufijo).datetimepicker({
            timepicker:false,
            format: 'd/m/Y'
    });
}

function ControlPlantilla_asientocontable(Sufijo, Elemento) {
    var plantilla = Elemento.value;
    document.getElementById('SeccionDetalleAsiento' + Sufijo).innerHTML = '';
    xajax_ControlPlantilla_asientocontable(plantilla);
}

function ButtonClick_asientocontable(Sufijo, Operacion)
{
    var objasientocontable = "id:estado:tipoasiento:numtipoasiento:idsucursal:fecha:glosa:tipomovimiento:glosadetalle:valordetalle:plantilla:txsucursal";
    var frmasientocontable = "estado:tipoasiento:numtipoasiento:idsucursal:fecha:glosa:tipomovimiento:glosadetalle:valordetalle:plantilla";
    var Ingasientocontable = "estado:tipoasiento:numtipoasiento:idsucursal:fecha:glosa:plantilla";

    if (Operacion == 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, frmasientocontable, "id:estado");
        BarButtonStateEnabled(Sufijo, "btsucursal:btCuenta");
        CargaFecha_asientocontable(Sufijo);
        ElementClear(Sufijo, objasientocontable);
    } else if (Operacion == 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateEnabled(Sufijo, "btsucursal:btCuenta");
        CargaFecha_asientocontable(Sufijo);
        ElementStatus(Sufijo, frmasientocontable, "id");
        if(ElementGetValue(Sufijo,"estado")==='0'){
            ElementStatus(Sufijo, "estado", "");
        }else{
            ElementStatus(Sufijo, "", "estado");
        }
    } else if (Operacion == 'addDel')
    {
        if (ElementGetValue(Sufijo, "estado") == 1)
        {
            if (confirm("Desea inactivar este registro?"))
                xajax_Elimina_asientocontable(ElementGetValue(Sufijo, "id"));
        } else
            alert("El registro se encuentra inactivo");
    } else if (Operacion == 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, Ingasientocontable))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {
                var Forma = xajax.getFormValues('FormDatosAsiento');
                clean(Sufijo);
                if (ValidaAsientoCuadrado(Sufijo)) {
                    xajax_Guarda_asientocontable(JSON.stringify({Forma}));
                } else {
                    BarButtonState(Sufijo, 'addNew');
                }
            }
        }
    } else if (Operacion == 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateDisabled(Sufijo, "btsucursal:btCuenta");
        ElementStatus(Sufijo, "id", frmasientocontable);
        ElementClear(Sufijo, objasientocontable);
        clean(Sufijo);
        document.getElementById('totalDebe' + Sufijo).innerHTML = '0.00';
        document.getElementById('totalHaber' + Sufijo).innerHTML = '0.00';
        document.getElementById('SeccionDetalleAsiento' + Sufijo).innerHTML = '<table class="SchGrdDet" id="DetSch' + Sufijo + 'DetalleAsiento" \n\
                                                        name="DetSch' + Sufijo + 'DetalleAsiento" cellpadding="0"><tbody></tbody></table>';
    } else if (Operacion == 'addImp')
    {
    } else {
        xajax_CargaModal_asientocontable(Operacion);
    }

    return false;
}

function SearchByElement_asientocontable(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    if (Elemento.name == "id" + Sufijo)
    {
        if (Elemento.value.length != 0)
        {
            ContentFlag = document.getElementById("numtipoasiento" + Sufijo);
            if (ContentFlag.value.length == 0)
            {
                if (BarButtonState(Sufijo, "Inactive")) {
                    xajax_MuestraByID_asientocontable(Elemento.value);
                    var idAsiento = document.getElementById('id' + Sufijo).value;
                    xajax_MuestraDetalle_asientocontable(idAsiento);
                }
            }
        } else
            BarButtonState(Sufijo, "Default");
    } else
    {
        xajax_MuestraByTX_asientocontable(Elemento.value);
    }
}

function SearchGetData_asientocontable(Sufijo, Grilla)
{   if (IsDisabled(Sufijo,"addSav")) 
    {
        BarButtonState(Sufijo, "Active");
        document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("idAsiento" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("numtipoasiento" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
        document.getElementById("tipoasiento" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
        document.getElementById("idsucursal" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
        document.getElementById("txsucursal" + Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
        document.getElementById("fecha" + Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue;
        document.getElementById("glosa" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
        document.getElementById("estado" + Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
        xajax_MuestraDetalle_asientocontable(Grilla.cells[0].childNodes[0].nodeValue);
    }
    return false;
}

function SearchGetData_asientocontableDetalleAsiento(Sufijo, Grilla)
{
    //if(document.getElementById('valordetalle'+Sufijo).disabled === true){
    var SufijoActual = document.getElementById('NomSufijo').value;
    document.getElementById("txCuenta" + SufijoActual).value = Grilla.cells[1].childNodes[0].nodeValue;
    document.getElementById("idCuenta" + SufijoActual).value = Grilla.cells[2].childNodes[0].value;
    document.getElementById("tipomovimiento" + SufijoActual).value = Grilla.cells[4].childNodes[0].value;
    if (Grilla.cells[4].childNodes[0].value === 'D') {
        document.getElementById("valordetalle" + SufijoActual).value = Grilla.cells[6].childNodes[0].value;
    } else {
        document.getElementById("valordetalle" + SufijoActual).value = Grilla.cells[8].childNodes[0].value;
    }
    document.getElementById("glosadetalle" + SufijoActual).value = Grilla.cells[10].childNodes[0].value;
    document.getElementById("FilaTrabajo" + SufijoActual).value = Grilla.cells[1].childNodes[0].parentNode.parentNode.id;
    //}
    return false;
}

function SearchSucursalGetData_asientocontable(Sufijo, DatosGrid)
{
    document.getElementById("idsucursal" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txsucursal" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
    cerrar();
    return false;
}

function SearchCuentaGetData_asientocontable(Sufijo, DatosGrid)
{
    document.getElementById("idCuenta" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txCuenta" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}

function XAJAXResponse_asientocontable(Sufijo, Mensaje, Datos)
{
    var objasientocontable = "descripcion:estado:tipoasiento:numtipoasiento:idsucursal:fecha:glosa:tipomovimiento:glosadetalle:valordetalle:plantilla";
    if (Mensaje === "GUARDADO")
    {
        BarButtonState(Sufijo, "Active");
        BarButtonStateDisabled(Sufijo, "btsucursal:btCuenta");
        ElementSetValue(Sufijo, "id", Datos);
        ElementSetValue(Sufijo, "idAsiento", Datos);
        ElementStatus(Sufijo, "id", objasientocontable);
        alert('Los datos se guardaron correctamente.');
    } else if (Mensaje === 'ELIMINADO')
    {
        ElementSetValue(Sufijo, "estado", Datos);
        alert("Los Datos se eliminaron correctamente");
    } else if (Mensaje === 'NOEXISTEID')
    {
        BarButtonState(Sufijo, 'Default');
        ElementClear(Sufijo, "id");
        alert(Datos);
    } else if (Mensaje === 'EXCEPCION')
    {
        BarButtonState(Sufijo, "addNew");
        alert(Datos);
    }
}

function addFilaAsiento(e, Sufijo) {
    var idVerifica = document.getElementById("id" + Sufijo);
    if (idVerifica.disabled === true) {
        var verificaTabla = RecorrerTabla(Sufijo);
        if (verificaTabla === 0) {
            agregafila(e, Sufijo);
            return false;
        } else {
            alert('La cuenta ya fue ingresada..');
            return false;
        }
    } else {
        return false;
    }
}

function agregafila(e, Sufijo) {
    e.preventDefault();
    var idcuenta = document.getElementById('idCuenta' + Sufijo).value;
    var idtipo = document.getElementById('tipomovimiento' + Sufijo).value;
    var valor = new Number(document.getElementById('valordetalle' + Sufijo).value);
    var debe = '0.00';
    var haber = '0.00';
    if (idtipo.trim() === 'D') {
        debe = valor.toFixed(2);
        haber = '0.00';
    } else {
        debe = '0.00';
        haber = valor.toFixed(2);
    }
    var glosa = document.getElementById('glosadetalle' + Sufijo).value;
    var idGeneral = Sufijo + idcuenta;
    insertaHijos(Sufijo, idGeneral, idcuenta, idtipo, valor, debe, haber, glosa);

}

function insertaHijos(Sufijo, idGeneral, idcuenta, idtipo, valor, debe, haber, glosa) {
    if (idcuenta !== '') {
        if (new Number(debe).toFixed(2) === '0.00' && new Number(haber).toFixed(2) === '0.00') {
            alert('Debe ingresar un valor al detalle...');
        } else {
            const row = createRow({
                imagen: '<input id="btn_eliAsiento' + Sufijo + '" class="borrar" \n\
                                                  src="src/modules/contabilidad/plantillacontable/img/delete.png" \n\
                                                  type="image" name="eliAsiento' + Sufijo + '" \n\
                                                  onclick="return deleteFilaAsiento();" />',
                cuenta: document.getElementById('txCuenta' + Sufijo).value,
                idcuenta: '<input type="hidden" id="detcuenta' + idGeneral + '" name="detcuenta' + idGeneral + '" value="' + idcuenta + '">',
                tipo: document.getElementById('tipomovimiento' + Sufijo).options[document.getElementById('tipomovimiento' + Sufijo).selectedIndex].text,
                idtipo: '<input type="hidden" id="dettipo' + idGeneral + '" name="dettipo' + idGeneral + '" value="' + idtipo + '">',
                debe: debe,
                Hdebe: '<input type="hidden" id="detdebe' + idGeneral + '" name="detdebe' + idGeneral + '" value="' + valor + '"/>',
                haber: haber,
                Hhaber: '<input type="hidden" id="dethaber' + idGeneral + '" name="dethaber' + idGeneral + '" value="' + valor + '"/>',
                glosa: glosa,
                Hglosa: '<input type="hidden" id="detglosa' + idGeneral + '" name="detglosa' + idGeneral + '" value="' + glosa + '"/>'

            }, Sufijo, idcuenta);
            $('#DetSch' + Sufijo + 'DetalleAsiento').append(row);
            clean(Sufijo);
            SumaValoresAsiento(Sufijo);
        }
    } else {
        alert('Debe ingresar una cuenta...');
    }
}

function createRow(data, Sufijo, idcuenta) {
    Sufijo = Sufijo + 'DetalleAsiento';
    return (
            '<tr onclick="return SearchGetData' + Sufijo + '(\'' + Sufijo + '\',this)" id="TR' + Sufijo + idcuenta + '">' +
            '<td style="width: 30px;" class="SchGrdDetCell">' + data.imagen + '</td>' +
            '<td style="width: 140px;" class="SchGrdDetCell">' + data.cuenta + '</td>' +
            '<td style="display:none;" class="SchGrdDetCell">' + data.idcuenta + '</td>' +
            '<td style="display:none;" class="SchGrdDetCell">' + data.tipo + '</td>' +
            '<td style="display:none;" class="SchGrdDetCell">' + data.idtipo + '</td>' +
            '<td style="width: 80px;" class="SchGrdDetCell" align="right">' + data.debe + '</td>' +
            '<td style="display:none;" class="SchGrdDetCell">' + data.Hdebe + '</td>' +
            '<td style="width: 80px;" class="SchGrdDetCell" align="right">' + data.haber + '</td>' +
            '<td style="display:none;" class="SchGrdDetCell">' + data.Hhaber + '</td>' +
            '<td style="width: 170px;" class="SchGrdDetCell" align="center">' + data.glosa + '</td>' +
            '<td style="display:none;" class="SchGrdDetCell">' + data.Hglosa + '</td>' +
            '</tr>'
            );
}

function clean(Sufijo) {
    $('#txCuenta' + Sufijo).val('');
    $('#idCuenta' + Sufijo).val('');
    $('#valordetalle' + Sufijo).val('');
    $('#detglosa' + Sufijo).val('');
    $('#tipomovimiento' + Sufijo).val('D');
    $('#FilaTrabajo' + Sufijo).val('');
    return false;
}

function deleteFilaAsiento() {
    $(document).on('click', '.borrar', function (event) {
        event.preventDefault();
        $(this).closest('tr').remove();
    });
}

function RecorrerTabla(Sufijo) {
    var idcuenta = document.getElementById('idCuenta' + Sufijo).value;
    var bandera = 0;
    $('#DetSch' + Sufijo + 'DetalleAsiento tr').each(function () {
        var idCelda = document.getElementById('FilaTrabajo' + Sufijo).value;
        var celdaActual = this.id;
        if (idCelda !== celdaActual) {
            bandera = ComparaCeldas(this, idcuenta, bandera);
        } else {
            var filaTrabajo = $('#FilaTrabajo' + Sufijo).val();
            if (filaTrabajo !== '') {
                this.remove();
                $('#FilaTrabajo' + Sufijo).val('');
            } else {
                bandera = ComparaCeldas(this, idcuenta, bandera);
            }
            return false;
        }
    });
    SumaValoresAsiento(Sufijo);
    return bandera;
}

function ComparaCeldas(element, idcuenta, bandera) {
    var cuenta = $(element).find("td").eq(2).children();
    var cuentaComp = cuenta[0].value;
    if (cuentaComp === idcuenta) {
        bandera = 1;
    }
    return bandera;
}

function ValidaAsientoCuadrado(Sufijo) {
    var contG = 0;
    var contDebe = 0;
    var contHaber = 0;
    var Debe = new Number(0);
    var Haber = new Number(0);
    $('#DetSch' + Sufijo + 'DetalleAsiento tr').each(function () {
        contG++;
        var tipo = $(this).find("td").eq(3).html();
        switch (tipo.trim()) {
            case 'DEBE':
                contDebe++;
                Debe += new Number($(this).find("td").eq(5).html());
                break;
            case 'HABER':
                contHaber++;
                Haber += new Number($(this).find("td").eq(7).html());
                break;
        }
    });
    if (contG == 0) {
        alert('Debe ingresar detalles para el Asiento');
        return false;
    } else {
        if (contDebe > 0 && contHaber > 0) {
            if (Debe === Haber) {
                return true;
            } else {
                alert('Los valores del Asiento no estan cuadrados');
                return false;
            }
        } else {
            alert('El detalle del asiento debe tener un DEBE y un HABER como minimo');
            return false;
        }

    }
}

function SumaValoresAsiento(Sufijo) {
    var Debe = new Number(0);
    var Haber = new Number(0);
    $('#DetSch' + Sufijo + 'DetalleAsiento tr').each(function () {
        var tipo = $(this).find("td").eq(3).html();
        switch (tipo.trim()) {
            case 'DEBE':
                Debe += new Number($(this).find("td").eq(5).html());
                break;
            case 'HABER':
                Haber += new Number($(this).find("td").eq(7).html());
                break;
        }
    });
    document.getElementById('totalDebe' + Sufijo).innerHTML = Debe.toFixed(2);
    document.getElementById('totalHaber' + Sufijo).innerHTML = Haber.toFixed(2);
}

                  