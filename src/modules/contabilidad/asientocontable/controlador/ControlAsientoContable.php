<?php
         require_once("src/rules/contabilidad/servicio/ServicioAsientoContable.php");  
         require_once("src/rules/contabilidad/servicio/ServicioPlantillaContable.php"); 
         require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");     
         require_once("src/rules/general/servicio/ServicioSucursal.php");          
         require_once("src/modules/contabilidad/asientocontable/render/RenderAsientoContable.php");
         require_once("src/modules/general/sucursal/render/RenderSucursal.php");        
         require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlAsientoContable
         {     private  $Sufijo;
               private  $ServicioAsientoContable;
               private  $ServicioPlantillaContable;
               private  $ServicioPlanCuentas;
               private  $ServicioSucursal;
               private  $RenderAsientoContable;
               private  $RenderPlanCuentas;
               private  $RenderSucursal;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioAsientoContable = new ServicioAsientoContable();
                        $this->ServicioPlantillaContable = new ServicioPlantillaContable();
                        $this->RenderAsientoContable = new RenderAsientoContable($Sufijo);
                        $this->ServicioSucursal = new ServicioSucursal();
                        $this->RenderSucursal = new RenderSucursal($Sufijo);
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
               }

               function CargaAsientoContableBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaAsientoContableMantenimiento()
               {        echo $this->RenderAsientoContable->CreaAsientoContableMantenimiento();
               }

               function CargaAsientoContableSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoAsientoContable = $this->ServicioAsientoContable->BuscarAsientoContable($prepareDQL);
                        echo $this->RenderAsientoContable->CreaAsientoContableSearchGrid($datoAsientoContable); 
               } 
               
               function ControlAsientoContable($plantilla)
               {        $ajaxRespon = new xajaxResponse(); 
                        $datosPlantilla = $this->ServicioPlantillaContable->BuscarDetallePlantillaContable(array('id' => intval($plantilla)));
                        return $this->RenderAsientoContable->MuestraDetalleAsientoContable($ajaxRespon,$datosPlantilla);
               }
               
               function MuestraAsientoContableByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoAsientoContable = $this->ServicioAsientoContable->BuscarAsientoContable($prepareDQL);
                        return $this->RenderAsientoContable->MuestraAsientoContable($ajaxRespon,$datoAsientoContable);
               }

               function MuestraAsientoContableByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoAsientoContable = $this->ServicioAsientoContable->BuscarAsientoContable($prepareDQL);
                        return $this->RenderAsientoContable->MuestraAsientoContableGrid($ajaxRespon,$datoAsientoContable);
               }
               
               function MuestraDetalleAsientoContable($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('id' => intval($id));
                        $datoAsientoContable = $this->ServicioAsientoContable->BuscarDetalleAsientoContable($prepareDQL);
                        return $this->RenderAsientoContable->MuestraDetalleAsientoContable($ajaxRespon,$datoAsientoContable);
               }    

               function GuardaAsientoContable($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $AsientoContable  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioAsientoContable->GuardaDBAsientoContable($AsientoContable);
                        if (is_numeric($id)){
                            $function = (empty($AsientoContable->idAsiento_asientocontable) ? "MuestraAsientoContableGuardado" : "MuestraAsientoContableEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioAsientoContable->BuscarAsientoContable($prepareDQL);
                            return $this->RenderAsientoContable->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderAsientoContable->MuestraAsientoContableExcepcion($ajaxRespon,$id);
                        }
               } 
               
               function EliminaAsientoContable($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioAsientoContable->DesactivaAsientoContable(intval($id));
                        $Datos = $this->ServicioAsientoContable->BuscarAsientoContable($prepareDQL);
                        return $this->RenderAsientoContable->MuestraAsientoContableEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridAsientoContable($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        
                        if ($Operacion==="btsucursal")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                            $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);                            
                            $Nombre="Sucursal";
                        }else{
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '');
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);                           
                            $Nombre="Cuentas";
                        }   
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca ".$Nombre;
                        if ($Operacion==="btsucursal")    
                        {   $jsonModal['Carga'] = $this->RenderSucursal->CreaModalGridSucursal($Operacion,$Datos);
                        }else{
                            $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                        }
                        
                        $jsonModal['Ancho'] = "603";
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalAsientoContableGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);                        
                        if ($Operacion==="btsucursal")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                            $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                            return $this->RenderSucursal->MuestraModalGridSucursal($ajaxResp,$Operacion,$Datos);                                        
                        }else{
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            return $this->RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);     
                        }    
                }
         }

?>

