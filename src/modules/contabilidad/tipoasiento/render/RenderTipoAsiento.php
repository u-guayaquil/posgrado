<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderTipoAsiento
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
                            
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo; 
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 2;                       
                }
              
                function CreaOpcionBarButton($Buttons)
                {       $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
                }
              
                function CreaTipoAsientoMantenimiento()
                {                          
                        $TipoAsiento = '<table class="Form-Frame" cellpadding="0" border="0">';
                        $TipoAsiento.= '       <tr height="30">';
                        $TipoAsiento.= '           <td class="Form-Label" style="width:20%">Id</td>';
                        $TipoAsiento.= '           <td class="Form-Label" style="width:80%">';
                        $TipoAsiento.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $TipoAsiento.= '           </td>';
                        $TipoAsiento.= '         </tr>';
                        $TipoAsiento.= '         <tr height="30">';
                        $TipoAsiento.= '             <td class="Form-Label">C&oacute;digo</td>';
                        $TipoAsiento.= '             <td class="Form-Label">';
                        $TipoAsiento.= '                 <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                        $TipoAsiento.= '             </td>';
                        $TipoAsiento.= '         </tr>';
                        $TipoAsiento.= '         <tr height="30">';
                        $TipoAsiento.= '             <td class="Form-Label">Descripci&oacute;n</td>';
                        $TipoAsiento.= '             <td class="Form-Label">';
                        $TipoAsiento.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="30" disabled/>';
                        $TipoAsiento.= '             </td>';
                        $TipoAsiento.= '         </tr>';
                        $TipoAsiento.= '         </tr>';                       
                        $TipoAsiento.= '         <tr height="30">';
                        $TipoAsiento.= '             <td class="Form-Label">Estado</td>';
                        $TipoAsiento.= '             <td class="Form-Label">';
                        $TipoAsiento.=               $this->CreaComboEstado();
                        $TipoAsiento.= '             </td>';
                        $TipoAsiento.= '         </tr>';
                        $TipoAsiento.= '</table>';
                        return $TipoAsiento;
                }
              
                private function SearchGridValues()
                {       $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('80px','left','');
                        $Columns['Descripci&oacute;n'] = array('150px','left','');
                        $Columns['Creacion'] = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
                }

                function CreaTipoAsientoSearchGrid($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->TipoAsientoGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }
              
                private function TipoAsientoGridHTML($ObjSchGrid,$Datos)
                {       foreach ($Datos as $tipo)
                        {        
                                $fecreacion = $this->Fechas->changeFormatDate($tipo['fecreacion'],"DMY");
                                $id = str_pad($tipo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($tipo['codigo']));                               
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($tipo['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$tipo['txtestado']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$tipo['idestado']); 
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($tipo['idestado']==0 ? "red" : ""));
                        }
                        return $ObjSchGrid->CreaSearchTableDetalle(); 
                }

                private function CreaComboEstado()
                {       $Select = new ComboBox($this->Sufijo);                        
                        $ServicioEstado = new ServicioEstado();
                        $datoEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));                       
                        return $Select->Combo("estado",$datoEstado)->Selected(1)->Create("s03");
                }
              
                function MuestraTipoAsiento($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Dato['codigo'])); 
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaTipoAsiento($Ajax,"NOEXISTEID","No existe un Tipo de Asiento con ese ID.");
                }

                function MuestraTipoAsientoGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->TipoAsientoGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }        

                function MuestraTipoAsientoGuardado($Ajax,$TipoAsiento)
                {       foreach ($TipoAsiento as $dato) 
                        {   $id = str_pad($dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraTipoAsientoGrid($Ajax,$TipoAsiento);
                            return $this->RespuestaTipoAsiento($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaTipoAsiento($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraTipoAsientoEditado($Ajax,$TipoAsiento)
                {       foreach ($TipoAsiento as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoAsientoRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoAsiento($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaTipoAsiento($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
              
                function MuestraTipoAsientoEliminado($Ajax,$TipoAsiento)
                {       foreach ($TipoAsiento as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoAsientoRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoAsiento($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->RespuestaTipoAsiento($xAjax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraTipoAsientoRowGrid($Ajax,$TipoAsiento,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($TipoAsiento['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($TipoAsiento['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($TipoAsiento['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($TipoAsiento['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($TipoAsiento['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($TipoAsiento['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($TipoAsiento['idestado'])); 
                        return $Ajax;
                }

                function MuestraTipoAsientoExcepcion($Ajax,$Msg)
                {       return $this->RespuestaTipoAsiento($Ajax,"EXCEPCION",$Msg);    
                }

                private function RespuestaTipoAsiento($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

