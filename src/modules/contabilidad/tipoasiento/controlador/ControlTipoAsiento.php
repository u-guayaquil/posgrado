<?php
        require_once("src/rules/contabilidad/servicio/ServicioTipoAsiento.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
        require_once("src/modules/contabilidad/tipoasiento/render/RenderTipoAsiento.php");

        class ControlTipoAsiento
        {       private  $Sufijo;
                private  $ServicioTipoAsiento;
                private  $RenderTipoAsiento;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->ServicioTipoAsiento = new ServicioTipoAsiento();
                        $this->RenderTipoAsiento = new RenderTipoAsiento($Sufijo);
                }

                function CargaTipoAsientoBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaTipoAsientoMantenimiento()
                {       echo $this->RenderTipoAsiento->CreaTipoAsientoMantenimiento();
                }

                function CargaTipoAsientoSearchGrid()
                {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoTipoAsiento = $this->ServicioTipoAsiento->BuscarTipoAsiento($prepareDQL);
                        echo $this->RenderTipoAsiento->CreaTipoAsientoSearchGrid($datoTipoAsiento); 
                }         
               
                function MuestraTipoAsientoByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoTipoAsiento = $this->ServicioTipoAsiento->BuscarTipoAsiento($prepareDQL);
                        return $this->RenderTipoAsiento->MuestraTipoAsiento($ajaxRespon,$datoTipoAsiento);
                }

                function MuestraTipoAsientoByTX($texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoTipoAsiento = $this->ServicioTipoAsiento->BuscarTipoAsiento($prepareDQL);
                        return $this->RenderTipoAsiento->MuestraTipoAsientoGrid($ajaxRespon,$datoTipoAsiento);
                }

                function GuardaTipoAsiento($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $TipoAsiento  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioTipoAsiento->GuardaDBTipoAsiento($TipoAsiento);
                        if (is_numeric($id)){
                            $function = (empty($TipoAsiento->id) ? "MuestraTipoAsientoGuardado" : "MuestraTipoAsientoEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioTipoAsiento->BuscarTipoAsiento($prepareDQL);
                            return $this->RenderTipoAsiento->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderTipoAsiento->MuestraTipoAsientoExcepcion($ajaxRespon,$id);
                        }
                } 
               
                function EliminaTipoAsiento($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioTipoAsiento->DesactivaTipoAsiento(intval($id));
                        $Datos = $this->ServicioTipoAsiento->BuscarTipoAsiento($prepareDQL);
                        return $this->RenderTipoAsiento->MuestraTipoAsientoEliminado($ajaxRespon,$Datos);
                }
               
         }

?>

