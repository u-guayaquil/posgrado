<?php
         require_once("src/rules/contabilidad/servicio/ServicioPlantillaContable.php");         
         require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/contabilidad/plantillacontable/render/RenderPlantillaContable.php");         
         require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlPlantillaContable
         {     private  $Sufijo;
               private  $ServicioPlantillaContable;
               private  $ServicioPlanCuentas;
               private  $RenderPlantillaContable;
               private  $RenderPlanCuentas;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioPlantillaContable = new ServicioPlantillaContable($this->Sufijo);
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        $this->RenderPlantillaContable = new RenderPlantillaContable($Sufijo);
                        $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
               }

               function CargaPlantillaContableBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaPlantillaContableMantenimiento()
               {        echo $this->RenderPlantillaContable->CreaPlantillaContableMantenimiento();
               }

               function CargaPlantillaContableSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoPlantillaContable = $this->ServicioPlantillaContable->BuscarPlantillaContable($prepareDQL);
                        echo $this->RenderPlantillaContable->CreaPlantillaContableSearchGrid($datoPlantillaContable); 
               }                    
               
               function ControlDetallePlantillaContable($item)
               {        $ajaxRespon = new xajaxResponse(); 
                        $tablaDetalle = $this->RenderPlantillaContable->CreaDetallePlantilla($item);
                        $nombre = "SeccionDetallePlantilla".$this->SufijoPantalla;
                        $ajaxRespon->Assign($nombre,"innerHTML",$tablaDetalle);
                        return $ajaxRespon;
               }
               
               function MuestraPlantillaContableByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoPlantillaContable = $this->ServicioPlantillaContable->BuscarPlantillaContable($prepareDQL);
                        return $this->RenderPlantillaContable->MuestraPlantillaContable($ajaxRespon,$datoPlantillaContable);
               }

               function MuestraPlantillaContableByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoPlantillaContable = $this->ServicioPlantillaContable->BuscarPlantillaContable($prepareDQL);
                        return $this->RenderPlantillaContable->MuestraPlantillaContableGrid($ajaxRespon,$datoPlantillaContable);
               }
               
               function MuestraDetallePlantillaContable($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('id' => intval($id));
                        $datoPlantillaContable = $this->ServicioPlantillaContable->BuscarDetallePlantillaContable($prepareDQL);
                        return $this->RenderPlantillaContable->MuestraDetallePlantillaContable($ajaxRespon,$datoPlantillaContable);
               }               

               function GuardaPlantillaContable($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $PlantillaContable  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioPlantillaContable->GuardaDBPlantillaContable($PlantillaContable);
                        if (is_numeric($id)){
                            $function = (empty($PlantillaContable->idPlantilla_plantillacontable) ? "MuestraPlantillaContableGuardado" : "MuestraPlantillaContableEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioPlantillaContable->BuscarPlantillaContable($prepareDQL);
                            return $this->RenderPlantillaContable->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderPlantillaContable->MuestraPlantillaContableExcepcion($ajaxRespon,$id);
                        }
               } 
               
               function EliminaPlantillaContable($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioPlantillaContable->DesactivaPlantillaContable(intval($id));
                        $Datos = $this->ServicioPlantillaContable->BuscarPlantillaContable($prepareDQL);
                        return $this->RenderPlantillaContable->MuestraPlantillaContableEliminada($ajaxRespon,$Datos);
                }
                
                function CargaModalGridPlantillaContable($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Cuentas";
                        $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                        $jsonModal['Ancho'] = "517";                           
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                                
                function ConsultaModalGridPlantillaContableByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        return $this->RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);                                                    
                }
               
         }

?>

