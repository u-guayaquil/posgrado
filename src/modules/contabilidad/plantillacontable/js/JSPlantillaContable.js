function ButtonClick_plantillacontable(Sufijo, Operacion)
{
    var objplantillacontable = "id:descripcion:estado:tipoasiento:glosa:tipomovimiento";
    var frmplantillacontable = "descripcion:estado:tipoasiento:glosa:tipomovimiento";

    if (Operacion == 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, frmplantillacontable, "id");
        BarButtonStateEnabled(Sufijo, "btCuenta");
        document.getElementById('DetSch' + Sufijo + 'DetallePlantilla').innerHTML = '';
        ElementClear(Sufijo, objplantillacontable);
    } else if (Operacion == 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateEnabled(Sufijo, "btCuenta");
        ElementStatus(Sufijo, frmplantillacontable, "id");
        if(ElementGetValue(Sufijo,"estado")==='0'){
            ElementStatus(Sufijo, "estado", "");
        }else{
            ElementStatus(Sufijo, "", "estado");
        }
    } else if (Operacion == 'addDel')
    {
        if (ElementGetValue(Sufijo, "estado") == 1)
        {
            if (confirm("Desea inactivar este registro?"))
                xajax_Elimina_plantillacontable(ElementGetValue(Sufijo, "id"));
        } else
            alert("El registro se encuentra inactivo");
    } else if (Operacion == 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, frmplantillacontable))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {
                var Forma = xajax.getFormValues('FormDatosPlantilla');
                if (ValidaAsientoCuadrado(Sufijo)) {
                    xajax_Guarda_plantillacontable(JSON.stringify({Forma}));
                } else {
                    BarButtonState(Sufijo, 'addNew');
                }

            }
        }
    } else if (Operacion == 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateDisabled(Sufijo, "btCuenta");
        ElementStatus(Sufijo, "id", frmplantillacontable);
        ElementClear(Sufijo, objplantillacontable);
        document.getElementById('DetSch' + Sufijo + 'DetallePlantilla').innerHTML = '';
    } else if (Operacion == 'addImp')
    {
    } else {
        xajax_CargaModal_plantillacontable(Operacion);
    }

    return false;
}

function SearchByElement_plantillacontable(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    if (Elemento.name == "id" + Sufijo)
    {
        if (Elemento.value.length != 0)
        {
            ContentFlag = document.getElementById("descripcion" + Sufijo);
            if (ContentFlag.value.length == 0)
            {
                if (BarButtonState(Sufijo, "Inactive")) {
                    xajax_MuestraByID_plantillacontable(Elemento.value);
                    var idPlantilla = document.getElementById('id' + Sufijo).value;
                    xajax_MuestraDetalle_plantillacontable(idPlantilla);
                }
            }
        } else
            BarButtonState(Sufijo, "Default");
    } else
    {
        xajax_MuestraByTX_plantillacontable(Elemento.value);
    }

}

function SearchCuentaGetData_plantillacontable(Sufijo, DatosGrid)
{
    document.getElementById("idCuenta" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txCuenta" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}

function SearchGetData_plantillacontable(Sufijo, Grilla)
{   if (IsDisabled(Sufijo,"addSav")) 
    {
        BarButtonState(Sufijo, "Active");

        document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("idPlantilla" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("descripcion" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
        document.getElementById("tipoasiento" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
        document.getElementById("glosa" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
        document.getElementById("estado" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
        xajax_MuestraDetalle_plantillacontable(Grilla.cells[0].childNodes[0].nodeValue);
    }
    return false;
}

function SearchGetData_plantillacontableDetallePlantilla(Sufijo, Grilla)
{
    var SufijoActual = document.getElementById('NomSufijo').value;
    document.getElementById('NomSufijo').value;
    document.getElementById("txCuenta" + SufijoActual).value = Grilla.cells[1].childNodes[0].nodeValue;
    document.getElementById("idCuenta" + SufijoActual).value = Grilla.cells[2].childNodes[0].value;
    document.getElementById("tipomovimiento" + SufijoActual).value = Grilla.cells[4].childNodes[0].value;
    document.getElementById("FilaTrabajo" + SufijoActual).value = Grilla.cells[1].childNodes[0].parentNode.parentNode.id;
    return false;
}

function XAJAXResponse_plantillacontable(Sufijo, Mensaje, Datos)
{
    var objplantillacontable = "descripcion:estado:tipoasiento:glosa:tipomovimiento";
    if (Mensaje === "GUARDADO")
    {
        BarButtonState(Sufijo, "Active");
        BarButtonStateDisabled(Sufijo, "btpadre");
        ElementSetValue(Sufijo, "id", Datos);
        ElementSetValue(Sufijo, "idPlantilla", Datos);
        ElementStatus(Sufijo, "id", objplantillacontable);
        alert('Los datos se guardaron correctamente.');
    } else if (Mensaje === 'ELIMINADO')
    {
        ElementSetValue(Sufijo, "estado", Datos);
        alert("Los Datos se eliminaron correctamente");
    } else if (Mensaje === 'NOEXISTEID')
    {
        BarButtonState(Sufijo, 'Default');
        ElementClear(Sufijo, "id");
        alert(Datos);
    } else if (Mensaje === 'EXCEPCION')
    {
        BarButtonState(Sufijo, "addNew");
        alert(Datos);
    }
}

function addFilaPlantilla(e, Sufijo) {
    var idVerifica = document.getElementById("id" + Sufijo);
    if (idVerifica.disabled === true) {
        var verificaTabla = RecorrerTabla(Sufijo);
        if (verificaTabla === 0) {
            agregafila(e, Sufijo);
            return false;
        } else {
            alert('La cuenta ya fue ingresada..');
            return false;
        }
    } else {
        return false;
    }
}

function agregafila(e, Sufijo) {
    e.preventDefault();
    var idcuenta = document.getElementById('idCuenta' + Sufijo).value;
    var idtipo = document.getElementById('tipomovimiento' + Sufijo).value;
    var idGeneral = Sufijo + idcuenta;
    if (idcuenta !== '') {
        const row = createRow({
            imagen: '<input id="btn_eliPlantilla' + Sufijo + '" class="borrar" \n\
                                                  src="src/modules/contabilidad/plantillacontable/img/delete.png" \n\
                                                  type="image" name="eliPlantilla' + Sufijo + '" \n\
                                                  onclick="return deleteFilaPlantilla();" />',
            cuenta: document.getElementById('txCuenta' + Sufijo).value,
            idcuenta: '<input type="hidden" id="idCuentaContable' + idGeneral + '" name="idCuentaContable' + idGeneral + '" value="' + idcuenta + '">',
            tipo: document.getElementById('tipomovimiento' + Sufijo).options[document.getElementById('tipomovimiento' + Sufijo).selectedIndex].text,
            idtipo: '<input type="hidden" id="idtipomovimiento' + idGeneral + '" name="idtipomovimiento' + idGeneral + '" value="' + idtipo + '">'
        }, Sufijo, idcuenta);
        $('#DetSch' + Sufijo + 'DetallePlantilla').append(row);
        clean(Sufijo);
    } else {
        alert('Debe ingresar una cuenta...');
    }
}

function createRow(data, Sufijo, idcuenta) {
    Sufijo = Sufijo + 'DetallePlantilla';
    return (
            '<tr onclick="return SearchGetData' + Sufijo + '(\'' + Sufijo + '\',this)" id="TR' + Sufijo + idcuenta + '">' +
            '<td style="width: 25px;" class="SchGrdDetCell">' + data.imagen + '</td>' +
            '<td style="width: 250px;" class="SchGrdDetCell">' + data.cuenta + '</td>' +
            '<td style="display:none;" class="SchGrdDetCell">' + data.idcuenta + '</td>' +
            '<td style="width: 150px;" class="SchGrdDetCell">' + data.tipo + '</td>' +
            '<td style="display:none;" class="SchGrdDetCell">' + data.idtipo + '</td>' +
            '</tr>'
            );
}

function clean(Sufijo) {
    $('#txCuenta' + Sufijo).val('');
    $('#idCuenta' + Sufijo).val('');
    $('#tipomovimiento' + Sufijo).val('D');
    $('#FilaTrabajo' + Sufijo).val('');
    return false;
}

function deleteFilaPlantilla() {
    $(document).on('click', '.borrar', function (event) {
        event.preventDefault();
        $(this).closest('tr').remove();
    });
}

function RecorrerTabla(Sufijo) {
    var idcuenta = document.getElementById('idCuenta' + Sufijo).value;
    var bandera = 0;
    $('#DetSch' + Sufijo + 'DetallePlantilla tr').each(function () {
        var idCelda = document.getElementById('FilaTrabajo' + Sufijo).value;
        var celdaActual = this.id;
        if (idCelda !== celdaActual) {
            bandera = ComparaCeldas(this, idcuenta, bandera);
        } else {
            var filaTrabajo = $('#FilaTrabajo' + Sufijo).val();
            if (filaTrabajo !== '') {
                this.remove();
                $('#FilaTrabajo' + Sufijo).val('');
            } else {
                bandera = ComparaCeldas(this, idcuenta, bandera);
            }
            return false;
        }
    });
    return bandera;
}

function ComparaCeldas(element, idcuenta, bandera) {
    var cuenta = $(element).find("td").eq(2).children();
    var cuentaComp = cuenta[0].value;
    if (cuentaComp === idcuenta) {
        bandera = 1;
    }
    return bandera;
}

function ValidaAsientoCuadrado(Sufijo) {
    var contG = 0;
    var contDebe = 0;
    var contHaber = 0;
    $('#DetSch' + Sufijo + 'DetallePlantilla tr').each(function () {
        contG++;
        var tipo = $(this).find("td").eq(3).html();
        switch (tipo.trim()) {
            case 'DEBE':
                contDebe++;
                break;
            case 'HABER':
                contHaber++;
                break;
        }
    });
    if (contG === 0) {
        alert('Debe ingresar detalles a la plantilla');
        return false;
    } else {
        if (contDebe > 0 && contHaber > 0) {
            return true;
        } else {
            alert('El detalle de la plantilla esta descuadrado');
            return false;
        }
    }
}