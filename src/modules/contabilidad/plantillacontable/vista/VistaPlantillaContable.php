<?php
         $Sufijo = '_plantillacontable';

         require_once('src/modules/contabilidad/plantillacontable/controlador/ControlPlantillaContable.php');
         $ControlPlantillaContable = new ControlPlantillaContable($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlPlantillaContable,'GuardaPlantillaContable'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlPlantillaContable,'MuestraPlantillaContableByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlPlantillaContable,'MuestraPlantillaContableByTX'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraDetalle'.$Sufijo, $ControlPlantillaContable,'MuestraDetallePlantillaContable'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlPlantillaContable,'EliminaPlantillaContable'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlPlantillaContable,'CargaModalGridPlantillaContable'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlPlantillaContable,'ConsultaModalGridPlantillaContableByTX'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/contabilidad/plantillacontable/js/JSPlantillaContable.js"></script>
         </head>
         <body>
         <div class="FormContainer" style="width:965px">
            <div class="FormContainerSeccion">
             <div class="FormBasic" style="width:430px" id="FormDatosPlantilla">
                <div class="FormSectionMenu"> 
                    <input type="hidden" id="NomSufijo" name="NomSufijo" value="<?php echo $Sufijo; ?>">
                    <input type="hidden" id="FilaTrabajo<?php echo $Sufijo; ?>" name="FilaTrabajo<?php echo $Sufijo; ?>" value="">
                <?php  $ControlPlantillaContable->CargaPlantillaContableBarButton($_GET['opcion']);
                       echo '<script type="text/javascript">';
                       echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                       echo '</script>';
                ?>
                </div>    
                <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                     <?php  $ControlPlantillaContable->CargaPlantillaContableMantenimiento();  ?>
                     </form>
                </div>
             </div>
            </div>
            <div class="FormContainerSeccion" style="width:500px">
              <div class="FormSectionGrid">          
              <?php  $ControlPlantillaContable->CargaPlantillaContableSearchGrid();  ?>
              </div>  
            </div> 
         </div>
        </body>
</html> 
              

