<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/contabilidad/servicio/ServicioTipoAsiento.php");
        require_once("src/rules/general/entidad/Estado.php"); 
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/DateControl.php");
         
        class RenderPlantillaContable
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
               
              function CreaPlantillaContableMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);
                       $PlantillaContable = '<table class="Form-Frame" cellpadding="0">';
                       $PlantillaContable.= '       <tr height="30">';
                       $PlantillaContable.= '            <td class="Form-Label" style="width:25%">Id</td>';
                       $PlantillaContable.= '             <td class="Form-Label" style="width:30%">';
                       $PlantillaContable.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:glosa:tipoasiento\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $PlantillaContable.= '                 <input type="hidden" id="idPlantilla'.$this->Sufijo.'" name="idPlantilla'.$this->Sufijo.'" value="">';
                       $PlantillaContable.= '             </td>';
                       $PlantillaContable.= '             <td class="Form-Label" style="width:25%">Tipo</td>';
                       $PlantillaContable.= '             <td class="Form-Label" style="width:25%">';
                                                    $PlantillaContable.= $this->CreaComboTipoAsiento();
                       $PlantillaContable.= '             </td>';
                       $PlantillaContable.= '         </tr>';
                       $PlantillaContable.= '         <tr height="30">';
                       $PlantillaContable.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $PlantillaContable.= '             <td class="Form-Label" style="width:75%" colspan="3">';
                       $PlantillaContable.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $PlantillaContable.= '             </td>';
                       $PlantillaContable.= '         </tr>';
                       $PlantillaContable.= '         <tr height="30">';
                       $PlantillaContable.= '             <td class="Form-Label" style="width:25%">Glosa</td>';
                       $PlantillaContable.= '             <td class="Form-Label" style="width:75%" colspan="3">';
                       $PlantillaContable.= '                 <textarea id="glosa'.$this->Sufijo.'" name="glosa'.$this->Sufijo.'" rows="2" cols="30" disabled></textarea>';
                       $PlantillaContable.= '             </td>';
                       $PlantillaContable.= '         </tr>';                     
                       $PlantillaContable.= '         <tr height="30">';
                       $PlantillaContable.= '             <td class="Form-Label">Estado</td>';
                       $PlantillaContable.= '             <td class="Form-Label" colspan="2">';
                                                    $PlantillaContable.= $this->CreaComboEstado();
                       $PlantillaContable.= '             </td>';
                       $PlantillaContable.= '         </tr>';
                       $PlantillaContable.= '         <tr height="30">';
                       $PlantillaContable.= '             <td class="Form-Label" style="width:100%" colspan="4">';
                       $PlantillaContable.= '                 <div id="SeccionCabIngresoDetallePlantilla'.$this->Sufijo.'" class="SchCab"> ';
                       $PlantillaContable.= '                   <table class="SchGrdCab" id="CabIngresoDetallePlantilla'.$this->Sufijo.'" name="CabIngresoDetallePlantilla'.$this->Sufijo.'" cellpadding="0">';
                       $PlantillaContable.= '                       <tbody>';                       
                       $PlantillaContable.= '                       <tr class="SchGrdCabCell">';
                       $PlantillaContable.= '                          <td class="SchGrdCabCell" style="width: 230px;">Cuenta</td>';
                       $PlantillaContable.= '                          <td class="SchGrdCabCell" style="width: 120px;">Tipo</td>';
                       $PlantillaContable.= '                       </tr>';                       
                       $PlantillaContable.= '                       </tbody>';
                       $PlantillaContable.= '                   </table>';
                       $PlantillaContable.= '                 </div>';
                       $PlantillaContable.= '                   <table class="SchGrdCab" id="CabIngresoDetallePlantilla'.$this->Sufijo.'" name="CabIngresoDetallePlantilla'.$this->Sufijo.'" cellpadding="0">';
                       $PlantillaContable.= '                       <tbody>';
                       $PlantillaContable.= '                       <tr class="Form-Label">';
                       $PlantillaContable.= '                          <td class="Form-Label" style="width: 230px; height:50px;" align="center">';
                       $PlantillaContable.=                                 $Search->TextSch("Cuenta","","")->Enabled(false)->Create("t07");
                       $PlantillaContable.= '                          </td>';
                       $PlantillaContable.= '                          <td class="Form-Label" style="width: 120px;" align="center">';
                       $PlantillaContable.= '                               <select id="tipomovimiento'.$this->Sufijo.'" name="tipomovimiento'.$this->Sufijo.'" class="sel-input s04" disabled>';
                       $PlantillaContable.= '                                   <option value="D">DEBE</option>';
                       $PlantillaContable.= '                                   <option value="H">HABER</option>';
                       $PlantillaContable.= '                               </select>';
                       $PlantillaContable.= '                          </td>';
                       $PlantillaContable.= '                          <td class="Form-Label" style="width: 20px; height:50px;" align="center">';
                       $PlantillaContable.= '                <input id="btn_addPlantilla'.$this->Sufijo.'" src="src/modules/contabilidad/plantillacontable/img/add.png" '
                                                            . ' type="image" name="addPlantilla'.$this->Sufijo.'" onclick="return addFilaPlantilla(event,\''.$this->Sufijo.'\');" />';
                       $PlantillaContable.= '                          </td>';
                       $PlantillaContable.= '                          <td class="Form-Label" style="width: 10px; height:50px;" align="center">';
                       $PlantillaContable.= '                             <input id="btn_borraDatos'.$this->Sufijo.'" src="src/modules/nomina/plantillarol/img/clean.png" '
                                                                         . ' type="image" name="borraDatos'.$this->Sufijo.'" style="width: 30px; height:25px;" '
                                                                         . 'onclick="return clean(\''.$this->Sufijo.'\');" />';
                       $PlantillaContable.= '                          </td>';
                       $PlantillaContable.= '                       </tr>';
                       $PlantillaContable.= '                       </tbody>';
                       $PlantillaContable.= '                   </table>';
                       $PlantillaContable.= '                 <div id="SeccionCabDetallePlantilla'.$this->Sufijo.'" class="SchCab"> ';
                       $PlantillaContable.= '                   <table class="SchGrdCab" id="CabDetallePlantilla'.$this->Sufijo.'" name="CabDetallePlantilla'.$this->Sufijo.'" cellpadding="0">';
                       $PlantillaContable.= '                       <tbody>';
                       $PlantillaContable.= '                       <tr class="SchGrdCabCell">';
                       $PlantillaContable.= '                          <td class="SchGrdCabCell" style="width: 30px;">&nbsp;&nbsp;&nbsp;</td>';
                       $PlantillaContable.= '                          <td class="SchGrdCabCell" style="width: 250px;">Cuenta</td>';
		       $PlantillaContable.= '                          <td class="SchGrdCabCell" style="width: 150px;">Tipo</td>';
                       $PlantillaContable.= '                       </tr>';
                       $PlantillaContable.= '                       </tbody>';
                       $PlantillaContable.= '                   </table>';
                       $PlantillaContable.= '                 </div>';
                       $PlantillaContable.= '                 <div id="SeccionDetallePlantilla'.$this->Sufijo.'" class="SchDet"> ';
                       $PlantillaContable.= '                   <table class="SchGrdDet" id="DetSch'.$this->Sufijo.'DetallePlantilla" name="DetSch'.$this->Sufijo.'DetallePlantilla" cellpadding="0">';
                       $PlantillaContable.= '                       <tbody>';
                       $PlantillaContable.= '                       </tbody>';
                       $PlantillaContable.= '                   </table>';
                       $PlantillaContable.= '                 </div>';
                       $PlantillaContable.= '             </td>';
                       $PlantillaContable.= '         </tr>';
                       $PlantillaContable.= '</table>';
                       return $PlantillaContable;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Descripci&oacute;n'] = array('150px','left','');
                        $Columns['Tipo Asiento'] = array('100px','left','');
                        $Columns['Creacion'] = array('85px','left','');
                        $Columns['Estado'] = array('85px','left','');
                        $Columns['tipo'] = array('0px','left','none');
                        $Columns['glosa'] = array('0px','left','none');
                        $Columns['idestado'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '400px','AltoRow' => '20px');
              }

              function CreaPlantillaContableSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->PlantillaContableGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }        

              private function PlantillaContableGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $plantillacontable)
                      {        $fecreacion = $this->Fechas->changeFormatDate($plantillacontable['fecreacion'],"DMY")[1];
                               $id = str_pad($plantillacontable['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['tipoasiento']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',($fecreacion));                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['txtestado']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['idtipoasto']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['glosa']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plantillacontable['idestado']));                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($plantillacontable['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }
              
              function TraeDatosPlantilla($item)
              {        
                       $ServicioAtributo = new ServicioItemAtributo(); 
                       $datoAtributos = $ServicioAtributo->BuscarItemsconAtributos($item);
                       return $datoAtributos;
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function TraeDatosTipoAsiento()
              {        
                       $ServicioTipoAsiento = new ServicioTipoAsiento();
                       $datoTipoAsiento = $ServicioTipoAsiento->BuscarTipoAsiento(array('estado' => 1));
                       return $datoTipoAsiento;
              }
              
              private function CreaComboTipoAsiento()
              {       
                       $datosTipoAsiento= $this->TraeDatosTipoAsiento();
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("tipoasiento",$datosTipoAsiento)->Selected(1)->Create("s04");
              }
              
              function MuestraPlantillaContable($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("idPlantilla".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));
                            $Ajax->Assign("tipoasiento".$this->Sufijo,"value", trim($Datos[0]['idtipoasto']));                       
                            $Ajax->Assign("glosa".$this->Sufijo,"value", trim($Datos[0]['glosa']));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);

                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaPlantillaContable($Ajax,"NOEXISTEID","No existe una Plantilla con ese ID.");
                       }
              }
              
              function MuestraPlantillaContableGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->PlantillaContableGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }
              
              private function SearchGridValuesDetalle()
              {         $Columns['opcion'] = array('50px','left','');
                        $Columns['Cuenta'] = array('250px','left','');
                        $Columns['idCuenta'] = array('0px','left','none');
                        $Columns['Tipo'] = array('150px','left','');
                        $Columns['idTipo'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }
              
              function MuestraDetallePlantillaContable($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo.'DetallePlantilla');
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesDetalle());
                       $ObjHTML = $this->DetallePlantillaContable($SearchGrid,$Datos);
                       $Ajax->Assign('SeccionDetallePlantilla'.$this->Sufijo,"innerHTML", $ObjHTML);
                       return $Ajax;
              }
              
              private function DetallePlantillaContable($ObjSchGrid,$Datos)
              {       foreach ($Datos as $plantillacontable)
                      {        $id = str_pad($plantillacontable['id'],2,'0',STR_PAD_LEFT);
                               $cuenta = trim($plantillacontable['cuenta']);
                               $idcuenta = trim($plantillacontable['idcuenta']);
                               $tipo = trim($plantillacontable['idmovnto']);
                               $idGeneral = $this->Sufijo.$idcuenta;
                               if($tipo=='D'){
                                   $destipo = 'DEBE';
                               }else{
                                   $destipo = 'HABER';
                               }
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input id="btn_eliPlantilla'.$this->Sufijo.'" class="borrar"'
                                      .'src="src/modules/contabilidad/plantillacontable/img/delete.png"'
                                      .'type="image" name="eliPlantilla'.$this->Sufijo.'"'
                                      .'onclick="return deleteFilaPlantilla();" />');
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cuenta);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="idCuentaContable'.$idGeneral.'" name="idCuentaContable'.$idGeneral.'" value="'.$idcuenta.'">');
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$destipo);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="idtipomovimiento'.$idGeneral.'" name="idtipomovimiento'.$idGeneral.'" value="'.$tipo.'">');                           
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,"");
                      }
                     
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }
              
               
              function MuestraPlantillaContableGuardado($Ajax,$PlantillaContable)
              {       if (count($PlantillaContable)>0)
                        {   $id = str_pad($PlantillaContable[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraPlantillaContableGrid($Ajax,$PlantillaContable);
                            return $this->RespuestaPlantillaContable($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaPlantillaContable($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraPlantillaContableEditado($Ajax,$PlantillaContable)
              {        foreach ($PlantillaContable as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraPlantillaContableRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaPlantillaContable($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaPlantillaContable($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraPlantillaContableEliminada($Ajax,$PlantillaContable)
              {        foreach ($PlantillaContable as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraPlantillaContableRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaPlantillaContable($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaPlantillaContable($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraPlantillaContableRowGrid($Ajax,$PlantillaContable,$estado=1)
              {         $Fila = $this->SearchGrid->GetRow($PlantillaContable['id'],$estado); 
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",trim($PlantillaContable['id']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($PlantillaContable['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($PlantillaContable['tipoasiento']));
                        $fecreacion = $this->Fechas->changeFormatDate($PlantillaContable['fecreacion'],"DMY");
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($PlantillaContable['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($PlantillaContable['idtipoasto']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($PlantillaContable['glosa']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($PlantillaContable['idestado']));
 
                        return $Ajax;
              }

              function MuestraPlantillaContableExcepcion($Ajax,$Msg)
              {        return $this->RespuestaPlantillaContable($Ajax,"EXCEPCION",$Msg);    
              }

              private function RespuestaPlantillaContable($Ajax,$Tipo,$Data)
              {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                      $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                      return $Ajax;
              }
             
        }
?>

