<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioRptEstudiantes.php");
        require_once("src/modules/maestria/rptestudiantes/render/RenderRptEstudiantes.php");
        //require_once("src/modules/maestria/rptestudiantes/vista/rptEstudiantes.php");
        require_once("src/rules/maestria/servicio/ServicioReporte.php");

        class ControlRptEstudiantes
        {       private $ServicioEstudiante;
                private $RenderEstudiante;

                function __construct()
                {       $this->ServicioEstudiante = new ServicioRptEstudiantes();
                        $this->RenderEstudiante = new RenderRptEstudiantes();
                }

                function CargaMantenimiento()
                {       echo $this->RenderEstudiante->CreaMantenimiento();
                }

                function ConsultaByID()
                {       $xAjax = new xajaxResponse();
                        $Estudiantes = $this->ServicioEstudiante->BuscarEstudiantes();
                        if ($Estudiantes[0])
                        {   $SearchGrid = $this->RenderEstudiante->CreaSearchGrid($Estudiantes,0);
                            $xAjax->Assign("WrkGrd","innerHTML",$SearchGrid);
                            $xAjax->Call("ElementStatus","MinPrn");
                        }
                        else
                        {       $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_ERR",$Estudiantes[1]);
                        }
                        return $xAjax;
                }

                function ExportarExcel()
                {       $xAjax = new xajaxResponse();
                        $Utils = new Utils();
                        $urlfl = $Utils->Encriptar("src/modules/maestria/rptestudiantes/vista/rptEstudiantes.php");
                        $title = "Reporte Generado Con Exito";
                        $jsonModal['Modal'] = "ModalExportExcel";
                        $jsonModal['Title'] = $title;
                        $jsonModal['Carga'] = "<iframe id='ModalExportExcel name='ModalExportExcel' src='index.php?url=".$urlfl."' height='465px' width='100%' scrolling='si' frameborder='0' marginheight='0' marginwidth='0'></iframe>";  
                        $jsonModal['Ancho'] = "230";
                        $jsonModal['Alto']  = "35";
                        $xAjax->call("CreaModal", json_encode($jsonModal));
                        $xAjax->call("document.getElementById('x1xx').style.display = 'none';");
                        return $xAjax;
                }
        }
?>

