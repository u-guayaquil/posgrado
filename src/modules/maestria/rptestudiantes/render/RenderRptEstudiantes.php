<?php
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php");

        class RenderRptEstudiantes
        {     private $Width;
              private $Search;

                function __construct()
                {       $this->Search = new SearchInput();
                        $this->SearchGrid = new SearchGrid();
                }

                function CreaMantenimiento()
                {       $Col2="40%";

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="35">';
                        $HTML.= '           <td class="Form-Label" colspan="5">:: REPORTE DE ESTUDIANTES</td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="10">';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="35">';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="button" id="Buscar" class="p-txt-label-bold" style="height: 30px;text-decoration: none;font-weight: 600;font-size: 14px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin:auto;" value="Consultar" onclick=" return SearchEstudiantesGetDataGrid(this);">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col2.'">';
                        $HTML.='                <input type="button" id="ExpXls" class="p-txt-label-bold" style="height: 30px;text-decoration: none;font-weight: 600;font-size: 14px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin:auto;" value="Exp. Excel" onclick=" return ButtonClick(\'ExpXls\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        return $HTML.'</table>';
                }

                function CreaSearchGrid($Estudiantes,$Cerrado=0)
                {       $this->Cerrado = $Cerrado;
                        $this->Columnas = $Estudiantes[1];
                        $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Estudiantes[1]);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }

                function GetGridTableWidth()
                {        return $this->Width;
                }

                private function SearchGridConfig()
                {       $Columns['No'] = array('30px','center','');
                        //$Columns['Facultad'] = array('250px','left','');
                        //$Columns['Maestria'] = array('250px','left','');
                        //$Columns['Cohorte'] = array('90px','center','');
                        $Columns['Cédula'] = array('75px','center','');
                        $Columns['Apellidos'] = array('150px','left','');
                        $Columns['Nombres'] = array('150px','left','');
                        $Columns['Estado'] = array('70px','center','');
                        return array('AltoCab' => '35px','DatoCab' => $Columns,'AltoDet' => '300px','AltoRow' => '25px','FindTxt' =>false);
                }

                private function GridDataHTML($Grid,$Datos)
                {       $num = 1;
                        foreach ($Datos as $Dato)
                        {       $Id = str_pad($num,2,'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($Id,'',$num++);
                                //$Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['FACULTAD']));
                                //$Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['MAESTRIA']));
                                //$Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['COHORTE']));
                                $Grid->CreaSearchCellsDetalle($Id,'',$Dato['CEDULA']);
                                $Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['APELLIDOS']));
                                $Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['NOMBRES']));
                                $Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['ESTADO']));
                                //$Grid->CreaSearchCellsDetalle($Id,'',($Dato['ESTADO']==1 ? "ACTIVO" : "RETIRADO"));
                                $Grid->CreaSearchRowsDetalle ($Id,"black",0);
                        }
                        return $Grid->CreaSearchTableDetalle(0,$Datos[1]);
                }

                function MuestraGrid($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {        $Ajax->Assign($Dato[0],"innerHTML",$Dato[1]);
                        }
                        return $Ajax;
                }
        }
?>

