<?php   require_once("src/rules/maestria/servicio/ServicioRptEstudiantes.php");
        require_once("src/rules/maestria/servicio/ServicioReporte.php");
        require __DIR__ . "/vendor/autoload.php";
        use PhpOffice\PhpSpreadsheet\Spreadsheet;
        use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
        use PhpOffice\PhpSpreadsheet\Worksheet;
        use PhpOffice\PhpSpreadsheet\IOFactory;
        use PhpOffice\PhpSpreadsheet\Style\Border;
        use PhpOffice\PhpSpreadsheet\Style\Fill;
        use PhpOffice\PhpSpreadsheet\Style\Style;

        class rptEstudiantes
        {       function DescargarExcel($Estudiantes)
                {       $documento = new Spreadsheet();
                        $fila = 5;
                        $documento
                            ->getProperties()
                            ->setCreator("Posgrados - UG")
                            ->setLastModifiedBy('UG') // última vez modificado por
                            ->setTitle('REPORTE DE ESTUDIANTES REGISTRADOS EN MAESTRIAS')
                            ->setSubject('REPORTE DE ESTUDIANTES REGISTRADOS EN MAESTRIAS')
                            ->setDescription('Este reporte fue generado en la plataforma de Posgrados - UG')
                            ->setKeywords('reporte maestria estudiantes')
                            ->setCategory('Repotes');

                            $documento->getActiveSheet()->getStyle('B2:B4')->getFont()->setBold(true)->setSize(16);
                            $documento->getActiveSheet()->getStyle('A4:G4')->getFont()->setBold(true)->setSize(12);
                            $documento->getActiveSheet()->getStyle('D4:D9999')->getAlignment()->setHorizontal('center');
                            $documento->getActiveSheet()->getStyle('A4:G4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('265792');
                            $documento->getActiveSheet()->getStyle('A4:G4')->getFont()->getColor()->setRGB('FFFFFF');

                            $documento->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                            $documento->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                            $documento->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                            $documento->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                            $documento->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                            $documento->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                            $documento->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

                            $hoja = $documento->getActiveSheet();
                            $hoja->setTitle("ESTUDIANTES - POSGRADO");
                            $hoja->setCellValue("B2", "REPORTE DE ESTUDIANTES REGISTRADOS EN MAESTRIAS - UG");
                            $hoja->setCellValue("A4", "FACULTAD");
                            $hoja->setCellValue("B4", "MAESTRIA");
                            $hoja->setCellValue("C4", "COHORTE");
                            $hoja->setCellValue("D4", "CEDULA");
                            $hoja->setCellValue("E4", "APELLIDOS");
                            $hoja->setCellValue("F4", "NOMBRES");
                            $hoja->setCellValue("G4", "ESTADO");

                            foreach($Estudiantes as $value){
                                $hoja->setCellValue("A".$fila, utf8_encode($value['FACULTAD']));
                                $hoja->setCellValue("B".$fila, utf8_encode($value['MAESTRIA']));
                                $hoja->setCellValue("C".$fila, $value['COHORTE']);
                                $hoja->setCellValue("D".$fila, $value['CEDULA']);
                                $hoja->setCellValue("E".$fila, utf8_encode($value['APELLIDOS']));
                                $hoja->setCellValue("F".$fila, utf8_encode($value['NOMBRES']));
                                $hoja->setCellValue("G".$fila, $value['ESTADO']);
                                $fila++;
                            }

                        $nombreDelDocumento = "Reporte_Estudiantes_Registrados_En_Maestria.xlsx";

                        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
                        header('Cache-Control: max-age=0');

                        $writer = IOFactory::createWriter($documento, 'Xlsx');
                        file_put_contents('depuracion.txt', ob_get_contents());
                        /* Limpiamos el búfer */
                        ob_end_clean();
                        $writer->save('php://output');
                        exit;
                }
        }

        $rptEst = new rptEstudiantes();
        $ServicioEstudiantes = new ServicioRptEstudiantes();
        $Estudiantes = $ServicioEstudiantes->BuscarEstudiantes();
        if ($Estudiantes[0])
        {       $rptEst->DescargarExcel($Estudiantes[1]);
        }   else
                echo $Estudiantes[1];
?>