<?php
        $Opcion = 'Estudiante';
        require_once('src/modules/maestria/rptestudiantes/controlador/ControlRptEstudiantes.php');
        $ControlEstudiante = new ControlRptEstudiantes();

        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlEstudiante,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('ExportarExcel', $ControlEstudiante,'ExportarExcel'));
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript();
                    require_once('src/utils/links.php');
            ?>
            <script type="text/javascript" src="src/modules/maestria/rptestudiantes/js/jsRptEstudiantes.js"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:530px">
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionData">
                             <?php  $ControlEstudiante->CargaMantenimiento();  ?>
                        </div>
                        <div id="<?php echo 'WrkGrd'; ?>" class="FormSectionGrid">
                        </div>
                    </form>
                </div>
        </body>
        </html>
