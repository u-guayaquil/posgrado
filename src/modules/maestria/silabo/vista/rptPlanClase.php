<?php   require_once("src/rules/maestria/dao/DAOSilabo.php");
        require_once("src/rules/maestria/dao/DAOPlanAnalitico.php");
        require_once("src/rules/maestria/servicio/ServicioMatContenido.php");
        require_once("src/libs/plugins/pdf/vendor/mpdf/mpdf/mpdf.php");
        
        class rptPlanClase
        {       private $Works;
                private $Col1="25%"; 
                private $Col2="25%"; 
                private $Col3="25%"; 
                private $Col4="25%";
                private $Alto="22px";
                
                public function __construct()
                {      $this->Works = new DAOSilabo();  
                }

                private function rptPlanClaseContenido($ID,$Params)
                {       $DAOPlanAnalitico = new DAOPlanAnalitico();
                        $PlanClases = $DAOPlanAnalitico->ObtenerPlanClases($Params);
                        
                        $PlanObjetv = $DAOPlanAnalitico->ObtenerPlanClasesObjetivos($Params[0]);
                        $METODO="<br>";
                        $OBJETIVO="<br>";
                        $COMPOTES="<br>";
                        $Veces = 1;

                        foreach ($PlanObjetv[1] as $Dato)
                        {       if ($Veces==1)
                                { $OBJETIVO.=$Dato['OBJETIVO']."<br><br>";
                                $METODO.= $Dato['METODO']."<br><br>";
                                $COMPOTES.='<b>CD:</b> '.$Dato['CD'].'<br><br><b>CPE:</b> '.$Dato['CP'].'<br><br><b>CA:</b> '.$Dato['CA'].'<br><br>';
                                //$Veces=2;
                                }
                        }
                            
                        $SUMCD = 0;
                        $SUMCP = 0;
                        $SUMCA = 0;
                        $HTML = "<tr>";
                        $HTML.= '<td colspan="4" style="border:none">';
                        $HTML.= '<table border="1" style="font-family:tahoma;font-size: 7pt;width:100%; border-collapse: collapse" cellpadding="0">';
                        $HTML.= '       <tr>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:3%;"><b>SEM</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:7%"><b>FECHA</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:30%"><b>DESCRIPCIÓN</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:3%"><b>CD</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:3%"><b>CPE</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:3%"><b>CA</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:17%"><b>OBJETIVOS<br>APRENDIZAJE</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:17%"><b>MÉTODOS</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:17%"><b>EVALUACIÓN</b></td>';                        
                        $HTML.= '       </tr>';
                        $NUM=0;    
                        foreach ($PlanClases[1] as $Dato)
                        {   $HTML.= '  <tr>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        utf8_encode($Dato['SEMANA']);
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        $Dato['FECHA']->format('d/m/Y');
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:justify;height:'.$this->Alto.'">';
                            $HTML.=        '<div style="padding-right:5px; padding-left:5px">'.utf8_encode($Dato['DESCRIPCION']).'</div>';
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        $Dato["CD"];
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        $Dato["CP"];
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        $Dato["CA"];
                            $HTML.= '      </td>';
                            
                            if ($NUM==0)
                            {   $HTML.= '      <td valign="top" rowspan="@" style="text-align:justify">';
                                $HTML.=        '<div style="padding-right:5px; padding-left:5px">'.utf8_encode($OBJETIVO).'</div>';
                                $HTML.= '      </td>';

                                $HTML.= '      <td valign="top" rowspan="@" style="text-align:justify">';
                                $HTML.=        '<div style="padding-right:5px; padding-left:5px">'.utf8_encode($METODO).'</div>';
                                $HTML.= '      </td>';
                                $HTML.= '      <td valign="top" rowspan="@" style="text-align:justify">';
                                $HTML.=        '<div style="padding-right:5px; padding-left:5px">'.utf8_encode($COMPOTES).'</div>';
                                $HTML.= '      </td>';
                            }
                            
                            $HTML.= '  </tr>';
                            $SUMCD = $SUMCD+$Dato["CD"];
                            $SUMCP = $SUMCP+$Dato["CP"];
                            $SUMCA = $SUMCA+$Dato["CA"];
                            $NUM++;
                        }
                        
                        $HTML.= '       <tr>';
                        $HTML.= '           <td colspan="3" style="text-align:center;height:'.$this->Alto.'"></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:3%"><b>'.$SUMCD.'</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:3%"><b>'.$SUMCP.'</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:3%"><b>'.$SUMCA.'</b></td>';
                        $HTML.= '           <td colspan="3" style="text-align:center;height:'.$this->Alto.'"></td>';
                        $HTML.= '       </tr>';
                        $HTML.= '</table>';   
                        $HTML.= "</td>";
                        $HTML.= "</tr>";
                        $HTML = str_replace("@", $NUM, $HTML);
                        return $HTML;
                }                    

                function rptHTMLClase($Params)
                {       $Datos = $this->Works->Silabo($Params[0]);
                        if ($Datos[0])
                        {   foreach ($Datos[1] as $Dato)
                            {       $HTML = '<table border="1" style="background-color: #FFF ;border:none;font-family:tahoma; font-size:8pt; width:1000px" cellpadding="5" cellspacing="5">';
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td rowspan="4" style="text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">';
                                    $HTML.= '               <img src="src/public/img/logo_ug_small.png" width="87px" height="103px">';
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>UNIVERSIDAD DE GUAYAQUIL</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Formato # DP-2019-003</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>'.$Params[4].'</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Versión 1.0</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>COORDINACIÓN DE POSGRADO</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Última actualización:</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>PLAN ANALÍTCO</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">29/08/2019</td>';
                                    $HTML.= '       </tr>';                        

                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center;height:20px; border-top: none; border-left: none; border-right: none;" colspan="4"><b>DATOS INFORMATIVOS</b></td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Maestría</b></td>';
                                    $HTML.= '           <td  style="border:none" colspan="3">';
                                    $HTML.=                 utf8_encode($Dato['MAESTRIA']);                         
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';         
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Asignatura</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                 utf8_encode($Dato['MATERIA']);                            
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-left:none; border-right:none; border-top:none" colspan="2"><b>HORAS DE APRENDIZAJE POR COMPONENTE</b></td>';
                                    $HTML.= '       </tr>';   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Modalidad de Estudio</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                 'PRESENCIAL';                  
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente de Docencia (CD)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                 $Dato['COMPONENTECD'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>COHORTE</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  $Params[1];
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente Práctico/Experimental (CPE)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECP'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Prerequisito Académico</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  utf8_encode($Dato['REQUISITO']);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente Autónomo (CA)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECA'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Unidad de Organización Curricular</b></td>';
                                    $HTML.= '           <td style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                 utf8_encode($Dato['ORGCURRICULAR']);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Total Horas</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECD']+$Dato['COMPONENTECA']+$Dato['COMPONENTECP'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Docente Responsable</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  $Params[2];
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Email</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Params[3];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                    
                                    $HTML.=         $this->rptPlanClaseContenido($Dato['ID'],$Params);    
                                    
                                    $DateControl = new DateControl();
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Elaborado<br>Por Docente:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'">'.$Params[2].'</td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Revisado<br>Por Responsable Académico:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                    
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Aprobado<br>Por Decano:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                                   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Validado<br>Por Dirección de Posgrado:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '</table>';
                            }        
                            return $HTML;
                        }
                }
        }
        
        if (isset($_GET['opcion']))
        {   $Utils = new Utils();
            $Nodos = $Utils->Desencriptar($_GET['opcion']);
            $Params = explode("|",$Nodos);
            
          //Params = [IDMTC,CICLO,DOCTE,EMAIL,FCLTD,IDCLO,IDMAE,IDMAT,IDDOC,IDGRP];
            $rptPlanClase = new rptPlanClase();
            $HTML= $rptPlanClase->rptHTMLClase($Params);
            //echo $HTML; 
            
            $mpdf = new Mpdf();
            $mpdf->WriteHTML($HTML);
            $mpdf->Output('PLN'.$Params[8].$Params[9].'.pdf','I');
            
        }
        else
        echo "Error en el paso de opciones.";
        

