<?php   require_once("src/rules/maestria/dao/DAOSilabo.php");
        require_once("src/rules/maestria/dao/DAOPlanAnalitico.php");
        require_once("src/rules/maestria/servicio/ServicioMatContenido.php");
        require_once("src/libs/plugins/pdf/vendor/mpdf/mpdf/mpdf.php");
        
        class rptSilabo
        {       private $Works;
                private $Col1="25%"; 
                private $Col2="25%"; 
                private $Col3="25%"; 
                private $Col4="25%";
                private $Alto="22px";
                
                public function __construct()
                {      $this->Works = new DAOSilabo();  
                }

                private function rptSilaboLibros($Datos)
                {       $HTML = '<table border="1" style="font-family:tahoma;font-size: 8pt;width:100%; border-collapse: collapse" cellpadding="0">';
                        $HTML.= '       <tr>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:80%;"><b>Titulo del Libro</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:10%"><b>Año</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:10%"><b>No.</b></td>';
                        $HTML.= '       </tr>';
                        foreach ($Datos[1] as $Dato)
                        {   $HTML.= '  <tr>';
                            $HTML.= '      <td style="text-align:justify;height:'.$this->Alto.'">';
                            $HTML.=        utf8_encode($Dato['LIBRO']);
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        utf8_encode($Dato['EDICION']);
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        $Dato["EJEMPLARES"];
                            $HTML.= '      </td>';
                            $HTML.= '  </tr>';
                        }
                        $HTML.= '</table>';
                        return $HTML;
                }   

                private function rptSilaboContenido($ID,$Params)
                {       $DAOPlanAnalitico = new DAOPlanAnalitico();
                        $ServicioMatContenido = new ServicioMatContenido();   
                        $prepareDQL = array('idmtc' => intval($ID), 'idcht'=>$Params[10]);
                        $root = json_decode($ServicioMatContenido->BuscarIndice($prepareDQL));
                        $HTML = "";
                        foreach($root as $sheet)
                        {       if ($sheet->estado!=0)
                                {   $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col1.';height:'.$this->Alto.'"><b>Unidad No. y Título</b></td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col2.'">';
                                    $HTML.=                  $sheet->orden.'.- '.$sheet->descripcion;
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col3.'"><b>Resultado de Aprendizaje</b></td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col4.'">';
                                    $HTML.=                  $sheet->resultado;
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';
                                    
                                    $Params[1] = $sheet->orden;
                                    $Distributivo = $DAOPlanAnalitico->ObtenerDistributivo($Params);
                                    if ($Distributivo[0])
                                    {   $Horas = $Distributivo[1][0];
                                        $HTML.= '   <tr>';
                                        $HTML.= '       <td  style="border:none; width:'.$this->Col1.';height:'.$this->Alto.'"><b>Distributivo de Horas</b></td>';
                                        $HTML.= '       <td  style="border:none; width:'.$this->Col2.'"><b>Componente CD:</b> '.$Horas['CD'].'</td>';
                                        $HTML.= '       <td  style="border:none; width:'.$this->Col3.'"><b>Componente CPE:</b> '.$Horas['CPE'].'</td>';
                                        $HTML.= '       <td  style="border:none; width:'.$this->Col4.'"><b>Componente CA:</b> '.$Horas['CA'].'</td>';
                                        $HTML.= '   </tr>';                                    
                                    }
                                    
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center; width:'.$this->Col1.';height:'.$this->Alto.'"><b>CONTENIDO TEMATICO</b></td>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center; width:'.$this->Col3.'"><b>OBJETIVOS DE APRENDIZAJE</b></td>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center; width:'.$this->Col2.'"><b>METODOS, TECNICAS E INSTRUMENTOS</b></td>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center; width:'.$this->Col4.'"><b>EVALUACION DE LOS APRENDIZAJES</b></td>';
                                    $HTML.= '       </tr>';

                                    $cont = "";
                                    $meth = $sheet->metodo;
                                    $objt = $sheet->objetivo;
                                    if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                    {   $Datos = $this->rptSilaboContenidoSgte($sheet->hijos);
                                        $cont.= $Datos['descripcion'];
                                        $meth.= $Datos['metodo'];
                                        $objt.= $Datos['objetivo'];
                                    }
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td valign="top" style="text-align:justify;width:'.$this->Col1.';height:'.$this->Alto.'">';
                                    $HTML.=             $cont;
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td valign="top" style="text-align:justify;width:'.$this->Col3.'">';
                                    $HTML.=             $objt;
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td valign="top" style="text-align:justify;width:'.$this->Col2.'">';
                                    $HTML.=             $meth;
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td valign="top" style="text-align:justify;width:'.$this->Col4.'">';
                                    $HTML.=                  '<b>CD:</b>'.$sheet->cd.'<br><b>CPE:</b>'.$sheet->cp.'<br><b>CA:</b>'.$sheet->ca;
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                }    
                        }
                        return $HTML;
                }                    

                private function rptSilaboContenidoSgte($root)
                {       $descripcion = "";
                        $metodo = "";
                        $objetivo = "";       
                        foreach($root as $sheet)
                        {       if ($sheet->estado!=0)
                                {   if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                        $descripcion.= '<b>'.$sheet->orden.'-'.$sheet->descripcion.'</b>'.'<br>';
                                    else
                                        $descripcion.= $sheet->orden.'-'.$sheet->descripcion.'<br>';

                                    $metodo.= ($sheet->metodo!='' ? $sheet->orden.'-'.$sheet->metodo.'<br>':'');
                                    $objetivo.= ($sheet->objetivo!='' ? $sheet->orden.'-'.$sheet->objetivo.'<br>':''); 

                                    if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                    {   $Datos = $this->rptSilaboContenidoSgte($sheet->hijos);
                                        $descripcion.=$Datos['descripcion'].'<br>';
                                        $metodo.=$Datos['metodo'].'<br>';
                                        $objetivo.=$Datos['objetivo'].'<br>';
                                    }
                                }
                        }       
                        return array("descripcion"=>$descripcion,"metodo"=>$metodo,"objetivo"=>$objetivo);
                }
                
                function rptHTMLSilabo($Params)
                {       $Datos = $this->Works->Silabo($Params[0]);
                        if ($Datos[0])
                        {   foreach ($Datos[1] as $Dato)
                            {       $HTML = '<table border="1" style="background-color: #FFF ;border:none;font-family:tahoma; font-size:8pt; width:1000px" cellpadding="5" cellspacing="5">';
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td rowspan="4" style="text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">';
                                    $HTML.= '               <img src="src/public/img/logo_ug_small.png" width="87px" height="103px">';
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>UNIVERSIDAD DE GUAYAQUIL</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Formato # DP-2019-002</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>'.$Params[4].'</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Versión 1.0</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>COORDINACION DE POSGRADO</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Última actualización:</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>SILABO</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">29/08/2019</td>';
                                    $HTML.= '       </tr>';                        

                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center;height:20px; border-top: none; border-left: none; border-right: none;" colspan="4"><b>DATOS INFORMATIVOS</b></td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Maestria</b></td>';
                                    $HTML.= '           <td  style="border:none" colspan="3">';
                                    $HTML.=                 utf8_encode($Dato['MAESTRIA']);                         
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';         
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Asignatura</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                 utf8_encode($Dato['MATERIA']);                            
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-left:none; border-right:none; border-top:none" colspan="2"><b>HORAS DE APRENDIZAJE POR COMPONENTE</b></td>';
                                    $HTML.= '       </tr>';   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Modalidad de Estudio</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  'PRESENCIAL';                  
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente de Docencia (CD)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                 $Dato['COMPONENTECD'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>COHORTE</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  $Params[1];
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente Práctico/Experimental (CPE)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECP'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Prerequisito Academico</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  utf8_encode($Dato['REQUISITO']);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente Autonomo (CA)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECA'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Unidad de Organización Curricular</b></td>';
                                    $HTML.= '           <td style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                 utf8_encode($Dato['ORGCURRICULAR']);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Total Horas</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECD']+$Dato['COMPONENTECA']+$Dato['COMPONENTECP'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Docente Responsable</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  $Params[2];
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Email</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Params[3];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; height:30px; border-left:none;border-right:none;border-top:none" colspan="4"><b>JUSTIFICACION</b></td>';
                                    $HTML.= '       </tr>';          
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="width:'.$this->Col1.';height:'.$this->Alto.'; border-left:none;border-right:none;border-top:none"><b>Objetivo General</b></td>';
                                    $HTML.= '           <td style="text-align: justify;border-left:none;border-right:none;border-top:none" colspan="3">';
                                    $HTML.=                 utf8_encode($Dato['OBGENERAL']);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="width:'.$this->Col1.';height:'.$this->Alto.'; border-left:none;border-right:none;border-top:none"><b>Objetivo Especifico</b></td>';
                                    $HTML.= '           <td style="text-align: justify; border-left:none;border-right:none;border-top:none" colspan="3">';
                                    $HTML.=                 utf8_encode($Dato['OBESPECIFICO']);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                          
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Metodología</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  utf8_encode($Dato['METODOLOGIA']);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Líneas de Investigación a la que Tributa</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  utf8_encode($Dato['LINEA']);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center;height:30px; border-left:none;border-right:none;border-top:none" colspan="4"><b>CONTENIDO</b></td>';
                                    $HTML.= '       </tr>';                                             
                                    $HTML.=         $this->rptSilaboContenido($Dato['ID'],$Params);    
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center;height:30px;border-left:none;border-right:none;border-top:none" colspan="4"><b>BIBLIOGRAFIA</b></td>';
                                    $HTML.= '       </tr>';                     
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center;height:'.$this->Alto.'" colspan="2"><b>BASICA</b></td>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center;height:'.$this->Alto.'" colspan="2"><b>COMPLEMENTARIA</b></td>';
                                    $HTML.= '       </tr>';                     
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td valign="top" colspan="2" style="border:none">';
                                                            $Datos = $this->Works->SilaboLibros($Dato['ID'],'BAS');    
                                    $HTML.=                 $this->rptSilaboLibros($Datos);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td valign="top" colspan="2" style="border:none">';
                                                            $Datos = $this->Works->SilaboLibros($Dato['ID'],'COM');    
                                    $HTML.=                 $this->rptSilaboLibros($Datos);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td valign="top" colspan="4" style="border:none">';
                                                            $Datos = $this->Works->SilaboLibros($Dato['ID'],'LNK');    
                                    $HTML.=                 $this->rptSilaboLibros($Datos);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';
                                    
                                    $DateControl = new DateControl();
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Elaborado<br>Por Docente:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'">'.$Params[2].'</td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Revisado<br>Por Responsable Académico:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                    
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Aprobado<br>Por Decano:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                                   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Validado<br>Por Dirección de Posgrado:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '</table>';
                            }        
                            return $HTML;
                        }
                }
        }
        
        if (isset($_GET['opcion']))
        {   $Utils = new Utils();
            $Nodos = $Utils->Desencriptar($_GET['opcion']);
            $Params = explode("|",$Nodos);
            
          //Params = [IDMTC,CICLO,DOCTE,EMAIL,FCLTD,IDCLO,IDMAE,IDMAT,IDDOC,IDGRP];
            $rptSilabo = new rptSilabo();
            $HTML= $rptSilabo->rptHTMLSilabo($Params);
            
            $mpdf = new Mpdf();
            $mpdf->WriteHTML($HTML);
            $mpdf->Output('SLB'.$Params[8].$Params[9].'.pdf','I');
        }
        else
        echo "Error en el paso de opciones.";
        

