<?php   
        $Opcion = 'Silabo';
        require_once('src/modules/maestria/silabo/controlador/ControlSilabo.php');
        $ControlSilabo = new ControlSilabo();
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlSilabo,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlSilabo,'ConsultaGridByTX')); 
        $xajax->processRequest();        
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="Content-Type" content="Mime-Type; charset=UTF-8"/>
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/maestria/silabo/js/jsSilabo.js"></script>
            
        </head>
        <body>
                <div class="FormBasic" style="width:550px">
                    <div class="FormSectionMenu">              
                        <?php   $ControlSilabo->CargaSilaboBarButton($_GET['opcion']);
                                echo '<script type="text/javascript">';
                                echo '        BarButtonState("addPDF"); ';
                                echo '</script>';
                        ?>
                    </div>  
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionData">   
                             <?php  $ControlSilabo->CargaSilaboMantenimiento();  ?>
                        </div> 
                    </form>
                </div>       
        </body>
        </html>

        <?php
    
        /*    
        require_once("src/modules/maestria/silabo/render/RenderSilabo.php");
        require_once("src/rules/maestria/servicio/ServicioMatContenido.php");
        require_once("src/rules/maestria/dao/DAOSilabo.php");
        //require_once("src/libs/plugins/pdf/vendor/mpdf/mpdf/mpdf.php");
        
        //$ServicioMatContenido = new ServicioMatContenido();   
        //$root = json_decode($ServicioMatContenido->BuscarIndice(30));
        
       
        /* 
        $RenderSilabo = new RenderSilabo(new DAOSilabo());
        $Params = array(30,"Carlos","Mio","Cas","2322");
        echo $RenderSilabo->rptSilabo($Params);
             /* $mpdf = new Mpdf();
                            $mpdf->WriteHTML($HTML);
                            $mpdf->Output('silabo.pdf','I');*/
        ?>