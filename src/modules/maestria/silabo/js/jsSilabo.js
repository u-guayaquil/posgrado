var grupos = "iddocente:txdocente";
var FCLTD = "";
function ButtonClick(Operacion)
{       if (Operacion=='addPdf')
        {   var IDMTC = ElementGetValue("idmatcont");
            var CICLO = ElementGetValue("txciclo");
            var DOCTE = ElementGetValue("txdocente");
            var EMAIL = ElementGetValue("txdocemail");
            
            var IDCLO = ElementGetValue("idciclo");
            var IDMAE = ElementGetValue("idmaestria");
            var IDMAT = ElementGetValue("idmateria");
            var IDDOC = ElementGetValue("iddocente");
            var IDGRP = ElementGetValue("idgrupo");
            var IDCHT = ElementGetValue("idcohorte");
            
            var Params = [IDMTC,CICLO,DOCTE,EMAIL,FCLTD,IDCLO,IDMAE,IDMAT,IDDOC,IDGRP,ElementGetValue("idreporte"),IDCHT];
            if (IDMTC!="" && CICLO!="" && DOCTE!="" && EMAIL!="")
                xajax_CargaModal(Operacion,Params);
            else
                SAlert("warning", "¡Campos vacios!", "Debe seleccionar todos los datos de la consulta.");
        }
        else if (Operacion=='btmaestria')
        {   xajax_CargaModal(Operacion,0);
        }
        else if (Operacion=='btdocente')
        {   var IDFAC = ElementGetValue("idfacultad");
            if (IDFAC!="")
                xajax_CargaModal(Operacion,IDFAC);
            else
                SAlert("warning", "¡Cuidado!", "Selecione una maestria.");    
        }    
        else if (Operacion=="btciclo")
        {   var IDVER = ElementGetValue("idversion");
            if (IDVER!="") 
                xajax_CargaModal(Operacion,IDVER);
            else
                SAlert("warning", "¡Cuidado!", "Selecione una maestria con una versión vigente.");    
        }
        else if (Operacion=="btgrupo")
        {   var IDCIC = ElementGetValue("idciclo");
            if (IDCIC!="") 
                xajax_CargaModal(Operacion,IDCIC);
            else
                SAlert("warning", "¡Cuidado!", "Selecione un ciclo.");    
        }
        else if (Operacion=="btmatcont")
        {   var IDVER = ElementGetValue("idversion");
            var IDCLO = ElementGetValue("idciclo");
            if (IDCLO!="") 
            {   var Params = [IDVER,IDCLO];
                xajax_CargaModal(Operacion,Params);
            }    
            else
            SAlert("warning", "¡Cuidado!", "Selecione un ciclo con malla vigente.");    
        }
        return false;
}

function SearchByElement(Elemento)
{       if (Elemento.id=="FindDocenteTextBx")
        {   var IDFAC = ElementGetValue("idfacultad");
            if (IDFAC!="")
            {   var Params = [IDFAC,Elemento.value];
                xajax_BuscaModalByTX("btdocente",Params);
            }    
        }
        else if (Elemento.id=="FindMaestriaTextBx")
        {    xajax_BuscaModalByTX("btmaestria",Elemento.value);
        }
        else if (Elemento.id=="FindMatcontTextBx")
        {   var IDVER = ElementGetValue("idversion");
            var IDCLO = ElementGetValue("idciclo");
            if (IDCLO!="") 
            {   var Params = [IDVER,IDCLO,Elemento.value];
                xajax_BuscaModalByTX("btmatcont",Params);
            }    
            else
            SAlert("warning", "¡Cuidado!", "Selecione un ciclo con malla vigente.");    
        }        
        return false;    
}

function SearchMaestriaGetData(DatosGrid)
{       var IDVER = DatosGrid.cells[2].innerHTML;
        if (parseInt(IDVER)!=0)
        {   var IDMST = DatosGrid.cells[0].innerHTML;
            if (ElementGetValue("idmaestria")!=IDMST)
            {   //BarButtonState("Inactive");
                ElementClear("id:idfacultad:idversion:idmalla:idciclo:txciclo:idmateria:idmatcont:txmatcont:"+grupos);
                document.getElementById("idversion").value = IDVER;
                document.getElementById("idfacultad").value = DatosGrid.cells[5].innerHTML;
                document.getElementById("idmaestria").value = IDMST;
                document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML+" ("+ DatosGrid.cells[3].innerHTML+")";
                FCLTD = DatosGrid.cells[4].innerHTML;
                cerrar();
            }
            else
            cerrar();
        }
        else
        SAlert("error", "¡Contenido no vigente!", "No se encuentra vigente una versión de contenido para la maestria seleccionada.");    
        return false;
}    

function SearchCicloGetData(DatosGrid)
{       var IDCIC = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("idciclo")!=IDCIC)
        {   var IDMLL = DatosGrid.cells[1].innerHTML;
            if (IDMLL!=0)
            {   ElementClear("idmateria:idmatcont:txmatcont:"+grupos);
                document.getElementById("idmalla").value = IDMLL;
                document.getElementById("idciclo").value = IDCIC;
                document.getElementById("txciclo").value = DatosGrid.cells[2].innerHTML;
                document.getElementById("idcohorte").value = DatosGrid.cells[4].innerHTML;
                cerrar();
            }
            else
            SAlert("error","Malla inactiva","No se puede planificar con una malla inactiva.");
        }    
        else    
        cerrar();    
        return false;
}

function SearchMatcontGetData(DatosGrid)
{       var IDMTC = DatosGrid.cells[0].innerHTML;
        if (IDMTC!="")
        {   if (ElementGetValue("idmatcont")!=IDMTC)
            {   //BarButtonState("Inactive");
                ElementClear(grupos);
                document.getElementById("idmatcont").value = IDMTC;
                document.getElementById("idmateria").value = DatosGrid.cells[1].innerHTML;
                document.getElementById("txmatcont").value = DatosGrid.cells[2].innerHTML+" - "+DatosGrid.cells[3].innerHTML;
                //NivPlan = DatosGrid.cells[8].innerHTML;
                cerrar();
            } 
            else
            cerrar();
        }
        else
        SAlert("error", "Contenido temático no definido", "Esta materia no tiene definido contenido temático.");
        return false;
}

function SearchGrupoGetData(DatosGrid)
{       var IDGRP = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("idgrupo")!=IDGRP)
        {   document.getElementById("idgrupo").value = IDGRP;
            document.getElementById("txgrupo").value = DatosGrid.cells[1].innerHTML;
            cerrar();
        }                          
        else    
        cerrar();    
        return false;
}

function SearchDocenteGetData(DatosGrid)
{       var IDDOC = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("iddocente")!=IDDOC)
        {   document.getElementById("iddocente").value = IDDOC;
            document.getElementById("txdocente").value = DatosGrid.cells[2].innerHTML;
            document.getElementById("txdocemail").value =  DatosGrid.cells[3].innerHTML;
            cerrar();
        }    
        else
        cerrar();
        return false;
}
