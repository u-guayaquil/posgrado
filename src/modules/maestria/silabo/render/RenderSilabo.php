<?php   require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php"); 
        
        class RenderSilabo
        {       private $Search;
                private $Width;
                private $SearchGrid;
                 
                public function __construct()
                {      //$this->Works = $DAOSilabo;  
                       $this->Search = new SearchInput();   
                       $this->SearchGrid = new SearchGrid();
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']    =array('30px','center','none');
                            $Columns['Maestria']=array('350px','left','');
                            $Columns['V.ID']    =array('0px','left','none');
                            $Columns['Version'] =array('50px','center','');
                            $Columns['Facultad']=array('200px','left','');
                            $Columns['F.ID']=array('0px','left','none');
                        }
                        else if ($Operacion=="btciclo")
                        {   $Columns['ID']    =array('40px','center','');
                            $Columns['MLL.ID']=array('0px','center','none');
                            $Columns['Ciclo']=array('250px','left','');
                            $Columns['Malla']=array('150px','left','');
                            $Columns['COH.ID']=array('40px','center','none');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px','FindTxt'=>false);
                        }
                        else if ($Operacion=="btmatcont")
                        {   $Columns['C.ID']   =array('45px','center','none');
                            $Columns['M.ID']   =array('40px','center','');
                            $Columns['Materia']=array('350px','left','');
                            $Columns['Orden']=array('60px','center',''); 
                            $Columns['Cd']= array('0px','center','none');
                            $Columns['Cp']= array('0px','center','none');
                            $Columns['Ca']= array('0px','center','none');
                            $Columns['Tt']= array('0px','center','none');
                            $Columns['Np']= array('0px','center','none');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '250px','AltoRow' => '20px','FindTxt'=>true);
                        }
                        else if ($Operacion=="btdocente")
                        {   $Columns['Id'] = array('0px','center','none');
                            $Columns['Codigo'] = array('80px','center','');
                            $Columns['Docente']= array('300px','left','');
                            $Columns['Email']  = array('300px','left','');
                        }
                        else if ($Operacion=="btgrupo")
                        {   $Columns['Id'] = array('40px','center','');
                            $Columns['Grupo']= array('250px','left','');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px','FindTxt'=>false);
                        }          
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }                
                
                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }                

                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = str_pad($ArDato['IDM'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $idV = $ArDato['IDV'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idV);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['TXVERSION'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['FACULTAD'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['IDFACULTAD']);
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }
                            elseif ($Operacion=="btciclo")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDCICLO'],CICLO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMALLA']);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['CICLO']));
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MALLA'])); 
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDCOHORTE']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmatcont")
                            {   $Nivel = -1;
                                foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMATERIA'],MATERIA,'0',STR_PAD_LEFT);
                                        $mc = str_pad($ArDato['IDMATCONT'],MAESCON,'0',STR_PAD_LEFT);
                                        
                                        $nv = str_pad($ArDato['NIVELORDEN'],2,'0',STR_PAD_LEFT);
                                        if ($ArDato['NIVEL']!=$Nivel)
                                        {   $Nivel = $ArDato['NIVEL'];
                                            $Grid->CreaSearchCellsDetalle($id,'','');
                                            $Grid->CreaSearchCellsDetalle($id,'','');
                                            $Grid->CreaSearchCellsDetalle($id,'',"<b> CICLO ".$Nivel."</b>",2,0);
                                            $Grid->CreaSearchRowsDetalle ($id,"",0);
                                        }   
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMATCONT']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'','M: '.$nv);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CD']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CP']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CA']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['TOTAL']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['NIVPLAN']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btgrupo")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['ID'],GRUPO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['PARALELO'])));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }                                             
                            elseif ($Operacion=="btdocente")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['CEDULA'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['APELLIDOS'])).' '.utf8_encode(trim($ArDato['NOMBRES'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['EMAIL'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }                                             
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }            
                
                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();
                        return $BarButton->CreaBarButton($Buttons);
                }                
              
                function CreaSilaboMantenimiento()
                {      
                        $Col1="20%"; $Col2="80%";
                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="id" name="id" value="" />';
                        $HTML.= '               <input type="hidden" id="idversion" name="idversion" value="" />';
                        $HTML.= '               <input type="hidden" id="idfacultad" name="idfacultad" value="" />';
                        $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t15");
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Cohorte</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idmalla" name="idmalla" value="" />';
                        $HTML.= '               <input type="hidden" id="idcohorte" name="idcohorte" value="" />';
                        $HTML.= '               <input type="hidden" id="txdocemail" name="txdocemail" value="" />';
                        $HTML.=                 $this->Search->TextSch("ciclo","","")->Enabled(true)->Create("t15");       
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                    
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Materia</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idmateria" name="idmateria" value="" />';
                        $HTML.=                 $this->Search->TextSch("matcont","","")->Enabled(true)->Create("t15");                         
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';             
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Docente</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("docente","","")->Enabled(true)->Create("t15");   
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';   
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Grupo</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("grupo","","")->Enabled(true)->Create("t08");  
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Reporte</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <select class="sel-input s07" id="idreporte" name="idreporte">';
                        $HTML.= '                       <option value="1">SILABO</option>';
                        $HTML.= '                       <option value="2">PLAN ANALITICO</option>';
                        $HTML.= '               </select>';                            
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                        
                        
                        return $HTML.'</table>';
                }


/*                
                private function rptSilaboLibros($Datos)
                {       $HTML = '<table border="1" style="font-family:tahoma;font-size: 8pt;width:100%; border-collapse: collapse" cellpadding="0">';
                        $HTML.= '       <tr>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:80%;"><b>Titulo del Libro</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:10%"><b>Año</b></td>';
                        $HTML.= '           <td style="text-align:center;height:'.$this->Alto.';width:10%"><b>No.</b></td>';
                        $HTML.= '       </tr>';
                        foreach ($Datos[1] as $Dato)
                        {   $HTML.= '  <tr>';
                            $HTML.= '      <td style="text-align:justify;height:'.$this->Alto.'">';
                            $HTML.=        utf8_encode($Dato['LIBRO']);
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        utf8_encode($Dato['EDICION']);
                            $HTML.= '      </td>';
                            $HTML.= '      <td style="text-align:center;height:'.$this->Alto.'">';
                            $HTML.=        $Dato["EJEMPLARES"];
                            $HTML.= '      </td>';
                            $HTML.= '  </tr>';
                        }
                        $HTML.= '</table>';
                        return $HTML;
                }   

//OK                
                function rptSilabo($Params)
                {       $Datos = $this->Works->Silabo($Params[0]);
                        if ($Datos[0])
                        {   foreach ($Datos[1] as $Dato)
                            {       $HTML = '<table border="1" style="background-color: #FFF ;border:none;font-family:tahoma; font-size:8pt; width:1000px" cellpadding="5" cellspacing="5">';
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td rowspan="4" style="text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">';
                                    $HTML.= '               <img src="src/public/img/logo_ug_small.png" width="87px" height="103px">';
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>UNIVERSIDAD DE GUAYAQUIL</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Formato # DP-2019-002</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>'.$Params[4].'</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Versión 1.0</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>COORDINACION DE POSGRADO</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">Última actualización:</td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr height="10px">';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;" colspan="2"><b>SILABO</b></td>';
                                    $HTML.= '           <td style="font-size:7pt;text-align:center; border-bottom: none; border-top: none; border-left: none; border-right: none;">29/08/2019</td>';
                                    $HTML.= '       </tr>';                        

                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center;height:20px; border-top: none; border-left: none; border-right: none;" colspan="4"><b>DATOS INFORMATIVOS</b></td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Maestria</b></td>';
                                    $HTML.= '           <td  style="border:none" colspan="3">';
                                    $HTML.=                 utf8_encode($Dato['MAESTRIA']);                         
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';         
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Asignatura</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                 utf8_encode($Dato['MATERIA']);                            
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; border-left:none; border-right:none; border-top:none" colspan="2"><b>HORAS DE APRENDIZAJE POR COMPONENTE</b></td>';
                                    $HTML.= '       </tr>';   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Modalidad de Estudio</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                 '';                  
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente de Docencia (CD)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                 $Dato['COMPONENTECD'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>COHORTE</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  $Params[1];
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente Práctico/Experimental (CPE)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECP'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Prerequisito Academico</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  utf8_encode($Dato['REQUISITO']);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Componente Autonomo (CA)</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECA'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Unidad de Organización Curricular</b></td>';
                                    $HTML.= '           <td style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                 utf8_encode($Dato['ORGCURRICULAR']);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Total Horas</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Dato['COMPONENTECD']+$Dato['COMPONENTECA']+$Dato['COMPONENTECP'];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Docente Responsable</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  $Params[2];
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Email</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  $Params[3];
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center; height:30px; border-left:none;border-right:none;border-top:none" colspan="4"><b>JUSTIFICACION</b></td>';
                                    $HTML.= '       </tr>';          
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="width:'.$this->Col1.';height:'.$this->Alto.'; border-left:none;border-right:none;border-top:none"><b>Objetivo General</b></td>';
                                    $HTML.= '           <td style="text-align: justify;border-left:none;border-right:none;border-top:none" colspan="3">';
                                    $HTML.=                 utf8_encode($Dato['OBGENERAL']);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                        
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="width:'.$this->Col1.';height:'.$this->Alto.'; border-left:none;border-right:none;border-top:none"><b>Objetivo Especifico</b></td>';
                                    $HTML.= '           <td style="text-align: justify; border-left:none;border-right:none;border-top:none" colspan="3">';
                                    $HTML.=                 utf8_encode($Dato['OBESPECIFICO']);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                          
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'; border:none"><b>Metodología</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'; border:none">';
                                    $HTML.=                  utf8_encode($Dato['METODOLOGIA']);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'; border:none"><b>Líneas de Investigación a la que Tributa</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'; border:none">';
                                    $HTML.=                  utf8_encode($Dato['LINEA']);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center;height:30px; border-left:none;border-right:none;border-top:none" colspan="4"><b>CONTENIDO</b></td>';
                                    $HTML.= '       </tr>';                                             
                                    $HTML.=         $this->rptSilaboContenido($Dato['ID']);    
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="font-size:10pt;text-align:center;height:30px;border-left:none;border-right:none;border-top:none" colspan="4"><b>BIBLIOGRAFIA</b></td>';
                                    $HTML.= '       </tr>';                     
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center;height:'.$this->Alto.'" colspan="2"><b>BASICA</b></td>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center;height:'.$this->Alto.'" colspan="2"><b>COMPLEMENTARIA</b></td>';
                                    $HTML.= '       </tr>';                     
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td valign="top" colspan="2" style="border:none">';
                                                            $Datos = $this->Works->SilaboLibros($Dato['ID'],'BAS');    
                                    $HTML.=                 $this->rptSilaboLibros($Datos);
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td valign="top" colspan="2" style="border:none">';
                                                            $Datos = $this->Works->SilaboLibros($Dato['ID'],'COM');    
                                    $HTML.=                 $this->rptSilaboLibros($Datos);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td valign="top" colspan="4" style="border:none">';
                                                            $Datos = $this->Works->SilaboLibros($Dato['ID'],'LNK');    
                                    $HTML.=                 $this->rptSilaboLibros($Datos);
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';
                                    
                                    $DateControl = new DateControl();
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Elaborado<br>Por Docente:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'">'.$Params[2].'</td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Revisado<br>Por Responsable Académico:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                    
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Aprobado<br>Por Decano:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                                   
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="width:'.$this->Col1.';height:'.$this->Alto.'"><b>Validado<br>Por Dirección de Posgrado:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col2.'"></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col3.'"><b>Firma:</b></td>';
                                    $HTML.= '           <td  style="width:'.$this->Col4.'"><b>Fecha Entrega:</b> '.$DateControl->getNowDateTime("FECHA").'</td>';
                                    $HTML.= '       </tr>';  
                                    $HTML.= '</table>';
                            }        
                            return $HTML;
                        }
                }
               
                private function rptSilaboContenido($ID)
                {       $ServicioMatContenido = new ServicioMatContenido();   
                        $root = json_decode($ServicioMatContenido->BuscarIndice($ID));
                        $HTML = "";
                        foreach($root as $sheet)
                        {       if ($sheet->estado!=0)
                                {   $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col1.';height:'.$this->Alto.'"><b>Unidad No. y Título</b></td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col2.'">';
                                    $HTML.=                  $sheet->orden.'.- '.$sheet->descripcion;
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col3.'"><b>Resultado de Aprendizaje</b></td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col4.'">';
                                    $HTML.=                  $sheet->resultado;
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';

                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col1.';height:'.$this->Alto.'"><b>Distributivo de Horas</b></td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col2.'">';
                                    $HTML.=                  '';
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col3.'"></td>';
                                    $HTML.= '           <td  style="border:none; width:'.$this->Col4.'">';
                                    $HTML.=                  '';
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';                                    
                                    
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center; width:'.$this->Col1.';height:'.$this->Alto.'"><b>CONTENIDO TEMATICO</b></td>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center; width:'.$this->Col2.'"><b>METODOS, TECNICAS E INSTRUMENTOS</b></td>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center; width:'.$this->Col3.'"><b>OBJETIVOS DE APRENDIZAJE</b></td>';
                                    $HTML.= '           <td style="border-bottom:none;border-left:none;border-right:none;border-top:none;text-align:center; width:'.$this->Col4.'"><b>EVALUACION DE LOS APRENDIZAJES</b></td>';
                                    $HTML.= '       </tr>';

                                    $cont = "";
                                    $meth = $sheet->metodo;
                                    $objt = $sheet->objetivo;
                                    if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                    {   $Datos = $this->rptSilaboContenidoSgte($sheet->hijos);
                                        $cont.= $Datos['descripcion'];
                                        $meth.= $Datos['metodo'];
                                        $objt.= $Datos['objetivo'];
                                    }
                                    $HTML.= '       <tr>';
                                    $HTML.= '           <td valign="top" style="text-align:justify;width:'.$this->Col1.';height:'.$this->Alto.'">';
                                    $HTML.=             $cont;
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td valign="top" style="text-align:justify;width:'.$this->Col2.'">';
                                    $HTML.=             $meth;
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td valign="top" style="text-align:justify;width:'.$this->Col3.'">';
                                    $HTML.=             $objt;
                                    $HTML.= '           </td>';
                                    $HTML.= '           <td valign="top" style="text-align:justify;width:'.$this->Col4.'">';
                                    $HTML.=                  '<b>CD:</b>'.$sheet->cd.'<br><b>CPE:</b>'.$sheet->cp.'<br><b>CA:</b>'.$sheet->ca;
                                    $HTML.= '           </td>';
                                    $HTML.= '       </tr>';  
                                }    
                        }
                        return $HTML;
                }    

                private function rptSilaboContenidoSgte($root)
                {       $descripcion = "";
                        $metodo = "";
                        $objetivo = "";       
                        foreach($root as $sheet)
                        {       if ($sheet->estado!=0)
                                {   if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                        $descripcion.= '<b>'.$sheet->orden.'-'.$sheet->descripcion.'</b>'.'<br>';
                                    else
                                        $descripcion.= $sheet->orden.'-'.$sheet->descripcion.'<br>';

                                    $metodo.= ($sheet->metodo!='' ? $sheet->orden.'-'.$sheet->metodo.'<br>':'');
                                    $objetivo.= ($sheet->objetivo!='' ? $sheet->orden.'-'.$sheet->objetivo.'<br>':''); 

                                    if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                    {   $Datos = $this->rptSilaboContenidoSgte($sheet->hijos);
                                        $descripcion.=$Datos['descripcion'].'<br>';
                                        $metodo.=$Datos['metodo'].'<br>';
                                        $objetivo.=$Datos['objetivo'].'<br>';
                                    }
                                }
                        }       
                        return array("descripcion"=>$descripcion,"metodo"=>$metodo,"objetivo"=>$objetivo);
                }*/
        }
?>

