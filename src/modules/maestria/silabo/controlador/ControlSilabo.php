<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");    
        require_once("src/rules/maestria/servicio/ServicioMatInformacion.php");
        require_once("src/rules/maestria/servicio/ServicioDocente.php");  
        require_once("src/rules/maestria/servicio/ServicioCiclo.php");
        require_once("src/rules/maestria/servicio/ServicioMaestria.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");
        require_once("src/rules/maestria/servicio/ServicioGrupo.php");
        require_once("src/rules/maestria/servicio/ServicioSilabo.php");
        require_once("src/modules/maestria/silabo/render/RenderSilabo.php");  
        require_once("src/rules/maestria/servicio/ServicioMatContenido.php");

        class ControlSilabo
        {       private $ServicioSilabo;
                private $ServicioDocente;
                private $ServicioCiclo;
                private $RenderSilabo;
                private $ServicioMaestria;
                private $ServicioMatInformacion;
                private $ServicioUsuarioFiltro;
                private $ServicioGrupo;
                
                function __construct()
                {       $this->ServicioSilabo = new ServicioSilabo();
                        $this->ServicioDocente = new ServicioDocente();     
                        $this->RenderSilabo = new RenderSilabo();
                        $this->ServicioCiclo = new ServicioCiclo(); 
                        $this->ServicioMaestria = new ServicioMaestria();
                        $this->ServicioMatInformacion = new ServicioMatInformacion();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                        $this->ServicioGrupo = new ServicioGrupo();
                }

                function CargaSilaboBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderSilabo->CreaBarButton($Buttons[1]);
                }
               
                function CargaSilaboMantenimiento()
                {       echo $this->RenderSilabo->CreaSilaboMantenimiento();
                   
                }
               
                private function FilterOfAccess($Params)
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        $Factad = $Params['fac'];
                        if ($Modulo->Idrol==3)
                        {   if ($Factad[0]=="00")
                                $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            return array('texto'=>$Params['txt'], 'facultad'=>$Factad);
                        }    
                        else if($Modulo->Idrol==2)
                        {    if ($Factad[0]=="00")
                                 $Factad = $this->ServicioUsuarioFiltro->BuscarDocenteFiltroByCedula($Modulo->Cedula);
                             return array('texto'=>$Params['txt'], 'facultad'=>$Factad, 'docente'=>$Modulo->Cedula);
                        }
                        else
                        return array('facultad'=>$Factad); //No permiso a Estudiante
                }                
                
                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();    
                      
                        if ($Operacion=="btmaestria")    
                        {   $Params = array('txt'=>'','fac'=>array('00'));
                            $ArDatos = $this->ServicioMatInformacion->BuscarMaestriaVersionByFacultadSinCohorte($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderSilabo->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderSilabo->GetGridTableWidth();
                        }         
                        elseif ($Operacion==="btciclo")    
                        {   $ArDatos = $this->ServicioCiclo->BuscarCicloByMaestriaVersion($IdRef);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Ciclo-Cohorte";
                            $jsonModal['Carga'] = $this->RenderSilabo->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderSilabo->GetGridTableWidth();
                        }   
                        elseif ($Operacion==="btmatcont")    
                        {   $prepareDQL = array('maesver'=>$IdRef[0],'ciclo'=>$IdRef[1]);
                            $ArDatos = $this->ServicioMatInformacion->BuscarMateriaContenidoByMalla($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Materia Malla";
                            $jsonModal['Carga'] = $this->RenderSilabo->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderSilabo->GetGridTableWidth();
                        }   
                        else if ($Operacion=="btdocente")
                        {   $Params = array('txt'=>'','fac'=>array($IdRef));
                            $ArDatos = $this->ServicioDocente->BuscarDocente($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Docente";
                            $jsonModal['Carga'] = $this->RenderSilabo->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderSilabo->GetGridTableWidth();
                        }
                        elseif ($Operacion==="btgrupo")    
                        {   $ArDatos = $this->ServicioGrupo->BuscarGrupoByCiclo($IdRef);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Grupo Ciclo/Cohorte";
                            $jsonModal['Carga'] = $this->RenderSilabo->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderSilabo->GetGridTableWidth();
                        }                          
                        else if ($Operacion=="addPdf")
                        {   $Utils = new Utils();
                            if ($IdRef[10]==1)
                            {   $urlfl = $Utils->Encriptar("src/modules/maestria/silabo/vista/rptSilabo.php");
                                $title = "Silabo";
                            }
                            else
                            {   $urlfl = $Utils->Encriptar("src/modules/maestria/silabo/vista/rptPlanClase.php");
                                $title = "Plan Analitico";
                            }
                            $optns = $Utils->Encriptar($IdRef[0]."|".$IdRef[1]."|".htmlentities($IdRef[2])."|".$IdRef[3]."|".htmlentities($IdRef[4])."|".$IdRef[5]."|".$IdRef[6]."|".$IdRef[7]."|".$IdRef[8]."|".$IdRef[9]."|".$IdRef[11]); 
                            
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = $title;
                            $jsonModal['Carga'] = "<iframe name='Modal".strtoupper($Operacion)."' src='index.php?url=".$urlfl."&opcion=".$optns."' height='465px' width='100%' scrolling='si' frameborder='0' marginheight='0' marginwidth='0'></iframe>";    //$this->RenderSilabo->rptSilabo($IdRef);
                            $jsonModal['Ancho'] = "900";
                            $jsonModal['Alto']  = "500";
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion==="btmaestria")
                        {   $Params = array('txt'=>strtoupper(trim($Texto)),'fac'=>array("00"));
                            $Datos = $this->ServicioMatInformacion->BuscarMaestriaVersionByFacultadSinCohorte($this->FilterOfAccess($Params));
                            return $this->RenderSilabo->MuestraModalGrid($ajaxResp,$Operacion,$Datos);        
                        }
                        if ($Operacion==="btdocente")    
                        {   $Params = array('txt'=>strtoupper(trim($Texto[1])),'fac'=>array($Texto[0]));
                            $Datos = $this->ServicioDocente->BuscarDocente($this->FilterOfAccess($Params));
                            return $this->RenderSilabo->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                        }
                        if ($Operacion==="btmatcont")
                        {   $prepareDQL = array('maesver'=>$Texto[0],'ciclo'=>$Texto[1],'texto'=> strtoupper(trim($Texto[2])));
                            $Datos = $this->ServicioMatInformacion->BuscarMateriaContenidoByMalla($prepareDQL);
                            return $this->RenderSilabo->MuestraModalGrid($ajaxResp,$Operacion,$Datos);
                        }
                }
        }

?>

