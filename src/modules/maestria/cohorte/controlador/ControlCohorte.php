<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioCohorte.php");
        require_once("src/modules/maestria/Cohorte/render/RenderCohorte.php");

        class ControlCohorte
        {       private  $ServicioCohorte;
                private  $RenderCohorte;

                function __construct()
                {       $this->ServicioCohorte = new ServicioCohorte();
                        $this->RenderCohorte = new RenderCohorte();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderCohorte->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderCohorte->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => 'NULL');
                        $Datos = $this->ServicioCohorte->BuscarCohorteByDescripcion($prepareDQL);
                        echo $this->RenderCohorte->CreaSearchGrid($Datos);
                }         
               
                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioCohorte->BuscarCohorteByID($id);
                        return $this->RenderCohorte->MuestraFormulario($ajaxRespon,$Datos);
                }
                
                function ConsultaByIDByVersion($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('id' => ($JsDatos -> id), 'idVersion' => $JsDatos->idversion);
                        $ArDatos = $this->ServicioCohorte->BuscarCohorteByIDByVersion($prepareDQL);
                        return $this->RenderCohorte->MuestraFormulario($ajaxRespon,$ArDatos);
                }

                function ConsultaByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();              
                        $prepareDQL = array('texto' => strtoupper(trim($Texto)));
                        $ArDatos = $this->ServicioCohorte->BuscarCohorteByDescripcion($prepareDQL);
                        return $this->RenderCohorte->MuestraGrid($ajaxRespon,$ArDatos);
                }
                
                function ConsultaByTXByVersion($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('texto' => strtoupper(trim($JsDatos -> FindTextBx)), 'idversion' => $JsDatos->idversion );
                        $ArDatos = $this->ServicioCohorte->BuscarCohorteByDescripcionByVersion($prepareDQL);
                        return $this->RenderCohorte->MuestraGrid($ajaxRespon,$ArDatos);
                }

                function Guarda($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");
                        $CohortesExistentes = $this->ServicioCohorte->BuscarCohortesByIdVersion($JsDatos->idversion);
                        $ArD = $this->ServicioCohorte->BuscarCohortesDeVersionByIdVersion($JsDatos->idversion);
                        $CohortesDeVersion = $ArD->COHORTES;
                        
                    
                        if(empty($JsDatos->id)){
                            $FechasValidadas = $this->ValidarFechas($JsDatos->feinicio, $JsDatos->idversion);
                            if($FechasValidadas==="true"){
                                $FechaDuracionCohorte = $this->ValidarDuracionCohorte($JsDatos->feinicio, $JsDatos->fefin);
                                if($FechaDuracionCohorte == "true"){
                                        if($CohortesExistentes >= $CohortesDeVersion)
                                        { $jsFuncion = "XAJAXResponse";
                                            $ajaxRespon->call($jsFuncion,"CMD_WRM","La cantidad de cohortes esta completa. Cree una nueva Version.");
                                            return $ajaxRespon;}
                                        else{$id = $this->ServicioCohorte->GuardaDBCohorte($JsDatos);
                                            $ArDatos = $this->ServicioCohorte->BuscarCohorteEstadoByID($id);
                                            return $this->RenderCohorte->{$funcion}($ajaxRespon,$ArDatos);}
                                }else{  $jsFuncion = "XAJAXResponse";
                                        $ajaxRespon->call($jsFuncion,"CMD_ERR","La Duración MÍNIMA de un Cohorte debe ser de 18 Meses.");
                                        return $ajaxRespon;}
                            }else{  $jsFuncion = "XAJAXResponse";
                                    $ajaxRespon->call($jsFuncion,"CMD_ERR","El AÑO debe ser mayor al año del ULTIMO cohorte.");
                                    return $ajaxRespon;} 
                        }else{$id = $this->ServicioCohorte->GuardaDBCohorte($JsDatos);
                                $ArDatos = $this->ServicioCohorte->BuscarCohorteEstadoByID($id);
                                return $this->RenderCohorte->{$funcion}($ajaxRespon,$ArDatos);
                        } 
                }
                
                function ValidarFechas($FeInicio,$idVersion)
                {   $FechaInicioComoEntero = strtotime($FeInicio);
                    $FechaInicioIngresada = date("Y", $FechaInicioComoEntero);
                    $ArD = $this->ServicioCohorte->BuscarFechaFinUltimoCohorte($idVersion);
                    if($ArD)
                    {   $FechaInicioUltima = $ArD->FEINICIO;
                        $FechasValidadas = ($FechaInicioIngresada>$FechaInicioUltima ? "true":"false");
                        return $FechasValidadas;  
                    }
                    else {  return "true";  }
                }
                
                function ValidarDuracionCohorte($FeInicio, $FeFin)
                {   $FechaFinMinimo = date("Y-m-d",strtotime($FeInicio."+ 18 month"));
                    if($FeFin >= $FechaFinMinimo)
                    {  return "true";    }
                    else{   return "false";  } 
                }

                function Elimina($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsData = json_decode($Form)->Forma;
                        $JsData->estado = $this->ServicioCohorte->DesactivaCohorte($JsData->id);
                        return $this->RenderCohorte->MuestraEliminado($ajaxRespon,$JsData);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1));
                        if ($Operacion==="btversion")    
                        {                            
                            $ArDatos = $this->ServicioCohorte->BuscarVersionByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Versiones";
                            $jsonModal['Carga'] = $this->RenderCohorte->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "750";
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function CargaModalGridMalla($Operacion,$Form)
                {       $ajaxResp = new xajaxResponse();  
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1), 'idversion' => $JsDatos->idversion);
                        if ($Operacion==="btmalla")    
                        {                            
                            $ArDatos = $this->ServicioCohorte->BuscarMallaByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Versiones";
                            $jsonModal['Carga'] = $this->RenderCohorte->CreaModalGridMalla($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "520";
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion==="btversion")    
                        {   $texto = trim($Texto);
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                            $Datos = $this->ServicioCohorte->BuscarVersionByDescripcion($prepareDQL);
                            return $this->RenderCohorte->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                        }    
                        if ($Operacion==="btmalla")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $Params = array('txt'=>$Texto[0],'idv'=>$Texto[1]);
                            $Datos = $this->ServicioCohorte->BuscarMallaByVersionByDescripcion($Params);
                            return $this->RenderCohorte->MuestraModalGridMalla($ajaxResp,$Operacion,$Datos);                                        
                        }  
                }
                
                function MostrarCohortebyVersion($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->txversion) ? "MuestrabyVersion" : "MuestraEditadobyVersion");
                        $id = $JsDatos->idversion;
                        $ArDatos = $this->ServicioCohorte->BuscarCohorteEstadoByVersion($id);
                        return $this->RenderCohorte->{$funcion}($ajaxRespon,$ArDatos);
                }
        }       
?>