<?php   $Opcion = 'Cohorte';

        require_once('src/modules/maestria/cohorte/controlador/ControlCohorte.php');
        $ControlCohorte = new ControlCohorte();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlCohorte,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlCohorte,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlCohorte,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlCohorte,'ConsultaByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlCohorte,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModalMalla', $ControlCohorte,'CargaModalGridMalla'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlCohorte,'ConsultaModalGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('MostrarbyVersion', $ControlCohorte,'MostrarCohortebyVersion'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTXByVersion', $ControlCohorte,'ConsultaByTXByVersion'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByIDByVersion', $ControlCohorte,'ConsultaByIDByVersion'));
        
        $xajax->processRequest();       
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
            
            var objCohorte = "id:descripcion:estado:txversion:idversion";
            var valCohorte = "txversion:descripcion";
                
                function ButtonClick(Operacion)
                { 
                        var objCohorte = "id:descripcion:estado:txversion:idversion:idmalla:txmalla:idmodalidad:feinicio:fefin";
                        var valCohorte = "txversion:idversion:idmalla:txmalla:idmodalidad:descripcion:feinicio:fefin";
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementStatus("descripcion:txmalla:btmalla:idmodalidad:feinicio:fefin","id:txversion:btversion:estado");
                            ElementClear("id:descripcion:txmalla:feinicio:fefin");
                            ElementSetValue("idmodalidad",1);
                            ElementSetValue("estado",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Operacion);
                                if (ElementGetValue("estado")==0)
                                {   BarButtonStateEnabled("btversion");
                                    valCohorte = valCohorte + ":estado";
                                }
                                valCohorte = valCohorte + ":btmalla";
                                ElementStatus(valCohorte,"id:txversion:btversion:feinicio");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue("estado")==1)
                                {   Swal.fire({
                                        title: 'Desactivar registro?',
                                        text: "Esta seguro que desea desactivar el registro?",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Si, estoy seguro!'
                                    }).then(function(result){
                                        if (result.value) {
                                            var Forma = PrepareElements("id:estado");
                                            xajax_Elimina(JSON.stringify({Forma}));
                                        }
                                    });  
                                }
                                else{SAlert("warning","Registro Inactivo","El registro se encuentra inactivo");}
                        }
                        else if (Operacion==='addSav')
                        {   if (ElementGetValue("feinicio")<ElementGetValue("fefin"))    
                            {   if (ElementValidateBeforeSave(valCohorte))
                                {   if (BarButtonState("Inactive"))
                                    {   var Forma = PrepareElements(objCohorte);
                                        xajax_Guarda(JSON.stringify({Forma}));
                                    }
                                }
                                else
                                {SAlert("warning","¡Cuidado!","Llene los campos por favor");}
                            }else{SAlert("error", "¡Error en fechas!", "La Fecha Inicial debe ser menor a la Fecha Fin");}
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Operacion);
                                ElementStatus("feinicio:fefin","");
                                ElementClear("id:descripcion:txmalla:feinicio:fefin");
                                ElementStatus("id:txversion:btversion","descripcion:estado:idmodalidad:txmalla:btmalla:feinicio:fefin");
                                ElementSetValue("estado",1);
                                ElementSetValue("idmodalidad","");
                        }
                        else if (Operacion==='btmalla')
                        {   var Forma = PrepareElements("idversion");
                            xajax_CargaModalMalla(Operacion, JSON.stringify({Forma}));
                        }
                        else 
                        {xajax_CargaModal(Operacion);}
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Default");
                            ElementSetValue("id",Datos); 
                            ElementStatus("feinicio:fefin","");
                            ElementClear("id:txmalla:descripcion:feinicio:fefin");
                            ElementStatus("id:txversion:btversion","descripcion:estado:idmodalidad:txmalla:btmalla:feinicio:fefin");
                            ElementSetValue("estado",1);
                            ElementSetValue("idmodalidad","");
                            SAlert("success","¡Excelente!","Los datos se guardaron correctamente.");
                        }
                        else if (Mensaje==="CMD_MAE")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                                BarButtonState("Default");
                                ElementStatus("feinicio:fefin","");
                                ElementClear("id:descripcion:txmalla:feinicio:fefin");
                                ElementStatus("id:txversion:btversion","descripcion:estado:txmalla:btmalla:feinicio:fefin");                    
                                ElementSetValue("estado",1);
                                ElementSetValue("idmodalidad","");
                            }
                            else if(ValidateStatusEnabled("addSave")){
                                BarButtonState("Active");
                                ElementStatus("feinicio:fefin","");
                                ElementClear("id:descripcion:txmalla:feinicio:fefin");
                                ElementStatus("descripcion","id:estado:txversion");
                            }
                            else {
                                BarButtonState("Default");
                                ElementStatus("feinicio:fefin","");
                                ElementClear("id:descripcion:txmalla:feinicio:fefin");
                                ElementStatus("id:txversion:btversion","descripcion:estado:txmalla:btmalla:feinicio:fefin");
                                ElementSetValue("estado",1);
                                ElementSetValue("idmodalidad","");
                            }
                        }
                        else if (Mensaje==="CMD_MAE_ERR")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                            }
                            else {
                                ElementStatus("descripcion","id:estado:txversion");
                            }
                        }
                        else if(Mensaje==='CMD_ERR')
                        {   BarButtonState("addNew");
                            SAlert("warnning", "¡Cuidado!", Datos); 
                        }    
                        else if(Mensaje==='CMD_DEL')
                        {   ElementSetValue("estado",Datos);
                            SAlert("success", "¡Excelente!", "Datos guardados correctamente");
                        }
                        else if(Mensaje==="CMD_WRM")
                        {   BarButtonState("Default");
                            SAlert("warning", "¡Cuidado!", Datos); 
                            ElementStatus("feinicio:fefin","");
                            ElementClear("id:descripcion:txmalla:feinicio:fefin");
                            ElementStatus("id:txversion:btversion","descripcion:estado:txmalla:btmalla:feinicio:fefin");
                            ElementSetValue("estado",1);
                        }
                }
                
                function SearchByElement(Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id")
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion");
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState("Inactive"))
                                    {
                                        var Forma = PrepareElements("id:idversion");
                                        xajax_BuscaByIDByVersion(JSON.stringify({Forma}));
                                    }
                                }
                            }
                            else
                            BarButtonState("Default"); 
                        }
                        else if (Elemento.id === "FindVersionTextBx")
                        {   xajax_BuscaModalByTX("btversion",Elemento.value);
                        } 
                        else if (Elemento.id === "FindMallaTextBx")
                        {   var Params = [Elemento.value,ElementGetValue("idversion")];
                            xajax_BuscaModalByTX("btmalla",Params);
                        }
                        else if(Elemento.id === "FindTextBx")
                        {   var Forma = PrepareElements("FindTextBx:idversion");
                            xajax_BuscaByTXByVersion(JSON.stringify({Forma}));
                        }
                }
                
                function SearchVersionGetData(DatosGrid)
                {       document.getElementById("idversion").value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        document.getElementById("txversion").value = DatosGrid.cells[1].childNodes[0].nodeValue.toUpperCase() + (' ')+ DatosGrid.cells[3].childNodes[0].nodeValue.toUpperCase();
                        cerrar();
                        ElementStatus("feinicio:fefin","");
                        ElementClear("id:descripcion:txmalla:feinicio:fefin");
                        ElementStatus("id:txversion:btversion","descripcion:estado:txmalla:btmalla:feinicio:fefin");
                        var Forma = PrepareElements(objCohorte);
                        xajax_MostrarbyVersion(JSON.stringify({Forma}));
                        return false;
                }
                
                function SearchMallaGetData(DatosGrid)
                {
                        document.getElementById("idmalla").value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txmalla").value = DatosGrid.cells[1].childNodes[0].nodeValue.toUpperCase() + (' ')+ DatosGrid.cells[3].childNodes[0].nodeValue.toUpperCase();
                        cerrar();
                        return false;
                }
                 
                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("idversion").value = Grilla.cells[1].childNodes[0].nodeValue; 
                            document.getElementById("txversion").value = Grilla.cells[4].childNodes[0].nodeValue.toUpperCase() + (' ') + Grilla.cells[2].childNodes[0].nodeValue.toUpperCase();
                            document.getElementById("idmalla").value = Grilla.cells[5].childNodes[0].nodeValue;
                            document.getElementById("txmalla").value = Grilla.cells[6].childNodes[0].nodeValue.toUpperCase(); 
                            document.getElementById("idmodalidad").value = Grilla.cells[7].childNodes[0].nodeValue;
                            document.getElementById("descripcion").value = Grilla.cells[9].childNodes[0].nodeValue.toUpperCase();
                            
                            var feinicio_entrada = Grilla.cells[10].childNodes[0].nodeValue;
                            var feinicio_format = formato(feinicio_entrada);
                            document.getElementById("feinicio").value = feinicio_format;
                            
                            var fefin_entrada = Grilla.cells[11].childNodes[0].nodeValue;
                            var fefin_format = formato(fefin_entrada);
                            document.getElementById("fefin").value = fefin_format;
                            
                            document.getElementById("estado").value = Grilla.cells[12].childNodes[0].nodeValue;
                        }
                        return false;
                }
                 
                function formato(string) {
                    var info = string.split('/').reverse().join('-');
                    return info;
                }
                 
        </script>
    </head>
        <body>
            
        <div class="FormBasic" style="width:720px">
            <div class="FormSectionMenu">              
            <?php   $ControlCohorte->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState("Inactive"); ';
                    echo '</script>';                    
            ?>    
            </div>  
            
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                    <?php  $ControlCohorte->CargaMantenimiento();  ?>
                    </form>
            </div> 
            
            <div class="FormSectionGrid">          
            <?php   $ControlCohorte->CargaSearchGrid();  ?>
            </div>  
            
        </div>
        </body>
    </html>
         