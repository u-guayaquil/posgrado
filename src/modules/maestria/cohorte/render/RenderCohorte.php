<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderCohorte
        {   private $SearchGrid;
            private $Maxlen;    
            
            function __construct()
            {       $this->Maxlen['id'] = 4;        
                    $this->Maxlen['descripcion'] = 50;
                    $this->Maxlen['fecha'] = 10; 
                    $this->SearchGrid = new SearchGrid();
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']          = array('40px','center','');
                    $Columns['IdVersion']   = array('0px','center','none');
                    $Columns['Version']     = array('80px','center','');
                    $Columns['IdMaestria']  = array('0px','center','none');
                    $Columns['Maestria']    = array('0px','center','none');
                    $Columns['IdMalla']     = array('0px','center','none');
                    $Columns['Malla']       = array('110px','left','');
                    $Columns['IdModalidad'] = array('0px','center','none');
                    $Columns['Modalidad']   = array('80px','left','');
                    $Columns['Cohorte']     = array('110px','left','');        
                    $Columns['Inicio']      = array('80px','left','');
                    $Columns['Fin']         = array('80px','left','');
                    $Columns['IdEstado']    = array('0px','center','none');
                    $Columns['Estado']      = array('80px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }
               
            function CreaMantenimiento()
            {      $Search = new SearchInput();
                    $HTML = '<table border=0 class="Form-Frame" cellpadding="0">';                 
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label">Maestría</td>';
                    $HTML.= '           <td class="Form-Label" colspan="3">';
                    $HTML.= $Search->TextSch("version","","")->Enabled(true)->Create("t16");
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label">Malla</td>';
                    $HTML.= '           <td class="Form-Label" colspan="3">';
                    $HTML.= $Search->TextSch("malla","","")->Enabled(false)->Create("t08");
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label">Modalidad</td>';
                    $HTML.= '           <td class="Form-Label" colspan="3">';
                    $HTML.= '               <select class="sel-input s05" id="idmodalidad" name="idmodalidad" onchange=" return ControlIdModalidad(this)" disabled>';
                    $HTML.= '                       <option value="" selected="true" disabled>SELECCIONE</option>';
                    $HTML.= '                       <option value="1">MODULO</option>';
                    $HTML.= '                       <option value="2">SEMESTRE</option>';     
                    $HTML.= '               </select>';    
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">Id</td>';
                    $HTML.= '             <td class="Form-Label" style="width:80%">';
                    $HTML.= '                <input type="text" class="txt-input t01" id="id" name="id" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\'descripcion\',\'NUM\'); " onBlur="return SearchByElement(this);" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';                   
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Cohorte</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                 <input type="text" class="txt-upper t05" id="descripcion" name="descripcion" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">Fecha Inicio</td>';
                    $HTML.= '             <td class="Form-Label" style="width:60%">';
                    $HTML.= '                <input type="date" class="txt-input t05" id="feinicio" name="feinicio" value="" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>'; 
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">Fecha Fin</td>';
                    $HTML.= '             <td class="Form-Label" style="width:60%">';
                    $HTML.= '                <input type="date" class="txt-input t05" id="fefin" name="fefin" value="" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>'; 
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Estado</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= $this->CreaComboEstadoByArray(array(0,1));
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function CreaBarButton($Buttons)
            {       $BarButton = new BarButton();    
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGrid($ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    return $this->SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
            
            private function GridDataHTML($Grid,$ArDatos)
            {       if ($ArDatos[0])
                    {   foreach ($ArDatos[1] as $ArDato)
                        {       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);                       
                                $Grid->CreaSearchCellsDetalle($id,'',$id);                
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDVERSION']);
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['VERSION']))); 
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMAESTRIA']);
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMALLA']);
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MALLA']))); 
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMODALIDA']);
                                $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDMODALIDA']==1 ? "MODULO" : "SEMESTRE"));
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['COHORTE'])));
                                $FeInicioBD = $ArDato['INICIO'];
                                $Grid->CreaSearchCellsDetalle($id,'',$FeInicioBD->format('d/m/Y'));
                                $FeFinBD = $ArDato['FIN'];
                                $Grid->CreaSearchCellsDetalle($id,'',$FeFinBD->format('d/m/Y'));
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
                                $Grid->CreaSearchRowsDetalle ($id,($ArDato['IDESTADO']==0 ? "red" : ""));
                        }
                    }    
                    return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
            }
               
            function MuestraFormulario($Ajax,$Cohorte)
            {   if ($Cohorte)
                    {   if ($Cohorte->ID>=0)
                        {   $Ajax->Assign("id","value", str_pad($Cohorte->ID,$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("idversion","value", $Cohorte->IDVERSION);
                            $Ajax->Assign("txmalla","value", utf8_encode(trim($Cohorte->MALLA)));
                            $Ajax->Assign("idmalla","value", $Cohorte->IDMALLA);
                            $Ajax->Assign("idmodalidad","value", $Cohorte->IDMODALIDA);
                            $Ajax->Assign("descripcion","value", utf8_encode(trim($Cohorte->COHORTE)));
                            $FeInicioBD = $Cohorte->INICIO;
                            $Ajax->Assign("feinicio","value", $FeInicioBD->format('Y-m-d'));
                            $FeFinBD = $Cohorte->FIN;
                            $Ajax->Assign("fefin","value", $FeFinBD->format('Y-m-d'));
                            $Ajax->Assign("estado","value", $Cohorte->IDESTADO);
                            $Ajax->call("BarButtonState","Active");
                            return $Ajax;
                        }    
                    }
                    return $this->Respuesta($Ajax,"CMD_WRM","No existe una Cohorte con el ID ingresado.");
            }
            
            function MuestraGuardado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);
            }
            
            function MuestraGrid($Ajax,$ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    $Ajax->Assign($this->SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }
            
            function MuestraEditado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraRowGrid($Ajax, $ArDatos[1][0]);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }    
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);            
            }
    
            function MuestraRowGrid($Ajax,$ArDato)
            {       $RowId = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$ArDato['IDESTADO']);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",$ArDato['IDVERSION']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",utf8_encode(trim($ArDato['VERSION'])));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$ArDato['IDMAESTRIA']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",utf8_encode(trim($ArDato['MAESTRIA'])));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$ArDato['IDMALLA']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",utf8_encode(trim($ArDato['MALLA'])));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$ArDato['IDMODALIDA']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",($ArDato['IDMODALIDA']==1 ? "MODULO" : "SEMESTRE"));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",utf8_encode(trim($ArDato['COHORTE'])));
                    $FeInicioBD = $ArDato['INICIO'];
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",$FeInicioBD->format('d/m/Y'));
                    $FeFinBD = $ArDato['FIN'];
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],11),"innerHTML",$FeFinBD->format('d/m/Y'));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],12),"innerHTML",$ArDato['IDESTADO']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],13),"innerHTML",($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
                    return $Ajax;
            }                                
            
            function MuestraEliminado($Ajax,$JsDatos)
            {       $RowId = str_pad($JsDatos->id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$JsDatos->estado);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);       
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],12),"innerHTML",$JsDatos->estado); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],13),"innerHTML","INACTIVO"); 
                    return $this->Respuesta($Ajax,"CMD_DEL",0);
            }

            private function Respuesta($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse";
                    $Ajax->call($jsFuncion,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox();
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Enabled(false)->Fields("id","descripcion")->Create("s05");
            }   
             
            
            function MuestraEditadobyVersion($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$id);
                    }
                else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_MAE_ERR",$ArDatos[1]);            
            }
            
            function MuestrabyVersion($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$id);
                    }
                    else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_MAE_ERR",$ArDatos[1]);            
            }
            
            /*** MODAL ***/
                private function SearchGridModalConfig()
                {       
                        $Columns['IdMaestria'] = array('0px','center','none');
                        $Columns['Maestria'] = array('310px','left','');
                        $Columns['IdVersion'] = array('0px','center','none');
                        $Columns['Version'] = array('150px','center','');
                        $Columns['Cohortes'] = array('0px','left','none');
                        $Columns['IdFacultad'] = array('0px','center','none');
                        $Columns['Facultad']  = array('260px','left','');
                        
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$ArDatos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcion($SearchGrid,$ArDatos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcion($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalOpcion($Grid,$ArDatos)
                {       if ($ArDatos[0])
                        {   foreach ($ArDatos[1] as $ArDato)
                            {       
                                    $id = str_pad($ArDato['IDMAESTRIA'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                    $Grid->CreaSearchCellsDetalle($id,'',$id);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDVERSION']);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['VERSION'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['COHORTES']);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDFACULTAD']);
                                    $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['FACULTAD']));
                                       
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }
                
                /*** MODAL MALLA***/
                private function SearchGridModalConfigMalla()
                {       
                        $Columns['Id']          = array('40px','center','');
                        $Columns['Malla']       = array('125px','left','');
                        $Columns['IdTipoMalla'] = array('0px','center','none');
                        $Columns['TipoMalla']   = array('100px','center','');
                        $Columns['Ciclos']      = array('50px','center','');
                        $Columns['Fecha']       = array('90px','center','');
                        $Columns['IdEstado']    = array('0px','center','none');
                        $Columns['Estado']      = array('80px','left','');
                        
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGridMalla($Operacion,$ArDatos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfigMalla());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcionMalla($SearchGrid,$ArDatos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGridMalla($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfigMalla());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcionMalla($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalOpcionMalla($Grid,$ArDatos)
                {       if ($ArDatos[0])
                        {   foreach ($ArDatos[1] as $ArDato)
                            {       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                    $Grid->CreaSearchCellsDetalle($id,'',$id);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MALLA'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDTIPOMALLA']);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['TIPOMALLA'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CICLOS']);
                                    $FechaBD = $ArDato['FECHA'];
                                    $Grid->CreaSearchCellsDetalle($id,'',$FechaBD->format('d/m/Y'));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                    $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));  
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }     
        }
?>

