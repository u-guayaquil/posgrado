<?php
        $Opcion = 'Estudiante';
        require_once('src/modules/maestria/estudiante/controlador/ControlEstudiante.php');
        $ControlEstudiante = new ControlEstudiante();
        
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlEstudiante,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaParalelosByCohorte', $ControlEstudiante,'ConsultaParalelosByCohorte'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlEstudiante,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlEstudiante,'ConsultaGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('ActaEstudiante',$ControlEstudiante,'ActaEstudiante'));
        $xajax->processRequest();
        
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/maestria/estudiante/js/jsEstudiante.js"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:705px">
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionData">   
                             <?php  $ControlEstudiante->CargaMantenimiento();  ?>
                        </div> 
                        <div id="<?php echo 'WrkGrd'; ?>" class="FormSectionGrid">  
                        </div>     
                    </form>
                </div>       
        </body>
        </html>
              

