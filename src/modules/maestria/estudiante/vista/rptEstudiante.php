<?php   require_once("src/rules/maestria/servicio/ServicioEstudiante.php");
        require_once("src/rules/maestria/servicio/ServicioReporte.php");
        require_once("src/libs/plugins/pdf/vendor/mpdf/mpdf/mpdf.php"); 
        
        class rptEstudiante
        {
                private $Cols;
              
                private function CabsCSS()
                {       $Style = 'style = "background-color: #FFF;
                                  border-collapse:collapse; 
                                  border:0px solid #ccc;
                                  font-family:tahoma; 
                                  font-size:9px; 
                                  width:100%"';    
                        return  $Style;          
                }
                
                private function ColcCSS()
                {       $Style = 'style="text-align:center;
                                  border:1px solid #ccc;
                                  font-weight:bold;
                                  font-size:12px;
                                  height:30px"';    
                        return  $Style;          
                }

                private function ColdCSS($align)
                {       $Style = 'style="text-align:'.$align.';
                                  border:1px solid #ccc;
                                  font-size:12px;
                                  height:23px"';    
                        return  $Style;          
                }

                function CrearReporte($Params,$Estudiante)
                {       $ServicioReporte = new ServicioReporte();
                        $Inform = $ServicioReporte->DatosCabeceraReporteEstudiante($Params);
                        if ($Inform[0])
                        {   $HTMI = '<table border="2" style="font-family:tahoma;font-size:8px; border-collapse:collapse">';
                            $HDET = $this->AsistenciaActaColumna(); 
                            $HDET.= $this->AsistenciaActaDetalle($Estudiante);
                            $HDET.= $this->AsistenciaActaPiePagina($Inform[1][0]['DOCENTE']);
                            $HTMF = '</table>';
                            $HDET = $HTMI.$HDET.$HTMF;
                            return $this->AsistenciaActaCuerpo($Inform[1],'').$HDET;
                        }
                        return $Inform[1];
                }
                
                private function AsistenciaActaCuerpo($Inform,$HDET)
                {       $Dato = $Inform[0];
                        $HTML =     '<table border="0" '.$this->CabsCSS().'>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:10%;text-align:left" rowspan="5">';
                        $HTML.=             '<img src="src/public/img/logo_ug_small.png" width="57px" height="67px">';
                        $HTML.=         '</td>';
                        $HTML.=         '<td style="width:90%;text-align:center" colspan="3"><b>UNIVERSIDAD DE GUAYAQUIL</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>FACULTA DE '.$Dato['FACULTAD'].'</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>COORDINACION DE POSGRADO</b></td>';
                        $HTML.=     '</tr>';    
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>MAESTRIA EN '.utf8_encode($Dato['MAESTRIA']).'</b></td>';
                        $HTML.=     '</tr>';    
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>ACTA DE ESTUDIANTE - SISTEMA '.$Dato['TIPOMALLA'].'</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        //$HTML.=         '<td style="width:10%;"><b>ASIGNATURA</b></td>'; 
                        //$HTML.=         '<td style="width:40%;">'.utf8_encode($Dato['MATERIA']).'</td>';
                        //$HTML.=         '<td style="width:10%;"><b>PROFESOR</b></td>'; 
                        //$HTML.=         '<td style="width:40%;">'.utf8_encode($Dato['DOCENTE']).'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:10%;"><b>GRUPO</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['PARALELO'].'</td>';
                        $HTML.=         '<td style="width:10%;"><b>COHORTE</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['COHORTE'].'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:10%;"><b>DESDE</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['DESDE']->format('d/m/Y').'</td>';
                        $HTML.=         '<td style="width:10%;"><b>HASTA</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['HASTA']->format('d/m/Y').'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr >';
                        $HTML.=         '<td colspan="4">'.$HDET.'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '</table>';
                        return $HTML;
                }   

                private function AsistenciaActaPiePagina($Docente)
                {       $DateControl = new DateControl();
                        $HTML = '<tr>';
                        $HTML.= '<td style="border:none" colspan="20">';
                        $HTML.=     '<table border="0" '.$this->CabsCSS().'>';
                        $HTML.=     '<tr><td height="80px" colspan="4"></td></tr>';
                        $HTML.=     '<tr>';
                      //$HTML.=         '<td height="50px" style="width:3%;text-align:center"></td>';
                        $HTML.=         '<td height="50px" style="width:29%;text-align:center"><br>'.$Docente.'<br><b>Firma del Docente</b><br>Fecha: '.$DateControl->getNowDateTime("FECHA").'</td>';
                        $HTML.=         '<td height="50px" style="width:29%;text-align:center">';
                        $HTML.=         '<b>Nombre y Firma<br>Coordinador de Posgrado</b><br><br>Fecha: _____/_____/________';
                        $HTML.=         '</td>';
                        $HTML.=         '<td height="50px" style="width:29%;text-align:center"><b>Nombre y Firma<br>Coordinador del Programa</b><br><br>Fecha: _____/_____/________</td>';
                        $HTML.=         '<td height="50px" style="width:13%;text-align:left">';
                        $HTML.=         'Formato # DP-2019-004<br>Versión 1.0<br>Última actualización:<br>29/07/2019';
                        $HTML.=         '</td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '</table>';
                        $HTML.= '</td>';
                        $HTML.= '</tr>';
                        return $HTML;
                }                

                private function AsistenciaActaColumna()
                {       $this->Cols = 0;    
                        $HTMA = "<tr>";
                        $HTMA.= "<td colspan='5' ".$this->ColcCSS().">ESTUDIANTE</td></tr>";
                        $HTMB = "<tr>";
                        $HTMB.= "<td width='30px' ".$this->ColcCSS().">No</td>";
                        $HTMB.= "<td width='90px' ".$this->ColcCSS().">CEDULA</td>";
                        $HTMB.= "<td width='300px' ".$this->ColcCSS().">NOMBRE</td></tr>";
                        $HTMB.= "<td width='210px' ".$this->ColcCSS().">OBSERVACIÓN</td></tr>";
                        $HTMB.= "<td width='90px' ".$this->ColcCSS().">ESTADO</td>";
                        return $HTMA.$HTMB;
                }
                
                private function AsistenciaActaDetalle($Estudiante)
                {           $STRI = "";
                            $HTMC = "";
                            $NUME = 0; 
                            $HDET = "";
                               
                            foreach ($Estudiante as $Dato)
                            {       if ($STRI!=$Dato['CEDULA'])
                                    {   $STRI = $Dato['CEDULA'];
                                        if ($HTMC!="")
                                        {   $HDET.= "<tr>".$HTMC."</tr>"; }
                                        $NUME++;
                                        $HTMC = "<td ".$this->ColdCSS('center').">".$NUME."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('center').">".$STRI."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('left').">".utf8_encode($Dato['ESTUDIANTE'])."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('left').">".utf8_encode($Dato['OBSERVACION'])."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('center').">".($Dato['ESTADO']==12 ? "RETIRADO" : "")."</td>";
                                    }
                            }
                            if ($NUME!=0)
                            {   $HDET.= "<tr>".$HTMC."</tr>";
                            }    
                        return $HDET; 
                }
        }
        
        if (isset($_GET['opcion']))
        {   $Utils = new Utils();
            $Nodos = $Utils->Desencriptar($_GET['opcion']);
            $Params = explode("|",$Nodos);
            
            $ServicioEstudiante = new ServicioEstudiante();
            $Estudiante = $ServicioEstudiante->BuscarEstudiantes($Params);
            //print_r($Estudiante);
            if ($Estudiante[0]) 
            {   $rptEstudiante = new rptEstudiante();
                $HTML = $rptEstudiante->CrearReporte($Params,$Estudiante[1]);
                $mpdf = new Mpdf('utf-8', 'A4');
                $mpdf->WriteHTML($HTML);
                $mpdf->Output('AESTD' . $Params[0] . $Params[3] . '.pdf', 'I');   
            }   else
                    echo $Estudiante[1];
        }   
?>
        
        