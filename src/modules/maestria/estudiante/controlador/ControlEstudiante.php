<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");    
        require_once("src/rules/maestria/servicio/ServicioEstudiante.php");
        require_once("src/modules/maestria/estudiante/render/RenderEstudiante.php");        
        require_once("src/rules/maestria/servicio/ServicioReporte.php");
        
        class ControlEstudiante
        {       private $ServicioEstudiante;
                private $RenderEstudiante;
                
                function __construct()
                {       $this->ServicioEstudiante = new ServicioEstudiante();        
                        $this->RenderEstudiante = new RenderEstudiante();
                }

                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderEstudiante->CreaBarButton($Buttons[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderEstudiante->CreaMantenimiento();
                }

                function ConsultaByID($Params)
                {       $xAjax = new xajaxResponse();
                        $Estudiantes = $this->ServicioEstudiante->BuscarEstudiantes($Params);
                        if ($Estudiantes[0])
                        {   $SearchGrid = $this->RenderEstudiante->CreaSearchGrid($Estudiantes,0);
                            $xAjax->Assign("WrkGrd","innerHTML",$SearchGrid);
                            $xAjax->Call("ElementStatus","MinPrn");
                        }
                        else
                        {       $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_ERR",$Estudiantes[1]);
                        }                        
                        return $xAjax;
                }
                
                function ActaEstudiante($Params)
                {       $xAjax = new xajaxResponse();
                        $Utils = new Utils();
                        $urlfl = $Utils->Encriptar("src/modules/maestria/estudiante/vista/rptEstudiante.php");
                        $title = "Acta de Calificaciones";
                        $optns = $Utils->Encriptar($Params[0]."|".$Params[1]."|".$Params[2]."|".$Params[3]."|".$Params[4]); 
                        $jsonModal['Modal'] = "ModalMINPRN"; 
                        $jsonModal['Title'] = $title;
                        $jsonModal['Carga'] = "<iframe name='ModalMINPRN' src='index.php?url=".$urlfl."&opcion=".$optns."' height='465px' width='100%' scrolling='si' frameborder='0' marginheight='0' marginwidth='0'></iframe>";  
                        $jsonModal['Ancho'] = "900";
                        $jsonModal['Alto']  = "500";
                        $xAjax->call("CreaModal", json_encode($jsonModal));
                        //$xAjax->Assign("WrkGrd","innerHTML","");
                        return $xAjax;
                }
                
                function ConsultaParalelosByCohorte($IdCohorte)
                {       $xAjax = new xajaxResponse();
                        $Paral = $this->ServicioEstudiante->BuscarParalelosByCohorte($IdCohorte);
                        $xAjax->Call("FillOptionCbx","Paralelo",$Paral[1]);
                        $xAjax->Call("ElementSetValue","Paralelo",0);
                        if (!$Paral[0]) 
                        {       $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_ERR","No existe paralelos.");
                        }
                        return $xAjax;
                }
                
                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion==="btmaestria")    
                        {   $Params = array('txt'=>'');
                            $ArDatos = $this->ServicioEstudiante->BuscarMaestria($Params);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderEstudiante->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderEstudiante->GetGridTableWidth();
                        }   
                        if ($Operacion==="btcohorte")    
                        {   $Params = array('txt'=>'','mtr'=>$IdRef);
                            $ArDatos = $this->ServicioEstudiante->BuscarCohorteByMaestria($Params);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cohorte";
                            $jsonModal['Carga'] = $this->RenderEstudiante->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderEstudiante->GetGridTableWidth();
                        }  
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btmaestria")    
                        {   $Texto = strtoupper(trim($Texto));
                            $Params = array('txt'=>$Texto);
                            $Datos = $this->ServicioEstudiante->BuscarMaestria($Params);
                            return $this->RenderEstudiante->MuestraModalGrid($xAjax,$Operacion,$Datos);                                       
                        }                        
                        else if ($Operacion=="btcohorte")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $prepareDQL = array('txt'=>$Texto[0],'mtr'=>$Texto[1]);
                            $Datos = $this->ServicioEstudiante->BuscarCohorteByMaestria($prepareDQL);
                            return $this->RenderEstudiante->MuestraModalGrid($xAjax,$Operacion,$Datos);         
                        }
                }
        }

?>

