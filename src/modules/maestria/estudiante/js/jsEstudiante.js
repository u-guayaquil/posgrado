﻿            var COLINI = 5;
            var NTETAP = "";
            var CAMBIO = 0;
            
            function ButtonClick(Operacion)
            {   if (Operacion=="MinPrn")
                    {   
                        //ElementStatus("","MinPrn");
                        var MAEST = ElementGetValue("idmaestria");
                        var COHRT = ElementGetValue("idcohorte");
                        var PARALELO = ElementGetText("Paralelo");    
                        var CICLO = ElementGetValue("idciclo");  
                        var FACULTAD = ElementGetValue("idfacultad"); 
                        var Params = [MAEST,COHRT,PARALELO,CICLO,FACULTAD]; 
                        //ElementSetValue("Paralelo",0);
                        xajax_ActaEstudiante(Params);
                            
                    }
                    else if (Operacion=="btmaestria")
                    {   xajax_CargaModal(Operacion,0);
                    }
                    else if (Operacion=="btcohorte")
                    {    var IDMST=ElementGetValue("idmaestria");
                         if (IDMST!="")
                         {   xajax_CargaModal(Operacion,IDMST);
                         }
                         else
                            SAlert("warning","¡Error!","¡Debe seleccionar una maestría!"); 
                    }
                    return false;
            }

            function SearchMaestriaGetData(DatosGrid)
            {       var IDMST = DatosGrid.cells[0].innerHTML;
                    if (ElementGetValue("idmaestria")!=IDMST)
                    {   NTETAP = "";
                        FillOptionCbx("Paralelo",[[0,"SELECCIONE..."]]);
                        ElementStatus("","MinPrn");
                        ElementSetValue("WrkGrd","");
                        ElementClear("idcohorte:txcohorte");
                        document.getElementById("idmaestria").value = IDMST;
                        document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML;
                        document.getElementById("idfacultad").value = DatosGrid.cells[4].innerHTML;
                        cerrar();
                    }
                    else
                    cerrar();    
                    return false;
            }  
    
            function SearchCohorteGetData(DatosGrid)
            {       var IDCHT= DatosGrid.cells[2].innerHTML;
            
                    if (ElementGetValue("idcohorte")!=IDCHT)
                    {   NTETAP = "";
                        ElementStatus("","MinPrn");
                        ElementSetValue("Paralelo",0);
                        ElementSetValue("WrkGrd","");
                        document.getElementById("idcohorte").value = DatosGrid.cells[0].innerHTML;;
                        document.getElementById("txcohorte").value = DatosGrid.cells[1].innerHTML;
                        document.getElementById("idciclo").value = DatosGrid.cells[5].innerHTML;
                        xajax_BuscaParalelosByCohorte(ElementGetValue("idcohorte"));
                        cerrar();
                    }    
                    else    
                    cerrar();    
                    return false;
            }
            
            function SearchParaleloGetData(DatosCombo)
            {       if (DatosCombo.value==0)
                    {   NTETAP = "";
                        ElementSetValue("WrkGrd","");
                        ElementStatus("","MinPrn");
                    }
                    else
                    {   var PARALELO = ElementGetText(DatosCombo.id);                     
                        if (NTETAP!=PARALELO)
                        {   NTETAP = PARALELO;
                            ElementStatus("MinPrn","");
                            SearchParaleloGetDataGrid();
                        }
                    }    
            }  
            
            function SearchParaleloGetDataGrid()
            {       var MAEST = ElementGetValue("idmaestria");
                    var COHRT = ElementGetValue("idcohorte");
                    var Params = [MAEST,COHRT,NTETAP];
                    xajax_BuscaByID(Params);
            }  
            
            function SearchByElement(Elemento)
            {   Elemento.value = TrimElement(Elemento.value);    
                    if (Elemento.id=="FindMaestriaTextBx")
                    {   xajax_BuscaModalByTX("btmaestria",Elemento.value);
                    }
                    else if (Elemento.id=="FindCohorteTextBx")
                    {    var IDMST=ElementGetValue("idmaestria");
                         var Params = [Elemento.value,IDMST];
                         xajax_BuscaModalByTX("btcohorte",Params);
                    }
                    return false;    
            }

            function XAJAXResponse(Mensaje,Datos)
            {   if (Mensaje==="CMD_SAV")
                    {   SAlert("success","¡Excelente!",Datos);
                    }
                else if(Mensaje==='CMD_ERR')
                    {   SAlert("error","¡Error!",Datos); 
                    }    
            }