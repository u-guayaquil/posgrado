<?php   
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php"); 
                
        class RenderEstudiante
        {     private $Width;
              private $SearchGrid;
              private $Search;
              private $Columnas;
              private $Cerrado;
              
                function __construct()
                {       $this->Search = new SearchInput();   
                        $this->SearchGrid = new SearchGrid();
                }

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaMantenimiento()
                {       $Col1="15%"; $Col2="40%"; $Col3="15%"; $Col4="15%"; $Col5="15%"; 

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="35">';
                        $HTML.= '           <td class="Form-Label" colspan="5">:: REPORTE DE ESTUDIANTES</td>';
                        $HTML.= '       </tr>';                                                                         
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t11"); 
                        $HTML.= '               <input type="hidden" id="idfacultad" name="idfacultad" value="" />';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';             
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Cohorte</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("cohorte","","")->Enabled(true)->Create("t11");     
                        $HTML.= '               <input type="hidden" id="idciclo" name="idciclo" value="" />';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                      //$HTML.='                <input type="button" id="mincontent" class="p-txt-label" style="border: 1px solid #aaa; width: 60px; height: 25px" value="Contenido" onclick=" return ButtonClick(\'mincontent\'); ">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col2.'" rowspan="2">';
                        $HTML.='                <input type="button" id="MinPrn" class="p-txt-label-bold" style="height: 30px;text-decoration: none;font-weight: 600;font-size: 14px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin:auto;" value="Imp. Listado" onclick=" return ButtonClick(\'MinPrn\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Paralelo</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <select class="sel-input s07" id="Paralelo" name="Paralelo" onchange=" return SearchParaleloGetData(this)">';
                        $HTML.= '                       <option value="0">SELECCIONE...</option>';
                        $HTML.= '               </select>';    
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';  
                        return $HTML.'</table>';
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']=array('40px','center','');
                            $Columns['Maestria']=array('300px','left','');
                            $Columns['IdEstado']=array('0px','center','none');
                            $Columns['Estado']=array('50px','center','');
                            $Columns['IdFacultad']=array('0px','center','none');
                        }
                        else if ($Operacion=="btcohorte")
                        {   $Columns['C.ID']=array('40px','center','');
                            $Columns['Cohorte']=array('200px','left','');
                            $Columns['Version Maestria']=array('150px','center','');
                            $Columns['IdEstado']=array('0px','center','none');
                            $Columns['Estado']=array('50px','center','');
                            $Columns['IdCiclo']=array('0px','center','none');
                        }
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btcohorte")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['ID'],MATERIA,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['COHORTE']));
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['VERSION_MAESTRIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                        $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDCICLO']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMAESTRIA'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MAESTRIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                        $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDFACULTAD']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }         
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
                private function SearchGridConfig()
                {       $Columns['No'] = array('30px','center','');
                        $Columns['Cédula'] = array('100px','center','');
                        $Columns['Estudiante'] = array('250px','left','');
                        $Columns['Observación'] = array('200px','left','');
                        $Columns['Estado'] = array('70px','center','');
                        return array('AltoCab' => '35px','DatoCab' => $Columns,'AltoDet' => '300px','AltoRow' => '25px','FindTxt' =>false);
                }

                function CreaSearchGrid($Estudiantes,$Cerrado=0)
                {       $this->Cerrado = $Cerrado;
                        $this->Columnas = $Estudiantes[1];
                        $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Estudiantes[1]);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                private function GridDataHTML($Grid,$Datos)
                {       $num = 1;
                        foreach ($Datos as $Dato)
                        {       $Id = str_pad($num,2,'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($Id,'',$num++);
                                $Grid->CreaSearchCellsDetalle($Id,'',$Dato['CEDULA']);
                                $Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['ESTUDIANTE']));
                                $Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['OBSERVACION']));
                                $Grid->CreaSearchCellsDetalle($Id,'',($Dato['ESTADO']==12 ? "RETIRADO" : ""));
                                $Grid->CreaSearchRowsDetalle ($Id,"black",0);
                        }
                        return $Grid->CreaSearchTableDetalle(0,$Datos[1]);
                }
                
                function MuestraGrid($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {        $Ajax->Assign($Dato[0],"innerHTML",$Dato[1]);
                        }
                        return $Ajax;
                }                    
        }
?>

