var objArea = "id:descripcion:siglas:estado";
var valArea = "descripcion:siglas";

function ButtonClick(Operacion)
{   if (Operacion==='addNew')
    {   BarButtonState(Operacion);
        ElementStatus("descripcion:siglas","id");
        ElementClear("id:descripcion:siglas");
        ElementSetValue("estado",1);
    }
    else if (Operacion==='addMod')
    {       BarButtonState(Operacion);
            if (ElementGetValue("estado")==0)
            {   BarButtonStateEnabled("btmaestria");
                valArea = valArea + ":estado";
            }
            ElementStatus(valArea,"id");
    }
    else if (Operacion==='addDel')
    {       if (ElementGetValue("estado")==1)
            {   Swal.fire({
                    title: 'Desactivar registro?',
                    text: "Esta seguro que desea desactivar el registro?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro!'
                }).then(function(result){
                    if (result.value) {
                        var Forma = PrepareElements("id:estado");
                        xajax_Elimina(JSON.stringify({Forma}));
                    }
                });   
            }
            else{SAlert("warning", "¡Cuidado!", "El registro se encuentra inactivo");}
    }
    else if (Operacion==='addSav')
    {       if (ElementValidateBeforeSave(valArea))
            {   if (BarButtonState("Inactive"))
                {   var Forma = PrepareElements(objArea);
                    xajax_Guarda(JSON.stringify({Forma}));
                }
            }
            else
            {SAlert("warning", "¡Cuidado!", "Llene los campos por favor");}
    }
    else if (Operacion==='addCan')
    {       BarButtonState(Operacion);
            ElementStatus("id","descripcion:siglas:estado");
            ElementClear("id:descripcion:siglas");
            ElementSetValue("estado",1);
    }
    return false;
}

function XAJAXResponse(Mensaje,Datos)
{   if (Mensaje==="CMD_SAV")
    {   BarButtonState("Default");
        ElementSetValue("id",Datos);
        ElementClear("id:descripcion:siglas");
        ElementStatus("id","descripcion:siglas:estado");
        ElementSetValue("estado",1);
        SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
    }
    else if(Mensaje==='CMD_ERR')
    {   BarButtonState("addNew");
        ElementSetValue("id",Datos);
        ElementClear("id:descripcion:siglas");
        ElementStatus("id","descripcion:siglas:estado");
        SAlert("error", "¡Error!", Datos); 
    }    
    else if(Mensaje==='CMD_DEL')
    {   ElementSetValue("estado",Datos);
        SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
    }
    else if(Mensaje==="CMD_WRM")
    {   BarButtonState("Default");
        ElementClear("id");
        ElementSetValue("estado",1);
        SAlert("info", "¡INFORMACIÓN!", Datos); 
    }
}

function SearchByElement(Elemento)
{   Elemento.value = TrimElement(Elemento.value);
    if (Elemento.name=="id")
    {   if (Elemento.value.length != 0)
        {   ContentFlag = document.getElementById("descripcion");
            if (ContentFlag.value.length==0)
            {   if (BarButtonState("Inactive"))
                {   xajax_BuscaByID(Elemento.value);
                }
            }
        }
        else
        BarButtonState("Default"); 
    }
    else if(Elemento.id === "FindTextBx")
    {   var Forma = PrepareElements("FindTextBx");
        xajax_BuscaByTX(JSON.stringify({Forma}));
    }
}
    
function SearchGetData(Grilla)
{   if (IsDisabled("addSav"))
    {   BarButtonState("Active");
        document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("descripcion").value = Grilla.cells[1].childNodes[0].nodeValue.toUpperCase();
        document.getElementById("siglas").value = Grilla.cells[2].childNodes[0].nodeValue.toUpperCase(); 
        document.getElementById("estado").value = Grilla.cells[4].childNodes[0].nodeValue; 
    }
    return false;
}