<?php
require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
require_once("src/rules/maestria/servicio/ServicioHospitalArea.php");
require_once("src/modules/maestria/hospitalarea/render/RenderHospitalArea.php");

class ControlHospitalArea
{   private  $ServicioArea;
	private  $RenderArea;

	function __construct()
	{       $this->ServicioArea = new ServicioHospitalArea();
			$this->RenderArea = new RenderHospitalArea();
	}

	function CargaBarButton($Opcion)
	{       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
			$Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
			if ($Datos[0])
			echo $this->RenderArea->CreaBarButton($Datos[1]);
	}
	
	function CargaMantenimiento()
	{       echo $this->RenderArea->CreaMantenimiento();
	}

	function CargaSearchGrid()
	{       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
			$Datos = $this->ServicioArea->BuscarAreaByDescripcion($prepareDQL);
			echo $this->RenderArea->CreaSearchGrid($Datos);
	}         
	
	function ConsultaByID($id)
	{       $ajaxRespon = new xajaxResponse(); 
			$Datos = $this->ServicioArea->BuscarAreaByID($id);
			return $this->RenderArea->MuestraFormulario($ajaxRespon,$Datos);
	}

	function ConsultaByTX($Form)
	{       $ajaxRespon = new xajaxResponse();   
			$JsDatos = json_decode($Form)->Forma;           
			$prepareDQL = array('texto' => strtoupper(trim($JsDatos->FindTextBx)));
			$ArDatos = $this->ServicioArea->BuscarAreaByDescripcion($prepareDQL);
			return $this->RenderArea->MuestraGrid($ajaxRespon,$ArDatos);
	}

	function Guarda($Form)
	{       $ajaxRespon = new xajaxResponse();
			$JsDatos = json_decode($Form)->Forma;
			$funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");   
			$id = $this->ServicioArea->GuardaDBArea($JsDatos);
			$ArDatos = $this->ServicioArea->BuscarAreaByIDGuardado($id);
			return $this->RenderArea->{$funcion}($ajaxRespon,$ArDatos);
	}

	function Elimina($Form)
	{       $ajaxRespon = new xajaxResponse();
			$JsData = json_decode($Form)->Forma;
			$JsData->estado = $this->ServicioArea->DesactivaArea($JsData->id);
			return $this->RenderArea->MuestraEliminado($ajaxRespon,$JsData);
	}
}       
?>