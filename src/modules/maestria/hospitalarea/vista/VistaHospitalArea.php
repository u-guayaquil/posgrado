<?php   $Opcion = 'Area';

require_once('src/modules/maestria/hospitalarea/controlador/ControlHospitalArea.php');
$ControlArea = new ControlHospitalArea();

$xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlArea,'Guarda'));
$xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlArea,'Elimina'));
$xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlArea,'ConsultaByID'));
$xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlArea,'ConsultaByTX'));

$xajax->processRequest();       
?>
<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <?php $xajax->printJavascript(); 
            require_once('src/utils/links.php');
    ?>
    <script type="text/javascript" src="src/modules/maestria/hospitalarea/js/jsHospitalArea.js?1"></script>
</head>
    <body>
        
    <div class="FormBasic" style="width:510px">
        <div class="FormSectionMenu">              
        <?php   $ControlArea->CargaBarButton($_GET['opcion']);
                echo '<script type="text/javascript">';
                echo '        BarButtonState("Inactive"); ';
                echo '</script>';                    
        ?>    
        </div>  
        
        <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Opcion; ?>">
                <?php  $ControlArea->CargaMantenimiento();  ?>
                </form>
        </div> 
        
        <div class="FormSectionGrid">          
        <?php   $ControlArea->CargaSearchGrid();  ?>
        </div>  
    </div>
    <script>BarButtonState("Default");ElementStatus("id","");</script>
    </body>
</html>