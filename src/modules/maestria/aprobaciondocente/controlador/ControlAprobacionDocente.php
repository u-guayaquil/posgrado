<?php   require_once("src/rules/maestria/servicio/ServicioAprobacionDocente.php");
        require_once("src/modules/maestria/aprobaciondocente/render/RenderAprobacionDocente.php");
        
        class ControlAprobacionDocente
        {       private  $ServicioAprobacionDocente;
                private  $RenderAprobacionDocente;

                function __construct()
                {       $this->ServicioAprobacionDocente = new ServicioAprobacionDocente();
                        $this->RenderAprobacionDocente = new RenderAprobacionDocente();
                }

                function CargaAprobacionDocenteSearchGrid()
                {   $Datos = $this->ServicioAprobacionDocente->BuscarAprobacionDocenteSinContrato();
                    echo $this->RenderAprobacionDocente->CreaSearchGridAprobacionDocente($Datos);
                }
                
                 function GuardaAprobacionDocente($Form)
                {   $ajaxRespon = new xajaxResponse();
                    $AprobDocent = json_decode($Form)->Forma;
                    $this->ServicioAprobacionDocente->EditaAprobacionDocente($AprobDocent);
                    $ArDatos = $this->ServicioAprobacionDocente->BuscarAprobacionDocenteSinContrato();
                    $this->RenderAprobacionDocente->MuestraGuardado($ajaxRespon,$ArDatos);
                    return $ajaxRespon->call("XAJAXResponse", "CMD_SAV", "Los datos se guardaron correctamente");
                }
                
                function CargaModalGrid()
                {       $ajaxResp = new xajaxResponse();    
                        $jsonModal['Modal'] = "Modal Observacion Docente"; 
                        $jsonModal['Title'] = "Agrega Observacion";
                        $jsonModal['Carga'] = $this->RenderAprobacionDocente->CreaModal();
                        $jsonModal['Ancho'] = "517";  
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
        }       
?>

