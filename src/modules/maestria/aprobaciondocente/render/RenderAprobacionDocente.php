<?php    
        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/SearchGrid.php");
        
        class RenderAprobacionDocente
        {   private $SearchGrid;
            private $Maxlen; 
            
                function __construct()
                {       $this->Maxlen['id'] = 4;        
                        $this->Maxlen['descripcion'] = 100;
                        $this->SearchGrid = new SearchGrid();
                }
                
                                
                private function Respuesta($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse";
                        $Ajax->call($jsFuncion,$Tipo,$Data);
                        return $Ajax;
                }
        
                private function SearchGridConfig()
                {       $Columns['Id'] = array('0px','center','none');
                        $Columns['Cédula'] = array('120px','center','');
                        $Columns['Nombre'] = array('160px','left','');
                        $Columns['Apellido'] = array('160px','left','');
                        $Columns['Facultad'] = array('185px','center','');
                        $Columns['✔'] = array('40px','center','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '425px','AltoRow' => '25px','FindTxt' =>false);
                }

                function CreaSearchGridAprobacionDocente($ArDatos)
                {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                        return $this->SearchGrid->CreaSearchGrid($GrdDataHTML);
                }

                function GridDataHTML($grid,$ArDatos)
                {   if ($ArDatos[0])
                    {   foreach ($ArDatos[1] as $ArDato)
                        {       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);                                          
                                $grid->CreaSearchCellsDetalle($id,'',$id); 
                                $grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['CEDULA'])));
                                $grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['NOMBRES'])));  
                                $grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['APELLIDOS'])));  
                                $grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['FACULTAD']))); 
                                $mark = ($ArDato['IDESTADO'] == 2 ? "" : "✔");
                                $grid->CreaSearchCellsDetalle($id,'',$mark,0,1); 
                                $grid->CreaSearchRowsDetalle ($id,($ArDato['IDESTADO']==0 ? "red" : ""));
                        }
                    }
                    return $grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }
                
                function MuestraGuardado($Ajax,$ArDatos)
                {       if ($ArDatos[0])
                        {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            return $this->MuestraGrid($Ajax,$ArDatos);
                        }
                        return $this->MuestraGrid($Ajax,$ArDatos);
                }

                function MuestraGrid($Ajax,$ArDatos)
                {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                        $Ajax->Assign($this->SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }
                
                /*** MODAL ***/ 

                function CreaModal()
                {       
                    $HTML = '<table border=0 class="Form-Frame" cellpadding="0">';                 
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="color:black;">&nbsp&nbspObservacion</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                 <input type="text" class="txt-upper t15" id="observacion" name="observacion" value="" maxlength="'.$this->Maxlen['descripcion'].'"/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" colspan="2" align="center">';
                    $HTML.= '                 <input type="button" value="ENVIAR" class="t05" id="enviar" name="enviar" onclick="ControlButton();"/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '</table>';
                    return $HTML;
                }
        }
?>

