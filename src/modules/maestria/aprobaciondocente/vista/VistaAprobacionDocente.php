<?php   $Opcion = 'AprobacionDocente';

        require_once('src/modules/maestria/aprobaciondocente/controlador/ControlAprobacionDocente.php');
        $ControlAprobacionDocente = new ControlAprobacionDocente();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlAprobacionDocente,'GuardaAprobacionDocente'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlAprobacionDocente,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('CargaDocente', $ControlAprobacionDocente,'CargaDocenteSearchGrid'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">

            var Objto;
            var Elemn;
            var idCelda;
            
                function SearchGetCell(Celda)
                {   
                    var str = Celda.id;
                    var opt = str.split("_");
                    var CeldaLlena = (Celda.innerHTML==="" ? "True":"False");
                    if(CeldaLlena==="True"){
                        Swal.fire({
                                    title: 'Aprobar docente',
                                    text: "¿Esta seguro que desea APROBAR al docente?",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Si, estoy seguro!'
                                }).then(function(result){
                                    if (result.value) {
                                        if (parseInt(opt[2])===5)
                                        {   idCelda = Celda.id;
                                            Objto = opt[0]+"_"+opt[1];
                                            Elemn = "IdObjto:" + Objto + "|IdBoton:" + parseInt(opt[2]) + "|Id:" + document.getElementById(Objto+"_000").innerHTML + "|Value:" + (Celda.innerHTML==="" ? "1":"2");
                                            xajax_CargaModal(); 
                                        }
                                        return false;
                                    }
                                });
                    }
                    else if (CeldaLlena === "False"){
                        Swal.fire({
                                    title: 'Anular docente',
                                    text: "¿Esta seguro que desea ANULAR al docente?",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Si, estoy seguro!'
                                }).then(function(result){
                                    if (result.value) {
                                        if (parseInt(opt[2])===5)
                                        {   idCelda = Celda.id;
                                            Objto = opt[0]+"_"+opt[1];
                                            Elemn = "IdObjto:" + Objto + "|IdBoton:" + parseInt(opt[2]) + "|Id:" + document.getElementById(Objto+"_000").innerHTML + "|Value:" + (Celda.innerHTML==="" ? "1":"2");
                                            xajax_CargaModal();
                                        }
                                        return false;
                                    }
                                });
                    }   
                    return false;
                }
                
                function ControlButton()
                {
                    var valdocente = "observacion";
                    var Elemnt = Elemn+"|observacion:"+document.getElementById("observacion").value;
                    var celda = document.getElementById(idCelda).innerHTML;
                    if (ElementValidateBeforeSave(valdocente))
                    {   cerrar();
                        if(celda === "")
                            {document.getElementById(idCelda).innerHTML = "✔";}
                        else
                            {document.getElementById(idCelda).innerHTML = "";}
                        var Forma = PrepareFreeElements(Elemnt);
                        xajax_Guarda(JSON.stringify({Forma}));
                    }
                    else
                    {SAlert("warning","¡Campos Vacíos!","Llene los campos por favor");}
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   
                            SAlert("success", "¡Excelente!", Datos);  
                        }
                        if (Mensaje==="CMD_ERR")
                        {   
                            SAlert("error", "¡Error!", Datos);  
                        }
                }
        </script>
    </head>
        <body>
        <div class="FormContainer" style="width:1260px; text-align:center;">  
            <div class="FormContainerSeccion" style="width:720px">   
                <div id="<?php echo 'Grid'; ?>" class="FormSectionGrid">          
                <?php $ControlAprobacionDocente->CargaAprobacionDocenteSearchGrid();  ?>
                </div>    
            </div>    
        </div>
        </body>
    </html>
