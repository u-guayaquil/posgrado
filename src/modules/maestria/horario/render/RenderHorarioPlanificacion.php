<?php       
    
    class RenderHorarioPlanificacion
    {       
            function CreaEditorHorario($horario)
            {       $DateControl= new DateControl();  
               
                    $HTML = '<table class="Form-Frame" cellpadding="0" border="1">';
                    $HTML.= '      <tr height="28">';
                    $HTML.= '          <td class="Form-Label-Center" colspan=3><p class="p-txt-label">DIAS DE LA SEMANA</p></td>';
                    $HTML.= '          <td class="Form-Label-Center" style="width:20%"><p class="p-txt-label">HORA</p></td>';
                    $HTML.= '          <td class="Form-Label-Center" style="width:20%"><p class="p-txt-label">TOTAL<BR>HORAS</p></td>';
                    $HTML.= '      </tr>';
                    
                    if ($horario[0])
                    {   $Cpte = "";
                        $Item = 0;
                        $Hexe = "";
                        $Exec = 0;
                        
                        $Suma[1] = 0;
                        $Suma[2] = 0;
                        $Suma[3] = 0;
                        foreach ($horario[1] as $ArDato)    
                        {       if ($ArDato['COMPTE']==1)
                                {   $HTML.= '<tr height="20px">';
                                    if ($ArDato['COMPTE']!=$Cpte)
                                    {   $Cpte = $ArDato['COMPTE'];
                                        $HTML = str_replace("@", $Item, $HTML);
                                        $Item = 0;
                                        $Hexe = "";
                                        $HTML = str_replace("?", $Exec, $HTML);
                                        $Exec = 0;
                                    }

                                    if ($ArDato['HOREJE']!=$Hexe)
                                    {   $Hexe = $ArDato['HOREJE'];
                                        $HTML = str_replace("?", $Exec, $HTML);
                                        $Exec = 0;
                                    }    

                                    if ($Item==0)
                                    {   $HTML.= '<td style="width:10%" rowspan=@ class="Form-Label-Center"><p class="p-txt-label">';
                                        $HTML.=  ($ArDato['COMPTE']==1 ? 'CD' : ($ArDato['COMPTE']==2 ? 'CPE': 'CA')); 
                                        $HTML.= '</P></td>';
                                    } 
                                
                                    if ($Exec==0)
                                    {   $HTML.= '<td style="width:10%" rowspan=? class="Form-Label-Center"><p class="p-txt-label">';
                                        $HTML.=   ($ArDato['HOREJE']=="S" ? "✔":""); 
                                        $HTML.= '</P></td>';
                                    } 
                                
                                    $HTML.= '<td style="width:30%" class="Form-Label-Center"><p class="p-txt-label">';
                                    $HTML.=      $DateControl->getDayName($ArDato['DIASEMANA']); 
                                    $HTML.= '</P></td>';
                                    $HTML.= '<td style="width:20%" class="Form-Label-Center">';
                                    $HTML.=      $this->CargaComboHorasFijas("hora",$ArDato); 
                                    $HTML.= '</td>';
                                    $HTML.= '<td style="width:20%" class="Form-Label-Center"><p class="p-txt-label">';
                                    $HTML.=      $ArDato['TOTALHORAS']; 
                                    $HTML.= '</P></td>';
                                    
                                    $HTML.= '</tr>';
                                    $Item++;
                                    $Exec++;
                                }
                                $Suma[$ArDato['COMPTE']]+= $ArDato['TOTALHORAS'];
                        }
                        $HTML = str_replace("?",$Exec,$HTML);
                        $HTML = str_replace("@",$Item,$HTML);
                    }
                    $HTML.= $this->TotalesComponentes($Suma[1],"CD");
                    $HTML.= $this->MuestraComponentes($Suma[2],"CPE");
                    $HTML.= $this->MuestraComponentes($Suma[3],"CA");
                    
                    $HTML.= '      <tr height="45px">';
                    $HTML.= '          <td class="Form-Label-Center" colspan="5">';
                    $HTML.= '              <input type="button" id="btaplica" class="p-txt-label" style="border: 1px solid #aaa; width: 56px; height: 25px" value="Cerrar" onclick=" return AplicaHorario(); ">';
                    $HTML.= '          </td>';
                    $HTML.= '      </tr>';        
                    $HTML.= '</table>';
                    return $HTML;
            }
            
            private function TotalesComponentes($Suma,$Compte)
            {       $HTML = "";
                    if ($Suma!=0)
                    {   $HTML = '<tr height="20px">';
                        $HTML.= '    <td class="Form-Label-Center" colspan="3"><p class="p-txt-label"></p></td>';
                        $HTML.= '    <td class="Form-Label-Center"><p class="p-txt-label">Total '.$Compte.'</p></td>';
                        $HTML.= '    <td class="Form-Label-Center"><p class="p-txt-label">';
                        $HTML.=          $Suma; 
                        $HTML.= '    </P></td>';
                        $HTML.= '</tr>';
                    }    
                    return $HTML;
            }
            
            private function MuestraComponentes($Suma,$Compte)
            {       $HTML = "";
                    if ($Suma!=0)
                    {   $HTML = '<tr height="20px">';
                        $HTML.= '    <td class="Form-Label-Center"><p class="p-txt-label">'.$Compte.'</p></td>';
                        $HTML.= '    <td class="Form-Label-Center" colspan="3"><p class="p-txt-label"></p></td>';
                        $HTML.= '    <td class="Form-Label-Center"><p class="p-txt-label">';
                        $HTML.=          $Suma; 
                        $HTML.= '    </P></td>';
                        $HTML.= '</tr>';
                    }    
                    return $HTML;
            }            
            
            function CargaComboHorasFijas($objname,$Datos)
            {       $Cp = $Datos['COMPTE'];
                    $Pl = $Datos['IDPLANANA'];
                    $Dy = $Datos['DIASEMANA'];
                    $Ex = $Datos['HOREJE'];
                    $Ho = $Datos['HORA'];
                    if ($Ex=="S")
                        return '<p class="p-txt-label">'.$Ho.'</p>';
                    else
                    {   $HTML = '<select id="'.$objname.'" name="'.$objname.'" class="sel-input" style="width:60px" onchange=" return ClickHorario(this,\''.$Cp.'\',\''.$Pl.'\',\''.$Dy.'\');">';
                        $HTML.= '<option value="00:00">00:00</option>';
                        for ($k=7; $k<=20; $k++)
                        {   $H1 = str_pad($k,2,'0',STR_PAD_LEFT).":00";
                          //$H2 = str_pad($k,2,'0',STR_PAD_LEFT).":30";
                            if ($H1==$Ho)
                                $HTML.= '<option value="'.$H1.'" selected>'.$H1.'</option>';
                            else
                                $HTML.= '<option value="'.$H1.'">'.$H1.'</option>';
                          //$HTML.= '<option value="'.$H2.'">'.$H2.'</option>';
                        } 
                        $HTML.= '</select>';
                        return $HTML;
                    } 
            }            
    }        
            
?>