<?php
        $Opcion = 'Asistencia';
        require_once('src/modules/maestria/asistencia/controlador/ControlAsistencia.php');
        $ControlAsistencia = new ControlAsistencia();

        $xajax->register(XAJAX_FUNCTION,array('GuardaDB', $ControlAsistencia,'GuardaDB'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlAsistencia,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlAsistencia,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlAsistencia,'ConsultaGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaPermiso', $ControlAsistencia,'GuardaPermiso'));
        
        $xajax->processRequest();
        
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/maestria/asistencia/js/jsAsistencia.js"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:800px">
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionData">   
                             <?php  $ControlAsistencia->CargaMantenimiento();  ?>
                        </div> 
                        <div id="<?php echo 'WrkGrd'; ?>" class="FormSectionGrid">  
                        </div>     
                    </form>
                </div>       
        </body>
        </html>
              

