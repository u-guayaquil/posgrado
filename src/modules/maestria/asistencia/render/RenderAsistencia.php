<?php   
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php"); 
                
        class RenderAsistencia
        {     private $Width;
              private $SearchGrid;
              private $Search;
              private $Columnas;
              
                function __construct()
                {       $this->Search = new SearchInput();   
                        $this->SearchGrid = new SearchGrid();
                }

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaMantenimiento()
                {      
                        $Col1="11%"; $Col2="40%"; $Col3="13%"; $Col4="18%"; $Col5="18%"; 

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="35">';
                        $HTML.= '           <td class="Form-Label" colspan="5">:: REGISTRO DE ASISTENCIA</td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Docente</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("docente","","")->Enabled(true)->Create("t11");   
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col3.'">';
                      //$HTML.='                <input type="button" id="MinSav" class="p-txt-label-bold" style="border: 1px solid #aaa; width: 80px; height: 25px" value="✔ Guardar" onclick=" return ButtonClick(\'MinSav\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col4.'">';
                        $HTML.='                <input type="button" id="MinOpn" class="p-txt-label-bold" style="height: 25px;text-decoration: none;font-weight: 600;font-size: 12px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin-top:0px;" value="Permiso" onclick=" return ButtonClick(\'MinOpn\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col5.'">';
                        $HTML.='                <input type="button" id="MinSav" class="p-txt-label-bold" style="height: 25px;text-decoration: none;font-weight: 600;font-size: 12px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin-top:0px;" value="✔ Guardar" onclick=" return ButtonClick(\'MinSav\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idciclo" name="idciclo" value="" />';
                        $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t11");                         
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">&nbsp;Ciclo</td>';
                        $HTML.= '           <td class="Form-Label" colspan="2">';
                        $HTML.= '               <input type="text" class="txt-input" style="width:270px" id="txciclo" name="txciclo" value="" readOnly/> '; 
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';             
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Materia</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("materia","","")->Enabled(true)->Create("t11");       
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label style="width:'.$Col3.'"">&nbsp;Grupo/Horario</td>';
                        $HTML.= '           <td class="Form-Label" colspan="2">';
                        $HTML.= '               <input type="hidden" id="idgrupo" name="idgrupo" value="" />';
                        $HTML.= '               <input type="text" class="txt-input" style="width:83px" id="txgrupo" name="txgrupo" value="" readOnly/> ';
                        $HTML.=                 $this->Search->TextSch("horario","","")->Enabled(true)->Create("t06");  
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        return $HTML.'</table>';
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btdocente")
                        {   $Columns['id'] = array('0px','center','none');
                            $Columns['cedula'] = array('80px','center','');
                            $Columns['Docente']= array('300px','left','');
                        }          
                        else if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']=array('40px','center','');
                            $Columns['Maestria']=array('300px','left','');
                            $Columns['C.ID']=array('0px','center','none');
                            $Columns['Ciclo']=array('200px','left','');
                        }
                        else if ($Operacion=="btmateria")
                        {   $Columns['M.Id']=array('40px','center','');
                            $Columns['Materia']=array('250px','left','');
                            $Columns['G.ID']=array('40px','center','');
                            $Columns['Grupo']=array('150px','left','');
                        }
                        else if ($Operacion=="bthorario")
                        {   $Columns['Id']  =array('40px','center','');
                            $Columns['Horeje'] =array('0px','left','none');
                            $Columns['Permiso'] =array('0px','left','none');
                            $Columns['Dia'] =array('250px','left','');
                            $Columns['Fecha']=array('0px','left','none');
                            $Columns['Hora']=array('0px','left','none');
                            $Columns['Horas']=array('40px','center','');
                            $Columns['Auth']=array('0px','left','none');
                            $Columns['Group']=array('10px','left','none');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '140px','AltoRow' => '20px','FindTxt'=>false);
                        }                            
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btmateria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMATERIA'],MATERIA,'0',STR_PAD_LEFT);
                                        $ig = str_pad($ArDato['IDGRUPO'],GRUPO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ig);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['GRUPO']));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMAESTRIA'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MAESTRIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['CICLO']));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btdocente")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['CEDULA'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['APELLIDOS'])).' '.utf8_encode(trim($ArDato['NOMBRES'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }                                             
                            elseif ($Operacion=="bthorario")
                            {   $horario=1;
                                foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($horario,3,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['HOREJE']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['PERMISO']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['DIASEMANA']." ".$ArDato['FECHA']->format('d/m/Y')." ".$ArDato['INICIO']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['FECHA']->format('d/m/Y'));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['INICIO']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['NUMHORAS']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['AUTH']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['GRPEST']);
                                        $Color = "black";
                                        if (!$ArDato['PERMISO'])
                                        {   $Color = ($ArDato['HOREJE']=="S" ? "blue":"red");
                                        }
                                        $Grid->CreaSearchRowsDetalle ($id,$Color);
                                        $horario++;
                                }
                            }                                             
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
                private function SearchGridConfig($Cols)
                {       $Columns['ID'] = array('0px','center','none');
                        $Columns['EM.ID'] = array('0px','center','none');
                        $Columns['FE.AS'] = array('0px','center','none');
                        $Columns['H.ID'] = array('0px','center','none');
                        $Columns['No.'] = array('25px','center','');
                        $Columns['Estudiante'] = array('250px','left','');
                        foreach($Cols as $Col)
                        {       $Columns[$Col] = array('40px','center','');   
                        }
                        return array('AltoCab' => '35px','DatoCab' => $Columns,'AltoDet' => '340px','AltoRow' => '25px','FindTxt' =>false);
                }

                function CreaSearchGrid($Asistencia,$Edicion)
                {       $Columns=$Asistencia[0];
                        $Datos=$Asistencia[1];
                        
                        $this->Columnas=count($Columns);
                        $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig($Columns));
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Datos,$Edicion);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                private function GridDataHTML($Grid,$Datos,$Edicion)
                {       if ($Datos[0])
                        {   $num = 1;
                            foreach ($Datos[1] as $Dato)
                            {       $Id = str_pad($num,2,'0',STR_PAD_LEFT);
                                    $Grid->CreaSearchCellsDetalle($Id,'',$Dato['IDASISTENCIA']);
                                    $Grid->CreaSearchCellsDetalle($Id,'',$Dato['IDESTUDMATE']);
                                    $Grid->CreaSearchCellsDetalle($Id,'',$Dato['FECHA']->format('d/m/Y'));
                                    $Grid->CreaSearchCellsDetalle($Id,'',$Dato['IDHORARIO']);
                                    $Grid->CreaSearchCellsDetalle($Id,'',$num++);
                                    $Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['ESTUDIANTE']));
                                    if ($Dato['ASISTENCIA']=="")
                                    {   for ($u=0;$u<$this->Columnas;$u++)
                                        {    $Grid->CreaSearchCellsDetalle($Id,'',(!$Edicion ? "":"✔"),0,1);
                                        }
                                    }
                                    else 
                                    {   $Nodo = explode("|",$Dato['ASISTENCIA']);
                                        for ($u=0;$u<$this->Columnas;$u++)
                                        {       $Eval = ($Nodo[$u] == 1 ? "✔":($Nodo[$u] == 2 ? "JUS":""));
                                                $Grid->CreaSearchCellsDetalle($Id,'',$Eval,0,1);
                                        }
                                        
                                    }
                                    $Grid->CreaSearchRowsDetalle ($Id,"black",0);
                            }
                        }
                        return $Grid->CreaSearchTableDetalle(0,$Datos[1]);
                }
               
                function MuestraGrid($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {        $Ajax->Assign($Dato[0],"innerHTML",$Dato[1]);
                        }
                        return $Ajax;
                }    
                
                function CrearPermiso($Datos)
                {       $Col1="20%"; $Col2="80%"; 
                        
                        $HTML = '<table border="1" class="Form-Frame" cellpadding="4">';
                        $HTML.= '       <tr height="20px">';
                        $HTML.= '           <td class="Form-Label-Center" colspan="2">';
                        $HTML.= '               <p class="p-txt-label">';
                        $HTML.= '               REGISTRO DE PERMISO';
                        $HTML.= '               </p>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';   
                            
                        foreach($Datos[1] as $Dato)
                        {   $HTML.= '       <tr height="20px">';
                            $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col1.'">';
                            $HTML.= '               <p class="p-txt-label">Solicitud</p>';
                            $HTML.= '           </td>';
                            $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                            $HTML.=                '<p class="p-txt-label">'.$Dato['SOLICITUD']->format('d/m/Y').'</p>';
                            $HTML.= '           </td>';
                            $HTML.= '       </tr>';
                            $HTML.= '       <tr height="20px">';
                            $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col1.'">';
                            $HTML.= '               <p class="p-txt-label">Clases</p>';
                            $HTML.= '           </td>';
                            $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                            $HTML.=                '<p class="p-txt-label">'.$Dato['FECHA']->format('d/m/Y').'</p>';
                            $HTML.= '           </td>';
                            $HTML.= '       </tr>'; 
                            $HTML.= '       <tr height="20px">';
                            $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col1.'">';
                            $HTML.= '               <p class="p-txt-label">Materia</p>';
                            $HTML.= '           </td>';
                            $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                            $HTML.=                  '<p class="p-txt-label">'.utf8_encode($Dato['MATERIA']).'</p>';
                            $HTML.= '           </td>';
                            $HTML.= '       </tr>'; 
                            $HTML.= '       <tr height="20px">';
                            $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col1.'">';
                            $HTML.= '               <p class="p-txt-label">Grupo</p>';
                            $HTML.= '           </td>';
                            $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                            $HTML.=                '<p class="p-txt-label">'.$Dato['PARALELO'].'</p>';
                            $HTML.= '           </td>';
                            $HTML.= '       </tr>';                         
                            $HTML.= '       <tr height="20px">';
                            $HTML.= '           <td class="Form-Label-Center" valign="top" style="padding-top:5px;width:'.$Col1.'">';
                            $HTML.= '               <p class="p-txt-label">Motivo</p>';
                            $HTML.= '           </td>';
                            $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                            $HTML.= '               <textarea class="txt-upper" style="width: 97%; height: 50px; resize: none;" id="motivo" name="motivo" maxlength="250" '.($Dato['ID']>0 ? "disabled":"").'>'.$Dato['MOTIVO'].'</textarea>';
                            $HTML.= '           </td>';
                            $HTML.= '       </tr>'; 
                            $HTML.= '      <tr height="40px">';
                            $HTML.= '          <td class="Form-Label-Center" colspan="2">';
                            $HTML.= '              <input type="button" id="btaplica" class="p-txt-label" style="border: 1px solid #aaa; width: 56px; height: 25px" value="Aceptar" onclick=" return AplicaPermisoAsistencia('.$Dato['ID'].',motivo); ">';
                            $HTML.= '          </td>';
                            $HTML.= '      </tr>';
                        }    
                        $HTML.= '</table>';
                        return $HTML;
                }
                
        }
?>

