<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");    
        require_once("src/rules/maestria/servicio/ServicioAsistencia.php");
        require_once("src/modules/maestria/asistencia/render/RenderAsistencia.php");        
        require_once("src/rules/maestria/servicio/ServicioDocente.php");  
        require_once("src/rules/maestria/servicio/ServicioPlanAnalitico.php");
        require_once("src/rules/maestria/servicio/ServicioHorarioPlanificacion.php");
        require_once("src/modules/maestria/horario/render/RenderHorarioPlanificacion.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");

        class ControlAsistencia
        {       private $ServicioAsistencia;
                private $ServicioDocente;
                private $RenderAsistencia;
                private $ServicioPlanAnalitico;
                private $ServicioHorarioPlanificacion;
                private $RenderHorarioPlanificacion;
                private $ServicioUsuarioFiltro;
                
                function __construct()
                {       $this->ServicioAsistencia = new ServicioAsistencia();
                        $this->ServicioDocente = new ServicioDocente();        
                        $this->RenderAsistencia = new RenderAsistencia();
                        $this->ServicioPlanAnalitico = new ServicioPlanAnalitico();
                        $this->ServicioHorarioPlanificacion = new ServicioHorarioPlanificacion();
                        $this->RenderHorarioPlanificacion = new RenderHorarioPlanificacion();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                }

                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderAsistencia->CreaBarButton($Buttons[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderAsistencia->CreaMantenimiento();
                }

                function ConsultaByID($Params)
                {       $xAjax = new xajaxResponse();
                        $Asistencia = $this->ServicioAsistencia->ConsultaAsistencia($Params);
                        $SearchGrid = $this->RenderAsistencia->CreaSearchGrid($Asistencia,$Params[8]);
                      //Se desactiva guardar si no hay alumnos o no se permite edicion por ser asistencia pasada 
                        $xAjax->Assign("MinSav","disabled",(!$Asistencia[1][0]) || !$Params[8]); 
                        $xAjax->Assign("WrkGrd","innerHTML",$SearchGrid);
                        return $xAjax; 
                }
                
                function GuardaDB($Form,$Params)
                {       $xAjax = new xajaxResponse();
                        $Datos = $this->ServicioAsistencia->GuardaDB($Form);
                        if ($Datos[0])
                        {   $EjePln = $this->ServicioPlanAnalitico->EjecutaAsistenciaPlanificacion($Params,'CD'); //Se creo registro de Asistencia
                            $EjeAut = $this->ServicioAsistencia->EjecutaPermisoAsistencia($Params,'CD'); //Permiso Solicitado
                            $xAjax = $this->RenderAsistencia->MuestraGrid($xAjax,$Datos[1]);
                            $xAjax->Call("ElementStatus",($EjeAut ? "":"MinSav"),($EjeAut ? "MinSav":""));
                            $xAjax->Call("EstadoEjecucion",($EjePln ? "S":"N"),($Params[7]==0 ? 1:0));
                            $xAjax->Call("XAJAXResponse", "CMD_SAV", "La asistencia se registró con exito.");       
                        }
                        else
                        $xAjax->Call("XAJAXResponse", "CMD_ERR", "No se puedo actualizar la información."); 
                        return $xAjax;
                } 
                
                function GuardaPermiso($Params)
                {       $xAjax = new xajaxResponse();
                        $Datos = $this->ServicioAsistencia->GuardaPermisoAsistencia($Params,'CD');
                        if ($Datos[0])
                        {   $xAjax->Call("cerrar","");
                        }
                        else
                        {   $xAjax->Call("XAJAXResponse", "CMD_ERR", $Datos[1]);
                        }
                        $xAjax->Call("ElementStatus","btaplica","");
                        return $xAjax;
                }
                
                private function FilterOfAccess($Params)
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        $Docnte = $Params['dct'];

                        if ($Modulo->Idrol==3)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            if ($Docnte==0){
                                return array('texto'=>$Params['txt'], 'facultad'=>$Factad); 
                            }    
                            return array('texto'=>$Params['txt'], 'facultad'=>$Factad, 'docente'=>$Params['dct']);
                        }
                        else if ($Modulo->Idrol==2)
                        {   $Dcte = ($Docnte==0 ? $Modulo->Cedula : $Docnte); 
                            return array('texto'=>$Params['txt'], 'docente'=>$Dcte);
                        }
                        else
                        return array('facultad'=>array('00')); //No permiso a Estudiante
                }
                
                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion=="btdocente")
                        {   $Params = array('txt'=>'','dct'=>0);
                            $ArDatos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Docente";
                            $jsonModal['Carga'] = $this->RenderAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAsistencia->GetGridTableWidth();
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Params = array('txt'=>'','dct'=>$IdRef);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAsistencia->GetGridTableWidth();
                        }   
                        else if ($Operacion==="btmateria")    
                        {   $prepareDQL = array('docente'=>$IdRef[0],'ciclo'=>$IdRef[1],'maestria'=>$IdRef[2]);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Materia";
                            $jsonModal['Carga'] = $this->RenderAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAsistencia->GetGridTableWidth();
                        }   
                        else if ($Operacion=="bthorario")
                        {   $ArDatos = $this->ServicioHorarioPlanificacion->BuscarHorarioMateriaByPlanificacion($IdRef,'CD');
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Horario";
                            $jsonModal['Carga'] = $this->RenderAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAsistencia->GetGridTableWidth();
                        }   
                        else if ($Operacion=="MinOpn")
                        {   $Datos = $this->ServicioAsistencia->BuscarPermisoAsistencia($IdRef,'CD');
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Permiso";
                            $jsonModal['Carga'] = $this->RenderAsistencia->CrearPermiso($Datos);
                            $jsonModal['Ancho'] = 350;
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btdocente")    
                        {   $Params = array('txt'=>strtoupper(trim($Texto)),'dct'=>0);
                            $Datos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            return $this->RenderAsistencia->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $Params = array('txt'=>$Texto[0],'dct'=>$Texto[1]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            return $this->RenderAsistencia->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }                        
                        else if ($Operacion=="btmateria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $prepareDQL = array('texto'=>$Texto[0],'docente'=>$Texto[1],'ciclo'=>$Texto[2],'maestria'=>$Texto[3]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            return $this->RenderAsistencia->MuestraModalGrid($xAjax,$Operacion,$Datos);         
                        }
                }
        }

?>

