            var Edicion, Ejecuto, Cambios, ExFecha;
            var ColInit = 6;
            
            
            function SearchGetCell(Celda)
            {       var str = Celda.id;
                    var opt = str.split("_");
                    if (parseInt(opt[2])>=ColInit)
                    {   if (Edicion)
                        {   Cambios=1;
                            Celda.innerHTML = (Celda.innerHTML=="" ? "✔":"");
                        }    
                        else
                        {   Cambios=0;
                            if (Ejecuto=="S")
                            {   if (Celda.innerHTML!="✔")
                                    Celda.innerHTML = (Celda.innerHTML=="" ? "JUS":"");
                                else
                                    SAlert("error","¡Error!","Sólo se activa casillero para justificación de falta.");
                            }
                            else
                            SAlert("error","¡No hay asistencia!","En esta fecha no se registró asistencia.");
                        }    
                    }   
                    return false;
            }
    
            function EstadoEjecucion(flag,Edit)
            {       AUTHZ = 0;
                    Ejecuto = flag; 
                    Cambios = (Ejecuto=="S" ? 0:1);
                    Edicion = Edit;
            }
            
            function ButtonClick(Operacion)
            {       if (Operacion=="MinSav")
                    {    
                        if (Cambios==1)
                        {   Cambios=0;
                            var tabla = document.getElementById("DetSch");
                            if (tabla.rows.length>0)
                            {   //if (AUTHZ==1)
                                //confirm("Sólo podrá registrar la asistencia en esta fecha mientras la app este activa.");
                                ElementStatus("","MinSav");
                                var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,ExFecha,Ejecuto,AUTHZ];
                                xajax_GuardaDB(PreparaDatosGrid(tabla.rows.length),Params);
                            }    
                        }    
                    }
                    else if(Operacion=="MinOpn")
                    {    var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,ExFecha];
                         xajax_CargaModal(Operacion,Params);
                    }
                    else if (Operacion=="btdocente")
                    {   xajax_CargaModal(Operacion,0);
                    }
                    else if (Operacion=="btmaestria")
                    {    var IDDOC=ElementGetValue("iddocente");
                         if (IDDOC!="")
                            xajax_CargaModal(Operacion,IDDOC);
                         else
                            SAlert("warning","¡Cuidado!","Debe seleccionar un Docente.");
                    }
                    else if (Operacion=="btmateria")
                    {    var IDMST=ElementGetValue("idmaestria");
                         if (IDMST!="")
                         {   var IDDOC=ElementGetValue("iddocente");
                             var IDCLC=ElementGetValue("idciclo");
                             var Params = [IDDOC,IDCLC,IDMST];
                             xajax_CargaModal(Operacion,Params);
                         }
                         else
                            SAlert("warning","¡Cuidado!","Debe seleccionar una Maestria.");
                    }
                    else if (Operacion=="bthorario")
                    {    var IDMAT=ElementGetValue("idmateria");
                         if (IDMAT!="")
                         {   var IDDOC=ElementGetValue("iddocente");
                             var IDCLC=ElementGetValue("idciclo");
                             var IDMST=ElementGetValue("idmaestria");
                             var IDGRP=ElementGetValue("idgrupo");
                             var Params = [IDDOC,IDCLC,IDMST,IDMAT,IDGRP];
                             xajax_CargaModal(Operacion,Params);
                         }  
                         else
                         SAlert("warning","¡Cuidado!","Debe seleccionar una Materia.");
                    }
                    return false;
            }

            function SearchDocenteGetData(DatosGrid)
            {       var IDDOC = DatosGrid.cells[0].innerHTML;
                    if (ElementGetValue("iddocente")!=IDDOC)
                    {   ElementStatus("","MinSav:MinOpn");
                        ElementSetValue("WrkGrd","");
                        ElementClear("idmaestria:txmaestria:idciclo:idmateria:txmateria:idgrupo:txgrupo:idhorario:txhorario:txciclo");
                        document.getElementById("iddocente").value = IDDOC;
                        document.getElementById("txdocente").value = DatosGrid.cells[2].innerHTML;
                        cerrar();
                    }    
                    else
                    cerrar();
                    return false;
            }

            function SearchMaestriaGetData(DatosGrid)
            {       var IDMST = DatosGrid.cells[0].innerHTML;
                    if (ElementGetValue("idmaestria")!=IDMST)
                    {   ElementStatus("","MinSav:MinOpn");
                        ElementSetValue("WrkGrd","");
                        ElementClear("idmateria:txmateria:idgrupo:txgrupo:idhorario:txhorario");
                        document.getElementById("idmaestria").value = IDMST;
                        document.getElementById("idciclo").value = DatosGrid.cells[2].innerHTML;
                        document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML;
                        document.getElementById("txciclo").value = DatosGrid.cells[3].innerHTML;
                        cerrar();
                    }
                    else
                    cerrar();    
                    return false;
            }  
    
            function SearchMateriaGetData(DatosGrid)
            {       var IDGRP = DatosGrid.cells[2].innerHTML;
                    if (ElementGetValue("idgrupo")!=IDGRP)
                    {   ElementStatus("","MinSav:MinOpn");
                        ElementSetValue("WrkGrd","");
                        ElementClear("idhorario:txhorario");
                        document.getElementById("idmateria").value = DatosGrid.cells[0].innerHTML;
                        document.getElementById("txmateria").value = DatosGrid.cells[1].innerHTML;
                        document.getElementById("idgrupo").value = IDGRP;
                        document.getElementById("txgrupo").value = DatosGrid.cells[3].innerHTML;
                        cerrar();
                    }    
                    else    
                    cerrar();    
                    return false;
            }
    
            function SearchHorarioGetData(DatosGrid)
            {       var IDHOR = DatosGrid.cells[0].innerHTML; //secuencial solo de control
                    if (ElementGetValue("idhorario")!=IDHOR)
                    {   ElementSetValue("idhorario",IDHOR);
                        ElementSetValue("txhorario",DatosGrid.cells[3].innerHTML);
                            
                        var HOHOR=DatosGrid.cells[5].innerHTML;
                        if (HOHOR!="00:00")
                        {   ExFecha = DatosGrid.cells[4].innerHTML;
                            Ejecuto = DatosGrid.cells[1].innerHTML;
                            Edicion = parseInt(DatosGrid.cells[2].innerHTML);
                            
                            DOCTE=ElementGetValue("iddocente");
                            CICLO=ElementGetValue("idciclo");   
                            MAEST=ElementGetValue("idmaestria");
                            MATER=ElementGetValue("idmateria");
                            GRUPO=ElementGetValue("idgrupo");
                            AUTHZ=DatosGrid.cells[7].innerHTML;
                            PERSN=DatosGrid.cells[8].innerHTML;
                            
                            Cambios = (Ejecuto=="S" && PERSN!="N" ? 0:1);
                            if (Edicion==0 && Ejecuto=="N") ElementStatus("MinOpn",""); else ElementStatus("","MinOpn");
                            
                            var NUHOR=DatosGrid.cells[6].innerHTML;
                            var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,ExFecha,HOHOR,NUHOR,Edicion,PERSN];
                            cerrar();
                            xajax_BuscaByID(Params);
                        }    
                        else
                        SAlert("error","¡ERROR!","Existe un problema en la hora para el horario seleccionado. Revise la planificación");    
                    }
                    else
                    cerrar();
            }  

            function PreparaDatosGrid(Estudiantes)
            {       var MyTable = [];
                    for (t=1;t<=Estudiantes;t++)
                    {   var Cell = "TD_"+StrPadCeroNumber("00",t);
                        
                        if (t==1)
                        {   var fila = document.getElementById("TR_"+StrPadCeroNumber("00",t)).cells;
                            var tope = fila.length-1;
                        }
                        var Rows = t;
                        var Asis = document.getElementById(Cell+"_000").innerHTML;
                        var Esmt = document.getElementById(Cell+"_001").innerHTML;
                        var Fast = document.getElementById(Cell+"_002").innerHTML;
                        var Hhor = document.getElementById(Cell+"_003").innerHTML;
                       
                        var East = "";
                        var Eftv = 0;
                        var Just = 0;
                        var Falt = 0;
                        
                        for (s=ColInit;s<=tope;s++)
                        {    var Evln = document.getElementById(Cell+"_"+StrPadCeroNumber("000",s)).innerHTML;  
                             East = East+(s==ColInit ? "":"|")+(Evln=="✔" ? "1":(Evln=="JUS" ? "2":"0"));
                             if (Evln=="✔")
                                 Eftv = Eftv+1;
                             else if (Evln=="JUS")
                                 Just = Just+1;
                             else
                                 Falt = Falt+1;
                        }
                        
                        var MyFilas = [Rows,Asis,Esmt,Fast,Hhor,East,Eftv,Just,Falt];
                        MyTable.push(MyFilas);
                    }
                    return MyTable;            
            }
            
            function SearchByElement(Elemento)
            {       if (Elemento.id=="FindDocenteTextBx")
                    {    xajax_BuscaModalByTX("btdocente",Elemento.value);
                    }
                    else if (Elemento.id=="FindMaestriaTextBx")
                    {    var Params = [Elemento.value,ElementGetValue("iddocente")];
                         xajax_BuscaModalByTX("btmaestria",Params);
                    }
                    else if (Elemento.id=="FindMateriaTextBx")
                    {    var IDDOC=ElementGetValue("iddocente");
                         var IDCLC=ElementGetValue("idciclo");
                         var IDMST=ElementGetValue("idmaestria");
                         var Params = [Elemento.value,IDDOC,IDCLC,IDMST];
                         xajax_BuscaModalByTX("btmateria",Params);
                    }
                    return false;    
            }
            
            function AplicaPermisoAsistencia(Id,Texto)
            {       if (Id==0)
                    {   var text = Texto.value;
                        if (text.trim()!="")
                        {   ElementStatus("","btaplica");
                            var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,ExFecha,Texto.value];
                            xajax_GuardaPermiso(Params);
                        }
                        else
                        SAlert("warning","¡Cuidado!","Debe ingresar el motivo.");
                    }
                    else
                    cerrar();
                    return false;
            }

            function XAJAXResponse(Mensaje,Datos)
            {   if (Mensaje==="CMD_SAV")
                {   SAlert("success", "¡Excelente!", Datos);
                }
                else if(Mensaje==='CMD_DEL')
                {   SAlert("warning", "¡Cuidado!", Datos);    
                }
                else if(Mensaje==='CMD_WRM')
                {   SAlert("warning", "¡Cuidado!", Datos);
                }
                else if(Mensaje==='CMD_ERR')
                {   SAlert("error", "¡Error!", Datos);
                }
            }