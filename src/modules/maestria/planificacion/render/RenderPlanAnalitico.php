<?php
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php");

        class RenderPlanAnalitico
        {     private $Width;
              private $SearchGrid;
              private $Search;

            function __construct()
            {       $this->Search = new SearchInput();
                    $this->SearchGrid = new SearchGrid();
            }

            function CreaBarButton($Buttons)
            {       $BarButton = new BarButton();
                    return $BarButton->CreaBarButton($Buttons);
            }

            function CreaPlanAnaliticoMantenimiento()
            {
                    $Col1="16%"; $Col2="55%"; $Col3="16%"; $Col4="14%";
                    $HTML = '<table border=0 class="Form-Frame" cellpadding="0">';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Maestria</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                    $HTML.= '               <input type="hidden" id="id" name="id" value="" />';
                    $HTML.= '               <input type="hidden" id="idversion" name="idversion" value="" />';
                    $HTML.= '               <input type="hidden" id="idfacultad" name="idfacultad" value="" />';
                    $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t15");
                    $HTML.= '           </td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Estado</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                    $HTML.=                 $this->CreaComboEstadoByArray(array(0,1));
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Ciclo</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                    $HTML.= '               <input type="hidden" id="idmalla" name="idmalla" value="" />';
                    $HTML.= '               <input type="hidden" id="idcht" name="idcht" value="" />';
                    $HTML.=                 $this->Search->TextSch("cohorte","","")->Enabled(true)->Create("t15");
                    $HTML.= '           </td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Componente (CD)</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                    $HTML.= '               <input type="text" class="txt-input t02" id="compcd" name="compcd" value="" maxlength="2" readonly/>';
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Materia</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                    $HTML.= '               <input type="hidden" id="idmateria" name="idmateria" value="" />';
                    $HTML.=                 $this->Search->TextSch("matcont","","")->Enabled(true)->Create("t15");
                    $HTML.= '           </td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Componente (CPE)</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                    $HTML.= '               <input type="text" class="txt-input t02" id="compcp" name="compcp" value="" maxlength="2"  readonly/>';
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Grupo</td>';
                    $HTML.= '           <td id="cpgrupo" class="Form-Label" style="width:'.$Col2.'">';
                    $HTML.=                 $this->Search->TextSch("grupo","","")->Enabled(true)->Create("t08");
                    $HTML.= '           </td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Componente (CA)</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                    $HTML.= '               <input type="text" class="txt-input t02" id="compca" name="compca" value="" maxlength="2" readonly/>';
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Docente</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                    $HTML.=                 $this->Search->TextSch("docente","","")->Enabled(false)->Create("t15");
                    $HTML.= '           </td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Total Horas</td>';
                    $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                    $HTML.= '               <input type="text" class="txt-input t02" id="totalhoras" name="totalhoras" value="" maxlength="3" readonly/>';
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    return $HTML.'</table>';
            }

            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox();
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Fields("id","descripcion")->Selected(1)->Create("s04");
            }

            function MuestraPlanAnalitico($Ajax,$Datos)
            {       if ($Datos[0])
                    {   foreach ($Datos[1] as $Dato)
                        {       $Ajax->Assign("id","value",$Dato['ID']);
                                $Ajax->Assign("iddocente","value", $Dato['IDDOCENTE']);
                                $Ajax->Assign("txdocente","value",utf8_encode(trim($Dato['APELLIDOS'])." ".trim($Dato['NOMBRES'])));
                                $Ajax->Assign("estado","value", $Dato['IDESTADO']);
                                $Ajax->call("BarButtonState","Update");
                        }
                        if (count($Datos[2])>0)
                        {   foreach ($Datos[2] as $Dato)
                            {       $Celda[0] = $Dato['ID'];
                                    $Celda[1] = $Dato['SEMANA'];
                                    $Celda[2] = $Dato['FECHA']->format("d/m/Y");
                                    $Celda[3] = $Dato['IDCONTENIDO'];
                                    $Celda[4] = utf8_encode($Dato['SBCONTENIDO']);
                                    $Celda[5] = utf8_encode($Dato['DESCRIPCION']);
                                    $Celda[6] = $Dato['CD'];
                                    $Celda[7] = $Dato['CP'];
                                    $Celda[8] = $Dato['CA'];
                                    if ($Dato['IDESTADO']==6 || $Dato['HOREXCD']=="S")
                                    {   if ($Dato['IDESTADO']==6)
                                        {   $Color = "#c0392b";
                                            $Texto = "!!!";
                                        }
                                        else
                                        {   $Color = "#1f618d";
                                            $Texto = "✔";
                                        }
                                        $Rowst = array(0,$Color,$Texto);
                                    }
                                    else
                                        $Rowst = array(1,"black","...");
                                    $Celda[9] = $Rowst;
                                    $Ajax->call("CreaWorkGridFila","FX",$Celda);
                            }
                        }
                        return $Ajax;
                    }
                    $Ajax->call("BarButtonState","Default");
                    return $this->Respuesta($Ajax,"CMD_INF","No existe una planificación definida.");
            }

            function MuestraPlanAnaliticoDetalle($Ajax,$Datos)
            {       if ($Datos[0])
                    {   foreach ($Datos[1] as $Dato)
                        {       $Celda[0] = $Dato['ID'];
                                $Celda[1] = $Dato['SEMANA'];
                                $Celda[2] = $Dato['FECHA']->format("d/m/Y");
                                $Celda[3] = $Dato['IDCONTENIDO'];
                                $Celda[4] = utf8_encode($Dato['SBCONTENIDO']);
                                $Celda[5] = utf8_encode($Dato['DESCRIPCION']);
                                $Celda[6] = $Dato['CD'];
                                $Celda[7] = $Dato['CP'];
                                $Celda[8] = $Dato['CA'];
                                if ($Dato['IDESTADO']==6 || $Dato['HOREXCD']=="S")
                                {   if ($Dato['IDESTADO']==6)
                                    {   $Color = "#c0392b";
                                        $Texto = "!!!";
                                    }
                                    else
                                    {   $Color = "#1f618d";
                                        $Texto = "✔";
                                    }
                                    $Rowst = array(0,$Color,$Texto);
                                }
                                else
                                $Rowst = array(1,"black","...");
                                $Celda[9] = $Rowst;
                                $Ajax->call("CreaWorkGridFila","FX",$Celda);
                        }
                    }
                    return $Ajax;
            }

            function ActualizaPlanAnalitico($Ajax,$Plan,$Ejecuta)
            {       $Ajax->Assign("id","value",$Plan->id);
                    $Ajax->Assign("iddocente","value",$Plan->iddocente);
                  //$Ajax->Assign("txdocente","value",utf8_encode($Plan->txdocente));
                    $Ajax->Assign("estado","value",$Plan->estado);
                    if (!empty($Plan->iddetalle))
                    {   $Datos[0] = $Plan->iddetalle;
                        $Datos[1] = $Plan->semana;
                        $Datos[2] = $Plan->fecha;
                        $Datos[3] = $Plan->idcontenido;
                        $Datos[4] = $Plan->sbcontenido;
                        $Datos[5] = $Plan->txcontenido;
                        $Datos[6] = $Plan->componentecd;
                        $Datos[7] = $Plan->componentecp;
                        $Datos[8] = $Plan->componenteca;
                        $Datos[9] = "...";
                        $Ajax->call($Ejecuta,$Datos,$Ejecuta);
                    }
                    return $Ajax;
            }

            function ActualizaPlanAnaliticoCab($Ajax,$Plan)
            {       $Ajax->Assign("id","value", $Plan->id);
                  //$Ajax->Assign("idocente","value", $Plan->iddocente);
                    $Ajax->Assign("estado","value", $Plan->estado);
                    $this->Respuesta($Ajax,"CMD_SAV",$Plan->id);
                    return $Ajax;
            }

            function Respuesta($Ajax,$Tipo,$Data)
            {       $Ajax->call("XAJAXResponse",$Tipo,$Data);
                    return $Ajax;
            }

            function ComboSemana()
            {        $HTML = '<select id="semana" name="semana" class="sel-input" style="width:60px" onchange=" return ConsultaBySemana(this); " disabled>';
                     $HTML.= '<option value="1">Sem 1</option>';
                     $HTML.= '<option value="2">Sem 2</option>';
                     $HTML.= '<option value="3">Sem 3</option>';
                     $HTML.= '<option value="4">Sem 4</option>';
                     $HTML.= '<option value="5">Sem 5</option>';
                     $HTML.= '<option value="6">Sem 6</option>';

                     $HTML.= '<option value="7">Sem 7</option>';
                     $HTML.= '<option value="8">Sem 8</option>';
                     $HTML.= '<option value="9">Sem 9</option>';
                     $HTML.= '<option value="10">Sem 10</option>';
                     $HTML.= '<option value="11">Sem 11</option>';
                     $HTML.= '<option value="12">Sem 12</option>';

                     $HTML.= '<option value="13">Sem 13</option>';
                     $HTML.= '<option value="14">Sem 14</option>';
                     $HTML.= '<option value="15">Sem 15</option>';
                     $HTML.= '<option value="16">Sem 16</option>';
                     $HTML.= '<option value="17">Sem 17</option>';
                     $HTML.= '<option value="18">Sem 18</option>';

                     $HTML.= '<option value="19">Sem 19</option>';
                     $HTML.= '<option value="20">Sem 20</option>';
                     $HTML.= '<option value="21">Sem 21</option>';
                     $HTML.= '<option value="22">Sem 22</option>';
                     $HTML.= '<option value="23">Sem 23</option>';
                     $HTML.= '<option value="24">Sem 24</option>';

                     $HTML.= '<option value="25">Sem 25</option>';
                     $HTML.= '<option value="26">Sem 26</option>';
                     $HTML.= '<option value="27">Sem 27</option>';
                     $HTML.= '<option value="28">Sem 28</option>';
                     $HTML.= '</select>';
                     return $HTML;
            }
            //********************************************************************************************************
            private function WorkGridCellValues()
            {       $texto = $this->Search->TextSch("contenido","","")->Enabled(false)->Create("t15");
                    $Horas = $this->Search->TextSch("horario","","")->Enabled(false)->ReadOnly(false,'txt')->TypeBtn('success')->Create("s01");
                    $height = "15px";
                    $Columns[0] = array(0,'left','id','<input type="hidden" id="iddetalle" name="iddetalle" value="" />');
                    $Columns[1] = array(62,'center','sem',$this->ComboSemana());
                    $Columns[2] = array(70,'center','fecha','<input type="text" class="txt-center t02_3"  style="height:'.$height.'" id="fecha" name="fecha" value="" maxlength="10" disabled/>');
                    $Columns[3] = array(0,'left','id','');
                    $Columns[4] = array(410,'left','Contenido Temático',$texto);
                    $Columns[5] = array(0,'left','tx','<input type="hidden" id="sbcontenido" name="sbcontenido" value="" />');
                    $Columns[6] = array(33,'center','hora<br>cd','<input type="text" class="txt-input t01" style="height: '.$height.'" id="componentecd" name="componentecd" value="" maxlength="1" onKeyDown="return soloNumeros(event); " disabled/>');
                    $Columns[7] = array(33,'center','hora<br>cpe','<input type="text" class="txt-input t01" style="height:'.$height.'" id="componentecp" name="componentecp" value="" maxlength="2" onKeyDown="return soloNumeros(event); " disabled/>');
                    $Columns[8] = array(33,'center','hora<br>ca','<input type="text" class="txt-input t01" style="height: '.$height.'" id="componenteca" name="componenteca" value="" maxlength="2" onKeyDown="return soloNumeros(event); " disabled/>');
                    $Columns[9] = array(33,'center','H(x)',$Horas);
                    return $Columns;
            }

            function CreaWorkGrid($Ajax,$Capa)
            {       $WorkGrid["Capa"] = $Capa;
                    $WorkGrid["Hcab"] = "40";
                    $WorkGrid["Hcob"] = "1";
                    $WorkGrid["Hdet"] = "250";
                    $WorkGrid["Sufy"] = "";
                    $WorkGrid["Name"] = "FX";
                    $WorkGrid["Hrow"] = "24";
                    $WorkGrid["Erow"] = false;
                    $WorkGrid["btEd"] = true;
                    $WorkGrid["btDl"] = true;
                    $WorkGrid["Cell"] = $this->WorkGridCellValues();
                    $Ajax->call("CreaWorkGrid",json_encode($WorkGrid));
                    return $Ajax;
            }

                /* ESTO ES PARA LLENAR UN COMBRO GROUP UTIL
                private function ComboContenidoSgte($root)
                {       $HTML = "";
                        foreach($root as $sheet)
                        {       if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                {   $THTM = $this->ComboContenidoSgte($sheet->hijos);
                                    if ($THTM !="")
                                    {   $HTML.= '<optgroup label="'.$sheet->orden.' - '.$sheet->descripcion.'">';
                                        $HTML.= $THTM;
                                        $HTML.= '</optgroup>';
                                    }
                                }
                                else
                                $HTML.= '<option value="'.$sheet->id.'">'.$sheet->descripcion.'</option>';
                        }
                        return $HTML;
                }

                public function ComboContenido($root)
                {      $HTML = '<select id="contenido" name="contenido" class="sel-input" style="width:398px">';
                       foreach($root as $sheet)
                       {       if ($sheet->idtipo==0 || $sheet->idtipo==1)
                               {    $THTM = $this->ComboContenidoSgte($sheet->hijos);
                                    if ($THTM !="")
                                    {   $HTML.= '<optgroup label="'.$sheet->orden.' - '.$sheet->descripcion.'">';
                                        $HTML.= $THTM;
                                        $HTML.= '</optgroup>';
                                    }
                               }
                               else
                               $HTML.= '<option value="'.$sheet->id.'">'.$sheet->descripcion.'</option>';
                       }
                       $HTML.= '</select>';
                       return $HTML;
                }*/

               /*** MODAL ***/
                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']    =array('30px','center','none');
                            $Columns['Maestria']=array('300px','left','');
                            $Columns['V.ID']    =array('0px','left','none');
                            $Columns['Version'] =array('50px','left','');
                            $Columns['Facultad']=array('200px','left','');
                            $Columns['F.ID']=array('0px','left','none');
                        }
                        else if ($Operacion=="btcohorte")
                        {   $Columns['ID']    =array('40px','center','');
                            $Columns['MLL.ID']=array('0px','center','none');
                            $Columns['Ciclo']=array('250px','left','');
                            $Columns['Malla']=array('150px','left','');
                            $Columns['CHT.ID']=array('0px','center','none');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px','FindTxt'=>false);
                        }
                        else if ($Operacion=="btmatcont")
                        {   $Columns['C.ID']   =array('45px','center','none');
                            $Columns['M.ID']   =array('40px','center','');
                            $Columns['Materia']=array('350px','left','');
                            $Columns['Orden']=array('60px','center','');
                            $Columns['Cd']= array('0px','center','none');
                            $Columns['Cp']= array('0px','center','none');
                            $Columns['Ca']= array('0px','center','none');
                            $Columns['Tt']= array('0px','center','none');
                            $Columns['Np']= array('0px','center','none');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '250px','AltoRow' => '20px','FindTxt'=>true);
                        }
                        else if ($Operacion=="btgrupo")
                        {   $Columns['Id'] = array('40px','center','');
                            $Columns['Grupo']= array('250px','left','');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px','FindTxt'=>false);
                        }
                        else if ($Operacion=="btdocente")
                        {   $Columns['Id'] = array('0px','center','none');
                            $Columns['Codigo'] = array('80px','center','');
                            $Columns['Docente']= array('300px','left','');
                            $Columns['Email']  = array('300px','left','');
                        }
                        else if ($Operacion=="btcontenido")
                        {   $Columns['Id'] = array('50px','center','');
                            $Columns['Ind']  = array('50px','center','');
                            $Columns['Contenido Temático']  = array('500px','justify','');
                            $Columns['tipo']  = array('0px','left','none');
                            $Columns['txtd']  = array('0px','left','none');
                            $Columns['ruta']  = array('0px','left','none');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '300px','AltoRow' => '20px','FindTxt'=>false);
                        }
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth();
                        return $ObModalGrid;
                }

                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = str_pad($ArDato['IDM'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $idV = $ArDato['IDV'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idV);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['TXVERSION'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['FACULTAD'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['IDFACULTAD']);
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }
                            elseif ($Operacion=="btcohorte")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDCICLO'],CICLO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMALLA']);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['CICLO']));
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MALLA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDCOHORTE']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmatcont")
                            {   $Nivel = -1;
                                foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMATERIA'],MATERIA,'0',STR_PAD_LEFT);
                                        $mc = str_pad($ArDato['IDMATCONT'],MAESCON,'0',STR_PAD_LEFT);
                                        $nv = str_pad($ArDato['NIVELORDEN'],2,'0',STR_PAD_LEFT);
                                        if ($ArDato['NIVEL']!=$Nivel)
                                        {   $Nivel = $ArDato['NIVEL'];
                                            $Grid->CreaSearchCellsDetalle($id,'','');
                                            $Grid->CreaSearchCellsDetalle($id,'','');
                                            $Grid->CreaSearchCellsDetalle($id,'',"<b> CICLO ".$Nivel."</b>",2,0);
                                            $Grid->CreaSearchRowsDetalle ($id,"",0);
                                        }
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMATCONT']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'','M: '.$nv);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CD']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CP']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CA']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['TOTAL']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['NIVPLAN']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btgrupo")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['ID'],GRUPO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['PARALELO'])));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btdocente")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['CEDULA'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['APELLIDOS'])).' '.utf8_encode(trim($ArDato['NOMBRES'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['EMAIL'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }
                            elseif ($Operacion=="btcontenido")
                            {      return $this->GridDataContenidoSgte($Grid,$Contenido,0);
                            }
                        }
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }

                function GridDataContenidoSgte($grid,$root,$nivel,$tpniv=0)
                {       $count = 0;
                        foreach($root as $sheet)
                        {       $count++;
                                $id = str_pad($sheet->id,MAESCON,'0',STR_PAD_LEFT);
                                $grid->CreaSearchCellsDetalle($id,'',$id);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->orden);

                                if ($nivel==0) $tpniv = $sheet->tpniv;
                                $dato = $this->FormatGridData($nivel,$sheet->idtipo,$sheet->descripcion,$tpniv);
                                $grid->CreaSearchCellsDetalle($id,'',$dato['texto']);      //Muestra en consulta
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->idtipo);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->descripcion); //Pura para el text
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->ruta);        //Concatenada
                                $grid->CreaSearchRowsDetalle ($id,"black");
                                if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                $this->GridDataContenidoSgte($grid,$sheet->hijos,$nivel+1,$tpniv);
                        }
                        if ($nivel==0)
                        return $grid->CreaSearchTableDetalle(0,"");
                }

                private function FormatGridData($level,$types,$description,$tpniv)
                {       $Margen = $level*20;
                        if ($types==0)
                            $Texto = ($tpniv>0 ? "<b>".$description."</b>" : $description);
                        else if ($types==1)
                            $Texto = "<div style='padding-right:10px; padding-left:".$Margen."px'>".($tpniv>1 ? "<b>".$description."</b>" : $description)."</div>";
                        else if($types==2)
                            $Texto = "<div style='padding-right:10px; padding-left:".$Margen."px'>".$description."</div>";
                        return array('texto' => $Texto, 'margen' => 0);
                }

                function GetGridTableWidth()
                {        return $this->Width;
                }

                function CrearModalReplica()
                {       $Col1="20%"; $Col2="80%";

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="4">';
                        $HTML.= '       <tr height="30px">';
                        $HTML.= '           <td class="Form-Label-Center" colspan=2 valign="top" style="padding-top:5px;width:'.$Col2.'">';
                        $HTML.= '               <p class="p-txt-label" align="justify">¿CUANTAS VECES DESEA REPLICAR LOS REGISTROS?</p>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <p></p>';
                        $HTML.= '       <tr height="20px">';
                        $HTML.= '           <td class="Form-Label-Center" valign="top" style="padding-top:5px;width:'.$Col1.'">';
                        $HTML.= '               <p class="p-txt-label">Cantidad:</p>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '                <input type="input" class="txt-input t09" id="nVeces" name="nVeces" value="" maxlength="2" onKeyDown="return Dependencia(event,\'\',\'NUM\'); "/>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '      <tr height="40px">';
                        $HTML.= '          <td class="Form-Label-Center" colspan="2">';
                        $HTML.= '              <input type="button" id="btaplica" class="p-txt-label" style="border: 1px solid #aaa; width: 56px; height: 25px" value="Aceptar" onclick=" return ReplicarPlanificacion(); ">';
                        $HTML.= '          </td>';
                        $HTML.= '      </tr>';
                        $HTML.= '</table>';
                        return $HTML;
                }
        }
?>

