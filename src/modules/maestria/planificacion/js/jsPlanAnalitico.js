$(document).ready(function()
{       xajax_CargaWorkGrid("WrkGrd");
});

var ActFila;
var NivPlan;

function CargaFecha()
{   $('#fecha').datetimepicker({
        timepicker:false,
        format: 'd/m/Y'
    });
}

var status = "btdocente:txdocente:semana:fecha:btcontenido:txcontenido:componentecd:componentecp:componenteca:bthorario";
var statux = "txmaestria:txmatcont:txcohorte:txgrupo";
var disabl = "btmaestria:btmatcont:btcohorte:btgrupo";
var clsdet = "fecha:iddetalle:idcontenido:txcontenido:sbcontenido:componentecd:componentecp:componenteca";
var grupos = "idgrupo:txgrupo:iddocente:txdocente";

function ButtonClick(Operacion)
{       if (Operacion=='addNew')
        {   CargaFecha();
            BarButtonState(Operacion);
            BarButtonStateDisabled(disabl);
            ElementStatus(status, statux);
            ElementClear("id:iddocente:txdocente:"+clsdet);
            ElementSetValue("estado",1);
        }
        else if (Operacion=='addMod')
        {   var validar=status;
            CargaFecha();
            BarButtonState(Operacion);
            BarButtonStateDisabled(disabl);
            if (ElementGetValue("estado")!=1) validar=":estado";
            ElementStatus(validar,statux);
        }
        else if (Operacion=='addDel')
        {       if (ElementGetValue("estado")==1)
                {   Swal.fire({
                        title: '¿Inactivar planificación?',
                        text: "¿Desea inactivar esta planificación en su totalidad?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si, estoy seguro!'
                    }).then(function(result){
                        if (result.value) {
                            var Forma = PrepareElements("id:estado");
                            xajax_EliminaCab(ElementGetValue("id"));
                        }
                    });
                }
                else
                SAlert("error", "¡Planificación inactiva!", "La planificación se encuentra inactivo");
        }
        else if (Operacion=='addSav')
        {       if (ElementValidateBeforeSave("iddocente"))
                {   if (BarButtonState("Inactive"))
                    {   var Forma = PrepareElements("id:idmatcont:idmaestria:idmateria:idcohorte:iddocente:idgrupo:estado");
                        xajax_GuardaCab(JSON.stringify({Forma}));
                    }
                }
                else
                SAlert("warning", "¡Cuidado!", "Debe ingresar datos al docente.");
        }
        else if (Operacion=='addCan')
        {       ElementSetValue("estado",1);
                BarButtonStateEnabled(disabl);
                ElementStatus(statux,status);
                if (ElementGetValue("id")!="")
                {   BarButtonState("Update");
                    ElementClear(clsdet);
                }
                else
                {   BarButtonState("Inactive");
                    ElementClear("id:"+grupos+":"+clsdet);
                    DeleteWorkGrid("FX");
                }
        }
        else
        {   if (Operacion=='btmaestria')
            {   xajax_CargaModal(Operacion,0);
            }
            else if (Operacion=='btdocente')
            {   var IDFAC = ElementGetValue("idfacultad");
                if (IDFAC!="")
                    xajax_CargaModal(Operacion,IDFAC);
                else
                    SAlert("warning", "¡Cuidado!", "Selecione una maestria.");
            }
            else if (Operacion=="btcohorte")
            {   var IDVER = ElementGetValue("idversion");
                if (IDVER!="")
                    xajax_CargaModal(Operacion,IDVER);
                else
                    SAlert("warning", "¡Cuidado!", "Selecione una maestria con una versión vigente.");
            }
            else if (Operacion=="btgrupo")
            {   var IDCOH = ElementGetValue("idcohorte");
                if (IDCOH!="")
                    xajax_CargaModal(Operacion,IDCOH);
                else
                    SAlert("warning", "¡Cuidado!", "Selecione una cohorte.");
            }
            else if (Operacion=="btmatcont")
            {   var IDVER = ElementGetValue("idversion");
                var IDCLO = ElementGetValue("idcohorte");
                if (IDCLO!="")
                {   var Params = [IDVER,IDCLO];
                    xajax_CargaModal(Operacion,Params);
                }
                else
                    SAlert("warning", "¡Cuidado!", "Selecione un ciclo con malla vigente.");
            }
            else if (Operacion=="btcontenido")
            {       var IdMtc = ElementGetValue("idmatcont");
                    var IdCht = ElementGetValue("idcht");
                    var Param = [ElementGetValue("id"),IdMtc,NivPlan,IdCht];
                    if (IdMtc!="")
                        xajax_CargaModal(Operacion,Param);
                    else
                    SAlert("warning", "¡Cuidado!", "Debe selecccionar una maestria");
            }
            else if (Operacion=="bthorario")
            {       var idPln = ElementGetValue("id");
                    if (idPln!=0 && idPln!="")
                        xajax_CargaModal(Operacion,idPln);
                    else
                    SAlert("warning", "¡Cuidado!", "Debe registrarse una planificación activa.");
            }
        }
        return false;
}

function AplicaHorario()
{       cerrar();
}

function SearchByElement(Elemento)
{       if (Elemento.id=="FindDocenteTextBx")
        {   var IDFAC = ElementGetValue("idfacultad");
            if (IDFAC!="")
            {   var Params = [IDFAC,Elemento.value];
                xajax_BuscaModalByTX("btdocente",Params);
            }
        }
        else if (Elemento.id=="FindMaestriaTextBx")
        {    xajax_BuscaModalByTX("btmaestria",Elemento.value);
        }
        else if (Elemento.id=="FindMatcontTextBx")
        {   var IDVER = ElementGetValue("idversion");
            var IDCLO = ElementGetValue("idcohorte");
            if (IDCLO!="")
            {   var Params = [IDVER,IDCLO,Elemento.value];
                xajax_BuscaModalByTX("btmatcont",Params);
            }
            else
            SAlert("error", "¡Error!", "Selecione un ciclo con malla vigente.");
        }
        return false;
}

function SearchMaestriaGetData(DatosGrid)
{   $("#BotonReplicar").remove();
    var IDVER = DatosGrid.cells[2].innerHTML;
        if (parseInt(IDVER)!=0)
        {   var IDMST = DatosGrid.cells[0].innerHTML;
            if (ElementGetValue("idmaestria")!=IDMST)
            {   BarButtonState("Inactive");
                ElementSetValue("estado",1);
                DeleteWorkGrid("FX");
                ElementClear("id:idfacultad:idversion:idmalla:idcohorte:txcohorte:idmateria:idmatcont:txmatcont:"+grupos+":compcd:compcp:compca:totalhoras:"+clsdet);
                document.getElementById("idversion").value = IDVER;
                document.getElementById("idfacultad").value = DatosGrid.cells[5].innerHTML;
                document.getElementById("idmaestria").value = IDMST;
                document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML+" ("+ DatosGrid.cells[3].innerHTML+")";
                cerrar();
            }
            else
            cerrar();
        }
        else
        SAlert("error","¡Error!","No se encuentra vigente una versión de contenido para la maestria seleccionada.");
        return false;
}

function SearchCohorteGetData(DatosGrid)
{       var IDCOH = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("idcohorte")!=IDCOH)
        {   var IDMLL = DatosGrid.cells[1].innerHTML;
            if (IDMLL!=0)
            {   BarButtonState("Inactive");
                ElementSetValue("estado",1);
                DeleteWorkGrid("FX");
                ElementClear("idmateria:idmatcont:txmatcont:"+grupos+":compcd:compcp:compca:totalhoras:"+clsdet);
                document.getElementById("idmalla").value = IDMLL;
                document.getElementById("idcohorte").value = IDCOH;
                document.getElementById("txcohorte").value = DatosGrid.cells[2].innerHTML;
                document.getElementById("idcht").value = DatosGrid.cells[4].innerHTML;
                cerrar();
            }
            else
            SAlert("error","¡Error!","No se puede planificar con una malla inactiva.");
        }
        else
        cerrar();
        return false;
}

function SearchMatcontGetData(DatosGrid)
{       var IDMTC = DatosGrid.cells[0].innerHTML;
        if (IDMTC!="")
        {   if (ElementGetValue("idmatcont")!=IDMTC)
            {   BarButtonState("Inactive");
                ElementSetValue("estado",1);
                DeleteWorkGrid("FX");
                ElementClear(grupos+":"+clsdet);
                document.getElementById("idmatcont").value = IDMTC;
                document.getElementById("idmateria").value = DatosGrid.cells[1].innerHTML;
                document.getElementById("txmatcont").value = DatosGrid.cells[2].innerHTML+" - "+DatosGrid.cells[3].innerHTML;
                document.getElementById("compcd").value = DatosGrid.cells[4].innerHTML;
                document.getElementById("compcp").value = DatosGrid.cells[5].innerHTML;
                document.getElementById("compca").value = DatosGrid.cells[6].innerHTML;
                document.getElementById("totalhoras").value = DatosGrid.cells[7].innerHTML;
                NivPlan = DatosGrid.cells[8].innerHTML;
                cerrar();
            }
            else
            cerrar();
        }
        else
        SAlert("error", "¡Error!", "Esta materia no tiene definido contenido temático.");
        return false;
}

function SearchGrupoGetData(DatosGrid)
{       var IDFCT = ElementGetValue("idfacultad");
        var PresentaBoton = "<div class='BotonReplicar' id='BotonReplicar' name='BotonReplicar'><input type='button' id='Replicar' name='Replicar' class='p-txt-label-bold' style='height: 35px;text-decoration: none;font-weight: 600;font-size: 15px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin: 0px auto 10px;' value='✔ Clonar Semana' onclick='return ModalReplicar();'></div>";

        if(IDFCT == 2)
            $(".divReplicar").prepend(PresentaBoton);

        var IDGRP = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("idgrupo")!=IDGRP)
        {   var IDMTC = ElementGetValue("idmatcont");
            var IDCOH = ElementGetValue("idcohorte");
            if (IDMTC!="" && IDCOH!="")
            {   var Param = [IDMTC,IDCOH,IDGRP,1];
                BarButtonState("Inactive");
                ElementSetValue("estado",1);
                ElementSetValue("semana",1);
                DeleteWorkGrid("FX");
                ElementClear("iddocente:txdocente:"+clsdet);
                document.getElementById("idgrupo").value = IDGRP;
                document.getElementById("txgrupo").value = DatosGrid.cells[1].innerHTML;
                cerrar();
                xajax_BuscaByID(Param);
            }
        }
        else
        cerrar();
        return false;
}

function SearchDocenteGetData(DatosGrid)
{       var IDDOC = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("iddocente")!=IDDOC)
        {   document.getElementById("iddocente").value = IDDOC;
            document.getElementById("txdocente").value = DatosGrid.cells[2].innerHTML;
            cerrar();
        }
        else
        cerrar();
        return false;
}

function SearchContenidoGetData(DatosGrid)
{       var IdTIP = DatosGrid.cells[3].innerHTML;
        if (IdTIP<=NivPlan)
            {   document.getElementById("iddetalle").value = "";
                document.getElementById("idcontenido").value = DatosGrid.cells[0].innerHTML;
                document.getElementById("txcontenido").value = DatosGrid.cells[4].innerHTML;
                document.getElementById("sbcontenido").value = ElementGetValue(DatosGrid.cells[5].id);
                cerrar();
            }
        else
            SAlert("error", "¡Error!", "No se permite seleccionar este nivel de detalle.");
        return false;
}

function ConsultaBySemana(Obj)
{       var IDPLN = parseInt(ElementGetValue("id"));
        if (IDPLN!=0)
        {   var Param = [IDPLN,Obj.value];
            DeleteWorkGrid("FX");
            ElementClear(clsdet);
            xajax_BuscaDetalleByID(Param);
        }
        return false;
}

function AddGridWorkGridRow(Datos)
{       ElementClear(clsdet);
        CreaWorkGridFila("FX",Datos);
}

function ClickHorario(cb,cp,pl,dy)
{       var params = [cb.value,cp,pl,dy];
        xajax_GuardaHorario(params);
        return false;
}

function WorkGridEditFX(Grilla,Celda)
{       if (!IsDisabled("addSav"))
        {   ActFila = Grilla;
            ElementSetValue("iddetalle",Grilla.cells[0].innerHTML);
            ElementSetValue("semana",Grilla.cells[1].innerHTML);
            ElementSetValue("fecha",Grilla.cells[2].innerHTML);
            ElementSetValue("idcontenido",Grilla.cells[3].innerHTML);
            ElementSetValue("txcontenido",Grilla.cells[5].innerHTML);
            ElementSetValue("sbcontenido",ElementGetValue(Grilla.cells[4].id));
            ElementSetValue("componentecd",Grilla.cells[6].innerHTML);
            ElementSetValue("componentecp",Grilla.cells[7].innerHTML);
            ElementSetValue("componenteca",Grilla.cells[8].innerHTML);
        }
        return false;
}

function UpdGridWorkGridRow(Datos)
{       ElementClear(clsdet);
        EditWorkGridFila(ActFila,Datos);
}

function WorkGridDeltFX(Grilla,Celda)
{       if (!IsDisabled("addSav"))
        {   Swal.fire({
                title: '¿Eliminar registro?',
                text: "¿Confirma eliminar este registro de la planificación?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, lo confirmo!'
            }).then(function(result){
                if (result.value) {
                    var id = Grilla.cells[0].innerHTML;
                    xajax_Elimina(id);
                    DeltWorkGridFila("FX",Grilla);
                }
            });
        }
        return false
}

function XAJAXResponse(Mensaje, Datos)
{       if (Mensaje=="CMD_SAV")
        {   BarButtonState("Update");
            BarButtonStateEnabled(disabl);
            ElementStatus(statux,"btdocente:txdocente:estado:semana:fecha:btcontenido:txcontenido:componentecd:componentecp:componenteca:bthorario");
            ElementClear(clsdet);
            SAlert("success","¡Excelente!", "Los datos se guardaron correctamente.");
        }
        else if (Mensaje === 'CMD_DEL')
        {    ElementSetValue("estado", Datos);
             SAlert("success", "¡Excelente!", "Los planificación quedó inactiva.");
        }
        else if (Mensaje === 'CMD_WRM')
        {    SAlert("warning", "¡Cuidado!", Datos);
        }
        else if (Mensaje === 'CMD_INF')
        {    SAlert("info", "¡Información!", Datos);
        }
        else if (Mensaje === 'CMD_ERR')
        {    BarButtonState("addNew");
             SAlert("warning", "¡Cuidado!", Datos);
        }
        else if (Mensaje === 'CMD_SAV_REPLICA')
        {   BarButtonState("Update");
            BarButtonStateEnabled(disabl);
            ElementStatus(statux,"btdocente:txdocente:estado:semana:fecha:btcontenido:txcontenido:componentecd:componentecp:componenteca:bthorario");
            ElementClear(clsdet);
            SAlert("success","¡Excelente!", "Los datos se guardaron correctamente.");
        }
        else if (Mensaje === 'CMD_WRM_REPLICA')
        {    SAlert("warning", "¡Cuidado!", "No se guardo correctamente los registros");
        }
}

function WorkGridAddsFX()
{       if (!IsDisabled("addSav"))
        {   if (ElementValidateBeforeSave("idgrupo:idmatcont:idcohorte:iddocente:estado:idcontenido:sbcontenido:fecha"))
            {   var cmpcd = parseInt(ElementGetValue("componentecd"));
                var cmpcp = parseInt(ElementGetValue("componentecp"));
                var cmpca = parseInt(ElementGetValue("componenteca"));
                if (cmpcd!=0 || cmpcp!=0 || cmpca!=0)
                {   var Forma = PrepareElements("id:idgrupo:idmatcont:idmaestria:idmateria:idcohorte:iddocente:estado:semana:"+clsdet);
                    xajax_Guarda(JSON.stringify({Forma}));
                }
                else
                SAlert("error", "¡Error!", "Debe ingresar por lo menos una hora en cualquier componente.");
            }
            else
            SAlert("error","¡Error!", "Falta ingresar datos para guardar un item de la planificación.");
        }
        return false;
}

function ModalReplicar(){
    if (!IsDisabled("addSav")){
        if (ElementValidateBeforeSave("idgrupo:idmatcont:idcohorte:iddocente:estado")){
            xajax_CargaModal("jModal","");
        }
        else
        SAlert("error","¡Error!", "Falta ingresar datos para guardar un item de la planificación.");
    }
    return false;
}

function ReplicarPlanificacion(){
    if (ElementValidateBeforeSave("nVeces")){
        var Forma = PrepareElements("id:iddetalle:nVeces:compcd:compcp:compca:idgrupo:idmatcont:idmaestria:idmateria:idcohorte:iddocente:estado:semana");
        xajax_ReplicarPlanificacion(JSON.stringify({Forma}));
        cerrar();
    }
    else{
        cerrar();
        SAlert("error","¡Error!", "Falta ingresar datos para guardar un item de la planificación.");
    }
}