<?php
        $Opcion = 'PlanAnalitico';
        require_once('src/modules/maestria/planificacion/controlador/ControlPlanAnalitico.php');
        $ControlPlanAnalitico = new ControlPlanAnalitico();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlPlanAnalitico,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaCab', $ControlPlanAnalitico,'GuardaCab'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlPlanAnalitico,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('EliminaCab', $ControlPlanAnalitico,'EliminaCab'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlPlanAnalitico,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaDetalleByID', $ControlPlanAnalitico,'ConsultaDetalleByID'));

        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlPlanAnalitico,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('CargaWorkGrid', $ControlPlanAnalitico,'CargaWorkGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlPlanAnalitico,'ConsultaGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaHorario', $ControlPlanAnalitico,'GuardaHorario'));

        $xajax->register(XAJAX_FUNCTION,array('ReplicarPlanificacion', $ControlPlanAnalitico,'ReplicarPlanificacion'));
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="Content-Type" content="Mime-Type; charset=UTF-8"/>
            <title></title>
            <?php   $xajax->printJavascript();
                    require_once('src/utils/links.php');
            ?>
            <script type="text/javascript" src="src/modules/maestria/planificacion/js/jsPlanAnalitico.js?20"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:800px">
                    <div class="FormSectionMenu">
                        <?php   $ControlPlanAnalitico->CargaPlanAnaliticoBarButton($_GET['opcion']);
                                echo '<script type="text/javascript">';
                                echo '        BarButtonState("Inactive"); ';
                                echo '</script>';
                        ?>
                    </div>
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionData">
                             <?php  $ControlPlanAnalitico->CargaPlanAnaliticoMantenimiento();  ?>
                        </div>
                        <div id="<?php echo 'WrkGrd'; ?>" class="FormSectionGrid">
                        </div>
                        <div class="center-items divReplicar" id="divReplicar" name="divReplicar" style="text-align:center;">
                                <!--<div class="BotonReplicar">
                                        <input type="button" id="Replicar" name="Replicar" class="p-txt-label-bold" style="height: 35px;text-decoration: none;font-weight: 600;font-size: 15px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin: 0px auto 10px;" value="✔ Replicar" onclick="return ModalReplicar();">
                                </div>-->
                        </div>
                    </form>
                </div>
        </body>
        </html>