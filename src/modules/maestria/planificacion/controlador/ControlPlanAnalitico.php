<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioPlanAnalitico.php");
        require_once("src/modules/maestria/planificacion/render/RenderPlanAnalitico.php");
        require_once("src/rules/maestria/servicio/ServicioMatInformacion.php");
        require_once("src/rules/maestria/servicio/ServicioMatContenido.php");
        require_once("src/rules/maestria/servicio/ServicioDocente.php");
        require_once("src/rules/maestria/servicio/ServicioCiclo.php");
        require_once("src/rules/maestria/servicio/ServicioMaestria.php");
        require_once("src/rules/maestria/servicio/ServicioGrupo.php");
        require_once("src/rules/maestria/servicio/ServicioHorarioPlanificacion.php");
        require_once("src/modules/maestria/horario/render/RenderHorarioPlanificacion.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");

        class ControlPlanAnalitico
        {       private $ServicioPlanAnalitico;
                private $ServicioDocente;
                private $ServicioMatContenido;
                private $ServicioCiclo;
                private $RenderPlanAnalitico;
                private $ServicioMaestria;
                private $ServicioMatInformacion;
                private $ServicioGrupo;
                private $ServicioHorarioPlanificacion;
                private $RenderHorarioPlanificacion;
                private $ServicioUsuarioFiltro;

                function __construct()
                {       $this->ServicioMatContenido = new ServicioMatContenido();
                        $this->ServicioPlanAnalitico = new ServicioPlanAnalitico();
                        $this->ServicioDocente = new ServicioDocente();
                        $this->RenderPlanAnalitico = new RenderPlanAnalitico();
                        $this->ServicioCiclo = new ServicioCiclo();
                        $this->ServicioMaestria = new ServicioMaestria();
                        $this->ServicioMatInformacion = new ServicioMatInformacion();
                        $this->ServicioGrupo = new ServicioGrupo();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                        $this->ServicioHorarioPlanificacion = new ServicioHorarioPlanificacion();
                        $this->RenderHorarioPlanificacion = new RenderHorarioPlanificacion();
                }

                function CargaPlanAnaliticoBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderPlanAnalitico->CreaBarButton($Buttons[1]);
                }

                function CargaPlanAnaliticoMantenimiento()
                {       echo $this->RenderPlanAnalitico->CreaPlanAnaliticoMantenimiento();
                }

                function CargaWorkGrid($Capa)
                {       $xAjax = new xajaxResponse();
                        return $this->RenderPlanAnalitico->CreaWorkGrid($xAjax,$Capa);
                }

                function ConsultaByID($Params)
                {       $xAjax = new xajaxResponse();
                        $Datos = $this->ServicioPlanAnalitico->BuscarPlanAnalitico($Params);
                        return $this->RenderPlanAnalitico->MuestraPlanAnalitico($xAjax,$Datos);
                }

                function ConsultaDetalleByID($Params)
                {       $xAjax = new xajaxResponse();
                        $Datos = $this->ServicioPlanAnalitico->BuscarPlanAnaliticoDetalle($Params[0],$Params[1]);
                        return $this->RenderPlanAnalitico->MuestraPlanAnaliticoDetalle($xAjax,$Datos);
                }

                function GuardaCab($Form)
                {       $xAjax = new xajaxResponse();
                        $Planificacion  = json_decode($Form)->Forma;
                        $Rsltd = $this->ServicioPlanAnalitico->GuardaDBCab($Planificacion);
                        if ($Rsltd[0])
                            return $this->RenderPlanAnalitico->ActualizaPlanAnaliticoCab($xAjax,$Rsltd[1]);
                        else
                            return $this->RenderPlanAnalitico->Respuesta($xAjax,"CMD_ERR",$Rsltd[1]);
                }

                function Elimina($id)
                {       $xAjax = new xajaxResponse();
                        $Rsltd = $this->ServicioPlanAnalitico->EliminaDetallePlanAnalitico($id);
                        return $xAjax;
                }

                function EliminaCab($id)
                {       $xAjax = new xajaxResponse();
                        $Rsltd = $this->ServicioPlanAnalitico->EliminaPlanAnalitico($id);
                        return $this->RenderPlanAnalitico->Respuesta($xAjax,"CMD_DEL",0);
                }

                private function FilterOfAccess($Params)
                {       $Modulo = json_decode(Session::getValue('Sesion'));
                        $Factad = $Params['fac'];
                        if ($Modulo->Idrol==3)
                        {   if ($Factad[0]=="00")
                                $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            return array('texto'=>$Params['txt'], 'facultad'=>$Factad);
                        }
                        else if($Modulo->Idrol==2)
                        {    if ($Factad[0]=="00")
                                 $Factad = $this->ServicioUsuarioFiltro->BuscarDocenteFiltroByCedula($Modulo->Cedula);
                             return array('texto'=>$Params['txt'], 'facultad'=>$Factad, 'docente'=>$Modulo->Cedula);
                        }
                        else
                        return array('facultad'=>$Factad); //No permiso a Estudiante
                }

                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();
                        if ($Operacion=="btmaestria")
                        {   $Params = array('txt'=>'','fac'=>array('00'));
                            $ArDatos = $this->ServicioMatInformacion->BuscarMaestriaVersionByFacultadSinCohorte($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion);
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderPlanAnalitico->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderPlanAnalitico->GetGridTableWidth();
                        }
                        elseif ($Operacion==="btcohorte")
                        {   $ArDatos = $this->ServicioCiclo->BuscarCicloByMaestriaVersion($IdRef);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion);
                            $jsonModal['Title'] = "Busca Ciclo-Cohorte";
                            $jsonModal['Carga'] = $this->RenderPlanAnalitico->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderPlanAnalitico->GetGridTableWidth();
                        }
                        elseif ($Operacion==="btmatcont")
                        {   $prepareDQL = array('maesver'=>$IdRef[0],'ciclo'=>$IdRef[1]);
                            $ArDatos = $this->ServicioMatInformacion->BuscarMateriaContenidoByMalla($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion);
                            $jsonModal['Title'] = "Busca Materia Malla";
                            $jsonModal['Carga'] = $this->RenderPlanAnalitico->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderPlanAnalitico->GetGridTableWidth();
                        }
                        elseif ($Operacion==="btgrupo")
                        {   $ArDatos = $this->ServicioGrupo->BuscarGrupoByCiclo($IdRef);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion);
                            $jsonModal['Title'] = "Busca Grupo Ciclo/Cohorte";
                            $jsonModal['Carga'] = $this->RenderPlanAnalitico->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderPlanAnalitico->GetGridTableWidth();
                        }
                        else if ($Operacion=="btdocente")
                        {   $Params = array('txt'=>'','fac'=>array($IdRef));
                            $ArDatos = $this->ServicioDocente->BuscarDocente($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion);
                            $jsonModal['Title'] = "Busca Docente";
                            $jsonModal['Carga'] = $this->RenderPlanAnalitico->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderPlanAnalitico->GetGridTableWidth();
                        }
                        else if ($Operacion=="bthorario")
                        {   $Horario = $this->ServicioHorarioPlanificacion->BuscarHorario($IdRef);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion);
                            $jsonModal['Title'] = "Horario Planificacion";
                            $jsonModal['Carga'] = $this->RenderHorarioPlanificacion->CreaEditorHorario($Horario);
                            $jsonModal['Ancho'] = 400;
                        }
                        else if ($Operacion=="btcontenido")
                        {   $ArDatos = json_decode($this->ServicioMatContenido->BuscarContenidoByIndice($IdRef));
                            if (count($ArDatos)>0)
                            {   $jsonModal['Modal'] = "Modal".strtoupper($Operacion);
                                $jsonModal['Title'] = "Busca Contenido Temático";
                                $jsonModal['Carga'] = $this->RenderPlanAnalitico->CreaModalGrid($Operacion,$ArDatos);
                                $jsonModal['Ancho'] = $this->RenderPlanAnalitico->GetGridTableWidth();
                            }
                            else
                            {   $ajaxResp->call("XAJAXResponse","CMD_WRM","Se ha planificado el totalidad del contenido temático.");
                                return $ajaxResp;
                            }
                        }
                        else if ($Operacion==="jModal")
                        {   $jsonModal['Modal'] = "ModalReplicar";
                            $jsonModal['Title'] = "Replicar Registro";
                            $jsonModal['Carga'] = $this->RenderPlanAnalitico->CrearModalReplica();
                            $jsonModal['Ancho'] = "300";
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp;
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $ajaxResp = new xajaxResponse();
                        if ($Operacion==="btmaestria")
                        {   $Params = array('txt'=>strtoupper(trim($Texto)),'fac'=>array("00"));
                            $Datos = $this->ServicioMatInformacion->BuscarMaestriaVersionByFacultadSinCohorte($this->FilterOfAccess($Params));
                            return $this->RenderPlanAnalitico->MuestraModalGrid($ajaxResp,$Operacion,$Datos);
                        }
                        if ($Operacion==="btdocente")
                        {   $Params = array('txt'=>strtoupper(trim($Texto[1])),'fac'=>array($Texto[0]));
                            $Datos = $this->ServicioDocente->BuscarDocente($this->FilterOfAccess($Params));
                            return $this->RenderPlanAnalitico->MuestraModalGrid($ajaxResp,$Operacion,$Datos);
                        }
                        if ($Operacion==="btmatcont")
                        {   $prepareDQL = array('maesver'=>$Texto[0],'ciclo'=>$Texto[1],'texto'=> strtoupper(trim($Texto[2])));
                            $Datos = $this->ServicioMatInformacion->BuscarMateriaContenidoByMalla($prepareDQL);
                            return $this->RenderPlanAnalitico->MuestraModalGrid($ajaxResp,$Operacion,$Datos);
                        }
                }

                function GuardaHorario($Params)
                {       $Rsltd = $this->ServicioHorarioPlanificacion->GuardaHorarioDB($Params);
                        if ($Rsltd[0])
                        {
                        }
                }

                function Guarda($Form)
                {       $xAjax = new xajaxResponse();
                        $Planificacion  = json_decode($Form)->Forma;
                        $PlanifDetalle = (empty($Planificacion->iddetalle) ? "AddGridWorkGridRow":"UpdGridWorkGridRow");
                        $Rsltd = $this->ServicioPlanAnalitico->GuardaDB($Planificacion);
                        if ($Rsltd[0])
                            $xAjax = $this->RenderPlanAnalitico->ActualizaPlanAnalitico($xAjax,$Rsltd[1],$PlanifDetalle);
                        else
                            $xAjax->call("XAJAXResponse","CMD_WRM",$Rsltd[1]);
                        return $xAjax;
                }

                function ReplicarPlanificacion($Form){
                    $xAjax = new xajaxResponse();
                    $Planificacion  = json_decode($Form)->Forma;
                    //$PlanifDetalle = (empty($Planificacion->iddetalle) ? "AddGridWorkGridRow":"UpdGridWorkGridRow");
                    $Rsltd = $this->ServicioPlanAnalitico->ReplicaPlanificacionDB($Planificacion);
                    if ($Rsltd[0])
                        $xAjax->call("XAJAXResponse","CMD_SAV_REPLICA",$Rsltd[1]);
                    else
                        $xAjax->call("XAJAXResponse","CMD_WRM_REPLICA",$Rsltd[1]);
                    return $xAjax;
            }
        }

?>

