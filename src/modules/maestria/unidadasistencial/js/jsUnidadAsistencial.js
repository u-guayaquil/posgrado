var objParalelo = "id:descripcion:estado:txcohorte:idcohorte:txvmaestria:idvmaestria";
var valParalelo = "txcohorte:descripcion";
    
function ButtonClick(Operacion)
{ 
        var objParalelo = "id:descripcion:estado:txcohorte:idcohorte:capacidad";
        var valParalelo = "txcohorte:descripcion:capacidad";
        
        if (Operacion==='addNew')
        {   BarButtonState(Operacion);
            ElementStatus("descripcion:capacidad","id:txcohorte:btcohorte:txvmaestria:btvmaestria");
            ElementClear("id:descripcion:capacidad");
            ElementSetValue("estado",1);
        }
        else if (Operacion==='addMod')
        {       BarButtonState(Operacion);
                if (ElementGetValue("estado")==0)
                {   
                    BarButtonStateEnabled("btcohorte");
                    valParalelo = valParalelo + ":estado";
                }
                
                ElementStatus(valParalelo,"id:txcohorte:btcohorte:txvmaestria:btvmaestria");
        }
        else if (Operacion==='addDel')
        {       if (ElementGetValue("estado")==1)
                {   if (confirm("Desea inactivar este registro?"))
                    {   var Forma = PrepareElements("id:estado");
                        xajax_Elimina(JSON.stringify({Forma}));
                    }    
                }
                else{SAlert("warning","¡Registro Inactivo!","El registro se encuentra inactivo");}
        }
        else if (Operacion==='addSav')
        {       if (ElementValidateBeforeSave(valParalelo))
                {   if (BarButtonState("Inactive"))
                    {   var Forma = PrepareElements(objParalelo);
                        xajax_Guarda(JSON.stringify({Forma}));
                    }
                }
                else
                {SAlert("warning","¡Cuidado!","Llene los campos por favor");}
        }
        else if (Operacion==='addCan')
        {       BarButtonState(Operacion);
                ElementStatus("id:btcohorte:txcohorte:txvmaestria:btvmaestria","descripcion:capacidad:estado");
                ElementClear("id:descripcion:capacidad");
                ElementSetValue("estado",1);
        }
        else if (Operacion==='btcohorte')
        {   var Forma = PrepareElements("idvmaestria");
            xajax_CargaModalCohorte(Operacion, JSON.stringify({Forma}));
        }
        else 
        {xajax_CargaModal(Operacion);}
        return false;
}

function XAJAXResponse(Mensaje,Datos)
{       if (Mensaje==="CMD_SAV")
        {   BarButtonState("Default");
            ElementSetValue("id",Datos);
            ElementClear("id:descripcion:capacidad");
            ElementStatus("id:btcohorte:txcohorte:txvmaestria:btvmaestria","descripcion:capacidad:estado");
            ElementSetValue("estado",1);
            SAlert("success","¡Excelente!","Los datos se guardaron correctamente.");
        }
        else if (Mensaje==="CMD_MAE")
        {   
            if (ValidateStatusEnabled("addNew")){
                BarButtonState("Default");
                ElementStatus("id:txcohorte","descripcion:capacidad:estado");
                ElementClear("id:descripcion:capacidad");
                ElementSetValue("estado",1);
            }
            else if(ValidateStatusEnabled("addSave")){
                BarButtonState("Active");
                ElementStatus("descripcion:capacidad","id:estado:txcohorte");
            }
            else {
                BarButtonState("Default");
                ElementStatus("id:txcohorte:btcohorte:txvmaestria:btvmaestria","descripcion:capacidad:estado");
                ElementClear("id:descripcion:capacidad");
                ElementSetValue("estado",1);
            }
        }
        else if (Mensaje==="CMD_MAE_ERR")
        {   
            if (ValidateStatusEnabled("addNew")){
            }
            else {
                ElementStatus("descripcion:capacidad","id:estado:txcohorte:btcohorte:txvmaestria:btvmaestria");
            }
        }
        else if (Mensaje==="CMD_VM")
        {
                BarButtonState("Inactive");
                ElementStatus("id:txcohorte:btcohorte:txvmaestria:btvmaestria","descripcion:capacidad:estado");
                ElementClear("id:descripcion:capacidad");
                ElementSetValue("estado",1);
        }
        else if (Mensaje==="CMD_VM_ERR")
        {
            BarButtonState("Inactive");
            SAlert("error", "¡Error!", Datos);
        }
        else if(Mensaje==='CMD_ERR')
        {   BarButtonState("addNew");
            SAlert("error","¡Error!",Datos); 
        }    
        else if(Mensaje==='CMD_DEL')
        {   ElementSetValue("estado",Datos);
            SAlert("success","¡Excelente!","Los datos se guardaron correctamente");
        }
        else if(Mensaje==="CMD_WRM")
        {   BarButtonState("Default");
            ElementClear("id");
            ElementSetValue("estado",1);
            SAlert("warning","¡Cuidado!", Datos); 
        }
}

function SearchByElement(Elemento)
{       Elemento.value = TrimElement(Elemento.value);
        if (Elemento.name=="id")
        {   if (Elemento.value.length != 0)
            {   ContentFlag = document.getElementById("descripcion");
                if (ContentFlag.value.length==0)
                {   if (BarButtonState("Inactive"))
                    {
                        var Forma = PrepareElements("id:idcohorte");
                        xajax_BuscaByIDByCohorte(JSON.stringify({Forma}));
                    }
                }
            }
            else
            BarButtonState("Default"); 
        }
        else if (Elemento.id === "FindVmaestriaTextBx")
        {   
            xajax_BuscaModalByTX("btvmaestria",Elemento.value);
        }
        else if (Elemento.id === "FindCohorteTextBx")
        {   var Params = [Elemento.value,ElementGetValue("idvmaestria")];
            xajax_BuscaModalByTX("btcohorte",Params);
        } 
        else if(Elemento.id === "FindTextBx")
        {
            var Forma = PrepareElements("FindTextBx:idcohorte");
            xajax_BuscaByTXByCohorte(JSON.stringify({Forma}));
        }
}

function SearchVmaestriaGetData(DatosGrid)
{
        document.getElementById("idvmaestria").value = DatosGrid.cells[3].childNodes[0].nodeValue;
        document.getElementById("txvmaestria").value = DatosGrid.cells[2].childNodes[0].nodeValue.toUpperCase();
        document.getElementById("version").value = DatosGrid.cells[4].childNodes[0].nodeValue;
        cerrar();
        ElementStatus("id:txcohorte:btcohorte:txvmaestria:btvmaestria","descripcion:capacidad:estado");
        ElementClear("id:descripcion:capacidad:idcohorte:txcohorte");
        var Forma = PrepareElements(objParalelo);
        xajax_MostrarbyVMaestria(JSON.stringify({Forma}));
        return false;
}

function SearchCohorteGetData(DatosGrid)
{
        document.getElementById("idcohorte").value = DatosGrid.cells[0].childNodes[0].nodeValue;
        document.getElementById("txcohorte").value = DatosGrid.cells[1].childNodes[0].nodeValue.toUpperCase();
        cerrar();
        var Forma = PrepareElements(objParalelo);
        xajax_MostrarbyCohorte(JSON.stringify({Forma}));
        return false;
}
    
function SearchGetData(Grilla)
{       if (IsDisabled("addSav"))
        {   BarButtonState("Active");
            document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
            document.getElementById("descripcion").value = Grilla.cells[1].childNodes[0].nodeValue.toUpperCase();
            document.getElementById("capacidad").value = Grilla.cells[2].childNodes[0].nodeValue; 
            document.getElementById("estado").value = Grilla.cells[6].childNodes[0].nodeValue; 
        }
        return false;
}