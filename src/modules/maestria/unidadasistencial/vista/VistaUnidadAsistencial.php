<?php   $Opcion = 'UnidadAsistencial';

        require_once('src/modules/maestria/paralelo/controlador/ControlParalelo.php');
        $ControlParalelo = new ControlParalelo();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlParalelo,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlParalelo,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlParalelo,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlParalelo,'ConsultaByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlParalelo,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModalCohorte', $ControlParalelo,'CargaModalGridCohorte'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlParalelo,'ConsultaModalGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('MostrarbyCohorte', $ControlParalelo,'MostrarParalelobyCohorte'));
        $xajax->register(XAJAX_FUNCTION,array('MostrarbyVMaestria', $ControlParalelo,'MostrarParalelobyVMaestria'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTXByCohorte', $ControlParalelo,'ConsultaByTXByCohorte'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByIDByCohorte', $ControlParalelo,'ConsultaByIDByCohorte'));
        
        $xajax->processRequest();       
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript" src="src/modules/maestria/unidadasistencial/js/jsUnidadAsistencia.js?0"></script>
    </head>
        <body>
            
        <div class="FormBasic" style="width:580px">
            <div class="FormSectionMenu">              
            <?php   $ControlParalelo->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState("Inactive"); ';
                    echo '</script>';                    
            ?>    
            </div>  
            
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                    <?php  $ControlParalelo->CargaMantenimiento();  ?>
                    </form>
            </div> 
            
            <div class="FormSectionGrid">          
            <?php   $ControlParalelo->CargaSearchGrid();  ?>
            </div>  
            
        </div>
        </body>
    </html>
         