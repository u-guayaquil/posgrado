<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioParalelo.php");
        require_once("src/modules/maestria/paralelo/render/RenderParalelo.php");

        class ControlParalelo
        {       private  $ServicioParalelo;
                private  $RenderParalelo;

                function __construct()
                {       $this->ServicioParalelo = new ServicioParalelo();
                        $this->RenderParalelo = new RenderParalelo();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderParalelo->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderParalelo->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => 'NULL');
                        $Datos = $this->ServicioParalelo->BuscarParaleloByDescripcion($prepareDQL);
                        echo $this->RenderParalelo->CreaSearchGrid($Datos);
                }         
               
                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioParalelo->BuscarParaleloByID($id);
                        return $this->RenderParalelo->MuestraFormulario($ajaxRespon,$Datos);
                }
                
                function ConsultaByIDByCohorte($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('id' => ($JsDatos -> id), 'idCohorte' => $JsDatos->idcohorte);
                        $ArDatos = $this->ServicioParalelo->BuscarParaleloByIDByCohorte($prepareDQL);
                        return $this->RenderParalelo->MuestraFormulario($ajaxRespon,$ArDatos);
                }

                function ConsultaByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();              
                        $prepareDQL = array('texto' => strtoupper(trim($Texto)));
                        $ArDatos = $this->ServicioParalelo->BuscarParaleloByDescripcion($prepareDQL);
                        return $this->RenderParalelo->MuestraGrid($ajaxRespon,$ArDatos);
                }
                
                function ConsultaByTXByCohorte($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('texto' => strtoupper(trim($JsDatos -> FindTextBx)), 'id' => $JsDatos->idcohorte );
                        $ArDatos = $this->ServicioParalelo->BuscarParaleloByDescripcionByCohorte($prepareDQL);
                        return $this->RenderParalelo->MuestraGrid($ajaxRespon,$ArDatos);
                }

                function Guarda($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");
                        $id = $this->ServicioParalelo->GuardaDBParalelo($JsDatos);
                        $ArDatos = $this->ServicioParalelo->BuscarParaleloEstadoByID($id);
                        return $this->RenderParalelo->{$funcion}($ajaxRespon,$ArDatos);
                }

                function Elimina($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsData = json_decode($Form)->Forma;
                        $JsData->estado = $this->ServicioParalelo->DesactivaParalelo($JsData->id);
                        return $this->RenderParalelo->MuestraEliminado($ajaxRespon,$JsData);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1));
                        if ($Operacion==="btvmaestria")    
                        {                            
                            $ArDatos = $this->ServicioParalelo->BuscarMaestriaByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestrias";
                            $jsonModal['Carga'] = $this->RenderParalelo->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "750";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function CargaModalGridCohorte($Operacion,$Form)
                {       $ajaxResp = new xajaxResponse();  
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1), 'idvmaestria' => $JsDatos->idvmaestria);
                        if ($Operacion==="btcohorte")    
                        {   $ArDatos = $this->ServicioParalelo->BuscarCohorteByVmaestriaByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cohorte";
                            $jsonModal['Carga'] = $this->RenderParalelo->CreaModalGridCohorte($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "360";
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();
                        if ($Operacion==="btvmaestria")    
                        {   $texto = trim($Texto);
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                            $Datos = $this->ServicioParalelo->BuscarMaestriaByDescripcion($prepareDQL);
                            return $this->RenderParalelo->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                        }    
                        if ($Operacion==="btcohorte")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'texto'=>$Texto[0],'idvmaestria'=>$Texto[1], 'tipo' => array(0,1));
                            $Datos = $this->ServicioParalelo->BuscarCohorteByVmaestriaByDescripcion($prepareDQL);
                            return $this->RenderParalelo->MuestraModalGridCohorte($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
                
                function MostrarParalelobyCohorte($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->txcohorte) ? "Muestrabycohorte" : "MuestraEditadobycohorte");
                        $idCohorte = $JsDatos->idcohorte;
                        $ArDatos = $this->ServicioParalelo->BuscarParaleloEstadoByCohorte($idCohorte);
                        return $this->RenderParalelo->{$funcion}($ajaxRespon,$ArDatos);
                }
                
                function MostrarParalelobyVMaestria($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->txcohorte) ? "MuestrabyVmaestria" : "MuestraEditadobyVmaestria");
                        $ArDatos = $this->ServicioParalelo->BuscarParaleloEstadoByVMaestria();
                        return $this->RenderParalelo->{$funcion}($ajaxRespon,$ArDatos);
                }
        }       
?>