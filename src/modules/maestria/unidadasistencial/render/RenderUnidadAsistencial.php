<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderParalelo
        {   private $SearchGrid;
            private $Maxlen;    
            
            function __construct()
            {       $this->Maxlen['id'] = 4;        
                    $this->Maxlen['descripcion'] = 50;
                    $this->SearchGrid = new SearchGrid();
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']          = array('40px','center','');
                    $Columns['Paralelo']    = array('120px','left',''); 
                    $Columns['Capacidad']    = array('80px','center','');
                    $Columns['IdCohorte']   = array('0px','center','none');
                    $Columns['Cohorte']     = array('105px','left','');
                    $Columns['FeUsCrea']    = array('100px','left','');
                    $Columns['IdEstado']    = array('0px','center','none');
                    $Columns['Estado']      = array('80px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }
               
            function CreaMantenimiento()
            {      $Search = new SearchInput();
                    $HTML = '<table border=0 class="Form-Frame" cellpadding="0">';                 
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label">Maestria</td>';
                    $HTML.= '           <td class="Form-Label" colspan="3">';
                    $HTML.= $Search->TextSch("vmaestria","","")->Enabled(true)->Create("t13");
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label">Version</td>';
                    $HTML.= '           <td class="Form-Label" colspan="3">';
                    $HTML.= '                 <input type="text" class="txt-upper t06" id="version" name="version" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label">Cohorte</td>';
                    $HTML.= '           <td class="Form-Label" colspan="3">';
                    $HTML.= $Search->TextSch("cohorte","","")->Enabled(false)->Create("t05");
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">Id</td>';
                    $HTML.= '             <td class="Form-Label" style="width:80%">';
                    $HTML.= '                <input type="text" class="txt-input t01" id="id" name="id" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\'descripcion:capacidad\',\'NUM\'); " onBlur="return SearchByElement(this);" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';                   
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Paralelo</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                 <input type="text" class="txt-upper t05" id="descripcion" name="descripcion" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">Capacidad</td>';
                    $HTML.= '             <td class="Form-Label" style="width:80%">';
                    $HTML.= '                <input type="text" class="txt-input t01" id="capacidad" name="capacidad" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Estado</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= $this->CreaComboEstadoByArray(array(0,1));
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function CreaBarButton($Buttons)
            {       $BarButton = new BarButton();    
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGrid($ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    return $this->SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
            
            private function GridDataHTML($Grid,$ArDatos)
            {       if ($ArDatos[0])
                    {   foreach ($ArDatos[1] as $ArDato)
                        {       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);                       
                                $Grid->CreaSearchCellsDetalle($id,'',$id);                     
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['PARALELO']))); 
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CAPACIDAD']);
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDCOHORTE']);
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['COHORTE'])));    
                                $dateBD = $ArDato['FCREACION'];
                                $Grid->CreaSearchCellsDetalle($id,'',$dateBD->format('d/m/Y'));
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
                                $Grid->CreaSearchRowsDetalle ($id,($ArDato['IDESTADO']==0 ? "red" : ""));
                        }
                    }    
                    return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
            }
               
            function MuestraFormulario($Ajax,$Paralelo)
            {   if ($Paralelo)
                    {   if ($Paralelo->ID>=0)
                        {   $Ajax->Assign("id","value", str_pad($Paralelo->ID,$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion","value", utf8_encode(trim($Paralelo->PARALELO)));
                            $Ajax->Assign("capacidad","value", utf8_encode(trim($Paralelo->CAPACIDAD)));
                            $Ajax->Assign("estado","value", $Paralelo->IDESTADO);
                            $Ajax->call("BarButtonState","Active");
                            return $Ajax;
                        }    
                    }
                    return $this->Respuesta($Ajax,"CMD_WRM","No existe una Paralelo con el ID ingresado.");
            }
            
            function MuestraGuardado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);
            }
            
            function MuestraGrid($Ajax,$ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    $Ajax->Assign($this->SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }
            
            function MuestraEditado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraRowGrid($Ajax, $ArDatos[1][0]);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }    
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);            
            }
    
            function MuestraRowGrid($Ajax,$ArDato)
            {       $RowId = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$ArDato['IDESTADO']);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);      
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",utf8_encode(trim($ArDato['PARALELO'])));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",$ArDato['CAPACIDAD']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$ArDato['IDCOHORTE']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",utf8_encode(trim($ArDato['COHORTE'])));
                    $dateBD = $ArDato['FCREACION'];
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$dateBD->format('d/m/Y'));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$ArDato['IDESTADO']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO")); 
                     
                    
                    return $Ajax;
            }                                
            
            function MuestraEliminado($Ajax,$JsDatos)
            {       $RowId = str_pad($JsDatos->id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$JsDatos->estado);
                    
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);       
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$JsDatos->estado); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML","INACTIVO"); 
                    return $this->Respuesta($Ajax,"CMD_DEL",0);
            }

            private function Respuesta($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse";
                    $Ajax->call($jsFuncion,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox();
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Enabled(false)->Fields("id","descripcion")->Create("s05");
            }   
             
            
            function MuestraEditadobycohorte($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$id);
                    }
                else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_MAE_ERR",$ArDatos[1]);            
            }
            
            function Muestrabycohorte($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$id);
                    }
                    else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_MAE_ERR",$ArDatos[1]);            
            }
            
            function MuestraEditadobyVmaestria($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_VM",$id);
                    }
                else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_VM",$ArDatos[1]); 
                    }
                    return $this->Respuesta($Ajax,"CMD_VM_ERR",$ArDatos[1]);            
            }
            
            function MuestrabyVmaestria($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_VM",$id);
                    }
                    else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_VM",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_VM_ERR",$ArDatos[1]);            
            }
            
            /*** MODAL MAESTRIA***/
            
            private function SearchGridModalConfig()
                {       
                        $Columns['IdFacultad']   = array('0px','center','none');
                        $Columns['Facultad']   = array('275px','left','');
                        $Columns['Maestria']    = array('325px','left','');
                        $Columns['IdVersion']   = array('0px','center','none');
                        $Columns['Version']     = array('60px','center','');
                        $Columns['Cohorte']     = array('60px','center','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$ArDatos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcion($SearchGrid,$ArDatos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcion($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalOpcion($Grid,$ArDatos)
                {       if ($ArDatos[0])
                        {   foreach ($ArDatos[1] as $ArDato)
                            {       $id = str_pad($ArDato['IDFACULTAD'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                    $Grid->CreaSearchCellsDetalle($id,'',$id);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['FACULTAD']);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDVERSION']);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['VERSION'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['COHORTES'])));
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }
                
            /*** MODAL COHORTE ***/
            
            private function SearchGridModalConfigCohorte()
                {       
                        $Columns['Id']   = array('0px','center','none');
                        $Columns['Cohorte']     = array('120px','left','');
                        $Columns['Inicio']      = array('70px','left','');
                        $Columns['Fin']         = array('70px','left','');
                        $Columns['IdEstado']    = array('0px','left','none');
                        $Columns['Estado']      = array('70px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGridCohorte($Operacion,$ArDatos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfigCohorte());
                        $GrdDataHTML = $this->GridDataHTMLModalCohorte($SearchGrid,$ArDatos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGridCohorte($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfigCohorte());
                        $GrdDataHTML = $this->GridDataHTMLModalCohorte($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalCohorte($Grid,$ArDatos)
                {       if ($ArDatos[0])
                        {   foreach ($ArDatos[1] as $ArDato)
                            {       
                                    $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                    $Grid->CreaSearchCellsDetalle($id,'',$id);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['COHORTE'])));
                                    $InicioBD = $ArDato['FEINICIO'];
                                    $Grid->CreaSearchCellsDetalle($id,'',$InicioBD->format('d/m/Y'));
                                    $FinBD = $ArDato['FEFIN'];
                                    $Grid->CreaSearchCellsDetalle($id,'',$FinBD->format('d/m/Y'));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                    $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO")); 
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }
        }
?>

