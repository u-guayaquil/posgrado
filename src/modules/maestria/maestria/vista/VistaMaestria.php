<?php   $Opcion = 'Maestria';

        require_once('src/modules/maestria/maestria/controlador/ControlMaestria.php');
        $ControlMaestria = new ControlMaestria();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlMaestria,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlMaestria,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlMaestria,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlMaestria,'ConsultaByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlMaestria,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlMaestria,'ConsultaModalGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('MostrarbyFacultad', $ControlMaestria,'MostrarMaestriabyFacultad'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTXByFacultad', $ControlMaestria,'ConsultaByTXByFacultad'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByIDByFacultad', $ControlMaestria,'ConsultaByIDByFacultad'));
        
        $xajax->processRequest();       
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
            
                var objmaestria = "id:descripcion:estado:txfacultad:idfacultad:maestriadesc:valadmision";
                var valmaestria = "txfacultad:descripcion:maestriadesc:valadmision";
                
                function ButtonClick(Operacion)
                { 
                        var objmaestria = "id:descripcion:estado:txfacultad:idfacultad:maestriadesc:valadmision";
                        var valmaestria = "txfacultad:descripcion:maestriadesc:valadmision"; 
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementStatus("descripcion:maestriadesc:valadmision","id:txfacultad:btfacultad");
                            ElementClear("id:descripcion:maestriadesc:valadmision");
                            ElementSetValue("estado",1);
                            ElementSetValue("valadmision",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Operacion);
                                if (ElementGetValue("estado")==0)
                                {   
                                    BarButtonStateEnabled("btfacultad");
                                    valmaestria = valmaestria + ":estado";
                                }
                                ElementStatus(valmaestria,"id:txfacultad:btfacultad");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue("estado")==1)
                                {   /*if (confirm("Desea inactivar este registro?"))
                                    {   var Forma = PrepareElements("id:estado");
                                        xajax_Elimina(JSON.stringify({Forma}));
                                    } */   
                                    Swal.fire({
                                        title: 'Desactivar registro?',
                                        text: "Esta seguro que desea desactivar el registro?",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Si, estoy seguro!'
                                    }).then(function(result){
                                        if (result.value) {
                                            var Forma = PrepareElements("id:estado");
                                            xajax_Elimina(JSON.stringify({Forma}));
                                        }
                                    })
                                }
                                else{SAlert("warning","¡Cuidado!","¡El registro se encuentra inactivo!");}
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(valmaestria))
                                {   if (BarButtonState("Inactive"))
                                    {   var Forma = PrepareElements(objmaestria);
                                        xajax_Guarda(JSON.stringify({Forma}));
                                    }
                                }
                                else
                                {SAlert("warning","¡Cuidado!","¡Llene los campos por favor!");}
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Operacion);
                                ElementStatus("id:btfacultad:txfacultad","descripcion:estado:maestriadesc:valadmision");
                                ElementClear("id:descripcion:maestriadesc:valadmision");
                                ElementSetValue("estado",1);
                                ElementSetValue("valadmision","");
                        }
                        else 
                        {xajax_CargaModal(Operacion);}
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Default");
                            ElementSetValue("id", Datos);
                            ElementClear("id:descripcion:maestriadesc:valadmision");
                            ElementStatus("id:btfacultad:txfacultad","descripcion:maestriadesc:valadmision:estado");
                            ElementSetValue("estado",1);
                            ElementSetValue("valadmision","");
                            SAlert("success","¡Excelente!","¡Los datos se guardaron correctamente!");
                        }
                        else if (Mensaje==="CMD_FAC")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                                BarButtonState("Default");
                                ElementStatus("id:btfacultad:txfacultad","descripcion:maestriadesc:valadmision:estado");
                                ElementClear("id:descripcion:maestriadesc:valadmision");
                                ElementSetValue("estado",1);
                                ElementSetValue("valadmision","");
                            }
                            else if(ValidateStatusEnabled("addSave")){
                                BarButtonState("Active");
                                ElementStatus("descripcion:maestriadesc:valadmision","id:estado:btfacultad:txfacultad");
                            }
                            else {
                                BarButtonState("Default");
                                ElementStatus("id:btfacultad:txfacultad","descripcion:maestriadesc:valadmision:estado");
                                ElementClear("id:descripcion:maestriadesc:valadmision");
                                ElementSetValue("estado",1);
                                ElementSetValue("valadmision","");
                            }
                        }
                        else if (Mensaje==="CMD_FAC_ERR")
                        {   
                           if (ValidateStatusEnabled("addNew")){
                            }
                            else {
                                ElementStatus("descripcion:maestriadesc:valadmision","id:estado:btfacultad:txfacultad");
                            }
                        }
                        else if(Mensaje==='CMD_ERR')
                        {   BarButtonState("addNew");
                            SAlert("error","Error", Datos); 
                        }    
                        else if(Mensaje==='CMD_DEL')
                        {   ElementSetValue("estado",Datos);
                            SAlert("success","¡Excelente!","¡Los datos se guardaron correctamente!");
                        }
                        else if(Mensaje==="CMD_WRM")
                        {   BarButtonState("Default");
                            ElementClear("id");
                            ElementSetValue("valadmision","");
                            ElementSetValue("estado",1);
                            SAlert("warning","Cuidado", Datos); 
                        }
                }
                
                function SearchByElement(Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id")
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion");
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState("Inactive"))
                                    {
                                        var Forma = PrepareElements("id:idfacultad");
                                        xajax_BuscaByIDByFacultad(JSON.stringify({Forma}));
                                    }
                                }
                            }
                            else
                            BarButtonState("Default"); 
                        }
                        else if (Elemento.id === "FindFacultadTextBx")
                        {    xajax_BuscaModalByTX("btfacultad",Elemento.value);
                        } 
                        else if(Elemento.id === "FindTextBx")
                        {
                            var Forma = PrepareElements("FindTextBx:idfacultad");
                            xajax_BuscaByTXByFacultad(JSON.stringify({Forma}));
                        }
                }
                
                function SearchFacultadGetData(DatosGrid)
                {       document.getElementById("idfacultad").value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txfacultad").value = DatosGrid.cells[1].childNodes[0].nodeValue.toUpperCase();
                        cerrar();
                        var Forma = PrepareElements(objmaestria);
                        xajax_MostrarbyFacultad(JSON.stringify({Forma}));
                        return false;
                }
                 
                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("descripcion").value = Grilla.cells[1].childNodes[0].nodeValue.toUpperCase();
                            document.getElementById("estado").value = Grilla.cells[3].childNodes[0].nodeValue;
                            document.getElementById("txfacultad").value = Grilla.cells[6].childNodes[0].nodeValue.toUpperCase(); 
                            document.getElementById("idfacultad").value = Grilla.cells[5].childNodes[0].nodeValue; 
                            document.getElementById("maestriadesc").value = Grilla.cells[7].childNodes[0].nodeValue.toUpperCase(); 
                            document.getElementById("valadmision").value = Grilla.cells[8].childNodes[0].nodeValue; 
                        }
                        return false;
                }
                 
        </script>
    </head>
        <body>
            
        <div class="FormBasic" style="width:480px">
            <div class="FormSectionMenu">              
            <?php   $ControlMaestria->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState("Inactive"); ';
                    echo '</script>';                    
            ?>    
            </div>  
            
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                    <?php  $ControlMaestria->CargaMantenimiento();  ?>
                    </form>
            </div> 
            
            <div class="FormSectionGrid">          
            <?php   $ControlMaestria->CargaSearchGrid();  ?>
            </div>  
            
        </div>
        </body>
    </html>
         