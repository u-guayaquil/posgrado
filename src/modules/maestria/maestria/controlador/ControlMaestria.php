<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioMaestria.php");
        require_once("src/modules/maestria/maestria/render/RenderMaestria.php");

        class ControlMaestria
        {       private  $ServicioMaestria;
                private  $RenderMaestria;

                function __construct()
                {       $this->ServicioMaestria = new ServicioMaestria();
                        $this->RenderMaestria = new RenderMaestria();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderMaestria->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderMaestria->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => 'NULL');
                        $Datos = $this->ServicioMaestria->BuscarMaestriaByDescripcion($prepareDQL);
                        echo $this->RenderMaestria->CreaSearchGrid($Datos);
                }         
               
                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioMaestria->BuscarMaestriaByID($id);
                        return $this->RenderMaestria->MuestraFormulario($ajaxRespon,$Datos);
                }
                
                function ConsultaByIDByFacultad($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('id' => ($JsDatos -> id), 'id_facultad' => $JsDatos->idfacultad);
                        $ArDatos = $this->ServicioMaestria->BuscarMaestriaByIDByFacultad($prepareDQL);
                        return $this->RenderMaestria->MuestraFormulario($ajaxRespon,$ArDatos);
                }

                function ConsultaByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();              
                        $prepareDQL = array('texto' => strtoupper(trim($Texto)));
                        
                        $ArDatos = $this->ServicioMaestria->BuscarMaestriaByDescripcion($prepareDQL);
                        return $this->RenderMaestria->MuestraGrid($ajaxRespon,$ArDatos);
                }
                
                function ConsultaByTXByFacultad($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('texto' => strtoupper(trim($JsDatos -> FindTextBx)), 'id' => $JsDatos->idfacultad );
                        $ArDatos = $this->ServicioMaestria->BuscarMaestriaByDescripcionByFacultad($prepareDQL);
                        return $this->RenderMaestria->MuestraGrid($ajaxRespon,$ArDatos);
                }

                function Guarda($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");
                        $id = $this->ServicioMaestria->GuardaDBMaestria($JsDatos);
                        $ArDatos = $this->ServicioMaestria->BuscarMaestriaEstadoByID($id);
                        return $this->RenderMaestria->{$funcion}($ajaxRespon,$ArDatos);
                }

                function Elimina($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsData = json_decode($Form)->Forma;
                        $JsData->estado = $this->ServicioMaestria->DesactivaMaestria($JsData->id);
                        return $this->RenderMaestria->MuestraEliminado($ajaxRespon,$JsData);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1));
                        if ($Operacion==="btfacultad")    
                        {                            
                            $ArDatos = $this->ServicioMaestria->BuscarFacultadByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Facultad";
                            $jsonModal['Carga'] = $this->RenderMaestria->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "517";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        if ($Operacion==="btfacultad")    
                        {   $Datos = $this->ServicioMaestria->BuscarFacultadByDescripcion($prepareDQL);
                            return $this->RenderMaestria->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
                
                function MostrarMaestriabyFacultad($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->txfacultad) ? "MuestrabyFacultad" : "MuestraEditadobyFacultad");
                        $id = $JsDatos->idfacultad;
                        $ArDatos = $this->ServicioMaestria->BuscarMaestriaEstadoByIdFacultad($id);
                        return $this->RenderMaestria->{$funcion}($ajaxRespon,$ArDatos);
                }
        }       
?>