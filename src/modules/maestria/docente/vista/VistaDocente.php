<?php   $Opcion = 'Docente';
        require_once('src/modules/maestria/docente/controlador/ControlDocente.php');
        $ControlDocente = new ControlDocente();
            
        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlDocente,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaFacultad', $ControlDocente,'GuardaFacultad'));
        $xajax->register(XAJAX_FUNCTION,array('BuscarByID', $ControlDocente,'BuscarByCedula')); 
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlDocente,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlDocente,'CargaModal'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlDocente,'ConsultaGridByTX'));
        
        $xajax->processRequest();
        
        $Modulo = json_decode(Session::getValue('Sesion'));
        $Perfil = $ControlDocente->ConsultaPerfilByRol($Modulo->Idusrol);
        if($Perfil)
        {   $PerfilUs = $Perfil->IDPERFIL;
        }
        else{   $PerfilUs = 1;  }
        
?>

         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php $xajax->printJavascript(); 
                require_once('src/utils/links.php'); ?>
            
            <script type="text/javascript" src="src/modules/maestria/docente/js/jsDocente.js"></script>
         </head>
         <body>      
             <div class="FormBasic" style="width:550px">
                <div class="FormSectionMenu">              
                <?php  $ControlDocente->CargaBarButton($_GET['opcion']);
                       echo '<script type="text/javascript">';
                       echo '        BarButtonState("Inactive"); ';
                       echo '</script>';
                ?>
                </div>    
                <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                          <?php  $ControlDocente->CargaMantenimiento();  ?>
                     </form>
                </div>
                
             </div>

             <script type="text/javascript">
                var perfil= <?php print_r($PerfilUs);?>;
            </script> 
        </body>
        </html>


