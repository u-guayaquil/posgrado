var accion = 0;
var select = 0;
var activo = 1;

function AgregarRegistro(Opcion)
{       accion = 0;
        Swal.fire({
            title: '¡El docente actual no está activo para posgrado!',
            text: "¿Desea activarlo?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, estoy seguro!'
        }).then(function(result){
            if (result.value) {
                if (ElementValidateBeforeSave("email:telefono:direccion"))
                    {   BarButtonState("Inactive");
                        ElementSetValue("idgestion","0");
                        var Forma = PrepareElements("idDocente:nombre:apellido:email:telefono:direccion:estado:idgestion:idroluser");
                        xajax_Guarda(accion,JSON.stringify({Forma})); 
                        return false;
                    }
                    else
                    {   SAlert("warning", "¡Campos vacios!", "Falta ingresar informacion.");   }
            }
        });      
    /*if (confirm("El docente actual no está activo para posgrado. ¿Desea activarlo?"))
        {   if (ElementValidateBeforeSave("email:telefono:direccion"))
            {   BarButtonState("Inactive");
                ElementSetValue("idgestion","0");
                var Forma = PrepareElements("idDocente:nombre:apellido:email:telefono:direccion:estado:idgestion:idroluser");
                xajax_Guarda(accion,JSON.stringify({Forma})); 
                return false;
            }
            else
            {   alert("Falta ingresar informacion.");}
        }   */
        BarButtonState(Opcion);
        ElementStatus("email:direccion:telefono","idDocente");
        if(perfil == 0)
        {   ElementStatus("idgestion", ""); }
        else{   ElementStatus("", "idgestion"); }
        BarButtonStateDisabled("btDocente");
        return false;
}

function ButtonClick( Operacion)
{       if (Operacion=="addMod")
        {   accion = 1;
            BarButtonState(Operacion);
            ElementStatus("email:direccion:telefono","idDocente");
            BarButtonStateDisabled("btDocente");
            if(perfil == 0)
            {   ElementStatus("idgestion", ""); }
            else{   ElementStatus("", "idgestion"); }
            select = ElementGetValue("idgestion");
            activo = ElementGetValue("estado");
            if(ElementGetValue("estado")==0) ElementStatus( "estado", "");
        }
        else if (Operacion=="addSav")
        {    if (ElementValidateBeforeSave("email:direccion:telefono"))
             {   BarButtonState("Inactive");
                 var Forma = PrepareElements("idDocente:nombre:apellido:email:telefono:direccion:estado:idgestion:idroluser");
                 xajax_Guarda(accion,JSON.stringify({Forma}));
             }
             else
             SAlert("warning", "¡Campos vacios!", "Falta ingresar datos como email, telefono o dirección.");
        }
        else if (Operacion=="addCan")
        {    if (accion == 1)
             {   BarButtonState("Active");
                 ElementSetValue("idgestion",select);    
                 ElementSetValue("estado",activo);
             }
             else
             {   BarButtonState("Inactive");
                 ElementSetValue("estado",1);
                 ElementSetValue("idgestion",0);
                 ElementClear("id:idDocente:nombre:apellido:email:telefono:direccion:idroluser");
             }   
             ElementStatus("idDocente","nombre:apellido:email:telefono:direccion:idgestion:estado");
             BarButtonStateEnabled( "btDocente");
             return false;
        } 
        else if (Operacion=="addDel")
        {   if (ElementGetValue("estado")==1)
            {   /*if (confirm("¿Desea inactivar este registro?"))
                xajax_Elimina(ElementGetValue("idDocente"));*/
                Swal.fire({
                    title: '¡Desactivar registro!',
                    text: "¿Esta seguro que desea desactivar el registro?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro!'
                }).then(function(result){
                    if (result.value) {
                        xajax_Elimina(ElementGetValue("idDocente"));
                    }
                });  
            } 
            else if (ElementGetValue("estado")==2)
                SAlert("error", "¡Error!", "Usted no puede inactivar un registro que esta por ser confirmado.");
            else
            SAlert("error", "¡Registro inactivo!", "El registro se encuentra inactivo");
        }
        else if (Operacion=="btDocente")
        {    xajax_CargaModal(Operacion,"");   
        }
        else if (Operacion=="btAplica")
        {   IDDOC = ElementGetValue("id");
            if (IDDOC!="" && IDDOC!=0) 
                xajax_CargaModal(Operacion,IDDOC);
            else
                SAlert("error", "¡Docente inactivo!", "Debe seleccionar un docente activo para posgrado.");
        }
}        
        
function SearchByElement(Elemento)
{       Elemento.value = TrimElement(Elemento.value);
        if (Elemento.id=="idDocente")
        {   if (Elemento.value.length != 0)
            {   ContentFlag = document.getElementById("nombre");
                if (ContentFlag.value.length==0)
                {   if (BarButtonState("Inactive"))
                    xajax_BuscarByID(Elemento.value);
                }
            }
            else
            BarButtonState("Inactive");
        }
        else if (Elemento.id=="FindDocenteTextBx")
        {   xajax_BuscaModalByTX("btDocente",Elemento.value);
        }
        return false;
}        

function SearchDocenteGetData(DatosGrid)
{       ElementSetValue("idDocente",DatosGrid.cells[0].innerHTML);
        ElementSetValue("apellido",DatosGrid.cells[2].innerHTML);
        ElementSetValue("nombre",DatosGrid.cells[3].innerHTML);
        ElementSetValue("direccion",DatosGrid.cells[4].innerHTML);
        ElementSetValue("email",DatosGrid.cells[5].innerHTML);
        ElementSetValue("telefono",DatosGrid.cells[6].innerHTML);
        ElementSetValue("estado",DatosGrid.cells[7].innerHTML);
        ElementSetValue("id",DatosGrid.cells[8].innerHTML);
        ElementSetValue("idgestion",DatosGrid.cells[9].innerHTML);
        ElementSetValue("idroluser",DatosGrid.cells[10].innerHTML);
        
        if (DatosGrid.cells[1].innerHTML=="S")
        {   accion = 1;
            BarButtonState("Active");
        }    
        else
        AgregarRegistro("addMod");
        cerrar();
}
 
function SearchAplicaGetData(DatosGrid)
{       var IDFAC=DatosGrid.cells[0].innerHTML;
        var IDDOC=DatosGrid.cells[1].innerHTML; 
        var MARKA=DatosGrid.cells[3].innerHTML;
        xajax_GuardaFacultad(IDDOC,IDFAC,MARKA);
        return false;
}

function CrearDocentePorConfirmar(Estado)
{       if (Estado)
        {   /*if (confirm("El docente no tiene un contrato en Talento Humano. ¿Desea crearlo?"))
            {   accion = 0;
                BarButtonState("addMod");
                ElementStatus("nombre:apellido:email:direccion:telefono","idDocente");
                if(perfil == 0)
                {   ElementStatus("idgestion", ""); }
                else{   ElementStatus("", "idgestion"); }
                BarButtonStateDisabled("btDocente");
                ElementSetValue("estado",2);
                ElementSetValue("idgestion",0);
            }*/
            Swal.fire({
                title: '¡Docente sin contraro!',
                text: "El docente no tiene un contrato en Talento Humano. ¿Desea crearlo?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, deseo crearlo!'
            }).then(function(result){
                if (result.value) {
                    accion = 0;
                    BarButtonState("addMod");
                    ElementStatus("nombre:apellido:email:direccion:telefono","idDocente");
                    if(perfil == 0)
                    {   ElementStatus("idgestion", ""); }
                    else{   ElementStatus("", "idgestion"); }
                    BarButtonStateDisabled("btDocente");
                    ElementSetValue("estado",2);
                    ElementSetValue("idgestion",0);
                }
            });
        }
        else
        SAlert("error","¡Cédula incorrecta!","Ingrese una cedula correcta.");    
        return false;
}

function XAJAXResponse(Mensaje,Datos)
{   if (Mensaje==="CMD_SAV")
    {   SAlert("success", "¡Excelente!", Datos);
    }
    else if(Mensaje==='CMD_DEL')
    {   SAlert("warning", "¡Cuidado!", Datos);    
    }
    else if(Mensaje==='CMD_WRM')
    {   SAlert("warning", "¡Cuidado!", Datos);
    }
    else if(Mensaje==='CMD_ERR')
    {   SAlert("error", "¡Error!", Datos);
    }
}