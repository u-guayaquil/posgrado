<?php
        require_once("src/rules/maestria/servicio/ServicioDocente.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/modules/maestria/docente/render/RenderDocente.php");
        require_once("src/rules/general/servicio/ServicioFacultad.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");
         
        class ControlDocente
        {       private $ServicioDocente;
                private $RenderDocente;
                private $ServicioFacultad;
                private $ServicioUsuarioFiltro;

                function __construct()
                {       $this->ServicioDocente = new ServicioDocente();
                        $this->RenderDocente = new RenderDocente();
                        $this->ServicioFacultad = new ServicioFacultad();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro(); 
                }

               function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderDocente->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {        echo $this->RenderDocente->CreaMantenimiento();
                }

                function BuscarByCedula($Cedula)
                {       $xAjax = new xajaxResponse();
                        $Datos= $this->ServicioDocente->BuscarByCedula($Cedula);
                        if ($Datos[0])
                            $xAjax = $this->RenderDocente->MuestraDatos($xAjax,$Datos[1]); 
                        else
                        {   $Utils = new Utils();
                            $Rspt = $Utils->validaIndentificacion(2,$Cedula);
                            $xAjax->Call("CrearDocentePorConfirmar",$Rspt[0]);
                        }
                        return $xAjax;
                }
               
                function Guarda($Op,$Form)
                {       $xAjax = new xajaxResponse();
                        $Docente  = json_decode($Form)->Forma;
                        $sEvento = $this->ServicioDocente->GuardaDB($Op,$Docente);
                        if ($sEvento[0])
                        {   if ($Op==0) $xAjax->Assign("id","value",$sEvento[1]);
                            $xAjax->Call("BarButtonState","Active");   
                            $xAjax->Call("ElementStatus","idDocente","nombre:apellido:email:direccion:telefono:estado:idgestion");
                            $xAjax->Call("BarButtonStateEnabled","btDocente");
                            $xAjax->Call("XAJAXResponse","CMD_SAV","Los datos se guardaron correctamente. ".$sEvento[2]);   
                            //$xAjax->alert("Los datos se guardaron correctamente. ".$sEvento[2]);
                        }
                        return $xAjax;
                }
                
                function GuardaFacultad($DocID,$FacID,$Mark)
                {       $xAjax = new xajaxResponse();
                        $Resp = $this->ServicioDocente->GuardaFacultad($DocID,$FacID);
                        if (!$Resp[0])
                            $xAjax->Call("XAJAXResponse", "CMD_WRM", $Resp[1]);  
                            //$xAjax->alert($Resp[1]);
                        else
                        $xAjax->Assign("TD_".$FacID."_003","innerHTML",($Mark=="✔" ? "":"✔"));
                        return $xAjax;
                }
               
                function Elimina($Cedula)
                {       $xAjax = new xajaxResponse();
                        $Datos = $this->ServicioDocente->Desactiva($Cedula);
                        if ($Datos[0])
                        {   $xAjax->assign("estado", "value", $Datos[1]);
                            $xAjax->assign("idgestion", "value", 0);
                        }
                        else
                            $xAjax->Call("XAJAXResponse", "CMD_ERR", $Datos[1]);
                            //$xAjax->alert($Datos[1]); 
                        return $xAjax; 
                }

                private function FilterOfAccess()
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        if ($Modulo->Idrol==3)
                            return $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                        else
                            return array('00'); //No permiso a Estudiante y Docente
                }                    
                
                function CargaModal($Operacion,$IdRef)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion=="btDocente")
                        {   $prepareDQL = array('texto' =>'');
                            $ArDatos = $this->ServicioDocente->BuscarDocenteByTX($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Docente";
                            $jsonModal['Carga'] = $this->RenderDocente->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderDocente->GetGridTableWidth();
                        }
                        elseif ($Operacion=="btAplica")
                        {   $prepareDQL = array('docente' =>$IdRef,'facultad'=>$this->FilterOfAccess());
                            $ArDatos = $this->ServicioFacultad->BuscarFacultadByDocente($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Seleccione la Facultad";
                            $jsonModal['Carga'] = $this->RenderDocente->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderDocente->GetGridTableWidth();
                        }
                        $xAjax->call("CreaModal", json_encode($jsonModal));
                        return $xAjax; 
                }
                
                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btDocente")    
                        {   $prepareDQL = array('texto'=>strtoupper(trim($Texto)));
                            $ArDatos = $this->ServicioDocente->BuscarDocenteByTX($prepareDQL);
                            return $this->RenderDocente->MuestraModalGrid($xAjax,$Operacion,$ArDatos);
                        }                
                }
                
                function ConsultaPerfilByRol($Idusrol)
                {   $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                    return $ServicioPerfilOpcion->ConsultarPerfilByRol($Idusrol);
                }
         }

?>

