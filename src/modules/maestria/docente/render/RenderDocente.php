<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");    
        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderDocente
        {       private $SearchGrid;
                private $Width;
              
                function __construct()
                {       $this->SearchGrid = new SearchGrid();
                }
              
                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();    
                        return $BarButton->CreaBarButton($Buttons);
                }
              
                function CreaMantenimiento()
                {        $Search = new SearchInput();                      
                         $Col1="30%"; $Col2="37%"; $Col3="12%"; $Col4="21%";
                         $Modulo = json_decode(Session::getValue('Sesion')); 
    
                         $HTML = '<table class="Form-Frame" cellpadding="0" border="0">';
                         $HTML.= '       <tr height="30">';
                         $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">C.I</td>'; 
                         $HTML.= '           <td class="Form-Label" colspan="3">';
                         $HTML.= '               <input type="hidden" id="id" name="id" value="" />';
                         $HTML.=                 $Search->TextSch("Docente","","")->Enabled(true)->MaxLength(10)->Keys("nombre:apellido:direccion:email:telefono:idgestion","NUM")->ReadOnly(false)->Create("t04");
                         $HTML.= '           </td>';
                         $HTML.= '       </tr>';
                         $HTML.= '       <tr height="30">';
                         $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Nombres</td>';
                         $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                         $HTML.= '               <input type="text" class="txt-upper t09" id="nombre" name="nombre" maxlength="30" value="" disabled/>';
                         $HTML.= '           </td>';
                         $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Estado</td>';
                         $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                         $HTML.=                 $this->CreaComboEstadoByArray(array(0,1,2));
                         $HTML.= '           </td>';
                         $HTML.= '       </tr>';
                         $HTML.= '         <tr height="30">';
                         $HTML.= '             <td class="Form-Label" style="width:'.$Col1.'">Apellidos</td>';
                         $HTML.= '             <td class="Form-Label" style="width:'.$Col2.'">';
                         $HTML.= '               <input type="text" class="txt-upper t09" id="apellido" name="apellido" maxlength="30" value="" disabled/>';
                         $HTML.= '             </td>';
                         $HTML.= '             <td class="Form-Label" style="width:'.$Col3.'">Tel&eacute;fono</td>';
                         $HTML.= '             <td class="Form-Label" style="width:'.$Col4.'">';
                         $HTML.= '                 <input type="text" class="txt-upper t04" id="telefono" name="telefono" maxlength="10" value="" onkeydown=" return soloNumeros(event); " disabled/>';
                         $HTML.= '             </td>';
                         $HTML.= '         </tr>';
                         $HTML.= '         <tr height="30">';
                         $HTML.= '             <td class="Form-Label" style="width:'.$Col1.'">Direcci&oacute;n</td>';
                         $HTML.= '             <td class="Form-Label" colspan="3">';
                         $HTML.= '                 <input type="text" class="txt-upper t16" id="direccion" name="direccion" maxlength="100" value="" disabled/>';
                         $HTML.= '             </td>';
                         $HTML.= '         </tr>';
                         $HTML.= '         <tr height="30" >';
                         $HTML.= '             <td class="Form-Label" style="width:'.$Col1.'">E-mail</td>';
                         $HTML.= '             <td class="Form-Label" colspan="3">';
                         $HTML.= '                 <input type="text" class="txt-upper t09" id="email" name="email" value="" maxlength="60" disabled/>';
                         $HTML.= '             </td>';
                         $HTML.= '         </tr>';
                         $HTML.= '         <tr height="30" >';
                         $HTML.= '             <td class="Form-Label" style="width:'.$Col1.'">Gestión</td>';
                         $HTML.= '             <td class="Form-Label" style="width:'.$Col2.'">';
                         $HTML.= '               <input type="hidden" id="idroluser" name="idroluser" value="" />'; 
                         $HTML.= '                 <select class="sel-input s09" id="idgestion" name="idgestion" disabled>';
                         $HTML.= '                         <option value="0">SELECCIONE...</option>';
                         $HTML.= '                         <option value="3">COORDINACION ADMINISTRATIVA</option>';
                         $HTML.= '                 </select>';    
                         $HTML.= '             </td>';
                         $HTML.= '             <td class="Form-Label-Center" colspan="2">';
                         $HTML.= '                 <input type="button" id="btaplica" class="p-txt-label" style="height: 25px;text-decoration: none;font-weight: 600;font-size: 12px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin-top:0px;" value="Asignar Facultad" onclick=" return ButtonClick(\'btAplica\'); ">';
                         $HTML.= '             </td>';
                         $HTML.= '         </tr>';
                         $HTML.= '</table>';
                         return $HTML;
                }
              
                function MuestraDatos($xAjax,$Datos)
                {        foreach ($Datos as $Dato)
                         {      $xAjax->Assign("id","value",$Dato['IDD']);
                                $xAjax->Assign("nombre","value",trim(utf8_encode($Dato['NMB'])));
                                $xAjax->Assign("apellido","value",trim(utf8_encode($Dato['APLL'])));
                                $xAjax->Assign("direccion","value",trim(utf8_encode($Dato['DIR']))); 
                                $xAjax->Assign("email","value",trim($Dato['EML']));
                                $xAjax->Assign("telefono","value",$Dato['FONO']);
                                $xAjax->Assign("estado","value",$Dato['ESTADO']);
                                $xAjax->Assign("idroluser","value",$Dato['ROLUSER']);
                                $xAjax->Assign("idgestion","value",$Dato['GESTION']);
                                if ($Dato["IDD"]==0)
                                    $xAjax->Call("AgregarRegistro","addMod");
                                else    
                                    $xAjax->Call("BarButtonState","Active");
                         }
                         return $xAjax;
                }
              
                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btDocente")
                        {   $Columns['Cedula'] = array('70px','center','');
                            $Columns['sino'] = array('0px','left','none');
                            $Columns['Apellido'] = array('150px','left','');
                            $Columns['Nombre'] = array('150px','left','');
                            $Columns['Dir'] = array('0px','left','none');
                            $Columns['email'] = array('0px','left','none');
                            $Columns['fono'] = array('0px','left','none');
                            $Columns['esta'] = array('0px','left','none');
                            $Columns['idd'] = array('0px','left','none');
                            $Columns['gestion'] = array('0px','left','none');
                            $Columns['roluser'] = array('0px','left','none');
                        }
                        else
                        {   $Columns['id'] = array('40px','center','');
                            $Columns['dc'] = array('40px','center','none');
                            $Columns['Facultad'] = array('250px','left','');
                            $Columns['✔'] = array('40px','center','');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px','FindTxt'=>false);
                        }
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Operacion=="btDocente")    
                        {   if ($Contenido[0])
                            {   foreach ($Contenido[1] as $ArDato)
                                {   $id = $ArDato['CEDULA'];
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CEDULA']);
                                    $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDD']==0 ? 'N':'S'));
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['APLL'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['NMB'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['DIR'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['EML'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['FONO'])); 
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ESTADO']);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDD']);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['GESTION']);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ROLUSER']);
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                                return $Grid->CreaSearchTableDetalle();
                            }
                            return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                        }
                        else
                        {   if ($Contenido[0])
                            {   $count = 1;
                                foreach ($Contenido[1] as $ArDato)
                                {   $id = $ArDato['ID']; 
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ID']);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['DC']);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['DESCRIPCION'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',($ArDato['AP']!=0 ? "✔":""));
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                                    $count++;
                                }        
                                return $Grid->CreaSearchTableDetalle();
                            }
                            return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                        }
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
                private function CreaComboEstadoByArray($IdArray)
                {       $Select = new ComboBox();
                        $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $Select->Combo("estado",$Datos)->Fields("id","descripcion")->Selected(1)->Create("s04");
                }        

        }
?>

