<?php   $Opcion = 'VMaestria';

        require_once('src/modules/maestria/vmaestria/controlador/ControlVMaestria.php');
        $ControlVMaestria = new ControlVMaestria();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlVMaestria,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlVMaestria,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlVMaestria,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlVMaestria,'ConsultaByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlVMaestria,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlVMaestria,'ConsultaModalGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('MostrarbyMaestria', $ControlVMaestria,'MostrarVMaestriabyMaestria'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTXByMaestria', $ControlVMaestria,'ConsultaByTXByMaestria'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByIDByMaestria', $ControlVMaestria,'ConsultaByIDByMaestria'));
        
        $xajax->processRequest();       
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
            
            var objvmaestria = "id:descripcion:estado:txmaestria:idmaestria:cohortes:materias";
            var valvmaestria = "txmaestria:cohortes:materias";
                
                function ButtonClick(Operacion)
                { 
                        var objvmaestria = "id:descripcion:estado:txmaestria:idmaestria:cohortes:materias";
                        var valvmaestria = "txmaestria:cohortes:materias";
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementStatus("cohortes:materias","id:descripcion:txmaestria:btmaestria");
                            ElementClear("id:descripcion:cohortes:materias");
                            ElementSetValue("estado",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Operacion);
                                if (ElementGetValue("estado")==0)
                                {   
                                    BarButtonStateEnabled("btmaestria");
                                    //valvmaestria = valvmaestria + ":estado";
                                }
                                ElementStatus(valvmaestria,"id:txmaestria:btmaestria");      
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue("estado")==1)
                                {   /*if (confirm("Desea inactivar este registro?"))
                                    {   var Forma = PrepareElements("id:estado");
                                        xajax_Elimina(JSON.stringify({Forma}));
                                    }    */
                                    Swal.fire({
                                        title: 'Desactivar registro?',
                                        text: "Esta seguro que desea desactivar el registro?",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Si, estoy seguro!'
                                    }).then(function(result){
                                        if (result.value) {
                                            var Forma = PrepareElements("id:estado");
                                            xajax_Elimina(JSON.stringify({Forma}));
                                        }
                                    });  
                                }
                                else{SAlert("warning", "¡Registro inactivo!", "El registro se encuentra inactivo");}
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(valvmaestria))
                                {   if (BarButtonState("Inactive"))
                                    {   var Forma = PrepareElements(objvmaestria);
                                        xajax_Guarda(JSON.stringify({Forma}));
                                    }
                                }
                                else
                                {SAlert("warning", "¡Cuidado!", "Llene los campos por favor");}
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Operacion);
                                ElementStatus("id:txmaestria:btmaestria","descripcion:estado:cohortes:materias");
                                ElementClear("id:descripcion:cohortes:materias");
                                ElementSetValue("estado",1);
                        }
                        else 
                        {xajax_CargaModal(Operacion);}
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Default");
                            ElementSetValue("id",Datos);
                            ElementClear("id:descripcion:cohortes:materias");
                            ElementStatus("id:txmaestria:btmaestria","descripcion:estado:cohortes:materias");
                            ElementSetValue("estado",1);
                            SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
                        }
                        else if (Mensaje==="CMD_MAE")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                                BarButtonState("Default");
                                ElementStatus("id:txmaestria:btmaestria","descripcion:estado:cohortes:materias");
                                ElementClear("id:descripcion:cohortes:materias");
                                ElementSetValue("estado",1);
                            }
                            else if(ValidateStatusEnabled("addSave")){
                                BarButtonState("Active");
                                ElementStatus("descripcion:cohortes:materias","id:estado:txmaestria:btmaestria");
                            }
                            else {
                                BarButtonState("Default");
                                ElementStatus("id:txmaestria:btmaestria","descripcion:estado:cohortes:materias");
                                ElementClear("id:descripcion:cohortes:materias");
                                ElementSetValue("estado",1);
                            }
                        }
                        else if (Mensaje==="CMD_MAE_ERR")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                            }
                            else {
                                ElementStatus("descripcion:cohortes:materias","id:estado:txmaestria:btmaestria");
                            }
                        }
                        else if(Mensaje==='CMD_ERR')
                        {   BarButtonState("addNew");
                            SAlert("error", "¡Error!", Datos); 
                        }    
                        else if(Mensaje==='CMD_DEL')
                        {    ElementSetValue("estado",Datos);
                            SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
                        }
                        else if(Mensaje==="CMD_WRM")
                        {   BarButtonState("Default");
                            ElementStatus("id:txmaestria:btmaestria","descripcion:estado:cohortes:materias");
                            ElementClear("id:descripcion:cohortes:materias");
                            ElementSetValue("estado",1);
                            SAlert("warning", "¡Cuidado!", Datos); 
                        }
                        else if(Mensaje==="CMD_WRM_COH")
                        {   BarButtonState("addMod");
                            ElementStatus("descripcion:estado:cohortes:materias","id:txmaestria:btmaestria");
                            SAlert("warning", "¡Cuidado!", Datos); 
                        }
                }
                
                function SearchByElement(Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id")
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion");
                                ContentFlag = document.getElementById("cohortes");
                                ContentFlag = document.getElementById("materias");
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState("Inactive"))
                                    {
                                        var Forma = PrepareElements("id:idmaestria");
                                        xajax_BuscaByIDByMaestria(JSON.stringify({Forma}));
                                    }
                                }
                            }
                            else
                            BarButtonState("Default"); 
                        }
                        else if (Elemento.id === "FindMaestriaTextBx")
                        {   
                            xajax_BuscaModalByTX("btmaestria",Elemento.value);
                        } 
                        else if(Elemento.id === "FindTextBx")
                        {
                            var Forma = PrepareElements("FindTextBx:idmaestria");
                            xajax_BuscaByTXByMaestria(JSON.stringify({Forma}));
                        }
                }
                
                function SearchMaestriaGetData(DatosGrid)
                {
                        document.getElementById("idmaestria").value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        document.getElementById("txmaestria").value = DatosGrid.cells[3].childNodes[0].nodeValue.toUpperCase();
                        cerrar();
                        BarButtonState("Default");
                        ElementStatus("id:txmaestria:btmaestria","descripcion:estado:cohortes:materias");
                        ElementClear("id:descripcion:cohortes:materias");
                        ElementSetValue("estado",1);
                        var Forma = PrepareElements(objvmaestria);
                        xajax_MostrarbyMaestria(JSON.stringify({Forma}));
                        return false;
                }
                 
                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("descripcion").value = Grilla.cells[1].childNodes[0].nodeValue.toUpperCase();
                            document.getElementById("cohortes").value = Grilla.cells[2].childNodes[0].nodeValue; 
                            document.getElementById("materias").value = Grilla.cells[3].childNodes[0].nodeValue;
                            document.getElementById("estado").value = Grilla.cells[4].childNodes[0].nodeValue; 
                            document.getElementById("txmaestria").value = Grilla.cells[6].childNodes[0].nodeValue.toUpperCase();; 
                            document.getElementById("idmaestria").value = Grilla.cells[7].childNodes[0].nodeValue; 
                            
                        }
                        return false;
                }
                 
        </script>
    </head>
        <body>
            
        <div class="FormBasic" style="width:480px">
            <div class="FormSectionMenu">              
            <?php   $ControlVMaestria->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState("Inactive"); ';
                    echo '</script>';                    
            ?>    
            </div>  
            
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                    <?php  $ControlVMaestria->CargaMantenimiento();  ?>
                    </form>
            </div> 
            
            <div class="FormSectionGrid">          
            <?php   $ControlVMaestria->CargaSearchGrid();  ?>
            </div>  
            
        </div>
        </body>
    </html>
         