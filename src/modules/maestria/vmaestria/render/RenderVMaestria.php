<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderVMaestria
        {   private $SearchGrid;
            private $Maxlen;    
            
            function __construct()
            {       $this->Maxlen['id'] = 4;    
                    $this->Maxlen['cohorte'] = 1; 
                    $this->Maxlen['materias'] = 2; 
                    $this->Maxlen['descripcion'] = 50;
                    $this->SearchGrid = new SearchGrid();
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']        = array('60px','center','');
                    $Columns['Versión']    = array('100px','center','');
                    $Columns['Cohorte']   = array('80px','center','');
                    $Columns['Materias']   = array('80px','center','');
                    $Columns['IdEstado']  = array('0px','center','none');
                    $Columns['Estado']    = array('110px','center','');
                    $Columns['Maestria']  = array('0px','center','none');
                    $Columns['IdMaestria']  = array('0px','center','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }
               
            function CreaMantenimiento()
            {      $Search = new SearchInput();
                    $HTML = '<table border=0 class="Form-Frame" cellpadding="0">';                 
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label">Maestría</td>';
                    $HTML.= '           <td class="Form-Label" colspan="3">';
                    $HTML.= $Search->TextSch("maestria","","")->Enabled(true)->Create("t13");
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">Id</td>';
                    $HTML.= '             <td class="Form-Label" style="width:80%">';
                    $HTML.= '                <input type="text" class="txt-input t01" id="id" name="id" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\'descripcion:cohortes:materias\',\'NUM\'); " onBlur="return SearchByElement(this);" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';                   
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Version</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                 <input type="text" class="txt-upper t02" id="descripcion" name="descripcion" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">N. Cohorte</td>';
                    $HTML.= '             <td class="Form-Label" style="width:80%">';
                    $HTML.= '                <input type="text" class="txt-input t02" id="cohortes" name="cohortes" value="" maxlength="'.$this->Maxlen['cohorte'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>'; 
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">Materias</td>';
                    $HTML.= '             <td class="Form-Label" style="width:80%">';
                    $HTML.= '                <input type="text" class="txt-input t02" id="materias" name="materias" value="" maxlength="'.$this->Maxlen['materias'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';       
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Estado</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= $this->CreaComboEstadoByArray(array(0,1));
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function CreaBarButton($Buttons)
            {       $BarButton = new BarButton();    
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGrid($ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    return $this->SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
            
            private function GridDataHTML($Grid,$ArDatos)
            {       if ($ArDatos[0])
                    {   foreach ($ArDatos[1] as $ArDato)
                        {       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);                       
                                $Grid->CreaSearchCellsDetalle($id,'',$id);                     
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['VERSION'])));  
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['COHORTES']);
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['MATERIAS']);
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMAESTRIA']);
                                $Grid->CreaSearchRowsDetalle ($id,($ArDato['IDESTADO']==0 ? "red" : ""));
                        }
                    }    
                    return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
            }
               
            function MuestraFormulario($Ajax,$Materia)
            {   if ($Materia)
                    {   if ($Materia->ID>=0)
                        {   $Ajax->Assign("id","value", str_pad($Materia->ID,$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion","value", strtoupper(utf8_encode(trim($Materia->VERSION))));
                            $Ajax->Assign("cohortes","value", utf8_encode(trim($Materia->COHORTES)));
                            $Ajax->Assign("materias","value", utf8_encode(trim($Materia->MATERIAS)));
                            $Ajax->Assign("estado","value", $Materia->IDESTADO);
                            $Ajax->call("BarButtonState","Active");
                            return $Ajax;
                        }    
                    }
                    return $this->Respuesta($Ajax,"CMD_WRM","No existe una version con el ID ingresado.");
            }
            
            function MuestraGuardado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);
            }
            
            function MuestraGrid($Ajax,$ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    $Ajax->Assign($this->SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }
            
            function MuestraEditado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraRowGrid($Ajax, $ArDatos[1][0]);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }    
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);            
            }
    
            function MuestraRowGrid($Ajax,$ArDato)
            {       $RowId = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$ArDato['IDESTADO']);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);      
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",utf8_encode(trim($ArDato['VERSION'])));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",$ArDato['COHORTES']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$ArDato['MATERIAS']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$ArDato['IDESTADO']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO")); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",utf8_encode(trim($ArDato['MAESTRIA']))); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$ArDato['IDMAESTRIA']);
                    return $Ajax;
            }                                
            
            function MuestraEliminado($Ajax,$JsDatos)
            {       $RowId = str_pad($JsDatos->id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$JsDatos->estado);
                    
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);       
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$JsDatos->estado); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML","INACTIVO"); 
                    return $this->Respuesta($Ajax,"CMD_DEL",0);
            }

            private function Respuesta($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse";
                    $Ajax->call($jsFuncion,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox();
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Enabled(false)->Fields("id","descripcion")->Create("s05");
            }   
             
            
            function MuestraEditadobyMaestria($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$id);
                    }
                else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_MAE_ERR",$ArDatos[1]);            
            }
            
            function MuestrabyMaestria($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$id);
                    }
                    else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_MAE_ERR",$ArDatos[1]);            
            }
            
            /*** MODAL ***/
                private function SearchGridModalConfig()
                {       
                        $Columns['IdFacultad'] = array('0px','center','none');
                        $Columns['Facultad']  = array('170px','left','');
                        $Columns['IdMaestria'] = array('70px','center','');
                        $Columns['Maestria'] = array('250px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$ArDatos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcion($SearchGrid,$ArDatos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcion($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalOpcion($Grid,$ArDatos)
                {       if ($ArDatos[0])
                        {   foreach ($ArDatos[1] as $ArDato)
                            {       
                                    $id = str_pad($ArDato['IDMAESTRIA'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDFACULTAD']);
                                    $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['FACULTAD']));
                                    $Grid->CreaSearchCellsDetalle($id,'',$id);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MAESTRIA'])));   
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }
        }
?>

