<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioVMaestria.php");
        require_once("src/modules/maestria/vmaestria/render/RenderVMaestria.php");

        class ControlVMaestria
        {       private  $ServicioVMaestria;
                private  $RenderVMaestria;

                function __construct()
                {       $this->ServicioVMaestria = new ServicioVMaestria();
                        $this->RenderVMaestria = new RenderVMaestria();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderVMaestria->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderVMaestria->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => 'NULL');
                        $Datos = $this->ServicioVMaestria->BuscarVMaestriaByDescripcion($prepareDQL);
                        echo $this->RenderVMaestria->CreaSearchGrid($Datos);
                }         
               
                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioVMaestria->BuscarVMaestriaByID($id);
                        return $this->RenderVMaestria->MuestraFormulario($ajaxRespon,$Datos);
                }
                
                function ConsultaByIDByMaestria($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('id' => ($JsDatos -> id), 'id_maestria' => $JsDatos->idmaestria);
                        $ArDatos = $this->ServicioVMaestria->BuscarVMaestriaByIDByMaestria($prepareDQL);
                        return $this->RenderVMaestria->MuestraFormulario($ajaxRespon,$ArDatos);
                }

                function ConsultaByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();              
                        $prepareDQL = array('texto' => strtoupper(trim($Texto)));
                        $ArDatos = $this->ServicioVMaestria->BuscarVMaestriaByDescripcion($prepareDQL);
                        return $this->RenderVMaestria->MuestraGrid($ajaxRespon,$ArDatos);
                }
                
                function ConsultaByTXByMaestria($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('texto' => strtoupper(trim($JsDatos -> FindTextBx)), 'id' => $JsDatos->idmaestria );
                        $ArDatos = $this->ServicioVMaestria->BuscarVMaestriaByDescripcionByMaestria($prepareDQL);
                        return $this->RenderVMaestria->MuestraGrid($ajaxRespon,$ArDatos);
                }

                function Guarda($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        if($JsDatos->cohortes <6 && $JsDatos->cohortes >0){                              
                            $funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");
                                $validar = $ArDatos = $this->ServicioVMaestria->ValidarVMaestriaByIdMaestria($JsDatos);
                                if(empty($validar))
                                    {$id = $this->ServicioVMaestria->GuardaDBVMaestriaSinVersion($JsDatos);
                                    $ArDatos = $this->ServicioVMaestria->BuscarVMaestriaEstadoByID($id);
                                    return $this->RenderVMaestria->{$funcion}($ajaxRespon,$ArDatos);
                                    }
                                else{
                                    $this->ServicioVMaestria->DesactivaVMaestriaVersionAnterior($JsDatos->idmaestria);
                                    $id = $this->ServicioVMaestria->GuardaDBVMaestriaConVersion($JsDatos);
                                    $ArDatos = $this->ServicioVMaestria->BuscarVMaestriaEstadoByID($id);
                                    return $this->RenderVMaestria->{$funcion}($ajaxRespon,$ArDatos);
                                    }}
                        else {$jsFuncion = "XAJAXResponse";
                            $ajaxRespon->call($jsFuncion,"CMD_WRM_COH","El campo Cohorte debe ser MÍNIMO 1 y MÁXIMO 5.");}
                            return $ajaxRespon;
                }

                function Elimina($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsData = json_decode($Form)->Forma;
                        $JsData->estado = $this->ServicioVMaestria->DesactivaVMaestria($JsData->id);
                        return $this->RenderVMaestria->MuestraEliminado($ajaxRespon,$JsData);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1));
                        if ($Operacion==="btmaestria")    
                        {                            
                            $ArDatos = $this->ServicioVMaestria->BuscarMaestriaByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestrias";
                            $jsonModal['Carga'] = $this->RenderVMaestria->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "517";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        if ($Operacion==="btmaestria")    
                        {   $Datos = $this->ServicioVMaestria->BuscarMaestriaByDescripcion($prepareDQL);
                            return $this->RenderVMaestria->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
                
                function MostrarVMaestriabyMaestria($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->txmaestria) ? "MuestrabyMaestria" : "MuestraEditadobyMaestria");
                        $id = $JsDatos->idmaestria;
                        $ArDatos = $this->ServicioVMaestria->BuscarVMaestriaEstadoByMaestria($id);
                        return $this->RenderVMaestria->{$funcion}($ajaxRespon,$ArDatos);
                }
        }       
?>