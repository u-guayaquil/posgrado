var objRotacion = "id:descripcion:feinicio:fefin:estado:txcohorte:idcohorte:txhospital:idhospital";
var valRotacion = "txhospital:descripcion:feinicio:fefin";

function ButtonClick(Operacion)
{   var valRotacion = "txhospital:descripcion:feinicio:fefin";
    if (Operacion==='addNew')
    {   BarButtonState(Operacion);
        ElementStatus(valRotacion+":id:bthospital","id:txcohorte:btcohorte:txvmaestria:btvmaestria");
        ElementClear(valRotacion+":id:idhospital");
        ElementSetValue("estado",1);
    }
    else if (Operacion==='addMod')
    {       BarButtonState(Operacion);
            if (ElementGetValue("estado")==0)
            {   valRotacion = valRotacion + ":estado";
            }
            ElementStatus(valRotacion+":bthospital","id:txcohorte:btcohorte:txvmaestria:btvmaestria");
    }
    else if (Operacion==='addDel')
    {       if (ElementGetValue("estado")==1)
            {   Swal.fire({
                    title: '¿Desactivar registro?',
                    text: "Esta seguro que desea desactivar el registro?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro!'
                }).then(function(result){
                    if (result.value) {
                        var Forma = PrepareElements("id:estado");
                        xajax_Elimina(JSON.stringify({Forma}));
                    }
                });  
            }
            else{SAlert("warning","¡Registro Inactivo!","El registro se encuentra inactivo");}
    }
    else if (Operacion==='addSav')
    {   var Inicio = ElementGetValue("feinicio");    
        var Inicio_Format = formato(Inicio);
        var Fin = ElementGetValue("fefin");
        var Fin_Format = formato(Fin);
        if (ElementValidateBeforeSave(valRotacion))
        {   if(Fin_Format<Inicio_Format){
                SAlert("warning","¡Fecha Incorrectas!","¡La fecha inicio debe ser mayor a la fecha fin!");
                return;
            }  
            if (BarButtonState("Inactive"))
            {   var Forma = PrepareElements(objRotacion);
                xajax_Guarda(JSON.stringify({Forma}));
            }
        }
        else
        {SAlert("warning","¡Cuidado!","Llene los campos por favor");}
    }
    else if (Operacion==='addCan')
    {       BarButtonState(Operacion);
            ElementStatus("id:btcohorte:txcohorte:txvmaestria:btvmaestria",valRotacion+":estado");
            ElementClear("id:descripcion:idhospital:txhospital:feinicio:fefin");
            ElementSetValue("estado",1);
    }
    else if (Operacion==='btvmaestria')
    {   xajax_CargaModal(Operacion,0);
    }
    else if (Operacion==='btcohorte')
    {   var IDMST=ElementGetValue("idvmaestria");
        if (IDMST!=""){
            xajax_CargaModal(Operacion,IDMST);
        }
        else
        SAlert("warning","¡Error!","¡Debe seleccionar una maestría!");
    }
    else if (Operacion==='bthospital')
    {   var IDMST=ElementGetValue("idvmaestria");
        var IDCHT=ElementGetValue("idcohorte");
        if (IDMST!="" && IDCHT!=""){
            xajax_CargaModal(Operacion,0);
        }
        else
        SAlert("warning","¡Error!","¡Debe seleccionar una cohorte!");
    }
    return false;
}

function SearchByElement(Elemento)
{   Elemento.value = TrimElement(Elemento.value);
    if (Elemento.name=="id")
    {   if (Elemento.value.length != 0)
        {   ContentFlag = document.getElementById("descripcion");
            if (ContentFlag.value.length==0)
            {   if (BarButtonState("Inactive"))
                {   var Forma = PrepareElements("id:idcohorte");
                    xajax_BuscaByID(JSON.stringify({Forma}));
                }
            }
        }
        else
        BarButtonState("Default"); 
    }
    else if (Elemento.id === "FindVmaestriaTextBx")
    {   
        xajax_BuscaModalByTX("btvmaestria",Elemento.value);
    }
    else if (Elemento.id === "FindCohorteTextBx")
    {   var Params = [Elemento.value,ElementGetValue("idvmaestria")];
        xajax_BuscaModalByTX("btcohorte",Params);
    } 
    else if (Elemento.id === "FindHospitalTextBx")
    {   xajax_BuscaModalByTX("bthospital",Elemento.value);
    } 
    else if(Elemento.id === "FindTextBx")
    {
        var Forma = PrepareElements("FindTextBx:idcohorte");
        xajax_BuscaByTX(JSON.stringify({Forma}));
    }
}

function SearchVmaestriaGetData(DatosGrid)
{   document.getElementById("idvmaestria").value = DatosGrid.cells[3].childNodes[0].nodeValue;
    document.getElementById("txvmaestria").value = DatosGrid.cells[2].childNodes[0].nodeValue.toUpperCase() + ' ' + DatosGrid.cells[4].childNodes[0].nodeValue;
    document.getElementById("version").value = DatosGrid.cells[4].childNodes[0].nodeValue;
    ElementStatus("txcohorte:btcohorte:txvmaestria:btvmaestria","id:descripcion:idhospital:txhospital:feinicio:fefin:estado");
    ElementClear("id:descripcion:feinicio:fefin:idcohorte:txcohorte:idhospital:txhospital");
    ElementSetValue("estado",1);
    BarButtonState("Inactive");
    cerrar();
    if(document.getElementById("DetSch"))
        $("#DetSch").remove();
}

function SearchCohorteGetData(DatosGrid)
{   document.getElementById("idcohorte").value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txcohorte").value = DatosGrid.cells[1].childNodes[0].nodeValue.toUpperCase();
    BarButtonState("Active");
    ElementStatus("id:txcohorte:btcohorte:txvmaestria:btvmaestria","descripcion:txhospital:feinicio:fefin:estado");
    ElementClear("id:descripcion:feinicio:fefin:idhospital:txhospital");
    ElementSetValue("estado",1);
    cerrar();
    var Forma = PrepareElements("idcohorte");
    xajax_MostrarbyCohorte(JSON.stringify({Forma}));
}

function SearchHospitalGetData(DatosGrid)
{   document.getElementById("idhospital").value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txhospital").value = DatosGrid.cells[1].childNodes[0].nodeValue.toUpperCase();
    cerrar();
}
    
function SearchGetData(Grilla)
{   if (IsDisabled("addSav"))
    {   BarButtonState("Active");
        document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("idhospital").value = Grilla.cells[1].childNodes[0].nodeValue;
        document.getElementById("txhospital").value = Grilla.cells[2].childNodes[0].nodeValue.toUpperCase(); 
        document.getElementById("descripcion").value = Grilla.cells[3].childNodes[0].nodeValue.toUpperCase(); 
        document.getElementById("feinicio").value = Grilla.cells[4].childNodes[0].nodeValue;;
        document.getElementById("fefin").value = Grilla.cells[5].childNodes[0].nodeValue;
        document.getElementById("estado").value = Grilla.cells[6].childNodes[0].nodeValue; 
    }
    return false;
}

function formato(string) {
    var info = string.split('/').reverse().join('-');
    return info;
}

function XAJAXResponse(Mensaje,Datos)
{   if (Mensaje==="CMD_SAV")
    {   BarButtonState("Default");
        ElementClear("id:"+valRotacion);
        ElementStatus("id:btcohorte:txcohorte:txvmaestria:btvmaestria",valRotacion+":estado");
        ElementSetValue("estado",1);
        SAlert("success","¡Excelente!",Datos);
    }
    else if(Mensaje==='CMD_ERR')
    {   BarButtonState("addNew");
        SAlert("error","¡Error!",Datos); 
    }    
    else if(Mensaje==='CMD_DEL')
    {   ElementSetValue("estado",Datos);
        SAlert("success","¡Excelente!","Datos Guardados Correctamente");
    }
    else if(Mensaje==="CMD_WRM")
    {   BarButtonState("Default");
        ElementClear("id");
        ElementSetValue("estado",1);
        SAlert("info","¡Información!", Datos); 
    }
}