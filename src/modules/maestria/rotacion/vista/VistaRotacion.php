<?php   
	$Opcion = 'Rotacion';
	require_once('src/modules/maestria/rotacion/controlador/ControlRotacion.php');
	$ControlRotacion = new ControlRotacion();

	$xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlRotacion,'Guarda'));
	$xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlRotacion,'Elimina'));
	$xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlRotacion,'ConsultaByID'));
	$xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlRotacion,'ConsultaByTX'));
	$xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlRotacion,'CargaModalGrid'));
	$xajax->register(XAJAX_FUNCTION,array('CargaModalCohorte', $ControlRotacion,'CargaModalGridCohorte'));
	$xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlRotacion,'ConsultaModalGridByTX'));
	$xajax->register(XAJAX_FUNCTION,array('MostrarbyCohorte', $ControlRotacion,'MostrarRotacionbyCohorte'));
	
	$xajax->processRequest();       
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>

	<?php $xajax->printJavascript(); 
			require_once('src/utils/links.php');
	?>
	<script type="text/javascript" src="src/modules/maestria/rotacion/js/jsRotacion.js?1"></script>
	<!-- Data Picker -->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
	<body>	
		<div class="FormBasic" style="width:655px">
			<div class="FormSectionMenu">              
				<?php   $ControlRotacion->CargaBarButton($_GET['opcion']);
						echo '<script type="text/javascript">';
						echo '        BarButtonState("Inactive"); ';
						echo '</script>';                    
				?>    
			</div>  
			
			<div class="FormSectionData">              
				<form id="<?php echo 'Form'.$Opcion; ?>">
				<?php  $ControlRotacion->CargaMantenimiento();  ?>
				</form>
			</div> 
			
			<div class="FormSectionGrid">          
				<?php   $ControlRotacion->CargaSearchGrid();  ?>
			</div>  
		</div>
	<!-- Data Picker -->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		$( function() {
			$.datepicker.regional['es'];
			$( "#feinicio, #fefin" ).datepicker({
				closeText: "Cerrar",
				prevText: "&#x3C;Ant",
				nextText: "Sig&#x3E;",
				currentText: "Hoy",
				monthNames: [ "enero","febrero","marzo","abril","mayo","junio",
				"julio","agosto","septiembre","octubre","noviembre","diciembre" ],
				monthNamesShort: [ "ene","feb","mar","abr","may","jun",
				"jul","ago","sep","oct","nov","dic" ],
				dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
				dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
				dayNamesMin: [ "D","L","M","X","J","V","S" ],
				weekHeader: "Sm",
				changeMonth: true,
				changeYear: true,
				dateFormat: 'dd/mm/yy',
				//yearRange: '1946:-20',
				//maxDate: '-4m-20y'
			});
		});
	</script>
	</body>
</html>
         