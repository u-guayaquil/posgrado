<?php
require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
require_once("src/rules/maestria/servicio/ServicioRotacion.php");
require_once("src/modules/maestria/rotacion/render/RenderRotacion.php");

class ControlRotacion
{   private  $ServicioRotacion;
	private  $RenderRotacion;

	function __construct()
	{   $this->ServicioRotacion = new ServicioRotacion();
		$this->RenderRotacion = new RenderRotacion();
	}

	function CargaBarButton($Opcion)
	{   $ServicioPerfilOpcion = new ServicioPerfilOpcion();
		$Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
		if ($Datos[0])
		echo $this->RenderRotacion->CreaBarButton($Datos[1]);
	}
	
	function CargaMantenimiento()
	{	echo $this->RenderRotacion->CreaMantenimiento();
	}

	function CargaSearchGrid()
	{   $prepareDQL = array('limite' => 50,'inicio' => 0,'txt' => 'NULL', 'idCohorte' => '0');
		$Datos = $this->ServicioRotacion->BuscarRotacionByDescripcion($prepareDQL);
		echo $this->RenderRotacion->CreaSearchGrid($Datos);
	}         
	
	function CargaModalGrid($Operacion,$IdRef)
	{  	$ajaxResp = new xajaxResponse();
		if ($Operacion==="btvmaestria")
		{   $Params = array('txt'=>'');
			$ArDatos = $this->ServicioRotacion->BuscarMaestriaByDescripcion($Params);
			$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
			$jsonModal['Title'] = "Busca Maestria";
			$jsonModal['Carga'] = $this->RenderRotacion->CreaModalGrid($Operacion,$ArDatos);
			$jsonModal['Ancho'] = "640";
		}
		if ($Operacion==="btcohorte")
		{   $Params = array('txt'=>'','mtr'=>$IdRef);
			$ArDatos = $this->ServicioRotacion->BuscarCohorteByDescripcion($Params);
			$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
			$jsonModal['Title'] = "Busca Cohorte";
			$jsonModal['Carga'] = $this->RenderRotacion->CreaModalGrid($Operacion,$ArDatos);
			$jsonModal['Ancho'] = "360";
		}
		if ($Operacion==="bthospital")
		{   $Params = array('txt'=>'');
			$ArDatos = $this->ServicioRotacion->BuscarHospitalByDescripcion($Params);
			$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
			$jsonModal['Title'] = "Busca Cohorte";
			$jsonModal['Carga'] = $this->RenderRotacion->CreaModalGrid($Operacion,$ArDatos);
			$jsonModal['Ancho'] = "472";
		}
		$ajaxResp->call("CreaModal", json_encode($jsonModal));
		return $ajaxResp; 
	}
	
	function ConsultaModalGridByTX($Operacion,$Texto = "")
	{   $ajaxResp = new xajaxResponse();
		if ($Operacion==="btvmaestria")    
		{   $texto = trim($Texto);
			$prepareDQL = array('limite' => 50,'inicio' => 0,'txt' => strtoupper($texto), 'tipo' => array(0,1));
			$Datos = $this->ServicioRotacion->BuscarMaestriaByDescripcion($prepareDQL);
			return $this->RenderRotacion->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
		}    
		if ($Operacion==="btcohorte")    
		{   $Texto[0] = strtoupper(trim($Texto[0]));
			$prepareDQL = array('limite' => 50,'inicio' => 0,'txt'=>$Texto[0],'mtr'=>$Texto[1], 'tipo' => array(0,1));
			$Datos = $this->ServicioRotacion->BuscarCohorteByDescripcion($prepareDQL);
			return $this->RenderRotacion->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
		}    
		if ($Operacion==="bthospital")    
		{   $texto = trim($Texto);
			$prepareDQL = array('limite' => 50,'inicio' => 0,'txt'=>strtoupper($texto),'tipo' => array(0,1));
			$Datos = $this->ServicioRotacion->BuscarHospitalByDescripcion($prepareDQL);
			return $this->RenderRotacion->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
		}    
	}
	
	function MostrarRotacionbyCohorte($Form)
	{   $ajaxRespon = new xajaxResponse();
		$JsDatos = json_decode($Form)->Forma;
		$idCohorte = $JsDatos->idcohorte;
		$ArDatos = $this->ServicioRotacion->BuscarRotacionByCohorte($idCohorte);
		return $this->RenderRotacion->MuestraGrid($ajaxRespon,$ArDatos);
	}

	function ConsultaByID($Form)
	{   $ajaxRespon = new xajaxResponse(); 
		$JsDatos = json_decode($Form)->Forma;
		$prepareDQL = array('id' => ($JsDatos -> id), 'idCohorte' => $JsDatos->idcohorte);
		$ArDatos = $this->ServicioRotacion->BuscarRotacionByID($prepareDQL);
		return $this->RenderRotacion->MuestraFormulario($ajaxRespon,$ArDatos);
	}
	
	function ConsultaByTX($Form)
	{   $ajaxRespon = new xajaxResponse(); 
		$JsDatos = json_decode($Form)->Forma;
		$prepareDQL = array('txt' => strtoupper(trim($JsDatos -> FindTextBx)), 'idCohorte' => $JsDatos->idcohorte );
		$ArDatos = $this->ServicioRotacion->BuscarRotacionByDescripcion($prepareDQL);
		return $this->RenderRotacion->MuestraGrid($ajaxRespon,$ArDatos);
	}

	function Guarda($Form)
	{   $ajaxRespon = new xajaxResponse();
		$JsDatos = json_decode($Form)->Forma;
		$funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");
		$id = $this->ServicioRotacion->GuardaDB($JsDatos);
		$ArDatos = $this->ServicioRotacion->BuscarRotacionByIDProcesado($id);
		return $this->RenderRotacion->{$funcion}($ajaxRespon,$ArDatos);
	}

	function Elimina($Form)
	{   $ajaxRespon = new xajaxResponse();
		$JsData = json_decode($Form)->Forma;
		$JsData->estado = $this->ServicioRotacion->DesactivaRotacion($JsData->id);
		return $this->RenderRotacion->MuestraEliminado($ajaxRespon,$JsData);
	}
}       
?>