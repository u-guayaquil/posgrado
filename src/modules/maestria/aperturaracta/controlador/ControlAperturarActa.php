<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");    
        require_once("src/rules/maestria/servicio/ServicioAperturarActa.php");
        require_once("src/modules/maestria/aperturaracta/render/RenderAperturarActa.php");        
        require_once("src/rules/maestria/servicio/ServicioDocente.php");  
        require_once("src/rules/maestria/servicio/ServicioPlanAnalitico.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");
        require_once("src/rules/maestria/servicio/ServicioPoliticaEvaluacion.php"); 
        
        class ControlAperturarActa
        {       private $ServicioAperturarActa;
                private $ServicioDocente;
                private $RenderAperturarActa;
                private $ServicioPlanAnalitico;
                private $ServicioUsuarioFiltro;
                private $ServicioPoliticaEvaluacion;
                
                function __construct()
                {       $this->ServicioAperturarActa = new ServicioAperturarActa();
                        $this->ServicioDocente = new ServicioDocente();        
                        $this->RenderAperturarActa = new RenderAperturarActa();
                        $this->ServicioPlanAnalitico = new ServicioPlanAnalitico();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                        $this->ServicioPoliticaEvaluacion = new ServicioPoliticaEvaluacion();
                }

                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderAperturarActa->CreaBarButton($Buttons[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderAperturarActa->CreaMantenimiento();
                }

                function ConsultaByID($Params)
                {       $xAjax = new xajaxResponse();
                        $Notas = $this->ServicioAperturarActa->ConsultarNotas($Params);
                        if ($Notas[0])
                        {   $Existe = $this->ServicioAperturarActa->ConsultaCierreActa($Params);
                            if ($Existe==1)
                            {$xAjax->Call("XAJAXResponse","CMD_OK","El acta se encuentra cerrada.");}    
                            else
                            {$xAjax->Call("XAJAXResponse","CMD_OPEN","El acta se encuentra aperturada");}
                        }
                        else
                        $xAjax->Call("XAJAXResponse","CMD_NULL",$Notas[1]);                         
                        return $xAjax;
                }
                
                function ConsultaParcialesByID($IdPolitica)
                {       $xAjax = new xajaxResponse();
                        $Polev = $this->ServicioPoliticaEvaluacion->ConsultaParcialesPoliticaEvaluacion($IdPolitica);
                        $xAjax->Call("FillOptionCbx","TipoNota",$Polev[1]);
                        $xAjax->Call("ElementSetValue","TipoNota",0);
                        if (!$Polev[0]) $xAjax->Call("XAJAXResponse","CMD_NULL","No existe definida una estructura para calificaciones.");
                        return $xAjax;
                }
                
                function GuardaDB($Params)
                {   $xAjax = new xajaxResponse();       
                    $Datos = $this->ServicioAperturarActa->GuardaDB($Params);
                    if ($Datos == "OK")
                    {   $xAjax->Call("XAJAXResponse","CMD_SAVE","Se aperturó el acta con éxito");
                    }
                    else
                    $xAjax->Call("XAJAXResponse","CMD_NULL","No se pudo aperturar el acta de calificaciones.");   
                    return $xAjax;
                }
             
                private function FilterOfAccess($Params)
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        $Docnte = $Params['dct'];

                        if ($Modulo->Idrol==3)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            if ($Docnte==0){
                                return array('texto'=>$Params['txt'], 'facultad'=>$Factad); 
                            }    
                            return array('texto'=>$Params['txt'], 'facultad'=>$Factad, 'docente'=>$Params['dct']);
                        }
                        else if ($Modulo->Idrol==2)
                        {   $Dcte = ($Docnte==0 ? $Modulo->Cedula : $Docnte); 
                            return array('texto'=>$Params['txt'], 'docente'=>$Dcte);
                        }
                        else
                        return array('facultad'=>array('00')); //No permiso a Estudiante
                }
                
                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion=="btdocente")
                        {   $Params = array('txt'=>'','dct'=>0);
                            $ArDatos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Docente";
                            $jsonModal['Carga'] = $this->RenderAperturarActa->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAperturarActa->GetGridTableWidth();
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Params = array('txt'=>'','dct'=>$IdRef);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderAperturarActa->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAperturarActa->GetGridTableWidth();
                        }   
                        else if ($Operacion==="btmateria")    
                        {   $prepareDQL = array('docente'=>$IdRef[0],'ciclo'=>$IdRef[1],'maestria'=>$IdRef[2]);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Materia";
                            $jsonModal['Carga'] = $this->RenderAperturarActa->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAperturarActa->GetGridTableWidth();
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btdocente")    
                        {   $Params = array('txt'=>strtoupper(trim($Texto)),'dct'=>0);
                            $Datos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            return $this->RenderAperturarActa->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $Params = array('txt'=>$Texto[0],'dct'=>$Texto[1]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            return $this->RenderAperturarActa->MuestraModalGrid($xAjax,$Operacion,$Datos);                                       
                        }                        
                        else if ($Operacion=="btmateria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $prepareDQL = array('texto'=>$Texto[0],'docente'=>$Texto[1],'ciclo'=>$Texto[2],'maestria'=>$Texto[3]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            return $this->RenderAperturarActa->MuestraModalGrid($xAjax,$Operacion,$Datos);         
                        }
                }
        }

?>

