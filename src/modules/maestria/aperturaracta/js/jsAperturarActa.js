﻿            var ACTACERRADA = 0;
            var NTETAP = "";
            
            function ButtonClick(Operacion)
            {       if (Operacion=="MinSav")
                    {   if (ACTACERRADA==1)
                        {   var DOCTE = ElementGetValue("iddocente");
                            var MAESV = ElementGetValue("idmaesv");
                            var CICLO = ElementGetValue("idciclo");   
                            var MAEST = ElementGetValue("idmaestria");
                            var MATER = ElementGetValue("idmateria");
                            var GRUPO = ElementGetValue("idgrupo");
                            var POLEV = ElementGetValue("idpolev");
                            var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP,MAESV];
                            Swal.fire({
                                title: 'Estás seguro?',
                                text: "Esta seguro que desea aperturar esta acta?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Si, estoy seguro!'
                            }).then(function(result){
                                if (result.value) {
                                    ElementStatus("","MinSav");
                                    xajax_GuardaDB(Params);
                                }
                            })
                        }
                    }
                    else if (Operacion=="btdocente")
                    {    xajax_CargaModal(Operacion,0);
                    }
                    else if (Operacion=="btmaestria")
                    {    var IDDOC=ElementGetValue("iddocente");
                         if (IDDOC!="")
                            xajax_CargaModal(Operacion,IDDOC);
                         else
                         {
                            SAlert("warning","¡Error!","¡Debe seleccionar un Docente!");
                         }
                    }
                    else if (Operacion=="btmateria")
                    {    var IDMST=ElementGetValue("idmaestria");
                         if (IDMST!="")
                         {   var IDDOC=ElementGetValue("iddocente");
                             var IDCLC=ElementGetValue("idciclo");
                             var Params = [IDDOC,IDCLC,IDMST];
                             xajax_CargaModal(Operacion,Params);
                         }
                         else
                         {
                            SAlert("warning","¡Error!","¡Debe seleccionar una Maestria!");
                         }
                    }
                    return false;
            }

            function SearchDocenteGetData(DatosGrid)
            {       var IDDOC = DatosGrid.cells[0].innerHTML;
                    if (ElementGetValue("iddocente")!=IDDOC)
                    {   NTETAP = "";
                        FillOptionCbx("TipoNota",[[0,"SELECCIONE..."]]);
                        ElementStatus("","MinSav");
                        ElementSetValue("TipoNota",0);
                        ElementClear("idmaestria:txmaestria:idciclo:idmateria:txmateria:idgrupo:txgrupo:idpolev:idmaesv");
                        document.getElementById("iddocente").value = IDDOC;
                        document.getElementById("txdocente").value = DatosGrid.cells[2].innerHTML;
                        cerrar();
                    }    
                    else
                    cerrar();
                    return false;
            }

            function SearchMaestriaGetData(DatosGrid)
            {       var IDMST = DatosGrid.cells[0].innerHTML;
                    if (ElementGetValue("idmaestria")!=IDMST)
                    {   NTETAP = "";
                        FillOptionCbx("TipoNota",[[0,"SELECCIONE..."]]);
                        ElementStatus("","MinSav");
                        ElementClear("idmateria:txmateria:idgrupo:txgrupo");
                        document.getElementById("idmaestria").value = IDMST;
                        document.getElementById("idciclo").value = DatosGrid.cells[2].innerHTML;
                        document.getElementById("idpolev").value = DatosGrid.cells[4].innerHTML;
                        document.getElementById("idmaesv").value = DatosGrid.cells[5].innerHTML;
                        document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML;
                        cerrar();
                    }
                    else
                    cerrar();    
                    return false;
            }  
    
            function SearchMateriaGetData(DatosGrid)
            {       var IDGRP = DatosGrid.cells[2].innerHTML;
            
                    if (ElementGetValue("idgrupo")!=IDGRP)
                    {   NTETAP = "";
                        ElementStatus("","MinSav");
                        ElementSetValue("TipoNota",0);
                        document.getElementById("idmateria").value = DatosGrid.cells[0].innerHTML;;
                        document.getElementById("txmateria").value = DatosGrid.cells[1].innerHTML;
                        document.getElementById("idgrupo").value = IDGRP;
                        document.getElementById("txgrupo").value = DatosGrid.cells[3].innerHTML;
                        xajax_BuscaParcialesByID(ElementGetValue("idpolev"));
                        cerrar();
                    }    
                    else    
                    cerrar();    
                    return false;
            }
            
            function SearchTipoNotaGetData(DatosCombo)
            {   if (DatosCombo.value==0)
                    {   ACTACERRADA = 0;  
                        NTETAP = "";
                        ElementStatus("","MinSav");
                    }
                else
                    {   var ETAPA = ElementGetText(DatosCombo.id);                     
                        if (NTETAP!=ETAPA)
                        {   NTETAP = ETAPA;
                            ElementStatus("MinSav","");
                            SearchNotasGetDataGrid();
                        }
                    }    
            }  
            
            function SearchNotasGetDataGrid()
            {       var DOCTE = ElementGetValue("iddocente");
                    var MAESV = ElementGetValue("idmaesv");
                    var CICLO = ElementGetValue("idciclo");   
                    var MAEST = ElementGetValue("idmaestria");
                    var MATER = ElementGetValue("idmateria");
                    var GRUPO = ElementGetValue("idgrupo");
                    var POLEV = ElementGetValue("idpolev");
                    var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP,MAESV];
                    xajax_BuscaByID(Params);
            }  

            function SearchByElement(Elemento)
            {       if (Elemento.id=="FindDocenteTextBx")
                    {    xajax_BuscaModalByTX("btdocente",Elemento.value);
                    }
                    else if (Elemento.id=="FindMaestriaTextBx")
                    {    var Params = [Elemento.value,ElementGetValue("iddocente")];
                         xajax_BuscaModalByTX("btmaestria",Params);
                    }
                    else if (Elemento.id=="FindMateriaTextBx")
                    {    var IDDOC=ElementGetValue("iddocente");
                         var IDCLC=ElementGetValue("idciclo");
                         var IDMST=ElementGetValue("idmaestria");
                         var Params = [Elemento.value,IDDOC,IDCLC,IDMST];
                         xajax_BuscaModalByTX("btmateria",Params);
                    }
                    return false;    
            }

            function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_OK")
                        {   ElementStatus("MinSav","");
                            SAlert("warning","",Datos);
                            ACTACERRADA = 1;
                        }
                        if (Mensaje==="CMD_OPEN")
                        {   ElementStatus("","MinSav");
                            SAlert("success","",Datos);
                            ACTACERRADA = 0;
                        }
                        if (Mensaje==="CMD_NULL")
                        {   ElementStatus("","MinSav");
                            SAlert("error","",Datos);
                            ACTACERRADA = 0;
                        }
                        if (Mensaje==="CMD_SAVE")
                        {   ElementStatus("","MinSav");
                            ElementSetValue("TipoNota",0);
                            NTETAP = "";
                            SAlert("success","Excelente!",Datos);
                            ACTACERRADA = 0;
                        }
                }