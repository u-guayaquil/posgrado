<?php   
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php"); 
        require_once("src/rules/maestria/servicio/ServicioAperturarActa.php");
                
        class RenderAperturarActa
        {     private $Width;
              private $Search;
              
                function __construct()
                {       $this->Search = new SearchInput();   
                        $this->SearchGrid = new SearchGrid();
                        $this->ServicioAperturarActa = new ServicioAperturarActa();
                }

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaMantenimiento()
                {       $Col1="10%"; $Col2="40%"; $Col3="15%"; $Col4="15%"; $Col5="15%"; 

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="35">';
                        $HTML.= '           <td class="Form-Label" colspan="5">:: APERTURAR ACTA</td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Docente</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("docente","","")->Enabled(true)->Create("t11");   
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idciclo" name="idciclo" value="" />';
                        $HTML.= '               <input type="hidden" id="idpolev" name="idpolev" value="" />';
                        $HTML.= '               <input type="hidden" id="idmaesv" name="idmaesv" value="" />';
                        $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t11");                         
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';             
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Materia</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("materia","","")->Enabled(true)->Create("t11");       
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Grupo</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idgrupo" name="idgrupo" value="" />';
                        $HTML.= '               <input type="text" class="txt-input" style="width:275px" id="txgrupo" name="txgrupo" value="" readOnly/> ';   
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';  
                        $HTML.= '       <tr>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Notas</td>';
                        $HTML.= '               <td>';
                        $HTML.= '                       <select class="sel-input s11" id="TipoNota" name="TipoNota" onchange=" return SearchTipoNotaGetData(this)">';
                        $HTML.= '                               <option value="0">SELECCIONE...</option>';
                        $HTML.= '                       </select>'; 
                        $HTML.= '               </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr>';
                        $HTML.= '           <td class="Form-Label-Center" colspan="2" style="height:50px;">';
                        $HTML.='                <input type="button" id="MinSav" class="p-txt-label-bold" style="width: 120px;height: 30px;text-decoration: none;font-weight: 600;font-size: 11px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;" value="✔ APERTURAR ✔" onclick=" return ButtonClick(\'MinSav\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        return $HTML.'</table>';
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btdocente")
                        {   $Columns['id'] = array('0px','center','none');
                            $Columns['cedula'] = array('80px','center','');
                            $Columns['Docente']= array('300px','left','');
                        }          
                        else if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']=array('40px','center','');
                            $Columns['Maestria']=array('300px','left','');
                            $Columns['C.ID']=array('0px','center','none');
                            $Columns['Ciclo']=array('200px','left','');
                            $Columns['POL.ID']=array('0px','center','none');
                            $Columns['VER.ID']=array('0px','center','none');
                        }
                        else if ($Operacion=="btmateria")
                        {   $Columns['M.Id']=array('40px','center','');
                            $Columns['Materia']=array('250px','left','');
                            $Columns['G.ID']=array('40px','center','');
                            $Columns['Grupo']=array('150px','left','');
                        }
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btmateria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMATERIA'],MATERIA,'0',STR_PAD_LEFT);
                                        $ig = str_pad($ArDato['IDGRUPO'],GRUPO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ig);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['GRUPO']));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMAESTRIA'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MAESTRIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['CICLO']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDPOLEVAL']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMAESVER']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btdocente")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['CEDULA'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['APELLIDOS'])).' '.utf8_encode(trim($ArDato['NOMBRES'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }          
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
        }
?>

