<?php
        $Opcion = 'AperturarActa';
        require_once('src/modules/maestria/aperturaracta/controlador/ControlAperturarActa.php');
        $ControlAperturarActa = new ControlAperturarActa();

        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlAperturarActa,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlAperturarActa,'ConsultaGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaParcialesByID', $ControlAperturarActa,'ConsultaParcialesByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlAperturarActa,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaDB', $ControlAperturarActa,'GuardaDB'));
        $xajax->processRequest();
        
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/maestria/aperturaracta/js/jsAperturarActa.js?3"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:500px">
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionData">   
                             <?php  $ControlAperturarActa->CargaMantenimiento();  ?>
                        </div>  
                    </form>
                </div>       
        </body>
        </html>
              

