<?php    
        require_once("src/libs/clases/SearchGrid.php");
        
        class RenderAsistenciaPermisos
        {       private $SearchGrid;
            
                function __construct()
                {       $this->SearchGrid = new SearchGrid();
                }
        
                private function SearchGridConfig()
                {       $Columns['Id']         =array('50px','center','none');
                        $Columns['SOLICITUD']  =array('70px','center','');
                        $Columns['CLASE']      =array('90px','center','');
                        $Columns['INFORMACION']=array('350px','justify','');
                        $Columns['MOTIVO SOLICITUD']     =array('248px','justify','');
                        $Columns['✔']         =array('40px','center','');
                        $Columns['EST']=array('0px','center','none');
                        return array('AltoCab' => '35px','DatoCab' => $Columns,'AltoDet' => '300px','AltoRow' => '25px','FindTxt' =>false);
                }

                function CreaSearchGridAsistenciaPermisos($Datos)
                {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$Datos);
                        return $this->SearchGrid->CreaSearchGrid($GrdDataHTML);
                }

                function GridDataHTML($Grid,$Datos)
                {   if ($Datos[0])
                    {   $DateControl = new DateControl();
                        $FacQuiebres = "";
                        foreach ($Datos[1] as $Solicitud)
                        {       $id = str_pad($Solicitud['ID'],ASISPER,'0',STR_PAD_LEFT); //IDFAC
                                
                                if ($FacQuiebres!=$Solicitud['IDFAC'])
                                {   $Grid->CreaSearchCellsDetalle($id,'',''); 
                                    $Grid->CreaSearchCellsDetalle($id,'',"<b>".$Solicitud['FACULTAD']."</b>",5);
                                    $Grid->CreaSearchRowsDetalle ($id,"black",0);
                                    $FacQuiebres=$Solicitud['IDFAC'];
                                }   
                                $Grid->CreaSearchCellsDetalle($id,'',$id); 
                                $Grid->CreaSearchCellsDetalle($id,'',$Solicitud['FEUSCREA']->format('d/m/Y'));
                        
                                $INFORMACION = "<B>".$DateControl->getDayName($Solicitud['DIASEMANA'])."</B><BR>".$Solicitud['FECHA']->format('d/m/Y');
                                $Grid->CreaSearchCellsDetalle($id,'',$INFORMACION);
                        
                                $INFORMACION = "<B>".$Solicitud['DOCENTE']."</B><BR>".$Solicitud['COHORTE']." ".$Solicitud['CICLO']."<BR>";
                                $INFORMACION.= utf8_encode($Solicitud["DESCRIPCION"])."<BR>".utf8_encode($Solicitud["MATERIA"])."<BR>".$Solicitud["PARALELO"]; 
                                $Grid->CreaSearchCellsDetalle($id,'',$INFORMACION);
                                $Grid->CreaSearchCellsDetalle($id,'', utf8_encode($Solicitud["OBSERVACION"]));
                                $ESTADO = $Solicitud['IDESTADO'];
                                if ($ESTADO==5)
                                    $MARK = "<b>X</b>"; 
                                else if ($ESTADO==1 || $ESTADO==9)
                                    $MARK = "";
                                else
                                    $MARK = "<b>✔</b>";
                                $Grid->CreaSearchCellsDetalle($id,'',$MARK,0,1); 
                                $Grid->CreaSearchRowsDetalle ($id,"black",0);
                        }
                    }
                    return $Grid->CreaSearchTableDetalle(0,$Datos[1]);
                }
                
                function CrearPermiso()
                {       $Col1="20%"; $Col2="80%"; 
                        
                        $HTML = '<table border="0" class="Form-Frame" cellpadding="4">';
                        $HTML.= '       <tr height="20px">';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col1.'">';
                        $HTML.= '               <p class="p-txt-label">Estado</p>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <select class="sel-input s07" id="estado" name="estado">';
                        $HTML.= '                       <option value="4">APROBADO</option>';
                        $HTML.= '                       <option value="5">NEGADO</option>';
                        $HTML.= '               </select>';    
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="20px">';
                        $HTML.= '           <td class="Form-Label-Center" valign="top" style="padding-top:5px;width:'.$Col1.'">';
                        $HTML.= '               <p class="p-txt-label">Motivo</p>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <textarea class="txt-upper" style="width: 97%; height: 50px; resize: none;" id="motivo" name="motivo" maxlength="250"></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>'; 
                        $HTML.= '      <tr height="40px">';
                        $HTML.= '          <td class="Form-Label-Center" colspan="2">';
                        $HTML.= '              <input type="button" id="btaplica" class="p-txt-label" style="border: 1px solid #aaa; width: 56px; height: 25px" value="Aceptar" onclick=" return ApruebaPermisoAsistencia(motivo,estado); ">';
                        $HTML.= '          </td>';
                        $HTML.= '      </tr>';
                        $HTML.= '</table>';
                        return $HTML;
                }
                

        }
?>

