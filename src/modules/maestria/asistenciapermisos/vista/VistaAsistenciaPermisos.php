<?php   require_once('src/modules/maestria/asistenciapermisos/controlador/ControlAsistenciaPermisos.php');
        $ControlAsistenciaPermisos = new ControlAsistenciaPermisos();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlAsistenciaPermisos,'GuardaAsistenciaPermisos'));
        $xajax->register(XAJAX_FUNCTION,array('jModal', $ControlAsistenciaPermisos,'CargaModal'));
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                var Permiso;
                
                function SearchGetCell(Celda)
                {       if (Celda.innerText=="")
                        {   var str = Celda.id;
                            Permiso = str.split("_")[1];
                            xajax_jModal();  
                        }
                        else
                        alert("Este registro ya fue procesado.");
                        
                        return false;
                }
                
                function ApruebaPermisoAsistencia(Texto,Opcion)
                {       Texto.value = TrimElement(Texto.value);
                        if (Texto.value!="")
                        {   var mensaje = confirm((Opcion.value==4 ? "Se dispone a la aprobación del registro de asistencia. ¿Desea continuar?" : "Se dispone a negar el registro de asistencia. ¿Desea continuar?"));
                            if (mensaje)
                            {   var Params = [Permiso,Opcion.value,Texto.value];
                                xajax_Guarda(Params);
                            }
                            cerrar();
                        }
                        else
                        alert("Debe ingresar una observación.");    
                        return false;
                }
                
        </script>
        </head>
        <body>
            <div class="FormContainer" style="width:850px; text-align:center;">  
                <div id="<?php echo 'Grid'; ?>" class="FormSectionGrid">          
                <?php $ControlAsistenciaPermisos->CargaAsistenciaPermisosSolicitudes();  ?>
                </div>    
            </div>
        </body>
        </html>
