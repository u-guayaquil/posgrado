<?php   require_once("src/rules/maestria/servicio/ServicioAsistenciaPermisos.php");
        require_once("src/modules/maestria/asistenciapermisos/render/RenderAsistenciaPermisos.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");
          
        class ControlAsistenciaPermisos
        {       private  $ServicioAsistenciaPermisos;
                private  $RenderAsistenciaPermisos;
                private $ServicioUsuarioFiltro;
                
                function __construct()
                {       $this->ServicioAsistenciaPermisos = new ServicioAsistenciaPermisos();
                        $this->RenderAsistenciaPermisos = new RenderAsistenciaPermisos();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                }

                private function FilterOfAccess()
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                
                        if ($Modulo->Idrol==3)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            return array('facultad'=>$Factad);
                        }
                        else if ($Modulo->Idrol==2)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarDocenteFiltroByCedula($Modulo->Cedula);
                            return array('facultad'=>$Factad);
                        }
                        else
                        return array('facultad'=>array('00')); //No permiso a Estudiante
                }
                
                function CargaAsistenciaPermisosSolicitudes()
                {       $Datos = $this->ServicioAsistenciaPermisos->BuscarSolicitudes($this->FilterOfAccess());
                        echo $this->RenderAsistenciaPermisos->CreaSearchGridAsistenciaPermisos($Datos);
                }         
                
                function CargaModal()
                {       $xAjax = new xajaxResponse();
                        $jsonModal['Modal'] = "ModalEjecucion"; 
                        $jsonModal['Title'] = "Permiso de Ejecución";
                        $jsonModal['Carga'] = $this->RenderAsistenciaPermisos->CrearPermiso();
                        $jsonModal['Ancho'] = "350";
                        $xAjax->call("CreaModal", json_encode($jsonModal));
                        return $xAjax; 
                }
                
                function GuardaAsistenciaPermisos($Form)
                {       $xAjax = new xajaxResponse();
                        $Respta = $this->ServicioAsistenciaPermisos->GuardaAsistenciaPermisos($Form);
                        if ($Respta[0])
                            $xAjax->Assign("TD_".$Form[0]."_005", "innerHTML",($Form[1]==4 ? "<b>✔</b>":"<b>X</b>"));
                        else
                        $xAjax->alert($Respta[1]);
                        return $xAjax;
                }
        }       
?>

