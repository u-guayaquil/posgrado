var accion = 0;
var select = 0;
var activo = 1;

function ButtonClick( Operacion)
{       if (Operacion=="addMod")
        {   accion = 1;
            BarButtonState(Operacion);
            ElementStatus("email:direccion:telefono","idExterno");
            BarButtonStateDisabled("btExterno");
            activo = ElementGetValue("estado");
            if(ElementGetValue("estado")==0) 
                ElementStatus( "estado", "");
        }
        else if (Operacion=="addSav")
        {    if (ElementValidateBeforeSave("email:direccion:telefono"))
             {   BarButtonState("Inactive");
                 var Forma = PrepareElements("idExterno:nombre:apellido:email:telefono:direccion:estado:idgestion");
                 xajax_Guarda(accion,JSON.stringify({Forma}));
             }
             else
             SAlert("warning", "¡Campos vacios!", "Falta ingresar datos como email, telefono o dirección.");
        }
        else if (Operacion=="addCan")
        {    if (accion == 1)
             {   BarButtonState("Active");
                 ElementSetValue("estado",activo);
             }
             else
             {   BarButtonState("Inactive");
                 ElementSetValue("estado",1);
                 ElementClear("id:idExterno:nombre:apellido:email:telefono:direccion");
             }   
             ElementStatus("idExterno","nombre:apellido:email:telefono:direccion:estado");
             BarButtonStateEnabled("btExterno");
             return false;
        } 
        else if (Operacion=="addDel")
        {   if (ElementGetValue("estado")==1)
            {   Swal.fire({
                    title: '¡Desactivar registro!',
                    text: "¿Esta seguro que desea desactivar el registro?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro!'
                }).then(function(result){
                    if (result.value) {
                        xajax_Elimina(ElementGetValue("idExterno"));
                    }
                });  
            } 
            else if (ElementGetValue("estado")==2)
                SAlert("error", "¡Error!", "Usted no puede inactivar un registro que esta por ser confirmado.");
            else
                SAlert("error", "¡Registro inactivo!", "El registro se encuentra inactivo");
        }
        else if (Operacion=="btExterno")
        {    xajax_CargaModal(Operacion,"");   
        }
        /*else if (Operacion=="btAplica")
        {   IDDOC = ElementGetValue("id");
            if (IDDOC!="" && IDDOC!=0) 
                xajax_CargaModal(Operacion,IDDOC);
            else
                SAlert("error", "¡Externo inactivo!", "Debe seleccionar un externo activo para posgrado.");
        }*/
}        
        
function SearchByElement(Elemento)
{       Elemento.value = TrimElement(Elemento.value);
        if (Elemento.id=="idExterno")
        {   if (Elemento.value.length != 0)
            {   ContentFlag = document.getElementById("nombre");
                if (ContentFlag.value.length==0)
                {   if (BarButtonState("Inactive"))
                    xajax_BuscarByID(Elemento.value);
                }
            }
            else
            BarButtonState("Inactive");
        }
        else if (Elemento.id=="FindExternoTextBx")
        {   xajax_BuscaModalByTX("btExterno",Elemento.value);
        }
        return false;
}        

function SearchExternoGetData(DatosGrid)
{   ElementSetValue("idExterno",DatosGrid.cells[0].innerHTML);
    ElementSetValue("apellido",DatosGrid.cells[2].innerHTML);
    ElementSetValue("nombre",DatosGrid.cells[3].innerHTML);
    ElementSetValue("direccion",DatosGrid.cells[4].innerHTML);
    ElementSetValue("email",DatosGrid.cells[5].innerHTML);
    ElementSetValue("telefono",DatosGrid.cells[6].innerHTML);
    ElementSetValue("estado",DatosGrid.cells[7].innerHTML);
    ElementSetValue("id",DatosGrid.cells[8].innerHTML);
    ElementSetValue("idgestion",DatosGrid.cells[9].innerHTML);
    
    if (DatosGrid.cells[1].innerHTML=="S")
    {   accion = 1;
        BarButtonState("Active");
    }    
    else
    AgregarRegistro("addMod");
    cerrar();
}

function AgregarRegistro(Opcion)
{       accion = 0;
        Swal.fire({
            title: '¡El usuario externo actual no está activo para posgrado!',
            text: "¿Desea activarlo?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, estoy seguro!'
        }).then(function(result){
            if (result.value) {
                if (ElementValidateBeforeSave("email:telefono:direccion"))
                    {   BarButtonState("Inactive");
                        var Forma = PrepareElements("idExterno:nombre:apellido:email:telefono:direccion:estado");
                        xajax_Guarda(accion,JSON.stringify({Forma})); 
                        return false;
                    }
                    else
                    {   SAlert("warning", "¡Campos vacios!", "Falta ingresar informacion.");   }
            }
        });      
        BarButtonState(Opcion);
        ElementStatus("email:direccion:telefono","idExterno");
        BarButtonStateDisabled("btExterno");
        return false;
}

function CrearExternoPorConfirmar(Estado)
{       if (Estado)
        {   /*if (confirm("El externo no tiene un contrato en Talento Humano. ¿Desea crearlo?"))
            {   accion = 0;
                BarButtonState("addMod");
                ElementStatus("nombre:apellido:email:direccion:telefono","idExterno");
                if(perfil == 0)
                {   ElementStatus("idgestion", ""); }
                else{   ElementStatus("", "idgestion"); }
                BarButtonStateDisabled("btExterno");
                ElementSetValue("estado",2);
                ElementSetValue("idgestion",0);
            }*/
            Swal.fire({
                title: '¡Externo sin contraro!',
                text: "Este usuario externo no consta en los registros del sistema. ¿Desea crearlo?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, deseo crearlo!'
            }).then(function(result){
                if (result.value) {
                    accion = 0;
                    BarButtonState("addMod");
                    ElementStatus("nombre:apellido:email:direccion:telefono","idExterno");
                    BarButtonStateDisabled("btExterno");
                    ElementSetValue("estado",1);
                }
            });
        }
        else
        SAlert("error","¡Cédula incorrecta!","Ingrese una cedula correcta.");    
        return false;
}

function XAJAXResponse(Mensaje,Datos)
{   if (Mensaje==="CMD_SAV")
    {   SAlert("success", "¡Excelente!", Datos);
    }
    else if(Mensaje==='CMD_DEL')
    {   SAlert("warning", "¡Cuidado!", Datos);    
    }
    else if(Mensaje==='CMD_WRM')
    {   SAlert("warning", "¡Cuidado!", Datos);
    }
    else if(Mensaje==='CMD_ERR')
    {   SAlert("error", "¡Error!", Datos);
    }
}