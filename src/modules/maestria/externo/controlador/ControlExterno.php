<?php
        require_once("src/rules/maestria/servicio/ServicioExterno.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/modules/maestria/externo/render/RenderExterno.php");
        require_once("src/rules/general/servicio/ServicioFacultad.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");
         
        class ControlExterno
        {       private $ServicioExterno;
                private $RenderExterno;
                private $ServicioFacultad;
                private $ServicioUsuarioFiltro;

                function __construct()
                {       $this->ServicioExterno = new ServicioExterno();
                        $this->RenderExterno = new RenderExterno();
                        $this->ServicioFacultad = new ServicioFacultad();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro(); 
                }

               function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderExterno->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {        echo $this->RenderExterno->CreaMantenimiento();
                }

                function BuscarByCedula($Cedula)
                {       $xAjax = new xajaxResponse();
                        $Datos= $this->ServicioExterno->BuscarByCedula($Cedula);
                        if ($Datos[0])
                            $xAjax = $this->RenderExterno->MuestraDatos($xAjax,$Datos[1]); 
                        else
                        {   $Utils = new Utils();
                            $Rspt = $Utils->validaIndentificacion(2,$Cedula);
                            $xAjax->Call("CrearExternoPorConfirmar",$Rspt[0]);
                        }
                        return $xAjax;
                }
               
                function Guarda($Op,$Form)
                {       $xAjax = new xajaxResponse();
                        $Externo  = json_decode($Form)->Forma;
                        $sEvento = $this->ServicioExterno->GuardaDB($Op,$Externo);
                        if ($sEvento[0])
                        {   if ($Op==0) $xAjax->Assign("id","value",$sEvento[1]);
                            $xAjax->Call("BarButtonState","Active");   
                            $xAjax->Call("ElementStatus","idExterno","nombre:apellido:email:direccion:telefono:estado:idgestion");
                            $xAjax->Call("BarButtonStateEnabled","btExterno");
                            $xAjax->Call("XAJAXResponse","CMD_SAV","Los datos se guardaron correctamente. ".$sEvento[2]);   
                            //$xAjax->alert("Los datos se guardaron correctamente. ".$sEvento[2]);
                        }
                        return $xAjax;
                }
                
                /*function GuardaFacultad($DocID,$FacID,$Mark)
                {       $xAjax = new xajaxResponse();
                        $Resp = $this->ServicioExterno->GuardaFacultad($DocID,$FacID);
                        if (!$Resp[0])
                            $xAjax->Call("XAJAXResponse", "CMD_WRM", $Resp[1]);  
                            //$xAjax->alert($Resp[1]);
                        else
                        $xAjax->Assign("TD_".$FacID."_003","innerHTML",($Mark=="✔" ? "":"✔"));
                        return $xAjax;
                }*/
               
                function Elimina($Cedula)
                {       $xAjax = new xajaxResponse();
                        $Datos = $this->ServicioExterno->Desactiva($Cedula);
                        if ($Datos[0])
                        {   $xAjax->assign("estado", "value", $Datos[1]);
                            $xAjax->Call("XAJAXResponse", "CMD_SAV", 'Datos guardados correctamente');
                        }
                        else
                            $xAjax->Call("XAJAXResponse", "CMD_ERR", $Datos[1]);
                            //$xAjax->alert($Datos[1]); 
                        return $xAjax; 
                }

                private function FilterOfAccess()
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        if ($Modulo->Idrol==3)
                            return $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                        else
                            return array('00'); //No permiso a Estudiante y Externo
                }                    
                
                function CargaModal($Operacion,$IdRef)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion=="btExterno")
                        {   $prepareDQL = array('texto' =>'');
                            $ArDatos = $this->ServicioExterno->BuscarExternoByTX($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Externo";
                            $jsonModal['Carga'] = $this->RenderExterno->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderExterno->GetGridTableWidth();
                        }
                        /*elseif ($Operacion=="btAplica")
                        {   $prepareDQL = array('externo' =>$IdRef,'facultad'=>$this->FilterOfAccess());
                            $ArDatos = $this->ServicioFacultad->BuscarFacultadByExterno($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Seleccione la Facultad";
                            $jsonModal['Carga'] = $this->RenderExterno->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderExterno->GetGridTableWidth();
                        }*/
                        $xAjax->call("CreaModal", json_encode($jsonModal));
                        return $xAjax; 
                }
                
                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btExterno")    
                        {   $prepareDQL = array('texto'=>strtoupper(trim($Texto)));
                            $ArDatos = $this->ServicioExterno->BuscarExternoByTX($prepareDQL);
                            return $this->RenderExterno->MuestraModalGrid($xAjax,$Operacion,$ArDatos);
                        }                
                }
                
                function ConsultaPerfilByRol($Idusrol)
                {   $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                    return $ServicioPerfilOpcion->ConsultarPerfilByRol($Idusrol);
                }
         }

?>

