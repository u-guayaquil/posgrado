<?php   $Opcion = 'Externo';
        require_once('src/modules/maestria/externo/controlador/ControlExterno.php');
        $ControlExterno = new ControlExterno();
            
        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlExterno,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaFacultad', $ControlExterno,'GuardaFacultad'));
        $xajax->register(XAJAX_FUNCTION,array('BuscarByID', $ControlExterno,'BuscarByCedula')); 
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlExterno,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlExterno,'CargaModal'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlExterno,'ConsultaGridByTX'));
        
        $xajax->processRequest();
        
        $Modulo = json_decode(Session::getValue('Sesion'));
        $Perfil = $ControlExterno->ConsultaPerfilByRol($Modulo->Idusrol);
        if($Perfil)
        {   $PerfilUs = $Perfil->IDPERFIL;
        }
        else{   $PerfilUs = 1;  }
        
?>

         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php $xajax->printJavascript(); 
                require_once('src/utils/links.php'); ?>
            
            <script type="text/javascript" src="src/modules/maestria/externo/js/jsExterno.js"></script>
         </head>
         <body>      
             <div class="FormBasic" style="width:550px">
                <div class="FormSectionMenu">              
                <?php  $ControlExterno->CargaBarButton($_GET['opcion']);
                       echo '<script type="text/javascript">';
                       echo '        BarButtonState("Inactive"); ';
                       echo '</script>';
                ?>
                </div>    
                <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                          <?php  $ControlExterno->CargaMantenimiento();  ?>
                     </form>
                </div>
                
             </div>

             <script type="text/javascript">
                var perfil= <?php print_r($PerfilUs);?>;
            </script> 
        </body>
        </html>


