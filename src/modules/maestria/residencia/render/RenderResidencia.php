<?php    
require_once("src/rules/general/servicio/ServicioEstado.php");
require_once("src/libs/clases/BarButton.php");
require_once("src/libs/clases/SearchGrid.php");
require_once('src/libs/clases/ComboBox.php');
require_once('src/libs/clases/SearchInput.php');
	
class RenderResidencia
{   private $SearchGrid;
	private $Maxlen;    
	private $Width;
	
	function __construct()
	{   $this->Maxlen['id'] = 4;        
		$this->Maxlen['descripcion'] = 50;
		$this->Maxlen['fecha'] = 10;  
		$this->SearchGrid = new SearchGrid();
	}

	private function SearchGridConfig()
	{   $Columns['Id']          = array('40px','center','');
		$Columns['H.ID']    = array('0px','left','none'); 
		$Columns['RESIDENCIA']   = array('150px','left','');
		$Columns['HOSPITAL']    = array('150px','left','');
		$Columns['Inicio']     = array('100px','center','');
		$Columns['Fin']    = array('100px','center','');
		$Columns['IdEstado']    = array('0px','center','none');
		$Columns['Estado']      = array('60px','center','');
		return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
	}

	function GetGridTableWidth()
	{        return $this->Width;
	}
		
	function CreaMantenimiento()
	{   $Search = new SearchInput();
		$HTML = '<table border=0 class="Form-Frame" cellpadding="0">';                 
		$HTML.= '       <tr height="30">';
		$HTML.= '           <td class="Form-Label">Maestria</td>';
		$HTML.= '           <td class="Form-Label" colspan="3">';
		$HTML.= '                 <input type="hidden" class="txt-upper t03" id="version" name="version" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
		$HTML.= $Search->TextSch("vmaestria","","")->Enabled(true)->Create("t17");
		$HTML.= '           </td>';
		$HTML.= '       </tr>';
		$HTML.= '       <tr height="30">';
		$HTML.= '           <td class="Form-Label">Cohorte</td>';
		$HTML.= '           <td class="Form-Label" colspan="3">';
		$HTML.= $Search->TextSch("cohorte","","")->Enabled(false)->Create("t10");
		$HTML.= '           </td>';
		$HTML.= '       </tr>';
		$HTML.= '           <td class="Form-Label">Hospital</td>';
		$HTML.= '           <td class="Form-Label" colspan="3">';
		$HTML.= $Search->TextSch("hospital","","")->Enabled(false)->Create("t10");
		$HTML.= '           </td>';
		$HTML.= '       </tr>';
		$HTML.= '       <tr height="30">';
		$HTML.= '             <td class="Form-Label">Id</td>';
		$HTML.= '             <td class="Form-Label">';
		$HTML.= '                <input type="text" class="txt-input t01" id="id" name="id" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\'descripcion\',\'NUM\'); " onBlur="return SearchByElement(this);" disabled/>';
		$HTML.= '             </td>';
		$HTML.= '         </tr>';     
		$HTML.= '             <td class="Form-Label">Residencia</td>';
		$HTML.= '             <td class="Form-Label" colspan="3">';
		$HTML.= '                 <input type="text" class="txt-upper t10" id="descripcion" name="descripcion" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
		$HTML.= '             </td>';
		$HTML.= '         </tr>'; 
		$HTML.= '       <tr height="30">';
		$HTML.= '             <td class="Form-Label">Fecha Inicio</td>';
		$HTML.= '             <td class="Form-Label">';
		$HTML.= '                <input type="text" class="txt-input t05" id="feinicio" name="feinicio" value="" maxlength="'.$this->Maxlen['fecha'].'"  onKeyDown="return Dependencia(event,\'\',\'NUM\'); " readonly disabled/>';
		$HTML.= '             </td>';
		$HTML.= '             <td class="Form-Label">Fecha Fin</td>';
		$HTML.= '             <td class="Form-Label">';
		$HTML.= '                <input type="text" class="txt-input t05" id="fefin" name="fefin" value="" maxlength="'.$this->Maxlen['fecha'].'"  onKeyDown="return Dependencia(event,\'\',\'NUM\'); " readonly disabled/>';
		$HTML.= '             </td>';
		$HTML.= '         </tr>';            
		$HTML.= '         <tr height="30">';
		$HTML.= '             <td class="Form-Label">Estado</td>';
		$HTML.= '             <td class="Form-Label">';
		$HTML.= $this->CreaComboEstadoByArray(array(0,1));
		$HTML.= '             </td>';
		$HTML.= '         </tr>';
		$HTML.= '</table>';
		return $HTML;
	}

	function CreaBarButton($Buttons)
	{   $BarButton = new BarButton();    
		return $BarButton->CreaBarButton($Buttons);
	}
	
	function CreaSearchGrid($ArDatos)
	{   $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
		$GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
		return $this->SearchGrid->CreaSearchGrid($GrdDataHTML);
	}
	
	private function GridDataHTML($Grid,$ArDatos)
	{   if ($ArDatos[0])
		{   foreach ($ArDatos[1] as $ArDato)
			{       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);                       
					$Grid->CreaSearchCellsDetalle($id,'',$id);     
					$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDHOSPITAL']);   
					$Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['RESIDENCIA'])));  
					$Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['HOSPITAL']))); 
					$feInicio = $ArDato['INICIO'];
					$Grid->CreaSearchCellsDetalle($id,'',$feInicio->format('d/m/Y'));
					$feFin = $ArDato['FIN'];
					$Grid->CreaSearchCellsDetalle($id,'',$feFin->format('d/m/Y'));
					$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
					$Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
					$Grid->CreaSearchRowsDetalle ($id,($ArDato['IDESTADO']==0 ? "red" : ""));
			}
		}    
		return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
	}
		
	function MuestraFormulario($Ajax,$Residencia)
	{   if ($Residencia)
		{   if ($Residencia->ID>=0)
			{   $Ajax->Assign("id","value", str_pad($Residencia->ID,$this->Maxlen['id'],'0',STR_PAD_LEFT));
				$Ajax->Assign("descripcion","value", utf8_encode(trim($Residencia->RESIDENCIA)));
				$Ajax->Assign("idhospital","value", $Residencia->IDHOSPITAL);
				$Ajax->Assign("txhospital","value", $Residencia->HOSPITAL);
				$FeInicioBD = $Residencia->INICIO;
				$Ajax->Assign("feinicio","value", $FeInicioBD->format('Y-m-d'));
				$FeFinBD = $Residencia->FIN;
				$Ajax->Assign("fefin","value", $FeFinBD->format('Y-m-d'));
				$Ajax->Assign("estado","value", $Residencia->IDESTADO);
				$Ajax->call("BarButtonState","Active");
				return $Ajax;
			}    
		}
		return $this->Respuesta($Ajax,"CMD_WRM","No existe una Residencia con el ID ingresado.");
	}
	
	function MuestraGuardado($Ajax,$ArDatos)
	{   if ($ArDatos[0])
		{   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
			$xAjax = $this->MuestraGrid($Ajax,$ArDatos);
			return $this->Respuesta($xAjax,"CMD_SAV","Datos Guardados Correctamente");
		}
		return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);
	}
	
	function MuestraEditado($Ajax,$ArDatos)
	{   if ($ArDatos[0])
		{   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
			$xAjax = $this->MuestraRowGrid($Ajax, $ArDatos[1][0]);
			return $this->Respuesta($xAjax,"CMD_SAV","Datos Guardados Correctamente");
		}    
		return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);            
	}

	function MuestraGrid($Ajax,$ArDatos)
	{   $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
		$GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
		$Ajax->Assign($this->SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
		return $Ajax;
	}

	function MuestraRowGrid($Ajax,$ArDato)
	{   $RowId = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
		$Fila = $this->SearchGrid->GetRow($RowId,$ArDato['IDESTADO']);
		$Ajax->Assign($Fila['Name'],"style",$Fila['Color']);      
		$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",utf8_encode(trim($ArDato['RESIDENCIA'])));
		$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",utf8_encode(trim($ArDato['HOSPITAL'])));
		$feIncio = $ArDato['INICIO'];
		$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$feIncio->format('d/m/Y'));
		$feFin = $ArDato['FIN'];
		$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$feFin->format('d/m/Y'));
		$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$ArDato['IDESTADO']);
		$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO")); 
		return $Ajax;
	}                                
	
	function MuestraEliminado($Ajax,$JsDatos)
	{   $RowId = str_pad($JsDatos->id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
		$Fila = $this->SearchGrid->GetRow($RowId,$JsDatos->estado);
		$Ajax->Assign($Fila['Name'],"style",$Fila['Color']);       
		$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$JsDatos->estado); 
		$Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML","INACTIVO"); 
		return $this->Respuesta($Ajax,"CMD_DEL",0);
	}

	private function Respuesta($Ajax,$Tipo,$Data)
	{   $jsFuncion = "XAJAXResponse";
		$Ajax->call($jsFuncion,$Tipo,$Data);
		return $Ajax;
	}
	
	private function CreaComboEstadoByArray($IdArray)
	{   $Select = new ComboBox();
		$ServicioEstado = new ServicioEstado();
		$Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
		return $Select->Combo("estado",$Datos)->Enabled(false)->Fields("id","descripcion")->Create("s05");
	}   
	
	/*** MODAL ***/
	
	private function SearchGridModalConfig($Operacion)
	{   	if ($Operacion=="btvmaestria")
			{   $Columns['IdFacultad']   = array('0px','center','none');
				$Columns['Facultad']   = array('250px','left','');
				$Columns['Maestria']    = array('300px','left','');
				$Columns['IdVersion']   = array('0px','center','none');
				$Columns['Version']     = array('60px','center','');
			}
			else if ($Operacion=="btcohorte")
			{   $Columns['Id']   = array('0px','center','none');
				$Columns['Cohorte']     = array('120px','left','');
				$Columns['Inicio']      = array('70px','left','');
				$Columns['Fin']         = array('70px','left','');
				$Columns['IdEstado']    = array('0px','left','none');
				$Columns['Estado']      = array('70px','center','');
			}
			else if ($Operacion=="bthospital")
			{   $Columns['ID']=array('30px','center','center');
				$Columns['Hospital']=array('200px','left','');
				$Columns['Siglas']=array('70px','center','');
				$Columns['Creado']=array('70px','center','');
				$Columns['IdEstado']    = array('0px','left','none');
				$Columns['Estado']      = array('70px','center','');
			}
			return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
	}

	function CreaModalGrid($Operacion,$ArDatos)
	{   $SearchGrid = new SearchGrid($Operacion);
		$SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
		$GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$ArDatos,$Operacion);
		$ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
		$this->Width = $SearchGrid->SearchTableWidth();
		return $ObModalGrid;
	}
		
	function MuestraModalGrid($Ajax,$Operacion,$Datos)
	{   $SearchGrid = new SearchGrid($Operacion);
		$SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
		$GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
		$Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
		return $Ajax;
	}        

	private function GridDataHTMLModal($Grid,$ArDatos,$Operacion)
	{   if ($ArDatos[0])
		{   if ($Operacion=="btvmaestria")
			{   foreach ($ArDatos[1] as $ArDato)
				{   $id = str_pad($ArDato['IDFACULTAD'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
					$Grid->CreaSearchCellsDetalle($id,'',$id);
					$Grid->CreaSearchCellsDetalle($id,'',$ArDato['FACULTAD']);
					$Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MAESTRIA'])));
					$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDVERSION']);
					$Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['VERSION'])));
					$Grid->CreaSearchRowsDetalle ($id,"black");
				}
			}
			if ($Operacion=="btcohorte")
			{   foreach ($ArDatos[1] as $ArDato)
				{   $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
					$Grid->CreaSearchCellsDetalle($id,'',$id);
					$Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['COHORTE'])));
					$InicioBD = $ArDato['FEINICIO'];
					$Grid->CreaSearchCellsDetalle($id,'',$InicioBD->format('d/m/Y'));
					$FinBD = $ArDato['FEFIN'];
					$Grid->CreaSearchCellsDetalle($id,'',$FinBD->format('d/m/Y'));
					$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
					$Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO")); 
					$Grid->CreaSearchRowsDetalle ($id,"black");
				}
			}
			elseif ($Operacion=="bthospital")
			{   foreach ($ArDatos[1] as $ArDato)
				{   $id = str_pad($ArDato['ID'],HOSPITAL,'0',STR_PAD_LEFT);
					$Grid->CreaSearchCellsDetalle($id,'',$id);
					$Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['HOSPITAL']));
					$Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['SIGLAS']));
					$Creado = $ArDato['FECREACION'];
					$Grid->CreaSearchCellsDetalle($id,'',$Creado->format('d/m/Y'));
					$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
					$Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO")); 
					$Grid->CreaSearchRowsDetalle ($id,"black");
				}
			}
		}    
		return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
	}
}
?>

