<?php
require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
require_once("src/rules/maestria/servicio/ServicioResidencia.php");
require_once("src/modules/maestria/Residencia/render/RenderResidencia.php");

class ControlResidencia
{   private  $ServicioResidencia;
	private  $RenderResidencia;

	function __construct()
	{   $this->ServicioResidencia = new ServicioResidencia();
		$this->RenderResidencia = new RenderResidencia();
	}

	function CargaBarButton($Opcion)
	{   $ServicioPerfilOpcion = new ServicioPerfilOpcion();
		$Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
		if ($Datos[0])
		echo $this->RenderResidencia->CreaBarButton($Datos[1]);
	}
	
	function CargaMantenimiento()
	{	echo $this->RenderResidencia->CreaMantenimiento();
	}

	function CargaSearchGrid()
	{   $prepareDQL = array('limite' => 50,'inicio' => 0,'txt' => 'NULL', 'idCohorte' => '0');
		$Datos = $this->ServicioResidencia->BuscarResidenciaByDescripcion($prepareDQL);
		echo $this->RenderResidencia->CreaSearchGrid($Datos);
	}         
	
	function CargaModalGrid($Operacion,$IdRef)
	{  	$ajaxResp = new xajaxResponse();
		if ($Operacion==="btvmaestria")
		{   $Params = array('txt'=>'');
			$ArDatos = $this->ServicioResidencia->BuscarMaestriaByDescripcion($Params);
			$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
			$jsonModal['Title'] = "Busca Maestria";
			$jsonModal['Carga'] = $this->RenderResidencia->CreaModalGrid($Operacion,$ArDatos);
			$jsonModal['Ancho'] = "640";
		}
		if ($Operacion==="btcohorte")
		{   $Params = array('txt'=>'','mtr'=>$IdRef);
			$ArDatos = $this->ServicioResidencia->BuscarCohorteByDescripcion($Params);
			$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
			$jsonModal['Title'] = "Busca Cohorte";
			$jsonModal['Carga'] = $this->RenderResidencia->CreaModalGrid($Operacion,$ArDatos);
			$jsonModal['Ancho'] = "360";
		}
		if ($Operacion==="bthospital")
		{   $Params = array('txt'=>'');
			$ArDatos = $this->ServicioResidencia->BuscarHospitalByDescripcion($Params);
			$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
			$jsonModal['Title'] = "Busca Cohorte";
			$jsonModal['Carga'] = $this->RenderResidencia->CreaModalGrid($Operacion,$ArDatos);
			$jsonModal['Ancho'] = "472";
		}
		$ajaxResp->call("CreaModal", json_encode($jsonModal));
		return $ajaxResp; 
	}
	
	function ConsultaModalGridByTX($Operacion,$Texto = "")
	{   $ajaxResp = new xajaxResponse();
		if ($Operacion==="btvmaestria")    
		{   $texto = trim($Texto);
			$prepareDQL = array('limite' => 50,'inicio' => 0,'txt' => strtoupper($texto), 'tipo' => array(0,1));
			$Datos = $this->ServicioResidencia->BuscarMaestriaByDescripcion($prepareDQL);
			return $this->RenderResidencia->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
		}    
		if ($Operacion==="btcohorte")    
		{   $Texto[0] = strtoupper(trim($Texto[0]));
			$prepareDQL = array('limite' => 50,'inicio' => 0,'txt'=>$Texto[0],'mtr'=>$Texto[1], 'tipo' => array(0,1));
			$Datos = $this->ServicioResidencia->BuscarCohorteByDescripcion($prepareDQL);
			return $this->RenderResidencia->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
		}    
		if ($Operacion==="bthospital")    
		{   $texto = trim($Texto);
			$prepareDQL = array('limite' => 50,'inicio' => 0,'txt'=>strtoupper($texto),'tipo' => array(0,1));
			$Datos = $this->ServicioResidencia->BuscarHospitalByDescripcion($prepareDQL);
			return $this->RenderResidencia->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
		}    
	}
	
	function MostrarResidenciabyCohorte($Form)
	{   $ajaxRespon = new xajaxResponse();
		$JsDatos = json_decode($Form)->Forma;
		$idCohorte = $JsDatos->idcohorte;
		$ArDatos = $this->ServicioResidencia->BuscarResidenciaByCohorte($idCohorte);
		return $this->RenderResidencia->MuestraGrid($ajaxRespon,$ArDatos);
	}

	function ConsultaByID($Form)
	{   $ajaxRespon = new xajaxResponse(); 
		$JsDatos = json_decode($Form)->Forma;
		$prepareDQL = array('id' => ($JsDatos -> id), 'idCohorte' => $JsDatos->idcohorte);
		$ArDatos = $this->ServicioResidencia->BuscarResidenciaByID($prepareDQL);
		return $this->RenderResidencia->MuestraFormulario($ajaxRespon,$ArDatos);
	}
	
	function ConsultaByTX($Form)
	{   $ajaxRespon = new xajaxResponse(); 
		$JsDatos = json_decode($Form)->Forma;
		$prepareDQL = array('txt' => strtoupper(trim($JsDatos -> FindTextBx)), 'idCohorte' => $JsDatos->idcohorte );
		$ArDatos = $this->ServicioResidencia->BuscarResidenciaByDescripcion($prepareDQL);
		return $this->RenderResidencia->MuestraGrid($ajaxRespon,$ArDatos);
	}

	function Guarda($Form)
	{   $ajaxRespon = new xajaxResponse();
		$JsDatos = json_decode($Form)->Forma;
		$funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");
		$id = $this->ServicioResidencia->GuardaDB($JsDatos);
		$ArDatos = $this->ServicioResidencia->BuscarResidenciaByIDProcesado($id);
		return $this->RenderResidencia->{$funcion}($ajaxRespon,$ArDatos);
	}

	function Elimina($Form)
	{   $ajaxRespon = new xajaxResponse();
		$JsData = json_decode($Form)->Forma;
		$JsData->estado = $this->ServicioResidencia->DesactivaResidencia($JsData->id);
		return $this->RenderResidencia->MuestraEliminado($ajaxRespon,$JsData);
	}
}       
?>