﻿var COLINI = 5;
var NTETAP = "";
var CAMBIO = 0;

function ButtonClick(Operacion)
{       if (Operacion=="MinSav")
        {   if (CAMBIO==1)
            {   var tabla = document.getElementById("DetSch");
                if (tabla.rows.length>0)
                {   CAMBIO=0;
                    ElementStatus("","MinSav:MinPrn");
                    xajax_GuardaDB(PreparaDatosGrid(tabla.rows.length));
                }    
            }
            else
            SAlert("warning","¡Cuidado!","Los datos estan actualizados.");
        }
        else if (Operacion=="MinPrn")
        {   
            if (ElementGetValue("TipoNota")!=0)
            {   //if (confirm("Se dispone a cerrar el acta de calificaciones. ¿Desea continuar?"))
                {
                    ElementSetValue("TipoNota",0);
                    ElementStatus("","MinSav:MinPrn");
                    var DOCTE = ElementGetValue("iddocente");
                    var CICLO = ElementGetValue("idciclo");   
                    var MAEST = ElementGetValue("idmaestria");
                    var MATER = ElementGetValue("idmateria");
                    var GRUPO = ElementGetValue("idgrupo");
                    var POLEV = ElementGetValue("idpolev");
                    var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP];  
                    NTETAP="";
                    xajax_ActaCalificacion(Params);
                }    
            }    
        }
        else if (Operacion=="btdocente")
        {    xajax_CargaModal(Operacion,0);
        }
        else if (Operacion=="btmaestria")
        {    var IDDOC=ElementGetValue("iddocente");
             if (IDDOC!="")
                xajax_CargaModal(Operacion,IDDOC);
             else
                SAlert("warning","¡Cuidado!","Debe seleccionar un Docente.");
        }
        else if (Operacion=="btmateria")
        {    var IDMST=ElementGetValue("idmaestria");
             if (IDMST!="")
             {   var IDDOC=ElementGetValue("iddocente");
                 var IDCLC=ElementGetValue("idciclo");
                 var Params = [IDDOC,IDCLC,IDMST];
                 xajax_CargaModal(Operacion,Params);
             }
             else
             SAlert("warning","¡Cuidado!","Debe seleccionar una Maestria.");
        }
        return false;
}

function PreparaDatosGrid(Estudiantes)
{       var Forma = [];
        for (t=1;t<=Estudiantes;t++)
        {   var Cell = "TD_"+StrPadCeroNumber("00",t);
            if (t==1)
            {   var fila = document.getElementById("TR_"+StrPadCeroNumber("00",t)).cells;
                var tope = fila.length-1;
            }
            var Nota = document.getElementById(Cell+"_000").innerHTML;
            var Esmt = document.getElementById(Cell+"_001").innerHTML;
            var Filas = 'NO='+t+'|ID='+Nota+'|ESMA='+Esmt+'|JSON=';
            var Sjson = '';    
            for (s=COLINI;s<=tope;s++)
            {   var Evl = document.getElementById(Cell+"_"+StrPadCeroNumber("000",s)).innerHTML;  
                var Str = $(Evl).prop("name");
                var Exc = Str.substring(0,1);
                if (Exc=="N")
                {   var Str = $(Evl).prop("id");
                    var Txt = Str.substring(2,Str.length);
                    var Vlr = ElementGetValue(Str);
                    Sjson = Sjson + (Sjson!='' ? '","': '{"')+Txt+'":"'+ (Vlr!='' ? Vlr:0);
                }
            }
            Sjson = Sjson + '"}';
            Forma.push(PrepareFreeElementsNotas(Filas+Sjson));
        }
        return JSON.stringify({Forma});            
}

function SearchDocenteGetData(DatosGrid)
{       var IDDOC = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("iddocente")!=IDDOC)
        {   NTETAP = "";
            FillOptionCbx("TipoNota",[[0,"SELECCIONE..."]]);
            ElementStatus("","MinSav:MinPrn");
            ElementSetValue("TipoNota",0);
            ElementSetValue("WrkGrd","");
            ElementClear("idmaestria:txmaestria:txcohorte:idciclo:idmateria:txmateria:idgrupo:txgrupo:idpolev:idmaesv");
            document.getElementById("iddocente").value = IDDOC;
            document.getElementById("txdocente").value = DatosGrid.cells[2].innerHTML;
            cerrar();
        }    
        else
        cerrar();
        return false;
}

function SearchMaestriaGetData(DatosGrid)
{       var IDMST = DatosGrid.cells[0].innerHTML;
        var IDCLC = DatosGrid.cells[2].innerHTML;
        if (ElementGetValue("idmaestria")!=IDMST || ElementGetValue("idciclo")!=IDCLC)
        {   NTETAP = "";
            FillOptionCbx("TipoNota",[[0,"SELECCIONE..."]]);
            ElementStatus("","MinSav:MinPrn");
            ElementSetValue("WrkGrd","");
            ElementClear("idmateria:txmateria:idgrupo:txgrupo");
            document.getElementById("idmaestria").value = IDMST;
            document.getElementById("idciclo").value = DatosGrid.cells[2].innerHTML;
            document.getElementById("idpolev").value = DatosGrid.cells[4].innerHTML;
            document.getElementById("idmaesv").value = DatosGrid.cells[5].innerHTML;
            document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("txcohorte").value = DatosGrid.cells[3].innerHTML;
            cerrar();
        }
        else
        cerrar();    
        return false;
}  

function SearchMateriaGetData(DatosGrid)
{       var IDGRP = DatosGrid.cells[2].innerHTML;
        if (ElementGetValue("idgrupo")!=IDGRP)
        {   NTETAP = "";
            ElementStatus("","MinSav:MinPrn");
            ElementSetValue("TipoNota",0);
            ElementSetValue("WrkGrd","");
            document.getElementById("idmateria").value = DatosGrid.cells[0].innerHTML;;
            document.getElementById("txmateria").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idgrupo").value = IDGRP;
            document.getElementById("txgrupo").value = DatosGrid.cells[3].innerHTML;
            xajax_BuscaParcialesByID(ElementGetValue("idpolev"));
            cerrar();
        }    
        else    
        cerrar();    
        return false;
}

function SearchTipoNotaGetData(DatosCombo)
{       if (DatosCombo.value==0)
        {   NTETAP = "";
            ElementSetValue("WrkGrd","");
            ElementStatus("","MinSav:MinPrn");
        }
        else
        {   var ETAPA = ElementGetText(DatosCombo.id);                     
            if (NTETAP!=ETAPA)
            {   NTETAP = ETAPA;
                ElementStatus("MinSav:MinPrn","");
                SearchNotasGetDataGrid();
            }
        }    
}  

function SearchNotasGetDataGrid()
{       var DOCTE = ElementGetValue("iddocente");
        var MAESV = ElementGetValue("idmaesv");
        var CICLO = ElementGetValue("idciclo");   
        var MAEST = ElementGetValue("idmaestria");
        var MATER = ElementGetValue("idmateria");
        var GRUPO = ElementGetValue("idgrupo");
        var POLEV = ElementGetValue("idpolev");
        var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP,MAESV];
        xajax_BuscaByID(Params);
}  

function calculaNotas(Cld,Pev,Min,Max)
{       if (Cld.value=="" || Cld.value==".")               
            var Eval = 0; 
        else
            var Eval = parseFloat(Cld.value);
        
        if (Eval>=parseFloat(Min) && Eval<=parseFloat(Max))
        {   var str = Cld.id;
            var Row = str.substring(0, 2);
            ContentFlag = document.getElementById(Row+"EST");
            if (ContentFlag.value.length==0)
            {   var Cll = "TD_"+Row;
                var fila = document.getElementById("TR_"+Row).cells;
                var tope = fila.length-1;
                var objs = "";
                for (s=COLINI;s<=tope;s++)
                {   var Evl = document.getElementById(Cll+"_"+StrPadCeroNumber("000",s)).innerHTML;  
                    objs = objs+(objs!="" ? ":":"")+$(Evl).prop("id");
                }    
                CAMBIO=1;
                var Forma = PrepareElements(objs);
                var Maesv = ElementGetValue("idmaesv");
                var Mater = ElementGetValue("idmateria");
                var Param = [Pev,NTETAP,Row,Maesv,Mater];
                xajax_CalculaNotas(Param,JSON.stringify({Forma}));
            }    
            return false;
        }   
        Cld.value="";    
}

function SearchByElement(Elemento)
{       if (Elemento.id=="FindDocenteTextBx")
        {    xajax_BuscaModalByTX("btdocente",Elemento.value);
        }
        else if (Elemento.id=="FindMaestriaTextBx")
        {    var Params = [Elemento.value,ElementGetValue("iddocente")];
             xajax_BuscaModalByTX("btmaestria",Params);
        }
        else if (Elemento.id=="FindMateriaTextBx")
        {    var IDDOC=ElementGetValue("iddocente");
             var IDCLC=ElementGetValue("idciclo");
             var IDMST=ElementGetValue("idmaestria");
             var Params = [Elemento.value,IDDOC,IDCLC,IDMST];
             xajax_BuscaModalByTX("btmateria",Params);
        }
        return false;    
}

function XAJAXResponse(Mensaje,Datos)
{   if (Mensaje==="CMD_SAV")
    {   SAlert("success", "¡Excelente!", Datos);
    }
    else if(Mensaje==='CMD_DEL')
    {   SAlert("warning", "¡Cuidado!", Datos);    
    }
    else if(Mensaje==='CMD_WRM')
    {   SAlert("warning", "¡Cuidado!", Datos);
    }
    else if(Mensaje==='CMD_ERR')
    {   SAlert("error", "¡Error!", Datos);
    }
}var COLINI = 5;
var NTETAP = "";
var CAMBIO = 0;

function ButtonClick(Operacion)
{       if (Operacion=="MinSav")
        {   if (CAMBIO==1)
            {   var tabla = document.getElementById("DetSch");
                if (tabla.rows.length>0)
                {   CAMBIO=0;
                    ElementStatus("","MinSav:MinPrn");
                    xajax_GuardaDB(PreparaDatosGrid(tabla.rows.length));
                }    
            }
            else
            SAlert("warning","¡Cuidado!","Los datos estan actualizados.");
        }
        else if (Operacion=="MinPrn")
        {   if (ElementGetValue("TipoNota")!=0)
            {   //if (confirm("Se dispone a cerrar el acta de calificaciones. ¿Desea continuar?"))
                {
                    ElementSetValue("TipoNota",0);
                    ElementStatus("","MinSav:MinPrn");
                    var DOCTE = ElementGetValue("iddocente");
                    var CICLO = ElementGetValue("idciclo");   
                    var MAEST = ElementGetValue("idmaestria");
                    var MATER = ElementGetValue("idmateria");
                    var GRUPO = ElementGetValue("idgrupo");
                    var POLEV = ElementGetValue("idpolev");
                    var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP];  
                    NTETAP="";
                    xajax_ActaCalificacion(Params);
                }    
            }    
        }
        else if (Operacion=="btdocente")
        {    xajax_CargaModal(Operacion,0);
        }
        else if (Operacion=="btmaestria")
        {    var IDDOC=ElementGetValue("iddocente");
             if (IDDOC!="")
                xajax_CargaModal(Operacion,IDDOC);
             else
                SAlert("warning","¡Cuidado!","Debe seleccionar un Docente.");
        }
        else if (Operacion=="btmateria")
        {    var IDMST=ElementGetValue("idmaestria");
             if (IDMST!="")
             {   var IDDOC=ElementGetValue("iddocente");
                 var IDCLC=ElementGetValue("idciclo");
                 var Params = [IDDOC,IDCLC,IDMST];
                 xajax_CargaModal(Operacion,Params);
             }
             else
             SAlert("warning","¡Cuidado!","Debe seleccionar una Maestria.");
        }
        return false;
}

function PreparaDatosGrid(Estudiantes)
{       var Forma = [];
        for (t=1;t<=Estudiantes;t++)
        {   var Cell = "TD_"+StrPadCeroNumber("00",t);
            if (t==1)
            {   var fila = document.getElementById("TR_"+StrPadCeroNumber("00",t)).cells;
                var tope = fila.length-1;
            }
            var Nota = document.getElementById(Cell+"_000").innerHTML;
            var Esmt = document.getElementById(Cell+"_001").innerHTML;
            var Filas = 'NO='+t+'|ID='+Nota+'|ESMA='+Esmt+'|JSON=';
            var Sjson = '';    
            for (s=COLINI;s<=tope;s++)
            {   var Evl = document.getElementById(Cell+"_"+StrPadCeroNumber("000",s)).innerHTML;  
                var Str = $(Evl).prop("name");
                var Exc = Str.substring(0,1);
                if (Exc=="N")
                {   var Str = $(Evl).prop("id");
                    var Txt = Str.substring(2,Str.length);
                    var Vlr = ElementGetValue(Str);
                    Sjson = Sjson + (Sjson!='' ? '","': '{"')+Txt+'":"'+ (Vlr!='' ? Vlr:0);
                }
            }
            Sjson = Sjson + '"}';
            Forma.push(PrepareFreeElementsNotas(Filas+Sjson));
        }
        return JSON.stringify({Forma});            
}

function SearchDocenteGetData(DatosGrid)
{       var IDDOC = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("iddocente")!=IDDOC)
        {   NTETAP = "";
            FillOptionCbx("TipoNota",[[0,"SELECCIONE..."]]);
            ElementStatus("","MinSav:MinPrn");
            ElementSetValue("TipoNota",0);
            ElementSetValue("WrkGrd","");
            ElementClear("idmaestria:txmaestria:txcohorte:idciclo:idmateria:txmateria:idgrupo:txgrupo:idpolev:idmaesv");
            document.getElementById("iddocente").value = IDDOC;
            document.getElementById("txdocente").value = DatosGrid.cells[2].innerHTML;
            cerrar();
        }    
        else
        cerrar();
        return false;
}

function SearchMaestriaGetData(DatosGrid)
{       var IDMST = DatosGrid.cells[0].innerHTML;
        var IDCLC = DatosGrid.cells[2].innerHTML;
        if (ElementGetValue("idmaestria")!=IDMST || ElementGetValue("idciclo")!=IDCLC)
        {   NTETAP = "";
            FillOptionCbx("TipoNota",[[0,"SELECCIONE..."]]);
            ElementStatus("","MinSav:MinPrn");
            ElementSetValue("WrkGrd","");
            ElementClear("idmateria:txmateria:idgrupo:txgrupo");
            document.getElementById("idmaestria").value = IDMST;
            document.getElementById("idciclo").value = DatosGrid.cells[2].innerHTML;
            document.getElementById("idpolev").value = DatosGrid.cells[4].innerHTML;
            document.getElementById("idmaesv").value = DatosGrid.cells[5].innerHTML;
            document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("txcohorte").value = DatosGrid.cells[3].innerHTML;
            cerrar();
        }
        else
        cerrar();    
        return false;
}  

function SearchMateriaGetData(DatosGrid)
{       var IDGRP = DatosGrid.cells[2].innerHTML;
        if (ElementGetValue("idgrupo")!=IDGRP)
        {   NTETAP = "";
            ElementStatus("","MinSav:MinPrn");
            ElementSetValue("TipoNota",0);
            ElementSetValue("WrkGrd","");
            document.getElementById("idmateria").value = DatosGrid.cells[0].innerHTML;;
            document.getElementById("txmateria").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idgrupo").value = IDGRP;
            document.getElementById("txgrupo").value = DatosGrid.cells[3].innerHTML;
            xajax_BuscaParcialesByID(ElementGetValue("idpolev"));
            cerrar();
        }    
        else    
        cerrar();    
        return false;
}

function SearchTipoNotaGetData(DatosCombo)
{       if (DatosCombo.value==0)
        {   NTETAP = "";
            ElementSetValue("WrkGrd","");
            ElementStatus("","MinSav:MinPrn");
        }
        else
        {   var ETAPA = ElementGetText(DatosCombo.id);                     
            if (NTETAP!=ETAPA)
            {   NTETAP = ETAPA;
                ElementStatus("MinSav:MinPrn","");
                SearchNotasGetDataGrid();
            }
        }    
}  

function SearchNotasGetDataGrid()
{       var DOCTE = ElementGetValue("iddocente");
        var MAESV = ElementGetValue("idmaesv");
        var CICLO = ElementGetValue("idciclo");   
        var MAEST = ElementGetValue("idmaestria");
        var MATER = ElementGetValue("idmateria");
        var GRUPO = ElementGetValue("idgrupo");
        var POLEV = ElementGetValue("idpolev");
        var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP,MAESV];
        xajax_BuscaByID(Params);
}  

function calculaNotas(Cld,Pev,Min,Max)
{       if (Cld.value=="" || Cld.value==".")               
            var Eval = 0; 
        else
            var Eval = parseFloat(Cld.value);
        
        if (Eval>=parseFloat(Min) && Eval<=parseFloat(Max))
        {   var str = Cld.id;
            var Row = str.substring(0, 2);
            ContentFlag = document.getElementById(Row+"EST");
            if (ContentFlag.value.length==0)
            {   var Cll = "TD_"+Row;
                var fila = document.getElementById("TR_"+Row).cells;
                var tope = fila.length-1;
                var objs = "";
                for (s=COLINI;s<=tope;s++)
                {   var Evl = document.getElementById(Cll+"_"+StrPadCeroNumber("000",s)).innerHTML;  
                    objs = objs+(objs!="" ? ":":"")+$(Evl).prop("id");
                }    
                CAMBIO=1;
                var Forma = PrepareElements(objs);
                var Maesv = ElementGetValue("idmaesv");
                var Mater = ElementGetValue("idmateria");
                var Param = [Pev,NTETAP,Row,Maesv,Mater];
                xajax_CalculaNotas(Param,JSON.stringify({Forma}));
            }    
            return false;
        }   
        Cld.value="";    
}

function SearchByElement(Elemento)
{       if (Elemento.id=="FindDocenteTextBx")
        {    xajax_BuscaModalByTX("btdocente",Elemento.value);
        }
        else if (Elemento.id=="FindMaestriaTextBx")
        {    var Params = [Elemento.value,ElementGetValue("iddocente")];
             xajax_BuscaModalByTX("btmaestria",Params);
        }
        else if (Elemento.id=="FindMateriaTextBx")
        {    var IDDOC=ElementGetValue("iddocente");
             var IDCLC=ElementGetValue("idciclo");
             var IDMST=ElementGetValue("idmaestria");
             var Params = [Elemento.value,IDDOC,IDCLC,IDMST];
             xajax_BuscaModalByTX("btmateria",Params);
        }
        return false;    
}

function XAJAXResponse(Mensaje,Datos)
{   if (Mensaje==="CMD_SAV")
    {   SAlert("success", "¡Excelente!", Datos);
    }
    else if(Mensaje==='CMD_DEL')
    {   SAlert("warning", "¡Cuidado!", Datos);    
    }
    else if(Mensaje==='CMD_WRM')
    {   SAlert("warning", "¡Cuidado!", Datos);
    }
    else if(Mensaje==='CMD_ERR')
    {   SAlert("error", "¡Error!", Datos);
    }
}