<?php
        $Opcion = 'NotasMod';
        require_once('src/modules/maestria/notasmod/controlador/ControlNotasMod.php');
        $ControlNotasMod = new ControlNotasMod();

        $xajax->register(XAJAX_FUNCTION,array('GuardaDB', $ControlNotasMod,'GuardaDB'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlNotasMod,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaParcialesByID', $ControlNotasMod,'ConsultaParcialesByID'));
        $xajax->register(XAJAX_FUNCTION,array('CalculaNotas', $ControlNotasMod,'CalculaNotas'));
        /*
        $xajax->register(XAJAX_FUNCTION,array('CreaArchivoNotas', $ControlNotasMod,'CreaArchivoNotas'));
        $xajax->register(XAJAX_FUNCTION,array('CargaArchivoNotas', $ControlNotasMod,'CargaArchivoNotas'));
        */
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlNotasMod,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlNotasMod,'ConsultaGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('ActaCalificacion', $ControlNotasMod,'ActaCalificacion'));
        $xajax->processRequest();
        
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/maestria/notasmod/js/jsNotasMod.js?3"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:900px">
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionData">   
                             <?php  $ControlNotasMod->CargaMantenimiento();  ?>
                        </div> 
                        <div id="<?php echo 'WrkGrd'; ?>" class="FormSectionGrid">  
                        </div>     
                    </form>
                </div>       
        </body>
        </html>
              

