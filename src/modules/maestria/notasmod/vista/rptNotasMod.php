<?php   require_once("src/rules/maestria/servicio/ServicioNotasMod.php");
        require_once("src/rules/maestria/servicio/ServicioReporte.php");
        require_once("src/libs/plugins/pdf/vendor/mpdf/mpdf/mpdf.php");
        require_once("src/rules/maestria/dao/DAOPoliticaEvaluacion.php"); 
        
        class rptNotasMod
        {
                private function ActaCSS()
                {       $Style = 'style = "background-color: #FFF;
                                  border-collapse:collapse; 
                                  border:1px solid #ccc;
                                  font-family:tahoma; 
                                  font-size:8pt; 
                                  width:875px"';    
                        return  $Style;          
                }

                private function ColmCSS()
                {       $Style = 'style="text-align:center;
                                  border:1px solid #ccc;
                                  border-bottom:none;
                                  border-left:none"';    
                        return  $Style;          
                }
                
                private function DetsCSS($align)
                {       $Style = 'style="text-align:'.$align.';
                                  border:1px solid #ccc;   
                                  border-left:none"';    
                        return  $Style;          
                }
                
                private function DetsCSSI($align)
                {       $Style = 'style="text-align:'.$align.';
                                  border:1px solid #ccc;   
                                  "';    
                        return  $Style;          
                }
                
                function GeneraActaCalificacion($Infrm,$Notas)
                {       $RESP = $this->ActaDetalles($Notas[1],$Notas[2]);
                        if ($RESP[0])
                        {   $HTML = '<table border="1" '.$this->ActaCSS().'>';
                            $HTML = $this->ActaCabecera($HTML,$Infrm[1]);
                            $HTML = $this->ActaColumnas($HTML,$Notas[1]);
                            $HTML.= $RESP[1]; //DETALLE DE CALIFICACION
                            $HTML = $this->ActaPiePagin($HTML,$Infrm[1][0]['DOCENTE']);
                            $HTML.= "</table>";
                            return array(true,$HTML);
                        }    
                        return $RESP;
                }
                
                private function CabsCSS()
                {       $Style = 'style = "background-color: #FFF;
                                  border-collapse:collapse; 
                                  border:0px solid #ccc;
                                  font-family:tahoma; 
                                  font-size:8pt; 
                                  width:100%"';    
                        return  $Style;          
                }
                
                function ActaCabecera($HTML,$Infrm)
                {       $Dato = $Infrm[0];
                        $HTML.= '<tr>';
                        $HTML.= '<td style="border:none" colspan="20">';
                        $HTML.=     '<table border="0" '.$this->CabsCSS().'>';
                        $HTML.=     '<tr height="20px"><td colspan="4"></td></tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:13%;text-align:center" rowspan="5">';
                        $HTML.=             '<img src="src/public/img/logo_ug_small.png" width="87px" height="103px">';
                        $HTML.=         '</td>';
                        $HTML.=         '<td style="width:87%;text-align:center" colspan="3"><b>UNIVERSIDAD DE GUAYAQUIL</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>FACULTA DE '.$Dato['FACULTAD'].'</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>COORDINACION DE POSGRADO</b></td>';
                        $HTML.=     '</tr>';    
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>MAESTRIA EN '.utf8_encode($Dato['MAESTRIA']).'</b></td>';
                        $HTML.=     '</tr>';    
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>ACTA DE CALIFICACIONES/SISTEMA '.$Dato['TIPOMALLA'].'</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:13%;"><b>ASIGNATURA</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.utf8_encode($Dato['MATERIA']).'</td>';
                        $HTML.=         '<td style="width:13%;"><b>PROFESOR</b></td>'; 
                        $HTML.=         '<td style="width:34%;">'.utf8_encode($Dato['DOCENTE']).'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:13%;"><b>GRUPO</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['PARALELO'].'</td>';
                        $HTML.=         '<td style="width:13%;"><b>COHORTE</b></td>'; 
                        $HTML.=         '<td style="width:34%;">'.$Dato['COHORTE'].'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:13%;"><b>DESDE</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['DESDE']->format('d/m/Y').'</td>';
                        $HTML.=         '<td style="width:13%;"><b>HASTA</b></td>'; 
                        $HTML.=         '<td style="width:34%;">'.$Dato['HASTA']->format('d/m/Y').'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr >';
                        $HTML.=         '<td height="10px" colspan="4"></td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '</table>';
                        $HTML.= '</td>';
                        $HTML.= '</tr>';
                        return $HTML;
                }   
                
                function ActaPiePagin($HTML,$Docente)
                {       $DateControl = new DateControl();
                        $HTML.= '<tr>';
                        $HTML.= '<td style="border:none" colspan="20">';
                        $HTML.=     '<table border="0" '.$this->CabsCSS().'>';
                        $HTML.=     '<tr><td height="50px" colspan="5"></td></tr>';
                        $HTML.=     '<tr>';
                        $HTML.=         '<td height="50px" style="width:5%;text-align:center"></td>';
                        $HTML.=         '<td height="50px" style="width:40%;text-align:center"><b>Firma del Docente</b><br>Fecha: '.$DateControl->getNowDateTime("FECHA").'</td>';
                        $HTML.=         '<td height="50px" style="width:10%;text-align:center"></td>';
                        $HTML.=         '<td height="50px" style="width:40%;text-align:center"><b>Firma del Coordinador de Programas</b><br>Fecha: ___/___/_____</td>';
                        $HTML.=         '<td height="50px" style="width:5%;text-align:center"></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '</table>';
                        $HTML.= '</td>';
                        $HTML.= '</tr>';
                        return $HTML;
                }
                
                function ActaColumnas($HTML,$Columnas)
                {       $HTML.= "<tr>";
                        $HTML.= '<td width="30px" '.$this->ColmCSS().'><b>No</b></td>';
                        $HTML.= '<td width="70px" '.$this->ColmCSS().'><b>CEDULA</b></td>';
                        $HTML.= '<td width="230px" '.$this->ColmCSS().'><b>ESTUDIANTE</b></td>';
                        foreach($Columnas as $Col)
                        {       $ancho = $Col['ANCHO']+10;
                                $HTML.= '<td width="'.$ancho.'px" '.$this->ColmCSS().'><b>'.$Col['COLUMNA'].'</b></td>';   
                        }
                        $HTML.= "</tr>";
                        return $HTML;
                }

                function ActaDetalles($Columnas,$Datos)
                {       $HTML = "";
                        $num = 0;
                        foreach ($Datos as $Dato)
                        {       $num++;
                                $HTML.= '<tr height="18px">';
                                $HTML.= '<td '.$this->DetsCSSI("center").'>'.str_pad($num,2,'0',STR_PAD_LEFT).'</td>';
                                $HTML.= '<td '.$this->DetsCSS("center").'>'.$Dato['CEDULA'].'</td>';
                                $HTML.= '<td '.$this->DetsCSS("left").'>'. htmlentities($Dato['ESTUDIANTE']).'</td>';
                                foreach ($Columnas as $Col)
                                {       $vacol = $Dato[$Col['VARIABLE']];
                                        if ($Col['TIPODATO']=="N")
                                            $HTML.= '<td '.$this->DetsCSS("right").'>'.(doubleval($vacol)==0 ? number_format($vacol,$Col['DECIMALES']):$vacol).'</td>';
                                        else
                                            $HTML.= '<td '.$this->DetsCSS("center").'>'.(is_numeric($vacol) ? "":$vacol).'</td>';
                                }
                                $HTML.= "</tr>";    
                        }
                        return array(true,$HTML);
                }
        }
        
        if (isset($_GET['opcion']))
        {   $Utils = new Utils();
            $Nodos = $Utils->Desencriptar($_GET['opcion']);
            $Params = explode("|",$Nodos);
          //[DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP] 
            $ServicioNotasMod = new ServicioNotasMod();
            $Notas = $ServicioNotasMod->CierreNotas($Params);
            if ($Notas[0])
            {   $ServicioReporte = new ServicioReporte();
                $Infrm = $ServicioReporte->DatosCabeceraReporte($Params);
                if ($Infrm[0])
                {   $rptNotasMod = new rptNotasMod();
                    $SearchGrid = $rptNotasMod->GeneraActaCalificacion($Infrm,$Notas);
                    if ($SearchGrid[0])
                    {   
                        $mpdf = new Mpdf();
                        $mpdf->WriteHTML($SearchGrid[1]);
                        $mpdf->Output('ACC'.$Params[0].$Params[3].'.pdf','I');
                    }
                    else
                    echo $SearchGrid[1];
                }
                else
                echo $Infrm[1];    
            }
            else
            echo $Notas[1];
        }
        else
        echo "Error en el paso de opciones.";
        
        