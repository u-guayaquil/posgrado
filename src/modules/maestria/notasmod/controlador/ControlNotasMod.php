<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");    
        require_once("src/rules/maestria/servicio/ServicioNotasMod.php");
        require_once("src/modules/maestria/notasmod/render/RenderNotasMod.php");        
        require_once("src/rules/maestria/servicio/ServicioDocente.php");  
        require_once("src/rules/maestria/servicio/ServicioPlanAnalitico.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");
        require_once("src/rules/maestria/servicio/ServicioPoliticaEvaluacion.php"); 
        require_once("src/rules/maestria/servicio/ServicioReporte.php");
        
        class ControlNotasMod
        {       private $ServicioNotasMod;
                private $ServicioDocente;
                private $RenderNotasMod;
                private $ServicioPlanAnalitico;
                private $ServicioUsuarioFiltro;
                private $ServicioPoliticaEvaluacion;
                private $ServicioReporte;
                
                function __construct()
                {       $this->ServicioNotasMod = new ServicioNotasMod();
                        $this->ServicioDocente = new ServicioDocente();        
                        $this->RenderNotasMod = new RenderNotasMod();
                        $this->ServicioPlanAnalitico = new ServicioPlanAnalitico();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                        $this->ServicioPoliticaEvaluacion = new ServicioPoliticaEvaluacion();
                        $this->ServicioReporte = new ServicioReporte();
                }

                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderNotasMod->CreaBarButton($Buttons[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderNotasMod->CreaMantenimiento();
                }

                function ConsultaByID($Params)
                {       $xAjax = new xajaxResponse();
                        $Notas = $this->ServicioNotasMod->ConsultarNotas($Params);
                        if ($Notas[0])
                        {   $Existe = $this->ServicioNotasMod->ConsultaCierreActa($Params);
                            $SearchGrid = $this->RenderNotasMod->CreaSearchGrid($Notas,$Existe);
                            $xAjax->Assign("WrkGrd","innerHTML",$SearchGrid);
                            if ($Existe==1)
                                $xAjax->Call("ElementStatus","MinPrn","MinSav");
                        }
                        else
                        $xAjax->Call("XAJAXResponse", "CMD_ERR", $Notas[1]);                       
                        return $xAjax;
                }
                
                function ActaCalificacion($Params)
                {       $xAjax = new xajaxResponse();
                        $Utils = new Utils();
                        $urlfl = $Utils->Encriptar("src/modules/maestria/notasmod/vista/rptNotasMod.php");
                        $title = "Acta de Calificaciones";
                        $optns = $Utils->Encriptar($Params[0]."|".$Params[1]."|".$Params[2]."|".$Params[3]."|".$Params[4]."|".$Params[5]."|".$Params[6]); 
                            
                        $jsonModal['Modal'] = "ModalMINPRN"; 
                        $jsonModal['Title'] = $title;
                        $jsonModal['Carga'] = "<iframe name='ModalMINPRN' src='index.php?url=".$urlfl."&opcion=".$optns."' height='465px' width='100%' scrolling='si' frameborder='0' marginheight='0' marginwidth='0'></iframe>";  
                        $jsonModal['Ancho'] = "900";
                        $jsonModal['Alto']  = "500";
                        $xAjax->call("CreaModal", json_encode($jsonModal));
                        $xAjax->Assign("WrkGrd","innerHTML","");
                        return $xAjax;
                }
                
                function ConsultaParcialesByID($IdPolitica)
                {       $xAjax = new xajaxResponse();
                        $Polev = $this->ServicioPoliticaEvaluacion->ConsultaParcialesPoliticaEvaluacion($IdPolitica);
                        $xAjax->Call("FillOptionCbx","TipoNota",$Polev[1]);
                        $xAjax->Call("ElementSetValue","TipoNota",0);
                        if (!$Polev[0]) $xAjax->Call("XAJAXResponse", "CMD_ERR", "No existe definida una estructura para calificaciones.");
                        return $xAjax;
                }
                
                function CalculaNotas($Param,$Form)
                {       $xAjax = new xajaxResponse();
                        $Notas = json_decode($Form)->Forma;
                        $Notas = $this->ServicioNotasMod->CalculaNotasByFila($Param,$Notas);
                        if ($Notas[0])
                            $xAjax = $this->RenderNotasMod->MuestraFilaNotas($xAjax,$Notas[1]);
                        else
                        $xAjax->Call("XAJAXResponse", "CMD_ERR", $Notas[1]);
                        return $xAjax;
                }
                
                function GuardaDB($Form)
                {       $xAjax = new xajaxResponse();
                        $Notas = json_decode($Form)->Forma;
                        $Datos = $this->ServicioNotasMod->GuardaDB($Notas);
                        if ($Datos[0])
                        {   $xAjax = $this->RenderNotasMod->MuestraGrid($xAjax,$Datos[1]);
                            $xAjax->Call("ElementStatus","MinSav:MinPrn","");
                            $xAjax->Call("XAJAXResponse", "CMD_SAV", "Las calificaciones se registraron con exito.");
                        }
                        else
                        $xAjax->Call("XAJAXResponse", "CMD_SAV", "No se pudo actualizar las calificaciones."); 
                        return $xAjax;
                }
             
                private function FilterOfAccess($Params)
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        $Docnte = $Params['dct'];

                        if ($Modulo->Idrol==3)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            if ($Docnte==0){
                                return array('texto'=>$Params['txt'], 'facultad'=>$Factad); 
                            }    
                            return array('texto'=>$Params['txt'], 'facultad'=>$Factad, 'docente'=>$Params['dct']);
                        }
                        else if ($Modulo->Idrol==2)
                        {   $Dcte = ($Docnte==0 ? $Modulo->Cedula : $Docnte); 
                            return array('texto'=>$Params['txt'], 'docente'=>$Dcte);
                        }
                        else
                        return array('facultad'=>array('00')); //No permiso a Estudiante
                }
                
                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion=="btdocente")
                        {   $Params = array('txt'=>'','dct'=>0);
                            $ArDatos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Docente";
                            $jsonModal['Carga'] = $this->RenderNotasMod->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderNotasMod->GetGridTableWidth();
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Params = array('txt'=>'','dct'=>$IdRef);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderNotasMod->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderNotasMod->GetGridTableWidth();
                        }   
                        else if ($Operacion==="btmateria")    
                        {   $prepareDQL = array('docente'=>$IdRef[0],'ciclo'=>$IdRef[1],'maestria'=>$IdRef[2]);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Materia";
                            $jsonModal['Carga'] = $this->RenderNotasMod->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderNotasMod->GetGridTableWidth();
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btdocente")    
                        {   $Params = array('txt'=>strtoupper(trim($Texto)),'dct'=>0);
                            $Datos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            return $this->RenderNotasMod->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $Params = array('txt'=>$Texto[0],'dct'=>$Texto[1]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            return $this->RenderNotasMod->MuestraModalGrid($xAjax,$Operacion,$Datos);                                       
                        }                        
                        else if ($Operacion=="btmateria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $prepareDQL = array('texto'=>$Texto[0],'docente'=>$Texto[1],'ciclo'=>$Texto[2],'maestria'=>$Texto[3]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            return $this->RenderNotasMod->MuestraModalGrid($xAjax,$Operacion,$Datos);         
                        }
                }
        }

?>

