<?php   
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php"); 
        require_once("src/rules/maestria/servicio/ServicioNotasMod.php");
                
        class RenderNotasMod
        {     private $Width;
              private $SearchGrid;
              private $Search;
              private $Columnas;
              private $Cerrado;
              
                function __construct()
                {       $this->Search = new SearchInput();   
                        $this->SearchGrid = new SearchGrid();
                        $this->ServicioNotasMod = new ServicioNotasMod();
                }

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaMantenimiento()
                {       $Col1="15%"; $Col2="40%"; $Col3="15%"; $Col4="15%"; $Col5="15%"; 

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="35">';
                        $HTML.= '           <td class="Form-Label" colspan="5">:: REGISTRO DE NOTAS</td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Docente</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("docente","","")->Enabled(true)->Create("t11");   
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idciclo" name="idciclo" value="" />';
                        $HTML.= '               <input type="hidden" id="idpolev" name="idpolev" value="" />';
                        $HTML.= '               <input type="hidden" id="idmaesv" name="idmaesv" value="" />';
                        $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t11");                         
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';     
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Cohorte</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idcohorte" name="idcohorte" value=""/>';
                        $HTML.= '               <input type="text" class="txt-input t12" id="txcohorte" name="txcohorte" value="" maxlength="4" ReadOnly/>';                       
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';        
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Materia</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("materia","","")->Enabled(true)->Create("t11");       
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                      //$HTML.='                <input type="button" id="mincontent" class="p-txt-label" style="border: 1px solid #aaa; width: 60px; height: 25px" value="Contenido" onclick=" return ButtonClick(\'mincontent\'); ">';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Grupo/Notas</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idgrupo" name="idgrupo" value="" />';
                        $HTML.= '               <input type="text" class="txt-input" style="width:113px" id="txgrupo" name="txgrupo" value="" readOnly/> ';
                        $HTML.= '               <select class="sel-input s07" id="TipoNota" name="TipoNota" onchange=" return SearchTipoNotaGetData(this)">';
                        $HTML.= '                       <option value="0">SELECCIONE...</option>';
                        $HTML.= '               </select>';    
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col3.'">';
                      //$HTML.= '               <div class="FileUpload">';
                      //$HTML.= '               <input type="file" class="BtnUp" name="file" id="file" onchange=" return LeerArchivoNotas(this);">';
                      //$HTML.= '               </div>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col4.'">';
                        $HTML.='                <input type="button" id="MinSav" class="p-txt-label-bold" style="height: 25px;text-decoration: none;font-weight: 600;font-size: 12px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin-top:0px;" value="✔ Guardar" onclick=" return ButtonClick(\'MinSav\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col5.'">';
                        $HTML.='                <input type="button" id="MinPrn" class="p-txt-label-bold" style="height: 25px;text-decoration: none;font-weight: 600;font-size: 12px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin-top:0px;" value="Imp. Acta" onclick=" return ButtonClick(\'MinPrn\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';  
                        return $HTML.'</table>';
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btdocente")
                        {   $Columns['id'] = array('0px','center','none');
                            $Columns['cedula'] = array('80px','center','');
                            $Columns['Docente']= array('300px','left','');
                        }          
                        else if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']=array('40px','center','');
                            $Columns['Maestria']=array('300px','left','');
                            $Columns['C.ID']=array('0px','center','none');
                            $Columns['Ciclo']=array('200px','left','');
                            $Columns['POL.ID']=array('0px','center','none');
                            $Columns['VER.ID']=array('0px','center','none');
                        }
                        else if ($Operacion=="btmateria")
                        {   $Columns['M.Id']=array('40px','center','');
                            $Columns['Materia']=array('250px','left','');
                            $Columns['G.ID']=array('40px','center','');
                            $Columns['Grupo']=array('150px','left','');
                        }
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btmateria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMATERIA'],MATERIA,'0',STR_PAD_LEFT);
                                        $ig = str_pad($ArDato['IDGRUPO'],GRUPO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ig);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['GRUPO']));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMAESTRIA'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MAESTRIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['CICLO']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDPOLEVAL']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMAESVER']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btdocente")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['CEDULA'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['APELLIDOS'])).' '.utf8_encode(trim($ArDato['NOMBRES'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }          
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
                private function SearchGridConfig($Cols)
                {       
                        //$Columns['UAD.ID'] = array('30px','center','');
                        $Columns['ID'] = array('0px','center','none');
                        $Columns['EM.ID'] = array('0px','center','none');
                        $Columns['ES.CD'] = array('0px','center','none');
                        $Columns['No'] = array('30px','center','');
                        $Columns['Estudiante'] = array('250px','left','');
                        
                        foreach($Cols as $Col)
                        {       $Columns[$Col[3]] = array($Col[4].'px','right','');   
                        }

                        
                        return array('AltoCab' => '35px','DatoCab' => $Columns,'AltoDet' => '300px','AltoRow' => '25px','FindTxt' =>false);
                }

                function CreaSearchGrid($Notas,$Cerrado=0)
                {       $this->Cerrado = $Cerrado;
                        $this->Columnas = $Notas[1];
                        $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig($Notas[1]));
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Notas[2]);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                private function GridDataHTML($Grid,$Datos)
                {       $num = 1;
                        $contador = 0;
                        $NotaFinal = 0;

                        foreach ($Datos as $Dato)
                        {       $Id = str_pad($num,2,'0',STR_PAD_LEFT);
                                //$Grid->CreaSearchCellsDetalle($Id,'',$Dato['UAD']);
                                $Grid->CreaSearchCellsDetalle($Id,'',$Dato['IDNOTAS']);
                                $Grid->CreaSearchCellsDetalle($Id,'',$Dato['IDESTUDMATE']);
                                $Grid->CreaSearchCellsDetalle($Id,'',$Dato['CEDULA']);
                                $Grid->CreaSearchCellsDetalle($Id,'',$num++);
                                $Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['ESTUDIANTE']));
                                
                                $Exis = false;
                                if ($Dato['NOTAS']!="")
                                {   $Exis = true;
                                    $Cols = json_decode($Dato['NOTAS']);   
                                }
                                foreach ($this->Columnas as $Col)
                                {       if ($this->Cerrado==0)
                                            $edita = $Col[8]=="S" ? "":"disabled";
                                        else
                                            $edita = "disabled";
                                            
                                        $tipod = $Col[9];
                                        $ancho = $Col[4]-4;
                                        $maxln = $Col[5]+3;
                                        $vacol = "";
                                        if ($Exis) 
                                        {   if ($Col[1]!="ASISTENCIA")
                                                $vacol = $Cols->{$Col[2]};
                                        }        
                                        if ($Col[1]=="ASISTENCIA")
                                        {$vacol = number_format(($Dato['ASIST']/$Dato['TCD'])*100,0);}


                                        if($contador == 6)
                                        {       $NotaFinal = $vacol;    }
                                        $contador++;

                                        $Asistencia = number_format(($Dato['ASIST']/$Dato['TCD'])*100,0); 
                                        $Estado = $NotaFinal >= 7 ? ( $Asistencia >= 80 ? "APROBADO":"REPROBADO"):"REPROBADO";
                                        
                                        if ($tipod=="N")
                                        {       $Txts = '<input type="text" '.$this->CellCSS($ancho,"right").' id="'.$Id.$Col[2].'" name="'.$Col[10].$Id.$Col[2].'" value="'.(doubleval($vacol)==0 ? "":$vacol).'" onKeyDown=" return soloFloatDep(event,this,'.$Col[5].',\''.$Id.'EST'.'\');" onblur=" return calculaNotas(this,'.$Col[0].','.$Col[6].','.$Col[7].');" maxlength="'.$maxln.'" '.$edita.'/>';}
                                        else
                                        {       if($vacol != $Estado && $edita == "disabled")
                                                {       if(isset($Cols)){
                                                                $Cols->EST = $Estado;
                                                                $this->ServicioNotasMod->ActualizaNotaEstadoDB($Dato['IDNOTAS'],json_encode($Cols));
                                                        }
                                                }
                                                $Txts = '<input type="text" '.$this->CellCSS($ancho,"left").'  id="'.$Id.$Col[2].'" name="'.$Col[10].$Id.$Col[2].'" value="'.($vacol=="0" ? "":$Estado).'" '.$edita.'/>';
                                        }
                                        $Grid->CreaSearchCellsDetalle($Id,'',$Txts);

                                        if($contador == 9)
                                        {       $contador = 0;  }  
                                }
                                
                                $Grid->CreaSearchRowsDetalle ($Id,"black",0);
                        }
                        return $Grid->CreaSearchTableDetalle(0,$Datos[1]);
                }
                
                function CellCSS($ancho,$align)
                {       $Style = 'Style = "width:'.$ancho.'px;                                   
                                  height:18px;
                                  border:1px solid #CCC;
                                  text-align:'.$align.';
                                  font-size: 8pt;
                                  background-color: transparent"';
                        return $Style;          
                }
                
                function MuestraFilaNotas($Ajax,$Notas)
                {       foreach ($Notas as $Name=>$Value)
                        {        if (is_numeric($Value))
                                 $Ajax->Assign($Name,"value",(doubleval($Value)==0 ? "":$Value));
                                 else
                                 $Ajax->Assign($Name,"value",$Value);
                        }
                        return $Ajax;
                }
                
                function MuestraGrid($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {        $Ajax->Assign($Dato[0],"innerHTML",$Dato[1]);
                        }
                        return $Ajax;
                }    

                //-----------------------------------------------------------------------------------------------------------------
                
                /*
                private function ActaCSS()
                {       $Style = 'style = "background-color: #FFF;
                                  border-collapse:collapse; 
                                  border:1px solid #ccc;
                                  font-family:tahoma; 
                                  font-size:8pt; 
                                  width:875px"';    
                        return  $Style;          
                }

                private function ColmCSS()
                {       $Style = 'style="text-align:center;
                                  border:1px solid #ccc;
                                  border-bottom:none;
                                  border-right:none"';    
                        return  $Style;          
                }
                
                private function DetsCSS($align)
                {       $Style = 'style="text-align:'.$align.';
                                  border:1px solid #ccc;   
                                   
                                  border-right:none"';    
                        return  $Style;          
                }
                
                function GeneraActaCalificacion($Infrm,$Notas)
                {       $RESP = $this->ActaDetalles($Notas[1],$Notas[2]);
                        if ($RESP[0])
                        {   $HTML = '<table border="1" '.$this->ActaCSS().'>';
                            $HTML = $this->ActaCabecera($HTML,$Infrm[1]);
                            $HTML = $this->ActaColumnas($HTML,$Notas[1]);
                            $HTML.= $RESP[1]; //DETALLE DE CALIFICACION
                            $HTML = $this->ActaPiePagin($HTML,$Infrm[1][0]['DOCENTE']);
                            $HTML.= "</table>";
                            return array(true,$HTML);
                        }    
                        return $RESP;
                }
                
                private function CabsCSS()
                {       $Style = 'style = "background-color: #FFF;
                                  border-collapse:collapse; 
                                  border:0px solid #ccc;
                                  font-family:tahoma; 
                                  font-size:8pt; 
                                  width:100%"';    
                        return  $Style;          
                }
                
                function ActaCabecera($HTML,$Infrm)
                {       $Dato = $Infrm[0];
                        $HTML.= '<tr>';
                        $HTML.= '<td style="border:none" colspan="20">';
                        $HTML.=     '<table border="0" '.$this->CabsCSS().'>';
                        $HTML.=     '<tr height="20px"><td colspan="4"></td></tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:13%;text-align:center" rowspan="5">';
                        $HTML.=             '<img src="src/public/img/logo_ug_small.png" width="87px" height="103px">';
                        $HTML.=         '</td>';
                        $HTML.=         '<td style="width:87%;text-align:center" colspan="3"><b>UNIVERSIDAD DE GUAYAQUIL</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>FACULTA DE '.$Dato['FACULTAD'].'</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>COORDINACION DE POSGRADO</b></td>';
                        $HTML.=     '</tr>';    
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>MAESTRIA EN '.utf8_encode($Dato['MAESTRIA']).'</b></td>';
                        $HTML.=     '</tr>';    
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>ACTA DE CALIFICACIONES/SISTEMA '.$Dato['TIPOMALLA'].'</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:13%;"><b>ASIGNATURA</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.utf8_encode($Dato['MATERIA']).'</td>';
                        $HTML.=         '<td style="width:13%;"><b>PROFESOR</b></td>'; 
                        $HTML.=         '<td style="width:34%;">'.utf8_encode($Dato['DOCENTE']).'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:13%;"><b>GRUPO</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['PARALELO'].'</td>';
                        $HTML.=         '<td style="width:13%;"><b>COHORTE</b></td>'; 
                        $HTML.=         '<td style="width:34%;">'.$Dato['COHORTE'].'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:13%;"><b>DESDE</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['DESDE']->format('d/m/Y').'</td>';
                        $HTML.=         '<td style="width:13%;"><b>HASTA</b></td>'; 
                        $HTML.=         '<td style="width:34%;">'.$Dato['HASTA']->format('d/m/Y').'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '</table>';
                        $HTML.= '</td>';
                        $HTML.= '</tr>';
                        return $HTML;
                }   
                
                function ActaPiePagin($HTML,$Docente)
                {       $DateControl = new DateControl();
                        $HTML.= '<tr>';
                        $HTML.= '<td style="border:none" colspan="20">';
                        $HTML.=     '<table border="0" '.$this->CabsCSS().'>';
                        $HTML.=     '<tr height="50px"><td colspan="5"></td></tr>';
                        $HTML.=     '<tr height="50px">';
                        $HTML.=         '<td style="width:5%;text-align:center"></td>';
                        $HTML.=         '<td style="width:40%;text-align:center"><b>Firma del Docente</b><br>Fecha: '.$DateControl->getNowDateTime("FECHA").'</td>';
                        $HTML.=         '<td style="width:10%;text-align:center"></td>';
                        $HTML.=         '<td style="width:40%;text-align:center"><b>Firma del Coordinador de Programas</b><br>Fecha: ___/___/_____</td>';
                        $HTML.=         '<td style="width:5%;text-align:center"></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '</table>';
                        $HTML.= '</td>';
                        $HTML.= '</tr>';
                        return $HTML;
                }
                
                function ActaColumnas($HTML,$Columnas)
                {       $HTML.= "<tr>";
                        $HTML.= '<td width="30px" '.$this->ColmCSS().'><b>No</b></td>';
                        $HTML.= '<td width="70px" '.$this->ColmCSS().'><b>CEDULA</b></td>';
                        $HTML.= '<td width="230px" '.$this->ColmCSS().'><b>ESTUDIANTE</b></td>';
                        foreach($Columnas as $Col)
                        {       $ancho = $Col['ANCHO']+10;
                                $HTML.= '<td width="'.$ancho.'px" '.$this->ColmCSS().'><b>'.$Col['COLUMNA'].'</b></td>';   
                        }
                        $HTML.= "</tr>";
                        return $HTML;
                }

                function ActaDetalles($Columnas,$Datos)
                {       $HTML = "";
                        $num = 0;
                        foreach ($Datos as $Dato)
                        {       $num++;
                                $HTML.= '<tr height="18px">';
                                $HTML.= '<td '.$this->DetsCSS("center").'>'.str_pad($num,2,'0',STR_PAD_LEFT).'</td>';
                                $HTML.= '<td '.$this->DetsCSS("center").'>'.$Dato['CEDULA'].'</td>';
                                $HTML.= '<td '.$this->DetsCSS("left").'>'. htmlentities($Dato['ESTUDIANTE']).'</td>';
                                foreach ($Columnas as $Col)
                                {       $vacol = $Dato[$Col['VARIABLE']];
                                        if ($Col['TIPODATO']=="N")
                                            $HTML.= '<td '.$this->DetsCSS("right").'>'.(doubleval($vacol)==0 ? number_format($vacol,$Col['DECIMALES']):$vacol).'</td>';
                                        else
                                            $HTML.= '<td '.$this->DetsCSS("center").'>'.(is_numeric($vacol) ? "":$vacol).'</td>';
                                }
                                $HTML.= "</tr>";    
                        }
                        return array(true,$HTML);
                }*/
                
        }
?>

