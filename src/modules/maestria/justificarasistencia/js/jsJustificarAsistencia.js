            $(document).ready(function()
            {       xajax_CargaWorkGrid("WrkGrd");  
            });
        
            function ButtonClick(Operacion)
            {   var objjustificar = "idCabecera:iddocente:txdocente:idmaestria:txmaestria:idciclo:txciclo:idmateria:txmateria:idgrupo:txgrupo:Fecha";
                var valjustificar = "txdocente:txmaestria:txciclo:txmateria:txgrupo:Fecha";    
                    if(Operacion=="addNew")
                    {   BarButtonState(Operacion);
                        document.getElementById("idCabecera").value = "";
                        document.getElementById("idhorario").value = "";
                        document.getElementById("txhorario").value = "";
                        ElementStatus("bthorario:txhorario","txciclo:txgrupo:btdocente:txdocente:btmaestria:txmaestria:btmateria:txmateria");
                    }
                    else if(Operacion=="addMod")
                    {   BarButtonState(Operacion);
                        ElementStatus("bthorario:txhorario","txciclo:txgrupo:btdocente:txdocente:btmaestria:txmaestria:btmateria:txmateria");
                    }
                    else if(Operacion=="addDel")
                    {   if (confirm("Desea inactivar este registro?"))
                        {   var Forma = PrepareElements(objjustificar);
                            xajax_Elimina(JSON.stringify({Forma}));
                        }
                    }
                    else if(Operacion=="addSav")
                    {   if (ElementValidateBeforeSave(valjustificar))
                        {      var Forma = PrepareElements(objjustificar);
                                xajax_CargaModal(Operacion,JSON.stringify({Forma}));
                        }
                        else
                        {SAlert("warning","¡Cuidado!","¡Llene todos los campos por favor!");}
                    }
                    else if(Operacion=="addCan")
                    {   BarButtonState(Operacion);
                        document.getElementById("idCabecera").value = "";
                        document.getElementById("idhorario").value = "";
                        document.getElementById("txhorario").value = "";
                        document.getElementById("Fecha").value = "";
                        ElementStatus("txciclo:txgrupo:btdocente:txdocente:btmaestria:txmaestria:btmateria:txmateria","bthorario:txhorario");
                    }
                    else if (Operacion=="btdocente")
                    {   xajax_CargaModal(Operacion,0);
                    }
                    else if (Operacion=="btmaestria")
                    {    var IDDOC=ElementGetValue("iddocente");
                        if (IDDOC!="")
                            xajax_CargaModal(Operacion,IDDOC);
                        else
                            SAlert("warning","¡Error!","¡Debe seleccionar un Docente!");
                    }
                    else if (Operacion=="btmateria")
                    {   var IDMST=ElementGetValue("idmaestria");
                        if (IDMST!="")
                        {   var IDDOC=ElementGetValue("iddocente");
                            var IDCLC=ElementGetValue("idciclo");
                            var Params = [IDDOC,IDCLC,IDMST];
                            xajax_CargaModal(Operacion,Params);
                        }
                        else
                            SAlert("warning","¡Error!","¡Debe seleccionar una Maestria!");
                    }
                    else if (Operacion=="bthorario")
                    {   var IDMAT=ElementGetValue("idmateria");
                        if (IDMAT!="")
                        {   var IDDOC=ElementGetValue("iddocente");
                            var IDCLC=ElementGetValue("idciclo");
                            var IDMST=ElementGetValue("idmaestria");
                            var IDGRP=ElementGetValue("idgrupo");
                            var Params = [IDDOC,IDCLC,IDMST,IDMAT,IDGRP];
                            xajax_CargaModal(Operacion,Params);
                        }
                        else
                            SAlert("warning","¡Error!","¡Debe seleccionar una Materia!");
                    }
                    return false;
            }

            function XAJAXResponse(Mensaje,Datos)
                {   if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Default");
                            document.getElementById("idCabecera").value = "";
                            document.getElementById("idhorario").value = "";
                            document.getElementById("txhorario").value = "";
                            document.getElementById("Fecha").value = "";
                            ElementStatus("txciclo:txgrupo:btdocente:txdocente:btmaestria:txmaestria:btmateria:txmateria","bthorario:txhorario");
                            SAlert("success","¡Excelente!",Datos);
                        }
                        else if(Mensaje==='CMD_ERR')
                        {   BarButtonState("addNew");
                            SAlert("error","¡Error!",Datos);
                        }    
                }

            function SearchDocenteGetData(DatosGrid)
            {       var IDDOC = DatosGrid.cells[0].innerHTML;
                    if (ElementGetValue("iddocente")!=IDDOC)
                    {   ElementStatus("","MinSav:MinOpn:bthorario:txhorario");
                        DeleteWorkGrid("FX");
                        BarButtonState("Inactive");
                        ElementClear("idmaestria:txmaestria:idciclo:idmateria:txmateria:idgrupo:txgrupo:idhorario:txhorario:txciclo");
                        document.getElementById("iddocente").value = IDDOC;
                        document.getElementById("txdocente").value = DatosGrid.cells[2].innerHTML;
                        cerrar();
                    }    
                    else
                    cerrar();
                    return false;
            }

            function SearchMaestriaGetData(DatosGrid)
            {       var IDMST = DatosGrid.cells[0].innerHTML;
                    if (ElementGetValue("idmaestria")!=IDMST)
                    {   ElementStatus("","MinSav:MinOpn:bthorario:txhorario");
                        DeleteWorkGrid("FX");
                        BarButtonState("Inactive");
                        ElementClear("idmateria:txmateria:idgrupo:txgrupo:idhorario:txhorario");
                        document.getElementById("idmaestria").value = IDMST;
                        document.getElementById("idciclo").value = DatosGrid.cells[2].innerHTML;
                        document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML;
                        document.getElementById("txciclo").value = DatosGrid.cells[3].innerHTML;
                        document.getElementById("idcohorte").value = DatosGrid.cells[4].innerHTML;
                        cerrar();
                    }
                    else
                    cerrar();    
                    return false;
            }  
    
            function SearchMateriaGetData(DatosGrid)
            {   DeleteWorkGrid("FX");
                var IDGRP = DatosGrid.cells[2].innerHTML;
                if (ElementGetValue("idgrupo")!=IDGRP)
                {   ElementStatus("","MinSav:MinOpn");
                    ElementClear("idhorario:txhorario");
                    
                    BarButtonState("Default");
                    ElementStatus("","bthorario:txhorario");
                    document.getElementById("idmateria").value = DatosGrid.cells[0].innerHTML;
                    document.getElementById("txmateria").value = DatosGrid.cells[1].innerHTML;
                    document.getElementById("idgrupo").value = IDGRP;
                    document.getElementById("txgrupo").value = DatosGrid.cells[3].innerHTML;

                    DOCTE=ElementGetValue("iddocente");
                    CICLO=ElementGetValue("idciclo");   
                    MAEST=ElementGetValue("idmaestria");
                    MATER=ElementGetValue("idmateria");
                    GRUPO=ElementGetValue("idgrupo");
                    var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO];
                    cerrar();
                    xajax_BuscaByID(Params);
                }    
                else    
                cerrar();    
                return false;
            }
    
            function SearchHorarioGetData(DatosGrid)
            {       var IDHOR = DatosGrid.cells[0].innerHTML; //secuencial solo de control
                    if (ElementGetValue("idhorario")!=IDHOR)
                    {   ElementSetValue("idhorario",IDHOR);
                        ElementSetValue("txhorario",DatosGrid.cells[3].innerHTML);
                        ElementSetValue("Fecha",DatosGrid.cells[4].innerHTML);
                        cerrar();
                    }
                    else
                    cerrar();
            }  

            function SearchByElement(Elemento)
            {       if (Elemento.id=="FindDocenteTextBx")
                    {    xajax_BuscaModalByTX("btdocente",Elemento.value);
                    }
                    else if (Elemento.id=="FindMaestriaTextBx")
                    {    var Params = [Elemento.value,ElementGetValue("iddocente")];
                         xajax_BuscaModalByTX("btmaestria",Params);
                    }
                    else if (Elemento.id=="FindMateriaTextBx")
                    {    var IDDOC=ElementGetValue("iddocente");
                         var IDCLC=ElementGetValue("idciclo");
                         var IDMST=ElementGetValue("idmaestria");
                         var Params = [Elemento.value,IDDOC,IDCLC,IDMST];
                         xajax_BuscaModalByTX("btmateria",Params);
                    }
                    return false;    
            }

            function WorkGridEditFX(Grilla){
                if(!ValidateStatusEnabled("addSav")){
                    BarButtonState("Active");
                    document.getElementById("idCabecera").value = Grilla.cells[0].innerHTML;
                    document.getElementById("txhorario").value = Grilla.cells[2].innerHTML;
                    document.getElementById("Fecha").value = Grilla.cells[3].innerHTML;
                }
            }

            function Guardar(Observacion)
            {   var objjustificar = "idCabecera:iddocente:txdocente:idmaestria:txmaestria:idciclo:txciclo:idmateria:txmateria:idgrupo:txgrupo:Fecha";
                var valjustificar = "txdocente:txmaestria:txciclo:txmateria:txgrupo:Fecha"; 
                var Obs = Observacion.value;
                if (ElementValidateBeforeSave(valjustificar) && Obs != "")
                {      if (BarButtonState("Inactive"))
                        {   cerrar();
                            var Forma = PrepareElements(objjustificar);
                            xajax_Guarda(JSON.stringify({Forma}), Obs);
                        }
                }
                else
                {   SAlert("warning","¡Warning!","¡Llene los campos por favor!");    }
            }

            function WorkGridAsigFX(Grilla)
            {   if(!ValidateStatusEnabled("addSav")){
                    var objjustificar = "iddocente:txdocente:idmaestria:txmaestria:idcohorte:idciclo:txciclo:idmateria:txmateria:idgrupo:txgrupo";
                    var valjustificar = "txdocente:txmaestria:txciclo:txmateria:txgrupo";
                    var Forma = PrepareElements(objjustificar);
                    Forma.idCabecera = Grilla.cells[0].innerHTML;
                    xajax_CargaModal("Asignar",JSON.stringify({Forma}));
                }
            }

            function SearchAsignarGetData(DatosGrid){
                var CEDULAEST=DatosGrid.cells[0].innerHTML;
                var IDCABECERA=DatosGrid.cells[2].innerHTML;
                if(DatosGrid.cells[3].innerHTML != "✔"){
                    xajax_AsignarEstudiantes(CEDULAEST,IDCABECERA,"Asignar");
                    DatosGrid.cells[3].innerHTML = "✔";
                }
                else{
                    xajax_AsignarEstudiantes(CEDULAEST,IDCABECERA,"Desasignar");
                    DatosGrid.cells[3].innerHTML = "";
                }
                return false;
            }