<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");    
        require_once("src/rules/maestria/servicio/ServicioJustificarAsistencia.php");
        require_once("src/modules/maestria/justificarasistencia/render/RenderJustificarAsistencia.php");        
        require_once("src/rules/maestria/servicio/ServicioDocente.php");  
        require_once("src/rules/maestria/servicio/ServicioPlanAnalitico.php");
        require_once("src/rules/maestria/servicio/ServicioHorarioPlanificacion.php");
        require_once("src/modules/maestria/horario/render/RenderHorarioPlanificacion.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");

        class ControlJustificarAsistencia
        {       private $ServicioJustificarAsistencia;
                private $ServicioDocente;
                private $RenderJustificarAsistencia;
                private $ServicioPlanAnalitico;
                private $ServicioHorarioPlanificacion;
                private $RenderHorarioPlanificacion;
                private $ServicioUsuarioFiltro;
                
                function __construct()
                {       $this->ServicioJustificarAsistencia = new ServicioJustificarAsistencia();
                        $this->ServicioDocente = new ServicioDocente();        
                        $this->RenderJustificarAsistencia = new RenderJustificarAsistencia();
                        $this->ServicioPlanAnalitico = new ServicioPlanAnalitico();
                        $this->ServicioHorarioPlanificacion = new ServicioHorarioPlanificacion();
                        $this->RenderHorarioPlanificacion = new RenderHorarioPlanificacion();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                }

                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderJustificarAsistencia->CreaBarButton($Buttons[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderJustificarAsistencia->CreaMantenimiento();
                }
                
                function CargaWorkGrid($Capa)
                {       $xAjax = new xajaxResponse();    
                        return $this->RenderJustificarAsistencia->CreaWorkGrid($xAjax,$Capa);
                }

                function ConsultaByID($Params)
                {       $xAjax = new xajaxResponse();
                        $Justificar = $this->ServicioJustificarAsistencia->ConsultaJustificaciones($Params);
                        return $this->RenderJustificarAsistencia->MuestraJustificarAsistencia($xAjax,$Justificar); 
                }

                function Guarda($Form,$Observacion)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma; 
                        $id = $this->ServicioJustificarAsistencia->GuardaDBJustificar($JsDatos,$Observacion);
                        if($id[0]){ 
                            $jsFuncionLimpiarGrid = "DeleteWorkGrid('FX')";
                            $ajaxRespon->call($jsFuncionLimpiarGrid);
                            $Params = [$JsDatos->iddocente,$JsDatos->idciclo,$JsDatos->idmaestria,$JsDatos->idmateria,$JsDatos->idgrupo];
                            $Justificar = $this->ServicioJustificarAsistencia->ConsultaJustificaciones($Params);
                            $this->RenderJustificarAsistencia->MuestraJustificarAsistencia($ajaxRespon,$Justificar);
                            $jsFuncion = "XAJAXResponse";
                            if(empty($JsDatos->idCabecera)){
                                $ajaxRespon->call($jsFuncion,"CMD_SAV","Justificación creada con éxito!");
                            } else {
                                $ajaxRespon->call($jsFuncion,"CMD_SAV","Justificación modificada con éxito!");
                            }
                            return $ajaxRespon;
                        }
                        else
                        {   $jsFuncion = "XAJAXResponse";
                            $ajaxRespon->call($jsFuncion,"CMD_ERR",$id[1]);
                            return $ajaxRespon;
                        }
                        
                }

                function Elimina($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $respuesta = $this->ServicioJustificarAsistencia->DesactivaJusticarAsistencia($JsDatos->idCabecera);
                        if($respuesta)
                        {   $jsFuncionLimpiarGrid = "DeleteWorkGrid('FX')";
                            $ajaxRespon->call($jsFuncionLimpiarGrid);
                            $Params = [$JsDatos->iddocente,$JsDatos->idciclo,$JsDatos->idmaestria,$JsDatos->idmateria,$JsDatos->idgrupo];
                            $Justificar = $this->ServicioJustificarAsistencia->ConsultaJustificaciones($Params);
                            $this->RenderJustificarAsistencia->MuestraJustificarAsistencia($ajaxRespon,$Justificar);
                            $jsFuncion = "XAJAXResponse";
                            $ajaxRespon->call($jsFuncion,"CMD_SAV","Justificación eliminada con éxito!");
                            return $ajaxRespon;
                        }
                        else{
                            $jsFuncion = "XAJAXResponse";
                            $ajaxRespon->call($jsFuncion,"CMD_ERR","Oops, ocurrio un error al eliminar la Justificación. Intente nuevamente por favor.");
                            return $ajaxRespon;
                        }
                }
                
                private function FilterOfAccess($Params)
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        $Docnte = $Params['dct'];

                        if ($Modulo->Idrol==3)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            if ($Docnte==0){
                                return array('texto'=>$Params['txt'], 'facultad'=>$Factad); 
                            }    
                            return array('texto'=>$Params['txt'], 'facultad'=>$Factad, 'docente'=>$Params['dct']);
                        }
                        else if ($Modulo->Idrol==2)
                        {   $Dcte = ($Docnte==0 ? $Modulo->Cedula : $Docnte); 
                            return array('texto'=>$Params['txt'], 'docente'=>$Dcte);
                        }
                        else
                        return array('facultad'=>array('00')); //No permiso a Estudiante
                }
                
                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();  
                        if ($Operacion=="btdocente")
                        {   $Params = array('txt'=>'','dct'=>0);
                            $ArDatos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Docente";
                            $jsonModal['Carga'] = $this->RenderJustificarAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderJustificarAsistencia->GetGridTableWidth();
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Params = array('txt'=>'','dct'=>$IdRef);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderJustificarAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderJustificarAsistencia->GetGridTableWidth();
                        }   
                        else if ($Operacion==="btmateria")    
                        {   $prepareDQL = array('docente'=>$IdRef[0],'ciclo'=>$IdRef[1],'maestria'=>$IdRef[2]);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Materia";
                            $jsonModal['Carga'] = $this->RenderJustificarAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderJustificarAsistencia->GetGridTableWidth();
                        }   
                        else if ($Operacion=="bthorario")
                        {   $ArDatos = $this->ServicioHorarioPlanificacion->BuscarHorarioMateriaByPlanificacion($IdRef,'CD');
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Horario";
                            $jsonModal['Carga'] = $this->RenderJustificarAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = 356;
                        }   
                        else if ($Operacion=="addSav")
                        {   $JsDatos = json_decode($IdRef)->Forma;
                            if(!$JsDatos->idCabecera)
                            {   $jsonModal['Modal'] = "Modal Observación"; 
                                $jsonModal['Title'] = "Observación";
                                $jsonModal['Carga'] = $this->RenderJustificarAsistencia->CrearModalObservacion($JsDatos);
                                $jsonModal['Ancho'] = 350;
                            }
                            else{
                                return $this->Guarda($IdRef,"");
                            }
                        }
                        else if ($Operacion=="Asignar")
                        {   $JsDatos = json_decode($IdRef)->Forma;
                            $ArDatos = $this->ServicioJustificarAsistencia->BuscarEstudiantes($JsDatos);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Seleccionar Estudiantes";
                            $jsonModal['Carga'] = $this->RenderJustificarAsistencia->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = 416;
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btdocente")    
                        {   $Params = array('txt'=>strtoupper(trim($Texto)),'dct'=>0);
                            $Datos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            return $this->RenderJustificarAsistencia->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $Params = array('txt'=>$Texto[0],'dct'=>$Texto[1]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            return $this->RenderJustificarAsistencia->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }                        
                        else if ($Operacion=="btmateria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $prepareDQL = array('texto'=>$Texto[0],'docente'=>$Texto[1],'ciclo'=>$Texto[2],'maestria'=>$Texto[3]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            return $this->RenderJustificarAsistencia->MuestraModalGrid($xAjax,$Operacion,$Datos);         
                        }
                }
                function AsignarEstudiantes($CedulaEstudiante,$IdCabecera,$Proceso)
                {   if($Proceso == "Asignar"){
                        $this->ServicioJustificarAsistencia->AsignarEstudiantes($CedulaEstudiante,$IdCabecera);
                    }
                    else if($Proceso == "Desasignar"){
                        $this->ServicioJustificarAsistencia->DesasignarEstudiantes($CedulaEstudiante);
                    }
                    return;
                }
        }

?>

