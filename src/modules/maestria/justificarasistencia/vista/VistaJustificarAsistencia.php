<?php
        $Opcion = 'JustificarAsistencia';
        require_once('src/modules/maestria/justificarasistencia/controlador/ControlJustificarAsistencia.php');
        $ControlJustificarAsistencia = new ControlJustificarAsistencia();

        $xajax->register(XAJAX_FUNCTION,array('CargaWorkGrid', $ControlJustificarAsistencia,'CargaWorkGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlJustificarAsistencia,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlJustificarAsistencia,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlJustificarAsistencia,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlJustificarAsistencia,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlJustificarAsistencia,'ConsultaGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaPermiso', $ControlJustificarAsistencia,'GuardaPermiso'));
        $xajax->register(XAJAX_FUNCTION,array('AsignarEstudiantes', $ControlJustificarAsistencia,'AsignarEstudiantes'));
        
        $xajax->processRequest();
        
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/maestria/justificarasistencia/js/jsJustificarAsistencia.js"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:720px">
                    <div class="FormSectionMenu">              
                    <?php   $ControlJustificarAsistencia->CargaBarButton($_GET['opcion']);
                            echo '<script type="text/javascript">';
                            echo '        BarButtonState("Inactive"); ';
                            echo '</script>';                    
                    ?>    
                    </div>  
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionData">   
                             <?php  $ControlJustificarAsistencia->CargaMantenimiento();  ?>
                        </div> 
                        <div id="<?php echo 'WrkGrd'; ?>" class="FormSectionGrid" style="width:645px; margin:auto;">  
                        </div>     
                    </form>
                </div>       
        </body>
        </html>
              

