<?php   
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php"); 
                
        class RenderJustificarAsistencia
        {     private $Width;
              private $SearchGrid;
              private $Search;
              private $Columnas;
              
                function __construct()
                {       $this->Search = new SearchInput();   
                        $this->SearchGrid = new SearchGrid();
                }

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaMantenimiento()
                {      
                        $Col1="11%"; $Col2="25%"; $Col3="13%"; $Col5="5%"; 

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="35">';
                        $HTML.= '           <td class="Form-Label" colspan="5">:: JUSTIFICAR ASISTENCIA</td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">Docente</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("docente","","")->Enabled(true)->Create("t10");   
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">Materia</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("materia","","")->Enabled(true)->Create("t10");       
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t10");                         
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">&nbsp;Grupo</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idgrupo" name="idgrupo" value="" />';
                        $HTML.= '               <input type="text" class="txt-input" style="width:275px" id="txgrupo" name="txgrupo" value="" readOnly/> '; 
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';             
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">&nbsp;Ciclo</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idcohorte" name="idciclo" value="" />';
                        $HTML.= '               <input type="hidden" id="idciclo" name="idciclo" value="" />';
                        $HTML.= '               <input type="text" class="txt-input" style="width:275px" id="txciclo" name="txciclo" value="" readOnly/> '; 
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">&nbsp;Horario</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="hidden" id="idCabecera" name="idCabecera" value="" />';
                        $HTML.= '               <input type="hidden" id="Fecha" name="Fecha" value="" />';
                        $HTML.=                 $this->Search->TextSch("horario","","")->Enabled(false)->Create("t10");  
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        return $HTML.'</table>';
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btdocente")
                        {   $Columns['id'] = array('0px','center','none');
                            $Columns['cedula'] = array('80px','center','');
                            $Columns['Docente']= array('300px','left','');
                        }          
                        else if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']=array('40px','center','');
                            $Columns['Maestria']=array('300px','left','');
                            $Columns['C.ID']=array('0px','center','none');
                            $Columns['Ciclo']=array('200px','left','');
                            $Columns['IdCohorte']=array('0px','center','none');
                        }
                        else if ($Operacion=="btmateria")
                        {   $Columns['M.Id']=array('40px','center','');
                            $Columns['Materia']=array('250px','left','');
                            $Columns['G.ID']=array('40px','center','');
                            $Columns['Grupo']=array('150px','left','');
                        }
                        else if ($Operacion=="bthorario")
                        {   $Columns['Id']  =array('40px','center','');
                            $Columns['Horeje'] =array('0px','left','none');
                            $Columns['Permiso'] =array('0px','left','none');
                            $Columns['Dia'] =array('250px','left','');
                            $Columns['Fecha']=array('0px','left','none');
                            $Columns['Hora']=array('0px','left','none');
                            $Columns['Horas']=array('40px','center','');
                            $Columns['Auth']=array('0px','left','none');
                            $Columns['Group']=array('10px','left','none');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '140px','AltoRow' => '20px','FindTxt'=>false);
                        }   
                        else if ($Operacion=="Asignar")
                        {       $Columns['Cedula'] = array('100px','center','');
                                $Columns['EstudianTe'] = array('250px','left','');
                                $Columns['Cabecera']=array('0px','left','none');
                                $Columns['✔'] = array('40px','center','');
                                return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px','FindTxt'=>false);
                        }                        
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btmateria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMATERIA'],MATERIA,'0',STR_PAD_LEFT);
                                        $ig = str_pad($ArDato['IDGRUPO'],GRUPO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ig);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['GRUPO']));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMAESTRIA'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MAESTRIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['CICLO']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDCOHORTE']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btdocente")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['CEDULA'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['APELLIDOS'])).' '.utf8_encode(trim($ArDato['NOMBRES'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }                                             
                            elseif ($Operacion=="bthorario")
                            {   $horario=1;
                                foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($horario,3,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['HOREJE']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['PERMISO']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['DIASEMANA']." ".$ArDato['FECHA']->format('d/m/Y')." ".$ArDato['INICIO']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['FECHA']->format('d/m/Y'));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['INICIO']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['NUMHORAS']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['AUTH']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['GRPEST']);
                                        $Color = "black";
                                        if (!$ArDato['PERMISO'])
                                        {   $Color = ($ArDato['HOREJE']=="S" ? "blue":"red");
                                        }
                                        $Grid->CreaSearchRowsDetalle ($id,$Color);
                                        $horario++;
                                }
                            }      
                            elseif ($Operacion=="Asignar")
                            {   if ($Contenido[0])
                                {   $count = 1;
                                    foreach ($Contenido[1] as $ArDato)
                                    {   $id = $ArDato['CEDULA']; 
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CEDULA']);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['ESTUDIANTE'])));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['CABECERA']);
                                        $Grid->CreaSearchCellsDetalle($id,'',($ArDato['PERMISO']!=0 ? "✔":"")); //Si existe la cedula del estudiante permiso detalle debe marcar el visto caso contrario nell
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                        $count++;
                                    }        
                                    return $Grid->CreaSearchTableDetalle();

                                }

                            }                                       
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
                function CrearModalObservacion($Datos)
                {       $Col1="20%"; $Col2="80%"; 
                               
                        $HTML = '<table border="0" class="Form-Frame" cellpadding="4">';
                        $HTML.= '       <tr height="60px">';
                        $HTML.= '           <td class="Form-Label-Center" colspan=2 valign="top" style="padding-top:5px;width:'.$Col2.'">';
                        $HTML.= '               <p class="p-txt-label" style="color:#000;" align="justify">DOCENTE: '.$Datos->txdocente.'.</p>'; 
                        $HTML.= '               </br>';      
                        $HTML.= '               <p class="p-txt-label" style="color:#000;" align="justify">MAESTRIA: '.$Datos->txmaestria.'.</p>';
                        $HTML.= '               </br>';
                        $HTML.= '               <p class="p-txt-label" style="color:#000;" align="justify">MATERIA: '.$Datos->txmateria.'.</p>';
                        $HTML.= '               </br>';
                        $HTML.= '               <p class="p-txt-label" style="color:#000;" align="justify">FECHA: '.$Datos->Fecha.'</p>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="20px">';
                        $HTML.= '           <td class="Form-Label-Center" valign="top" style="padding-top:5px;width:'.$Col1.'">';
                        $HTML.= '               <p class="p-txt-label" style="color:#000;">OFICIO<br/>O</br> QUIPUX</p>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <textarea class="txt-upper" style="width: 97%; height: 50px; resize: none;" id="observacion" name="observación" maxlength="250"></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>'; 
                        $HTML.= '      <tr height="40px">';
                        $HTML.= '          <td class="Form-Label-Center" colspan="2">';
                        $HTML.= '              <input type="button" id="btaplica" class="p-txt-label" style="height: 35px;text-decoration: none;font-weight: 600;font-size: 15px;color: #ffffff;background-color: #253140;border-radius: 6px;border: 2px solid #FFF;margin-top:10px;" value="✔ Aceptar" onclick=" return Guardar(observacion); ">';
                        $HTML.= '          </td>';
                        $HTML.= '      </tr>';
                        $HTML.= '</table>';
                        return $HTML;
                }
                
                function CreaWorkGrid($Ajax,$Capa)                
                {       $WorkGrid["Capa"] = $Capa;
                        $WorkGrid["Hcab"] = "40";
                        $WorkGrid["Hcob"] = "";
                        $WorkGrid["Hdet"] = "250";
                        $WorkGrid["Sufy"] = "";
                        $WorkGrid["Name"] = "FX";
                        $WorkGrid["Hrow"] = "24";
                        $WorkGrid["Erow"] = false;
                        $WorkGrid["btEd"] = true;
                        $WorkGrid["btAsg"] = true;
                        $WorkGrid["Cell"] = $this->WorkGridCellValues();
                        $Ajax->call("CreaWorkGrid",json_encode($WorkGrid));
                        return $Ajax; 
                } 
                
                private function WorkGridCellValues()
                {       $Columns[0] = array(0,'left','id','');
                        $Columns[1] = array(50,'center','No','');
                        $Columns[2] = array(0,'left','DiaSemana','');
                        $Columns[3] = array(150,'center','Fecha','');  
                        $Columns[4] = array(0,'left','Inicio','');           
                        $Columns[5] = array(250,'left','Referencia','');
                        $Columns[6] = array(0,'left','idEstado','');
                        $Columns[7] = array(100,'center','Estado','');
                        return $Columns;
                }
                
                function MuestraJustificarAsistencia($Ajax,$Datos)
                {       $num = 1;
                        if ($Datos[0])
                        {   foreach ($Datos[1] as $Dato)
                                {       $Celda[0] = $Dato['ID'];
                                        $Celda[1] = $num;
                                        $FechaDB  = $Dato['FECHA'];
                                        $Celda[2] = $Dato['DIASEMANA']." ".$FechaDB->format('d/m/Y')." ".$Dato['INICIO'];                                        
                                        $Celda[3] = $FechaDB->format('d/m/Y');
                                        $Celda[4] = $Dato['INICIO'];
                                        $Celda[5] = utf8_encode($Dato['OBSERVACION']);
                                        $Celda[6] = $Dato['IDESTADO'];
                                        $Celda[7] = ($Dato['IDESTADO'] == 4 ? "SOLICITADO" : "PROCESADO");
                                        $Ajax->call("CreaWorkGridFila","FX",$Celda);
                                        $num++;
                                }
                        }
                        return $Ajax;
                }
                
        }
?>

