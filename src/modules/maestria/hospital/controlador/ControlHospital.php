<?php
require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
require_once("src/rules/maestria/servicio/ServicioHospital.php");
require_once("src/modules/maestria/hospital/render/RenderHospital.php");

class ControlHospital
{   private  $ServicioHospital;
	private  $RenderHospital;

	function __construct()
	{       $this->ServicioHospital = new ServicioHospital();
			$this->RenderHospital = new RenderHospital();
	}

	function CargaBarButton($Opcion)
	{       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
			$Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
			if ($Datos[0])
			echo $this->RenderHospital->CreaBarButton($Datos[1]);
	}
	
	function CargaMantenimiento()
	{       echo $this->RenderHospital->CreaMantenimiento();
	}

	function CargaSearchGrid()
	{       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
			$Datos = $this->ServicioHospital->BuscarHospitalByDescripcion($prepareDQL);
			echo $this->RenderHospital->CreaSearchGrid($Datos);
	}         
	
	function ConsultaByID($id)
	{       $ajaxRespon = new xajaxResponse(); 
			$Datos = $this->ServicioHospital->BuscarHospitalByID($id);
			return $this->RenderHospital->MuestraFormulario($ajaxRespon,$Datos);
	}

	function ConsultaByTX($Form)
	{       $ajaxRespon = new xajaxResponse();   
			$JsDatos = json_decode($Form)->Forma;           
			$prepareDQL = array('texto' => strtoupper(trim($JsDatos->FindTextBx)));
			$ArDatos = $this->ServicioHospital->BuscarHospitalByDescripcion($prepareDQL);
			return $this->RenderHospital->MuestraGrid($ajaxRespon,$ArDatos);
	}

	function Guarda($Form)
	{       $ajaxRespon = new xajaxResponse();
			$JsDatos = json_decode($Form)->Forma;
			$funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");   
			$id = $this->ServicioHospital->GuardaDBHospital($JsDatos);
			$ArDatos = $this->ServicioHospital->BuscarHospitalByIDGuardado($id);
			return $this->RenderHospital->{$funcion}($ajaxRespon,$ArDatos);
	}

	function Elimina($Form)
	{       $ajaxRespon = new xajaxResponse();
			$JsData = json_decode($Form)->Forma;
			$JsData->estado = $this->ServicioHospital->DesactivaHospital($JsData->id);
			return $this->RenderHospital->MuestraEliminado($ajaxRespon,$JsData);
	}
}       
?>