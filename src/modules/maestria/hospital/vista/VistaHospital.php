<?php   $Opcion = 'Hospital';

require_once('src/modules/maestria/hospital/controlador/ControlHospital.php');
$ControlHospital = new ControlHospital();

$xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlHospital,'Guarda'));
$xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlHospital,'Elimina'));
$xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlHospital,'ConsultaByID'));
$xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlHospital,'ConsultaByTX'));

$xajax->processRequest();       
?>
<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <?php $xajax->printJavascript(); 
            require_once('src/utils/links.php');
    ?>
    <script type="text/javascript" src="src/modules/maestria/hospital/js/jsHospital.js?1"></script>
</head>
    <body>
        
    <div class="FormBasic" style="width:510px">
        <div class="FormSectionMenu">              
        <?php   $ControlHospital->CargaBarButton($_GET['opcion']);
                echo '<script type="text/javascript">';
                echo '        BarButtonState("Inactive"); ';
                echo '</script>';                    
        ?>    
        </div>  
        
        <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Opcion; ?>">
                <?php  $ControlHospital->CargaMantenimiento();  ?>
                </form>
        </div> 
        
        <div class="FormSectionGrid">          
        <?php   $ControlHospital->CargaSearchGrid();  ?>
        </div>  
    </div>
    <script>BarButtonState("Default");ElementStatus("id","");</script>
    </body>
</html>