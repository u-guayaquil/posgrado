<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        
        class RenderMatContenido
        {       private $Width;
                private $tpniv;
                
                function __construct()
                {       
                }
        
                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();    
                        return $BarButton->CreaBarButton($Buttons);
                }
            
                function CreaMantenimiento()
                {       $Search = new SearchInput();
                        $Col1="20%"; $Col2="27%"; $Col3="27%"; $Col4="13%"; $Col5="13%";

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" colspan="2">';
                        $HTML.= '               <input type="hidden" id="id" name="id" value=""/>';
                        $HTML.= '               <input type="hidden" id="idmaesvers" name="idmaesvers" value=""/>';
                        $HTML.=                 $Search->TextSch("maestria","","")->Enabled(true)->Create("t18");                
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">Cohorte</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">';
                        $HTML.= '               <input type="hidden" id="idcohorte" name="idcohorte" value=""/>';
                        $HTML.= '               <input type="text" class="txt-input t04" id="txcohorte" name="txcohorte" value="" maxlength="4" ReadOnly/>';                       
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Materia</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'" colspan="2">';
                        $HTML.=                 $Search->TextSch("matcont","","")->Enabled(true)->Create("t18");
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">Estado</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">';
                        $HTML.=                 $this->CreaComboEstadoByArray(array(0,1));
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'; padding-top:5px" valign="top" rowspan="2">Descripción del item</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'" rowspan="2" colspan="2">';
                        $HTML.= '               <textarea class="txt-upper" style="width: 90%; height: 42px; resize: none;" id="descripcion" name="descripcion" maxlength="500" disabled></textarea>';                  
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">Nivel del item</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">';
                        $HTML.= '               <select class="sel-input s04" id="idtipo" name="idtipo" onchange=" return ControlIdTipo(this)" disabled>';
                        $HTML.= '                       <option value="0">UNIDAD</option>';
                        $HTML.= '                       <option value="1">TEMA</option>';    
                        $HTML.= '                       <option value="2">SUBTEMA</option>';    
                        $HTML.= '               </select>';    
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';

                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">Indice del item</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col5.'">';
                        $HTML.= '               <input type="text" class="txt-input t04" id="orden" name="orden" value="" maxlength="4" onKeyDown="return soloNumeros(event); " disabled/>';                       
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';           
                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Unidad/Tema del item</td>';
                        $HTML.= '           <td class="Form-Label" colspan="5">';
                        $HTML.=                 $Search->TextSch("padre","","")->Enabled(false)->Create("t18");
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                        
                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col1.'"></td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col2.'">Resultado del aprendizaje</td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col3.'">Métodos, Técnicas e Instrumentos</td>';
                        $HTML.= '           <td class="Form-Label-Center" colspan="2">Objetivo de aprendizaje</td>';                        
                        $HTML.= '       </tr>';
                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <textarea class="txt-upper" style="width: 96%; height: 50px; resize: none;" id="resultado" name="resultado" maxlength="1000" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">';
                        $HTML.= '               <textarea class="txt-upper" style="width: 96%; height: 50px; resize: none;" id="metodos" name="metodos" maxlength="1000" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" colspan="2">';                                                
                        $HTML.= '               <textarea class="txt-upper" style="width: 96%; height: 50px; resize: none;" id="objaprende" name="objaprende" maxlength="1000" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';

                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Evaluación de los aprendizajes</td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col2.'">Componente de docencia (CD)</td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col3.'">Componente Práctico (CPE)</td>';
                        $HTML.= '           <td class="Form-Label-Center" colspan="2">Componente Autónomo (CA)</td>';                        
                        $HTML.= '       </tr>';                 
                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'"></td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <textarea class="txt-upper" style="width:96%; height:50px; resize: none;" id="cd" name="cd" maxlength="500" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">';
                        $HTML.= '               <textarea class="txt-upper" style="width:96%; height:50px; resize: none;" id="cp" name="cp" maxlength="500" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" colspan="2">';
                        $HTML.= '               <textarea class="txt-upper" style="width:96%; height:50px; resize: none;" id="ca" name="ca" maxlength="500" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        
                        return $HTML.'</table>';
                }
            
                private function SearchGridConfig()
                {       $Columns['Id']       = array('60px','center','');
                        $Columns['Tipo']     = array('0px','left','none');
                        $Columns['Ind']      = array('60px','center','');
                        $Columns['Contenido']= array('650px','justify','');
                        $Columns['Resultado aprendizaje'] = array('105px','','none');
                        $Columns['Métodos Técnicas']      = array('105px','','none');
                        $Columns['Objetivos aprendizaje'] = array('105px','','none');
                        $Columns['Evaluación de los aprendizaje CD']   = array('140px','left','none');
                        $Columns['Evaluación de los aprendizaje CPE']  = array('140px','left','none');
                        $Columns['Evaluación de los aprendizaje CA']   = array('140px','left','none');
                        $Columns['IdPadre']  = array('0px','center','none');
                        $Columns['TxPadre']  = array('0px','center','none');
                        $Columns['IdEstado'] = array('0px','center','none');
                        $Columns['TxDescrip']= array('0px','center','none');
                        return array('AltoCab' => '35px','DatoCab' => $Columns,'AltoDet' => '250px','AltoRow' => '20px','FindTxt' =>false);
                }

                function CreaSearchGrid($jsData)
                {       $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$jsData);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML[1]);
                }

                function GridDataHTML($grid,$root,$nivel=0)
                {       $count = 0;
                        foreach($root as $sheet)
                        {       $count++;
                                $id = str_pad($sheet->id,MAESCON,'0',STR_PAD_LEFT);
                                $grid->CreaSearchCellsDetalle($id,'',$id);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->idtipo);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->orden);
                                $dato = $this->FormatGridData($sheet->nivel,$sheet->idtipo,$sheet->descripcion);        
                                $grid->CreaSearchCellsDetalle($id,$dato['margen'],$dato['texto']);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->resultado);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->metodo);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->objetivo);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->cd);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->cp);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->ca);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->idpadre);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->txpadre);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->estado);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->descripcion);
                                $grid->CreaSearchRowsDetalle ($id,($sheet->estado==0 ? "red" : "")); 
                                if ($sheet->idtipo==0 || $sheet->idtipo==1)
                                $this->GridDataHTML($grid,$sheet->hijos,$nivel+1);
                        }
                        if ($nivel==0)
                        return array($count,$grid->CreaSearchTableDetalle(0,""));
                }

                function MuestraGrid($Ajax,$mxnvl,$Contenido)
                {       $this->tpniv = $mxnvl;
                        $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Contenido);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML[1]);
                        $Ajax->call("BarButtonState","Default");
                        if($mxnvl == "")
                        {       return $this->Respuesta($Ajax,"CMD_WRM_CLS","No existe definido contenido temático.");  }
                        else{
                                if ($GrdDataHTML[0]==0)
                                {       return $this->Respuesta($Ajax,"CMD_INF","No existe definido contenido temático.");  }
                        } 
                        return $Ajax;
                }        
                
                private function FormatGridData($level,$types,$description)
                {       $Margen = $level*20;
                        if ($types==0)
                            $Texto = ($this->tpniv>0 ? "<b>".$description."</b>" : $description);
                        else if ($types==1)
                            $Texto = "<div style='padding-right:10px; padding-left:".$Margen."px'>".($this->tpniv>1 ? "<b>".$description."</b>" : $description)."</div>";
                        else if($types==2)
                            $Texto = "<div style='padding-right:10px; padding-left:".$Margen."px'>".$description."</div>";
                        return array('texto' => $Texto, 'margen' => 0);
                }

                function MuestraGuardado($Ajax,$MaxNivel,$Contenido)
                {       $this->tpniv = $MaxNivel;
                        $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Contenido);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML[1]);
                        return $Ajax;   
                }
                
                function MuestraEliminada($Ajax,$mxnvl,$Contenido)
                {       $this->tpniv = $mxnvl;
                        $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Contenido);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML[1]);
                        return $this->Respuesta($Ajax,"CMD_DEL",0);
                }

                function Respuesta($Ajax,$Tipo,$Data)
                {     
                        $Ajax->call("XAJAXResponse",$Tipo,$Data);
                        return $Ajax;
                }

                private function CreaComboEstadoByArray($IdArray)
                {       $Select = new ComboBox();
                        $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $Select->Combo("estado",$Datos)->Fields("id","descripcion")->Selected(1)->Create("s04");
                }        

                /*** MODAL ***/
                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']    =array('30px','center','');
                            $Columns['Maestria']=array('250px','left','');
                            $Columns['V.ID']    =array('0px','left','none');
                            $Columns['Version'] =array('50px','left','');
                            $Columns['C.ID']    =array('0px','left','none');
                            $Columns['Cohorte'] =array('100px','left','');
                            $Columns['Facultad']=array('250px','left','');
                        }
                        elseif ($Operacion=="btmatcont")
                        {   $Columns['Id']     =array('0px','center','none');
                            $Columns['M.ID']   =array('40px','center','');
                            $Columns['Materia']=array('350px','left','');
                            $Columns['Orden']=array('60px','center','');
                            $Columns['Nivcont']=array('0px','center','none');
                        }    
                        elseif ($Operacion=="btpadre")
                        {   $Columns['Id']     =array('0px','center','none');
                            $Columns['Ind']  =array('50px','center','');
                            $Columns['Tipo']   =array('60px','center','none');
                            $Columns['Descripcion']=array('500px','justify','');
                            $Columns['Sbdescrip']=array('0px','left','none');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '250px','AltoRow' => '20px','FindTxt'=>false);
                        }
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btpadre")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['ID'],MAESCON,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['ORDEN']));
                                        if ($ArDato['IDTIPOPCION']==0)
                                        {   //$Tipo = "UNIDAD";
                                            $Text = "<b>".$ArDato['DESCRIPCION']."</b>";
                                        }
                                        else
                                        {   //$Tipo = "TEMA";
                                            $Text = "<div style='padding-right:10px; padding-left:20px'>".$ArDato['DESCRIPCION']."<div>";
                                        }
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDTIPOPCION']);
                                        $Grid->CreaSearchCellsDetalle($id,'',$Text);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['DESCRIPCION']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmatcont")
                            {   $Nivel = -1;
                                foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['ID'],MAESCON,'0',STR_PAD_LEFT);
                                        $Mid = str_pad($ArDato['MID'],MATERIA,'0',STR_PAD_LEFT);
                                        $niO = str_pad($ArDato['NIVELORDEN'],2,'0',STR_PAD_LEFT);
                                        if ($ArDato['NIVEL']!=$Nivel)
                                        {   $Nivel = $ArDato['NIVEL'];
                                            $Grid->CreaSearchCellsDetalle($id,'','');
                                            $Grid->CreaSearchCellsDetalle($id,'','');
                                            $Grid->CreaSearchCellsDetalle($id,'',"<b> CICLO ".$Nivel."</b>",2,0);
                                            $Grid->CreaSearchRowsDetalle ($id,"",0);
                                        }   
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',$Mid);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'','M: '.$niO);
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['NIVCONT']);
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = str_pad($ArDato['IDM'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $idV = $ArDato['IDV'];
                                        $idC = $ArDato['IDC'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idV);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['TXVERSION'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idC);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['COHORTE'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['FACULTAD'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
        }
?>

