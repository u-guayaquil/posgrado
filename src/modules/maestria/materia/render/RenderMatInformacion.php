<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        
        class RenderMatInformacion
        {       private $Width;

                function __construct()
                {     
                }
        
                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();    
                        return $BarButton->CreaBarButton($Buttons);
                }
            
                function CreaMantenimiento($InfoAdd)
                {       $Search = new SearchInput();
                        $Col1="28%"; $Col2="30%"; $Col3="28%"; $Col4="14%";
                        
                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">';
                        $HTML.= '               Maestria';
                        $HTML.= '               <input type="hidden" id="idversion" name="idversion" value=""/>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $Search->TextSch("maestria","","")->Enabled(true)->Create("t10");                         
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" colspan="2">HORAS DE APRENDIZAJE POR COMPONENTE</td>';
                        $HTML.= '       </tr>';                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Materia (malla)</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $Search->TextSch("materia","","")->Enabled(true)->Create("t10");                         
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Componente de Docencia (CD)</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                        $HTML.= '               <input type="text" class="txt-input t02" id="componentecd" name="componentecd" value="" maxlength="2" onKeyDown="return soloNumeros(event); " disabled/>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Prerequisito Académico</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="text" class="txt-upper t11" id="requisito" name="requisito" value="" maxlength="50" disabled/>';
                        $HTML.= '           </td>';                      
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Componente Práctico/Experimental (CPE)</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                        $HTML.= '               <input type="text" class="txt-input t02" id="componentecp" name="componentecp" value="" maxlength="2" onKeyDown="return soloNumeros(event); " disabled/>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Unidad de Organización Curricular</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <input type="text" class="txt-upper t11" id="orgcurricular" name="orgcurricular" value="" maxlength="50" disabled/>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Componente Autónomo (CA)</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                        $HTML.= '               <input type="text" class="txt-input t02" id="componenteca" name="componenteca" value="" maxlength="3" onKeyDown="return soloNumeros(event); " disabled/>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';    
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Nivel de contenido / Planificación</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <select class="sel-input s05" id="nivcon" name="nivcon" disabled>';
                        $HTML.= '                       <option value="0">UNIDAD</option>';
                        $HTML.= '                       <option value="1" SELECTED>TEMA</option>';    
                        $HTML.= '                       <option value="2">SUBTEMA</option>';    
                        $HTML.= '               </select>';
                        $HTML.= '               &nbsp;/&nbsp;<select class="sel-input s05" id="nivpln" name="nivpln" disabled>';
                        $HTML.= '                       <option value="0">UNIDAD</option>';
                        $HTML.= '                       <option value="1" SELECTED>TEMA</option>';    
                        $HTML.= '                       <option value="2">SUBTEMA</option>';    
                        $HTML.= '               </select>';                            
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'"></td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col4.'">';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';    
                        $HTML.= '       <tr height="35px">';
                        $HTML.= '           <td class="Form-Label-Center" colspan="4">JUSTIFICACION</td>';
                        $HTML.= '       </tr>';                                             
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="padding-top:5px; width:'.$Col1.'" valign="top">Objetivo General</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                        $HTML.= '               <textarea class="txt-upper" style="width: 99%; height: 40px; resize: none;" id="obgeneral" name="obgeneral" maxlength="1000" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                        
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="padding-top:5px; width:'.$Col1.'" valign="top">Objetivo Especifico</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                        $HTML.= '               <textarea class="txt-upper" style="width: 99%; height: 50px; resize: none;" id="obespecifico" name="obespecifico" maxlength="1000" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';                          
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Metodología</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                     // $HTML.= '               <input type="text" class="txt-upper" style="width: 99%" id="metodologia" name="metodologia" value="" maxlength="200" disabled/>';
                        $HTML.= '               <textarea class="txt-upper" style="width: 99%; height: 40px; resize: none;" id="metodologia" name="metodologia" maxlength="500" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Líneas de Investigación a la que Tributa</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                      //$HTML.= '               <input type="text" class="txt-upper" style="width: 99%" id="metodologia" name="metodologia" value="" maxlength="200" disabled/>';
                        $HTML.= '               <textarea class="txt-upper" style="width: 99%; height: 40px; resize: none;" id="linea" name="linea" maxlength="500" disabled></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';  
                        $HTML.= '       <tr height="35px">';
                        $HTML.= '           <td class="Form-Label-Center" colspan="4">BIBLIOGRAFIA</td>';
                        $HTML.= '       </tr>';   
                        $HTML.= '       <tr>';
                        $HTML.= '           <td class="Form-Label" colspan="4">';
                        $HTML.= '               <table border=0 class="Form-Frame" cellpadding="0" style="width:980px">';
                        $HTML.= '                      <tr height="28px">';
                        $HTML.= '                          <td class="Form-Label-Center" colspan="3">BASICA</td>';
                        $HTML.= '                          <td class="Form-Label-Center" colspan="3">COMPLEMENTARIA</td>';
                        $HTML.= '                      </tr>';  
                        $HTML.= '                      <tr height="40px">';
                        $HTML.= '                          <td class="Form-Label-Center" style="width:370px">Título del Libro</td>';
                        $HTML.= '                          <td class="Form-Label-Center" style="width:60px">Año<br>Edición</td>';
                        $HTML.= '                          <td class="Form-Label-Center" style="width:60px">No.<br>Ejemplar</td>';
                        $HTML.= '                          <td class="Form-Label-Center" style="width:370px">Título del Libro</td>';
                        $HTML.= '                          <td class="Form-Label-Center" style="width:60px">Año<br>Edición</td>';
                        $HTML.= '                          <td class="Form-Label-Center" style="width:60px">No.<br>Ejemplar</td>';
                        $HTML.= '                      </tr>';  
                        $HTML.=                        $this->EjecutaLibros($InfoAdd,"Bas","Com");
                        $HTML.= '                      <tr height="28px">';
                        $HTML.= '                          <td class="Form-Label-Center" colspan="6">SITIOS WEB</td>';
                        $HTML.= '                      </tr>';      
                        $HTML.= '                      <tr height="40px">';
                        $HTML.= '                          <td class="Form-Label-Center" colspan="5">Dirección Web/Libro Electrónico</td>';
                        $HTML.= '                          <td class="Form-Label-Center" style="width:60px">Año<br>Edición</td>';
                        $HTML.= '                      </tr>';  
                        $HTML.=                        $this->EjecutaEnlaces($InfoAdd,"Lnk");
                        $HTML.= '               </table>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        return $HTML.'</table>';
                }
            
                private function EjecutaLibros($InfoAdd,$Nomb1,$Nomb2)
                {       $HTML = '';
                        for ($i=1;$i<=$InfoAdd;$i++)
                        {    $HTML.= '  <tr height="28px">';
                             $HTML.= '      <td class="Form-Label">';
                             $HTML.= '          <input type="text" class="txt-upper" style="width:360px" id="'.$Nomb1.'name'.$i.'" name="'.$Nomb1.'name'.$i.'" value="" maxlength="200" disabled/>';
                             $HTML.= '      </td>';
                             $HTML.= '      <td class="Form-Label">';
                             $HTML.= '          <input type="text" class="txt-upper" style="width:50px"  id="'.$Nomb1.'anio'.$i.'" name="'.$Nomb1.'anio'.$i.'" value="" maxlength="4" onKeyDown="return soloNumeros(event); "  disabled/>';
                             $HTML.= '      </td>';
                             $HTML.= '      <td class="Form-Label">';
                             $HTML.= '          <input type="text" class="txt-input" style="width:50px" id="'.$Nomb1.'nume'.$i.'" name="'.$Nomb1.'nume'.$i.'" value="" maxlength="2" onKeyDown="return soloNumeros(event); " disabled/>';
                             $HTML.= '      </td>';
                             $HTML.= '      <td class="Form-Label">';
                             $HTML.= '          <input type="text" class="txt-upper" style="width:360px" id="'.$Nomb2.'name'.$i.'" name="'.$Nomb2.'name'.$i.'" value="" maxlength="200"  disabled/>';
                             $HTML.= '      </td>';
                             $HTML.= '      <td class="Form-Label">';
                             $HTML.= '          <input type="text" class="txt-upper" style="width:50px" id="'.$Nomb2.'anio'.$i.'" name="'.$Nomb2.'anio'.$i.'" value="" maxlength="4" onKeyDown="return soloNumeros(event); " disabled/>';
                             $HTML.= '      </td>';
                             $HTML.= '      <td class="Form-Label">';
                             $HTML.= '          <input type="text" class="txt-input" style="width:50px" id="'.$Nomb2.'nume'.$i.'" name="'.$Nomb2.'nume'.$i.'" value="" maxlength="2" onKeyDown="return soloNumeros(event); " disabled/>';
                             $HTML.= '      </td>';
                             $HTML.= '  </tr>';
                        }
                        return $HTML;
                }        

                private function EjecutaEnlaces($InfoAdd,$Nomb1)
                {       $HTML = '';
                        for ($i=1;$i<=$InfoAdd;$i++)
                        {    $HTML.= '  <tr height="28px">';
                             $HTML.= '      <td class="Form-Label" colspan="5">';
                             $HTML.= '          <input type="text" class="txt-input" style="width:910px" id="'.$Nomb1.'name'.$i.'" name="'.$Nomb1.'name'.$i.'" value="" maxlength="500" disabled/>';
                             $HTML.= '      </td>';
                             $HTML.= '      <td class="Form-Label">';
                             $HTML.= '          <input type="text" class="txt-upper" style="width:50px"  id="'.$Nomb1.'anio'.$i.'" name="'.$Nomb1.'anio'.$i.'" value="" maxlength="4"  onKeyDown="return soloNumeros(event); " disabled/>';
                             $HTML.= '      </td>';
                             $HTML.= '  </tr>';
                        }
                        return $HTML;
                }        
            
                function MuestraFormulario($Ajax,$ArDatos,$SbDatos)
                {       $Utils = new Utils();
                        if ($ArDatos[0])
                        {   $ArDato = $ArDatos[1][0];
                            
                            $Ajax->Assign("requisito","value", utf8_encode($Utils->ConvertTildesToUpper($ArDato['REQUISITO'])));
                            $Ajax->Assign("orgcurricular","value", utf8_encode($Utils->ConvertTildesToUpper($ArDato['ORGCURRICULAR'])));
                            
                            $Ajax->Assign("componentecd","value", $ArDato['COMPONENTECD']);
                            $Ajax->Assign("componentecp","value", $ArDato['COMPONENTECP']);
                            $Ajax->Assign("componenteca","value", $ArDato['COMPONENTECA']);
                          //$Ajax->Assign("totalhoras","value", $ArDato['COMPONENTECD']+$ArDato['COMPONENTECP']+$ArDato['COMPONENTECA']);

                            $Ajax->Assign("obgeneral","value", utf8_encode($Utils->ConvertTildesToUpper($ArDato['OBGENERAL'])));
                            $Ajax->Assign("obespecifico","value", utf8_encode($Utils->ConvertTildesToUpper($ArDato['OBESPECIFICO'])));
                            $Ajax->Assign("metodologia","value", utf8_encode($Utils->ConvertTildesToUpper($ArDato['METODOLOGIA'])));
                            $Ajax->Assign("linea","value", utf8_encode($Utils->ConvertTildesToUpper($ArDato['LINEA'])));
                            $Ajax->Assign("nivcon","value", $ArDato['NIVCONT']);
                            $Ajax->Assign("nivpln","value", $ArDato['NIVPLAN']);
                            $j=0;
                            $k=0;
                            $z=0;
                            
                            if ($SbDatos[0])
                            {   foreach($SbDatos[1] as $SbDato)
                                {       if ($SbDato['TIPO']=="BAS")
                                        {   $j++;
                                            $Ajax->Assign("Basname".$j,"value", utf8_encode($Utils->ConvertTildesToUpper($SbDato['LIBRO'])));
                                            $Ajax->Assign("Basanio".$j,"value", $SbDato['EDICION']);
                                          //$Ajax->Assign("Basexis".$j,"value", utf8_encode($SbDato['EXISTENBIBLIOTECA']));
                                            $Ajax->Assign("Basnume".$j,"value", $SbDato['EJEMPLARES']);
                                        }
                                        else if ($SbDato['TIPO']=="COM")
                                        {   $k++;
                                            $Ajax->Assign("Comname".$k,"value", utf8_encode($Utils->ConvertTildesToUpper($SbDato['LIBRO'])));
                                            $Ajax->Assign("Comanio".$k,"value", $SbDato['EDICION']);
                                          //$Ajax->Assign("Comexis".$k,"value", utf8_encode($SbDato['EXISTENBIBLIOTECA']));
                                            $Ajax->Assign("Comnume".$k,"value", $SbDato['EJEMPLARES']);
                                        }
                                        else
                                        {   $z++;
                                            $Ajax->Assign("Lnkname".$z,"value", utf8_encode($Utils->ConvertTildesToUpper($SbDato['LIBRO'])));
                                            $Ajax->Assign("Lnkanio".$z,"value", $SbDato['EDICION']);
                                        }    
                                }
                            }
                            $Ajax->call("BarButtonState","Update");
                            return $Ajax;
                        }
                        $Ajax->call("BarButtonState","Default");
                        return $this->Respuesta($Ajax,"CMD_WRM",$ArDatos[1]);
                }

                function MuestraGuardado($Ajax,$id)
                {       return $this->Respuesta($Ajax,"CMD_SAV",$id);  
                }
                
                function Respuesta($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse";
                        $Ajax->call($jsFuncion,$Tipo,$Data);
                        return $Ajax;
                }

                private function CreaComboEstadoByArray($IdArray)
                {       $Select = new ComboBox();
                        $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $Select->Combo("estado",$Datos)->Fields("id","descripcion")->Selected(1)->Create("s04");
                }        

                /*** MODAL ***/
                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btmaestria")
                        {   $Columns['MID']     = array('35px','center','');
                            $Columns['Maestria']  = array('320px','left','');
                            $Columns['VID']  =  array('0px','left','none');
                            $Columns['Ver']  =  array('30px','left','');
                            $Columns['Facultad'] = array('250px','left','');
                        }
                        elseif ($Operacion=="btmateria")
                        {   $Columns['MID']     = array('35px','center','');
                            $Columns['Materia']= array('300px','left','');
                            $Columns['Estado']= array('60px','left','none');
                            $Columns['Orden']= array('60px','center','');
                        }    
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$ArDatos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$ArDatos,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid; 
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
                
                private function GridDataHTMLModal($Grid,$ArDatos,$Operacion)
                {       if ($ArDatos[0])
                        {   if ($Operacion=="btmaestria")
                            {   foreach ($ArDatos[1] as $ArDato)
                                {       $idM = str_pad($ArDato['IDM'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $idV = $ArDato['IDV'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode($ArDato['MAESTRIA']));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idV);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode($ArDato['TXVERSION']));
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode($ArDato['FACULTAD']));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }
                            elseif ($Operacion=="btmateria") 
                            {   $Nivel = -1;
                                foreach ($ArDatos[1] as $ArDato)
                                {       $idM = str_pad($ArDato['ID'],MATERIA,'0',STR_PAD_LEFT);
                                        $niO = str_pad($ArDato['NIVELORDEN'],2,'0',STR_PAD_LEFT);
                                        if ($ArDato['NIVEL']!=$Nivel)
                                        {   $Nivel = $ArDato['NIVEL'];
                                            $Grid->CreaSearchCellsDetalle($idM,'','');
                                            $Grid->CreaSearchCellsDetalle($idM,'',"<b> CICLO ".$Nivel."</b>",2,0);
                                            $Grid->CreaSearchRowsDetalle ($idM,"",0);
                                        }   
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode($ArDato['ESTADO']));
                                        $Grid->CreaSearchCellsDetalle($idM,'','M: '.$niO);
                                        $Grid->CreaSearchRowsDetalle ($idM,($ArDato['IDESTADO']==1 ? "black":"red"));
                                }
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }
        }
?>

