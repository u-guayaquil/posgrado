<?php   $Opcion = 'Materia';

        require_once('src/modules/maestria/materia/controlador/ControlMateria.php');
        $ControlMateria = new ControlMateria();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlMateria,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlMateria,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlMateria,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlMateria,'ConsultaByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlMateria,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlMateria,'ConsultaModalGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('MostrarbyMaestria', $ControlMateria,'MostrarMateriabyMaestria'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTXByMaestria', $ControlMateria,'ConsultaByTXByMaestria'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByIDByMaestria', $ControlMateria,'ConsultaByIDByMaestria'));
        
        $xajax->processRequest();       
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
            
            var objmateria = "id:descripcion:estado:txmaestria:idmaestria";
            var valmateria = "txmaestria:descripcion";
                
                function ButtonClick(Operacion)
                {       var objmateria = "id:descripcion:estado:txmaestria:idmaestria";
                        var valmateria = "txmaestria:descripcion";
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementStatus("descripcion","id:txmaestria:btmaestria");
                            ElementClear("id:descripcion");
                            ElementSetValue("estado",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Operacion);
                                if (ElementGetValue("estado")==0)
                                {   BarButtonStateEnabled("btmaestria");
                                    valmateria = valmateria + ":estado";
                                }
                                ElementStatus(valmateria,"id:txmaestria:btmaestria");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue("estado")==1)
                                {   /*if (confirm("Desea inactivar este registro?"))
                                    {   var Forma = PrepareElements("id:estado");
                                        xajax_Elimina(JSON.stringify({Forma}));
                                    } */
                                    Swal.fire({
                                        title: 'Desactivar registro?',
                                        text: "Esta seguro que desea desactivar el registro?",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Si, estoy seguro!'
                                    }).then(function(result){
                                        if (result.value) {
                                            var Forma = PrepareElements("id:estado");
                                            xajax_Elimina(JSON.stringify({Forma}));
                                        }
                                    });   
                                }
                                else{SAlert("warning", "¡Cuidado!", "El registro se encuentra inactivo");}
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(valmateria))
                                {   if (BarButtonState("Inactive"))
                                    {   var Forma = PrepareElements(objmateria);
                                        xajax_Guarda(JSON.stringify({Forma}));
                                    }
                                }
                                else
                                {SAlert("warning", "¡Cuidado!", "Llene los campos por favor");}
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Operacion);
                                ElementStatus("id:btmaestria:txmaestria","descripcion:estado");
                                ElementClear("id:descripcion");
                                ElementSetValue("estado",1);
                        }
                        else 
                        {xajax_CargaModal(Operacion);}
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Default");
                            ElementSetValue("id",Datos);
                            ElementClear("id:descripcion");
                            ElementStatus("id:btmaestria:txmaestria","descripcion:estado");
                            ElementSetValue("estado",1);
                            SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
                        }
                        else if (Mensaje==="CMD_MAE")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                                BarButtonState("Default");
                                ElementStatus("id:btmaestria:txmaestria","descripcion:estado");
                                ElementClear("id:descripcion");
                                ElementSetValue("estado",1);
                            }
                            else if(ValidateStatusEnabled("addSave")){
                                BarButtonState("Active");
                                ElementStatus("descripcion","id:estado:txmaestria");
                            }
                            else {
                                BarButtonState("Default");
                                ElementStatus("id:btmaestria:txmaestria","descripcion:estado");
                                ElementClear("id:descripcion");
                                ElementSetValue("estado",1);
                            }
                        }
                        else if (Mensaje==="CMD_MAE_ERR")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                            }
                            else {
                                ElementStatus("descripcion","id:estado:txmaestria");
                            }
                        }
                        else if(Mensaje==='CMD_ERR')
                        {   BarButtonState("addNew");
                            SAlert("error", "¡Error!", Datos); 
                        }    
                        else if(Mensaje==='CMD_DEL')
                        {   ElementSetValue("estado",Datos);
                            SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
                        }
                        else if(Mensaje==="CMD_WRM")
                        {   BarButtonState("Default");
                            ElementClear("id");
                            ElementSetValue("estado",1);
                            SAlert("warning", "¡Cuidado!", Datos); 
                        }
                }
                
                function SearchByElement(Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id")
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion");
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState("Inactive"))
                                    {
                                        var Forma = PrepareElements("id:idmaestria");
                                        xajax_BuscaByIDByMaestria(JSON.stringify({Forma}));
                                    }
                                }
                            }
                            else
                            BarButtonState("Default"); 
                        }
                        else if (Elemento.id === "FindMaestriaTextBx")
                        {   
                            xajax_BuscaModalByTX("btmaestria",Elemento.value);
                        } 
                        else if(Elemento.id === "FindTextBx")
                        {
                            var Forma = PrepareElements("FindTextBx:idmaestria");
                            xajax_BuscaByTXByMaestria(JSON.stringify({Forma}));
                        }
                }
                
                function SearchMaestriaGetData(DatosGrid)
                {
                        document.getElementById("idmaestria").value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        document.getElementById("txmaestria").value = DatosGrid.cells[3].childNodes[0].nodeValue.toUpperCase();
                        cerrar();
                        ElementStatus("id:btmaestria:txmaestria","descripcion:estado");
                        ElementClear("id:descripcion");
                        ElementSetValue("estado",1);
                        var Forma = PrepareElements(objmateria);
                        xajax_MostrarbyMaestria(JSON.stringify({Forma}));
                        return false;
                }
                 
                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("descripcion").value = Grilla.cells[1].childNodes[0].nodeValue.toUpperCase();
                            document.getElementById("estado").value = Grilla.cells[3].childNodes[0].nodeValue; 
                            document.getElementById("txmaestria").value = Grilla.cells[5].childNodes[0].nodeValue.toUpperCase(); 
                            document.getElementById("idmaestria").value = Grilla.cells[6].childNodes[0].nodeValue; 
                        }
                        return false;
                }
                 
        </script>
    </head>
        <body>
            
        <div class="FormBasic" style="width:480px">
            <div class="FormSectionMenu">              
            <?php   $ControlMateria->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState("Inactive"); ';
                    echo '</script>';                    
            ?>    
            </div>  
            
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                    <?php  $ControlMateria->CargaMantenimiento();  ?>
                    </form>
            </div> 
            
            <div class="FormSectionGrid">          
            <?php   $ControlMateria->CargaSearchGrid();  ?>
            </div>  
            
            <script></script>
        </div>
        </body>
    </html>
         