<?php   $Opcion = 'MatInformacion';
        $Libros = 5;
        require_once('src/modules/maestria/materia/controlador/ControlMatInformacion.php');
        $ControlMatInformacion = new ControlMatInformacion();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlMatInformacion,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlMatInformacion,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlMatInformacion,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlMatInformacion,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlMatInformacion,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                var Accion = 1;
                var validar = "componentecd:componentecp:componenteca:requisito:orgcurricular:obgeneral:obespecifico:metodologia:linea";
                var selects = ":nivcon:nivpln";
                var Libros = 5;

                function BatchStatusElement(Habilitar)
                {       for (u=1; u<=Libros; u++)
                        {    if (Habilitar) 
                                 ElementStatus("Basname"+u+":Basanio"+u+":Basnume"+u+":Comname"+u+":Comanio"+u+":Comnume"+u+":Lnkname"+u+":Lnkanio"+u,"");
                             else
                             ElementStatus("","Basname"+u+":Basanio"+u+":Basnume"+u+":Comname"+u+":Comanio"+u+":Comnume"+u+":Lnkname"+u+":Lnkanio"+u);
                        }
                }
                function BatchClearElement()
                {       for (u=1; u<=Libros; u++)
                        {    ElementClear("Basname"+u+":Basanio"+u+":Basnume"+u+":Comname"+u+":Comanio"+u+":Comnume"+u+":Lnkname"+u+":Lnkanio"+u);
                        }
                }
                
                function BatchPrepElement()
                {       var concatena="";
                        for (u=1; u<=Libros; u++)
                        {    concatena = concatena+":Basname"+u+":Basanio"+u+":Basnume"+u+":Comname"+u+":Comanio"+u+":Comnume"+u+":Lnkname"+u+":Lnkanio"+u;
                        }
                        return concatena;
                }
                
                function ButtonClick(Operacion)
                {       if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementStatus(validar+selects,"btmaestria:txmaestria:btmateria:txmateria");
                            ElementSetValue("nivcon",0);
                            ElementSetValue("nivpln",0);
                            BatchStatusElement(true);
                        }
                        else if (Operacion==='addMod')
                        {       Accion = 2;
                                BarButtonState(Operacion);
                                ElementStatus(validar+selects,"btmaestria:txmaestria:btmateria:txmateria");
                                BatchStatusElement(true);
                        }
                        else if (Operacion==='addDel')
                        {       if (confirm("Desea inactivar este regsistro?"))
                                {   IDVER=ElementGetValue("idversion");
                                    IDMAT=ElementGetValue("idmateria");
                                    xajax_Elimina(IDVER,IDMAT);
                                }
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementGetValue("nivcon")>=1 && ElementGetValue("nivpln")>=1) 
                                {   guardar=validar+selects+":idversion:idmaestria:idmateria";
                                    if (ElementValidateBeforeSave(guardar))
                                    {   if (BarButtonState("Inactive"))
                                        {   var Formd = BatchPrepElement();
                                            var Forma = PrepareElements(guardar+Formd);
                                            xajax_Guarda(Accion,JSON.stringify({Forma}));
                                        }
                                    }
                                    else
                                    SAlert("error", "¡Campos Vacios!", "Falta ingresar datos.");
                                }                                
                                else
                                SAlert("error", "¡Error!", "El nivel de contenido y planificación, mínimo debe ser temático.");        
                        }
                        else if (Operacion==='addCan')
                        {       BatchStatusElement();
                                if (Accion == 1)
                                {   BarButtonState("Default");
                                    ElementSetValue("nivcon",0);
                                    ElementSetValue("nivpln",0);
                                    ElementClear(validar);
                                    BatchClearElement();    
                                }
                                else
                                BarButtonState("Update");
                                ElementStatus("btmaestria:txmaestria:btmateria:txmateria",validar+selects);    
                        }
                        else 
                        {   if (Operacion=="btmateria")
                            {   IDVER=ElementGetValue("idversion");
                                if (IDVER!="")    
                                    xajax_CargaModal(Operacion,IDVER);
                                else
                                    SAlert("warning", "¡Seleccionar Maestría!", "Seleccione una maestria.");
                            }
                            else
                            xajax_CargaModal(Operacion,0);
                        }
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   Accion = 2;
                            BarButtonState('Update');
                            ElementStatus("btmaestria:txmaestria:btmateria:txmateria",validar+selects);
                            BatchStatusElement(false);
                            SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
                        }
                        else if(Mensaje==='CMD_DEL')
                        {       Accion = 1;
                                BatchStatusElement();
                                BarButtonState("Default");
                                ElementClear(validar);
                                BatchClearElement();    
                                ElementStatus("btmaestria:txmaestria:btmateria:txmateria",validar+selects);    
                                SAlert("success", "¡Excelente!", "El registro fue inactivado con exito.");
                        }
                        else if(Mensaje==='CMD_WRM')
                        {   Accion = 1;
                            ElementClear(validar);
                            ElementSetValue("nivcon",0);
                            ElementSetValue("nivpln",0);
                            BatchClearElement();
                            SAlert("warning", "¡Cuidado!", Datos);
                        }
                        else if(Mensaje==='CMD_ERR')
                        {   SAlert("error", "¡Error!", Datos);
                        }
                }
                
                function SearchByElement(Elemento)
                {       if (Elemento.id === "FindMaestriaTextBx")
                            xajax_BuscaModalByTX("btmaestria",Elemento.value);
                        else if (Elemento.id === "FindMateriaTextBx" )
                        {   var Params = [Elemento.value,ElementGetValue("idversion")];
                            xajax_BuscaModalByTX("btmateria",Params);
                        }
                }
                    
                function SearchMaestriaGetData(DatosGrid)
                {       var IDVER = DatosGrid.cells[2].innerHTML;
                        if (parseInt(IDVER)!=0)
                        {   var IDMST = DatosGrid.cells[0].innerHTML;
                            if (ElementGetValue("idmaestria")!=IDMST)
                            {   document.getElementById("idmaestria").value = IDMST;
                                document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML+" ("+DatosGrid.cells[3].innerHTML+")";
                                document.getElementById("idversion").value = IDVER;
                                cerrar();
                                if (ElementGetValue("idmateria")!="")
                                {   Accion = 1;
                                    BarButtonState("Inactive");
                                    ElementClear(validar+":idmateria:txmateria");
                                    BatchClearElement();
                                }
                            }
                            else
                            cerrar();
                        }
                        else
                        SAlert("error", "¡No existe versión vigente!", "No se encuentra vigente una versión de contenido para la maestria seleccionada.");
                        return false;
                }
                function SearchMateriaGetData(DatosGrid)
                {       var IDMAT = DatosGrid.cells[0].innerHTML;
                        if (ElementGetValue("idmateria")!=IDMAT)
                        {   var IDVER = ElementGetValue("idversion");
                            BatchClearElement();
                            document.getElementById("idmateria").value = IDMAT;
                            document.getElementById("txmateria").value = DatosGrid.cells[1].innerHTML+" - "+DatosGrid.cells[3].innerHTML;
                            cerrar();
                            xajax_BuscaByID(IDVER,IDMAT);                
                        }
                        else
                        cerrar();
                }

        </script>
    </head>
        <body>
            <div class="FormBasic" style="width:1000px">
                <div class="FormSectionMenu">              
                        <?php   $ControlMatInformacion->CargaBarButton($_GET['opcion']);
                                echo '<script type="text/javascript">';
                                echo '        BarButtonState("Inactive"); ';
                                echo '</script>';
                        ?>
                </div>      
                <div class="FormSectionData">              
                        <form id="<?php echo 'Form'.$Opcion; ?>">
                                <?php  $ControlMatInformacion->CargaMantenimiento($Libros);  ?>
                        </form>
                </div>    
            </div>            
        </body>
    </html>
