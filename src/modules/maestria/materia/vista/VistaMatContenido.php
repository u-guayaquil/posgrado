<?php   $Opcion = 'MatContenido';
        require_once('src/modules/maestria/materia/controlador/ControlMatContenido.php');
        $ControlMatContenido = new ControlMatContenido();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlMatContenido,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlMatContenido,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlMatContenido,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlMatContenido,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlMatContenido,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                var Maxnivel;
                function ButtonClick(Operacion)
                {       if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementSetValue("estado",1);
                            ElementSetValue("idtipo",0); 
                            ElementStatus("orden:idtipo:descripcion:cd:cp:ca:resultado:metodos:objaprende","btmaestria:txmaestria:btmatcont:txmatcont");
                            ElementClear("id:orden:descripcion:idpadre:txpadre:cd:cp:ca:resultado:metodos:objaprende");
                        }
                        else if (Operacion==='addMod')
                        {       var validar = "orden:idtipo:descripcion";
                                BarButtonState(Operacion);
                                if (ElementGetValue("estado")==0) validar = validar + ":estado";                      
                                var IDTIP = ElementGetValue("idtipo");
                                if (IDTIP==0) validar = validar + ":cd:cp:ca:resultado:metodos:objaprende";
                                if (IDTIP>=1) 
                                {   validar = validar + ":btpadre:txpadre";
                                    validar = validar + ":resultado:metodos:objaprende";   
                                }
                                ElementStatus(validar,"btmaestria:txmaestria:txmatcont:btmatcont");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue("estado")==1)
                                {   if (confirm("Desea inactivar este regsistro?"))
                                    xajax_Elimina(ElementGetValue("id"),ElementGetValue("idmatcont"),Maxnivel);
                                }
                                else
                                SAlert("error", "!Registro inactivo!", "El registro se encuentra inactivo");
                        }
                        else if (Operacion==='addSav')
                        {       var validar = "orden:descripcion";
                                var IDTIP = ElementGetValue("idtipo");
                                if (IDTIP==0) validar = validar + ":cd:cp:ca:resultado:metodos:objaprende";
                                if (IDTIP>=1) validar = validar + ":idpadre:txpadre";
                                
                                if (ElementValidateBeforeSave(validar))
                                {   if (BarButtonState("Inactive"))
                                    {   var validar = "idmatcont:id:orden:idtipo:descripcion:idpadre:estado:cd:cp:ca:resultado:metodos:objaprende:idcohorte";
                                        var Forma = PrepareElements(validar);
                                        xajax_Guarda(JSON.stringify({Forma}),Maxnivel);
                                    }
                                }
                                else
                                SAlert("warning","¡Campos Vacios!","Falta ingresar datos.");    
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Operacion);
                                BarButtonStateDisabled("btpadre");
                                ElementStatus("btmaestria:txmaestria:txmatcont:btmatcont","orden:idtipo:descripcion:txpadre:estado:cd:cp:ca:resultado:metodos:objaprende");
                                ElementClear("orden:descripcion:idpadre:txpadre:cd:cp:ca:resultado:metodos:objaprende");                    
                        }
                        else
                        {       if (Operacion==='btmaestria')
                                {   xajax_CargaModal(Operacion,0);
                                }    
                                else if (Operacion==='btpadre')
                                {   var IDVER = ElementGetValue("idmatcont");
                                    var IDCOH = ElementGetValue("idcohorte");
                                    var PARAMS = [IDVER,IDCOH];
                                    xajax_CargaModal(Operacion,PARAMS);
                                }    
                                else if (Operacion==='btmatcont')
                                {   var IDVER = ElementGetValue("idmaesvers");
                                    var IDCOH = ElementGetValue("idcohorte");
                                    var PARAMS = [IDVER,IDCOH];
                                    if (IDVER!="" && IDCOH!="") 
                                        xajax_CargaModal(Operacion,PARAMS);
                                    else
                                        SAlert("warning","¡Maestría no seleccionada!", "Selecione una maestria");    
                                }    
                        }        
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Active");
                            BarButtonStateDisabled("btpadre");
                            ElementSetValue("id",Datos);
                            ElementStatus("btmaestria:txmaestria:btmatcont:txmatcont","orden:idtipo:descripcion:txpadre:estado:cd:cp:ca:resultado:metodos:objaprende");
                            SAlert("success","¡Excelente!","Los datos se guardaron correctamente.");
                        }
                        else if(Mensaje==='CMD_DEL')
                        {    ElementSetValue("estado",Datos);
                        }
                        else if(Mensaje==='CMD_WRM')
                        {    SAlert("warning", "¡Cuidado!", Datos);
                        }
                        else if(Mensaje==='CMD_INF')
                        {    SAlert("info", "¡INFORMACION!", Datos);
                        }
                        else if(Mensaje==='CMD_WRM_CLS')
                        {    }
                        else if(Mensaje==='CMD_ERR')
                        {   BarButtonState("addNew"); 
                            SAlert("error", "¡Error!", Datos);
                        }    
                }
                
                function SearchByElement(Elemento)
                {       if (Elemento.id === "FindPadreTextBx")
                        {    xajax_BuscaModalByTX("btpadre",Elemento.value,ElementGetValue("idmatcont"));
                        }
                        else if (Elemento.id === "FindMatcontTextBx")
                        {   var IDVER = ElementGetValue("idmaesvers");
                            var IDCOH = ElementGetValue("idcohorte");
                            var PARAMS = [IDVER,IDCOH];
                            xajax_BuscaModalByTX("btmatcont",Elemento.value,PARAMS);
                        }    
                        else if (Elemento.id === "FindMaestriaTextBx")
                        {    xajax_BuscaModalByTX("btmaestria",Elemento.value,0);   
                        }
                }
                    
                function SearchMaestriaGetData(DatosGrid)
                {       var IDVER = DatosGrid.cells[2].innerHTML;
                        if (parseInt(IDVER)!=0)
                        {   var IDMST = DatosGrid.cells[0].innerHTML;
                            var IDCHT = DatosGrid.cells[4].innerHTML;
                            if (ElementGetValue("idmaestria")!=IDMST || ElementGetValue("idcohorte")!=IDCHT)
                            {   ElementSetValue("estado",1);
                                ElementSetValue("idtipo",0); 
                                ElementClear("idmatcont:txmatcont:id:orden:descripcion:idpadre:txpadre:cd:cp:ca:resultado:metodos:objaprende");
                                document.getElementById("idmaesvers").value = IDVER;
                                document.getElementById("idmaestria").value = IDMST;
                                document.getElementById("idcohorte").value = IDCHT;
                                document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML+" ("+DatosGrid.cells[3].innerHTML+")";
                                document.getElementById("txcohorte").value = DatosGrid.cells[5].innerHTML;
                                xajax_BuscaByID("",""); 
                                cerrar();
                            }
                            else
                            cerrar();
                        }
                        else
                        SAlert("warning", "¡No existe contenido!", "No se encuentra vigente una versión de contenido para la maestria seleccionada.");    
                        return false;
                }

                function SearchMatcontGetData(DatosGrid)
                {       var IDMTC = DatosGrid.cells[0].innerHTML;
                        var IDCHT = document.getElementById("idcohorte").value;
                        var PARAMS = [IDMTC,IDCHT];
                        if (ElementGetValue("idmatcont")!=IDMTC)
                        {   ElementSetValue("estado",1);
                            ElementSetValue("idtipo",0); 
                            ElementClear("id:orden:descripcion:idpadre:txpadre:cd:cp:ca:resultado:metodos:objaprende");
                            document.getElementById("idmatcont").value = IDMTC;
                            document.getElementById("txmatcont").value = DatosGrid.cells[2].innerHTML+" - "+DatosGrid.cells[3].innerHTML;
                            Maxnivel = DatosGrid.cells[4].innerHTML;
                            FillOptionCbx("idtipo",RetornaOptions(Maxnivel));
                            cerrar();
                            xajax_BuscaByID(PARAMS,Maxnivel);                
                        }
                        else
                        cerrar();
                        return false;
                }
                
                function SearchPadreGetData(DatosGrid)
                {       var Tipo = DatosGrid.cells[2].innerHTML;
                        if (Tipo<Maxnivel)
                        {   document.getElementById("idpadre").value = DatosGrid.cells[0].innerHTML;
                            document.getElementById("txpadre").value = DatosGrid.cells[4].innerHTML;
                            cerrar();
                        }    
                        else
                        SAlert("error", "¡Error de selección!", "No esta permitido seleccionar este nivel.");
                        return false;
                }

                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   var IdTipo = Grilla.cells[1].innerHTML;
                            BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].innerHTML;
                            document.getElementById("idtipo").value = IdTipo;
                            /*if (IdTipo==0) var TxTipo = "";
                            if (IdTipo==1) var TxTipo = "UNIDAD: ";
                            if (IdTipo==2) var TxTipo = "TEMA: ";*/
                            document.getElementById("orden").value = Grilla.cells[2].innerHTML;
                            document.getElementById("descripcion").value = Grilla.cells[13].innerHTML; 
                            document.getElementById("resultado").value = ElementGetValue(Grilla.cells[4].id);
                            document.getElementById("metodos").value = ElementGetValue(Grilla.cells[5].id); 
                            document.getElementById("objaprende").value = ElementGetValue(Grilla.cells[6].id); 
                            document.getElementById("cd").value = ElementGetValue(Grilla.cells[7].id); 
                            document.getElementById("cp").value = ElementGetValue(Grilla.cells[8].id); 
                            document.getElementById("ca").value = ElementGetValue(Grilla.cells[9].id); 
                            document.getElementById("idpadre").value = Grilla.cells[10].innerHTML; 
                            document.getElementById("txpadre").value = Grilla.cells[11].innerHTML; 
                            document.getElementById("estado").value = Grilla.cells[12].innerHTML; 
                        }
                        return false;
                }
                
                function ControlIdTipo(IdTipo)
                {       if (IdTipo.value==0)
                        {   BarButtonStateDisabled("btpadre");
                            ElementStatus("cd:cp:ca:resultado:metodos:objaprende","idpadre:txpadre");
                            ElementClear("idpadre:txpadre:cd:cp:ca"); //:resultado:metodos:objaprende
                        }    
                        else if (IdTipo.value==1 || IdTipo.value==2) 
                        {   BarButtonStateEnabled("btpadre");
                            ElementStatus("idpadre:txpadre:resultado:metodos:objaprende","cd:cp:ca");
                            ElementClear("cd:cp:ca");
                        }
                        return false;
                }      
        </script>
    </head>
        <body>
            <div class="FormBasic" style="width:1000px">
                <div class="FormSectionMenu">              
                    <?php   $ControlMatContenido->CargaBarButton($_GET['opcion']);
                            echo '<script type="text/javascript">';
                            echo '        BarButtonState("Inactive"); ';
                            echo '</script>';
                    ?>
                </div>      
                <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                              <?php  $ControlMatContenido->CargaMantenimiento();  ?>
                    </form>
                </div>    
                <div class="FormSectionGrid">          
                    <?php   $ControlMatContenido->CreaSearchGrid();  ?>
                </div>                    
            </div>            
        </body>
    </html>
