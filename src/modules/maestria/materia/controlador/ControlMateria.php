<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioMateria.php");
        require_once("src/modules/maestria/materia/render/RenderMateria.php");

        class ControlMateria
        {       private  $ServicioMateria;
                private  $RenderMateria;

                function __construct()
                {       $this->ServicioMateria = new ServicioMateria();
                        $this->RenderMateria = new RenderMateria();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderMateria->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderMateria->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => 'NULL');
                        $Datos = $this->ServicioMateria->BuscarMateriaByDescripcion($prepareDQL);
                        echo $this->RenderMateria->CreaSearchGrid($Datos);
                }         
               
                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioMateria->BuscarMateriaByID($id);
                        return $this->RenderMateria->MuestraFormulario($ajaxRespon,$Datos);
                }
                
                function ConsultaByIDByMaestria($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('id' => ($JsDatos -> id), 'id_maestria' => $JsDatos->idmaestria);
                        $ArDatos = $this->ServicioMateria->BuscarMateriaByIDByMaestria($prepareDQL);
                        return $this->RenderMateria->MuestraFormulario($ajaxRespon,$ArDatos);
                }

                function ConsultaByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();              
                        $prepareDQL = array('texto' => strtoupper(trim($Texto)));
                        $ArDatos = $this->ServicioMateria->BuscarMateriaByDescripcion($prepareDQL);
                        return $this->RenderMateria->MuestraGrid($ajaxRespon,$ArDatos);
                }
                
                function ConsultaByTXByMaestria($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('texto' => strtoupper(trim($JsDatos -> FindTextBx)), 'id' => $JsDatos->idmaestria );
                        $ArDatos = $this->ServicioMateria->BuscarMateriaByDescripcionByMaestria($prepareDQL);
                        return $this->RenderMateria->MuestraGrid($ajaxRespon,$ArDatos);
                }

                function Guarda($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");   
                        $id = $this->ServicioMateria->GuardaDBMateria($JsDatos);
                        $ArDatos = $this->ServicioMateria->BuscarMateriaEstadoByID($id);
                        return $this->RenderMateria->{$funcion}($ajaxRespon,$ArDatos);
                }

                function Elimina($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsData = json_decode($Form)->Forma;
                        $JsData->estado = $this->ServicioMateria->DesactivaMateria($JsData->id);
                        return $this->RenderMateria->MuestraEliminado($ajaxRespon,$JsData);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1));
                        if ($Operacion==="btmaestria")    
                        {                            
                            $ArDatos = $this->ServicioMateria->BuscarMaestriaByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestrias";
                            $jsonModal['Carga'] = $this->RenderMateria->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "517";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        if ($Operacion==="btmaestria")    
                        {   $Datos = $this->ServicioMateria->BuscarMaestriaByDescripcion($prepareDQL);
                            return $this->RenderMateria->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
                
                function MostrarMateriabyMaestria($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->txmaestria) ? "MuestrabyMaestria" : "MuestraEditadobyMaestria");
                        $id = $JsDatos->idmaestria;
                        $ArDatos = $this->ServicioMateria->BuscarMateriaEstadoByMaestria($id);
                        return $this->RenderMateria->{$funcion}($ajaxRespon,$ArDatos);
                }
        }       
?>