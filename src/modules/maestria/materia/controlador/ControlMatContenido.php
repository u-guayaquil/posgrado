<?php   require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioMaestria.php");
        require_once("src/rules/maestria/servicio/ServicioMatInformacion.php");
        require_once("src/rules/maestria/servicio/ServicioMatContenido.php");
        require_once("src/modules/maestria/materia/render/RenderMatContenido.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");
        
        class ControlMatContenido
        {       private $ServicioMatInformacion;
                private $ServicioMaestria;
                private $ServicioMatContenido;
                private $RenderMatContenido;
                private $ServicioUsuarioFiltro;

                function __construct($Sufijo = "")
                {       $this->ServicioMatInformacion = new ServicioMatInformacion();
                        $this->ServicioMaestria = new ServicioMaestria();
                        $this->ServicioMatContenido = new ServicioMatContenido();
                        $this->RenderMatContenido = new RenderMatContenido();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderMatContenido->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderMatContenido->CreaMantenimiento();
                }

                function CreaSearchGrid()
                {       $Datos ="[]";
                        echo $this->RenderMatContenido->CreaSearchGrid(json_decode($Datos));
                }         
                
                function ConsultaByID($Datos,$mxnvl)
                {       $ajaxRespon = new xajaxResponse();
                        if($Datos){
                                $prepareDQL = array('idmtc'=>$Datos[0], 'idcht'=>$Datos[1]);
                                $ArDatos = json_decode($this->ServicioMatContenido->BuscarIndice($prepareDQL));
                        }else
                        {        $ArDatos = json_decode($this->ServicioMatContenido->BuscarIndice($Datos));}
                        return $this->RenderMatContenido->MuestraGrid($ajaxRespon,$mxnvl,$ArDatos);
                }

                function Guarda($Form,$MaxNivel)
                {       $xAjax = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $IDDatos = $this->ServicioMatContenido->GuardaDB($JsDatos);
                        if ($IDDatos[0])
                        {       $prepareDQL = array('idmtc'=>$JsDatos->idmatcont, 'idcht'=>$JsDatos->idcohorte);
                                $ArDatos = json_decode($this->ServicioMatContenido->BuscarIndice($prepareDQL));
                                $xAjax = $this->RenderMatContenido->MuestraGuardado($xAjax,$MaxNivel,$ArDatos);
                                return $this->RenderMatContenido->Respuesta($xAjax,"CMD_SAV",$IDDatos[1]); 
                        }
                        return $this->RenderMatContenido->Respuesta($xAjax,"CMD_ERR",$IDDatos[1]);
                }

                function Elimina($id,$idmtc,$mxnvl)
                {       $ajaxRespon = new xajaxResponse();
                        $resp = $this->ServicioMatContenido->Desactiva($id);
                        if ($resp[0])
                        {   $ArDatos = json_decode($this->ServicioMatContenido->BuscarIndice($idmtc));
                            return $this->RenderMatContenido->MuestraEliminada($ajaxRespon,$mxnvl,$ArDatos);
                        }
                        else
                        return $this->RenderMatContenido->Respuesta($ajaxRespon,"CMD_WRM",$resp[1]);
                }
                
                private function FilterOfAccess($Ref="")
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        if ($Modulo->Idrol==3)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            return array('texto'=>$Ref, 'facultad'=>$Factad);
                        }    
                        else if($Modulo->Idrol==2)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarDocenteFiltroByCedula($Modulo->Cedula);
                            return array('texto'=>$Ref, 'facultad'=>$Factad);
                        }
                        else
                        return array('00'); //No permiso a Estudiante
                }                
                
                function CargaModalGrid($Operacion,$IdRel)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion=="btmaestria")    
                        {   $ArDatos = $this->ServicioMatInformacion->BuscarMaestriaVersionByFacultad($this->FilterOfAccess());
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderMatContenido->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderMatContenido->GetGridTableWidth();
                        }                           
                        else if ($Operacion=="btmatcont")    
                        {   $prepareDQL = array('maestver'=>$IdRel[0], 'cohorte'=>$IdRel[1]);
                            $ArDatos = $this->ServicioMatInformacion->BuscarMateriaByMaestriaVersion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Materia";
                            $jsonModal['Carga'] = $this->RenderMatContenido->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderMatContenido->GetGridTableWidth();
                        }                           
                        elseif ($Operacion==="btpadre")    
                        {   $prepareDQL = array('idmtc'=>$IdRel[0], 'idcht'=>$IdRel[1]);
                            $ArDatos = $this->ServicioMatContenido->BuscarByTipo($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Unidad y Tema";
                            $jsonModal['Carga'] = $this->RenderMatContenido->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderMatContenido->GetGridTableWidth();
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto,$IdRel)
                {       $ajaxResp = new xajaxResponse();    
                        $Texto = strtoupper(trim($Texto));
                        
                        if ($Operacion==="btmaestria")
                        {   $Datos = $this->ServicioMatInformacion->BuscarMaestriaVersionByFacultad($this->FilterOfAccess($Texto));
                            return $this->RenderMatContenido->MuestraModalGrid($ajaxResp,$Operacion,$Datos);        
                        }
                        else if ($Operacion==="btmatcont")
                        {   $prepareDQL = array('maestver' => intval($IdRel[0]), 'texto' => $Texto, 'cohorte'=>$IdRel[1]);
                            $Datos = $this->ServicioMatInformacion->BuscarMateriaByMaestriaVersion($prepareDQL);
                            return $this->RenderMatContenido->MuestraModalGrid($ajaxResp,$Operacion,$Datos);        
                        }
                        else if ($Operacion==="btpadre")    
                        {   /*$prepareDQL = array('id' => intval($IdRel),'texto' => $texto, 'tipo' => array(0,1));
                            $Datos = $this->ServicioMatContenido->BuscarMatContenidoTipoByDescripcion($prepareDQL);
                            return $this->RenderMatContenido->MuestraModalGrid($ajaxResp,$Operacion,$Datos);*/                                        
                        }    
                }
        }       
?>

