<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioMatInformacion.php");
        require_once("src/rules/maestria/servicio/ServicioMaestria.php");
        require_once("src/rules/maestria/servicio/ServicioMateria.php");
        require_once("src/modules/maestria/materia/render/RenderMatInformacion.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");

        class ControlMatInformacion
        {       private $ServicioMatInformacion;
                private $RenderMatInformacion;
                private $ServicioMaestria;
                private $ServicioMateria;
                private $ServicioUsuarioFiltro;

                function __construct()
                {       $this->ServicioMatInformacion = new ServicioMatInformacion();
                        $this->RenderMatInformacion = new RenderMatInformacion();
                        $this->ServicioMaestria = new ServicioMaestria();
                        $this->ServicioMateria = new ServicioMateria();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderMatInformacion->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento($InfoAdd)
                {       echo $this->RenderMatInformacion->CreaMantenimiento($InfoAdd);
                }
                
                function ConsultaByID($idVer,$idMat)
                {       $ajaxRespon = new xajaxResponse();
                        $ArDatos = $this->ServicioMatInformacion->BuscarObjetivosByMateriaVersion($idVer,$idMat);
                        $SbDatos = $this->ServicioMatInformacion->BuscarBibliografiaByMateriaVersion($idVer,$idMat);
                        return $this->RenderMatInformacion->MuestraFormulario($ajaxRespon,$ArDatos,$SbDatos);
                }

                function Guarda($Accion,$Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $IDDatos = $this->ServicioMatInformacion->GuardaDB($Accion,$JsDatos);
                        if ($IDDatos[0])
                                return $this->RenderMatInformacion->MuestraGuardado($ajaxRespon,$IDDatos[1]);
                        else
                        {       $ajaxRespon->call("BarButtonState","addMod");
                                $ajaxRespon->call("XAJAXResponse","CMD_ERR",$IDDatos[1]);
                        }
                        return $ajaxRespon; 
                }

                function Elimina($idVer,$idMat)
                {       $ajaxRespon = new xajaxResponse();
                        $Resp = $this->ServicioMatInformacion->Desactiva($idVer,$idMat);
                        return $this->RenderMatInformacion->Respuesta($ajaxRespon,"CMD_DEL",($Resp ? 1:0));
                }
                
                private function FilterOfAccess($Ref="")
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        if ($Modulo->Idrol==3)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            return array('texto'=>$Ref, 'facultad'=>$Factad);
                        }    
                        else if($Modulo->Idrol==2)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarDocenteFiltroByCedula($Modulo->Cedula);
                            return array('texto'=>$Ref, 'facultad'=>$Factad);
                        }
                        else
                        return array('facultad'=>array("00"));
                }                
                
                function CargaModalGrid($Operacion,$IdRelacion)
                {       $ajaxResp = new xajaxResponse();
                        if ($Operacion=="btmaestria")    
                        {   $ArDatos = $this->ServicioMaestria->BuscarMaestriaVersionByFacultad($this->FilterOfAccess());
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderMatInformacion->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderMatInformacion->GetGridTableWidth();
                        }   
                        if ($Operacion=="btmateria")    
                        {   $prepareDQL = array('maesver' => $IdRelacion);
                            $ArDatos = $this->ServicioMateria->BuscarMateriaMallaByMaestriaVersion($prepareDQL);
                           
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Materia Malla";
                            $jsonModal['Carga'] = $this->RenderMatInformacion->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderMatInformacion->GetGridTableWidth();
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp;
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion=="btmaestria")    
                        {   $Texto = strtoupper(trim($Texto));
                            $ArDatos = $this->ServicioMaestria->BuscarMaestriaVersionByFacultad($this->FilterOfAccess($Texto));
                            return $this->RenderMatInformacion->MuestraModalGrid($ajaxResp,$Operacion,$ArDatos);                                        
                        }    
                        if ($Operacion=="btmateria")    
                        {   $prepareDQL = array('texto' => strtoupper(trim($Texto[0])), 'maesver' => $Texto[1]);
                            $ArDatos = $this->ServicioMateria->BuscarMateriaMallaByMaestriaVersion($prepareDQL);
                            return $this->RenderMatInformacion->MuestraModalGrid($ajaxResp,$Operacion,$ArDatos);                                        
                        }
                }
        }       
?>

