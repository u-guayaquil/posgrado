<?php
require_once("src/libs/clases/BarButton.php");
require_once('src/libs/clases/SearchInput.php');
require_once("src/libs/clases/SearchGrid.php");

class RenderRecord
{   private $Width;
	private $SearchGrid;
	private $Search;
	private $Columnas;
	private $Cerrado;

	function __construct()
	{       $this->Search = new SearchInput();
			$this->SearchGrid = new SearchGrid();
	}

	function CreaBarButton($Buttons)
	{       $BarButton = new BarButton();
			return $BarButton->CreaBarButton($Buttons);
	}

	function CreaMantenimiento()
	{       $Col1="15%"; $Col2="40%"; $Col3="15%"; $Col4="15%"; $Col5="15%";

			$HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
			$HTML.= '       <tr height="35">';
			$HTML.= '           <td class="Form-Label" colspan="5">:: REPORTE DE RECORD ESTUDIANTE</td>';
			$HTML.= '       </tr>';
			$HTML.= '       <tr height="28">';
			$HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Maestria</td>';
			$HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
			$HTML.= '               <input type="hidden" id="idfacultad" name="idfacultad" value="" />';
			$HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t11");
			$HTML.= '           </td>';
			$HTML.= '           <td class="Form-Label" colspan="3">';
			$HTML.= '           </td>';
			$HTML.= '       </tr>';
			$HTML.= '       <tr height="28">';
			$HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Cohorte</td>';
			$HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
			$HTML.= '               <input type="hidden" id="idciclo" name="idciclo" value="" />';
			$HTML.=                 $this->Search->TextSch("cohorte","","")->Enabled(true)->Create("t11");
			$HTML.= '           </td>';
			$HTML.= '       </tr>';
			$HTML.= '       <tr height="28">';
			$HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Estudiante</td>';
			$HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
			$HTML.= '               <input type="hidden" id="ciestudiante" name="ciestudiante" value="" />';
			$HTML.=                 $this->Search->TextSch("estudiante","","")->Enabled(true)->Create("t11");
			$HTML.= '           </td>';
			$HTML.= '       </tr>';
			return $HTML.'</table>';
	}

	private function SearchGridModalConfig($Operacion)
	{       if ($Operacion=="btmaestria")
			{   $Columns['M.ID']=array('40px','center','');
				$Columns['Maestria']=array('300px','left','');
				$Columns['IdEstado']=array('0px','center','none');
				$Columns['Estado']=array('50px','center','');
				$Columns['IdFacultad']=array('0px','center','none');
			}
			else if ($Operacion=="btcohorte")
			{   $Columns['C.ID']=array('40px','center','');
				$Columns['Cohorte']=array('200px','left','');
				$Columns['Version Maestria']=array('0px','center','none');
				$Columns['IdEstado']=array('0px','center','none');
				$Columns['Estado']=array('50px','center','');
				$Columns['IdCiclo']=array('0px','center','none');
			}
			else if ($Operacion=="btestudiante")
			{   $Columns['E.ID']=array('0px','center','none');
				$Columns['Cedula']=array('100px','center','');
				$Columns['Apellidos']=array('150px','left','');
				$Columns['Nombres']=array('150px','left','');
			}
			return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
	}

	function CreaModalGrid($Operacion,$Contenido)
	{       $SearchGrid = new SearchGrid($Operacion);
			$SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
			$GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
			$ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
			$this->Width = $SearchGrid->SearchTableWidth();
			return $ObModalGrid;
	}

	private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
	{       if ($Contenido[0])
			{   if ($Operacion=="btmaestria")
				{   foreach ($Contenido[1] as $ArDato)
					{       $id = str_pad($ArDato['IDMAESTRIA'],MAESTRIA,'0',STR_PAD_LEFT);
							$Grid->CreaSearchCellsDetalle($id,'',$id);
							$Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MAESTRIA']));
							$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
							$Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
							$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDFACULTAD']);
							$Grid->CreaSearchRowsDetalle ($id,"black");
					}
				}
				elseif ($Operacion=="btcohorte")
				{   foreach ($Contenido[1] as $ArDato)
					{       $id = str_pad($ArDato['ID'],COHORTE,'0',STR_PAD_LEFT);
							$Grid->CreaSearchCellsDetalle($id,'',$id);
							$Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['COHORTE']));
							$Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['VERSION_MAESTRIA']));
							$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
							$Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
							$Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDCICLO']);
							$Grid->CreaSearchRowsDetalle ($id,"black");
					}
				}
				elseif ($Operacion=="btestudiante")
				{   foreach ($Contenido[1] as $ArDato)
					{       $id = str_pad($ArDato['ID'],ESTUDIANTE,'0',STR_PAD_LEFT);
							$Grid->CreaSearchCellsDetalle($id,'',$id);
							$Grid->CreaSearchCellsDetalle($id,'',$ArDato['CEDULA']);
							$Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['APELLIDOS']));
							$Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['NOMBRES']));
							$Grid->CreaSearchRowsDetalle ($id,"black");
					}
				}
			}
			return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
	}

	function GetGridTableWidth()
	{        return $this->Width;
	}

	function MuestraModalGrid($Ajax,$Operacion,$Datos)
	{       $SearchGrid = new SearchGrid($Operacion);
			$SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
			$GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
			$Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
			return $Ajax;
	}

	private function SearchGridConfig()
	{       $Columns['No'] = array('30px','center','');
			$Columns['Notas.ID'] = array('0px','center','none');
			$Columns['EstMat.ID'] = array('0px','center','none');
			$Columns['Materia'] = array('250px','left','');
			$Columns['Paralelo'] = array('100px','left','');
			$Columns['Notas'] = array('50px','center','');
			$Columns['%Asist'] = array('50px','center','');
			$Columns['Estado'] = array('70px','center','');
			return array('AltoCab' => '35px','DatoCab' => $Columns,'AltoDet' => '300px','AltoRow' => '25px','FindTxt' =>false);
	}

	function CreaSearchGrid($Records,$Cerrado=0)
	{       $this->Cerrado = $Cerrado;
			$this->Columnas = $Records[1];
			$SearchGrid = new SearchGrid();
			$SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
			$GrdDataHTML = $this->GridDataHTML($SearchGrid,$Records[1]);
			return $SearchGrid->CreaSearchGrid($GrdDataHTML);
	}

	private function GridDataHTML($Grid,$Datos)
	{   $num = 1;
		if($Datos){
			foreach ($Datos as $Dato)
			{       $Id = str_pad($num,2,'0',STR_PAD_LEFT);
					$Grid->CreaSearchCellsDetalle($Id,'',$num++);
					$Grid->CreaSearchCellsDetalle($Id,'',$Dato['IDNOTAS']);
					$Grid->CreaSearchCellsDetalle($Id,'',$Dato['IDESTUDMATE']);
					$Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['MATERIA']));
					$Grid->CreaSearchCellsDetalle($Id,'',utf8_encode($Dato['PARALELO']));
					$NotasTotales = json_decode($Dato['NOTAS']);
					if($NotasTotales)
						$Grid->CreaSearchCellsDetalle($Id,'',$NotasTotales->FIN);
					else
						$Grid->CreaSearchCellsDetalle($Id,'',' ');
					$Asistencia = number_format(($Dato['ASIST']/$Dato['TCD'])*100,0);
					$NotaFinal = ($NotasTotales?$NotasTotales->FIN:0);
					$Estado = $NotaFinal >= 7 ? ( $Asistencia >= 80 ? "APROBADO":"REPROBADO"):"REPROBADO";
					$Grid->CreaSearchCellsDetalle($Id,'',$Asistencia);
					$Grid->CreaSearchCellsDetalle($Id,'',$Estado);
					$Grid->CreaSearchRowsDetalle ($Id,"black",0);
			}
			return $Grid->CreaSearchTableDetalle();
		}else{
			return $Grid->CreaSearchTableDetalle(0,$Datos[1]);
		}
	}

	function MuestraGrid($Ajax,$Datos)
	{       foreach ($Datos as $Dato)
			{        $Ajax->Assign($Dato[0],"innerHTML",$Dato[1]);
			}
			return $Ajax;
	}
}
?>

