﻿function ButtonClick(Operacion)
{   if (Operacion=="addPdf")
    {   var MAEST = ElementGetValue("idmaestria");
        var IDCHT = ElementGetValue("idcohorte");
        var CEDULA = ElementGetText("ciestudiante");
        var EST = ElementGetText("txestudiante");
        if (IDMST!="" && IDCHT!="" && CEDULA!=""){
            var Params = [MAEST,IDCHT,CEDULA,EST];
            xajax_ActaRecord(Params);
        } 
        else
        SAlert("warning","¡Campos Vacios!","¡Todos los campos deben estar llenos!");
    }
    else if (Operacion=="btmaestria")
    {   xajax_CargaModal(Operacion,0);
    }
    else if (Operacion=="btcohorte")
    {   var IDMST=ElementGetValue("idmaestria");
        if (IDMST!="")
        {   xajax_CargaModal(Operacion,IDMST);
        }
        else
        SAlert("warning","¡Error!","¡Debe seleccionar una maestría!");
    }
    else if (Operacion=="btestudiante")
    {   var IDMST=ElementGetValue("idmaestria");
        var IDCHT=ElementGetValue("idcohorte");
        if (IDMST!="" && IDCHT!=""){
            var Params = [IDMST,IDCHT];
            xajax_CargaModal(Operacion,Params);
        }
        else
        SAlert("warning","¡Error!","¡Debe seleccionar una cohorte!");
    }
    return false;
}

function SearchMaestriaGetData(DatosGrid)
{       var IDMST = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("idmaestria")!=IDMST)
        {   ElementStatus("","");
            ElementSetValue("WrkGrd","");
            BarButtonState("Inactive");
            ElementClear("idcohorte:txcohorte:idestudiante:txestudiante:ciestudiante");
            document.getElementById("idmaestria").value = IDMST;
            document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idfacultad").value = DatosGrid.cells[4].innerHTML;
            cerrar();
        }
        else
        cerrar();
        return false;
}

function SearchCohorteGetData(DatosGrid)
{       var IDCHT= DatosGrid.cells[2].innerHTML;
        if (ElementGetValue("idcohorte")!=IDCHT)
        {   ElementStatus("","");
            ElementSetValue("WrkGrd","");
            BarButtonState("Inactive");
            ElementClear("idestudiante:txestudiante:ciestudiante");
            document.getElementById("idcohorte").value = DatosGrid.cells[0].innerHTML;
            document.getElementById("txcohorte").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idciclo").value = DatosGrid.cells[5].innerHTML;
            cerrar();
        }
        else
        cerrar();
        return false;
}

function SearchEstudianteGetData(DatosGrid)
{       var IDEST= DatosGrid.cells[2].innerHTML;
        if (ElementGetValue("idestudiante")!=IDEST)
        {   ElementStatus("","");
            ElementSetValue("WrkGrd","");
            BarButtonState("Active");
            document.getElementById("idestudiante").value = DatosGrid.cells[0].innerHTML;
            document.getElementById("ciestudiante").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("txestudiante").value = DatosGrid.cells[2].innerHTML+' '+DatosGrid.cells[3].innerHTML;
            var MAEST = ElementGetValue("idmaestria");
            var COHRT = ElementGetValue("idcohorte");
            var CEDULA = ElementGetText("ciestudiante");
            var Params = [MAEST,COHRT,CEDULA];
            xajax_BuscaRecordEstudiante(Params);
            cerrar();
        }
        else
        cerrar();
        return false;
}

function SearchByElement(Elemento)
{   Elemento.value = TrimElement(Elemento.value);
        if (Elemento.id=="FindMaestriaTextBx")
        {   xajax_BuscaModalByTX("btmaestria",Elemento.value);
        }
        else if (Elemento.id=="FindCohorteTextBx")
        {    var IDMST=ElementGetValue("idmaestria");
            var Params = [Elemento.value,IDMST];
            xajax_BuscaModalByTX("btcohorte",Params);
        }
        else if (Elemento.id=="FindEstudianteTextBx")
        {	var IDMST=ElementGetValue("idmaestria");
            var IDCHT=ElementGetValue("idcohorte");
            var Params = [Elemento.value,IDMST,IDCHT];
            xajax_BuscaModalByTX("btestudiante",Params);
        }
        return false;
}

function XAJAXResponse(Mensaje,Datos)
{   if (Mensaje==="CMD_SAV"){
        SAlert("success","¡Excelente!",Datos);
    }
    else if(Mensaje==='CMD_ERR'){
        SAlert("error","¡Error!",Datos);
    }
}