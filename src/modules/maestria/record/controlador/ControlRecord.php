<?php
require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
require_once("src/rules/maestria/servicio/ServicioRecord.php");
require_once("src/modules/maestria/record/render/RenderRecord.php");
require_once("src/rules/maestria/servicio/ServicioReporte.php");

class ControlRecord
{   private $ServicioRecord;
	private $RenderRecord;

	function __construct()
	{       $this->ServicioRecord = new ServicioRecord();
			$this->RenderRecord = new RenderRecord();
	}

	function CargaBarButton($Opcion)
	{       $BarButton = new BarButton();
			$ServicioPerfilOpcion = new ServicioPerfilOpcion();
			$Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
			if ($Buttons[0])
			echo $this->RenderRecord->CreaBarButton($Buttons[1]);
	}

	function CargaMantenimiento()
	{       echo $this->RenderRecord->CreaMantenimiento();
	}

	function ConsultaRecordEstudiante($Form)
	{       $xAjax = new xajaxResponse();
			$Records = $this->ServicioRecord->BuscarRecords($Form);
			if ($Records[0])
			{   $SearchGrid = $this->RenderRecord->CreaSearchGrid($Records,0);
				$xAjax->Assign("WrkGrd","innerHTML",$SearchGrid);
				$xAjax->Call("ElementStatus","MinPrn");
			}
			else
			{       $jsFuncion = "XAJAXResponse";
					$xAjax->call($jsFuncion,"CMD_ERR",$Records[1]);
			}
			return $xAjax;
	}

	function ActaRecord($Params)
	{   $xAjax = new xajaxResponse();
		$Utils = new Utils();
		$urlfl = $Utils->Encriptar("src/modules/maestria/record/vista/rptRecord.php");
		$title = "Record de Estudiante";
		$optns = $Utils->Encriptar($Params[0]."|".$Params[1]."|".$Params[2]."|".$Params[3]);
		$jsonModal['Modal'] = "ModalMINPRN";
		$jsonModal['Title'] = $title;
		$jsonModal['Carga'] = "<iframe name='ModalMINPRN' src='index.php?url=".$urlfl."&opcion=".$optns."' height='465px' width='100%' scrolling='si' frameborder='0' marginheight='0' marginwidth='0'></iframe>";  
		$jsonModal['Ancho'] = "900";
		$jsonModal['Alto']  = "500";
		$xAjax->call("CreaModal", json_encode($jsonModal));
		return $xAjax;
	}

	function CargaModalGrid($Operacion,$IdRef)
	{       $ajaxResp = new xajaxResponse();
			if ($Operacion==="btmaestria")
			{   $Params = array('txt'=>'');
				$ArDatos = $this->ServicioRecord->BuscarMaestria($Params);
				$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
				$jsonModal['Title'] = "Busca Maestria";
				$jsonModal['Carga'] = $this->RenderRecord->CreaModalGrid($Operacion,$ArDatos);
				$jsonModal['Ancho'] = $this->RenderRecord->GetGridTableWidth();
			}
			if ($Operacion==="btcohorte")
			{   $Params = array('txt'=>'','mtr'=>$IdRef);
				$ArDatos = $this->ServicioRecord->BuscarCohorteByMaestria($Params);
				$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
				$jsonModal['Title'] = "Busca Cohorte";
				$jsonModal['Carga'] = $this->RenderRecord->CreaModalGrid($Operacion,$ArDatos);
				$jsonModal['Ancho'] = $this->RenderRecord->GetGridTableWidth();
			}
			if ($Operacion==="btestudiante")
			{   $Params = array('txt'=>'','mtr'=>$IdRef[0],'cht'=>$IdRef[1]);
				$ArDatos = $this->ServicioRecord->BuscarEstudiantesByCohorte($Params);
				$jsonModal['Modal'] = "Modal".strtoupper($Operacion);
				$jsonModal['Title'] = "Busca Cohorte";
				$jsonModal['Carga'] = $this->RenderRecord->CreaModalGrid($Operacion,$ArDatos);
				$jsonModal['Ancho'] = $this->RenderRecord->GetGridTableWidth();
			}
			$ajaxResp->call("CreaModal", json_encode($jsonModal));
			return $ajaxResp;
	}

	function ConsultaGridByTX($Operacion,$Texto)
	{   $xAjax = new xajaxResponse();
		if ($Operacion==="btmaestria")
		{   $Texto = strtoupper(trim($Texto));
			$Params = array('txt'=>$Texto);
			$Datos = $this->ServicioRecord->BuscarMaestria($Params);
			return $this->RenderRecord->MuestraModalGrid($xAjax,$Operacion,$Datos);
		}
		else if ($Operacion=="btcohorte")
		{   $Texto[0] = strtoupper(trim($Texto[0]));
			$prepareDQL = array('txt'=>$Texto[0],'mtr'=>$Texto[1]);
			$Datos = $this->ServicioRecord->BuscarCohorteByMaestria($prepareDQL);
			return $this->RenderRecord->MuestraModalGrid($xAjax,$Operacion,$Datos);
		}
		else if ($Operacion=="btestudiante")
		{   if(is_numeric($Texto[0]))
				$Texto[0] = intval($Texto[0]);
			else
				$Texto[0] = strtoupper(trim($Texto[0]));
			$prepareDQL = array('txt'=>$Texto[0],'mtr'=>$Texto[1],'cht'=>$Texto[2]);
			$Datos = $this->ServicioRecord->BuscarEstudiantesByTxt($prepareDQL);
			return $this->RenderRecord->MuestraModalGrid($xAjax,$Operacion,$Datos);
		}
	}
}

?>

