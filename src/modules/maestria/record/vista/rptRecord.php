<?php   require_once("src/rules/maestria/servicio/ServicioRecord.php");
        require_once("src/rules/maestria/servicio/ServicioReporte.php");
        require_once("src/libs/plugins/pdf/vendor/mpdf/mpdf/mpdf.php"); 
        
        class rptRecord
        {
                private $Cols;
              
                private function CabsCSS()
                {       $Style = 'style = "background-color: #FFF;
                                  border-collapse:collapse; 
                                  border:0px solid #ccc;
                                  font-family:tahoma; 
                                  font-size:9px; 
                                  width:100%"';    
                        return  $Style;          
                }
                
                private function ColcCSS()
                {       $Style = 'style="text-align:center;
                                  border:1px solid #ccc;
                                  font-weight:bold;
                                  font-size:12px;
                                  height:30px"';    
                        return  $Style;          
                }

                private function ColdCSS($align)
                {       $Style = 'style="text-align:'.$align.';
                                  border:1px solid #ccc;
                                  font-size:12px;
                                  height:23px"';    
                        return  $Style;          
                }

                function CrearReporte($Params,$Record)
                {       $ServicioRecord = new ServicioRecord();
                        $Inform = $ServicioRecord->DatosCabeceraReporteEstudiante($Params);
                        if ($Inform[0])
                        {   $HTMI = '<table border="2" style="font-family:tahoma;font-size:8px; border-collapse:collapse">';
                            $HDET = $this->AsistenciaActaColumna(); 
                            $HDET.= $this->AsistenciaActaDetalle($Record);
                            $HDET.= $this->AsistenciaActaPiePagina();
                            $HTMF = '</table>';
                            $HDET = $HTMI.$HDET.$HTMF;
                            return $this->AsistenciaActaCuerpo($Inform[1],'',$Params[3]).$HDET;
                        }
                        return $Inform[1];
                }
                
                private function AsistenciaActaCuerpo($Inform,$HDET,$Estudiante)
                {       $Dato = $Inform[0];
                        $HTML =     '<table border="0" '.$this->CabsCSS().'>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:10%;text-align:left" rowspan="5">';
                        $HTML.=             '<img src="src/public/img/logo_ug_small.png" width="57px" height="67px">';
                        $HTML.=         '</td>';
                        $HTML.=         '<td style="width:90%;text-align:center" colspan="3"><b>UNIVERSIDAD DE GUAYAQUIL</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>FACULTA DE '.$Dato['FACULTAD'].'</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>COORDINACION DE POSGRADO</b></td>';
                        $HTML.=     '</tr>';    
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>MAESTRIA EN '.utf8_encode($Dato['MAESTRIA']).'</b></td>';
                        $HTML.=     '</tr>';    
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="text-align:center" colspan="3"><b>ACTA DE ESTUDIANTE - SISTEMA '.$Dato['TIPOMALLA'].'</b></td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:10%;"><b>ESTUDIANTE</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.utf8_encode($Estudiante).'</td>';
                        //$HTML.=         '<td style="width:10%;"><b>PROFESOR</b></td>'; 
                        //$HTML.=         '<td style="width:40%;">'.utf8_encode($Dato['DOCENTE']).'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr height="20px">';
                        $HTML.=         '<td style="width:10%;"><b>COHORTE</b></td>'; 
                        $HTML.=         '<td style="width:40%;">'.$Dato['COHORTE'].'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '<tr >';
                        $HTML.=         '<td colspan="4">'.$HDET.'</td>'; 
                        $HTML.=     '</tr>';
                        $HTML.=     '</table>';
                        return $HTML;
                }   

                private function AsistenciaActaPiePagina()
                {       $DateControl = new DateControl();
                        $HTML = '<tr>';
                        $HTML.= '<td style="border:none" colspan="20">';
                        $HTML.=     '<table border="0" '.$this->CabsCSS().'>';
                        $HTML.=     '<tr><td height="80px" colspan="4"></td></tr>';
                        $HTML.=     '<tr>';
                      //$HTML.=         '<td height="50px" style="width:3%;text-align:center"></td>';
                        $HTML.=         '<td height="50px" style="width:29%;text-align:center"><b>Firma del Decano(a)</b><br>Fecha: '.$DateControl->getNowDateTime("FECHA").'</td>';
                        $HTML.=         '<td height="50px" style="width:29%;text-align:center">';
                        $HTML.=         '<b>Nombre y Firma<br>Coordinador de Posgrado</b><br><br>Fecha: _____/_____/________';
                        $HTML.=         '</td>';
                        $HTML.=         '<td height="50px" style="width:29%;text-align:center"><b>Nombre y Firma<br>Coordinador del Programa</b><br><br>Fecha: _____/_____/________</td>';
                        $HTML.=         '<td height="50px" style="width:13%;text-align:left">';
                        $HTML.=         'Formato # DP-2019-004<br>Versión 1.0<br>Última actualización:<br>29/07/2019';
                        $HTML.=         '</td>';
                        $HTML.=     '</tr>';
                        $HTML.=     '</table>';
                        $HTML.= '</td>';
                        $HTML.= '</tr>';
                        return $HTML;
                }                

                private function AsistenciaActaColumna()
                {       $this->Cols = 0;    
                        $HTMA = "<tr>";
                        $HTMA.= "<td colspan='9' ".$this->ColcCSS().">RECORD DEL ESTUDIANTE</td></tr>";
                        $HTMB = "<tr>";
                        $HTMB.= "<td width='30px' ".$this->ColcCSS().">No</td>";
                        $HTMB.= "<td width='250px' ".$this->ColcCSS().">MATERIA</td>";
                        $HTMB.= "<td width='100px' ".$this->ColcCSS().">PARALELO</td></tr>";
						$HTMB.= "<td width='75px' ".$this->ColcCSS().">CD</td></tr>";
						$HTMB.= "<td width='75px' ".$this->ColcCSS().">CPE</td></tr>";
						$HTMB.= "<td width='75px' ".$this->ColcCSS().">CA</td></tr>";
                        $HTMB.= "<td width='75px' ".$this->ColcCSS().">NOTA</td></tr>";
						$HTMB.= "<td width='75px' ".$this->ColcCSS().">ASIST</td></tr>";
                        $HTMB.= "<td width='120px' ".$this->ColcCSS().">ESTADO</td>";
                        return $HTMA.$HTMB;
                }
                
                private function AsistenciaActaDetalle($Record)
                {           $STRI = "";
                            $HTMC = "";
                            $NUME = 0; 
                            $HDET = "";
							$PorcTotalNota = 0;
                            foreach ($Record as $Dato)
                            {       if ($STRI!=$Dato['IDESTUDMATE'])
                                    {   $STRI = $Dato['IDESTUDMATE'];
                                        if ($HTMC!="")
                                        {   $HDET.= "<tr>".$HTMC."</tr>"; }
                                        $NUME++;
                                        $HTMC = "<td ".$this->ColdCSS('center').">".$NUME."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('left').">".utf8_encode($Dato['MATERIA'])."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('left').">".utf8_encode($Dato['PARALELO'])."</td>";
                                        $NotasTotales = json_decode($Dato['NOTAS']);

                                        if($NotasTotales){
											$HTMC.= "<td ".$this->ColdCSS('center').">".$NotasTotales->CD."</td>";
											$HTMC.= "<td ".$this->ColdCSS('center').">".$NotasTotales->CPE."</td>";
											$HTMC.= "<td ".$this->ColdCSS('center').">".$NotasTotales->CA."</td>";
											$HTMC.= "<td ".$this->ColdCSS('center').">".$NotasTotales->FIN."</td>";
											$PorcTotalNota = $PorcTotalNota+$NotasTotales->FIN;
										}
                                        else{
											$HTMC.= "<td ".$this->ColdCSS('center')."> </td>";
											$HTMC.= "<td ".$this->ColdCSS('center')."> </td>";
											$HTMC.= "<td ".$this->ColdCSS('center')."> </td>";
											$HTMC.= "<td ".$this->ColdCSS('center')."> </td>";
											$PorcTotalNota = $PorcTotalNota+0;
										}					
										$Asistencia = number_format(($Dato['ASIST']/$Dato['TCD'])*100,0);
										$NotaFinal = ($NotasTotales?$NotasTotales->FIN:0);
										$Estado = $NotaFinal >= 7 ? ( $Asistencia >= 80 ? "APROBADO":"REPROBADO"):"REPROBADO";
                                        $HTMC.= "<td ".$this->ColdCSS('center').">".$Asistencia."%</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('center').">".$Estado."</td>";
                                    }
                            }
							
                            if ($NUME!=0){
								$HTMC.= "<tr><td ".$this->ColdCSS('center')." colspan='6'>PORCENTAJE TOTAL NOTA:</td><td ".$this->ColdCSS('center').">".number_format(($PorcTotalNota/$NUME), 2)."</td><tr>";   
								$HDET.= "<tr>".$HTMC."</tr>";
                            }    
                        return $HDET; 
                }
        }
        
        if (isset($_GET['opcion']))
        {   $Utils = new Utils();
            $Nodos = $Utils->Desencriptar($_GET['opcion']);
            $Params = explode("|",$Nodos);
            
            $ServicioRecord = new ServicioRecord();
            $Record = $ServicioRecord->BuscarRecords($Params);
            if ($Record[0]) 
            {   $rptRecord = new rptRecord();
                $HTML = $rptRecord->CrearReporte($Params,$Record[1]);
                $mpdf = new Mpdf('utf-8', 'A4 L');
                $mpdf->WriteHTML($HTML);
                $mpdf->Output('RCESTD-'.$Params[0].$Params[1].'-'.$Params[2].'.pdf', 'I');   
            }   else
                    echo $Record[1];
        }   
?>
        
        