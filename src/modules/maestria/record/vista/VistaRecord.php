<?php
        $Opcion = 'Record';
        require_once('src/modules/maestria/record/controlador/ControlRecord.php');
        $ControlRecord = new ControlRecord();
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlRecord,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaRecordEstudiante', $ControlRecord,'ConsultaRecordEstudiante'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlRecord,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlRecord,'ConsultaGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('ActaRecord',$ControlRecord,'ActaRecord'));
        $xajax->processRequest();
?>
<!doctype html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <?php   $xajax->printJavascript();
                require_once('src/utils/links.php');
        ?>
        <script type="text/javascript" src="src/modules/maestria/record/js/jsrecord.js?1"></script>
</head>
<body>
	<div class="FormBasic" style="width:605px">
		<div class="FormSectionMenu">
			<?php   $ControlRecord->CargaBarButton($_GET['opcion']);
					echo '<script type="text/javascript">';
					echo '        BarButtonState("Inactive"); ';
					echo '</script>';
			?>
		</div>
			<form id="<?php echo 'Form'.$Opcion; ?>">
			<div class="FormSectionData">
					<?php  $ControlRecord->CargaMantenimiento();  ?>
			</div>
			<div id="<?php echo 'WrkGrd'; ?>" class="FormSectionGrid">
			</div>
			</form>
	</div>
</body>
</html>

