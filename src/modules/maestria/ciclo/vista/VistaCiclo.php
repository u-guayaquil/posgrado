<?php   $Opcion = 'Ciclo';

        require_once('src/modules/maestria/ciclo/controlador/ControlCiclo.php');
        $ControlCiclo= new ControlCiclo();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlCiclo,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlCiclo,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlCiclo,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlCiclo,'ConsultaByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlCiclo,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlCiclo,'ConsultaModalGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('MostrarbyCohorte', $ControlCiclo,'MostrarCiclobyCohorte'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTXByCohorte', $ControlCiclo,'ConsultaByTXByCohorte'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByIDByCohorte', $ControlCiclo,'ConsultaByIDByCohorte'));
        
        $xajax->processRequest();       
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
            
            var objciclo = "idcohorte:txcohorte:id:descripcion:nivel:feinicio:fefin:estado";
            var valciclo = "txcohorte:descripcion:nivel:feinicio:fefin";
                
                function ButtonClick(Operacion)
                { 
                        var objciclo = "idcohorte:txcohorte:id:descripcion:nivel:feinicio:fefin:estado";
                        var valciclo = "idcohorte:txcohorte:descripcion:nivel:feinicio:fefin";
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementStatus("descripcion:nivel:feinicio:fefin","id:txcohorte:btcohorte");
                            ElementClear("id:descripcion:nivel:feinicio:fefin");
                            ElementSetValue("estado",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Operacion);
                                if (ElementGetValue("estado")==0)
                                {   BarButtonStateEnabled("btcohorte");
                                    valciclo = valciclo + ":estado";
                                }
                                ElementStatus(valciclo,"id:txcohorte:btcohorte:feinicio");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue("estado")==1)
                                {   /*if (confirm("Desea inactivar este registro?"))
                                    {   var Forma = PrepareElements("id:estado");
                                        xajax_Elimina(JSON.stringify({Forma}));
                                    }   */
                                    Swal.fire({
                                        title: 'Desactivar registro?',
                                        text: "Esta seguro que desea desactivar el registro?",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Si, estoy seguro!'
                                    }).then(function(result){
                                        if (result.value) {
                                            var Forma = PrepareElements("id:estado");
                                            xajax_Elimina(JSON.stringify({Forma}));
                                        }
                                    });   
                                }
                                else{SAlert("warning", "¡Registro inactivo!", "El registro se encuentra inactivo");}
                        }
                        else if (Operacion==='addSav')
                        {   if (ElementGetValue("feinicio")<ElementGetValue("fefin"))
                                {    if (ElementValidateBeforeSave(valciclo))
                                    {   if (BarButtonState("Inactive"))
                                        {   var Forma = PrepareElements(objciclo);
                                            xajax_Guarda(JSON.stringify({Forma}));
                                        }
                                    }else{SAlert("warning", "¡Cuidado!", "Llene los campos por favor");}
                                }else{SAlert("error", "¡Fechas incorrectas!", "La Fecha Inicial debe ser menor a la Fecha Fin");}
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Operacion);
                                ElementStatus("feinicio:fefin","");
                                ElementClear("id:descripcion:nivel:feinicio:fefin");
                                ElementStatus("id:btcohorte:txcohorte","descripcion:nivel:feinicio:fefin:estado");
                                ElementSetValue("estado",1);
                        }
                        else 
                        {xajax_CargaModal(Operacion);}
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Default");
                            ElementSetValue("id",Datos);
                            ElementStatus("feinicio:fefin","");
                            ElementClear("id:descripcion:nivel:feinicio:fefin");
                            ElementStatus("id:txcohorte:btcohorte","descripcion:nivel:feinicio:fefin:estado");
                            ElementSetValue("estado",1);
                            SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
                        }
                        else if (Mensaje==="CMD_MAE")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                                BarButtonState("Default");
                                ElementStatus("feinicio:fefin","");
                                ElementClear("id:descripcion:nivel:feinicio:fefin");
                                ElementStatus("id:txcohorte","descripcion:nivel:feinicio:fefin:estado");
                                ElementSetValue("estado",1);
                            }
                            else if(ValidateStatusEnabled("addSave")){
                                BarButtonState("Active");
                                ElementStatus("descripcion:nivel:feinicio:fefin","id:estado:txcohorte:btcohorte");
                            }
                            else {
                                BarButtonState("Default");
                                ElementStatus("feinicio:fefin","");
                                ElementClear("id:descripcion:nivel:feinicio:fefin");
                                ElementStatus("id:txcohorte:btcohorte","descripcion:nivel:feinicio:fefin:estado");
                                ElementSetValue("estado",1);
                            }
                        }
                        else if (Mensaje==="CMD_MAE_ERR")
                        {   
                            if (ValidateStatusEnabled("addNew")){
                            }
                            else {
                                ElementStatus("descripcion:nivel:feinicio:fefin","id:estado:txcohorte:btcohorte");
                            }
                        }
                        else if(Mensaje==='CMD_ERR')
                        {   BarButtonState("addNew");
                            SAlert("error", "¡Error!", Datos); 
                        }    
                        else if(Mensaje==='CMD_DEL')
                        {   ElementSetValue("estado",Datos);
                            SAlert("success", "¡Excelente!", "¡Datos guardados correctamente!");
                        }
                        else if(Mensaje==="CMD_WRM")
                        {   BarButtonState("Default");
                            ElementStatus("feinicio:fefin","");
                            ElementClear("id:feinicio:fefin");
                            ElementStatus("","feinicio:fefin");
                            ElementSetValue("estado",1);
                            SAlert("warning", "¡Cuidado!", Datos); 
                        }
                }
                
                function SearchByElement(Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id")
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion");
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState("Inactive")){
                                        var Forma = PrepareElements("id:idcohorte");
                                        xajax_BuscaByIDByCohorte(JSON.stringify({Forma}));
                                    }
                                }
                            }
                            else
                            BarButtonState("Default"); 
                        }
                        else if (Elemento.id === "FindCohorteTextBx")
                        {   xajax_BuscaModalByTX("btcohorte",Elemento.value);  
                        } 
                        else if(Elemento.id === "FindTextBx")
                        {   var Forma = PrepareElements("FindTextBx:idcohorte");
                            xajax_BuscaByTXByCohorte(JSON.stringify({Forma}));
                        }
                }
                
                function SearchCohorteGetData(DatosGrid)
                {
                        document.getElementById("idcohorte").value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txcohorte").value = DatosGrid.cells[2].childNodes[0].nodeValue.toUpperCase();
                        document.getElementById("chorte").value = DatosGrid.cells[4].childNodes[0].nodeValue;
                        document.getElementById("version").value = DatosGrid.cells[5].childNodes[0].nodeValue;
                        cerrar();
                        ElementStatus("feinicio:fefin","");
                        ElementClear("id:descripcion:nivel:feinicio:fefin");
                        ElementStatus("id:txcohorte:btcohorte","descripcion:nivel:feinicio:fefin:estado");
                        ElementSetValue("estado",1);
                        var Forma = PrepareElements(objciclo);
                        xajax_MostrarbyCohorte(JSON.stringify({Forma}));
                        return false;
                }
                 
                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("descripcion").value = Grilla.cells[1].childNodes[0].nodeValue.toUpperCase();
                            document.getElementById("nivel").value = Grilla.cells[2].childNodes[0].nodeValue;
                            var feinicio_entrada = Grilla.cells[5].childNodes[0].nodeValue;
                            var feinicio_format = formato(feinicio_entrada);
                            document.getElementById("feinicio").value = feinicio_format;
                            var fefin_entrada = Grilla.cells[6].childNodes[0].nodeValue;
                            var fefin_format = formato(fefin_entrada);
                            document.getElementById("fefin").value = fefin_format;
                            document.getElementById("estado").value = Grilla.cells[7].childNodes[0].nodeValue; 
                        }
                        return false;
                }
                
                function formato(string) {
                    var info = string.split('/').reverse().join('-');
                    return info;
                }
                 
        </script>
    </head>
        <body>
            
        <div class="FormBasic" style="width:600px">
            <div class="FormSectionMenu">              
            <?php   $ControlCiclo->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState("Inactive"); ';
                    echo '</script>';                    
            ?>    
            </div>  
            
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                    <?php  $ControlCiclo->CargaMantenimiento();  ?>
                    </form>
            </div> 
            
            <div class="FormSectionGrid">          
            <?php   $ControlCiclo->CargaSearchGrid();  ?>
            </div>  
            
        </div>
        </body>
    </html>
         