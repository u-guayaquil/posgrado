<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderCiclo
        {   private $SearchGrid;
            private $Maxlen;    
            
            function __construct()
            {       $this->Maxlen['id'] = 4;        
                    $this->Maxlen['descripcion'] = 50;
                    $this->Maxlen['nivel'] = 2; 
                    $this->Maxlen['fecha'] = 10;  
                    $this->SearchGrid = new SearchGrid();
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']          = array('40px','center','');
                    $Columns['Ciclo']       = array('100px','center',''); 
                    $Columns['Nivel']       = array('60px','center','');
                    $Columns['IdCohorte']   = array('0px','center','none');
                    $Columns['Cohorte']     = array('0px','center','none');
                    $Columns['Inicio']      = array('130px','center','');
                    $Columns['Fin']         = array('130px','center','');
                    $Columns['IdEstado']    = array('0px','center','none');
                    $Columns['Estado']      = array('85px','center','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }
               
            function CreaMantenimiento()
            {      $Search = new SearchInput();
                    $HTML = '<table border=0 class="Form-Frame" cellpadding="0">';                 
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label">Maestría</td>';
                    $HTML.= '           <td class="Form-Label" colspan="4">';
                    $HTML.= $Search->TextSch("cohorte","","")->Enabled(true)->Create("t15");
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '           <td class="Form-Label" style="width:20%">Version</td>';
                    $HTML.= '           <td class="Form-Label">';
                    $HTML.= '                 <input type="text" class="txt-upper t05" id="chorte" name="chorte" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $HTML.= '           </td>';
                    $HTML.= '           <td class="Form-Label">Cohorte</td>';
                    $HTML.= '           <td class="Form-Label">';  
                    $HTML.= '                 <input type="text" class="txt-upper t05" id="version" name="version" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $HTML.= '           </td>';
                    $HTML.= '       </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Id</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                <input type="text" class="txt-input t01" id="id" name="id" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\'descripcion:nivel:feinicio:fefin\',\'NUM\'); " onBlur="return SearchByElement(this);" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';                   
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Ciclo</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                 <input type="text" class="txt-upper t05" id="descripcion" name="descripcion" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '             <td class="Form-Label">Nivel</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                <input type="text" class="txt-input t01" id="nivel" name="nivel" value="" maxlength="'.$this->Maxlen['nivel'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Fecha Inicio</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                <input type="date" class="txt-input t05" id="feinicio" name="feinicio" value="" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '             <td class="Form-Label">Fecha Fin</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                <input type="date" class="txt-input t05" id="fefin" name="fefin" value="" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>'; 
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Estado</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= $this->CreaComboEstadoByArray(array(0,1));
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function CreaBarButton($Buttons)
            {       $BarButton = new BarButton();    
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGrid($ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    return $this->SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
            
            private function GridDataHTML($Grid,$ArDatos)
            {       if ($ArDatos[0])
                    {   foreach ($ArDatos[1] as $ArDato)
                        {       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);                       
                                $Grid->CreaSearchCellsDetalle($id,'',$id);                     
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['CICLO'])));
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['NIVEL']);
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDCOHORTE']);
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['COHORTE'])));
                                $FeInicioBD = $ArDato['INICIO'];
                                $Grid->CreaSearchCellsDetalle($id,'',$FeInicioBD->format('d/m/Y'));
                                $FeFinBD = $ArDato['FIN'];
                                $Grid->CreaSearchCellsDetalle($id,'',$FeFinBD->format('d/m/Y'));
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));
                                $Grid->CreaSearchRowsDetalle ($id,($ArDato['IDESTADO']==0 ? "red" : ""));
                        }
                    }    
                    return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
            }
               
            function MuestraFormulario($Ajax,$Ciclo)
            {   if ($Ciclo)
                    {   if ($Ciclo->ID>=0)
                        {   $Ajax->Assign("id","value", str_pad($Ciclo->ID,$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion","value", utf8_encode(trim($Ciclo->CICLO)));
                            $Ajax->Assign("nivel","value", $Ciclo->NIVEL);
                            $FeInicioBD = $Ciclo->INICIO;
                            $Ajax->Assign("feinicio","value", $FeInicioBD->format('Y-m-d'));
                            $FeFinBD = $Ciclo->FIN;
                            $Ajax->Assign("fefin","value", $FeFinBD->format('Y-m-d'));
                            $Ajax->Assign("estado","value", $Ciclo->IDESTADO);
                            $Ajax->call("BarButtonState","Active");
                            return $Ajax;
                        }    
                    }
                    return $this->Respuesta($Ajax,"CMD_WRM","No existe una Ciclo con el ID ingresado.");
            }
            
            function MuestraGuardado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);
            }
            
            function MuestraGrid($Ajax,$ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    $Ajax->Assign($this->SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }
            
            function MuestraEditado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraRowGrid($Ajax, $ArDatos[1][0]);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }    
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);            
            }
    
            function MuestraRowGrid($Ajax,$ArDato)
            {       $RowId = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$ArDato['IDESTADO']);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);      
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",utf8_encode(trim($ArDato['CICLO'])));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",$ArDato['NIVEL']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$ArDato['IDCOHORTE']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",utf8_encode(trim($ArDato['COHORTE']))); 
                    $FeInicioBD = $ArDato['INICIO'];
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$FeInicioBD->format('d/m/Y'));
                    $FeFinBD = $ArDato['FIN'];
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$FeFinBD->format('d/m/Y'));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$ArDato['IDESTADO']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));

                    return $Ajax;
            }                                
            
            function MuestraEliminado($Ajax,$JsDatos)
            {       $RowId = str_pad($JsDatos->id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$JsDatos->estado);
                    
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);       
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$JsDatos->estado); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML","INACTIVO"); 
                    return $this->Respuesta($Ajax,"CMD_DEL",0);
            }

            private function Respuesta($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse";
                    $Ajax->call($jsFuncion,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox();
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Enabled(false)->Fields("id","descripcion")->Create("s05");
            }   
             
            
            function MuestraEditadobyCohorte($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$id);
                    }
                else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_MAE_ERR",$ArDatos[1]);            
            }
            
            function MuestrabyCohorte($Ajax,$ArDatos)
            {     
                if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$id);
                    }
                    else if($ArDatos[1])
                    {                       
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_MAE",$ArDatos[1]);
                    }
                    return $this->Respuesta($Ajax,"CMD_MAE_ERR",$ArDatos[1]);            
            }
            
            /*** MODAL ***/
                private function SearchGridModalConfig()
                {       
                        $Columns['IdCohorte']   = array('0px','center','none');
                        $Columns['IdMaestria']  = array('0px','center','none');
                        $Columns['Maestria']    = array('325px','left','');
                        $Columns['IdVersion']   = array('0px','center','none');
                        $Columns['Version']     = array('60px','left','');
                        $Columns['Cohorte']     = array('120px','left','');
                        $Columns['Inicio']      = array('70px','left','');
                        $Columns['Fin']         = array('70px','left','');
                        $Columns['IdEstado']    = array('0px','left','none');
                        $Columns['Estado']      = array('70px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$ArDatos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcion($SearchGrid,$ArDatos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalOpcion($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalOpcion($Grid,$ArDatos)
                {       if ($ArDatos[0])
                        {   foreach ($ArDatos[1] as $ArDato)
                            {       
                                    $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                    $Grid->CreaSearchCellsDetalle($id,'',$id);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMAESTRIA']);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDMAESVER']);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['VERSION'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['COHORTE'])));
                                    $InicioBD = $ArDato['FEINICIO'];
                                    $Grid->CreaSearchCellsDetalle($id,'',$InicioBD->format('d/m/Y'));
                                    $FinBD = $ArDato['FEFIN'];
                                    $Grid->CreaSearchCellsDetalle($id,'',$FinBD->format('d/m/Y'));
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                    $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDESTADO']==1 ? "ACTIVO" : "INACTIVO"));   
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }
        }
?>

