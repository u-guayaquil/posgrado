<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/maestria/servicio/ServicioCiclo.php");
        require_once("src/modules/maestria/Ciclo/render/RenderCiclo.php");

        class ControlCiclo
        {       private  $ServicioCiclo;
                private  $RenderCiclo;

                function __construct()
                {       $this->ServicioCiclo = new ServicioCiclo();
                        $this->RenderCiclo = new RenderCiclo();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderCiclo->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderCiclo->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => 'NULL');
                        $Datos = $this->ServicioCiclo->BuscarCicloByDescripcion($prepareDQL);
                        echo $this->RenderCiclo->CreaSearchGrid($Datos);
                }         
               
                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioCiclo->BuscarCicloByID($id);
                        return $this->RenderCiclo->MuestraFormulario($ajaxRespon,$Datos);
                }
                
                function ConsultaByIDByCohorte($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('id' => ($JsDatos -> id), 'idcohorte' => $JsDatos->idcohorte);
                        $ArDatos = $this->ServicioCiclo->BuscarCicloByIDByCohorte($prepareDQL);
                        return $this->RenderCiclo->MuestraFormulario($ajaxRespon,$ArDatos);
                }

                function ConsultaByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();              
                        $prepareDQL = array('texto' => strtoupper(trim($Texto)));
                        $ArDatos = $this->ServicioCiclo->BuscarCicloByDescripcion($prepareDQL);
                        return $this->RenderCiclo->MuestraGrid($ajaxRespon,$ArDatos);
                }
                
                function ConsultaByTXByCohorte($Form)
                {       $ajaxRespon = new xajaxResponse(); 
                        $JsDatos = json_decode($Form)->Forma;
                        $prepareDQL = array('texto' => strtoupper(trim($JsDatos -> FindTextBx)), 'idcohorte' => $JsDatos->idcohorte );
                        $ArDatos = $this->ServicioCiclo->BuscarCicloByDescripcionByCohorte($prepareDQL);
                        return $this->RenderCiclo->MuestraGrid($ajaxRespon,$ArDatos);
                }

                function Guarda($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");
                        if(empty($JsDatos->id))
                        {   $FechaValidada = $this->ValidarFechas($JsDatos->feinicio, $JsDatos->idcohorte);
                            if ($FechaValidada==="true")
                            {   $FechaRangoCohorte = $this->ValidarRangoCohorte($JsDatos->feinicio, $JsDatos->fefin, $JsDatos->idcohorte);
                                if($FechaRangoCohorte==="true")
                                {   $id = $this->ServicioCiclo->GuardaDBCiclo($JsDatos);
                                    $ArDatos = $this->ServicioCiclo->BuscarCicloEstadoByID($id);
                                    return $this->RenderCiclo->{$funcion}($ajaxRespon,$ArDatos);
                                }else{  $jsFuncion = "XAJAXResponse";
                                $ajaxRespon->call($jsFuncion,"CMD_ERR","Las Fechas de INICIO y FIN del CICLO deben de estar dentro del rango de DURACION del COHORTE Seleccionado");
                                return $ajaxRespon;
                                }          
                            }else{  $jsFuncion = "XAJAXResponse";
                                $ajaxRespon->call($jsFuncion,"CMD_ERR","La FECHA INICIAL debe ser mayor a la FECHA FIN del ULTIMO CICLO.");
                                return $ajaxRespon;
                            }
                        }else{  $id = $this->ServicioCiclo->GuardaDBCiclo($JsDatos);
                                $ArDatos = $this->ServicioCiclo->BuscarCicloEstadoByID($id);
                                return $this->RenderCiclo->{$funcion}($ajaxRespon,$ArDatos);
                        }    
                }
                
                function ValidarFechas($FeInicio,$idCohorte)
                {   $FechaInicio = $FeInicio;
                    $ArD = $this->ServicioCiclo->BuscarFechaFinUltimoCiclo($idCohorte);
                    if($ArD)
                    {   $FechaFinUltimoCiclo = $ArD->FEFIN;
                        $FechaFinUltima =  $FechaFinUltimoCiclo->format('Y-m-d');
                        if ($FechaInicio>$FechaFinUltima)
                        {  return "true";  }
                    }
                    else {  return "true";  }
                }
                
                function ValidarRangoCohorte($FeInicio,$FeFin, $idCohorte)
                {
                    $ArD = $this->ServicioCiclo->BuscarRangoCicloByCohorte($idCohorte);
                    if($ArD)
                    {   $FechaInicioCohorte = $ArD->FEINICIO;
                        $FeIniCohorte = $FechaInicioCohorte->format('Y-m-d');
                        $FechaFinCohorte = $ArD->FEFIN;
                        $FeFinCohorte = $FechaFinCohorte->format('Y-m-d');
                        if ($FeInicio>=$FeIniCohorte && $FeFin <=$FeFinCohorte)
                        { return "true";  }
                        else { return "false";  }
                    } 
                }

                function Elimina($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsData = json_decode($Form)->Forma;
                        $JsData->estado = $this->ServicioCiclo->DesactivaCiclo($JsData->id);
                        return $this->RenderCiclo->MuestraEliminado($ajaxRespon,$JsData);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1));
                        if ($Operacion==="btcohorte")    
                        {                            
                            $ArDatos = $this->ServicioCiclo->BuscarCohorteByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cohortes";
                            $jsonModal['Carga'] = $this->RenderCiclo->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "750";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        if ($Operacion==="btcohorte")    
                        {   $Datos = $this->ServicioCiclo->BuscarCohorteByDescripcion($prepareDQL);
                            return $this->RenderCiclo->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
                
                function MostrarCiclobyCohorte($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->txcohorte) ? "MuestrabyCohorte" : "MuestraEditadobyCohorte");
                        $idCohorte = $JsDatos->idcohorte;
                        $ArDatos = $this->ServicioCiclo->BuscarCicloEstadoByCohorte($idCohorte);
                        return $this->RenderCiclo->{$funcion}($ajaxRespon,$ArDatos);
                }
        }       
?>