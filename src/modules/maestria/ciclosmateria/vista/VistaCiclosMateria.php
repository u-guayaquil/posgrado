<?php
        $Opcion = 'CiclosMateria';
        require_once('src/modules/maestria/CiclosMateria/controlador/ControlCiclosMateria.php');
        $ControlCiclosMateria = new ControlCiclosMateria();

        $xajax->register(XAJAX_FUNCTION,array('CargaWorkGrid', $ControlCiclosMateria,'CargaWorkGrid'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlCiclosMateria,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaMaterias', $ControlCiclosMateria,'ConsultaMaterias'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlCiclosMateria,'ConsultaGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('GuardarDB', $ControlCiclosMateria,'GuardarDB'));
        $xajax->register(XAJAX_FUNCTION,array('ActualizarDB', $ControlCiclosMateria,'ActualizarDB'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaHistorico', $ControlCiclosMateria,'GuardaHistorico'));
        $xajax->register(XAJAX_FUNCTION,array('CambiarEstado', $ControlCiclosMateria,'CambiarEstado'));
        
        $xajax->processRequest();
        
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/maestria/CiclosMateria/js/jsCiclosMateria.js?1"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:1180px">
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionMenu">              
                        <?php   $ControlCiclosMateria->CargaBarButton($_GET['opcion']);
                                echo '<script type="text/javascript">';
                                echo '        BarButtonState("addPDF"); ';
                                echo '</script>';
                        ?>
                    </div>
                        <div class="FormSectionData">   
                             <?php  $ControlCiclosMateria->CargaMantenimiento();  ?>
                        </div> 
                        <div id="<?php echo 'WrkGrd'; ?>" class="FormSectionGrid">  
                        </div>     
                    </form>
                </div>       
        </body>
        </html>
              

