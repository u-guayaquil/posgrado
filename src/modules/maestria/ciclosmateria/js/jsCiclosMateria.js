$(document).ready(function()
{       xajax_CargaWorkGrid("WrkGrd");  
});

var Objto;
var Elemn;
var idCelda;
var CAMBIO;
var COLINI = 5;
var ActFila;
var Datos;
var Params;
var Row;
var TemporalID;

function ButtonClick(Operacion)
{       if (Operacion=="btmaestria")
        {   xajax_CargaModal(Operacion,0);
        }
        else if (Operacion=="btciclo")
        {    var IDMST=ElementGetValue("idmaestria");
             var IDVMT=ElementGetValue("idversion");
             var IDCOH=ElementGetValue("idcohorte");
             var Params = [IDMST, IDVMT, IDCOH];
             if (IDCOH!="" && IDMST!="" && IDVMT!="")
                xajax_CargaModal(Operacion,Params);
             else
                SAlert("warning","¡Error!","¡Debe seleccionar una Cohorte!");
                //alert("Debe seleccionar un Cohorte.");
        }
        else if (Operacion=="btcohorte")
        {    var IDMST=ElementGetValue("idmaestria");
             var IDVMT=ElementGetValue("idversion");
             var Params = [IDMST, IDVMT];
             if (IDMST!="" && IDVMT!="")
                xajax_CargaModal(Operacion,Params);
             else
                SAlert("warning","¡Error!","¡Debe seleccionar una Maestría!");
                //alert("Debe seleccionar una Maestria.");
        }
        return false;
}

function SearchMaestriaGetData(DatosGrid)
{       var IDCOH = DatosGrid.cells[2].innerHTML;
        if (ElementGetValue("idmaestria")!=IDCOH)
        {   ElementClear("feinicio:fefin");
            ElementStatus("","MinBusc:feinicio:fefin");
            DeleteWorkGrid("FX");
            ElementClear("idcohorte:txcohorte:idpolitica:idmalla:idciclo:txciclo:idnivel");
            document.getElementById("idfacultad").value = DatosGrid.cells[0].innerHTML;
            document.getElementById("txfacultad").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idmaestria").value = DatosGrid.cells[2].innerHTML;
            document.getElementById("txmaestria").value = DatosGrid.cells[3].innerHTML;
            document.getElementById("idversion").value = DatosGrid.cells[4].innerHTML;
            cerrar();
        }    
        else
        cerrar();
        return false;
}

function SearchCohorteGetData(DatosGrid)
{       var IDCOH = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("idcohorte")!=IDCOH)
        {   ElementClear("feinicio:fefin");
            ElementStatus("","MinBusc:feinicio:fefin");
            DeleteWorkGrid("FX");
            ElementClear("idciclo:txciclo:idnivel");
            document.getElementById("idcohorte").value = IDCOH;
            document.getElementById("txcohorte").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idpolitica").value = DatosGrid.cells[2].innerHTML;
            document.getElementById("idmalla").value = DatosGrid.cells[3].innerHTML;
            cerrar();
        }    
        else
        cerrar();
        return false;
}

function SearchCicloGetData(DatosGrid)
{       var IDCCL = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("idcohorte")!=IDCCL)
        {   ElementStatus("MinBusc","feinicio:fefin");
            DeleteWorkGrid("FX");
            document.getElementById("idciclo").value = IDCCL;
            document.getElementById("txciclo").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idnivel").value = DatosGrid.cells[2].innerHTML;
            var feinicio_entrada = DatosGrid.cells[3].innerHTML;
            var feinicio_format = formato(feinicio_entrada);
            document.getElementById("feinicio").value = feinicio_format;
            var fefin_entrada = DatosGrid.cells[4].innerHTML;
            var fefin_format = formato(fefin_entrada);
            document.getElementById("fefin").value = fefin_format;
            cerrar();
        }
        else
        cerrar();    
        return false;
}  

function SearchDatosGetData()
{   ElementStatus("MinBusc","");
    DeleteWorkGrid("FX");
    COHORTE=ElementGetValue("idcohorte");
    MALLA=ElementGetValue("idmalla");
    FACULTAD=ElementGetValue("idfacultad");
    POLEVA=ElementGetValue("idpolitica");
    CICLO=ElementGetValue("idciclo");   
    NIVEL=ElementGetValue("idnivel");
    FEINICIO=ElementGetValue("feinicio");
    FEFIN=ElementGetValue("fefin");
    var Params = [COHORTE,MALLA,FACULTAD,POLEVA,CICLO,NIVEL,FEINICIO,FEFIN];
    if(COHORTE=="" || MALLA=="" || FACULTAD=="" || POLEVA=="" || CICLO=="" || NIVEL == "" || FEINICIO=="" || FEFIN=="")
    {   SAlert("warning","¡Error!","¡Ingrese todos los datos por favor!");
    }   else{
        if(FEINICIO < FEFIN){
            xajax_BuscaMaterias(Params);
        }else{
            SAlert("warning","¡Error!","¡La fecha final debe ser mayor a la fecha inicial!");
        }
    }
}  

function SearchByElement(Elemento)
{       if (Elemento.id=="FindMaestriaTextBx")
        {    xajax_BuscaModalByTX("btmaestria",Elemento.value);
        }
        else if (Elemento.id=="FindCohorteTextBx")
        {   var Params = [Elemento.value,ElementGetValue("idmaestria"),ElementGetValue("idversion")]; 
            xajax_BuscaModalByTX("btcohorte",Params);
        }
        else if (Elemento.id=="FindCicloTextBx")
        {    var Params = [Elemento.value,ElementGetValue("idmaestria"),ElementGetValue("idversion"),ElementGetValue("idcohorte")];
             xajax_BuscaModalByTX("btciclo",Params);
        }
        return false;    
}

function formato(string) 
{       var info = string.split('/').reverse().join('-');
        return info;
}

function WorkGridSaveFX(Grilla)
{       ActFila = Grilla;
        COHORTE=ElementGetValue("idcohorte");
        MALLA=ElementGetValue("idmalla");
        FACULTAD=ElementGetValue("idfacultad");
        POLEVA=ElementGetValue("idpolitica");
        CICLO=ElementGetValue("idciclo");   
        NIVEL=ElementGetValue("idnivel");
        FEINI=ElementGetValue("feinicio"); 
        FFIN=ElementGetValue("fefin");
        
        Params = [COHORTE,MALLA,FACULTAD,POLEVA,CICLO,NIVEL,FEINI,FFIN];
        
        var CicloMat = Grilla.cells[0].innerHTML;
        Row = Grilla.cells[1].innerHTML;
        var MateriaID = Grilla.cells[2].innerHTML;
        var Materia = Grilla.cells[3].innerHTML;
        var MallaDetID = Grilla.cells[4].innerHTML;
        
        var Dcnt = document.getElementById("docente_"+StrPadCeroNumber("00",Row));
        var DocntID = Dcnt.options[Dcnt.selectedIndex].value;
        
        var Grpo = document.getElementById("grupo_"+StrPadCeroNumber("00",Row));
        var GrupoID = Grpo.options[Grpo.selectedIndex].value;
        
        var FeInicio =  ElementGetValue("feinicio_"+StrPadCeroNumber("00",Row));
        var FeFin = ElementGetValue("fefin_"+StrPadCeroNumber("00",Row));
        
        var EstadoID =  Grilla.cells[9].innerHTML;
        
        Datos = [CicloMat,Row,MateriaID,Materia,MallaDetID,DocntID,GrupoID,FeInicio,FeFin,EstadoID];
        
        var RegistroActualHabilitado = ValidarRegistroHabilitado(Row);
        
        if (RegistroActualHabilitado > 0)
        {   if(DocntID != 0){
                if(GrupoID != 0){
                    if(FeInicio < FeFin)
                    {   if(FEINI <= FeInicio && FeFin <= FFIN)
                        {   if(EstadoID == 2){
                                ElementStatus("MinBusc","");
                                xajax_ActualizarDB(Params,Datos,TemporalID);
                            } 
                            else if(EstadoID == 1  || EstadoID == 0){
                                ElementStatus("MinBusc","");
                                xajax_GuardarDB(Params,Datos,TemporalID);
                            }
                        }
                        else{   SAlert("warning","¡Error!","¡Fechas incorrectas en comparación al ciclo!");}
                    }else{   SAlert("warning","¡Error!","¡La fecha inicio debe ser menor a la fecha fin!");}
                }else{  SAlert("warning","¡Error!","¡Seleccione un Grupo por favor!");}
            }else{  SAlert("warning","¡Error!","¡Seleccione un Docente por favor!");}
        }else{  SAlert("warning","¡Error!","¡El registro está inabilitado, presione editar por favor!");}
        
        return false;
}

function WorkGridEditFX(Grilla)
{       ActFila = Grilla;
        
        COHORTE=ElementGetValue("idcohorte");
        MALLA=ElementGetValue("idmalla");
        FACULTAD=ElementGetValue("idfacultad");
        POLEVA=ElementGetValue("idpolitica");
        CICLO=ElementGetValue("idciclo");   
        NIVEL=ElementGetValue("idnivel");
        FEINI=ElementGetValue("feinicio"); 
        FFIN=ElementGetValue("fefin");
        
        Params = [COHORTE,MALLA,FACULTAD,POLEVA,CICLO,NIVEL,FEINI,FFIN];
        
        var CicloMat = Grilla.cells[0].innerHTML;
        Row = Grilla.cells[1].innerHTML;
        var MateriaID = Grilla.cells[2].innerHTML;
        var Materia = Grilla.cells[3].innerHTML;
        var MallaDetID = Grilla.cells[4].innerHTML;
        
        var Dcnt = document.getElementById("docente_"+StrPadCeroNumber("00",Row));
        var DocntID = Dcnt.options[Dcnt.selectedIndex].value;
        
        var Grpo = document.getElementById("grupo_"+StrPadCeroNumber("00",Row));
        var GrupoID = Grpo.options[Grpo.selectedIndex].value;
        
        var FeInicio =  ElementGetValue("feinicio_"+StrPadCeroNumber("00",Row));
        var FeFin = ElementGetValue("fefin_"+StrPadCeroNumber("00",Row));
        
        var EstadoID =  Grilla.cells[9].innerHTML;
                            
        var GrpTx = document.getElementById("grupo_"+StrPadCeroNumber("00",Row));
        var GrupoTxt = GrpTx.options[GrpTx.selectedIndex].text;
        
        Datos = [CicloMat,Row,MateriaID,Materia,MallaDetID,DocntID,GrupoID,FeInicio,FeFin,EstadoID,GrupoTxt];
        
        var RegistroActualHabilitado = ValidarRegistroHabilitado(Row);
        
        var tabla = document.getElementById("FX");
        var OtroRegistroHabilitado = PreparaDatosGrid(tabla.rows.length);
        
        if(OtroRegistroHabilitado > 0)
        {    SAlert("warning","¡Cuidado!","¡Existe un registro habilitado!");}
        else
        {   if (RegistroActualHabilitado > 0)
            {   SAlert("warning","¡Cuidado!","¡El registro está habilitado!");}
            else{
                if(CicloMat == 0)
                    {   SAlert("success","¡Excelente!","¡Registro Habilitado!");
                        ElementStatus("docente_"+StrPadCeroNumber("00",Row),"");
                        ElementStatus("grupo_"+StrPadCeroNumber("00",Row),"");
                        ElementStatus("feinicio_"+StrPadCeroNumber("00",Row),"");
                        ElementStatus("fefin_"+StrPadCeroNumber("00",Row),"");
                        ElementStatus("","MinBusc");
                    }
                else
                    {   xajax_CargaModal("jModal",Datos);
                    }
            }
        } 
        return false;
}

function WorkGridDeclineFX(Grilla)
{       ActFila = Grilla;       
        var CicloMat = Grilla.cells[0].innerHTML;
        Row = Grilla.cells[1].innerHTML;
        
        var RegistroActualHabilitado = ValidarRegistroHabilitado(Row);
        
        if(RegistroActualHabilitado > 0)
        {   if(CicloMat>0)    
            {       xajax_CambiarEstado(CicloMat,TemporalID);   
            }  
            else{   SAlert("success","¡Excelente!","¡Registro inhabilitado!");
                    ElementStatus("","docente_"+StrPadCeroNumber("00",Row));
                    ElementStatus("","grupo_"+StrPadCeroNumber("00",Row));
                    ElementStatus("","feinicio_"+StrPadCeroNumber("00",Row));
                    ElementStatus("","fefin_"+StrPadCeroNumber("00",Row));
                    ElementStatus("MinBusc:btmaestria:btcohorte:btciclo","");

                    ElementSetValue("docente_"+StrPadCeroNumber("00",Row),0);
                    ElementSetValue("grupo_"+StrPadCeroNumber("00",Row),0);
                    var fechaDefault = formato("01/01/1999");
                    ElementSetValue("feinicio_"+StrPadCeroNumber("00",Row),fechaDefault);
                    ElementSetValue("fefin_"+StrPadCeroNumber("00",Row),fechaDefault);
            }
        }else{   SAlert("warning","¡Error!","¡El registro está inabilitado, presione editar por favor!");}
}

function GuardarHistorico(Texto)
    {       Texto.value = TrimElement(Texto.value);
            var opt = Texto.value.split(" ");
            if (Texto.value!="")
            {   if(opt.length >= 3)
                {   ElementStatus("docente_"+StrPadCeroNumber("00",Row),"");
                    ElementStatus("grupo_"+StrPadCeroNumber("00",Row),"");
                    ElementStatus("feinicio_"+StrPadCeroNumber("00",Row),"");
                    ElementStatus("fefin_"+StrPadCeroNumber("00",Row),"");
                    ElementStatus("","MinBusc");
                    xajax_GuardaHistorico(Params,Datos,Texto.value.toUpperCase());
                    cerrar();
                }   else
                {   SAlert("warning","¡Error!","¡Debe ingresar mínimo 4 palabras!");
                }
            }
            else
            SAlert("warning","¡Error!","¡Debe ingresar un motivo!"); 
            return false;
    }
    
    function XAJAXResponse(Mensaje,Datos)
    {       if (Mensaje==="CMD_SAV")
            {   //alert(Datos);   
                SAlert("success","¡Excelente!",Datos);  
                DeleteWorkGrid("FX");
                ElementStatus("MinBusc","");
                xajax_BuscaMaterias(Params);                            
            }
            if (Mensaje==="CMD_ERROR")
            {   SAlert("warning","¡Error!",Datos); 
                DeleteWorkGrid("FX");
                xajax_BuscaMaterias(Params);                            
            }
            if (Mensaje==="CMD_NON")
            {   SAlert("error","¡Error!",Datos); 
                DeleteWorkGrid("FX");
                xajax_BuscaMaterias(Params);                            
            }
            if (Mensaje==="CMD_NEW_REG")
            {   TemporalID=Datos;      
                SAlert("success","¡Excelente!","Registro Habilitado");                     
            }
            if (Mensaje==="CMD_INF")
            {   SAlert("info", "¡Información!", Datos);
            }
    }

function PreparaDatosGrid(Elementos)
{   var Habilitados = 0;    
    for (t=1;t<=Elementos;t++)
        {   if (ValidateStatusEnabled("docente_"+StrPadCeroNumber("00",t)))
            {   Habilitados++;   }
            if (ValidateStatusEnabled("grupo_"+StrPadCeroNumber("00",t)))
            {   Habilitados++;   }
            if (ValidateStatusEnabled("feinicio_"+StrPadCeroNumber("00",t)))
            {   Habilitados++;   }
            if (ValidateStatusEnabled("fefin_"+StrPadCeroNumber("00",t)))
            {   Habilitados++;  }
        }
        return Habilitados;         
}

function ValidarRegistroHabilitado(col)
{           var Habilitados = 0;    
            if (ValidateStatusEnabled("docente_"+StrPadCeroNumber("00",col)))
            {   Habilitados++;   }
            if (ValidateStatusEnabled("grupo_"+StrPadCeroNumber("00",col)))
            {   Habilitados++;   }
            if (ValidateStatusEnabled("feinicio_"+StrPadCeroNumber("00",col)))
            {   Habilitados++;   }
            if (ValidateStatusEnabled("fefin_"+StrPadCeroNumber("00",col)))
            {   Habilitados++;   }
    return Habilitados;         
}

/*function XAJAXResponse(Mensaje,Datos)
{   if (Mensaje==="CMD_SAV")
    {   BarButtonState("Default");
        ElementSetValue("id",Datos);
        ElementClear("id:descripcion");
        ElementStatus("id:btmaestria:txmaestria","descripcion:estado");
        ElementSetValue("estado",1);
        SAlert("success", "¡Excelente!", "Los datos se guardaron correctamente.");
    }
    if (Mensaje==="CMD_INF")
    {   SAlert("info", "¡Información!", Datos);
    }
}*/