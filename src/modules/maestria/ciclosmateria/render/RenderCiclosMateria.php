<?php   require_once("src/rules/maestria/servicio/ServicioCiclosMateria.php");
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php"); 
        require_once('src/libs/clases/ComboBox.php');
                
        class RenderCiclosMateria
        {   private $ServicioCiclosMateria;  
            private $Width;
              private $SearchGrid;
              private $Search;
              private $Columnas;
              private $Works;
              private $Col1="25%"; 
              private $Col2="25%"; 
              private $Col3="25%"; 
              private $Col4="25%";
              private $Alto="22px";
              private $Maxlen;
              
                function __construct()
                {   $this->ServicioCiclosMateria = new ServicioCiclosMateria();    
                    $this->Search = new SearchInput();   
                    $this->SearchGrid = new SearchGrid();
                    $this->Maxlen['fecha'] = 10; 
                }

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaMantenimiento()
                {      
                         $Col2="30%"; $Col3="13%";  $Col6="10%";

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="35">';
                        $HTML.= '           <td class="Form-Label" colspan="6">:: CICLOS DE MATERIAS</td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col6.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" >';
                        $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t11"); 
                        $HTML.= '               <input type="hidden" id="idversion" name="idversion" value="">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col6.'">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col6.'">Facultad</td>';
                        $HTML.= '           <td class="Form-Label" colspan="4">';
                        $HTML.= '               <input type="hidden" id="idfacultad" name="idfacultad" value="">';
                        $HTML.= '               <input type="text" class="txt-input" style="width:430px" id="txfacultad" name="txfacultad" value="" readOnly/> '; 
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';       
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col6.'">Cohorte</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("cohorte","","")->Enabled(true)->Create("t11");   
                        $HTML.= '               <input type="hidden" id="idpolitica" name="idpolitica" value="">';
                        $HTML.= '               <input type="hidden" id="idmalla" name="idmalla" value="">';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>'; 
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col6.'">Ciclo</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';                    
                        $HTML.=                 $this->Search->TextSch("ciclo","","")->Enabled(true)->Create("t11");
                        $HTML.= '               <input type="hidden" id="idnivel" name="idnivel" value="" />';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col6.'">';
                        $HTML.= '           </td>';
                        $HTML.= '             <td class="Form-Label" style="'.$Col6.'">Fecha Inicio</td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col6.'">';
                        $HTML.= '                <input type="date" class="txt-input t05" id="feinicio" name="feinicio" value="" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                        $HTML.= '             </td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col6.'">Fecha Fin</td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col6.'">';
                        $HTML.= '                <input type="date" class="txt-input t05" id="fefin" name="fefin" value="" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " disabled/>';
                        $HTML.= '             </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col6.'">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col3.'">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col6.'">';
                        $HTML.='                <input type="button" id="MinBusc" class="p-txt-label-bold" style="height: 35px;text-decoration: none;font-weight: 600;font-size: 15px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin-top:10px;" value="✔ Consultar" onclick=" return SearchDatosGetData();" disabled>';
                        //$HTML.='                <input type="button" id="MinSav" class="p-txt-label-bold" style="border: 1px solid #aaa; width: 80px; height: 25px" value="✔ Guardar" onclick=" return ButtonClick(\'MinSav\');" disabled>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col6.'">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col3.'">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col6.'">';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label-Center" style="width:'.$Col6.'">';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        return $HTML.'</table>';
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btmaestria")
                        {   $Columns['idFacultad'] = array('0px','center','none');
                            $Columns['Facultad'] = array('300px','left','');
                            $Columns['idMaestria'] = array('0px','center','none');
                            $Columns['Maestria'] = array('300px','left','');
                            $Columns['idVersion'] = array('0px','center','none');
                            $Columns['Version'] = array('100px','center','');
                        }  
                        else if ($Operacion=="btcohorte")
                        {   $Columns['idCohorte'] = array('100px','center','');
                            $Columns['Cohorte'] = array('200px','center','');
                            $Columns['idPolitica'] = array('0px','center','none');
                            $Columns['idMalla'] = array('0px','center','none');
                        }  
                        else if ($Operacion=="btciclo")
                        {   $Columns['Id']=array('40px','center','');
                            $Columns['Ciclo']=array('150px','center','');
                            $Columns['Nivel']=array('100px','center','');
                            $Columns['Inicio']=array('100px','center','');
                            $Columns['Fin']=array('100px','center','');
                        }                         
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }   
                
                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btciclo")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['ID'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['CICLO']));
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['NIVEL']));
                                        $FeInicioBD = $ArDato['FEINICIO'];
                                        $Grid->CreaSearchCellsDetalle($id,'',$FeInicioBD->format('d/m/Y'));
                                        $FeFinBD = $ArDato['FEFIN'];
                                        $Grid->CreaSearchCellsDetalle($id,'',$FeFinBD->format('d/m/Y'));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btcohorte")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['IDCOHORTE'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['COHORTE'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['IDPOLITICA']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['IDMALLA']);
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }     
                            elseif ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['IDMAESTRIA'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['IDFACULTAD']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['FACULTAD'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['MAESTRIA'])));
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['IDVERSION']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['VERSION'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }    
  
                private function CreaComboDocenteByFacultad($Params,$num,$id)
                {       $Select = new ComboBox();
                        $Datos = $this->ServicioCiclosMateria->BuscarDocenteByFacultad($Params);
                        if($num > 9){
                            $name = str_pad("docente_",10,$num,STR_PAD_RIGHT);
                            return $Select->Combo($name,$Datos)->Fields("ID","DOCENTE")->Selected($id)->Enabled(false)->Evento()->Create("s09");
                        } else 
                        {   $name = str_pad("docente_",10,'0'.$num,STR_PAD_RIGHT);
                            return $Select->Combo($name,$Datos)->Fields("ID","DOCENTE")->Selected($id)->Enabled(false)->Evento()->Create("s09");
                        }
                }
                
                private function CreaComboGrupoByCohorte($Params,$num,$id)
                {       $Select = new ComboBox();
                        $Datos = $this->ServicioCiclosMateria->BuscarParaleloByCohorte($Params);
                        if($num > 9){
                            $name = str_pad("grupo_",8,$num,STR_PAD_RIGHT);
                            return $Select->Combo($name,$Datos)->Fields("ID","PARALELO")->Selected($id)->Enabled(false)->Evento()->Create("s04");
                        } else 
                        {   $name = str_pad("grupo_",8,'0'.$num,STR_PAD_RIGHT);
                            return $Select->Combo($name,$Datos)->Fields("ID","PARALELO")->Selected($id)->Enabled(false)->Evento()->Create("s04");
                        }      
                }
                
                private function CreaCampoFechaInicio($num,$feinicio)
                {       if($num > 9)
                        {   $name = str_pad("feinicio_",11,$num,STR_PAD_RIGHT);
                            $HTML = '<input type="date" class="txt-input t04" id="'.$name.'" name="'.$name.'" value="'.$feinicio.'" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " onchange="return ControlFechaInicio(this,feinicio_0'.$num.');" disabled/>';
                            return $HTML;
                        } else 
                        {   $name = str_pad("feinicio_",11,'0'.$num,STR_PAD_RIGHT);
                            $HTML = '<input type="date" class="txt-input t04" id="'.$name.'" name="'.$name.'" value="'.$feinicio.'" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " onchange="return ControlFechaInicio(this,feinicio_0'.$num.');" disabled/>';
                            return $HTML;
                        }
                }
                
                private function CreaCampoFechaFin($num,$fefin)
                {       if($num > 9)
                        {       $name = str_pad("fefin_",8,$num,STR_PAD_RIGHT);
                                $HTML = '<input type="date" class="txt-input t04" id="'.$name.'" name="'.$num.'" value="'.$fefin.'" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " onchange="return ControlFechaFin(this);" disabled/>';
                        return $HTML;
                        } else 
                        {       $name = str_pad("fefin_",8,'0'.$num,STR_PAD_RIGHT);
                                $HTML = '<input type="date" class="txt-input t04" id="'.$name.'" name="'.$num.'" value="'.$fefin.'" maxlength="'.$this->Maxlen['fecha'].'" onKeyDown="return Dependencia(event,\'\',\'NUM\'); " onchange="return ControlFechaFin(this);" disabled/>';
                                return $HTML;
                        }
                }
                
                private function WorkGridCellValues()
                {       $Columns[0] = array(0,'left','idCicMat','');
                        $Columns[1] = array(50,'center','No','');
                        $Columns[2] = array(0,'center','idMateria','');             
                        $Columns[3] = array(250,'left','Materia','');
                        $Columns[4] = array(0,'left','idMallaDet','');
                        $Columns[5] = array(250,'center','Docente','');
                        $Columns[6] = array(150,'center','Grupo','');
                        $Columns[7] = array(110,'center','Inicio','');
                        $Columns[8] = array(110,'center','Fin','');
                        $Columns[9] = array(0,'left','idEstado','');
                        $Columns[10] = array(100,'center','Estado','');
                        return $Columns;
                }
            
                function CreaWorkGrid($Ajax,$Capa)                
                {       $WorkGrid["Capa"] = $Capa;
                        $WorkGrid["Hcab"] = "40";
                        $WorkGrid["Hcob"] = "0";
                        $WorkGrid["Hdet"] = "250";
                        $WorkGrid["Sufy"] = "";
                        $WorkGrid["Name"] = "FX";
                        $WorkGrid["Hrow"] = "24";
                        $WorkGrid["Erow"] = false;
                        $WorkGrid["btSv"] = true;
                        $WorkGrid["btEd"] = true;
                        $WorkGrid["btDc"] = true;
                        $WorkGrid["Cell"] = $this->WorkGridCellValues();
                        $Ajax->call("CreaWorkGrid",json_encode($WorkGrid));
                        return $Ajax; 
                } 
            
                function MuestraCiclosMateria($Ajax,$Datos,$Params)
                {       $num = 1;
                        if ($Datos[0])
                        {   foreach ($Datos[1] as $Dato)
                                {       $Celda[0] = $Dato['IDCICLOSMAT'];
                                        $Celda[1] = $num;
                                        $Celda[2] = $Dato['IDMATERIA'];
                                        $Celda[3] = utf8_encode($Dato['MATERIA']);
                                        $Celda[4] = $Dato['IDMALLADET'];
                                        $Docente = $this->CreaComboDocenteByFacultad($Params[2],$num,$Dato['IDDOCENTE']);
                                        $Celda[5] = utf8_encode($Docente);
                                        
                                        $Grupo = $this->CreaComboGrupoByCohorte($Params[0],$num,$Dato['IDGRUPO']);
                                        $Celda[6] = $Grupo;
                                        
                                        $FechaInicio = $this->CreaCampoFechaInicio($num,$Dato['FEINICIO']->format('Y-m-d'));
                                        $Celda[7] = $FechaInicio;
                                        
                                        $FechaFin = $this->CreaCampoFechaFin($num,$Dato['FEFIN']->format('Y-m-d'));
                                        $Celda[8] = $FechaFin;
                                        
                                        $Celda[9] = $Dato['IDESTADO'];
                                        $Celda[10] = ($Dato['IDESTADO']>0 ? "ACTIVO" : "SIN ASIGNAR");
                                        $Ajax->call("CreaWorkGridFila","FX",$Celda);   
                                        $num++;
                                }
                        }else
                        { return $this->Respuesta($Ajax,"CMD_INF",$Datos[1]);}
                        return $Ajax;
                }    
            
                function CrearHistorico($Datos)
                {       $Materia = $Datos[3];
                        $Paralelo = $Datos[10];
                        $Col1="20%"; $Col2="80%"; 
                               
                        $HTML = '<table border="0" class="Form-Frame" cellpadding="4">';
                        $HTML.= '       <tr height="60px">';
                        $HTML.= '           <td class="Form-Label-Center" colspan=2 valign="top" style="padding-top:5px;width:'.$Col2.'">';
                        $HTML.= '               <p class="p-txt-label" align="justify">USTED VA A EDITAR LA MATERIA: '.$Materia.', CON EL GRUPO: '.$Paralelo.'</p>';
                        $HTML.= '               <br>';       
                        $HTML.= '               <p class="p-txt-label" align="justify">USTED SE RESPONSABILIZA DE LOS DATOS MODIFICADOS EN ESTE REGISTRO. POR MOTIVOS DE SEGURIDAD Y AUDITORIA SE ENVIARÁ UN CORREO ELECTRÓNICO A SU BANDEJA Y AL CORREO DE ADMINISTRACIÓN DE POSGRADO.</p>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <p></p>';
                        $HTML.= '       <tr height="20px">';
                        $HTML.= '           <td class="Form-Label-Center" valign="top" style="padding-top:5px;width:'.$Col1.'">';
                        $HTML.= '               <p class="p-txt-label">Motivo</p>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '               <textarea class="txt-upper" style="width: 97%; height: 50px; resize: none;" id="motivo" name="motivo" maxlength="250"></textarea>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>'; 
                        $HTML.= '      <tr height="40px">';
                        $HTML.= '          <td class="Form-Label-Center" colspan="2">';
                        $HTML.= '              <input type="button" id="btaplica" class="p-txt-label" style="border: 1px solid #aaa; width: 56px; height: 25px" value="Aceptar" onclick=" return GuardarHistorico(motivo); ">';
                        $HTML.= '          </td>';
                        $HTML.= '      </tr>';
                        $HTML.= '</table>';
                        return $HTML;
                }

                private function Respuesta($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse";
                        $Ajax->call($jsFuncion,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>


