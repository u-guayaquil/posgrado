<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");    
        require_once("src/rules/maestria/servicio/ServicioCiclosMateria.php");
        require_once("src/modules/maestria/CiclosMateria/render/RenderCiclosMateria.php");        
        require_once("src/rules/maestria/servicio/ServicioDocente.php");  

        class ControlCiclosMateria
        {       private $ServicioCiclosMateria;
                private $ServicioDocente;
                private $RenderCiclosMateria;
                public $IdCicloMateria;
                        
                function __construct()
                {       $this->ServicioCiclosMateria = new ServicioCiclosMateria();
                        $this->ServicioDocente = new ServicioDocente();        
                        $this->RenderCiclosMateria = new RenderCiclosMateria();
                        $this->IdCicloMateria = 0;
                }

                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderCiclosMateria->CreaBarButton($Buttons[1]);
                }
                
                function CargaWorkGrid($Capa)
                {       $xAjax = new xajaxResponse();    
                        return $this->RenderCiclosMateria->CreaWorkGrid($xAjax,$Capa);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderCiclosMateria->CreaMantenimiento();
                }

                function ConsultaMaterias($Params)
                {       $xAjax = new xajaxResponse();
                        $Materias = $this->ServicioCiclosMateria->BuscarMaterias($Params); 
                        return $this->RenderCiclosMateria->MuestraCiclosMateria($xAjax,$Materias,$Params);
                }
                
                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion=="btmaestria")
                        {   $ArDatos = $this->ServicioCiclosMateria->BuscarMaestria();
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cohorte";
                            $jsonModal['Carga'] = $this->RenderCiclosMateria->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "730";
                        }
                        else if ($Operacion=="btcohorte")
                        {   $Params = array('mst'=>$IdRef[0], 'vmt'=>$IdRef[1]);
                            $ArDatos = $this->ServicioCiclosMateria->BuscarCohorteByMaestria($Params);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cohorte";
                            $jsonModal['Carga'] = $this->RenderCiclosMateria->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "325";
                        }
                        else if ($Operacion==="btciclo")    
                        {   $Params = array('mst'=>$IdRef[0], 'vmt'=>$IdRef[1], 'cht'=>$IdRef[2]);
                            $ArDatos = $this->ServicioCiclosMateria->BuscarCicloByCohorte($Params);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Ciclo";
                            $jsonModal['Carga'] = $this->RenderCiclosMateria->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "525";
                        }   
                        else if ($Operacion==="jModal")
                        {   $jsonModal['Modal'] = "ModalHistorico"; 
                            $jsonModal['Title'] = "Modificar Registro";
                            $jsonModal['Carga'] = $this->RenderCiclosMateria->CrearHistorico($IdRef);
                            $jsonModal['Ancho'] = "350";                          
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btmaestria")    
                        {   $Params = array('txt'=>strtoupper(trim($Texto)));
                            $Datos = $this->ServicioCiclosMateria->BuscarMaestriaByTx($Params);
                            return $this->RenderCiclosMateria->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }
                        else if ($Operacion==="btcohorte")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $Params = array('txt'=>$Texto[0],'mst'=>$Texto[1],'vmt'=>$Texto[2]);
                            $Datos = $this->ServicioCiclosMateria->BuscarCohorteByMaestriaByTx($Params);
                            return $this->RenderCiclosMateria->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }
                        else if ($Operacion==="btciclo")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $Params = array('txt'=>$Texto[0],'mst'=>$Texto[1],'vmt'=>$Texto[2],'cht'=>$Texto[3]);
                            $Datos = $this->ServicioCiclosMateria->BuscarCicloByCohorteByTx($Params);
                            return $this->RenderCiclosMateria->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }                        
                }
                
                function GuardaHistorico($Param,$Datos,$Texto)
                {   $xAjax = new xajaxResponse();
                    $GuardaHistorico = $this->ServicioCiclosMateria->GuardaHistorico($Datos,$Texto);
                    $GuardaNuevoRegistro = $this->ServicioCiclosMateria->GuardaNuevoRegistro($Param,$Datos);
                    if ($GuardaHistorico != 0){
                        $jsFuncion = "XAJAXResponse";
                        $xAjax->call($jsFuncion,"CMD_NEW_REG",$GuardaNuevoRegistro);
                        return $xAjax;
                    }
                }
                
                function GuardarDB($Params,$Datos,$IdCicloMat)
                {   $xAjax = new xajaxResponse();
                
                    $ExisteGrupoMateria = $this->ServicioCiclosMateria->ConsultarExisteGrupoMateria($Datos);
                    $ExisteCicloMateria = $this->ServicioCiclosMateria->ConsultarExisteCicloMateria($Datos,$IdCicloMat);
                    
                    if ($ExisteGrupoMateria == 0)
                    {   if($ExisteCicloMateria == 1)
                        {   $ActualizarDB = $this->ServicioCiclosMateria->ActualizarDB($Params, $Datos, $IdCicloMat);
                            if ($ActualizarDB != 0){
                                $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_SAV","Datos Guardados Correctamente");
                                 return $xAjax;
                            }   else    {
                                $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_ERROR","ERROR EN LA BASE DE DATOS");
                                 return $xAjax;
                            }
                        }   else    {
                            $GuardarDB = $this->ServicioCiclosMateria->GuardarDB($Params,$Datos);
                            if ($GuardarDB != 0){
                                $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_SAV","Datos Guardados Correctamente");
                                 return $xAjax;
                            }   else    {
                                $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_ERROR","ERROR EN LA BASE DE DATOS");
                                 return $xAjax;
                            }
                        }
                    }   else    {
                            $jsFuncion = "XAJAXResponse";
                            $xAjax->call($jsFuncion,"CMD_NON","!El registro ya existe!");     
                            return $xAjax;
                        } 
                }
                
                function ActualizarDB($Params,$Datos,$IdCicloMat)
                {   $xAjax = new xajaxResponse();
                    $ExisteGrupoMateria = $this->ServicioCiclosMateria->ConsultarExisteGrupoMateria($Datos);
                    if ($ExisteGrupoMateria == 0)
                        {   $ActualizarDB = $this->ServicioCiclosMateria->ActualizarDB($Params,$Datos,$IdCicloMat);
                            if ($ActualizarDB != 0){
                                $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_SAV","Datos Guardados Correctamente");
                                 return $xAjax;
                            }   else    {
                                $jsFuncion = "XAJAXResponse";
                                $xAjax->call($jsFuncion,"CMD_ERROR","ERROR EN LA BASE DE DATOS");
                                 return $xAjax;
                            }
                        }   else    {
                            $jsFuncion = "XAJAXResponse";
                            $xAjax->call($jsFuncion,"CMD_NON","!El registro ya existe!");  
                            return $xAjax;
                        } 
                }

                function CambiarEstado($IdCicloMat,$IdTemporalCM)
                {   $xAjax = new xajaxResponse();
                    $Estado = $this->ServicioCiclosMateria->CambiarEstado($IdCicloMat);
                    $EstadoTemporal = $this->ServicioCiclosMateria->CambiarEstadoTemporal($IdTemporalCM);
                    if ($Estado == 1)
                        {   $jsFuncion = "XAJAXResponse";
                            $xAjax->call($jsFuncion,"CMD_SAV","Registro Inabilitado");
                            return $xAjax;
                            
                    }   else    {
                            $jsFuncion = "XAJAXResponse";
                            $xAjax->call($jsFuncion,"CMD_NON","!Error en la base de datos!");   
                            return $xAjax;
                    } 
                }
        }

?>

