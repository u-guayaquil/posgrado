<?php   
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/SearchGrid.php");
        
        class RenderAsistenciaActa
        {       private $Width;
                private $Search;
                private $SearchGrid;
              
                function __construct()
                {       $this->Search = new SearchInput();   
                        $this->SearchGrid = new SearchGrid();
                }

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();
                        return $BarButton->CreaBarButton($Buttons);
                }

                function CreaMantenimiento()
                {       $Col1="11%"; $Col2="39%"; $Col3="11%"; $Col4="39%"; $Col5="10%"; 
                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="30px">';
                        $HTML.= '           <td class="Form-Label" colspan="5">:: REPORTE DE ASISTENCIA</td>';
                        $HTML.= '       </tr>';                                      
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Docente</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("docente","","")->Enabled(true)->Create("t11");   
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">Maestria</td>';
                        $HTML.= '           <td class="Form-Label" colspan="2" style="width:'.$Col4.'">';
                        $HTML.= '               <input type="hidden" id="idciclo" name="idciclo" value="" />';
                        $HTML.=                 $this->Search->TextSch("maestria","","")->Enabled(true)->Create("t11");                         
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';   
                        $HTML.= '       <tr height="28px">';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col1.'">Materia</td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                 $this->Search->TextSch("materia","","")->Enabled(true)->Create("t11");       
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:'.$Col3.'">&nbsp;Ciclo</td>';
                        $HTML.= '           <td class="Form-Label">';
                        $HTML.= '               <input type="text" class="txt-input" style="width:200px" id="txciclo" name="txciclo" value="" readOnly/> '; 
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label">';
                        $HTML.= '               <input type="hidden" id="idgrupo" name="idgrupo" value=""/>';
                        $HTML.= '               <input type="text" class="txt-input" style="width:80px" id="txgrupo" name="txgrupo" value="" readOnly/> ';
                        $HTML.= '           </td>';                        
                        $HTML.= '       </tr>';             
                        return $HTML.'</table>';
                }

                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btdocente")
                        {   $Columns['id'] = array('0px','center','none');
                            $Columns['cedula'] = array('80px','center','');
                            $Columns['Docente']= array('300px','left','');
                        }          
                        else if ($Operacion=="btmaestria")
                        {   $Columns['M.ID']=array('40px','center','');
                            $Columns['Maestria']=array('300px','left','');
                            $Columns['C.ID']=array('0px','center','none');
                            $Columns['Ciclo']=array('200px','left','');
                        }
                        else if ($Operacion=="btmateria")
                        {   $Columns['M.Id']=array('40px','center','');
                            $Columns['Materia']=array('250px','left','');
                            $Columns['G.ID']=array('40px','center','');
                            $Columns['Grupo']=array('150px','left','');
                        }                          
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$Contenido)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Contenido,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Contenido[0])
                        {   if ($Operacion=="btmateria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMATERIA'],MATERIA,'0',STR_PAD_LEFT);
                                        $ig = str_pad($ArDato['IDGRUPO'],GRUPO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MATERIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ig);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['GRUPO']));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btmaestria")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $id = str_pad($ArDato['IDMAESTRIA'],MAESTRIA,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['MAESTRIA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode($ArDato['CICLO']));
                                        $Grid->CreaSearchRowsDetalle ($id,"black");
                                }
                            }
                            elseif ($Operacion=="btdocente")
                            {   foreach ($Contenido[1] as $ArDato)
                                {       $idM = $ArDato['CEDULA'];
                                        $Grid->CreaSearchCellsDetalle($idM,'',$ArDato['ID']);
                                        $Grid->CreaSearchCellsDetalle($idM,'',$idM);
                                        $Grid->CreaSearchCellsDetalle($idM,'',utf8_encode(trim($ArDato['APELLIDOS'])).' '.utf8_encode(trim($ArDato['NOMBRES'])));
                                        $Grid->CreaSearchRowsDetalle ($idM,"black");
                                }
                            }                                                                                        
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                }
                
                function GetGridTableWidth()
                {        return $this->Width;
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        
        }
?>

