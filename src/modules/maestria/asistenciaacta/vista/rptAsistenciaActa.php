<?php   require_once("src/rules/maestria/servicio/ServicioReporte.php");
        require_once("src/rules/maestria/servicio/ServicioMatInformacion.php");
        require_once("src/libs/plugins/pdf/vendor/mpdf/mpdf/mpdf.php");
        require_once("src/rules/maestria/servicio/ServicioAsistenciaActa.php");
        
        class rptAsistenciaActa
        {   private $Cols;
            
            private function CabsCSS()
            {       $Style = 'style = "background-color: #FFF;
                                border-collapse:collapse; 
                                border:0px solid #ccc;
                                font-family:tahoma; 
                                font-size:9px; 
                                width:100%"';    
                    return  $Style;          
            }
            
            private function ColcCSS()
            {       $Style = 'style="text-align:center;
                                border:1px solid #ccc;
                                font-weight:bold;
                                height:30px"';    
                    return  $Style;          
            }

            private function ColdCSS($align)
            {       $Style = 'style="text-align:'.$align.';
                                border:1px solid #ccc;
                                height:20px"';    
                    return  $Style;          
            }

            function CrearReporte($Params,$Cabecera,$Detalles)
            {       $ServicioReporte = new ServicioReporte();
                    $ServicioMatInforma = new ServicioMatInformacion();
                    
                    $Inform = $ServicioReporte->DatosCabeceraReporte($Params);
                    if ($Inform[0])
                    { //[DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP];  
                        $ValorComponente = $ServicioMatInforma->BuscarValorComponente('CD', array($Params[2],$Params[3]));
                        $HTMI = '<table border="1" style="font-family:tahoma;font-size:8px; border-collapse:collapse">';
                        $HDET = $this->AsistenciaActaColumna($Cabecera); 
                        $HDET.= $this->AsistenciaActaDetalle($Detalles,$ValorComponente[1][0]['COMPTE']);
                        $HDET.= $this->AsistenciaActaPiePagina($Inform[1][0]['DOCENTE']);
                        $HTMF = '</table>';
                        $HDET = $HTMI.$HDET.$HTMF;
                        return $this->AsistenciaActaCuerpo($Inform[1],'').$HDET;
                    }
                    return $Inform[1];
            }
            
            private function AsistenciaActaCuerpo($Inform,$HDET)
            {       $Dato = $Inform[0];
                    $Utils = new Utils();
                    $HTML =     '<table border="0" '.$this->CabsCSS().'>';
                    $HTML.=     '<tr height="20px">';
                    $HTML.=         '<td style="width:10%;text-align:left" rowspan="5">';
                    $HTML.=             '<img src="src/public/img/logo_ug_small.png" width="57px" height="67px">';
                    $HTML.=         '</td>';
                    $HTML.=         '<td style="width:90%;text-align:center" colspan="3"><b>UNIVERSIDAD DE GUAYAQUIL</b></td>';
                    $HTML.=     '</tr>';
                    $HTML.=     '<tr height="20px">';
                    $HTML.=         '<td style="text-align:center" colspan="3"><b>FACULTA DE '.$Dato['FACULTAD'].'</b></td>';
                    $HTML.=     '</tr>';
                    $HTML.=     '<tr height="20px">';
                    $HTML.=         '<td style="text-align:center" colspan="3"><b>COORDINACION DE POSGRADO</b></td>';
                    $HTML.=     '</tr>';    
                    $HTML.=     '<tr height="20px">';
                    $HTML.=         '<td style="text-align:center" colspan="3"><b>MAESTRIA EN '.utf8_encode($Dato['MAESTRIA']).'</b></td>';
                    $HTML.=     '</tr>';    
                    $HTML.=     '<tr height="20px">';
                    $HTML.=         '<td style="text-align:center" colspan="3"><b>ACTA DE ASISTENCIA - SISTEMA '.$Dato['TIPOMALLA'].'</b></td>';
                    $HTML.=     '</tr>';
                    $HTML.=     '<tr height="20px">';
                    $HTML.=         '<td style="width:10%;"><b>ASIGNATURA</b></td>'; 
                    $HTML.=         '<td style="width:40%;">'.utf8_encode($Dato['MATERIA']).'</td>';
                    $HTML.=         '<td style="width:10%;"><b>PROFESOR</b></td>'; 
                    $HTML.=         '<td style="width:40%;">'.$Dato['DOCENTE'].'</td>'; //utf8_encode($Dato['DOCENTE'])
                    $HTML.=     '</tr>';
                    $HTML.=     '<tr height="20px">';
                    $HTML.=         '<td style="width:10%;"><b>GRUPO</b></td>'; 
                    $HTML.=         '<td style="width:40%;">'.$Dato['PARALELO'].'</td>';
                    $HTML.=         '<td style="width:10%;"><b>COHORTE</b></td>'; 
                    $HTML.=         '<td style="width:40%;">'.$Dato['COHORTE'].'</td>'; 
                    $HTML.=     '</tr>';
                    $HTML.=     '<tr height="20px">';
                    $HTML.=         '<td style="width:10%;"><b>DESDE</b></td>'; 
                    $HTML.=         '<td style="width:40%;">'.$Dato['DESDE']->format('d/m/Y').'</td>';
                    $HTML.=         '<td style="width:10%;"><b>HASTA</b></td>'; 
                    $HTML.=         '<td style="width:40%;">'.$Dato['HASTA']->format('d/m/Y').'</td>'; 
                    $HTML.=     '</tr>';
                    $HTML.=     '<tr >';
                    $HTML.=         '<td colspan="4">'.$HDET.'</td>'; 
                    $HTML.=     '</tr>';
                    $HTML.=     '</table>';
                    return $HTML;
            }   

            private function AsistenciaActaPiePagina($Docente)
            {       $DateControl = new DateControl();
                    $HTML = '<tr>';
                    $HTML.= '<td style="border:none" colspan="20">';
                    $HTML.=     '<table border="0" '.$this->CabsCSS().'>';
                    $HTML.=     '<tr><td height="80px" colspan="4"></td></tr>';
                    $HTML.=     '<tr>';
                    //$HTML.=         '<td height="50px" style="width:3%;text-align:center"></td>';
                    $HTML.=         '<td height="50px" style="width:29%;text-align:center"><br>'.$Docente.'<br><b>Firma del Docente</b><br>Fecha: '.$DateControl->getNowDateTime("FECHA").'</td>';
                    $HTML.=         '<td height="50px" style="width:29%;text-align:center">';
                    $HTML.=         '<b>Nombre y Firma<br>Coordinador de Posgrado</b><br><br>Fecha: _____/_____/________';
                    $HTML.=         '</td>';
                    $HTML.=         '<td height="50px" style="width:29%;text-align:center"><b>Nombre y Firma<br>Coordinador del Programa</b><br><br>Fecha: _____/_____/________</td>';
                    $HTML.=         '<td height="50px" style="width:13%;text-align:left">';
                    $HTML.=         'Formato # DP-2019-004<br>Versión 1.0<br>Última actualización:<br>29/07/2019';
                    $HTML.=         '</td>';
                    $HTML.=     '</tr>';
                    $HTML.=     '</table>';
                    $HTML.= '</td>';
                    $HTML.= '</tr>';
                    return $HTML;
            }                

            private function AsistenciaActaColumna($Cabecera)
            {       $this->Cols = 0;    
                    $HTMA = "<tr>";
                    $HTMA.= "<td colspan='3' ".$this->ColcCSS().">ESTUDIANTE</td>";
                    $HTMB = "<tr>";
                    $HTMB.= "<td width='30px' ".$this->ColcCSS().">No</td>";
                    $HTMB.= "<td width='60px' ".$this->ColcCSS().">CEDULA</td>";
                    $HTMB.= "<td width='200px' ".$this->ColcCSS().">NOMBRE</td>";
                    foreach ($Cabecera as $Dato)
                    {       $Nodo = explode("<br>",$Dato['HORARIO']);
                            $HTMB.= "<td width='42px' ".$this->ColcCSS().">";        
                            $HTMB.= $Nodo[0]."<br>".$Nodo[count($Nodo)-1];    
                            $HTMB.= "</td>";
                            $HTMA.= "<td width='42px' ".$this->ColcCSS().">";        
                            $HTMA.= $Dato['FECHASISTENCIA']->format('d/m/y');    
                            $HTMA.= "</td>";
                            $this->Cols++;
                    }
                    $HTMA.= "<td colspan='3' ".$this->ColcCSS().">TOTAL HORAS</td></tr>";
                    $HTMB.= "<td width='42px' ".$this->ColcCSS().">ASIST</td>";
                    $HTMB.= "<td width='42px' ".$this->ColcCSS().">COMPTE<br>CD</td>";
                    $HTMB.= "<td width='42px' ".$this->ColcCSS().">%</td>";
                    $HTMB.= "<td width='70px' ".$this->ColcCSS().">OBSERVACIÓN</td></tr>";
                    return $HTMA.$HTMB;
            }
            
            private function AsistenciaActaDetalle($Detalles,$COMP)
            {       if ($this->Cols!=0)
                    {   $STRI = "";
                        $HTMC = "";
                        $NUME = 0;
                        $ACUM = 0;  
                        $HDET = "";
                            
                        foreach ($Detalles as $Dato)
                        {       if ($STRI!=$Dato['CEDULA'])
                                {   $STRI = $Dato['CEDULA'];
                                    if ($HTMC!="")
                                    {   if ($this->Cols!=$Cols)
                                        $HTMC.= "<td ".$this->ColdCSS('center')." colspan='".($this->Cols-$Cols)."'>.</td>";
                                    
                                        $PORC = number_format(($ACUM/$COMP)*100,0);
                                        $HTMC.= "<td ".$this->ColdCSS('center').">".$ACUM."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('center').">".$COMP."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('center').">".$PORC."</td>";
                                        $HTMC.= "<td ".$this->ColdCSS('center').">".($PORC<80 ? "REPROBADO":"APROBADO")."</td>";
                                        $HDET.= "<tr>".$HTMC."</tr>";
                                        $ACUM = 0;
                                    }
                                    $Cols = 0;
                                    $NUME++;
                                    $HTMC = "<td ".$this->ColdCSS('center').">".$NUME."</td>";
                                    $HTMC.= "<td ".$this->ColdCSS('center').">".$STRI."</td>";
                                    $HTMC.= "<td ".$this->ColdCSS('left').">".utf8_encode($Dato['ALUMNO'])."</td>";
                                }
                                $HTMC.= "<td ".$this->ColdCSS('center').">";        
                                $HTMC.= $Dato['EFECTIVA']+$Dato['JUSTIFICADA'];    
                                $HTMC.= "</td>";
                                $Cols++;
                                $ACUM = $ACUM + $Dato['EFECTIVA']+$Dato['JUSTIFICADA'];
                        }
                        if ($NUME!=0)
                        {   $PORC = intval(($ACUM/$COMP)*100);
                            $HTMC.= "<td ".$this->ColdCSS('center').">".$ACUM."</td>";
                            $HTMC.= "<td ".$this->ColdCSS('center').">".$COMP."</td>";
                            $HTMC.= "<td ".$this->ColdCSS('center').">".$PORC."</td>";
                            $HTMC.= "<td ".$this->ColdCSS('center').">".($PORC<80 ? "REPROBADO":"APROBADO")."</td>";
                            $HDET.= "<tr>".$HTMC."</tr>";
                        }    
                    }    
                    return $HDET; 
            }

            function ValidarUAD($Params)
            {   $TotalEstudiantes=0;$Verificador=0;$Diferenciador=0;
                $ValorGrupo=array();$CantidadGrupos=0;
                foreach($Params as $Count=>$Data){
                    if($Data['IDUAD']!=0){//Estudiantes de Medicina
                        if(!in_array($Data['IDUAD'], $ValorGrupo)){
                            $ValorGrupo[]=$Data['IDUAD'];
                            $CantidadGrupos++;
                        }
                        $Diferenciador++;
                    }
                    else{//Estudiantes de otros posgrados
                        $Verificador++;
                    }
                    $TotalEstudiantes = $Count+1;
                }
                if($TotalEstudiantes==$Verificador)
                    return array(true,0);
                if  ($TotalEstudiantes==$Diferenciador)
                    return array(true,$ValorGrupo);
                return array(false,'EXISTEN ESTUDIANTES QUE NO TIENEN ASIGNADO UNIDAD ASISTENCIAL DOCENTE');
            }
        }

        if (isset($_GET['opcion']))
        {   $Utils = new Utils();
            $Nodos = $Utils->Desencriptar($_GET['opcion']);
            $Params = explode("|",$Nodos);

            $ServicioAsistenciaActa = new ServicioAsistenciaActa();
            $Asistencia = $ServicioAsistenciaActa->ConsultaAsistenciaActa($Params);
            if ($Asistencia[0])
            {   $Detalles = $ServicioAsistenciaActa->ConsultaDetalleAsistenciaActa($Params);
                if ($Detalles[0])
                {   $rptAsistenciaActa = new rptAsistenciaActa();
                    $Respuesta = $rptAsistenciaActa->ValidarUAD($Detalles[1]);
                    if($Respuesta[0]){
                        if($Respuesta[1]!=0){
                            $mpdf = new Mpdf('utf-8','A4');
                            foreach($Respuesta[1] as $UAD){
                                $DetallesMedicina = $ServicioAsistenciaActa->ConsultaDetalleAsistenciaActaByUAD($Params,$UAD);
                                $HTML = $rptAsistenciaActa->CrearReporte($Params,$Asistencia[1],$DetallesMedicina[1]);
                                $mpdf->AddPage();
                                $mpdf->WriteHTML($HTML);
                            }
                            $mpdf->Output('AASIS'.$Params[0].$Params[3].'.pdf','I');
                            //echo $Respuesta[1];
                        }
                        else{
                            $HTML = $rptAsistenciaActa->CrearReporte($Params,$Asistencia[1],$Detalles[1]);
                            $mpdf = new Mpdf('utf-8','A4-L');
                            $mpdf->WriteHTML($HTML);
                            $mpdf->Output('AASIS'.$Params[0].$Params[3].'.pdf','I');
                        }
                    }else
                    echo $Respuesta[1];
                }
                else
                echo $Detalles[1];
            }
            else
            echo $Asistencia[1];
        }
?>