<?php   $Opcion = 'AsistenciaActa';
     
        require_once('src/modules/maestria/asistenciaacta/controlador/ControlAsistenciaActa.php');
        $ControlAsistenciaActa = new ControlAsistenciaActa();

        $xajax->register(XAJAX_FUNCTION,array('ReporteAsistenciaActa', $ControlAsistenciaActa,'ReporteAsistenciaActa'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlAsistenciaActa,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlAsistenciaActa,'ConsultaGridByTX'));
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/maestria/asistenciaacta/js/jsAsistenciaActa.js"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:825px">
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                        <div class="FormSectionMenu">              
                        <?php   $ControlAsistenciaActa->CargaBarButton($_GET['opcion']);
                                echo '<script type="text/javascript">';
                                echo '        BarButtonState("addPDF"); ';
                                echo '</script>';
                        ?>
                    </div>
                        <div class="FormSectionData">   
                             <?php  $ControlAsistenciaActa->CargaMantenimiento();  ?>
                        </div> 
                    </form>
                </div>       
        </body>
        </html>