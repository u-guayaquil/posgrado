<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");    
        require_once("src/rules/maestria/servicio/ServicioAsistenciaActa.php");
        require_once("src/modules/maestria/asistenciaacta/render/RenderAsistenciaActa.php");        
        require_once("src/rules/maestria/servicio/ServicioDocente.php");  
        require_once("src/rules/maestria/servicio/ServicioPlanAnalitico.php");
        require_once("src/rules/maestria/servicio/ServicioHorarioPlanificacion.php");
        require_once("src/modules/maestria/horario/render/RenderHorarioPlanificacion.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");

        class ControlAsistenciaActa
        {       private $ServicioAsistenciaActa;
                private $ServicioDocente;
                private $RenderAsistenciaActa;
                private $ServicioPlanAnalitico;
                private $ServicioHorarioPlanificacion;
                private $RenderHorarioPlanificacion;
                private $ServicioUsuarioFiltro;
                
                function __construct()
                {       $this->ServicioAsistenciaActa = new ServicioAsistenciaActa();
                        $this->ServicioDocente = new ServicioDocente();        
                        $this->RenderAsistenciaActa = new RenderAsistenciaActa();
                        $this->ServicioPlanAnalitico = new ServicioPlanAnalitico();
                        $this->ServicioHorarioPlanificacion = new ServicioHorarioPlanificacion();
                        $this->RenderHorarioPlanificacion = new RenderHorarioPlanificacion();
                        $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                }

                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton();
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Buttons[0])
                        echo $this->RenderAsistenciaActa->CreaBarButton($Buttons[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderAsistenciaActa->CreaMantenimiento();
                }

                function ReporteAsistenciaActa($Params)
                {       $xAjax = new xajaxResponse();    
                        
                        $Utils = new Utils();
                        $urlfl = $Utils->Encriptar("src/modules/maestria/asistenciaacta/vista/rptAsistenciaActa.php");
                        $title = "Acta de Asistencias";
                        $optns = $Utils->Encriptar($Params[0]."|".$Params[1]."|".$Params[2]."|".$Params[3]."|".$Params[4]); 
                          
                        $jsonModal['Modal'] = "ModaladdPdf"; 
                        $jsonModal['Title'] = $title;
                        $jsonModal['Carga'] = "<iframe name='ModaladdPdf' src='index.php?url=".$urlfl."&opcion=".$optns."' height='465px' width='100%' scrolling='si' frameborder='0' marginheight='0' marginwidth='0'></iframe>"; 
                        $jsonModal['Ancho'] = "900";
                        $jsonModal['Alto'] = "500";
                        $xAjax->call("CreaModal", json_encode($jsonModal));
                        return $xAjax; 
                }
                
                private function FilterOfAccess($Params)
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        $Docnte = $Params['dct'];

                        if ($Modulo->Idrol==3)
                        {   $Factad = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByUsrol($Modulo->Idusrol);
                            if ($Docnte==0){
                                return array('texto'=>$Params['txt'], 'facultad'=>$Factad); 
                            }    
                            return array('texto'=>$Params['txt'], 'facultad'=>$Factad, 'docente'=>$Params['dct']);
                        }
                        else if ($Modulo->Idrol==2)
                        {   $Dcte = ($Docnte==0 ? $Modulo->Cedula : $Docnte); 
                            return array('texto'=>$Params['txt'], 'docente'=>$Dcte);
                        }
                        else
                        return array('facultad'=>array('00')); //No permiso a Estudiante
                }
                
                function CargaModalGrid($Operacion,$IdRef)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion=="btdocente")
                        {   $Params = array('txt'=>'','dct'=>0);
                            $ArDatos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Docente";
                            $jsonModal['Carga'] = $this->RenderAsistenciaActa->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAsistenciaActa->GetGridTableWidth();
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Params = array('txt'=>'','dct'=>$IdRef);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Maestria";
                            $jsonModal['Carga'] = $this->RenderAsistenciaActa->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAsistenciaActa->GetGridTableWidth();
                        }   
                        else if ($Operacion==="btmateria")    
                        {   $prepareDQL = array('docente'=>$IdRef[0],'ciclo'=>$IdRef[1],'maestria'=>$IdRef[2]);
                            $ArDatos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Materia";
                            $jsonModal['Carga'] = $this->RenderAsistenciaActa->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = $this->RenderAsistenciaActa->GetGridTableWidth();
                        }  
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btdocente")    
                        {   $Params = array('txt'=>strtoupper(trim($Texto)),'dct'=>0);
                            $Datos = $this->ServicioDocente->BuscarDocentePlanificacion($this->FilterOfAccess($Params));
                            return $this->RenderAsistenciaActa->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }
                        else if ($Operacion==="btmaestria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $Params = array('txt'=>$Texto[0],'dct'=>$Texto[1]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMaestriaByDocenteFacultad($this->FilterOfAccess($Params));
                            return $this->RenderAsistenciaActa->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                        }                        
                        else if ($Operacion=="btmateria")    
                        {   $Texto[0] = strtoupper(trim($Texto[0]));
                            $prepareDQL = array('texto'=>$Texto[0],'docente'=>$Texto[1],'ciclo'=>$Texto[2],'maestria'=>$Texto[3]);
                            $Datos = $this->ServicioPlanAnalitico->BuscarMateriaGrupoByMaestriaCiclo($prepareDQL);
                            return $this->RenderAsistenciaActa->MuestraModalGrid($xAjax,$Operacion,$Datos);         
                        }
                }
        }
?>