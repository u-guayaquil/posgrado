function ButtonClick(Operacion)
{       if (Operacion=="addPdf")
        {   var DOCTE = ElementGetValue("iddocente");
            var CICLO = ElementGetValue("idciclo");   
            var MAEST = ElementGetValue("idmaestria");
            var MATER = ElementGetValue("idmateria");
            var GRUPO = ElementGetValue("idgrupo");
            if (DOCTE!="" && CICLO!="" && MAEST!="" && MATER!="" && GRUPO!="")
            {   var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO];
                xajax_ReporteAsistenciaActa(Params);
            }
            else
                SAlert("warning", "¡Campos incompletos!", "Debe seleccionar toda la información.");
        }
        else if (Operacion=="btdocente")
        {    xajax_CargaModal(Operacion,0);
        }
        else if (Operacion=="btmaestria")
        {    var IDDOC=ElementGetValue("iddocente");
             if (IDDOC!="")
                 xajax_CargaModal(Operacion,IDDOC);
             else
                SAlert("warning", "¡Cuidado!", "Debe seleccionar un Docente.");
        }
        else if (Operacion=="btmateria")
        {    var IDMST=ElementGetValue("idmaestria");
             if (IDMST!="")
             {   var IDDOC=ElementGetValue("iddocente");
                 var IDCLC=ElementGetValue("idciclo");
                 var Params = [IDDOC,IDCLC,IDMST];
                 xajax_CargaModal(Operacion,Params);
             }
             else
             SAlert("warning", "¡Cuidado!","Debe seleccionar una Maestria.");
        }
        return false;
}

function SearchDocenteGetData(DatosGrid)
{       var IDDOC = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("iddocente")!=IDDOC)
        {   //ElementStatus("","MinSav:MinOpn");
            ElementClear("idmaestria:txmaestria:idciclo:txciclo:idmateria:txmateria:idgrupo:txgrupo");
            document.getElementById("iddocente").value = IDDOC;
            document.getElementById("txdocente").value = DatosGrid.cells[2].innerHTML;
            cerrar();
        }    
        else
        cerrar();
        return false;
}

function SearchMaestriaGetData(DatosGrid)
{       var IDMST = DatosGrid.cells[0].innerHTML;
        if (ElementGetValue("idmaestria")!=IDMST)
        {   //ElementStatus("","MinSav:MinOpn");
            ElementClear("idmateria:txmateria:idgrupo:txgrupo");
            document.getElementById("idmaestria").value = IDMST;
            document.getElementById("txmaestria").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idciclo").value = DatosGrid.cells[2].innerHTML;
            document.getElementById("txciclo").value = DatosGrid.cells[3].innerHTML;
            cerrar();
        }
        else
        cerrar();    
        return false;
}  

function SearchMateriaGetData(DatosGrid)
{       var IDGRUPO = DatosGrid.cells[2].innerHTML;
        if (ElementGetValue("idgrupo")!=IDGRUPO)
        {   //ElementStatus("MinSav","");
            document.getElementById("idmateria").value = DatosGrid.cells[0].innerHTML;
            document.getElementById("txmateria").value = DatosGrid.cells[1].innerHTML;
            document.getElementById("idgrupo").value =  IDGRUPO;
            document.getElementById("txgrupo").value = DatosGrid.cells[3].innerHTML;
            cerrar();
        }
        else    
        cerrar();    
        return false;
}

function EjecutaAsistenciaActa()
{       DOCTE=ElementGetValue("iddocente");
        CICLO=ElementGetValue("idciclo");   
        MAEST=ElementGetValue("idmaestria");
        MATER=ElementGetValue("idmateria");
        GRUPO=ElementGetValue("idgrupo");
        var Params = [DOCTE,CICLO,MAEST,MATER,GRUPO];
        xajax_BuscaByID(Params);
}  

function SearchByElement(Elemento)
{       Elemento.value = TrimElement(Elemento.value);
        if (Elemento.id === "FindDocenteTextBx")
        {   xajax_BuscaModalByTX("btdocente",Elemento.value);
        } 
        else if (Elemento.id === "FindMaestriaTextBx")
        {   var Params = [Elemento.value,ElementGetValue("iddocente")];
            xajax_BuscaModalByTX("btmaestria",Params);
        }
        else if(Elemento.id === "FindMateriaTextBx")
        {   var Params = [Elemento.value,ElementGetValue("iddocente"),ElementGetValue("idciclo"),ElementGetValue("idmaestria")];
            xajax_BuscaModalByTX("btmateria",Params);
        }
}
