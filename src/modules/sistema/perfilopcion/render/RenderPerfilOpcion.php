<?php    
        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/SearchGrid.php");
        
        class RenderPerfilOpcion
        {       
                function CreaMantenimientoPerfilOpcion()
                {       $PerfilOpcion = '<table border=0 class="Form-Frame" cellpadding="0">';
                        $PerfilOpcion.= '       <tr height="28">';
                        $PerfilOpcion.= '           <td class="Form-Label" style="width:20%">Perfil</td>';
                        $PerfilOpcion.= '           <td class="Form-Label" style="width:80%">';
                                                        $PerfilOpcion.= $this->CreaComboPerfil();
                        $PerfilOpcion.= '           </td>';
                        $PerfilOpcion.= '       </tr>';
                        $PerfilOpcion.= '       <tr height="28">';
                        $PerfilOpcion.= '           <td class="Form-Label" style="width:20%">Menú</td>';
                        $PerfilOpcion.= '           <td class="Form-Label" style="width:80%">';
                                                        $PerfilOpcion.= $this->CreaComboMenu();
                        $PerfilOpcion.= '           </td>';
                        $PerfilOpcion.= '       </tr>';
                        return $PerfilOpcion.'</table>';
                }
            
                private function SearchGridConfig()
                {       $Columns['IdOpcion'] = array('0px','center','none');
                        $Columns['TpOpcion'] = array('0px','left','none');
                        $Columns['Opciones'] = array('360px','left','');
                        
                        $Columns['PO'] = array('30px','center','none');
                        $Columns['NW'] = array('30px','center','');
                        $Columns['ED'] = array('30px','center','');
                        $Columns['DL'] = array('30px','center','');
                        $Columns['PR'] = array('30px','center','');
                        $Columns['EX'] = array('30px','center','');
                        $Columns['PD'] = array('30px','center','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '425px','AltoRow' => '25px','FindTxt' =>false);
                }

                function CreaSearchGridPerfilOpcionPermisos($PerfilOpcion)
                {       $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTMLPerfilOpcionPermisos($SearchGrid,$PerfilOpcion);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }

                function GridDataHTMLPerfilOpcionPermisos($grid,$root)
                {       $entro = false;
                        foreach($root as $sheet)
                        {       $id = str_pad($sheet->id,4,'0',STR_PAD_LEFT);
                                $grid->CreaSearchCellsDetalle($id,'',$id);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->idtipo);
                                $desc = $this->FormatGridDataOption($sheet->nivel,$sheet->idtipo,$sheet->descripcion)['texto'];
                                $grid->CreaSearchCellsDetalle($id,'',$desc,($sheet->idtipo!=2 ? 8:0));
                                
                                $nums = count($sheet->boton);
                                for ($i = 0; $i < $nums; $i++) 
                                {   if ($i>0){
                                        $mark = ($sheet->boton[$i] == 1 ? "✔" : "");
                                        $grid->CreaSearchCellsDetalle($id,$sheet->boton[0],$mark,0,1); 
                                    }else{
                                        $grid->CreaSearchCellsDetalle($id,$sheet->boton[0],$sheet->boton[$i],0,1); 
                                    }
                                }
                                $grid->CreaSearchRowsDetalle ($id,"",0); 
                                if ($sheet->idtipo==0 || $sheet->idtipo==1){
                                    $this->GridDataHTMLPerfilOpcionPermisos($grid,$sheet->hijos);
                                }
                                $entro = true;
                        }
                        if ($entro)
                        {   if ($sheet->idtipo==0){
                            return $grid->CreaSearchTableDetalle();
                            }
                        }    
                }
   
                private function FormatGridDataOption($level,$types,$description)
                {       $Texto = "<b>".$description."</b>";
                        $Margen = $level*30;
                        if ($types==0)
                            $Texto = "<div style='padding-left:".$Margen."px'>&nbsp;&nbsp;".$Texto."</div>";
                        elseif ($types==1)
                            $Texto = "<div style='padding-left:".$Margen."px'><li type='disc'>".$Texto."</li></div>";
                        elseif($types==2)
                            $Texto = "<div style='padding-left:".$Margen."px'><li type='circle'>".$description."</li></div>";
                            
                        return array('texto' => $Texto, 'margen' => $Margen);
                }

                private function CreaComboPerfil()
                {       $Select = new ComboBox();
                        $DAOPerfil = new DAOPerfil();
                        $Datos = $DAOPerfil->ObtenerPerfiles();
                        return $Select->Combo("perfil",$Datos[1])->Fields('ID','DESCRIPCION')->Evento()->Enabled(true)->Create("s10");
                }        

                private function CreaComboMenu()
                {       $Select = new ComboBox();
                        $DAOOpcion = new DAOOpcion();
                        $Datos = $DAOOpcion->ObtenerOpcionesByTipo(0,1);
                        return $Select->Combo("menu",$Datos[1])->Fields('ID','DESCRIPCION')->Evento()->Enabled(true)->Create("s10");
                }        

        }
?>

