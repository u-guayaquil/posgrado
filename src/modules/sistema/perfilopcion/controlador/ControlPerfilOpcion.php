<?php   require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/modules/sistema/perfilopcion/render/RenderPerfilOpcion.php");
        
        class ControlPerfilOpcion
        {       private  $ServicioPerfilOpcion;
                private  $RenderPerfilOpcion;

                function __construct()
                {       $this->ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $this->RenderPerfilOpcion = new RenderPerfilOpcion();
                }

                function CargaPerfilOpcionMantenimiento()
                {       echo $this->RenderPerfilOpcion->CreaMantenimientoPerfilOpcion();
                }

                function CargaPerfilOpcionSearchGrid()
                {       $Datos = $this->ServicioPerfilOpcion->BuscarPerfilOpcionPermisos();
                        echo $this->RenderPerfilOpcion->CreaSearchGridPerfilOpcionPermisos(json_decode($Datos));
                }         
                
                function GuardaPerfilOpcion($Form)
                {       $xAjax = new xajaxResponse();
                        $PerfOpcion = json_decode($Form)->Forma;
                        if (is_numeric($PerfOpcion->Id)){
                            $idx = $this->ServicioPerfilOpcion->EditaPerfilOpcion($PerfOpcion);
                        }else{
                            $idx = $this->ServicioPerfilOpcion->CreaPerfilOpcion($PerfOpcion);
                            $xAjax->Assign($PerfOpcion->IdObjto."_003","innerHTML",$idx);
                        }
                        return $xAjax;
                }
                
                function ConsultaPerfilOpcion($Pefil,$Menu)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioPerfilOpcion->BuscarPerfilOpcionPermisos($Pefil,$Menu);
                        $Grids = $this->RenderPerfilOpcion->CreaSearchGridPerfilOpcionPermisos(json_decode($Datos));
                        $ajaxRespon->Assign("Grid","innerHTML",$Grids);
                        return $ajaxRespon;
                }

        }       
?>

