<?php   $Opcion = 'PerfilOpcion';

        require_once('src/modules/sistema/perfilopcion/controlador/ControlPerfilOpcion.php');
        $ControlPerfilOpcion = new ControlPerfilOpcion();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlPerfilOpcion,'GuardaPerfilOpcion'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlPerfilOpcion,'ConsultaPerfilOpcion'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                function SearchGetCell(Celda)
                {       var str = Celda.id;
                        var opt = str.split("_");
                        if (parseInt(opt[2])>=4 && parseInt(opt[2])<=9)
                        {   Celda.innerHTML = (Celda.innerHTML=="" ? "✔":"");
                            var Objto = opt[0]+"_"+opt[1];
                            var Elemn = "IdObjto:" + Objto + "|IdBoton:" + parseInt(opt[2]) + "|IdOpcion:" + parseInt(opt[1]) + "|IdPerfil:" + document.getElementById("perfil").value + "|Id:" + document.getElementById(Objto+"_003").innerHTML + "|Value:" + (Celda.innerHTML=="" ? "0":"1");
                            var Forma = PrepareFreeElements(Elemn);
                            xajax_Guarda(JSON.stringify({Forma}));
                        }    
                        return false;
                }

                function ControlPerfil(Perfil)
                {       var Menu = document.getElementById("menu");
                        xajax_BuscaByID(Perfil.value,Menu.value);
                }
                
                function ControlMenu(Menu)
                {       var Perfil = document.getElementById("perfil");
                        xajax_BuscaByID(Perfil.value,Menu.value);
                }
        </script>
    </head>
        <body>
        <div class="FormContainer" style="width:1265px">
            <div class="FormContainerSeccion" style="width:350px">  
                <div class="FormBasic">
                    <div class="FormSectionData">              
                        <form id="<?php echo 'Form'.$Opcion; ?>">
                        <?php $ControlPerfilOpcion->CargaPerfilOpcionMantenimiento();  ?>
                        </form>
                    </div>    
                </div>            
            </div>       
            <div class="FormContainerSeccion" style="width:600px">   
                <div id="<?php echo 'Grid'; ?>" class="FormSectionGrid">          
                <?php $ControlPerfilOpcion->CargaPerfilOpcionSearchGrid();  ?>
                </div>    
            </div>    
        </div>
        </body>
    </html>
