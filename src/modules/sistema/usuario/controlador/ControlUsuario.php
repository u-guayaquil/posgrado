<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/sistema/servicio/ServicioUsuario.php");

        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/modules/general/empleado/render/RenderEmpleado.php");
        
        require_once("src/modules/sistema/usuario/render/RenderUsuario.php");

        class ControlUsuario
        {       private $ServicioUsuario;
                private $RenderUsuario;
                private $ServicioEmpleado;
                private $RenderEmpleado;

                function __construct($Sufijo = "")
                {       $this->ServicioUsuario = new ServicioUsuario();
                        $this->RenderUsuario = new RenderUsuario($Sufijo);
                        $this->ServicioEmpleado = new ServicioEmpleado();
                        $this->RenderEmpleado = new RenderEmpleado($Sufijo);
                }

                function CargaUsuarioBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $this->RenderUsuario->CreaBarButtonUsuario($Datos);
                }
               
                function CargaUsuarioMantenimiento()
                {       echo $this->RenderUsuario->CreaMantenimientoUsuario();
                }

                function CargaUsuarioSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioUsuario->BuscarUsuarioByDescripcion($prepareDQL);
                        echo $this->RenderUsuario->CreaSearchGridUsuario($Datos);
                }         
               
                function ConsultaUsuarioByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioUsuario->BuscarUsuarioByID($id);
                        return $this->RenderUsuario->MuestraUsuarioForm($ajaxRespon,$Datos);
                }

                function ConsultaUsuarioByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $texto = Trim($Texto);      
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioUsuario->BuscarUsuarioByDescripcion($prepareDQL);
                        return $this->RenderUsuario->MuestraUsuarioGrid($ajaxRespon,$Datos);
                }

                function GuardaUsuario($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $Usuario = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioUsuario->GuardaDBUsuario($Usuario);
                        if (is_numeric($id)){
                            $function = (empty($Usuario->id) ? "MuestraUsuarioGuardado" : "MuestraUsuarioEditado");
                            $Datos = $this->ServicioUsuario->BuscarUsuarioByID($id);
                            return $this->RenderUsuario->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderUsuario->MuestraUsuarioExcepcion($ajaxRespon,$id);
                        }
                }

                function EliminaUsuario($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioUsuario->DesactivaUsuario($id);
                        $Datos = $this->ServicioUsuario->BuscarUsuarioByID($id);
                        return $this->RenderUsuario->MuestraUsuarioEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        if ($Operacion==="btempleado")    
                        {   $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Empleado";
                            $jsonModal['Carga'] = $this->RenderEmpleado->CreaModalGrid($Operacion,$Datos);
                            $jsonModal['Ancho'] = "563";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        if ($Operacion==="btempleado")    
                        {   $Datos = $this->ServicioEmpleado->BuscarEmpleadoByNombreCompleto($prepareDQL);
                            return $this->RenderEmpleado->MuestraModalGridEmpleado($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
                
                
        }       
?>

