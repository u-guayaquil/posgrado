<?php   $Sufijo = '_usuario';

        require_once('src/modules/sistema/usuario/controlador/ControlUsuario.php');
        $ControlUsuario = new ControlUsuario($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlUsuario,'GuardaUsuario'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlUsuario,'EliminaUsuario'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlUsuario,'ConsultaUsuarioByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX'.$Sufijo, $ControlUsuario,'ConsultaUsuarioByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlUsuario,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlUsuario,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                var oprusuario = "usuario:caducidad:desde:hasta:estado:idempleado:txempleado";
                var objusuario = "id:"+oprusuario;
                    
                function ButtonClick_usuario(Sufijo,Operacion)
                {       var valusuario = "usuario:desde:hasta";   
                        var clsusuario = "id:usuario:caducidad:idempleado:txempleado";
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Sufijo,Operacion);
                            ElementStatus(Sufijo,"usuario:desde:hasta:idempleado:txempleado","id");
                            ElementClear(Sufijo,clsusuario);
                            BarButtonStateEnabled(Sufijo,"btempleado");
                            ElementSetValue(Sufijo,"estado",2);
                        }
                        else if (Operacion==='addMod')
                        {       valusuario = valusuario + ":caducidad:idempleado:txempleado";
                                BarButtonState(Sufijo,Operacion);
                                BarButtonStateEnabled(Sufijo,"btempleado");
                                if (ElementGetValue(Sufijo,"estado")==0)
                                {   valusuario = valusuario + ":estado";
                                }    
                                ElementStatus(Sufijo,valusuario,"id");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue(Sufijo,"estado")==1 || ElementGetValue(Sufijo,"estado")==2)
                                {   if (confirm("Desea inactivar este regsistro?"))
                                    xajax_Elimina_usuario(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(Sufijo,valusuario))
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    {   var Forma = PrepareElements(Sufijo,objusuario);
                                        xajax_Guarda_usuario(JSON.stringify({Forma}));
                                    }
                                }
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Sufijo,Operacion);
                                BarButtonStateDisabled(Sufijo,"btempleado");
                                ElementStatus(Sufijo,"id",oprusuario);
                                ElementClear(Sufijo,clsusuario);
                        }
                        else 
                            xajax_CargaModal_usuario(Operacion);
                        return false;
                }
                
                function XAJAXResponse_usuario(Sufijo,Mensaje,Datos)
                {       if (Mensaje==="GUARDADO" || Mensaje==="ACTUALIZADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btempleado");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",oprusuario);
                            if (Mensaje==="GUARDADO")
                                alert('Los datos se guardaron correctamente. Se envió un correo al usuario.');
                            else
                                alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='EXCEPCION')
                        {   BarButtonState(Sufijo,"addNew");
                            alert(Datos); 
                        }    
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                        }
                        else if(Mensaje==="NOEXISTEID")
                        {   BarButtonState(Sufijo,"Default");
                            ElementClear(Sufijo,"id");
                            alert(Datos); 
                        }
                }
                
                function SearchByElement_usuario(Sufijo,Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("usuario"+Sufijo);
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    xajax_BuscaByID_usuario(Elemento.value);
                                }
                            }
                            BarButtonState(Sufijo,"Default"); //OJO AQUI
                        }
                        else if (Elemento.id === "FindEmpleadoTextBx"+Sufijo)
                        {   xajax_BuscaModalByTX_usuario("btempleado",Elemento.value);
                        }
                        else
                            xajax_BuscaByTX_usuario(Elemento.value);
                        
                }
                 
                function SearchGetData_usuario(Sufijo,Grilla)
                {       if (IsDisabled(Sufijo,"addSav"))
                        {   BarButtonState(Sufijo,"Active");
                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("usuario"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("estado"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                            
                            document.getElementById("caducidad"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue; 
                            document.getElementById("desde"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue; 
                            document.getElementById("hasta"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                            
                            document.getElementById("idempleado"+Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue; 
                            document.getElementById("txempleado"+Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue;
                        }
                        return false;
                }

                function SearchEmpleadoGetData_usuario(Sufijo,DatosGrid)
                {       document.getElementById("idempleado"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txempleado"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }
                 
                $(document).ready(function()
                {       $('#caducidad_usuario').datetimepicker({
                                timepicker:false,
                                format: 'd/m/Y'
                        });
                });
    
        
        </script>
    </head>
        <body>
        <div class="FormBasic" style="width:480px">
            <div class="FormSectionMenu">              
            <?php   $ControlUsuario->CargaUsuarioBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlUsuario->CargaUsuarioMantenimiento();  ?>
                    </form>
            </div>    
            <div class="FormSectionGrid">          
            <?php   $ControlUsuario->CargaUsuarioSearchGrid();  ?>
            </div>    
        </div>
        </body>
    </html>

        