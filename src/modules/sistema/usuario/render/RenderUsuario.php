<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        
        class RenderUsuario
        {   private $Sufijo;
            private $Maxlen;    
            
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;
                    $this->Maxlen['id'] = 3;        
                    $this->Maxlen['usuario'] = 80;
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']       = array('0px','center','none');
                    $Columns['Usuario']  = array('292px','left','');
                    $Columns['Creación'] = array('70px','center','');
                    $Columns['IdEstado'] = array('0px','center','none');
                    $Columns['Estado']   = array('71px','left','');
                    $Columns['Caducidad'] = array('0px','center','none');
                    $Columns['Desde'] = array('0px','center','none');
                    $Columns['Hasta'] = array('0px','center','none');
                    $Columns['IdEmpleado'] = array('35px','center','none');
                    $Columns['TxEmpleado'] = array('35px','center','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
            }
               
            function CreaMantenimientoUsuario()
            {       $Search = new SearchInput($this->Sufijo);
                
                    $Usuario = '<table border=0 class="Form-Frame" cellpadding="0">';
                    $Usuario.= '       <tr height="28">';
                    $Usuario.= '             <td class="Form-Label" style="width:20%">Id</td>';
                    $Usuario.= '             <td class="Form-Label" colspan="3">';
                    $Usuario.= '                <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'usuario:caducidad\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Usuario.= '             </td>';
                    $Usuario.= '         </tr>';
                    $Usuario.= '         <tr height="28">';
                    $Usuario.= '             <td class="Form-Label">Usuario</td>';
                    $Usuario.= '             <td class="Form-Label" colspan="3">';
                    $Usuario.= '                 <input type="text" class="txt-upper t13" id="usuario'.$this->Sufijo.'" name="usuario'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['usuario'].'" disabled/>';
                    $Usuario.= '             </td>';
                    $Usuario.= '         </tr>';
                    $Usuario.= '         <tr height="28">';
                    $Usuario.= '             <td class="Form-Label">Empleado</td>';
                    $Usuario.= '             <td class="Form-Label" colspan="3">';
                                                $Usuario.= $Search->TextSch("empleado","","")->Enabled(false)->Create("t12");
                    $Usuario.= '             </td>';
                    $Usuario.= '         </tr>';
                    $Usuario.= '         <tr height="28">';
                    $Usuario.= '             <td class="Form-Label">Caducidad</td>';
                    $Usuario.= '             <td class="Form-Label" style="width:25%">';
                    $Usuario.= '                 <input type="text" class="txt-input t04" id="caducidad'.$this->Sufijo.'" name="caducidad'.$this->Sufijo.'" value="" maxlength="10" disabled />';
                    $Usuario.= '             </td>';
                    $Usuario.= '             <td class="Form-Label-Center" style="width:23%">Acceso desde</td>';
                    $Usuario.= '             <td class="Form-Label" style="width:32%">';
                                                $Usuario.= $this->HorarioAcceso("desde");
                    $Usuario.= '             </td>';
                    $Usuario.= '         </tr>';
                    $Usuario.= '         <tr height="28">';
                    $Usuario.= '             <td class="Form-Label">Estado</td>';
                    $Usuario.= '             <td class="Form-Label" style="width:25%">';
                                                 $Usuario.= $this->CreaComboEstadoByArray(array(0,1,2));
                    $Usuario.= '             </td>';
                    $Usuario.= '             <td class="Form-Label-Center" style="width:23%">Acceso hasta</td>';
                    $Usuario.= '             <td class="Form-Label" style="width:32%">';
                                                 $Usuario.= $this->HorarioAcceso("hasta");
                    $Usuario.= '             </td>';
                    $Usuario.= '         </tr>';
                    return $Usuario.'</table>';
            }

            function CreaBarButtonUsuario($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);    
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGridUsuario($Usuario)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTMLUsuario($SearchGrid,$Usuario);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }

            private function GridDataHTMLUsuario($Grid,$Datos)
            {       $Fechas = new DateControl();
                    foreach ($Datos as $usuario)
                    {       $fecreacion = $Fechas->changeFormatDate($usuario['fecreacion'],"DMY"); 
                            $fecaducidad = $Fechas->changeFormatDate($usuario['fecaducidad'],"DMY"); 
                            $id = str_pad($usuario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($usuario['usuario']));
                            $Grid->CreaSearchCellsDetalle($id,'',$fecreacion[1]);
                            $Grid->CreaSearchCellsDetalle($id,'',$usuario['idxestado']);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($usuario['txtestado']));
                            $Grid->CreaSearchCellsDetalle($id,'',$fecaducidad[1]);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($usuario['horadesde']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($usuario['horahasta']));
                            $Grid->CreaSearchCellsDetalle($id,'',$usuario['idempleado']);
                            if ($usuario['idempleado']==0){
                                $Grid->CreaSearchCellsDetalle($id,''," ");
                            }else{
                                $Grid->CreaSearchCellsDetalle($id,'', trim($usuario['apellido'])." ".trim($usuario['nombre']) );
                            }    
                            $Grid->CreaSearchRowsDetalle ($id,($usuario['idxestado']==0 ? "red" : ""));
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
               
            function MuestraUsuarioForm($Ajax,$Usuario)
            {       if (count($Usuario)>0)
                    {   $Fechas = new DateControl();
                        foreach ($Usuario as $usuario)
                        {       $fecaducidad = $Fechas->changeFormatDate($usuario['fecaducidad'],"DMY"); //->format('d/m/Y');

                                $Ajax->Assign("id".$this->Sufijo,"value", str_pad($usuario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                                $Ajax->Assign("usuario".$this->Sufijo,"value", trim($usuario['usuario']));
                                $Ajax->Assign("idempleado".$this->Sufijo,"value", trim($usuario['idempleado']));
                                if ($usuario['idempleado']==0){
                                    $Ajax->Assign("txempleado".$this->Sufijo,"value", " ");
                                }else{
                                    $Ajax->Assign("txempleado".$this->Sufijo,"value", trim($usuario['apellido'])." ".trim($usuario['nombre']));
                                }    
                                $Ajax->Assign("estado".$this->Sufijo,"value", $usuario['idxestado']);
                                $Ajax->Assign("caducidad".$this->Sufijo,"value", $fecaducidad[1]);
                                $Ajax->Assign("desde".$this->Sufijo,"value", $usuario['horadesde']);
                                $Ajax->Assign("hasta".$this->Sufijo,"value", $usuario['horahasta']);
                                $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                                return $Ajax;
                        }        
                    }
                    return $this->RespuestaUsuario($Ajax,"NOEXISTEID","No existe un usuario con el ID ingresado.");
            }

            function MuestraUsuarioGrid($Ajax,$Usuario)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTMLUsuario($SearchGrid,$Usuario);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }        
            
            function MuestraUsuarioGuardado($Ajax,$Usuario)
            {       if (count($Usuario)>0)
                    {   $id = str_pad($Usuario[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraUsuarioGrid($Ajax,$Usuario);
                        return $this->RespuestaUsuario($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaUsuario($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraUsuarioEditado($Ajax,$Usuario)
            {       foreach ($Usuario as $usuario) 
                    {       $id = str_pad($usuario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraUsuarioRowGrid($Ajax, $usuario);
                            return $this->RespuestaUsuario($xAjax,"ACTUALIZADO",$id);
                    }    
                    return $this->RespuestaUsuario($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
            
            function MuestraUsuarioEliminado($Ajax,$Usuario)
            {       foreach ($Usuario as $usuario)  
                    {   $xAjax = $this->MuestraUsuarioRowGridDelete($Ajax,$usuario);  
                        return $this->RespuestaUsuario($xAjax,"ELIMINADO",0);
                    }
                    return $this->RespuestaUsuario($Ajax,"ELIMINADO",1);
            }

            function MuestraUsuarioRowGrid($Ajax,$Usuario)
            {       $Fechas = new DateControl();
                    $fecaducidad = $Fechas->changeFormatDate($Usuario['fecaducidad'],"DMY"); 
                    
                    $Fila = $this->GetRow($Usuario['id'],$Usuario['idxestado']);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);                    
                    $Ajax->Assign($this->GetCell($Fila['Id'],1),"innerHTML",trim($Usuario['usuario'])); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],3),"innerHTML",$Usuario['idxestado']); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],4),"innerHTML",trim($Usuario['txtestado'])); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],5),"innerHTML",$fecaducidad[1]); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],6),"innerHTML",trim($Usuario['horadesde'])); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],7),"innerHTML",trim($Usuario['horahasta'])); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],8),"innerHTML",$Usuario['idempleado']); 
                    if ($Usuario['idempleado']==0){
                        $Ajax->Assign($this->GetCell($Fila['Id'],9),"innerHTML", " ");
                    }else{
                        $Ajax->Assign($this->GetCell($Fila['Id'],9),"innerHTML", trim($Usuario['apellido'])." ".trim($Usuario['nombre']));
                    }   
                    return $Ajax;
            }

            function MuestraUsuarioRowGridDelete($Ajax,$Usuario)
            {       $Fechas = new DateControl();
                    $fecaducidad = $Fechas->changeFormatDate($Usuario['fecaducidad'],"DMY");             
            
                    $Fila = $this->GetRow($Usuario['id'],$Usuario['idxestado']);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);                    
                    $Ajax->Assign($this->GetCell($Fila['Id'],3),"innerHTML",$Usuario['idxestado']); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],4),"innerHTML",trim($Usuario['txtestado']));
                    $Ajax->Assign($this->GetCell($Fila['Id'],5),"innerHTML",$fecaducidad[1]); 
                    return $Ajax;
            }        
            
            function MuestraUsuarioExcepcion($Ajax,$Msg)
            {       return $this->RespuestaUsuario($Ajax,"EXCEPCION",$Msg);    
            }
            
            private function RespuestaUsuario($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox($this->Sufijo);
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Selected(1)->Create("s04");
            }   
            
            private function GetRow($id,$estado)
            {       $RowId = $this->Sufijo."_".str_pad($id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Style = "color: ".($estado==0 ? "red" : "#000");
                    $RowOb = "TR".$RowId;
                    return array('Id' => $RowId,'Name' => $RowOb,'Color' => $Style);
            }
                
            private function GetCell($RowId,$Cell)
            {       return "TD".$RowId."_".str_pad(intval($Cell),3,'0',STR_PAD_LEFT);
            }
            
            private function HorarioAcceso($Name)
            {       $combo = '<select class="sel-input s04" id="'.$Name.$this->Sufijo.'" name="'.$Name.$this->Sufijo.'" disabled>';
                    for ($i=0; $i<24; $i++)
                    {    $hora = str_pad($i,2,'0',STR_PAD_LEFT).':00:00';
                         $half = str_pad($i,2,'0',STR_PAD_LEFT).':30:00';
                         $combo.= '<option value="'.$hora.'">'.$hora.'</option>';
                         $combo.= '<option value="'.$half.'">'.$half.'</option>';
                    }
                    $combo.= '</select>';
                    return $combo;
            }

            
            /** Modal **/
            private function SearchGridModalConfig()
            {       $Columns['Id']     = array('40px','center','');
                    $Columns['Usuario']= array('240px','left','');
                    $Columns['Nombre'] = array('215px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
            }
            
            function CreaModalGridUsuario($Operacion,$Usuario)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->ModalGridDataHTMLUsuario($SearchGrid,$Usuario);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }

            private function ModalGridDataHTMLUsuario($Grid,$Datos)
            {       foreach ($Datos as $usuarioperfil)  
                    {       $id = str_pad($usuarioperfil['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($usuarioperfil['usuario']));
                            if ($usuarioperfil['idempleado']==0){
                                $Grid->CreaSearchCellsDetalle($id,'',trim($usuarioperfil['nombre']));
                            }else{
                                $Grid->CreaSearchCellsDetalle($id,'', trim($usuarioperfil['apellido'])." ".trim($usuarioperfil['nombre']) );
                            }    
                            $Grid->CreaSearchRowsDetalle ($id,"");
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
            
            function MuestraModalGridUsuario($Ajax,$Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->ModalGridDataHTMLUsuario($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }
            
        }
?>

