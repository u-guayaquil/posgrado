<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/sistema/servicio/ServicioPerfil.php");
        require_once("src/modules/sistema/perfil/render/RenderPerfil.php");

        class ControlPerfil
        {       private  $ServicioPerfil;
                private  $RenderPerfil;

                function __construct()
                {       $this->ServicioPerfil = new ServicioPerfil();
                        $this->RenderPerfil = new RenderPerfil();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderPerfil->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderPerfil->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $prepareDQL = array('texto' => '');
                        $Datos = $this->ServicioPerfil->BuscarPerfilByDescripcion($prepareDQL);
                        echo $this->RenderPerfil->CreaSearchGrid($Datos);
                }         
               
                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioPerfil->BuscarPerfilByID($id);
                        return $this->RenderPerfil->MuestraFormulario($ajaxRespon,$Datos);
                }

                function ConsultaByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('texto' => strtoupper(trim($Texto)));
                        $ArDatos = $this->ServicioPerfil->BuscarPerfilByDescripcion($prepareDQL);
                        return $this->RenderPerfil->MuestraGrid($ajaxRespon,$ArDatos);
                }

                function Guarda($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $funcion = (empty($JsDatos->id) ? "MuestraGuardado" : "MuestraEditado");
                        $id = $this->ServicioPerfil->GuardaDBPerfil($JsDatos);
                        $ArDatos = $this->ServicioPerfil->BuscarPerfilEstadoByID($id);
                        return $this->RenderPerfil->{$funcion}($ajaxRespon,$ArDatos);
                }

                function Elimina($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsData = json_decode($Form)->Forma;
                        $JsData->estado = $this->ServicioPerfil->DesactivaPerfil($JsData->id);
                        return $this->RenderPerfil->MuestraEliminado($ajaxRespon,$JsData);
                }
        }       
?>