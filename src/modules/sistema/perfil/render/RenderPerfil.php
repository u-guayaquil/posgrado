<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioRol.php");
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/ComboBox.php');
         
        class RenderPerfil
        {   private $SearchGrid;
            private $Maxlen;    
            
            function __construct()
            {       $this->Maxlen['id'] = 3;        
                    $this->Maxlen['descripcion'] = 20;
                    $this->SearchGrid = new SearchGrid();
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']        = array('40px','center','');
                    $Columns['Perfil']    = array('180px','left','');
                    $Columns['IdDefault'] = array('0px','center','none');
                    $Columns['Default']   = array('130px','left','');
                    $Columns['IdEstado']  = array('0px','center','none');
                    $Columns['Estado']    = array('81px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }
               
            function CreaMantenimiento()
            {       $HTML = '<table border=0 class="Form-Frame" cellpadding="0">';
                    $HTML.= '       <tr height="28">';
                    $HTML.= '             <td class="Form-Label" style="width:20%">Id</td>';
                    $HTML.= '             <td class="Form-Label" style="width:80%">';
                    $HTML.= '                <input type="text" class="txt-input t01" id="id" name="id" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\'descripcion\',\'NUM\'); " onBlur="return SearchByElement(this);"/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Perfil</td>';
                    $HTML.= '             <td class="Form-Label">';
                    $HTML.= '                 <input type="text" class="txt-upper t07" id="descripcion" name="descripcion" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Default</td>';
                    $HTML.= '             <td class="Form-Label">';
                                                $HTML.= $this->CreaComboRolModulo(1);
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>'; 
                    $HTML.= '         <tr height="28">';
                    $HTML.= '             <td class="Form-Label">Estado</td>';
                    $HTML.= '             <td class="Form-Label">';
                                                $HTML.= $this->CreaComboEstadoByArray(array(0,1));
                    $HTML.= '             </td>';
                    $HTML.= '         </tr>';
                    $HTML.= '</table>';
                    return $HTML;
            }

            function CreaBarButton($Buttons)
            {       $BarButton = new BarButton();    
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGrid($ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    return $this->SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
            
            private function GridDataHTML($Grid,$ArDatos)
            {       if ($ArDatos[0])
                    {   foreach ($ArDatos[1] as $ArDato)
                        {       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);                     
                                $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['DESCRIPCION']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['IDDEFAULTBYROL']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['DEFECTO']));
                                $Grid->CreaSearchCellsDetalle($id,'',$ArDato['IDESTADO']);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['ESTADO']));
                                $Grid->CreaSearchRowsDetalle ($id,($ArDato['IDESTADO']==0 ? "red" : ""));
                        }
                    }    
                    return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
            }
               
            function MuestraFormulario($Ajax,$Perfil)
            {       if ($Perfil)
                    {   if ($Perfil->ID>=0)
                        {   $Ajax->Assign("id","value", str_pad($Perfil->ID,$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion","value", trim($Perfil->DESCRIPCION));
                            $Ajax->Assign("rol","value", $Perfil->IDDEFECTO);
                            $Ajax->Assign("estado","value", $Perfil->IDESTADO);
                            $Ajax->call("BarButtonState","Active");
                            return $Ajax;
                        }    
                    }
                    return $this->Respuesta($Ajax,"CMD_WRM","No existe un perfil con el ID ingresado.");
            }
            
            function MuestraGuardado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);
            }
            
            function MuestraGrid($Ajax,$ArDatos)
            {       $this->SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTML($this->SearchGrid,$ArDatos);
                    $Ajax->Assign($this->SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }
            
            function MuestraEditado($Ajax,$ArDatos)
            {       if ($ArDatos[0])
                    {   $id = str_pad($ArDatos[1][0]['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraRowGrid($Ajax, $ArDatos[1][0]);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);
                    }    
                    return $this->Respuesta($Ajax,"CMD_ERR",$ArDatos[1]);            
            }
    
            function MuestraRowGrid($Ajax,$ArDato)
            {       $RowId = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$ArDato['IDESTADO']);
                    
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);      
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($ArDato['DESCRIPCION']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",$ArDato['IDDEFAULTBYROL']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($ArDato['DEFECTO']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$ArDato['IDESTADO']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($ArDato['ESTADO'])); 
                    return $Ajax;
            }                                
            
            function MuestraEliminado($Ajax,$JsDatos)
            {       $RowId = str_pad($JsDatos->id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Fila = $this->SearchGrid->GetRow($RowId,$JsDatos->estado);
                    
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);       
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$JsDatos->estado); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML","INACTIVO"); 
                    return $this->Respuesta($Ajax,"CMD_DEL",0);
            }

            private function Respuesta($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse";
                    $Ajax->call($jsFuncion,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function CreaComboEstadoByArray($IdArray)
            {       $Select = new ComboBox();
                    $ServicioEstado = new ServicioEstado();
                    $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $Select->Combo("estado",$Datos)->Enabled(false)->Fields("id","descripcion")->Create("s05");
            }   
            
            private function CreaComboRolModulo($WithDef)
            {       $Select = new ComboBox();
                    $ServicioEstado = new ServicioRol();
                    $Datos = $ServicioEstado->SeleccionarRolesModulo($WithDef);
                    if ($Datos[0])
                        return $Select->Combo("rol",$Datos[1])->Enabled(false)->Fields("ID","DESCRIPCION")->Create("s05");
                    else
                        return $Datos[1];
                        
            }   
        }
?>

