<?php   
        $Opcion = "Perfil";
        require_once('src/modules/sistema/perfil/controlador/ControlPerfil.php');
        $ControlPerfil = new ControlPerfil();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlPerfil,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlPerfil,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlPerfil,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX', $ControlPerfil,'ConsultaByTX'));
        
        $xajax->processRequest();
        
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                var objperfil = "id:descripcion:estado:rol";
                function ButtonClick(Operacion)
                {       
                        var valperfil = "descripcion:rol";   
                           
                        if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementStatus("descripcion:rol","id");
                            ElementClear("id:descripcion");
                            ElementSetValue("estado",1);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Operacion);
                                if (ElementGetValue("estado")==0)
                                {   valperfil = valperfil + ":estado";
                                }    
                                ElementStatus(valperfil,"id");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue("estado")==1)
                                {   /*if (confirm("Desea inactivar este regsistro?"))
                                    {   var Forma = PrepareElements("id:estado");
                                        xajax_Elimina(JSON.stringify({Forma}));
                                    }   */
                                    Swal.fire({
                                        title: '¡Inactivar Registro!',
                                        text: "¿Desea inactivar este registro?",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Si, deseo inactivarlo!'
                                    }).then(function(result){
                                        if (result.value) {
                                            xajax_Elimina(JSON.stringify({Forma}));
                                        }
                                    }); 
                                }
                                else
                                SAlert("warning","¡Cuidado!","El registro se encuentra inactivo");
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(valperfil))
                                {   if (BarButtonState("Inactive"))
                                    {   var Forma = PrepareElements(objperfil);
                                        xajax_Guarda(JSON.stringify({Forma}));
                                    }
                                }
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Operacion);
                                ElementStatus("id","descripcion:estado:rol");
                                ElementClear("id:descripcion");
                        }
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Active");
                            ElementSetValue("id",Datos);
                            ElementStatus("id","descripcion:estado:rol");
                            SAlert("success","¡Excelente!","Los datos se guardaron correctamente.");
                        }
                        else if(Mensaje==='CMD_ERR')
                        {   BarButtonState("addNew");
                            SAlert("error","¡Error!",Datos); 
                        }    
                        else if(Mensaje==='CMD_DEL')
                        {    ElementSetValue("estado",Datos);
                        }
                        else if(Mensaje==="CMD_WRM")
                        {   BarButtonState("Default");
                            ElementClear("id");
                            SAlert("warning","¡Cuidado!",Datos); 
                        }
                }
                
                function SearchByElement(Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id")
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion");
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState("Inactive"))
                                    xajax_BuscaByID(Elemento.value);
                                }
                            }
                            else
                            BarButtonState("Default"); 
                        }
                        else
                        xajax_BuscaByTX(Elemento.value);
                }
                 
                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("descripcion").value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("rol").value = Grilla.cells[2].childNodes[0].nodeValue;
                            document.getElementById("estado").value = Grilla.cells[4].childNodes[0].nodeValue; 
                        }
                        return false;
                }
                 
        </script>
    </head>
        <body>
        <div class="FormBasic" style="width:480px">
            <div class="FormSectionMenu">              
            <?php   $ControlPerfil->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState("Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Opcion; ?>">
                    <?php  $ControlPerfil->CargaMantenimiento();  ?>
                    </form>
            </div>    
            <div class="FormSectionGrid">          
            <?php   $ControlPerfil->CargaSearchGrid();  ?>
            </div>    
        </div>
        </body>
    </html>
         
              

