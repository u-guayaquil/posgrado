<?php    
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once('src/libs/clases/ComboBox.php');
        
        class RenderUsuarioFiltro
        {   private $Sufijo;
            private $Maxlen;    
            
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;
                    $this->Maxlen['id'] = 4;        
            }
        
            private function SearchGridConfig()
            {       $Columns['Id']       = array('0px','center','none');
                    $Columns['Usuario']  = array('240px','left','');
                    $Columns['Nombre']   = array('200px','left','');
                    $Columns['Sucursal'] = array('93px','left','');
                    $Columns['IdUsuario'] = array('0px','center','none');
                    $Columns['IdSucursal']= array('0px','center','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
            }
               
            function CreaMantenimientoUsuarioFiltro()
            {       $Search = new SearchInput($this->Sufijo);
                    $UsuarioFiltro = '<table border=0 class="Form-Frame" cellpadding="0">';
                    $UsuarioFiltro.= '       <tr height="28">';
                    $UsuarioFiltro.= '             <td class="Form-Label" style="width:20%">Id</td>';
                    $UsuarioFiltro.= '             <td class="Form-Label" colspan="3">';
                    $UsuarioFiltro.= '                <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'idusuario:txusuario:empleado:idsucursal:txsucursal\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $UsuarioFiltro.= '             </td>';
                    $UsuarioFiltro.= '         </tr>';
                    $UsuarioFiltro.= '         <tr height="28">';
                    $UsuarioFiltro.= '             <td class="Form-Label">Usuario</td>';
                    $UsuarioFiltro.= '             <td class="Form-Label" colspan="3">';
                                                    $UsuarioFiltro.= $Search->TextSch("usuario","","")->Enabled(false)->Create("t12");
                    $UsuarioFiltro.= '             </td>';
                    $UsuarioFiltro.= '         </tr>';
                    $UsuarioFiltro.= '         <tr height="28">';
                    $UsuarioFiltro.= '             <td class="Form-Label">Nombre</td>';
                    $UsuarioFiltro.= '             <td class="Form-Label" colspan="3">';
                    $UsuarioFiltro.= '                 <input type="text" class="txt-upper t13" id="empleado'.$this->Sufijo.'" name="empleado'.$this->Sufijo.'" value="" disabled/>';
                    $UsuarioFiltro.= '             </td>';
                    $UsuarioFiltro.= '         </tr>';
                    $UsuarioFiltro.= '         <tr height="28">';
                    $UsuarioFiltro.= '             <td class="Form-Label">Facultad</td>';
                    $UsuarioFiltro.= '             <td class="Form-Label" colspan="3">';
                    $UsuarioFiltro.=                   $this->CreaComboFacultad();     
                    $UsuarioFiltro.= '             </td>';
                    $UsuarioFiltro.= '         </tr>';
                    $UsuarioFiltro.= '         <tr height="28">';
                    $UsuarioFiltro.= '             <td class="Form-Label">Carrera</td>';
                    $UsuarioFiltro.= '             <td class="Form-Label" colspan="3">';
                    $UsuarioFiltro.=                   $this->CreaComboCarrera();     
                    $UsuarioFiltro.= '             </td>';
                    $UsuarioFiltro.= '         </tr>';
                    
                    return $UsuarioFiltro.'</table>';
            }

            private function CreaComboFacultad()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioFacultad = new ServicioFacultad();
                    $datosFacultad = $ServicioFacultad->BuscarFacultadByDescripcion(array('texto' => ''));
                    return $Select->Combo("facultad",$datosFacultad)->Enabled(true)->Fields('COD_FACULTAD', 'NOMBRE')->Evento()->Create("s13","T");
            }

            private function CreaComboCarrera()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioCarrera = new ServicioCarrera();
                    $datosFacultad = $ServicioCarrera->BuscarByDescripcion(array('texto' => '_@_','estado' => ('A') ));
                    return $Select->Combo("carrera",$datosFacultad)->Enabled(true)->Fields('COD_FACULTAD', 'NOMBRE')->Create("s13","T");
            }
            

            function CreaBarButtonUsuarioFiltro($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);    
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaSearchGridUsuarioFiltro($UsuarioFiltro)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTMLUsuarioFiltro($SearchGrid,$UsuarioFiltro);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }

            private function GridDataHTMLUsuarioFiltro($Grid,$Datos)
            {       foreach ($Datos as $sucursalusuario)
                    {       $id = str_pad($sucursalusuario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($sucursalusuario['txtusuario']));
                            if ($sucursalusuario['idempleado']==0){
                                $Grid->CreaSearchCellsDetalle($id,'',trim($sucursalusuario['nombre']));
                            }else{
                                $Grid->CreaSearchCellsDetalle($id,'',trim($sucursalusuario['apellido'])." ".trim($sucursalusuario['nombre']));
                            }    
                            $Grid->CreaSearchCellsDetalle($id,'',trim($sucursalusuario['txtsucursal']));
                            $Grid->CreaSearchCellsDetalle($id,'',$sucursalusuario['idxusuario']);
                            $Grid->CreaSearchCellsDetalle($id,'',$sucursalusuario['idxsucursal']);
                            $Grid->CreaSearchRowsDetalle ($id,"");
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
               
            function MuestraUsuarioFiltroForm($Ajax,$UsuarioFiltro)
            {       if (count($UsuarioFiltro)>0)
                    {   foreach ($UsuarioFiltro as $sucursalusuario)
                        {       $id = str_pad($sucursalusuario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Ajax->Assign("id".$this->Sufijo,"value", $id);
                                $Ajax->Assign("idusuario".$this->Sufijo,"value", $sucursalusuario['idxusuario']);
                                $Ajax->Assign("txusuario".$this->Sufijo,"value", trim($sucursalusuario['txtusuario']));
                                if ($sucursalusuario['idempleado']==0){
                                    $Ajax->Assign("empleado".$this->Sufijo,"value", trim($sucursalusuario['nombre']));
                                }else{
                                    $Ajax->Assign("empleado".$this->Sufijo,"value", trim($sucursalusuario['apellido'])." ".trim($sucursalusuario['nombre']));
                                }    
                                $Ajax->Assign("idsucursal".$this->Sufijo,"value", $sucursalusuario['idxsucursal']);
                                $Ajax->Assign("txsucursal".$this->Sufijo,"value", trim($sucursalusuario['txtsucursal']));
                                $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                                return $Ajax;
                        }        
                    }
                    return $this->RespuestaUsuarioFiltro($Ajax,"NOEXISTEID","No existe un Usuario Perfil con el ID ingresado.");
            }

            function MuestraUsuarioFiltroGrid($Ajax,$UsuarioFiltro)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                    $GrdDataHTML = $this->GridDataHTMLUsuarioFiltro($SearchGrid,$UsuarioFiltro);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }        
            
            function MuestraUsuarioFiltroGuardado($Ajax,$UsuarioFiltro)
            {       if (count($UsuarioFiltro)>0)
                    {   $id = str_pad($UsuarioFiltro[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraUsuarioFiltroGrid($Ajax,$UsuarioFiltro);
                        return $this->RespuestaUsuarioFiltro($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaUsuarioFiltro($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraUsuarioFiltroEditado($Ajax,$UsuarioFiltro)
            {       foreach ($UsuarioFiltro as $usuario) 
                    {       $id = str_pad($usuario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraUsuarioFiltroRowGrid($Ajax, $usuario);
                            return $this->RespuestaUsuarioFiltro($xAjax,"GUARDADO",$id);
                    }    
                    return $this->RespuestaUsuarioFiltro($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
            
            function MuestraUsuarioFiltroEliminado($Ajax,$Datos)
            {        $xAjax = $this->MuestraUsuarioFiltroGrid($Ajax,$Datos);   
                     return $this->RespuestaUsuarioFiltro($xAjax,"ELIMINADO","El registro fue eliminado con éxito.");
            }

            function MuestraUsuarioFiltroRowGrid($Ajax,$UsuarioFiltro)
            {       $Fila = $this->GetRow($UsuarioFiltro['id'],1);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);                    
                    $Ajax->Assign($this->GetCell($Fila['Id'],0),"innerHTML",str_pad($UsuarioFiltro['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT)); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],1),"innerHTML",trim($UsuarioFiltro['txtusuario'])); 
                    if ($UsuarioFiltro['idempleado']==0){
                        $Ajax->Assign($this->GetCell($Fila['Id'],2),"innerHTML",trim($UsuarioFiltro['nombre']));
                    }else{
                        $Ajax->Assign($this->GetCell($Fila['Id'],2),"innerHTML",trim($UsuarioFiltro['apellido'])." ".trim($UsuarioFiltro['nombre']));
                    }    
                    $Ajax->Assign($this->GetCell($Fila['Id'],3),"innerHTML",$UsuarioFiltro['txtsucursal']); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],4),"innerHTML",$UsuarioFiltro['idxusuario']); 
                    $Ajax->Assign($this->GetCell($Fila['Id'],5),"innerHTML",$UsuarioFiltro['idxsucursal']); 
                    return $Ajax;
            }

            function MuestraUsuarioFiltroExcepcion($Ajax,$Msg)
            {       return $this->RespuestaUsuarioFiltro($Ajax,"EXCEPCION",$Msg);    
            }
            
            private function RespuestaUsuarioFiltro($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
            
            private function GetRow($id,$estado)
            {       $RowId = $this->Sufijo."_".str_pad($id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    $Style = "color: ".($estado==0 ? "red" : "#000");
                    $RowOb = "TR".$RowId;
                    return array('Id' => $RowId,'Name' => $RowOb,'Color' => $Style);
            }
                
            private function GetCell($RowId,$Cell)
            {       return "TD".$RowId."_".str_pad(intval($Cell),3,'0',STR_PAD_LEFT);
            }
            
        }
?>

