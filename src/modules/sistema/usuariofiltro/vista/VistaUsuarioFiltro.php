<?php   $Sufijo = '_usuariofiltro';

        require_once('src/modules/sistema/usuariofiltro/controlador/ControlUsuarioFiltro.php');
        $ControlUsuarioFiltro = new ControlUsuarioFiltro($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlUsuarioFiltro,'GuardaUsuarioFiltro'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlUsuarioFiltro,'EliminaUsuarioFiltro'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlUsuarioFiltro,'ConsultaUsuarioFiltroByID'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByTX'.$Sufijo, $ControlUsuarioFiltro,'ConsultaUsuarioFiltroByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlUsuarioFiltro,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlUsuarioFiltro,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                var sta_usuariofiltro = "idusuario:txusuario:idsucursal:txsucursal";
                var frm_usuariofiltro = "id:" + sta_usuariofiltro;
                var cls_usuariofiltro = "id:idusuario:txusuario:idsucursal:txsucursal:empleado";                    
                
                function ButtonClick_usuariofiltro(Sufijo,Operacion)
                {       var val_usuariofiltro = "idusuario:txusuario:idsucursal:txsucursal";   
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Sufijo,Operacion);
                            BarButtonStateEnabled(Sufijo,"btusuario:btsucursal");
                            ElementStatus(Sufijo,sta_usuariofiltro,"id");
                            ElementClear(Sufijo,cls_usuariofiltro);
                        }
                        else if (Operacion==='addMod')
                        {       BarButtonState(Sufijo,Operacion);
                                BarButtonStateEnabled(Sufijo,"btusuario:btsucursal");
                                ElementStatus(Sufijo,sta_usuariofiltro,"id");
                        }
                        else if (Operacion==='addDel')
                        {       /*if (confirm("Desea inactivar este regsistro?"))
                                {   xajax_Elimina_usuariofiltro(ElementGetValue(Sufijo,"id"));
                                }    */
                                Swal.fire({
                                        title: '¡Inactivar Registro!',
                                        text: "¿Desea inactivar este registro?",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Si, deseo inactivarlo!'
                                    }).then(function(result){
                                        if (result.value) {
                                            xajax_Elimina_usuariofiltro(ElementGetValue(Sufijo,"id"));
                                        }
                                    }); 
                        }
                        else if (Operacion==='addSav')
                        {       if (ElementValidateBeforeSave(Sufijo,val_usuariofiltro))
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    {   var Forma = PrepareElements(Sufijo,frm_usuariofiltro);
                                        xajax_Guarda_usuariofiltro(JSON.stringify({Forma}));
                                    }
                                }
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Sufijo,Operacion);
                                BarButtonStateDisabled(Sufijo,"btusuario:btsucursal");
                                ElementStatus(Sufijo,"id",sta_usuariofiltro);
                                ElementClear(Sufijo,cls_usuariofiltro);
                        }
                        else 
                            xajax_CargaModal_usuariofiltro(Operacion);
                        return false;
                }
                
                function XAJAXResponse_usuariofiltro(Sufijo,Mensaje,Datos)
                {       if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btusuario:btsucursal");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",sta_usuariofiltro);
                            SAlert("success","¡Excelente!","Los datos se guardaron correctamente.");
                        }
                        else if(Mensaje==='EXCEPCION')
                        {   BarButtonState(Sufijo,"addNew");
                            SAlert("error","¡Error!",Datos); 
                        }    
                        else if(Mensaje==='ELIMINADO')
                        {   BarButtonState(Sufijo,'addCan');
                            ElementClear(Sufijo,cls_usuariofiltro);
                            SAlert("success","¡Excelente!",Datos); 
                        }
                        else if(Mensaje==="NOEXISTEID")
                        {   BarButtonState(Sufijo,"Default");
                            ElementClear(Sufijo,"id");
                            SAlert("error","¡Error!",Datos); 
                        }
                }
                
                function SearchByElement_usuariofiltro(Sufijo,Elemento)
                {       
                        Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("txusuario"+Sufijo);
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState(Sufijo,"Inactive"))
                                    xajax_BuscaByID_usuariofiltro(Elemento.value);
                                }
                            }
                            BarButtonState(Sufijo,"Default"); //OJO AQUI
                        }
                        else if (Elemento.id === "FindUsuarioTextBx"+Sufijo)
                        {   xajax_BuscaModalByTX_usuariofiltro("btusuario",Elemento.value);
                        }
                        else if (Elemento.id === "FindSucursalTextBx"+Sufijo)
                        {   xajax_BuscaModalByTX_usuariofiltro("btsucursal",Elemento.value);
                        }
                        else
                            xajax_BuscaByTX_usuariofiltro(Elemento.value);
                }
                 
                function SearchGetData_usuariofiltro(Sufijo,Grilla)
                {       if (IsDisabled(Sufijo,"addSav"))
                        {   BarButtonState(Sufijo,"Active");
                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("idusuario"+Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
                            document.getElementById("txusuario"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("empleado"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                            document.getElementById("idsucursal"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue; 
                            document.getElementById("txsucursal"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                        }
                        return false;
                }

                function SearchUsuarioGetData_usuariofiltro(Sufijo,DatosGrid)
                {       document.getElementById("idusuario"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txusuario"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
                        document.getElementById("empleado"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }

                function SearchSucursalGetData_usuariofiltro(Sufijo,DatosGrid)
                {       document.getElementById("idsucursal"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txsucursal"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }
                
                function ControlFacultad_usuariofiltro(Sufijo,Facultad)
                {         alert(Facultad.value);
                    
                }
                
        </script>
    </head>
        <body>
        <div class="FormBasic" style="width:580px">
            <div class="FormSectionMenu">              
            <?php   $ControlUsuarioFiltro->CargaUsuarioFiltroBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlUsuarioFiltro->CargaUsuarioFiltroMantenimiento();  ?>
                    </form>
            </div>    
            <div class="FormSectionGrid">          
            <?php   $ControlUsuarioFiltro->CargaUsuarioFiltroSearchGrid();  ?>
            </div>    
        </div>
        </body>
    </html>       

        