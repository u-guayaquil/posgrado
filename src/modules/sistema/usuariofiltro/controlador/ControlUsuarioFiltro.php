<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");

        require_once("src/rules/sistema/servicio/ServicioUsuarioFiltro.php");
        require_once("src/modules/sistema/usuariofiltro/render/RenderUsuarioFiltro.php");
        
        require_once("src/rules/sistema/servicio/ServicioUsuario.php");
        require_once("src/modules/sistema/usuario/render/RenderUsuario.php");
        
        
        require_once("src/rules/general/servicio/ServicioFacultad.php");  
        //require_once("src/modules/general/sucursal/render/RenderSucursal.php");
        require_once("src/rules/general/servicio/ServicioCarrera.php");  
        

        class ControlUsuarioFiltro
        {       private $ServicioUsuarioFiltro;
                private $RenderUsuarioFiltro;
                private $ServicioUsuario;
                private $RenderUsuario;

                function __construct($Sufijo = "")
                {       $this->ServicioUsuarioFiltro = new ServicioUsuarioFiltro();
                        $this->RenderUsuarioFiltro = new RenderUsuarioFiltro($Sufijo);
                        $this->ServicioUsuario = new ServicioUsuario();
                        $this->RenderUsuario = new RenderUsuario($Sufijo);
                        $this->ServicioFacultad = new ServicioFacultad();
                        //$this->RenderSucursal = new RenderSucursal($Sufijo);
                }

                function CargaUsuarioFiltroBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $this->RenderUsuarioFiltro->CreaBarButtonUsuarioFiltro($Datos);
                }
               
                function CargaUsuarioFiltroMantenimiento()
                {       echo $this->RenderUsuarioFiltro->CreaMantenimientoUsuarioFiltro();
                }

                function CargaUsuarioFiltroSearchGrid()
                {       $prepareDQL = array('top' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByDescripcion($prepareDQL);
                        echo $this->RenderUsuarioFiltro->CreaSearchGridUsuarioFiltro($Datos);
                }         
               
                function ConsultaUsuarioFiltroByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByID($id);
                        return $this->RenderUsuarioFiltro->MuestraUsuarioFiltroForm($ajaxRespon,$Datos);
                }

                function ConsultaUsuarioFiltroByTX($Texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $texto = Trim($Texto);      
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByDescripcion($prepareDQL);
                        return $this->RenderUsuarioFiltro->MuestraUsuarioFiltroGrid($ajaxRespon,$Datos);
                }

                function GuardaUsuarioFiltro($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $UsuarioFiltro = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioUsuarioFiltro->GuardaDBUsuarioFiltro($UsuarioFiltro);
                        if (is_numeric($id)){
                            $function = (empty($UsuarioFiltro->id) ? "MuestraUsuarioFiltroGuardado" : "MuestraUsuarioFiltroEditado");
                            $Datos = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByID($id);
                            return $this->RenderUsuarioFiltro->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderUsuarioFiltro->MuestraUsuarioFiltroExcepcion($ajaxRespon,$id);
                        }
                }

                function EliminaUsuarioFiltro($id)
                {       $ajaxRespon = new xajaxResponse();
                        if ($this->ServicioUsuarioFiltro->EliminaUsuarioFiltro($id))
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                            $Datos = $this->ServicioUsuarioFiltro->BuscarUsuarioFiltroByDescripcion($prepareDQL);
                            return $this->RenderUsuarioFiltro->MuestraUsuarioFiltroEliminado($ajaxRespon,$Datos);
                        }else{
                            return $this->RenderUsuarioFiltro->MuestraUsuarioFiltroExcepcion($ajaxRespon,"El registro no puedo ser eliminado.");
                        }    
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'estado' => array(1,2) );
                        if ($Operacion==="btusuario")    
                        {   $Datos = $this->ServicioUsuario->BuscarUsuarioByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Usuario";
                            $jsonModal['Carga'] = $this->RenderUsuario->CreaModalGridUsuario($Operacion,$Datos);
                            $jsonModal['Ancho'] = "521";
                        }  
                        else if($Operacion==="btsucursal")
                        {   $Datos = $this->ServicioFacultad->BuscarSucursalByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Sucursal";
                            $jsonModal['Carga'] = $this->RenderSucursal->CreaModalGridSucursal($Operacion,$Datos);
                            $jsonModal['Ancho'] = "603";
                        }
                        
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'estado' => array(1,2));
                        if ($Operacion==="btusuario")    
                        {   $Datos = $this->ServicioUsuario->BuscarUsuarioByDescripcion($prepareDQL);
                            return $this->RenderUsuario->MuestraModalGridUsuario($ajaxResp,$Operacion,$Datos);                                        
                        }    
                        else if($Operacion==="btsucursal")
                        {   $Datos = $this->ServicioFacultad->BuscarSucursalByDescripcion($prepareDQL);
                            return $this->RenderSucursal->MuestraModalGridSucursal($ajaxResp,$Operacion,$Datos);                                        
                        }
                }

        }       
?>

