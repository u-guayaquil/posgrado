<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/sistema/servicio/ServicioOpcion.php");
        require_once("src/modules/sistema/opcion/render/RenderOpcion.php");

        class ControlOpcion
        {       private  $ServicioOpcion;
                private  $RenderOpcion;

                function __construct($Sufijo = "")
                {       $this->ServicioOpcion = new ServicioOpcion();
                        $this->RenderOpcion = new RenderOpcion();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderOpcion->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderOpcion->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $Datos = $this->ServicioOpcion->ObtenerArbolOpciones();
                        echo $this->RenderOpcion->CreaSearchGrid(json_decode($Datos));
                }         
                
                function ConsultaByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $Datos = $this->ServicioOpcion->BuscarOpcionByID($id);
                        return $this->RenderOpcion->MuestraFormulario($ajaxRespon,$Datos);
                }

                function GuardaOpcion($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $JsDatos = json_decode($Form)->Forma;
                        $IDDatos = $this->ServicioOpcion->GuardaDBOpcion($JsDatos);
                        if ($IDDatos[0])
                        {   $ArDatos = json_decode($this->ServicioOpcion->ObtenerArbolOpciones());
                            return $this->RenderOpcion->MuestraGuardado($ajaxRespon,$IDDatos[1],$ArDatos);
                        }
                        else
                        return $this->RenderOpcion->Respuesta($ajaxRespon,"CMD_ERR",$IDDatos[1]);
                }

                function EliminaOpcion($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioOpcion->DesactivaOpcion($id);
                        $ArDatos = json_decode($this->ServicioOpcion->ObtenerArbolOpciones());
                        return $this->RenderOpcion->MuestraEliminada($ajaxRespon,$ArDatos);
                }
                
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '', 'tipo' => array(0,1));
                        if ($Operacion==="btpadre")    
                        {   $ArDatos = $this->ServicioOpcion->BuscarOpcionTipoByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Menu y SubMenu";
                            $jsonModal['Carga'] = $this->RenderOpcion->CreaModalGrid($Operacion,$ArDatos);
                            $jsonModal['Ancho'] = "517";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        if ($Operacion==="btpadre")    
                        {   $Datos = $this->ServicioOpcion->BuscarOpcionTipoByDescripcion($prepareDQL);
                            return $this->RenderOpcion->MuestraModalGrid($ajaxResp,$Operacion,$Datos);                                        
                        }    
                }
        }       
?>

