<?php   $Opcion = 'Opcion';
        require_once('src/modules/sistema/opcion/controlador/ControlOpcion.php');
        $ControlOpcion = new ControlOpcion();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlOpcion,'GuardaOpcion'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlOpcion,'EliminaOpcion'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlOpcion,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlOpcion,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlOpcion,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
                
                function ButtonClick(Operacion)
                {       var objopcion = "id:orden:idtipo:descripcion:ruta:idpadre:txpadre:estado";
                        var valopcion = "orden:idtipo:descripcion";
                        
                        if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            ElementStatus("orden:idtipo:descripcion","id");
                            ElementClear(objopcion);
                            ElementSetValue("estado",1);
                            ElementSetValue("idtipo",0); 
                        }
                        else if (Operacion==='addMod')
                        {       var validar = valopcion;
                                BarButtonState(Operacion);
                                if (ElementGetValue("estado")==0){
                                    validar = validar + ":estado";
                                }
                                if (ElementGetValue("idtipo")==1){
                                    BarButtonStateEnabled("btpadre");
                                    validar = validar + ":idpadre:txpadre";
                                }
                                if (ElementGetValue("idtipo")==2){
                                    BarButtonStateEnabled("btpadre");
                                    validar = validar + ":ruta:idpadre:txpadre";
                                }
                                ElementStatus(validar,"id");
                        }
                        else if (Operacion==='addDel')
                        {       if (ElementGetValue("estado")==1)
                                {   Swal.fire({
                                        title: '¡Inactivar Registro!',
                                        text: "¿Desea inactivar este registro?",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Si, deseo inactivarlo!'
                                    }).then(function(result){
                                        if (result.value) {
                                            xajax_Elimina(ElementGetValue("id"));
                                        }});
                                }
                                else
                                SAlert("warning","¡Cuidado!","El registro se encuentra inactivo");
                        }
                        else if (Operacion==='addSav')
                        {       var validar = valopcion;
                                if (ElementGetValue("idtipo")==1){
                                    validar = validar + ":idpadre:txpadre";
                                }    
                                if (ElementGetValue("idtipo")==2){
                                    validar = validar + ":ruta:idpadre:txpadre";
                                }          
                                if (ElementValidateBeforeSave(validar))
                                {   if (BarButtonState("Inactive"))
                                    {   var Forma = PrepareElements(objopcion);
                                        xajax_Guarda(JSON.stringify({Forma}));
                                    }
                                }
                                else
                                SAlert("error","¡Campos vacíos!","Falta ingresar datos.");    
                        }
                        else if (Operacion==='addCan')
                        {       BarButtonState(Operacion);
                                BarButtonStateDisabled("btpadre");
                                ElementStatus("id","orden:idtipo:descripcion:ruta:idpadre:txpadre:estado");
                                ElementClear(objopcion);                    
                        }
                        else 
                        xajax_CargaModal(Operacion);
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Active");
                            BarButtonStateDisabled("btpadre");
                            ElementSetValue("id",Datos);
                            ElementStatus("id","orden:idtipo:descripcion:ruta:idpadre:txpadre:estado");
                            SAlert("success","¡Excelente!","Los datos se guardaron correctamente.");
                        }
                        else if(Mensaje==='CMD_DEL')
                        {   ElementSetValue("estado",Datos);
                        }
                        else if(Mensaje==='CMD_WRM')
                        {   BarButtonState('Default');
                            ElementClear("id");
                            SAlert("warning","¡Cuidado!",Datos);
                        }
                        else if(Mensaje==='CMD_ERR')
                        {    BarButtonState("addNew"); 
                             SAlert("error","¡Error!",Datos);
                        }    
                }
                
                function SearchByElement(Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id")
                        {   if (Elemento.value.length != 0)
                            {   ContentFlag = document.getElementById("descripcion");
                                if (ContentFlag.value.length==0)
                                {   if (BarButtonState("Inactive"))
                                    xajax_BuscaByID(Elemento.value);
                                }
                            }
                            else
                            BarButtonState("Default");
                        }
                        else if (Elemento.id === "FindPadreTextBx")
                        {    xajax_BuscaModalByTX("btpadre",Elemento.value);
                        }    
                }
                    
                function SearchPadreGetData(DatosGrid)
                {       document.getElementById("idpadre").value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txpadre").value = DatosGrid.cells[3].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }
                
                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("idtipo").value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("orden").value = Grilla.cells[2].childNodes[0].nodeValue;
                            document.getElementById("descripcion").value = Grilla.cells[8].childNodes[0].nodeValue; 
                            document.getElementById("ruta").value = Grilla.cells[4].childNodes[0].nodeValue; 
                            document.getElementById("idpadre").value = Grilla.cells[5].childNodes[0].nodeValue; 
                            document.getElementById("txpadre").value = Grilla.cells[6].childNodes[0].nodeValue; 
                            document.getElementById("estado").value = Grilla.cells[7].childNodes[0].nodeValue; 
                        }
                        return false;
                }
                
                function ControlIdTipo(IdTipo)
                {       if (IdTipo.value==0)
                        {   BarButtonStateDisabled("btpadre");
                            ElementStatus("","ruta:idpadre:txpadre");
                            ElementClear("ruta:idpadre:txpadre");
                        }    
                        else if (IdTipo.value==1)
                        {   BarButtonStateEnabled("btpadre");
                            ElementStatus("idpadre:txpadre","ruta");
                        }   
                        else if (IdTipo.value==2)
                        {   BarButtonStateEnabled("btpadre");
                            ElementStatus("ruta:idpadre:txpadre","");
                        }   
                        return false;
                }                
        </script>
    </head>
        <body>
        <div class="FormContainer" style="width:965px">
            <div class="FormContainerSeccion" style="width:475px">  
                <div class="FormBasic">
                    <div class="FormSectionMenu">              
                        <?php   $ControlOpcion->CargaBarButton($_GET['opcion']);
                                echo '<script type="text/javascript">';
                                echo '        BarButtonState("Default"); ';
                                echo '</script>';
                        ?>
                    </div>      
                    <div class="FormSectionData">              
                        <form id="<?php echo 'Form'.$Opcion; ?>">
                                <?php  $ControlOpcion->CargaMantenimiento();  ?>
                        </form>
                    </div>    
                </div>            
            </div>       
            <div class="FormContainerSeccion" style="width:475px">   
                <div class="FormSectionGrid">          
                    <?php  $ControlOpcion->CargaSearchGrid();  ?>
                </div>    
            </div>    
        </div>
        </body>
    </html>
