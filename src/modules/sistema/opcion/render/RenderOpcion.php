<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        
        class RenderOpcion
        {       private $Maxlen;
              
                function __construct()
                {       $this->Maxlen['id'] = 4;
                        $this->Maxlen['descripcion'] = 30;
                        $this->Maxlen['ruta'] = 255;
                }
        
                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();    
                        return $BarButton->CreaBarButton($Buttons);
                }
            
                function CreaMantenimiento()
                {       $Search = new SearchInput();
                        $HTML = '<table border=0 class="Form-Frame" cellpadding="0">';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label" style="width:20%">Id</td>';
                        $HTML.= '           <td class="Form-Label" style="width:60%">';
                        $HTML.= '               <input type="text" class="txt-input t01" id="id" name="id" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\'orden:descripcion:ruta:idpadre:txpadre\',\'NUM\'); " onBlur="return SearchByElement(this);"/>';
                        $HTML.= '           </td>';
                        $HTML.= '           <td class="Form-Label" style="width:11%">Orden</td>';
                        $HTML.= '           <td class="Form-Label" style="width:9%">';
                        $HTML.= '               <input type="text" class="txt-input t01" id="orden" name="orden" value="" maxlength="3" onKeyDown="return soloNumeros(event); " disabled/>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label">Tipo</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                        $HTML.= '               <select class="sel-input s04" id="idtipo" name="idtipo" onchange=" return ControlIdTipo(this)" disabled>';
                        $HTML.= '                       <option value="0">MENU</option>';
                        $HTML.= '                       <option value="1">SUBMENU</option>';    
                        $HTML.= '                       <option value="2">OPCION</option>';    
                        $HTML.= '               </select>';    
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label">Descripción</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                        $HTML.= '               <input type="text" class="txt-upper t10" id="descripcion" name="descripcion" value="" maxlength="'.$this->Maxlen['descripcion'].'" disabled/>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label">Ruta</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                        $HTML.= '               <input type="text" class="txt-input t14" id="ruta" name="ruta" value="" maxlength="'.$this->Maxlen['ruta'].'" disabled/>';
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label">Padre</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                                              $HTML.= $Search->TextSch("padre","","")->Enabled(false)->Create("t13");
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        $HTML.= '       <tr height="28">';
                        $HTML.= '           <td class="Form-Label">Estado</td>';
                        $HTML.= '           <td class="Form-Label" colspan="3">';
                                              $HTML.= $this->CreaComboEstadoByArray(array(0,1));
                        $HTML.= '           </td>';
                        $HTML.= '       </tr>';
                        return $HTML.'</table>';
                }
            
                private function SearchGridConfig()
                {       $Columns['Id']       = array('35px','center','');
                        $Columns['Tipo']     = array('0px','left','none');
                        $Columns['Ord']      = array('34px','left','');
                        $Columns['Opcion']   = array('360px','left','');
                        $Columns['TxRuta']   = array('0px','center','none');
                        $Columns['IdPadre']  = array('0px','center','none');
                        $Columns['TxPadre']  = array('0px','center','none');
                        $Columns['IdEstado'] = array('0px','center','none');
                        $Columns['TxDescrip']= array('0px','center','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '440px','AltoRow' => '20px','FindTxt' =>false);
                }

                function CreaSearchGrid($Opcion)
                {       $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Opcion);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }

                function GridDataHTML($grid,$root)
                {       $entro = false;
                        foreach($root as $sheet)
                        {       $id = str_pad($sheet->id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $grid->CreaSearchCellsDetalle($id,'',$id);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->idtipo);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->orden);
                                $dato = $this->FormatGridData($sheet->nivel,$sheet->idtipo,$sheet->descripcion,$sheet->ruta);        
                                $grid->CreaSearchCellsDetalle($id,'',$dato['texto']);
                                $grid->CreaSearchCellsDetalle($id,'',$dato['rutas']); //ruta
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->idpadre);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->txpadre);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->estado);
                                $grid->CreaSearchCellsDetalle($id,'',$sheet->descripcion);
                                $grid->CreaSearchRowsDetalle ($id,($sheet->estado==0 ? "red" : "")); 
                                if ($sheet->idtipo==0 || $sheet->idtipo==1){
                                    $this->GridDataHTML($grid,$sheet->hijos);
                                }
                                $entro = true;
                        }
                        
                        if ($entro)
                        {  if ($sheet->idtipo==0){
                               return $grid->CreaSearchTableDetalle();
                           }
                        }
                }
   
                private function FormatGridData($level,$types,$description,$route)
                {       $Rutas = " "; 
                        $Texto = "<b>".$description."</b>";
                        $Sangria = "<font color='#bbb'>".str_repeat("..", $level*5)."</font>";
                        if ($types==1){
                            $Texto = $Sangria." ".$Texto;
                        }    
                        elseif($types==2){
                            $Texto = $Sangria." ".$description;
                            $Rutas = $route;
                        }    
                        return array('texto' => $Texto, 'rutas' => $Rutas);
                }
            
                function MuestraFormulario($Ajax,$ArDatos)
                {       if ($ArDatos[0])
                        {   $ArDato = $ArDatos[1][0];
                            $Ajax->Assign("id","value", str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion","value", trim($ArDato['DESCRIPCION']));
                            $Ajax->Assign("idtipo","value", $ArDato['IDTIPOPCION']);
                            $Ajax->Assign("ruta","value", trim($ArDato['RUTA']));
                            $Ajax->Assign("idpadre","value", $ArDato['IDXPADRE']);
                            $Ajax->Assign("txpadre","value", trim($ArDato['TXTPADRE']));
                            $Ajax->Assign("orden","value", $ArDato['ORDEN']);
                            $Ajax->Assign("estado","value", $ArDato['IDESTADO']);
                            $Ajax->call("BarButtonState","Active");
                            return $Ajax;
                        }
                        return $this->Respuesta($Ajax,"CMD_WRM",$ArDatos[1]);
                }

                function MuestraGuardado($Ajax,$Id,$ArDatos)
                {       $id = str_pad($Id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_SAV",$id);  
                }
                
                function MuestraEliminada($Ajax,$ArDatos)
                {       $xAjax = $this->MuestraGrid($Ajax,$ArDatos);
                        return $this->Respuesta($xAjax,"CMD_DEL",0);
                }

                function Respuesta($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse";
                        $Ajax->call($jsFuncion,$Tipo,$Data);
                        return $Ajax;
                }

                private function MuestraGrid($Ajax,$Opcion)
                {       $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Opcion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function CreaComboEstadoByArray($IdArray)
                {       $Select = new ComboBox();
                        $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $Select->Combo("estado",$Datos)->Fields("id","descripcion")->Selected(1)->Create("s04");
                }        

                /*** MODAL ***/
                private function SearchGridModalConfig()
                {       $Columns['Id']     = array('40px','center','');
                        $Columns['Orden']  = array('45px','center','');
                        $Columns['Tipo']  =  array('100px','left','');
                        $Columns['Descripcion'] = array('150px','left','');
                        $Columns['Padre']  = array('150px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGrid($Operacion,$ArDatos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$ArDatos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModal($Grid,$ArDatos)
                {       if ($ArDatos[0])
                        {   foreach ($ArDatos[1] as $ArDato)
                            {       $id = str_pad($ArDato['ID'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                    $Grid->CreaSearchCellsDetalle($id,'',$id);
                                    $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['ORDEN']));
                                    $Grid->CreaSearchCellsDetalle($id,'',($ArDato['IDTIPOPCION']==0 ? "MENU":"SUBMENU"));
                                    $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['DESCRIPCION']));
                                    $Grid->CreaSearchCellsDetalle($id,'',trim($ArDato['txtpadre']));
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                            }
                        }    
                        return $Grid->CreaSearchTableDetalle(0,$ArDatos[1]);
                }
        }
?>

