<?php    
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        
        class RenderUsuarioPerfil
        {       private $Width;    

                private function SearchGridConfig()
                {       $Columns['Id']      = array('0px','center','none');
                        $Columns['IdPerfil']= array('0px','center','none');
                        $Columns['Perfil']  = array('180px','left','');
                        $Columns['Desde']   = array('70px','center','');
                        $Columns['Hasta']   = array('70px','center','');
                        $Columns['IdEstado']= array('0px','center','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px','FindTxt'=>false);
                }
               
                function CreaMantenimiento()
                {       $Search = new SearchInput();
                        $Col1="18%"; $Col2="48%"; $Col3="15%"; $Col4="19%";

                        $HTML = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $HTML.= '         <tr height="28">';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col1.'">Rol</td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '                 <input type="hidden" id="id" name="id" value=""/>';
                        $HTML.=                   $this->CreaComboRol("s08");
                        $HTML.= '             </td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col3.'">Estado</td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col4.'">';
                        $HTML.=                   $this->CreaComboEstado("s03");
                        $HTML.= '             </td>';                    
                        $HTML.= '         </tr>';                    
                        $HTML.= '         <tr height="28">';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col1.'">Usuario</td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.= '                 <input type="hidden" id="cedula" name="cedula" value=""/>';
                        $HTML.=                   $Search->TextSch("usuario","","")->Enabled(true)->Create("t07");
                        $HTML.= '             </td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col3.'">Desde</td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col4.'">';
                        $HTML.= '                 <input type="text" class="txt-input t03" id="desde" name="desde" value="" maxlength="10" onkeydown=" return false; " disabled />';
                        $HTML.= '             </td>';
                        $HTML.= '         </tr>';
                        $HTML.= '         <tr height="28">';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col1.'">Perfil</td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col2.'">';
                        $HTML.=                   $Search->TextSch("perfil","","")->Enabled(false)->Create("t07");
                        $HTML.= '             </td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col3.'"><span style="cursor:pointer" onclick=" return SeteaFecha(hasta); ">« Hasta »</span></td>';
                        $HTML.= '             <td class="Form-Label" style="width:'.$Col4.'">';
                        $HTML.= '                 <input type="text" class="txt-input t03" id="hasta" name="hasta" value="" maxlength="10" onkeydown=" return false; " disabled />';
                        $HTML.= '             </td>';
                        $HTML.= '         </tr>';
                        $HTML.= '         <tr height="28">';
                        $HTML.= '             <td class="Form-Label" colspan="3"></td>';
                        $HTML.= '             <td class="Form-Label">';
                        $HTML.= '                 <input type="button" id="btFiltro" class="p-txt-label" style="height: 25px;width: 83px;text-decoration: none;font-weight: 600;font-size: 12px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #FFF;margin-top:0px;" value="Filtro" onclick=" return ButtonClick(\'btFiltro\'); ">';                        
                        $HTML.= '             </td>';
                        $HTML.= '         </tr>';
                        return $HTML.'</table>';
                }

                private function CreaComboRol($tama)
                {       $Select = new ComboBox();
                        $ServicioRolModulo= new ServicioRolModulo();
                        $Datos = $ServicioRolModulo->BuscarRolesByModulo();
                        return $Select->Combo("rol", $Datos[1])->Fields('ID','DESCRIPCION')->Enabled(true)->Evento()->Create($tama,"SUPE");
                }               

                private function CreaComboEstado($tama)
                {       $Select = new ComboBox();
                        $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                        return $Select->Combo("estado",$Datos)->Fields("id","descripcion")->Selected(1)->Create($tama);
                }        

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton();    
                        return $BarButton->CreaBarButton($Buttons);
                }
            
                function CreaSearchGrid($Datos)
                {       $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }

                function MuestraGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid();
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridConfig());
                        $GrdDataHTML = $this->GridDataHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }
                
                private function GridDataHTML($Grid,$Datos)
                {       $count = 0;
                        foreach ($Datos as $Dato)
                        {       $count++;
                                $id = str_pad($Dato['ID'],USUARIO,'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['IDPERFIL']));
                                $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($Dato['DESCRIPCION'])));
                                $Grid->CreaSearchCellsDetalle($id,'',$Dato['FEDESDE']->format('d/m/Y'));
                                $Grid->CreaSearchCellsDetalle($id,'',$Dato['FEHASTA']->format('d/m/Y'));
                                $Grid->CreaSearchCellsDetalle($id,'',$Dato['IDESTADO']);
                                $Grid->CreaSearchRowsDetalle ($id,($Dato['IDESTADO']==1 ? "black":"red"));
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
               
                function Respuesta($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse";
                        $Ajax->call($jsFuncion,$Tipo,$Data);
                        return $Ajax;
                }
            
                function MuestraEliminado($Ajax,$Id)
                {       $SearchGrid = new SearchGrid();
                        $RowId = str_pad($Id,USUARIO,'0',STR_PAD_LEFT);
                        $Fila = $SearchGrid->GetRow($RowId,0);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);       
                        $Ajax->Assign($SearchGrid->GetCell($Fila['Id'],5),"innerHTML",0); 
                        return $this->Respuesta($Ajax,"CMD_DEL",0);
                }
            
                private function SearchGridModalConfig($Operacion)
                {       if ($Operacion=="btusuario")
                        {   $Columns['Id']     = array('40px','center','');
                            $Columns['Cedula'] = array('70px','center','');
                            $Columns['Usuario']= array('315px','left','');
                        }
                        elseif ($Operacion=="btperfil")
                        {   $Columns['Id']     = array('40px','center','');
                            $Columns['Perfil'] = array('200px','left','');
                            $Columns['Rol Automatico']= array('200px','left','');
                        }
                        else if ($Operacion=="btFiltro")
                        {   $Columns['id'] = array('40px','center','');
                            $Columns['Us'] = array('40px','center','none');
                            $Columns['Facultad'] = array('250px','left','');
                            $Columns['✔'] = array('40px','center','');
                            return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px','FindTxt'=>false);
                        }
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
                }
            
                function CreaModalGrid($Operacion,$Usuario)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Usuario,$Operacion);
                        $ObModalGrid = $SearchGrid->CreaSearchGrid($GrdDataHTML);
                        $this->Width = $SearchGrid->SearchTableWidth(); 
                        return $ObModalGrid;
                }

                private function GridDataHTMLModal($Grid,$Contenido,$Operacion)
                {       if ($Operacion=="btusuario")
                        {   if ($Contenido[0])
                            {   foreach ($Contenido[1] as $Datos)  
                                {       $id = str_pad($Datos['ID'],USUARIO,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'', trim($Datos['CEDULA']));
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($Datos['USUARIO'])));
                                        $Grid->CreaSearchRowsDetalle ($id,"");
                                }
                                return $Grid->CreaSearchTableDetalle();
                            }
                            return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                        }
                        else if ($Operacion=="btperfil")
                        {   if ($Contenido[0])
                            {   foreach ($Contenido[1] as $Datos)  
                                {       $id = str_pad($Datos['ID'],PERFIL,'0',STR_PAD_LEFT);
                                        $Grid->CreaSearchCellsDetalle($id,'',$id);
                                        $Grid->CreaSearchCellsDetalle($id,'', utf8_encode(trim($Datos['DESCRIPCION'])));
                                        $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($Datos['ROLAUTO'])));
                                        $Grid->CreaSearchRowsDetalle ($id,"");
                                }
                                return $Grid->CreaSearchTableDetalle();
                            }
                            return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                        }
                        else if ($Operacion=="btFiltro")
                        {   if ($Contenido[0])
                            {   $count = 1;
                                foreach ($Contenido[1] as $ArDato)
                                {   $id = $ArDato['ID']; 
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['ID']);
                                    $Grid->CreaSearchCellsDetalle($id,'',$ArDato['US']);
                                    $Grid->CreaSearchCellsDetalle($id,'',utf8_encode(trim($ArDato['DESCRIPCION'])));
                                    $Grid->CreaSearchCellsDetalle($id,'',($ArDato['UR']!=0 ? "✔":""));
                                    $Grid->CreaSearchRowsDetalle ($id,"black");
                                    $count++;
                                }        
                                return $Grid->CreaSearchTableDetalle();
                            }
                            return $Grid->CreaSearchTableDetalle(0,$Contenido[1]);
                        }
                }
            
                function MuestraModalGrid($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModal($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }
            
                function GetGridTableWidth()
                {        return $this->Width;
                }
        }
?>

