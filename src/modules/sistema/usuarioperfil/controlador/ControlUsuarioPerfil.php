<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/sistema/servicio/ServicioUsuarioPerfil.php");
        require_once("src/rules/sistema/servicio/ServicioUsuario.php");
        require_once("src/modules/sistema/usuarioperfil/render/RenderUsuarioPerfil.php");
        require_once("src/rules/sistema/servicio/ServicioRolModulo.php");
        require_once("src/rules/sistema/servicio/ServicioPerfil.php");
        require_once("src/rules/general/servicio/ServicioEstado.php");  
        require_once("src/rules/general/servicio/ServicioFacultad.php");
        
        class ControlUsuarioPerfil
        {       private $ServicioUsuarioPerfil;
                private $RenderUsuarioPerfil;
                private $ServicioUsuario;
                private $ServicioPerfil;
                private $ServicioFacultad;

                function __construct()
                {       $this->ServicioUsuarioPerfil = new ServicioUsuarioPerfil();
                        $this->RenderUsuarioPerfil = new RenderUsuarioPerfil();
                        $this->ServicioUsuario = new ServicioUsuario();
                        $this->ServicioPerfil = new ServicioPerfil();
                        $this->ServicioFacultad = new ServicioFacultad();
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        if ($Datos[0])
                        echo $this->RenderUsuarioPerfil->CreaBarButton($Datos[1]);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderUsuarioPerfil->CreaMantenimiento();
                }

                function CargaSearchGrid()
                {       $Datos = array();
                        echo $this->RenderUsuarioPerfil->CreaSearchGrid($Datos);
                }         
               
                function ConsultaByID($idusrol)
                {       $xAjax = new xajaxResponse();
                        $Datos = $this->ServicioUsuarioPerfil->BuscarUsuarioPerfilByID($idusrol);
                        if ($Datos[0])
                        {   $xAjax->call("BarButtonState","Default");
                            return $this->RenderUsuarioPerfil->MuestraGrid($xAjax,$Datos[1]);
                        }
                        else
                        {   $xAjax->call("BarButtonState","Inactive");
                            $xAjax->alert($Datos[1]);
                            return $xAjax; 
                        }
                }

                function Guarda($Form)
                {       $xAjax = new xajaxResponse();
                        $Datos = json_decode($Form)->Forma;
                        $Respta = $this->ServicioUsuarioPerfil->GuardaDB($Datos);
                        if ($Respta[0])
                        {   $Forma = $this->ServicioUsuarioPerfil->BuscarUsuarioPerfilByID($Datos->idusuario); 
                            $xAjax = $this->RenderUsuarioPerfil->MuestraGrid($xAjax,$Forma[1]);
                            return $this->RenderUsuarioPerfil->Respuesta($xAjax,"CMD_SAV",$Respta[1]);
                        }
                        else    
                            return $this->RenderUsuarioPerfil->Respuesta($xAjax,"CMD_ERR",$Respta[1]);
                }
                
                function GuardaFacultad($UsrID,$FacID,$Mark)
                {       $xAjax = new xajaxResponse();
                        $Resp = $this->ServicioUsuario->GuardaFacultad($UsrID,$FacID);
                        if (!$Resp[0])
                            $xAjax->alert($Resp[1]);
                        else
                        $xAjax->Assign("TD_".$FacID."_003","innerHTML",($Mark=="✔" ? "":"✔"));
                        return $xAjax;
                }
                
                function Elimina($id)
                {       $xAjax = new xajaxResponse();
                        $Rspta = $this->ServicioUsuarioPerfil->Elimina($id);
                        if ($Rspta[0])
                            return $this->RenderUsuarioPerfil->MuestraEliminado($xAjax,$Rspta[1]);
                        else
                        {   $xAjax->alert($Rspta[1]);
                            return $this->RenderUsuarioPerfil->Respuesta($xAjax,"CMD_DEL",1);
                        }
                }
                
                function CargaModalGrid($Operacion,$Ref)
                {       $xAjax = new xajaxResponse();    
                    
                        if ($Operacion==="btusuario")    
                        {   $prepareDQL = array('txt' => '','rol'=>$Ref);
                            $Datos = $this->ServicioUsuario->BuscarUsuarioByParams($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Usuario";
                            $jsonModal['Carga'] = $this->RenderUsuarioPerfil->CreaModalGrid($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderUsuarioPerfil->GetGridTableWidth();
                        }
                        elseif ($Operacion==="btperfil")
                        {   $prepareDQL = array('txt' => '','dfl'=>$Ref);
                            $Datos = $this->ServicioPerfil->BuscarPerfilByParams($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Perfil";
                            $jsonModal['Carga'] = $this->RenderUsuarioPerfil->CreaModalGrid($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderUsuarioPerfil->GetGridTableWidth();    
                        }    
                        elseif ($Operacion==="btFiltro")
                        {   $prepareDQL = array('idusrol'=>$Ref[1]);
                            $Datos = $this->ServicioFacultad->BuscarFacultadByUsuario($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Filtro Facultad";
                            $jsonModal['Carga'] = $this->RenderUsuarioPerfil->CreaModalGrid($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderUsuarioPerfil->GetGridTableWidth();    
                        }    
                        $xAjax->call("CreaModal", json_encode($jsonModal));
                        return $xAjax; 
                }

                function ConsultaModalGridByTX($Operacion,$Params)
                {       $xAjax = new xajaxResponse();    
                        if ($Operacion==="btusuario")    
                        {   $prepareDQL = array('txt'=>strtoupper(trim($Params[1])),'rol'=>$Params[0]);
                            $Datos = $this->ServicioUsuario->BuscarUsuarioByParams($prepareDQL);
                        }
                        elseif ($Operacion==="btperfil")
                        {   $prepareDQL = array('txt'=>strtoupper(trim($Params[1])),'rol'=>$Params[0]);
                            $Datos = $this->ServicioPerfil->BuscarPerfilByParams($prepareDQL);
                        }                            
                        return $this->RenderUsuarioPerfil->MuestraModalGrid($xAjax,$Operacion,$Datos);                                        
                }
        }       
?>

