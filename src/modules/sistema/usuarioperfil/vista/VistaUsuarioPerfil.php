<?php   $Opcion = 'UsuarioPerfil';

        require_once('src/modules/sistema/usuarioperfil/controlador/ControlUsuarioPerfil.php');
        $ControlUsuarioPerfil = new ControlUsuarioPerfil();

        $xajax->register(XAJAX_FUNCTION,array('Guarda', $ControlUsuarioPerfil,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('GuardaFacultad', $ControlUsuarioPerfil,'GuardaFacultad'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina', $ControlUsuarioPerfil,'Elimina'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID', $ControlUsuarioPerfil,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal', $ControlUsuarioPerfil,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX', $ControlUsuarioPerfil,'ConsultaModalGridByTX'));
        $xajax->processRequest();
?>
    <!doctype html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <?php $xajax->printJavascript(); 
              require_once('src/utils/links.php');
        ?>
        <script type="text/javascript">
            
                function ButtonClick(Operacion)
                {       if (Operacion==='addNew')
                        {   BarButtonState(Operacion);
                            BarButtonStateEnabled("btperfil");
                            BarButtonStateDisabled("btusuario");
                            ElementStatus("desde:hasta:txperfil","rol:txusuario");
                            ElementClear("desde:idperfil:txperfil:id");
                            ElementSetValue("estado",1);
                            ElementSetValue("hasta","01/01/1900");
                        }
                        else if (Operacion==='addMod')
                        {   var valestado = "";
                            BarButtonState(Operacion);
                            BarButtonStateEnabled("btperfil");
                            BarButtonStateDisabled("btusuario");
                            if (ElementGetValue("estado")==0) valestado = ":estado";
                            ElementStatus("desde:hasta:txperfil"+valestado,"rol:txusuario");
                        }
                        else if (Operacion==='addDel')
                        {   if (ElementGetValue("estado")!=0)
                            {   if (confirm("¿Desea inactivar este regsistro?"))
                                xajax_Elimina(ElementGetValue("id"));
                            }
                            else
                            alert("El registro ya se encuentra inactivo.");
                        }
                        else if (Operacion==='addSav')
                        {   if (ElementValidateBeforeSave("desde:hasta:idperfil"))
                            {   if (BarButtonState("Inactive"))
                                {   var Forma = PrepareElements("id:desde:hasta:idusuario:idperfil:estado");
                                    xajax_Guarda(JSON.stringify({Forma}));
                                }
                            }
                            else
                            alert("Falta ingresar información.");
                        }
                        else if (Operacion==='addCan')
                        {   BarButtonStateEnabled("btusuario");
                            BarButtonStateDisabled("btperfil");
                            ElementStatus("rol:txusuario","estado:desde:hasta:txperfil");
                            if (ElementGetValue("id")=="")
                            {   BarButtonState(Operacion);
                                ElementClear("desde:hasta:idperfil:txperfil:id");
                            }
                            else
                            BarButtonState("Active");
                        }
                        else if(Operacion=="btusuario")
                        {   var IDRLM = ElementGetValue("rol");
                            if (IDRLM!="@S00")
                                xajax_CargaModal(Operacion,IDRLM);
                            else
                                alert("Debe seleccionar un rol.");
                        }
                        else if(Operacion=="btperfil")
                        {   var IDRLM = ElementGetValue("rol");
                            var IDUSR = ElementGetValue("idusuario");
                            if (IDRLM!="" && IDUSR!="")
                                xajax_CargaModal(Operacion,IDRLM);
                            else
                            alert("Debe seleccionar un rol y un usuario.");
                        }
                        else if(Operacion=="btFiltro")
                        {   var IDRLM = ElementGetValue("rol");
                            var IDUSR = ElementGetValue("idusuario");
                            if (IDRLM==3 && IDUSR!="")
                            {   var Params = [IDRLM,IDUSR];
                                xajax_CargaModal(Operacion,Params);    
                            }
                            else
                            alert("Debe seleccionar un rol administrativo y un usuario.");
                        }
                        return false;
                }
                
                function XAJAXResponse(Mensaje,Datos)
                {       if (Mensaje==="CMD_SAV")
                        {   BarButtonState("Active");
                            ElementSetValue("id",Datos);
                            BarButtonStateEnabled("btusuario");
                            BarButtonStateDisabled("btperfil");
                            ElementStatus("rol:txusuario","estado:desde:hasta:txperfil");                           
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='CMD_ERR')
                        {    BarButtonState("addNew");
                             alert(Datos); 
                        }    
                        else if(Mensaje==='CMD_DEL')
                        {    ElementSetValue("estado",Datos);
                        }
                        else if(Mensaje==="CMD_WRM")
                        {   
                        }
                }
                
                function SearchByElement(Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.id==="FindUsuarioTextBx")
                        {   var Params = [ElementGetValue("rol"),Elemento.value];
                            xajax_BuscaModalByTX("btusuario",Params);
                        }
                        else if (Elemento.id==="FindPerfilTextBx")
                        {    var Params = [ElementGetValue("rol"),Elemento.value];
                             xajax_BuscaModalByTX("btperfil",Params);
                        }
                }
                 
                function SearchGetData(Grilla)
                {       if (IsDisabled("addSav"))
                        {   BarButtonState("Active");
                            document.getElementById("id").value = Grilla.cells[0].innerHTML;
                            document.getElementById("idperfil").value = Grilla.cells[1].innerHTML;
                            document.getElementById("txperfil").value = Grilla.cells[2].innerHTML;
                            document.getElementById("desde").value = Grilla.cells[3].innerHTML; 
                            document.getElementById("hasta").value = Grilla.cells[4].innerHTML;
                            document.getElementById("estado").value = Grilla.cells[5].innerHTML;
                        }
                        return false;
                }

                function ControlRol(Cmb)
                {       BarButtonState("Inactive");
                        ElementSetValue("estado",1);
                        ElementClear("id:idusuario:txusuario:idperfil:txperfil:desde:hasta");
                        ElementSetValue("SchDet","");
                        return false;
                }

                function SearchUsuarioGetData(DatosGrid)
                {       var IDUSR = DatosGrid.cells[0].innerHTML;
                        if (ElementGetValue("idusuario")!=IDUSR)
                        {   ElementSetValue("idusuario",IDUSR);
                            ElementSetValue("cedula",DatosGrid.cells[1].innerHTML);
                            ElementSetValue("txusuario",DatosGrid.cells[2].innerHTML);

                            BarButtonState("Inactive");
                            ElementSetValue("estado",1);
                            ElementClear("id:idperfil:txperfil:desde:hasta");
                            ElementSetValue("SchDet","");
                            
                            xajax_BuscaByID(IDUSR);
                        }    
                        cerrar();
                        return false;
                }
                 
                function SearchPerfilGetData(DatosGrid)
                {       var IDPER = DatosGrid.cells[0].innerHTML;
                        if (ElementGetValue("idperfil")!=IDPER)
                        {   ElementSetValue("idperfil",IDPER);
                            ElementSetValue("txperfil",DatosGrid.cells[1].innerHTML);
                        }    
                        cerrar();
                        return false;
                }
                
                function SeteaFecha(Fecha)
                {       if (!IsDisabled("addSav"))
                        {   Fecha.value = "01/01/1900";
                        }    
                        return false;
                }

                function SearchFiltroGetData(DatosGrid)
                {       var IDFAC=DatosGrid.cells[0].innerHTML;
                        var IDUSR=DatosGrid.cells[1].innerHTML; 
                        var MARKA=DatosGrid.cells[3].innerHTML;
                        xajax_GuardaFacultad(IDUSR,IDFAC,MARKA);
                        return false;
                }

                $(document).ready(function()
                {       $('#desde').datetimepicker({
                                timepicker:false,
                                format: 'd/m/Y'
                        });
                        $('#hasta').datetimepicker({
                                timepicker:false,
                                format: 'd/m/Y'
                        });
                });
        </script>
    </head>
        <body>
        <div class="FormBasic" style="width:480px">
            <div class="FormSectionMenu">              
            <?php   $ControlUsuarioPerfil->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState("Inactive"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Opcion; ?>">
                      <?php  $ControlUsuarioPerfil->CargaMantenimiento();  ?>
                </form>
            </div>    
            <div class="FormSectionGrid">          
                <?php   $ControlUsuarioPerfil->CargaSearchGrid();  ?>
            </div>    
        </div>
        </body>
    </html>

        