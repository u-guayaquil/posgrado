<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");  
        require_once('src/rules/general/servicio/ServicioSucursal.php');
        require_once('src/rules/general/servicio/ServicioTerminos.php');
        require_once("src/rules/impuesto/servicio/ServicioCreditoTributario.php");
        require_once("src/rules/impuesto/servicio/ServicioTipoComprobante.php");          
        
        
        require_once("src/rules/caja/servicio/ServicioValidacion.php");          
        require_once("src/rules/caja/servicio/ServicioTipo.php");
        require_once("src/rules/general/entidad/Estado.php"); 
        require_once("src/rules/caja/entidad/Validacion.php");
        require_once("src/rules/caja/entidad/Tipo.php");
        
        
        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/WorkGrid.php");
        require_once("src/libs/clases/DateControl.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderProvision
        {       private $Sufijo;
                //private $WorkGrid;
                
                private $Maxlen;
                private $Fechas;
                private $Search;
                private $Select;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;  
                        //$this->WorkGrid = new WorkGrid($this->Sufijo);
                        $this->Search = new SearchInput($Sufijo);
                        $this->Select = new ComboBox($Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen = 10; 
                }

                function CreaBarButton($Buttons)
                {       $BarButton = new BarButton($this->Sufijo);    
                        return $BarButton->CreaBarButton($Buttons);
                }
              
                function CreaMantenimiento()
                {       $ServicioSucursal = new ServicioSucursal();
                        $datosSucursal    = $ServicioSucursal->BuscarSucursalByDescripcion(array('limite' => 50,'inicio' => 0,'texto' => ''));
                        $ServicioTerminos = new ServicioTerminos();
                        $datosTerminos    = $ServicioTerminos->BuscarTerminosByEstadoID(1);
                        $ServicioCreditoTributario = new ServicioCreditoTributario();
                        $datosCredTrib    = $ServicioCreditoTributario->BuscarCreditoTributarioByDescripcion(array('limite' => 50,'inicio' => 0,'texto' => '','estado'=>'1'));
                        $ServicioTipoComprobante = new ServicioTipoComprobante();
                        $datosTipoCompbte = $ServicioTipoComprobante->BuscarTipoComprobanteByDescripcion(array('limite' => 50,'inicio' => 0,'texto' => '','estado'=>'1'));
                        
                        $Mantenimiento = '<table class="Form-Frame" cellpadding="0" border="0" style="width:980px">';
                        $Mantenimiento.= '<tr height="28px">';
                        $Mantenimiento.= '      <td class="Form-Label" width="88px">Id:</td>';
			$Mantenimiento.= '      <td class="Form-Label" width="163px">Sucursal:</td>';
			$Mantenimiento.= '      <td class="Form-Label" width="88px">Fecha:</td>';
			$Mantenimiento.= '      <td class="Form-Label" width="88px">Estado:</td>';
			$Mantenimiento.= '      <td class="Form-Label" width="88px">Asiento:</td>';
			$Mantenimiento.= '      <td class="Form-Label" width="163px">Término:</td>';
			$Mantenimiento.= '      <td class="Form-Label" width="213px">Tipo Comprobante:</td>';
                        $Mantenimiento.= '      <td class="Form-Label" width="89px">Anexo SRI:</td>';
                        $Mantenimiento.= '</tr>';
                        $Mantenimiento.= '<tr>';                        
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.=            $this->Search->TextSch("provision","","")->Enabled(true)->MaxLength($this->Maxlen)->ReadOnly(false)->Create("t02");
                        $Mantenimiento.='       </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.=            $this->Select->Combo('sucursal',$datosSucursal)->Fields('id', 'descripcion')->Create("s06");                                                                        
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.= '          <input type="text" class="txt-center t03" id="ingreso'.$this->Sufijo.'" name="ingreso'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.=            $this->CreaComboEstadoByArray(array(0,1));
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.= '          <input type="text" class="txt-upper t03" id="asiento'.$this->Sufijo.'" name="asiento'.$this->Sufijo.'" value="" maxlength="10" disabled readonly/>';
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.=            $this->Select->Combo('terminos',$datosTerminos)->Fields('id', 'descripcion')->Selected(0)->Create("s06");                                                                        
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.=            $this->Select->Combo('compbate',$datosTipoCompbte)->Fields('id', 'descripcion')->Selected(0)->Create("s08");                                                                        
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label-Center">';
                        $Mantenimiento.= '          <input type="checkbox" id="SRI'.$this->Sufijo.'" name="SRI'.$this->Sufijo.'" value="1" checked disabled/>'; 
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '</tr>';
                        $Mantenimiento.= '<tr height="28">';
                        $Mantenimiento.= '      <td class="Form-Label" colspan="2">Proveedor:</td>';
			$Mantenimiento.= '      <td class="Form-Label">Fecha Fact:</td>';	
    			$Mantenimiento.= '      <td class="Form-Label">Serie:</td>';
			$Mantenimiento.= '      <td class="Form-Label">Secuencia:</td>';	
			$Mantenimiento.= '      <td class="Form-Label">Autorización:</td>';		
			$Mantenimiento.= '      <td class="Form-Label">Crédito Tributario:</td>';	
                        $Mantenimiento.= '      <td class="Form-Label">Caducidad:</td>';
			$Mantenimiento.= '</tr>';
                        $Mantenimiento.= '<tr>';                        
                        $Mantenimiento.= '      <td class="Form-Label" colspan="2">';
                        $Mantenimiento.=            $this->Search->TextSch("proveedor","","")->Enabled(false)->Create("t08");
                        $Mantenimiento.='       </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.= '          <input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.= '          <input type="text" class="txt-upper t03" id="serie'.$this->Sufijo.'" name="serie'.$this->Sufijo.'" value="" maxlength="7" disabled/>';
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.= '          <input type="text" class="txt-upper t03" id="secuencia'.$this->Sufijo.'" name="secuencia'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.= '          <input type="text" class="txt-upper t06" id="autorizar'.$this->Sufijo.'" name="autorizar'.$this->Sufijo.'" value="" maxlength="50" disabled/>';
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.=            $this->Select->Combo('credtrib',$datosCredTrib)->Fields('id', 'descripcion')->Selected(0)->Create("s08");                                                                        
                        $Mantenimiento.= '      </td>';
                        $Mantenimiento.= '      <td class="Form-Label">';
                        $Mantenimiento.= '          <input type="text" class="txt-center t03" id="caduca'.$this->Sufijo.'" name="caduca'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                        $Mantenimiento.= '      </td>';
			$Mantenimiento.= '</tr>';
                        $Mantenimiento.= '</table>';
                        return $Mantenimiento;
                        
                        /*
			<tr>
				<td colspan="9"><br>
				    <table id="gridCabeceras" cellspacing="0" cellpadding="0" style="border:1px solid #06577D;background-color:#003263;float:left">
						   <tr height="25px">
							   <td class="GCI" style="width:80px">ITEM</td>
						       <td class="GCC" style="width:221px" colspan="2">DESCRIPCION</td>
							   <td class="GCC" style="width:175px">BODEGA</td>
							   <td class="GCC" style="width:80px">P.CANT</td>
							   <td class="GCC" style="width:60px">EMPAQUE</td>
							   <td class="GCC" style="width:80px">PRECIO</td>
							   <td class="GCC" style="width:40px">%DTO</td>
							   <td class="GCC" style="width:30px">IVA</td>
							   <td class="GCC" style="width:30px">ICE</td>
							   <td class="GCC" style="width:80px">TOTAL</td>
							   <td class="GCC" style="width:30px" title="Agregar Item">ADD</td>
							   <td class="GCC" style="width:18px"></td>
						   </tr>
  				           <tr height="30px">
							   <td class="GCI"><input type="text" id="ItemID" name="ItemID"       style="width:70px"  maxlength="<?php echo $LTR; ?>" onKeyDown="return depCodigos(event,'ItemTX:EmpaqueTX:IVAck:ICEck:Cantidad:Precio:Descto:Total');" onblur="return BscDescripcion(ItemID,ItemTX,'Form_Compras');" disabled/></td>
                               <td class="GCC"><input type="text" id="ItemTX" name="ItemTX"       style="width:190px" disabled readonly/></td>
				               <td class="GCI"><input class="barra_botton_bus" id="btitem" name="btitem" type="button" onclick="return BtOperator('ItemID','IT','Form_Compras');" disabled/><span id="btmvm" class="mvm" url="bsItem" w="866" h="256"></span></td>
							   <td class="GCC"><select 			  id="BodegaID"  name="BodegaID"  style="width:175px" disabled><?php select_Bodega($mylocalP); ?></select></td>
                               <td class="GCC"><input type="text" id="Cantidad"  name="Cantidad"  style="width:70px;text-align:right" maxlength="12" onKeyDown="return soloFloat(event,Cantidad,5);" onblur="return CalculaDatos('Form_Compras');" disabled/></td> 
                               <td class="GCC"><input type="text" id="EmpaqueTX" name="EmpaqueTX" style="width:70px" value="" disabled readonly/></td>
                               <td class="GCC"><input type="text" id="Precio"    name="Precio"    style="width:70px;text-align:right" maxlength="12" onKeyDown="return soloFloat(event,Precio,5);" onblur="return CalculaDatos('Form_Compras');" disabled/></td>
                               <td class="GCC"><input type="text" id="Descto"    name="Descto"    style="width:30px;text-align:right" maxlength="12" onKeyDown="return soloFloat(event,Descto,2);" onblur="return CalculaDatos('Form_Compras');" disabled/></td>
						       <td class="GCC" align="center"><input type="checkbox" id="IVAck" name="IVAck" value="1" onclick="return CalculaDatos('Form_Compras');" disabled/></td>
                               <td class="GCC" align="center"><input type="checkbox" id="ICEck" name="ICEck" value="1" disabled></td>
						       <td class="GCC"><input type="text" id="Total"     name="Total"     style="width:70px;text-align:right" maxlength="12" onKeyDown="return soloFloat(event,Total,2);" disabled readonly/></td>
                               <td class="GCC" style="text-align:center;"><input class="barra_botton_add" id="btadds" name="btadds" type="button" onclick="return inserta_filas('gridProductos');" disabled/></td>
						       <td class="GCC"></td>
                           </tr>
						   <tr>
							   <td class="GCI" colspan="13">
				                   <div id="xajax_msg_det" style='border:0px solid;width:100%;height:200px;background-color:#DDD;overflow-y:scroll;margin:0px;float:left'>
						           <table id="gridProductos" cellspacing="0" cellpadding="0" border="1" style="border:0px"></table>
							       </div>	
						       </td>
					       </tr>
						   <tr>
							   <td class="GCI" colspan="13" style="text-align:center">
								   <table id="gridTotales" cellspacing="0" cellpadding="0" border="0">
							       <tr height="30">
							              <td class="coletiqueta_s" width="80px">Subtotal</td>	
								          <td><input type="text" id="SubTotal" name="SubTotal" value="" style="text-align:right;width:80px" maxlength="12" readonly/></td>	
         								  <td class="coletiqueta_s" width="80px">Descto</td>	
								          <td><input type="text" id="DesTotal" name="DesTotal" value="" style="text-align:right;width:80px" maxlength="12" readonly/></td>	
								          <td class="coletiqueta_s" width="80px">Impuesto</td>	
								          <td><input type="text" id="IvaTotal" name="IvaTotal" value="" style="text-align:right;width:80px" maxlength="12" readonly/></td>	
								          <td class="coletiqueta_s" width="80px">Total</td>	
								          <td><input type="text" id="TotTotal" name="TotTotal" value="" style="text-align:right;width:80px" maxlength="12" readonly/></td>	
										  <td class="coletiqueta_s" width="80px" id="CaptionBar" align="right"></td>
										  <td><img Id="ProgressBar" style="visibility:hidden" src="images/ajax_wait_4.gif" width="180" height="19" border="0"></td>
							       </tr>
							       </table>
						       </td>
					       </tr>
                    </table>
				</td>	
			</tr>			
                        */


                
                }

                private function WorkGridValues()
                {       $Columns['IVA']      = array('100px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['ICE']      = array('100px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['Cuenta']   = array('150px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['Detalle']  = array('150px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['Valor']    = array('80px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['% Descto'] = array('60px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['$ Descto'] = array('80px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['$ IVA']    = array('80px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['$ Total']  = array('80px','center','','<input type="text" class="txt-center t03" id="factura'.$this->Sufijo.'" name="factura'.$this->Sufijo.'" value="" maxlength="10" disabled/>');
                        $Columns['Id']       = array('0px','center','none','');
                        $Columns['Idetalle'] = array('0px','center','none','');
                        $Columns['Idiva']    = array('0px','center','none','');                        
                        $Columns['Idice']    = array('0px','center','none','');
                        $Columns['Idcta']    = array('0px','center','none','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
                }

                
                function CreaWorkGrid()
                {       $WorkGrid = new WorkGrid($this->Sufijo);
                        $WorkGrid->ConfiguraWorkGrid($this->WorkGridValues());
                        return $WorkGrid->CreaWorkGrid($ObjHTML);
                }
                
                private function CreaComboEstadoByArray($IdArray)
                {       $ServicioEstado = new ServicioEstado();
                        $Datos = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $this->Select->Combo("estado",$Datos)->Selected(1)->Create("s03");
                }        
                
                
        }
        
        
?>        