<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php"); 
        require_once("src/modules/compras/provision/render/RenderProvision.php");        
/*
        require_once("src/rules/caja/servicio/ServicioPago.php");
        require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
        require_once("src/rules/general/servicio/ServicioSucursal.php");
        
        
        require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
        require_once("src/modules/general/sucursal/render/RenderSucursal.php");        
 * 
 */
        require_once('src/libs/clases/DateControl.php');


        class ControlProvision
        {       private  $Sufijo; 
                private  $ServicioPago;
                private  $ServicioPlanCuentas;
                private  $ServicioSucursal;
                private  $RenderProvision;
                private  $RenderPlanCuentas;
                private  $RenderSucursal;

                function __construct($Sufijo = "")
                {        $this->Sufijo = $Sufijo;
                        //$this->ServicioPago = new ServicioPago();
                        //$this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        //$this->ServicioSucursal = new ServicioSucursal();
                        $this->RenderProvision = new RenderProvision($Sufijo);
                        //$this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
                        //$this->RenderSucursal = new RenderSucursal($Sufijo);
                }

                function CargaBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $this->RenderProvision->CreaBarButton($Datos);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderProvision->CreaMantenimiento();
                }

                function CargaWorkGrid()
                {       echo $this->RenderProvision->CreaWorkGrid();
                }
                
               
        }       
?>