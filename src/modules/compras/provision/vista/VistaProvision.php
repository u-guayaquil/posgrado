<?php
        $Sufijo = '_Provision';
         
        require_once('src/modules/compras/provision/controlador/ControlProvision.php');
        $ControlProvision = new ControlProvision($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlProvision,'GuardaDocumento'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlProvision,'MuestraDocumentoByID'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlProvision,'MuestraDocumentoByTX'));
        $xajax->register(XAJAX_FUNCTION,array('PresentGridData'.$Sufijo, $ControlProvision,'PresentaGridData'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlProvision,'EliminaDocumento'));
        $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <?php   $xajax->printJavascript(); 
                require_once('src/utils/links.php');
        ?>
         <script type="text/javascript"> 
                 function ButtonClick_Documento(Sufijo,Operacion)
                 {        var objDocumento = "id:descripcion:estado:tipo:validacion";
                          var frmDocumento = "descripcion:estado:tipo:validacion";
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,objDocumento,"id");
                              ElementClear(Sufijo,objDocumento);
                              ElementSetValue(Sufijo,"estado",1);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objDocumento,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_Documento(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"descripcion"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {    var Forma = PrepareElements(Sufijo,objDocumento);
                                        xajax_Guarda_Documento(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmDocumento);
                               ElementClear(Sufijo,objDocumento);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_Documento(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_Documento(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_Documento(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_Documento(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")){
                             BarButtonState(Sufijo,"Active");

                             document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                             document.getElementById("descripcion"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                             document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;        
                             document.getElementById("validacion"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                             document.getElementById("tipo"+Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
                          }
                          return false;
                 }
                 
                function XAJAXResponse_Documento(Sufijo,Mensaje,Datos)
                {       
                        var objDocumento = "descripcion:estado:tipo:validacion";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objDocumento);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                }                 
         </script>
         </head>
<body>
        <div class="FormBasic" style="width:1000px">
            <div class="FormSectionMenu">              
            <?php   $ControlProvision->CargaBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Sufijo; ?>">
                <?php   $ControlProvision->CargaMantenimiento();              ?>
                </form>
            </div>    
            <div class="FormSectionGrid"> 
            <?php       $ControlProvision->CargaWorkGrid();                   ?>
            </div>  
        </div> 
</body>
</html> 
              


