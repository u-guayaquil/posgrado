<?php
         require_once("src/rules/impuesto/servicio/ServicioTipoComprobante.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/impuesto/tipocomprobante/render/RenderTipoComprobante.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlTipoComprobante
         {     private  $Sufijo; 
               private  $ServicioTipoComprobante;
               private  $RenderTipoComprobante;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioTipoComprobante = new ServicioTipoComprobante();
                        $this->RenderTipoComprobante = new RenderTipoComprobante($Sufijo);
               }

               function CargaTipoComprobanteBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaTipoComprobanteMantenimiento()
               {        echo $this->RenderTipoComprobante->CreaTipoComprobanteMantenimiento();
               }

               function CargaTipoComprobanteSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoTipoComprobante = $this->ServicioTipoComprobante->BuscarTipoComprobanteByDescripcion($prepareDQL);
                        echo $this->RenderTipoComprobante->CreaTipoComprobanteSearchGrid($datoTipoComprobante); 
               }         
               
               function MuestraTipoComprobanteByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoTipoComprobante = $this->ServicioTipoComprobante->BuscarTipoComprobanteByID($prepareDQL);
                        return $this->RenderTipoComprobante->MuestraTipoComprobante($ajaxRespon,$datoTipoComprobante);
               }

               function MuestraTipoComprobanteByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoTipoComprobante = $this->ServicioTipoComprobante->BuscarTipoComprobanteByDescripcion($prepareDQL);
                        return $this->RenderTipoComprobante->MuestraTipoComprobanteGrid($ajaxRespon,$datoTipoComprobante);
               }

               function GuardaTipoComprobante($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $TipoComprobante  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioTipoComprobante->GuardaDBTipoComprobante($TipoComprobante);
                        if (is_numeric($id)){
                            $function = (empty($TipoComprobante->id) ? "MuestraTipoComprobanteGuardado" : "MuestraTipoComprobanteEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioTipoComprobante->BuscarTipoComprobanteByID($prepareDQL);
                            return $this->RenderTipoComprobante->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderTipoComprobante->MuestraTipoComprobanteExcepcion($ajaxRespon,intval($id));
                        }
               } 
               
               function EliminaTipoComprobante($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioTipoComprobante->DesactivaTipoComprobante(intval($id));
                        $Datos = $this->ServicioTipoComprobante->BuscarTipoComprobanteByDescripcion($prepareDQL);
                        return $this->RenderTipoComprobante->MuestraTipoComprobanteEliminado($ajaxRespon,$Datos);
                }
               
               
               
         }

?>

