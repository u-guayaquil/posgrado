<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderTipoComprobante
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;   
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2;                       
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaTipoComprobanteMantenimiento()
              {        $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                  
                       $TipoComprobante = '<table class="Form-Frame" cellpadding="0">';
                       $TipoComprobante.= '       <tr height="30">';
                       $TipoComprobante.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $TipoComprobante.= '             <td class="Form-Label" style="width:75%">';
                       $TipoComprobante.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $TipoComprobante.= '             </td>';
                       $TipoComprobante.= '         </tr>';
                       $TipoComprobante.= '         <tr height="30">';
                       $TipoComprobante.= '             <td class="Form-Label" style="width:25%">C&oacute;digo</td>';
                       $TipoComprobante.= '             <td class="Form-Label" style="width:75%">';
                       $TipoComprobante.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t07" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $TipoComprobante.= '             </td>';
                       $TipoComprobante.= '         </tr>';
                       $TipoComprobante.= '         <tr height="30">';
                       $TipoComprobante.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $TipoComprobante.= '             <td class="Form-Label" style="width:75%">';
                       $TipoComprobante.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $TipoComprobante.= '             </td>';
                       $TipoComprobante.= '         </tr>';
                       $TipoComprobante.= '         </tr>';                       
                       $TipoComprobante.= '         <tr height="30">';
                       $TipoComprobante.= '             <td class="Form-Label">Estado</td>';
                       $TipoComprobante.= '             <td class="Form-Label">';
                                                   $TipoComprobante.= $this->CreaComboEstado();
                       $TipoComprobante.= '             </td>';
                       $TipoComprobante.= '         </tr>';
                       $TipoComprobante.= '</table>';
                       return $TipoComprobante;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('80px','left','');
                        $Columns['Descripci&oacute;n'] = array('100px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaTipoComprobanteSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoComprobanteGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function TipoComprobanteGridHTML($ObjSchGrid,$Datos)
              {       
                  if(count($Datos)>0){
                     foreach ($Datos as $comprobante)
                      {        $fecreacion = $this->Fechas->changeFormatDate($comprobante['fecreacion'],"DMY");
                               $id = str_pad($comprobante['id'],2,'0',STR_PAD_LEFT);
                               $codigo = str_pad($comprobante['codigo'],10,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($codigo));                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($comprobante['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$comprobante['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$comprobante['idestado']); 
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($comprobante['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraTipoComprobante($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo'])); 
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaTipoComprobante($Ajax,"NOEXISTEID","No existe un Tipo con ese ID.");
                       }
              }
              function MuestraTipoComprobanteGrid($Ajax,$Datos)
              {        
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoComprobanteGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }        

              function MuestraTipoComprobanteGuardado($Ajax,$TipoComprobante)
              {       if (count($TipoComprobante)>0)
                        {   $id = str_pad($TipoComprobante[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraTipoComprobanteGrid($Ajax,$TipoComprobante);
                            return $this->RespuestaTipoComprobante($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaTipoComprobante($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraTipoComprobanteEditado($Ajax,$TipoComprobante)
              {       foreach ($TipoComprobante as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoComprobanteRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoComprobante($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaTipoComprobante($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraTipoComprobanteEliminado($Ajax,$TipoComprobante)
              {        foreach ($TipoComprobante as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoComprobanteRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoComprobante($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaTipoComprobante($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraTipoComprobanteRowGrid($Ajax,$TipoComprobante,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($TipoComprobante['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($TipoComprobante['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($TipoComprobante['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($TipoComprobante['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($TipoComprobante['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($TipoComprobante['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($TipoComprobante['idestado'])); 
                        return $Ajax;
                }

              function MuestraTipoComprobanteExcepcion($Ajax,$Msg)
              {        return $this->RespuestaTipoComprobante($Ajax,"EXCEPCION",$Msg);    
              }

              private function RespuestaTipoComprobante($Ajax,$Tipo,$Data)
              {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                      $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                      return $Ajax;
              }
              
              /*** MODAL ***/
                private function SearchGridModalConfig()
                {       $Columns['Id']     = array('40px','center','');
                        $Columns['Codigo']  = array('45px','center','');
                        $Columns['Descripcion'] = array('150px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGridTipoComprobante($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalTipoComprobante($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGridTipoComprobante($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalTipoComprobante($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalTipoComprobante($Grid,$Datos)
                {       foreach ($Datos as $Opcion)
                        {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['descripcion']));
                                $Grid->CreaSearchRowsDetalle ($id,"black");
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
        }
?>

