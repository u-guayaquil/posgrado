<?php
         $Sufijo = '_iva';

         require_once('src/modules/impuesto/iva/controlador/ControlIva.php');
         $ControlIva = new ControlIva($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlIva,'GuardaIva'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlIva,'MuestraIvaByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlIva,'MuestraIvaByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlIva,'EliminaIva'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlIva,'CargaModalGridIva'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlIva,'ConsultaModalGridIvaByTX'));
         
         $xajax->processRequest();

?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript">
                 function ButtonClick_iva(Sufijo,Operacion)
                 {        var objIva = "id:estado:porcentaje:impuesto:txCuentaIva:idCuentaIva";
                          var frmIva = "estado:porcentaje:impuesto:txCuentaIva:idCuentaIva";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,frmIva,"id:estado");
                              BarButtonStateEnabled(Sufijo,"btCuentaIva");
                              ElementClear(Sufijo,objIva);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               BarButtonStateEnabled(Sufijo,"btCuentaIva");
                               ElementStatus(Sufijo,frmIva,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_iva(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,frmIva))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objIva);
                                       xajax_Guarda_iva(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               BarButtonStateDisabled(Sufijo,"btCuentaIva");
                               ElementStatus(Sufijo,"id",frmIva);
                               ElementClear(Sufijo,objIva);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                           else { 
                             xajax_CargaModal_iva(Operacion);  
                           }
                            
                          return false;
                 }

                 function SearchByElement_iva(Sufijo,Elemento)
                 {      Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   if (BarButtonState(Sufijo,"Inactive"))
                                xajax_MuestraByID_iva(Elemento.value);                                  
                            }
                            else
                            BarButtonState(Sufijo,"Default");
                        }
                        else if (Elemento.id === "FindCuentaIvaTextBx"+Sufijo)
                        {   xajax_BuscaModalByTX_iva("btCuentaIva",Elemento.value);
                        } 
                        else 
                        xajax_MuestraByTX_iva(Elemento.value);    
                 }
                 
                 function SearchGetData_iva(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")) {
                                BarButtonState(Sufijo,"Active");

                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                                document.getElementById("porcentaje"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                                document.getElementById("idCuentaIva"+Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
                                document.getElementById("txCuentaIva"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                                document.getElementById("impuesto"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;  
                                document.getElementById("estado"+Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue; 
                          }
                          return false;
                 }
                 
                 function SearchCuentaIvaGetData_iva(Sufijo,DatosGrid)
                 {       document.getElementById("idCuentaIva"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                         document.getElementById("txCuentaIva"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                                                cerrar();
                         return false;
                 }
                                 
                 function XAJAXResponse_iva(Sufijo,Mensaje,Datos)
                 {       
                        var objIva = "estado:porcentaje:impuesto:txCuentaIva";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btCuentaIva");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objIva);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
                 
         </script>
          </head>
<body>       
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlIva->CargaIvaBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '    BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlIva->CargaIvaMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlIva->CargaIvaSearchGrid();  ?>
              </div>  
         </div> 
       </body>
</html>  
              

