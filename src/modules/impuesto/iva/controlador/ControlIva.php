    <?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/impuesto/servicio/ServicioIva.php");
        require_once("src/modules/impuesto/iva/render/RenderIva.php");
        
        require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
        require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
        
         class ControlIva
         {     private  $Sufijo; 
               private  $ServicioIva;
               private  $RenderIva;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioIva = new ServicioIva();
                        $this->RenderIva = new RenderIva($Sufijo);
               }

               function CargaIvaBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaIvaMantenimiento()
               {        echo $this->RenderIva->CreaIvaMantenimiento();
               }

               function CargaIvaSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoIva = $this->ServicioIva->BuscarIvaByDescripcion($prepareDQL);
                        echo $this->RenderIva->CreaIvaSearchGrid($datoIva);
               }         
               
               function MuestraIvaByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoIva = $this->ServicioIva->BuscarIvaByID($prepareDQL);
                        return $this->RenderIva->MuestraIva($ajaxRespon,$datoIva);
               }

               function MuestraIvaByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoIva = $this->ServicioIva->BuscarIvaByDescripcion($prepareDQL);
                        return $this->RenderIva->MuestraIvaGrid($ajaxRespon,$datoIva);
               }

               function GuardaIva($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Iva  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioIva->GuardaDBIva($Iva);
                        if (is_numeric($id)){
                            $function = (empty($Iva->id) ? "MuestraIvaGuardado" : "MuestraIvaEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioIva->BuscarIvaByID($prepareDQL);
                            return $this->RenderIva->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderIva->MuestraIvaExcepcion($ajaxRespon,intval($id));
                        }
               }
               
               function EliminaIva($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioIva->DesactivaIva(intval($id));
                        $Datos = $this->ServicioIva->BuscarIvaByDescripcion($prepareDQL);
                        return $this->RenderIva->MuestraIvaEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridIva($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '');
                        if ($Operacion==="btCuentaIva")    
                        {   $ServicioPlanCuentas = new ServicioPlanCuentas();
                            $RenderPlanCuentas = new RenderPlanCuentas($this->Sufijo);
                            $Datos = $ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Buscar Cuenta";
                            $jsonModal['Carga'] = $RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                            $jsonModal['Ancho'] = "750";                        
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                                
                function ConsultaModalGridIvaByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        if ($Operacion==="btCuentaIva")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                            $ServicioPlanCuentas = new ServicioPlanCuentas();
                            $RenderPlanCuentas = new RenderPlanCuentas($this->Sufijo);
                            $Datos = $ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            return $RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);                                                                
                        }
                }
         }

?>

