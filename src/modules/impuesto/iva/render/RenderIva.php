<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/impuesto/servicio/ServicioIvaPorcentaje.php");
        require_once("src/rules/impuesto/servicio/ServicioTipoImpuesto.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderIva
        {   private $Sufijo;
            private $SearchGrid;
            private $Maxlen;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Maxlen['id'] = 2; 
            }
               
            function CreaIvaMantenimiento()
            {       $Search = new SearchInput($this->Sufijo);
                    $Iva = '<table class="Form-Frame" cellpadding="0" border="0">';
                    $Iva.= '        <tr height="30">';
                    $Iva.= '            <td class="Form-Label" style="width:25%">Id</td>';
                    $Iva.= '            <td class="Form-Label" style="width:75%">';
                    $Iva.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'porcentaje:impuesto:txCuentaIva:idCuentaIva\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Iva.= '            </td>';
                    $Iva.= '         </tr>';
                    $Iva.= '        <tr height="30">';
                    $Iva.= '            <td class="Form-Label">Tipo</td>';
                    $Iva.= '            <td class="Form-Label">';
                    $Iva.=                  $this->CreaComboTipoImpuesto();
                    $Iva.= '            </td>';
                    $Iva.= '        </tr>';
                    $Iva.= '        <tr height="30">';
                    $Iva.= '            <td class="Form-Label">Porcentaje</td>';
                    $Iva.= '            <td class="Form-Label">';
                    $Iva.=                  $this->CreaComboPorcentaje();
                    $Iva.= '            </td>';
                    $Iva.= '        </tr>';
                    $Iva.= '        <tr height="30">';
                    $Iva.= '            <td class="Form-Label">Cuenta</td>';
                    $Iva.= '            <td class="Form-Label">';
                    $Iva.=                  $Search->TextSch("CuentaIva","","")->Enabled(false)->Create("t14");
                    $Iva.= '            </td>';
                    $Iva.= '        </tr>';
                    $Iva.= '        <tr height="30">';
                    $Iva.= '            <td class="Form-Label">Estado</td>';
                    $Iva.= '            <td class="Form-Label">';
                    $Iva.=                  $this->CreaComboEstado();
                    $Iva.= '            </td>';
                    $Iva.= '        </tr>';
                    $Iva.= '</table>';
                    return $Iva;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']         = array('30px','center','');
                    $Columns['Impuesto']   = array('105px','left','');
                    $Columns['Tipo']       = array('250px','left','');
                    $Columns['Estado']     = array('100px','left','');
                    $Columns['idctacontable'] = array('0px','left','none');
                    $Columns['cuenta']        = array('0px','left','none');
                    $Columns['idporcentaje']  = array('0px','left','none');
                    $Columns['idimpuesto']    = array('0px','left','none');
                    $Columns['estado']        = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
            }

            function CreaIvaSearchGrid($Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->IvaGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }        

            private function IvaGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $plancuentas)
                    {       $id = str_pad($plancuentas['id'],2,'0',STR_PAD_LEFT);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['porcentaje']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['impuesto']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['txtestado']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['idctacontable']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'', strtoupper(trim($plancuentas['cuentacontable'])));                           
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$plancuentas['idporcentaje']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$plancuentas['idimpuesto']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$plancuentas['idestado']);
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,($plancuentas['idestado']==0 ? "red" : ""));

                    }
                    return $ObjSchGrid->CreaSearchTableDetalle();
            }

            private function CreaComboEstado()
            {       $Select = new ComboBox($this->Sufijo);
                    $ServicioEstado = new ServicioEstado();
                    $datoEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));  
                    return $Select->Combo("estado",$datoEstado)->Selected(1)->Create("s04");
            }
              
            private function CreaComboPorcentaje()
            {       $Select = new ComboBox($this->Sufijo);  
                    $ServicioIvaPorcentaje = new ServicioIvaPorcentaje();
                    $datosPorcentaje = $ServicioIvaPorcentaje->BuscarIvaPorcentajeByArrayID(array('limite' => 50,'estado'=>1));
                    return $Select->Combo("porcentaje",$datosPorcentaje)->Selected(1)->Create("s04");
            }
              
            private function CreaComboTipoImpuesto()
            {       $ServicioTipoImpuesto = new ServicioTipoImpuesto();
                    $datoTipoImpuesto = $ServicioTipoImpuesto->BuscarTipoImpuestoByPadre('2,3');         
                    $Select = new ComboBox($this->Sufijo);                        
                    return $Select->Combo("impuesto",$datoTipoImpuesto)->Selected(1)->Create("s08");
            }
              
            function MuestraIva($Ajax,$Datos)
            {       if($Datos[0]['id'] != ''){
                        $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                        $Ajax->Assign("porcentaje".$this->Sufijo,"value", $Datos[0]['idporcentaje']);
                        $Ajax->Assign("impuesto".$this->Sufijo,"value", $Datos[0]['idimpuesto']);
                        $Ajax->Assign("txCuentaIva".$this->Sufijo,"value", strtoupper(trim($Datos[0]['cuentacontable'])));
                        $Ajax->Assign("idCuentaIva".$this->Sufijo,"value", $Datos[0]['idctacontable']);
                        $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                        $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                        return $Ajax;
                    }else{
                        return $this->RespuestaIva($Ajax,"NOEXISTEID","No existe un Codigo con ese ID.");
                    }
            }
            
            function MuestraIvaGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->IvaGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }
               
            function MuestraIvaGuardado($Ajax,$Iva)
            {       if (count($Iva)>0)
                    {   $id = str_pad($Iva[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraIvaGrid($Ajax,$Iva);
                        return $this->RespuestaIva($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaIva($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraIvaEditado($Ajax,$Iva)
            {       foreach ($Iva as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraIvaRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaIva($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaIva($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraIvaEliminado($Ajax,$Iva)
            {       foreach ($Iva as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraIvaRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaIva($xAjax,"ELIMINADO",$datos['idestado']);
                    }  
                    return $this->RespuestaIva($xAjax,"EXCEPCION","No se eliminó la información.");
            }
              
            function MuestraIvaRowGrid($Ajax,$Iva,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($Iva['id'],$estado);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Iva['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Iva['porcentaje']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Iva['impuesto']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Iva['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$Iva['idctacontable']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",strtoupper(trim($Iva['cuentacontable'])));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$Iva['idporcentaje']); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$Iva['idimpuesto']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",$Iva['idestado']);
                    return $Ajax;
            }

            function MuestraIvaExcepcion($Ajax,$Msg)
            {       return $this->RespuestaIva($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaIva($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
                
              /*** MODAL ***/
            /*
            private function SearchGridModalConfig()
            {       $Columns['Id']     = array('40px','center','');
                    $Columns['Codigo']  = array('45px','center','');
                    $Columns['Descripcion'] = array('150px','left','');
                    $Columns['Padre']  = array('150px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
            }

            function CreaModalGridIva($Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->GridDataHTMLModalIva($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
                
            function MuestraModalGridIva($Ajax,$Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->GridDataHTMLModalIva($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }        

            private function GridDataHTMLModalIva($Grid,$Datos)
            {       foreach ($Datos as $Opcion)
                    {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['descripcion']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['padre']));
                            $Grid->CreaSearchRowsDetalle ($id,"black");
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
                
            function CreaModalExcelIva()
            {       $Iva = '<table class="Form-Frame" cellpadding="0">';
                    $Iva.= '</table>';
                    return $Iva;
            }
             * 
             */
        }
?>

