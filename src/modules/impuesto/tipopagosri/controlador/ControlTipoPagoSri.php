<?php
         require_once("src/rules/impuesto/servicio/ServicioTipoPagoSri.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/impuesto/tipopagosri/render/RenderTipoPagoSri.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlTipoPagoSri
         {     private  $Sufijo;
               private  $ServicioTipoPagoSri;
               private  $RenderTipoPagoSri;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioTipoPagoSri = new ServicioTipoPagoSri();
                        $this->RenderTipoPagoSri = new RenderTipoPagoSri($Sufijo);
               }
               
               function CargaTipoPagoSriBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
               function CargaTipoPagoSriMantenimiento()
               {        echo $this->RenderTipoPagoSri->CreaTipoPagoSriMantenimiento();
               }

               function CargaTipoPagoSriSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoTipoPagoSri = $this->ServicioTipoPagoSri->BuscarTipoPagoSriByDescripcion($prepareDQL);
                        echo $this->RenderTipoPagoSri->CreaTipoPagoSriSearchGrid(($datoTipoPagoSri)); 
               }         
               
               function MuestraTipoPagoSriByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoTipoPagoSri = $this->ServicioTipoPagoSri->BuscarTipoPagoSriByID($prepareDQL);
                        return $this->RenderTipoPagoSri->MuestraTipoPagoSri($ajaxRespon,$datoTipoPagoSri);
               }

               function MuestraTipoPagoSriByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoTipoPagoSri = $this->ServicioTipoPagoSri->BuscarTipoPagoSriByDescripcion($prepareDQL);
                        return $this->RenderTipoPagoSri->MuestraTipoPagoSriGrid($ajaxRespon,$datoTipoPagoSri);
               }

               function GuardaTipoPagoSri($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $TipoPagoSri  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioTipoPagoSri->GuardaDBTipoPagoSri($TipoPagoSri);
                        if (is_numeric(($id))){
                            $function = (empty($TipoPagoSri->id) ? "MuestraTipoPagoSriGuardado" : "MuestraTipoPagoSriEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioTipoPagoSri->BuscarTipoPagoSriByID($prepareDQL);
                            return $this->RenderTipoPagoSri->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderTipoPagoSri->MuestraTipoPagoSriExcepcion($ajaxRespon,intval($id));
                        }                    
               }
               
               function EliminaTipoPagoSri($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioTipoPagoSri->DesactivaTipoPagoSri(intval($id));
                        $Datos = $this->ServicioTipoPagoSri->BuscarTipoPagoSriByDescripcion($prepareDQL);
                        return $this->RenderTipoPagoSri->MuestraTipoPagoSriEliminado($ajaxRespon,$Datos);
                }
               
               
               
         }

?>

