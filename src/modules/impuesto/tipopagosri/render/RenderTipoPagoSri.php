<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/caja/servicio/ServicioTipo.php");  ;
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderTipoPagoSri
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2;
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
              
              function CreaTipoPagoSriMantenimiento()
              {                               
                       $TipoPagoSri = '<table class="Form-Frame" cellpadding="0">';
                       $TipoPagoSri.= '       <tr height="30">';
                       $TipoPagoSri.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $TipoPagoSri.= '             <td class="Form-Label" style="width:75%">';
                       $TipoPagoSri.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $TipoPagoSri.= '             </td>';
                       $TipoPagoSri.= '         </tr>';
                       $TipoPagoSri.= '         <tr height="30">';
                       $TipoPagoSri.= '             <td class="Form-Label" style="width:25%">C&oacute;digo</td>';
                       $TipoPagoSri.= '             <td class="Form-Label" style="width:75%">';
                       $TipoPagoSri.= '                 <input type="text" class="txt-upper t07" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $TipoPagoSri.= '             </td>';
                       $TipoPagoSri.= '         </tr>';
                       $TipoPagoSri.= '         <tr height="30">';
                       $TipoPagoSri.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $TipoPagoSri.= '             <td class="Form-Label" style="width:75%">';
                       $TipoPagoSri.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $TipoPagoSri.= '             </td>';
                       $TipoPagoSri.= '         </tr>';
                       $TipoPagoSri.= '         <tr height="30">';
                       $TipoPagoSri.= '             <td class="Form-Label">Tipo</td>';
                       $TipoPagoSri.= '             <td class="Form-Label">';
                                                   $TipoPagoSri.= $this->CreaComboTipo();
                       $TipoPagoSri.= '             </td>';
                       $TipoPagoSri.= '         </tr>';
                       $TipoPagoSri.= '         <tr height="30">';
                       $TipoPagoSri.= '             <td class="Form-Label">Estado</td>';
                       $TipoPagoSri.= '             <td class="Form-Label">';
                                                   $TipoPagoSri.= $this->CreaComboEstado();
                       $TipoPagoSri.= '             </td>';
                       $TipoPagoSri.= '         </tr>';
                       $TipoPagoSri.= '</table>';
                       return $TipoPagoSri;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('80px','left','');
                        $Columns['Descripci&oacute;n'] = array('100px','left','');
                        $Columns['Tipo'] = array('80px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        $Columns['idTipo']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }
               
              function CreaTipoPagoSriSearchGrid($Datos)
              {                               
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoPagoSriGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function MuestraTipoPagoSriGrid($Ajax,$Datos)
              {                              
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoPagoSriGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }        

              private function TipoPagoSriGridHTML($ObjSchGrid,$Datos)
              {  
                  if(count($Datos)>0){                      
                     foreach ($Datos as $ivaporcentaje)
                      {        $fecreacion = $this->Fechas->changeFormatDate($ivaporcentaje['fecreacion'],"DMY");
                               $id = str_pad($ivaporcentaje['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ivaporcentaje['codigo']));                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ivaporcentaje['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ivaporcentaje['tipo']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ivaporcentaje['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ivaporcentaje['idestado']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ivaporcentaje['idcajatipo']);                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($ivaporcentaje['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              function TraeDatosTipos()
              {        
                       $ServicioTipo= new ServicioTipo();
                       $datosTipos = $ServicioTipo->BuscarTipoCombo();
                       return $datosTipos;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              private function CreaComboTipo()
              {       
                       $datosTipos= $this->TraeDatosTipos();
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("tipo",$datosTipos)->Selected(1)->Create("s04");
              }
              
              function MuestraTipoPagoSri($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion'])); 
                            $Ajax->Assign("tipo".$this->Sufijo,"value", trim($Datos[0]['idcajatipo']));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaTipoPagoSri($Ajax,"NOEXISTEID","No existe un Tipo de Pago con ese ID.");
                       }
              }
              
              function MuestraTipoPagoSriGuardado($Ajax,$TipoPagoSri)
              {       if (count($TipoPagoSri)>0)
                        {   $id = str_pad($TipoPagoSri[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraTipoPagoSriGrid($Ajax,$TipoPagoSri);
                            return $this->RespuestaTipoPagoSri($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaTipoPagoSri($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraTipoPagoSriEditado($Ajax,$TipoPagoSri)
              {       foreach ($TipoPagoSri as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoPagoSriRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoPagoSri($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaTipoPagoSri($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraTipoPagoSriEliminado($Ajax,$TipoPagoSri)
              {        foreach ($TipoPagoSri as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoPagoSriRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoPagoSri($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaTipoPagoSri($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraTipoPagoSriRowGrid($Ajax,$TipoPagoSri,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($TipoPagoSri['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($TipoPagoSri['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($TipoPagoSri['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($TipoPagoSri['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($TipoPagoSri['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($TipoPagoSri['tipo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($TipoPagoSri['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($TipoPagoSri['idestado'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($TipoPagoSri['idcajatipo']));
                        return $Ajax;
                }

              function MuestraTipoPagoSriExcepcion($Ajax,$Msg)
                {       return $this->RespuestaTipoPagoSri($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaTipoPagoSri($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

