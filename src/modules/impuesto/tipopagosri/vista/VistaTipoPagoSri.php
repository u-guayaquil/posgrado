<?php
         $Sufijo = '_TipoPagoSri';
         
         require_once('src/modules/impuesto/tipopagosri/controlador/ControlTipoPagoSri.php');
         $ControlTipoPagoSri = new ControlTipoPagoSri($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlTipoPagoSri,'GuardaTipoPagoSri'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlTipoPagoSri,'MuestraTipoPagoSriByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlTipoPagoSri,'MuestraTipoPagoSriByTX'));
         $xajax->register(XAJAX_FUNCTION,array('PresentGridData'.$Sufijo, $ControlTipoPagoSri,'PresentaGridData'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlTipoPagoSri,'EliminaTipoPagoSri'));
         
         $xajax->processRequest();

?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript"> 
                 function ButtonClick_TipoPagoSri(Sufijo,Operacion)
                 {        var objTipoPagoSri = "id:descripcion:estado:codigo:tipo";
                          var frmTipoPagoSri = "descripcion:estado:codigo:tipo";
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,objTipoPagoSri,"id:estado");
                              ElementClear(Sufijo,objTipoPagoSri);
                              ElementSetValue(Sufijo,"estado",1);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objTipoPagoSri,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_TipoPagoSri(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"descripcion"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {    var Forma = PrepareElements(Sufijo,objTipoPagoSri);
                                        xajax_Guarda_TipoPagoSri(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmTipoPagoSri);
                               ElementClear(Sufijo,objTipoPagoSri);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_TipoPagoSri(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_TipoPagoSri(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_TipoPagoSri(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_TipoPagoSri(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")) {
                                BarButtonState(Sufijo,"Active");

                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                                document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                                document.getElementById("descripcion"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;        
                                document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                                document.getElementById("tipo"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                          }
                          return false;
                 }
                 
                function XAJAXResponse_TipoPagoSri(Sufijo,Mensaje,Datos)
                {       
                        var objTipoPagoSri = "descripcion:estado:codigo:tipo";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objTipoPagoSri);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                }                 
         </script>
         </head>
<body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlTipoPagoSri->CargaTipoPagoSriBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlTipoPagoSri->CargaTipoPagoSriMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlTipoPagoSri->CargaTipoPagoSriSearchGrid();  ?>
              </div>  
         </div> 
       </body>
</html>  
              

