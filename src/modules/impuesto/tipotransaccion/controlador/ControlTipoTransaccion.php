<?php
         require_once("src/rules/impuesto/servicio/ServicioTipoTransaccion.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/impuesto/tipotransaccion/render/RenderTipoTransaccion.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlTipoTransaccion
         {     private  $Sufijo; 
               private  $ServicioTipoTransaccion;
               private  $RenderTipoTransaccion;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioTipoTransaccion = new ServicioTipoTransaccion();
                        $this->RenderTipoTransaccion = new RenderTipoTransaccion($Sufijo);
               }

               function CargaTipoTransaccionBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaTipoTransaccionMantenimiento()
               {        echo $this->RenderTipoTransaccion->CreaTipoTransaccionMantenimiento();
               }

               function CargaTipoTransaccionSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoTipoTransaccion = $this->ServicioTipoTransaccion->BuscarTipoTransaccionByDescripcion($prepareDQL);
                        echo $this->RenderTipoTransaccion->CreaTipoTransaccionSearchGrid($datoTipoTransaccion); 
               }         
               
               function MuestraTipoTransaccionByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoTipoTransaccion = $this->ServicioTipoTransaccion->BuscarTipoTransaccionByID($prepareDQL);
                        return $this->RenderTipoTransaccion->MuestraTipoTransaccion($ajaxRespon,$datoTipoTransaccion);
               }

               function MuestraTipoTransaccionByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoTipoTransaccion = $this->ServicioTipoTransaccion->BuscarTipoTransaccionByDescripcion($prepareDQL);
                        return $this->RenderTipoTransaccion->MuestraTipoTransaccionGrid($ajaxRespon,$datoTipoTransaccion);
               }

               function GuardaTipoTransaccion($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $TipoTransaccion  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioTipoTransaccion->GuardaDBTipoTransaccion($TipoTransaccion);
                        if (is_numeric($id)){
                            $function = (empty($TipoTransaccion->id) ? "MuestraTipoTransaccionGuardado" : "MuestraTipoTransaccionEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioTipoTransaccion->BuscarTipoTransaccionByID($prepareDQL);
                            return $this->RenderTipoTransaccion->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderTipoTransaccion->MuestraTipoTransaccionExcepcion($ajaxRespon,intval($id));
                        }
               } 
               
               function EliminaTipoTransaccion($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioTipoTransaccion->DesactivaTipoTransaccion(intval($id));
                        $Datos = $this->ServicioTipoTransaccion->BuscarTipoTransaccionByDescripcion($prepareDQL);
                        return $this->RenderTipoTransaccion->MuestraTipoTransaccionEliminado($ajaxRespon,$Datos);
                }
               
         }

?>

