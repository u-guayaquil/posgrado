<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderTipoTransaccion
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2;                       
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaTipoTransaccionMantenimiento()
              {                          
                       $TipoTransaccion = '<table class="Form-Frame" cellpadding="0">';
                       $TipoTransaccion.= '       <tr height="30">';
                       $TipoTransaccion.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $TipoTransaccion.= '             <td class="Form-Label" style="width:75%">';
                       $TipoTransaccion.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $TipoTransaccion.= '             </td>';
                       $TipoTransaccion.= '         </tr>';
                       $TipoTransaccion.= '         <tr height="30">';
                       $TipoTransaccion.= '             <td class="Form-Label" style="width:25%">C&oacute;digo</td>';
                       $TipoTransaccion.= '             <td class="Form-Label" style="width:75%">';
                       $TipoTransaccion.= '                 <input type="text" class="txt-upper t07" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $TipoTransaccion.= '             </td>';
                       $TipoTransaccion.= '         </tr>';
                       $TipoTransaccion.= '         <tr height="30">';
                       $TipoTransaccion.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $TipoTransaccion.= '             <td class="Form-Label" style="width:75%">';
                       $TipoTransaccion.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="100" disabled/>';
                       $TipoTransaccion.= '             </td>';
                       $TipoTransaccion.= '         </tr>';
                       $TipoTransaccion.= '         </tr>';                       
                       $TipoTransaccion.= '         <tr height="30">';
                       $TipoTransaccion.= '             <td class="Form-Label">Estado</td>';
                       $TipoTransaccion.= '             <td class="Form-Label">';
                                                   $TipoTransaccion.= $this->CreaComboEstado();
                       $TipoTransaccion.= '             </td>';
                       $TipoTransaccion.= '         </tr>';
                       $TipoTransaccion.= '</table>';
                       return $TipoTransaccion;
              }

              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('80px','left','');
                        $Columns['Descripci&oacute;n'] = array('100px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }
              
              function CreaTipoTransaccionSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoTransaccionGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function TipoTransaccionGridHTML($ObjSchGrid,$Datos)
              {       
                  if(count($Datos)>0){
                     foreach ($Datos as $atributo)
                      {        $fecreacion = $this->Fechas->changeFormatDate($atributo['fecreacion'],"DMY");
                               $id = str_pad($atributo['id'],2,'0',STR_PAD_LEFT);
                               $codigo = str_pad($atributo['codigo'],10,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($codigo));                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($atributo['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$atributo['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$atributo['idestado']); 
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($atributo['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraTipoTransaccion($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo'])); 
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaTipoTransaccion($Ajax,"NOEXISTEID","No existe un Tipo con ese ID.");
                       }
              }
              function MuestraTipoTransaccionGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->TipoTransaccionGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }        

              function MuestraTipoTransaccionGuardado($Ajax,$TipoTransaccion)
              {       if (count($TipoTransaccion)>0)
                        {   $id = str_pad($TipoTransaccion[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraTipoTransaccionGrid($Ajax,$TipoTransaccion);
                            return $this->RespuestaTipoTransaccion($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaTipoTransaccion($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraTipoTransaccionEditado($Ajax,$TipoTransaccion)
              {       foreach ($TipoTransaccion as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoTransaccionRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoTransaccion($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaTipoTransaccion($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraTipoTransaccionEliminado($Ajax,$TipoTransaccion)
              {        foreach ($TipoTransaccion as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraTipoTransaccionRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaTipoTransaccion($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaTipoTransaccion($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraTipoTransaccionRowGrid($Ajax,$TipoTransaccion,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($TipoTransaccion['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($TipoTransaccion['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($TipoTransaccion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($TipoTransaccion['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($TipoTransaccion['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($TipoTransaccion['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($TipoTransaccion['idestado'])); 
                        return $Ajax;
                }

              function MuestraTipoTransaccionExcepcion($Ajax,$Msg)
                {       return $this->RespuestaTipoTransaccion($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaTipoTransaccion($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

