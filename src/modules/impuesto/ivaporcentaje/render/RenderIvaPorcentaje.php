<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/impuesto/servicio/ServicioIvaCategoria.php");                
        require_once("src/rules/general/entidad/Estado.php"); 
        require_once("src/rules/impuesto/entidad/IvaCategoria.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderIvaPorcentaje
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2;
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
              
              function CreaIvaPorcentajeMantenimiento()
              {                               
                       $IvaPorcentaje = '<table class="Form-Frame" cellpadding="0">';
                       $IvaPorcentaje.= '       <tr height="30">';
                       $IvaPorcentaje.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $IvaPorcentaje.= '             <td class="Form-Label" style="width:75%">';
                       $IvaPorcentaje.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:valor:codigo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $IvaPorcentaje.= '             </td>';
                       $IvaPorcentaje.= '         </tr>';
                       $IvaPorcentaje.= '         <tr height="30">';
                       $IvaPorcentaje.= '             <td class="Form-Label" style="width:25%">C&oacute;digo</td>';
                       $IvaPorcentaje.= '             <td class="Form-Label" style="width:75%">';
                       $IvaPorcentaje.= '                 <input type="text" class="txt-upper t07" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $IvaPorcentaje.= '             </td>';
                       $IvaPorcentaje.= '         </tr>';
                       $IvaPorcentaje.= '         <tr height="30">';
                       $IvaPorcentaje.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $IvaPorcentaje.= '             <td class="Form-Label" style="width:75%">';
                       $IvaPorcentaje.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $IvaPorcentaje.= '             </td>';
                       $IvaPorcentaje.= '         </tr>';
                       $IvaPorcentaje.= '         <tr height="30">';
                       $IvaPorcentaje.= '             <td class="Form-Label" style="width:25%">Valor</td>';
                       $IvaPorcentaje.= '             <td class="Form-Label" style="width:75%">';
                       $IvaPorcentaje.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t07" id="valor'.$this->Sufijo.'" name="valor'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $IvaPorcentaje.= '             </td>';                       
                       $IvaPorcentaje.= '         </tr>'; 
                       $IvaPorcentaje.= '         <tr height="30">';
                       $IvaPorcentaje.= '             <td class="Form-Label">Categoria</td>';
                       $IvaPorcentaje.= '             <td class="Form-Label">';
                                                   $IvaPorcentaje.= $this->CreaComboCategoria();
                       $IvaPorcentaje.= '             </td>';
                       $IvaPorcentaje.= '         </tr>';
                       $IvaPorcentaje.= '         <tr height="30">';
                       $IvaPorcentaje.= '             <td class="Form-Label">Estado</td>';
                       $IvaPorcentaje.= '             <td class="Form-Label">';
                                                   $IvaPorcentaje.= $this->CreaComboEstado();
                       $IvaPorcentaje.= '             </td>';
                       $IvaPorcentaje.= '         </tr>';
                       $IvaPorcentaje.= '</table>';
                       return $IvaPorcentaje;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('80px','left','');
                        $Columns['Descripci&oacute;n'] = array('100px','left','');
                        $Columns['Valor'] = array('70px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        $Columns['idCategoria']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }
               
              function CreaIvaPorcentajeSearchGrid($Datos)
              {                               
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->IvaPorcentajeGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function MuestraIvaPorcentajeGrid($Ajax,$Datos)
              {                              
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->IvaPorcentajeGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }        

              private function IvaPorcentajeGridHTML($ObjSchGrid,$Datos)
              {  
                  if(count($Datos)>0){                      
                     foreach ($Datos as $ivaporcentaje)
                      {        $fecreacion = $this->Fechas->changeFormatDate($ivaporcentaje['fecreacion'],"DMY");
                               $id = str_pad($ivaporcentaje['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ivaporcentaje['codigo']));                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ivaporcentaje['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($ivaporcentaje['valor']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ivaporcentaje['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ivaporcentaje['idestado']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$ivaporcentaje['idcativa']); 
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($ivaporcentaje['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              function TraeDatosCategorias()
              {        
                       $ServicioCategorias = new ServicioIvaCategoria();
                       $prepareDQL = array('limite' => 50);
                       $datoCategoria = $ServicioCategorias->BuscarIvaCategoriaCombo($prepareDQL);
                       return $datoCategoria;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              private function CreaComboCategoria()
              {       
                       $datosCategoria= $this->TraeDatosCategorias();
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("categoria",$datosCategoria)->Selected(1)->Create("s04");
              }
              
              function MuestraIvaPorcentaje($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                            $Ajax->Assign("valor".$this->Sufijo,"value", trim($Datos[0]['valor']));
                            $Ajax->Assign("categoria".$this->Sufijo,"value", trim($Datos[0]['idcativa']));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaIvaPorcentaje($Ajax,"NOEXISTEID","No existe un Codigo con ese ID.");
                       }
              }
              
              function MuestraIvaPorcentajeGuardado($Ajax,$IvaPorcentaje)
              {       if (count($IvaPorcentaje)>0)
                        {   $id = str_pad($IvaPorcentaje[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraIvaPorcentajeGrid($Ajax,$IvaPorcentaje);
                            return $this->RespuestaIvaPorcentaje($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaIvaPorcentaje($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraIvaPorcentajeEditado($Ajax,$IvaPorcentaje)
              {       foreach ($IvaPorcentaje as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraIvaPorcentajeRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaIvaPorcentaje($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaIvaPorcentaje($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraIvaPorcentajeEliminado($Ajax,$IvaPorcentaje)
              {        foreach ($IvaPorcentaje as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraIvaPorcentajeRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaIvaPorcentaje($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaIvaPorcentaje($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraIvaPorcentajeRowGrid($Ajax,$IvaPorcentaje,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($IvaPorcentaje['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($IvaPorcentaje['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($IvaPorcentaje['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($IvaPorcentaje['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($IvaPorcentaje['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($IvaPorcentaje['valor']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($IvaPorcentaje['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($IvaPorcentaje['idestado'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($IvaPorcentaje['idcativa']));
                        return $Ajax;
                }

              function MuestraIvaPorcentajeExcepcion($Ajax,$Msg)
                {       return $this->RespuestaIvaPorcentaje($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaIvaPorcentaje($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

