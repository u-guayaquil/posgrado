<?php
         $Sufijo = '_IvaPorcentaje';
         
         require_once('src/modules/impuesto/ivaporcentaje/controlador/ControlIvaPorcentaje.php');
         $ControlIvaPorcentaje = new ControlIvaPorcentaje($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlIvaPorcentaje,'GuardaIvaPorcentaje'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlIvaPorcentaje,'MuestraIvaPorcentajeByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlIvaPorcentaje,'MuestraIvaPorcentajeByTX'));
         $xajax->register(XAJAX_FUNCTION,array('PresentGridData'.$Sufijo, $ControlIvaPorcentaje,'PresentaGridData'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlIvaPorcentaje,'EliminaIvaPorcentaje'));
         
         $xajax->processRequest();

?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript"> 
                 function ButtonClick_IvaPorcentaje(Sufijo,Operacion)
                 {        var objIvaPorcentaje = "id:descripcion:estado:codigo:valor:categoria";
                          var frmIvaPorcentaje = "descripcion:estado:codigo:valor:categoria";
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,objIvaPorcentaje,"id:estado");
                              ElementClear(Sufijo,objIvaPorcentaje);
                              ElementSetValue(Sufijo,"estado",1);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objIvaPorcentaje,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_IvaPorcentaje(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,frmIvaPorcentaje))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {    var Forma = PrepareElements(Sufijo,objIvaPorcentaje);
                                        xajax_Guarda_IvaPorcentaje(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmIvaPorcentaje);
                               ElementClear(Sufijo,objIvaPorcentaje);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_IvaPorcentaje(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_IvaPorcentaje(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_IvaPorcentaje(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_IvaPorcentaje(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")) {
                                BarButtonState(Sufijo,"Active");

                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                                document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                                document.getElementById("descripcion"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;        
                                document.getElementById("valor"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                                document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                                document.getElementById("categoria"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                          }
                          return false;
                 }
                 
                function XAJAXResponse_IvaPorcentaje(Sufijo,Mensaje,Datos)
                {       
                        var objIvaPorcentaje = "descripcion:estado:codigo:valor:categoria";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objIvaPorcentaje);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                }                 
         </script>
         </head>
<body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlIvaPorcentaje->CargaIvaPorcentajeBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlIvaPorcentaje->CargaIvaPorcentajeMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlIvaPorcentaje->CargaIvaPorcentajeSearchGrid();  ?>
              </div>  
         </div> 
       </body>
</html>  
              

