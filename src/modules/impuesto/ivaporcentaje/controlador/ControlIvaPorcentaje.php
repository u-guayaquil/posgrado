<?php
         require_once("src/rules/impuesto/servicio/ServicioIvaPorcentaje.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/impuesto/ivaporcentaje/render/RenderIvaPorcentaje.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlIvaPorcentaje
         {     private  $Sufijo;
               private  $ServicioIvaPorcentaje;
               private  $RenderIvaPorcentaje;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioIvaPorcentaje = new ServicioIvaPorcentaje();
                        $this->RenderIvaPorcentaje = new RenderIvaPorcentaje($Sufijo);
               }
               
               function CargaIvaPorcentajeBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
               function CargaIvaPorcentajeMantenimiento()
               {        echo $this->RenderIvaPorcentaje->CreaIvaPorcentajeMantenimiento();
               }

               function CargaIvaPorcentajeSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoIvaPorcentaje = $this->ServicioIvaPorcentaje->BuscarIvaPorcentajeByDescripcion($prepareDQL);
                        echo $this->RenderIvaPorcentaje->CreaIvaPorcentajeSearchGrid(($datoIvaPorcentaje)); 
               }         
               
               function MuestraIvaPorcentajeByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoIvaPorcentaje = $this->ServicioIvaPorcentaje->BuscarIvaPorcentajeByID($prepareDQL);
                        return $this->RenderIvaPorcentaje->MuestraIvaPorcentaje($ajaxRespon,$datoIvaPorcentaje);
               }

               function MuestraIvaPorcentajeByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoIvaPorcentaje = $this->ServicioIvaPorcentaje->BuscarIvaPorcentajeByDescripcion($prepareDQL);
                        return $this->RenderIvaPorcentaje->MuestraIvaPorcentajeGrid($ajaxRespon,$datoIvaPorcentaje);
               }

               function GuardaIvaPorcentaje($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $IvaPorcentaje  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioIvaPorcentaje->GuardaDBIvaPorcentaje($IvaPorcentaje);
                        if (is_numeric($id)){
                            $function = (empty($IvaPorcentaje->id) ? "MuestraIvaPorcentajeGuardado" : "MuestraIvaPorcentajeEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioIvaPorcentaje->BuscarIvaPorcentajeByID($prepareDQL);
                            return $this->RenderIvaPorcentaje->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderIvaPorcentaje->MuestraIvaPorcentajeExcepcion($ajaxRespon,intval($id));
                        }                      
               }
               
               function EliminaIvaPorcentaje($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioIvaPorcentaje->DesactivaIvaPorcentaje(intval($id));
                        $Datos = $this->ServicioIvaPorcentaje->BuscarIvaPorcentajeByDescripcion($prepareDQL);
                        return $this->RenderIvaPorcentaje->MuestraIvaPorcentajeEliminado($ajaxRespon,$Datos);
                }
               
               
               
         }

?>

