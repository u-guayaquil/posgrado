<?php
         require_once("src/rules/impuesto/servicio/ServicioCreditoTributario.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/impuesto/creditotributario/render/RenderCreditoTributario.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlCreditoTributario
         {     private  $Sufijo;
               private  $ServicioCreditoTributario;
               private  $RenderCreditoTributario;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioCreditoTributario = new ServicioCreditoTributario();
                        $this->RenderCreditoTributario = new RenderCreditoTributario($Sufijo);
               }

               function CargaCreditoTributarioBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaCreditoTributarioMantenimiento()
               {        echo $this->RenderCreditoTributario->CreaCreditoTributarioMantenimiento();
               }

               function CargaCreditoTributarioSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoCreditoTributario = $this->ServicioCreditoTributario->BuscarCreditoTributarioByDescripcion($prepareDQL);
                        echo $this->RenderCreditoTributario->CreaCreditoTributarioSearchGrid($datoCreditoTributario); 
               }         
               
               function MuestraCreditoTributarioByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoCreditoTributario = $this->ServicioCreditoTributario->BuscarCreditoTributarioByID($prepareDQL);
                        return $this->RenderCreditoTributario->MuestraCreditoTributario($ajaxRespon,$datoCreditoTributario);
               }

               function MuestraCreditoTributarioByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoCreditoTributario = $this->ServicioCreditoTributario->BuscarCreditoTributarioByDescripcion($prepareDQL);
                        return $this->RenderCreditoTributario->MuestraCreditoTributarioGrid($ajaxRespon,$datoCreditoTributario);
               }

               function GuardaCreditoTributario($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $CreditoTributario  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioCreditoTributario->GuardaDBCreditoTributario($CreditoTributario);
                        if (is_numeric($id)){
                            $function = (empty($CreditoTributario->id) ? "MuestraCreditoTributarioGuardado" : "MuestraCreditoTributarioEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioCreditoTributario->BuscarCreditoTributarioByID($prepareDQL);
                            return $this->RenderCreditoTributario->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderCreditoTributario->MuestraCreditoTributarioExcepcion($ajaxRespon,intval($id));
                        }
               } 
               
               function EliminaCreditoTributario($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioCreditoTributario->DesactivaCreditoTributario(intval($id));
                        $Datos = $this->ServicioCreditoTributario->BuscarCreditoTributarioByDescripcion($prepareDQL);
                        return $this->RenderCreditoTributario->MuestraCreditoTributarioEliminado($ajaxRespon,$Datos);
                }
               
         }

?>

