<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");
         
        class RenderCreditoTributario
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
                            
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;    
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2;                      
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
              
              function CreaCreditoTributarioMantenimiento()
              {                          
                       $CreditoTributario = '<table class="Form-Frame" cellpadding="0">';
                       $CreditoTributario.= '       <tr height="30">';
                       $CreditoTributario.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $CreditoTributario.= '             <td class="Form-Label" style="width:75%">';
                       $CreditoTributario.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $CreditoTributario.= '             </td>';
                       $CreditoTributario.= '         </tr>';
                       $CreditoTributario.= '         <tr height="30">';
                       $CreditoTributario.= '             <td class="Form-Label" style="width:25%">C&oacute;digo</td>';
                       $CreditoTributario.= '             <td class="Form-Label" style="width:75%">';
                       $CreditoTributario.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t07" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $CreditoTributario.= '             </td>';
                       $CreditoTributario.= '         </tr>';
                       $CreditoTributario.= '         <tr height="30">';
                       $CreditoTributario.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $CreditoTributario.= '             <td class="Form-Label" style="width:75%">';
                       $CreditoTributario.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $CreditoTributario.= '             </td>';
                       $CreditoTributario.= '         </tr>';                      
                       $CreditoTributario.= '         <tr height="30">';
                       $CreditoTributario.= '             <td class="Form-Label">Estado</td>';
                       $CreditoTributario.= '             <td class="Form-Label">';
                                                   $CreditoTributario.= $this->CreaComboEstado();
                       $CreditoTributario.= '             </td>';
                       $CreditoTributario.= '         </tr>';
                       $CreditoTributario.= '</table>';
                       return $CreditoTributario;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('80px','left','');
                        $Columns['Descripci&oacute;n'] = array('200px','left','');
                        $Columns['Creacion']   = array('150px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaCreditoTributarioSearchGrid($Datos)
              {                              
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->CreditoTributarioGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function CreditoTributarioGridHTML($ObjSchGrid,$Datos)
              {       
                  if(count($Datos)>0){
                     foreach ($Datos as $credito)
                      {        $fecreacion = $this->Fechas->changeFormatDate($credito['fecreacion'],"DMY");
                               $id = str_pad($credito['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($credito['codigo']));                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($credito['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$credito['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$credito['idestado']); 
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($credito['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstadoByArray($IdArray)
              {       
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraCreditoTributario($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo'])); 
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaCreditoTributario($Ajax,"NOEXISTEID","No existe un Codigo con ese ID.");
                       }
              }
              
              function MuestraCreditoTributarioGrid($Ajax,$Datos)
              {        
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->CreditoTributarioGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }        

              function MuestraCreditoTributarioGuardado($Ajax,$CreditoTributario)
              {       if (count($CreditoTributario)>0)
                        {   $id = str_pad($CreditoTributario[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraCreditoTributarioGrid($Ajax,$CreditoTributario);
                            return $this->RespuestaCreditoTributario($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaCreditoTributario($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraCreditoTributarioEditado($Ajax,$CreditoTributario)
              {       foreach ($CreditoTributario as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraCreditoTributarioRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaCreditoTributario($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaCreditoTributario($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraCreditoTributarioEliminado($Ajax,$CreditoTributario)
              {        foreach ($CreditoTributario as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraCreditoTributarioRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaCreditoTributario($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaCreditoTributario($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraCreditoTributarioRowGrid($Ajax,$CreditoTributario,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($CreditoTributario['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($CreditoTributario['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($CreditoTributario['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($CreditoTributario['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($CreditoTributario['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($CreditoTributario['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($CreditoTributario['idestado'])); 
                        return $Ajax;
                }

              function MuestraCreditoTributarioExcepcion($Ajax,$Msg)
                {       return $this->RespuestaCreditoTributario($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaCreditoTributario($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

