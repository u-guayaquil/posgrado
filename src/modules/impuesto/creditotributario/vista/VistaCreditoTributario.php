<?php
         $Sufijo = '_CreTributario';
         
         require_once('src/modules/impuesto/creditotributario/controlador/ControlCreditoTributario.php');
         $ControlCreditoTributario = new ControlCreditoTributario($Sufijo);
         
         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlCreditoTributario,'GuardaCreditoTributario'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlCreditoTributario,'MuestraCreditoTributarioByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlCreditoTributario,'MuestraCreditoTributarioByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlCreditoTributario,'EliminaCreditoTributario'));
         
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript"> 
                 function ButtonClick_CreTributario(Sufijo,Operacion)
                 {        var objCreTributario = "id:descripcion:estado:codigo";
                          var frmCreTributario = "descripcion:estado:codigo";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              ElementStatus(Sufijo,objCreTributario,"id:estado");
                              ElementClear(Sufijo,objCreTributario);
                              ElementSetValue(Sufijo,"estado",1);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objCreTributario,"id");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {     if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_CreTributario(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"descripcion"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = PrepareElements(Sufijo,objCreTributario);
                                        xajax_Guarda_CreTributario(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmCreTributario);
                               ElementClear(Sufijo,objCreTributario);
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_CreTributario(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_CreTributario(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_CreTributario(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_CreTributario(Sufijo,Grilla)
                 {        if (IsDisabled(Sufijo,"addSav")) {
                            BarButtonState(Sufijo,"Active");

                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                            document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("descripcion"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;        
                            document.getElementById("estado"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue; 
                          }
                          return false;
                 }
                 
                 function XAJAXResponse_CreTributario(Sufijo,Mensaje,Datos)
                 {       
                        var objCreTributario = "descripcion:estado:codigo";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objCreTributario);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
<body>
         <div class="FormBasic" style="width:585px">
               <div class="FormSectionMenu">              
              <?php  $ControlCreditoTributario->CargaCreditoTributarioBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '    BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlCreditoTributario->CargaCreditoTributarioMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlCreditoTributario->CargaCreditoTributarioSearchGrid();  ?>
              </div>  
         </div> 
    </body>
</html>     
              

