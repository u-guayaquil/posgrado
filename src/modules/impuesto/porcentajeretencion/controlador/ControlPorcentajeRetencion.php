<?php
         require_once("src/rules/impuesto/servicio/ServicioPorcentajeRetencion.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
         require_once("src/modules/impuesto/porcentajeretencion/render/RenderPorcentajeRetencion.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlPorcentajeRetencion
         {     private  $Sufijo;
               private  $ServicioPorcentajeRetencion;
               private  $RenderPorcentajeRetencion;
               private  $ServicioPlanCuentas;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioPorcentajeRetencion = new ServicioPorcentajeRetencion();
                        $this->RenderPorcentajeRetencion = new RenderPorcentajeRetencion($Sufijo);
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
               }
               
               function CargaPorcentajeRetencionBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
               function CargaPorcentajeRetencionMantenimiento()
               {        echo $this->RenderPorcentajeRetencion->CreaPorcentajeRetencionMantenimiento();
               }

               function CargaPorcentajeRetencionSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoPorcentajeRetencion = $this->ServicioPorcentajeRetencion->BuscarPorcentajeRetencionByDescripcion($prepareDQL);
                        echo $this->RenderPorcentajeRetencion->CreaPorcentajeRetencionSearchGrid(($datoPorcentajeRetencion)); 
               }         
               
               function MuestraPorcentajeRetencionByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoPorcentajeRetencion = $this->ServicioPorcentajeRetencion->BuscarPorcentajeRetencionByID($prepareDQL);
                        return $this->RenderPorcentajeRetencion->MuestraPorcentajeRetencion($ajaxRespon,$datoPorcentajeRetencion);
               }

               function MuestraPorcentajeRetencionByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoPorcentajeRetencion = $this->ServicioPorcentajeRetencion->BuscarPorcentajeRetencionByDescripcion($prepareDQL);
                        return $this->RenderPorcentajeRetencion->MuestraPorcentajeRetencionGrid($ajaxRespon,$datoPorcentajeRetencion);
               }

               function GuardaPorcentajeRetencion($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $PorcentajeRetencion  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioPorcentajeRetencion->GuardaDBPorcentajeRetencion($PorcentajeRetencion);
                        if (is_numeric($id)){
                            $function = (empty($PorcentajeRetencion->id) ? "MuestraPorcentajeRetencionGuardado" : "MuestraPorcentajeRetencionEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioPorcentajeRetencion->BuscarPorcentajeRetencionByID($prepareDQL);
                            return $this->RenderPorcentajeRetencion->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderPorcentajeRetencion->MuestraPorcentajeRetencionExcepcion($ajaxRespon,intval($id));
                        }                   
               }
               
               function EliminaPorcentajeRetencion($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioPorcentajeRetencion->DesactivaPorcentajeRetencion($id);
                        $Datos = $this->ServicioPorcentajeRetencion->BuscarPorcentajeRetencionByDescripcion($prepareDQL);
                        return $this->RenderPorcentajeRetencion->MuestraPorcentajeRetencionEliminado($ajaxRespon,$Datos);
                }
               
                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Cuentas";
                        $jsonModal['Carga'] = $this->RenderPorcentajeRetencion->CreaModalGridPorcentajeRetencion($Operacion,$Datos);
                        $jsonModal['Ancho'] = "517";
                           
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                                
                function ConsultaModalGridPorcentajeRetencionByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        $Datos = $this->ServicioPorcentajeRetencion->BuscarPorcentajeRetencionByDescripcion($prepareDQL);
                        return $this->RenderPorcentajeRetencion->MuestraModalGridPorcentajeRetencion($ajaxResp,$Operacion,$Datos);                                                    
                }
               
         }

?>

