<?php
         $Sufijo = '_PorcentajeRetencion';
         
         require_once('src/modules/impuesto/porcentajeretencion/controlador/ControlPorcentajeRetencion.php');
         $ControlPorcentajeRetencion = new ControlPorcentajeRetencion($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlPorcentajeRetencion,'GuardaPorcentajeRetencion'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlPorcentajeRetencion,'MuestraPorcentajeRetencionByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlPorcentajeRetencion,'MuestraPorcentajeRetencionByTX'));
         $xajax->register(XAJAX_FUNCTION,array('PresentGridData'.$Sufijo, $ControlPorcentajeRetencion,'PresentaGridData'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlPorcentajeRetencion,'EliminaPorcentajeRetencion'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlPorcentajeRetencion,'CargaModalGrid'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlPorcentajeRetencion,'ConsultaModalGridByTX'));
         
         $xajax->processRequest();

?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript"> 
                 function ButtonClick_PorcentajeRetencion(Sufijo,Operacion)
                 {        var objPorcentajeRetencion = "id:codigo:descripcion:valor:estado:iddeudora:idacreedora"
                                                      +":txdeudora:txacreedora:tipoimpuesto:btacreedora:btdeudora";
                          var frmPorcentajeRetencion = "codigo:descripcion:valor:estado:iddeudora:idacreedora"
                                                      +":txdeudora:txacreedora:tipoimpuesto";
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              BarButtonStateEnabled(Sufijo,"btacreedora:btdeudora");
                              ElementStatus(Sufijo,objPorcentajeRetencion,"id:estado");
                              ElementClear(Sufijo,objPorcentajeRetencion);
                              ElementSetValue(Sufijo,"estado",1);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objPorcentajeRetencion,"id");
                               BarButtonStateEnabled(Sufijo,"btacreedora:btdeudora");
                                if(ElementGetValue(Sufijo,"estado")==='0'){
                                    ElementStatus(Sufijo, "estado", "");
                                }else{
                                    ElementStatus(Sufijo, "", "estado");
                                }
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_PorcentajeRetencion(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"descripcion"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {    var Forma = PrepareElements(Sufijo,objPorcentajeRetencion);
                                        xajax_Guarda_PorcentajeRetencion(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmPorcentajeRetencion);
                               ElementClear(Sufijo,objPorcentajeRetencion);
                               BarButtonStateEnabled(Sufijo,"","btacreedora:btdeudora");
                          }
                          else if (Operacion=='addImp')
                          {
                          }else { 
                             xajax_CargaModal_PorcentajeRetencion(Operacion);  
                           }
                          return false;
                 }

                 function SearchByElement_PorcentajeRetencion(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          switch(Elemento.id){
                              case "id"+Sufijo:
                                  if (Elemento.value.length != 0)
                                    {      if (BarButtonState(Sufijo,"Inactive"))
                                            xajax_MuestraByID_PorcentajeRetencion(Elemento.value);                                  
                                    }
                                    else
                                    BarButtonState(Sufijo,"Default");
                              break;
                              case "FindDeudoraTextBx"+Sufijo:
                                  xajax_BuscaModalByTX_PorcentajeRetencion("btdeudora",Elemento.value);
                              break;
                              case 'FindTextBx'+Sufijo:
                                  xajax_MuestraByTX_PorcentajeRetencion(Elemento.value);
                              break;
                              case 'FindAcreedoraTextBx'+Sufijo:
                                  xajax_BuscaModalByTX_PorcentajeRetencion("btacreedora",Elemento.value);
                              break;
                              
                          }               
                 }
                 
                function SearchGetData_PorcentajeRetencion(Sufijo,Grilla)
                {     if (IsDisabled(Sufijo,"addSav"))
                        {   BarButtonState(Sufijo,"Active");
                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                            document.getElementById("codigo"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                            document.getElementById("descripcion"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;        
                            document.getElementById("valor"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                            document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                            document.getElementById("iddeudora"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                            document.getElementById("idacreedora"+Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;                          
                            document.getElementById("txacreedora"+Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue;
                            document.getElementById("txdeudora"+Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
                            document.getElementById("tipoimpuesto"+Sufijo).value = Grilla.cells[11].childNodes[0].nodeValue;
                        }
                        return false;
                }

                function SearchDeudoraGetData_PorcentajeRetencion(Sufijo,DatosGrid)
                {       document.getElementById("iddeudora"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txdeudora"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }
                
                function SearchAcreedoraGetData_PorcentajeRetencion(Sufijo,DatosGrid)
                {       document.getElementById("idacreedora"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                        document.getElementById("txacreedora"+Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
                        cerrar();
                        return false;
                }
                 
                function XAJAXResponse_PorcentajeRetencion(Sufijo,Mensaje,Datos)
                {       
                        var objPorcentajeRetencion = "codigo:descripcion:valor:estado:iddeudora:idacreedora"
                                                      +":txdeudora:txacreedora:tipoimpuesto:btacreedora:btdeudora";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objPorcentajeRetencion);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                }                 
         </script>
         </head>
<body>
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlPorcentajeRetencion->CargaPorcentajeRetencionBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlPorcentajeRetencion->CargaPorcentajeRetencionMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlPorcentajeRetencion->CargaPorcentajeRetencionSearchGrid();  ?>
              </div>  
         </div> 
       </body>
</html>  
              

