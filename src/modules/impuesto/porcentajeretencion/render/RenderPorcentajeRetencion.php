<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/impuesto/servicio/ServicioPorcentajeRetencion.php");  
        require_once("src/rules/impuesto/servicio/ServicioTipoImpuesto.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/DateControl.php");
         
        class RenderPorcentajeRetencion
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
              
              function CreaPorcentajeRetencionMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);                      
                       $PorcentajeRetencion = '<table class="Form-Frame" cellpadding="0">';
                       $PorcentajeRetencion.= '       <tr height="30">';
                       $PorcentajeRetencion.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $PorcentajeRetencion.= '             <td class="Form-Label" style="width:75%" colspan="3">';
                       $PorcentajeRetencion.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'codigo:descripcion:valor:estado:iddeudora:idacreedora:deudora:acreedora:tipoimpuesto\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $PorcentajeRetencion.= '             </td>';
                       $PorcentajeRetencion.= '         </tr>';
                       $PorcentajeRetencion.= '         <tr height="30">';
                       $PorcentajeRetencion.= '             <td class="Form-Label" style="width:25%">C&oacute;digo</td>';
                       $PorcentajeRetencion.= '             <td class="Form-Label" style="width:75%" colspan="3">';
                       $PorcentajeRetencion.= '                 <input type="text" class="txt-upper t04" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $PorcentajeRetencion.= '             </td>';
                       $PorcentajeRetencion.= '         </tr>';
                       $PorcentajeRetencion.= '         <tr height="30">';
                       $PorcentajeRetencion.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $PorcentajeRetencion.= '             <td class="Form-Label" style="width:75%" colspan="3">';
                       $PorcentajeRetencion.= '                 <input type="text" class="txt-upper t07" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $PorcentajeRetencion.= '             </td>';
                       $PorcentajeRetencion.= '         </tr>';
                       $PorcentajeRetencion.= '         <tr height="30">';
                       $PorcentajeRetencion.= '             <td class="Form-Label" style="width:25%">Valor</td>';
                       $PorcentajeRetencion.= '             <td class="Form-Label" style="width:25%">';
                       $PorcentajeRetencion.= '                 <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-upper t04" id="valor'.$this->Sufijo.'" name="valor'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $PorcentajeRetencion.= '             </td>';
                       $PorcentajeRetencion.= '             <td class="Form-Label">Tipo</td>';
                       $PorcentajeRetencion.= '             <td class="Form-Label">';
                                                   $PorcentajeRetencion.= $this->CreaComboTipoImpuesto();
                       $PorcentajeRetencion.= '             </td>';                       
                       $PorcentajeRetencion.= '         </tr>'; 
                       $PorcentajeRetencion.= '       <tr height="28">';
                       $PorcentajeRetencion.= '           <td class="Form-Label">Acreedora</td>';
                       $PorcentajeRetencion.= '           <td class="Form-Label" colspan="3">';
                                      $PorcentajeRetencion.= $Search->TextSch("acreedora","","")->Enabled(false)->Create("t10");
                       $PorcentajeRetencion.= '           </td>';
                       $PorcentajeRetencion.= '       </tr>';
                       $PorcentajeRetencion.= '       <tr height="28">';
                       $PorcentajeRetencion.= '           <td class="Form-Label">Deudora</td>';
                       $PorcentajeRetencion.= '           <td class="Form-Label" colspan="3">';
                                      $PorcentajeRetencion.= $Search->TextSch("deudora","","")->Enabled(false)->Create("t10");
                       $PorcentajeRetencion.= '           </td>';
                       $PorcentajeRetencion.= '       </tr>';
                       $PorcentajeRetencion.= '         <tr height="30">';
                       $PorcentajeRetencion.= '             <td class="Form-Label">Estado</td>';
                       $PorcentajeRetencion.= '             <td class="Form-Label">';
                                                   $PorcentajeRetencion.= $this->CreaComboEstado();
                       $PorcentajeRetencion.= '             </td>';
                       $PorcentajeRetencion.= '         </tr>';
                       $PorcentajeRetencion.= '</table>';
                       return $PorcentajeRetencion;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('80px','left','');
                        $Columns['Descripci&oacute;n'] = array('100px','left','');
                        $Columns['Valor'] = array('70px','left','');
                        $Columns['Creacion']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        $Columns['idDeudora']   = array('0px','left','none');
                        $Columns['idAcreedora']   = array('0px','left','none');
                        $Columns['Deudora']   = array('0px','left','none');
                        $Columns['Acreedora']   = array('0px','left','none');
                        $Columns['TipoImpuesto']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }
               
              function CreaPorcentajeRetencionSearchGrid($Datos)
              {                               
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->PorcentajeRetencionGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function MuestraPorcentajeRetencionGrid($Ajax,$Datos)
              {                              
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->PorcentajeRetencionGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }        

              private function PorcentajeRetencionGridHTML($ObjSchGrid,$Datos)
              {  
                  if(count($Datos)>0){                      
                     foreach ($Datos as $porcentaretencion)
                      {        $fecreacion = $this->Fechas->changeFormatDate($porcentaretencion['fecreacion'],"DMY");
                               $id = str_pad($porcentaretencion['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($porcentaretencion['codigo']));                               
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($porcentaretencion['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($porcentaretencion['valor']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$porcentaretencion['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$porcentaretencion['idestado']);                                
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$porcentaretencion['idctadeudora']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$porcentaretencion['idctaacreedora']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$porcentaretencion['acreedora']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$porcentaretencion['deudora']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$porcentaretencion['idimpuesto']);
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($porcentaretencion['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              function TraeDatosTipoImpuesto()
              {        
                       $ServicioTipoImpuesto = new ServicioTipoImpuesto();
                       $datoTipoImpuesto = $ServicioTipoImpuesto->BuscarTipoImpuestoCombo(0);
                       return $datoTipoImpuesto;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              private function CreaComboTipoImpuesto()
              {       
                       $datosTipoImpuesto= $this->TraeDatosTipoImpuesto();
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("tipoimpuesto",$datosTipoImpuesto)->Selected(1)->Create("s04");
              }
              
              function MuestraPorcentajeRetencion($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo']));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                            $Ajax->Assign("valor".$this->Sufijo,"value", trim($Datos[0]['valor']));
                            $Ajax->Assign("deudora".$this->Sufijo,"value", trim($Datos[0]['deudora']));
                            $Ajax->Assign("acreedora".$this->Sufijo,"value", trim($Datos[0]['acreedora']));
                            $Ajax->Assign("iddeudora".$this->Sufijo,"value", trim($Datos[0]['idctadeudora']));
                            $Ajax->Assign("idacreedora".$this->Sufijo,"value", trim($Datos[0]['idctaacreedora']));
                            $Ajax->Assign("tipoimpuesto".$this->Sufijo,"value", trim($Datos[0]['idimpuesto']));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaPorcentajeRetencion($Ajax,"NOEXISTEID","No existe un Codigo con ese ID.");
                       }
              }
              
              function MuestraPorcentajeRetencionGuardado($Ajax,$PorcentajeRetencion)
              {       if (count($PorcentajeRetencion)>0)
                        {   $id = str_pad($PorcentajeRetencion[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraPorcentajeRetencionGrid($Ajax,$PorcentajeRetencion);
                            return $this->RespuestaPorcentajeRetencion($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaPorcentajeRetencion($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraPorcentajeRetencionEditado($Ajax,$PorcentajeRetencion)
              {       foreach ($PorcentajeRetencion as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraPorcentajeRetencionRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaPorcentajeRetencion($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaPorcentajeRetencion($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraPorcentajeRetencionEliminado($Ajax,$PorcentajeRetencion)
              {        foreach ($PorcentajeRetencion as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraPorcentajeRetencionRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaPorcentajeRetencion($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaPorcentajeRetencion($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraPorcentajeRetencionRowGrid($Ajax,$PorcentajeRetencion,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($PorcentajeRetencion['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($PorcentajeRetencion['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($PorcentajeRetencion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($PorcentajeRetencion['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($PorcentajeRetencion['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($PorcentajeRetencion['valor']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($PorcentajeRetencion['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($PorcentajeRetencion['idestado'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($PorcentajeRetencion['idctadeudora']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($PorcentajeRetencion['idctaacreedora'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",trim($PorcentajeRetencion['acreedora']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",trim($PorcentajeRetencion['deudora'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],11),"innerHTML",trim($PorcentajeRetencion['idimpuesto'])); 
                        return $Ajax;
                }

              function MuestraPorcentajeRetencionExcepcion($Ajax,$Msg)
                {       return $this->RespuestaPorcentajeRetencion($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaPorcentajeRetencion($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
                /*** MODAL ***/
                private function SearchGridModalConfig()
                {       $Columns['Id']     = array('40px','center','');
                        $Columns['Codigo']  = array('45px','center','');
                        $Columns['Descripcion'] = array('150px','left','');
                        $Columns['Padre']  = array('150px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGridPorcentajeRetencion($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalPorcentajeRetencion($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGridPorcentajeRetencion($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalPorcentajeRetencion($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalPorcentajeRetencion($Grid,$Datos)
                {       foreach ($Datos as $Opcion)
                        {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['descripcion']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['padre']));
                                $Grid->CreaSearchRowsDetalle ($id,"black");
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
        }
?>

