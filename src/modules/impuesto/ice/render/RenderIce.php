<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderIce
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 2; 
              }
               
              function CreaIceMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);
                       $FrmIce = "estado:descripcion:codigo:valor:txIvaCompra:idIvaCompra"
                                .":txIvaVenta:idIvaVenta:txIceCompra:idIceCompra:txIceVenta:idIceVenta";
                       $Ice = '<table class="Form-Frame" cellpadding="0">';
                       $Ice.= '       <tr height="30">';
                       $Ice.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $Ice.= '           <td class="Form-Label" style="width:75%" colspan="3">';
                       $Ice.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\''.$FrmIce.'\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $Ice.= '           </td>';
                       $Ice.= '       </tr>';
                       $Ice.= '       <tr height="30">';
                       $Ice.= '           <td class="Form-Label" style="width:25%">C&oacute;digo</td>';
                       $Ice.= '           <td class="Form-Label" style="width:75%">';
                       $Ice.= '                 <input type="text" class="txt-upper t07" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Ice.= '           </td>';
                       $Ice.= '           <td class="Form-Label" style="width:35%">Valor</td>';
                       $Ice.= '           <td class="Form-Label" style="width:65%">';
                       $Ice.= '                 <input type="text" class="txt-upper t04" id="valor'.$this->Sufijo.'" name="valor'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Ice.= '           </td>';
                       $Ice.= '       </tr>';
                       $Ice.= '       <tr height="30">';
                       $Ice.= '             <td class="Form-Label" style="width:25%">Descripci&oacute;n</td>';
                       $Ice.= '             <td class="Form-Label" style="width:75%" colspan="3">';
                       $Ice.= '                 <input type="text" class="txt-upper t10" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Ice.= '             </td>';
                       $Ice.= '         </tr>';
                       $Ice.= '       <tr height="28">';
                       $Ice.= '           <td class="Form-Label">Ice Compra</td>';
                       $Ice.= '           <td class="Form-Label" colspan="3">';
                                      $Ice.= $Search->TextSch("IceCompra","","")->Enabled(false)->Create("t13");
                       $Ice.= '           </td>';
                       $Ice.= '       </tr>';
                       $Ice.= '       <tr height="30">';
                       $Ice.= '           <td class="Form-Label">Ice Venta</td>';
                       $Ice.= '           <td class="Form-Label" colspan="3">';
                                      $Ice.= $Search->TextSch("IceVenta","","")->Enabled(false)->Create("t13");
                       $Ice.= '             </td>';
                       $Ice.= '       </tr>';
                       $Ice.= '       <tr height="30">';
                       $Ice.= '           <td class="Form-Label">Iva Compra</td>';
                       $Ice.= '           <td class="Form-Label" colspan="3">';
                                      $Ice.= $Search->TextSch("IvaCompra","","")->Enabled(false)->Create("t13");
                       $Ice.= '           </td>';
                       $Ice.= '       </tr>';
                       $Ice.= '       <tr height="30">';
                       $Ice.= '           <td class="Form-Label">Iva Venta</td>';
                       $Ice.= '           <td class="Form-Label" colspan="3">';
                                      $Ice.= $Search->TextSch("IvaVenta","","")->Enabled(false)->Create("t13");
                       $Ice.= '           </td>';
                       $Ice.= '       </tr>';
                       $Ice.= '       <tr height="30">';
                       $Ice.= '           <td class="Form-Label">Estado</td>';
                       $Ice.= '           <td class="Form-Label" colspan="3">';
                                            $Ice.= $this->CreaComboEstado();
                       $Ice.= '           </td>';
                       $Ice.= '       </tr>';
                       $Ice.= '</table>';
                       return $Ice;
              }
              
              private function SearchGridValues()
              {         $Columns['Id'] = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('100px','left','');
                        $Columns['Descripci&oacute;n'] = array('150px','left','');
                        $Columns['Valor'] = array('100px','left','');
                        $Columns['Estado'] = array('100px','left','');
                        $Columns['idIceVenta'] = array('0px','left','none');
                        $Columns['idIceCompra'] = array('0px','left','none');
                        $Columns['idIvaVenta'] = array('0px','left','none');
                        $Columns['idIvaCompra'] = array('0px','left','none');
                        $Columns['IceVenta'] = array('0px','left','none');
                        $Columns['IceCompra'] = array('0px','left','none');
                        $Columns['IvaVenta'] = array('0px','left','none');
                        $Columns['IvaCompra'] = array('0px','left','none');
                        $Columns['estado'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaIceSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->IceGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }        

              private function IceGridHTML($ObjSchGrid,$Datos)
              {       foreach ($Datos as $plancuentas)
                      {       
                               $id = str_pad($plancuentas['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['codigo']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['descripcion']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['valor']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['txtestado']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['idctaiceventa']));                           
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['idctaicecompra']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['idctaivaventa']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['idctaivacompra']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['iceventa']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['icecompra']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['ivaventa']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['ivacompra']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($plancuentas['idestado']));
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($plancuentas['idestado']==0 ? "red" : ""));

                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraIce($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],2,'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Datos[0]['codigo']));
                            $Ajax->Assign("valor".$this->Sufijo,"value", trim($Datos[0]['valor']));
                            $Ajax->Assign("txIceCompra".$this->Sufijo,"value", trim($Datos[0]['icecompra']));
                            $Ajax->Assign("idIceCompra".$this->Sufijo,"value", trim($Datos[0]['idicecompra']));
                            $Ajax->Assign("txIceVenta".$this->Sufijo,"value", trim($Datos[0]['iceventa']));
                            $Ajax->Assign("idIceVenta".$this->Sufijo,"value", trim($Datos[0]['idiceventa']));
                            $Ajax->Assign("txIvaCompra".$this->Sufijo,"value", trim($Datos[0]['ivacompra']));
                            $Ajax->Assign("idIvaCompra".$this->Sufijo,"value", trim($Datos[0]['idivacompra']));
                            $Ajax->Assign("txIvaVenta".$this->Sufijo,"value", trim($Datos[0]['ivaventa']));
                            $Ajax->Assign("idIvaVenta".$this->Sufijo,"value", trim($Datos[0]['idivaventa']));
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                       }else{
                            return $this->RespuestaIce($Ajax,"NOEXISTEID","No existe un Codigo con ese ID.");
                       }
              }
              
              function MuestraIceGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->IceGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }
              
              function MuestraIceGuardado($Ajax,$Ice)
              {       if (count($Ice)>0)
                        {   $id = str_pad($Ice[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraIceGrid($Ajax,$Ice);
                            return $this->RespuestaIce($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaIce($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraIceEditado($Ajax,$Ice)
              {       foreach ($Ice as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraIceRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaIce($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaIce($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraIceEliminado($Ajax,$Ice)
              {        foreach ($Ice as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraIceRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaIce($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaIce($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraIceRowGrid($Ajax,$Ice,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Ice['id'],$estado);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Ice['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Ice['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Ice['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Ice['valor']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Ice['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Ice['idiceventa']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($Ice['idicecompra'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($Ice['idivaventa']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($Ice['idivacompra'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",trim($Ice['iceventa']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",trim($Ice['icecompra'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],11),"innerHTML",trim($Ice['ivaventa'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],12),"innerHTML",trim($Ice['ivacompra'])); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],13),"innerHTML",trim($Ice['idestado']));
                        
                        return $Ajax;
                }

              function MuestraIceExcepcion($Ajax,$Msg)
                {       return $this->RespuestaIce($Ajax,"EXCEPCION",$Msg);    
                }

              private function RespuestaIce($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
              /*** MODAL ***/
              private function SearchGridModalConfig()
              {       $Columns['Id']     = array('40px','center','');
                      $Columns['Codigo']  = array('45px','center','');
                      $Columns['Descripcion'] = array('150px','left','');
                      $Columns['Padre']  = array('150px','left','');
                      return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
              }

              function CreaModalGridIce($Operacion,$Datos)
              {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                      $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                      $GrdDataHTML = $this->GridDataHTMLModalIce($SearchGrid,$Datos);
                      return $SearchGrid->CreaSearchGrid($GrdDataHTML);
              }
                
              function MuestraModalGridIce($Ajax,$Operacion,$Datos)
              {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                      $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                      $GrdDataHTML = $this->GridDataHTMLModalIce($SearchGrid,$Datos);
                      $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                      return $Ajax;
              }        

              private function GridDataHTMLModalIce($Grid,$Datos)
              {       foreach ($Datos as $Opcion)
                      {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                              $Grid->CreaSearchCellsDetalle($id,'',$id);
                              $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                              $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['descripcion']));
                              $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['padre']));
                              $Grid->CreaSearchRowsDetalle ($id,"black");
                      }
                      return $Grid->CreaSearchTableDetalle();
              }
        }
?>

