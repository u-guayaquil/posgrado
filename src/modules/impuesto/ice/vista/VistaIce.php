<?php
         $Sufijo = '_ice';

         require_once('src/modules/impuesto/ice/controlador/ControlIce.php');
         $ControlIce = new ControlIce($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlIce,'GuardaIce'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlIce,'MuestraIceByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlIce,'MuestraIceByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlIce,'EliminaIce'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlIce,'CargaModalGridIce'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlIce,'ConsultaModalGridIceByTX'));
         
         $xajax->processRequest();

?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/impuesto/ice/js/JSICE.js"></script>
         </head>
<body>        
         <div class="FormBasic" style="width:535px">
               <div class="FormSectionMenu">              
              <?php  $ControlIce->CargaIceBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '    BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlIce->CargaIceMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlIce->CargaIceSearchGrid();  ?>
              </div>  
         </div> 
      </body>
</html>   
              

