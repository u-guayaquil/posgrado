function ButtonClick_ice(Sufijo, Operacion)
{
    var objIce = "id:estado:descripcion:codigo:valor:txIvaCompra:idIvaCompra"
            + ":txIvaVenta:idIvaVenta:txIceCompra:idIceCompra:txIceVenta:idIceVenta";
    var frmIce = "estado:descripcion:codigo:valor:txIvaCompra:idIvaCompra"
            + ":txIvaVenta:idIvaVenta:txIceCompra:idIceCompra:txIceVenta:idIceVenta";

    if (Operacion == 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, frmIce, "id:estado");
        BarButtonStateEnabled(Sufijo, "btIceCompra:btIceVenta:btIvaCompra:btIvaVenta");
        ElementClear(Sufijo, objIce);
    } else if (Operacion == 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateEnabled(Sufijo, "btIceCompra:btIceVenta:btIvaCompra:btIvaVenta");
        ElementStatus(Sufijo, frmIce, "id");
        if(ElementGetValue(Sufijo,"estado")==='0'){
            ElementStatus(Sufijo, "estado", "");
        }else{
            ElementStatus(Sufijo, "", "estado");
        }
    } else if (Operacion == 'addDel')
    {
        if (ElementGetValue(Sufijo, "estado") == 1)
        {
            if (confirm("Desea inactivar este registro?"))
                xajax_Elimina_ice(ElementGetValue(Sufijo, "id"));
        } else
            alert("El registro se encuentra inactivo");
    } else if (Operacion == 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, frmIce))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {
                var Forma = PrepareElements(Sufijo, objIce);
                xajax_Guarda_ice(JSON.stringify({Forma}));
            }
        }
    } else if (Operacion == 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateDisabled(Sufijo, "btCuentaIce");
        ElementStatus(Sufijo, "id", frmIce);
        ElementClear(Sufijo, objIce);
    } else if (Operacion == 'addImp')
    {
    } else {
        xajax_CargaModal_ice(Operacion);
    }

    return false;
}

function SearchByElement_ice(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    switch (Elemento.id) {
        case "id" + Sufijo:
            if (Elemento.value.length != 0)
            {
                if (BarButtonState(Sufijo, "Inactive"))
                    xajax_MuestraByID_ice(Elemento.value);
            } else
                BarButtonState(Sufijo, "Default");
            break;
        case "FindIvaCompraTextBx" + Sufijo:
            xajax_BuscaModalByTX_ice("btIvaCompra", Elemento.value);
            break;
        case 'FindTextBx' + Sufijo:
            xajax_MuestraByTX_ice(Elemento.value);
            break;
        case 'FindIvaVentaTextBx' + Sufijo:
            xajax_BuscaModalByTX_ice("btIvaVenta", Elemento.value);
            break;
        case 'FindIceCompraTextBx' + Sufijo:
            xajax_BuscaModalByTX_ice("btIceCompra", Elemento.value);
            break;
        case 'FindIceVentaTextBx' + Sufijo:
            xajax_BuscaModalByTX_ice("btIceVenta", Elemento.value);
            break;
    }
}

function SearchGetData_ice(Sufijo, Grilla)
{   if (IsDisabled(Sufijo,"addSav")) {
            BarButtonState(Sufijo, "Active");

            document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
            document.getElementById("descripcion" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
            document.getElementById("codigo" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
            document.getElementById("valor" + Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
            document.getElementById("idIceCompra" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
            document.getElementById("idIceVenta" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
            document.getElementById("idIvaCompra" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
            document.getElementById("idIvaVenta" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
            document.getElementById("txIceCompra" + Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
            document.getElementById("txIceVenta" + Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue;
            document.getElementById("txIvaCompra" + Sufijo).value = Grilla.cells[11].childNodes[0].nodeValue;
            document.getElementById("txIvaVenta" + Sufijo).value = Grilla.cells[12].childNodes[0].nodeValue;
            document.getElementById("estado" + Sufijo).value = Grilla.cells[13].childNodes[0].nodeValue;
    }
    return false;
}

function SearchIceCompraGetData_ice(Sufijo, DatosGrid)
{
    document.getElementById("idIceCompra" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txIceCompra" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}
function SearchIceVentaGetData_ice(Sufijo, DatosGrid)
{
    document.getElementById("idIceVenta" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txIceVenta" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}
function SearchIvaCompraGetData_ice(Sufijo, DatosGrid)
{
    document.getElementById("idIvaCompra" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txIvaCompra" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}
function SearchIvaVentaGetData_ice(Sufijo, DatosGrid)
{
    document.getElementById("idIvaVenta" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txIvaVenta" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}

function XAJAXResponse_ice(Sufijo, Mensaje, Datos)
{
    var objIce = "estado:descripcion:codigo:valor:txIvaCompra:idIvaCompra"
            + ":txIvaVenta:idIvaVenta:txIceCompra:idIceCompra:txIceVenta:idIceVenta";
    if (Mensaje === "GUARDADO")
    {
        BarButtonState(Sufijo, "Active");
        BarButtonStateDisabled(Sufijo, "btIceCompra:btIceVenta:btIvaCompra:btIvaVenta");
        ElementSetValue(Sufijo, "id", Datos);
        ElementStatus(Sufijo, "id", objIce);
        alert('Los datos se guardaron correctamente.');
    } else if (Mensaje === 'ELIMINADO')
    {
        ElementSetValue(Sufijo, "estado", Datos);
        alert("Los Datos se eliminaron correctamente");
    } else if (Mensaje === 'NOEXISTEID')
    {
        BarButtonState(Sufijo, 'Default');
        ElementClear(Sufijo, "id");
        alert(Datos);
    } else if (Mensaje === 'EXCEPCION')
    {
        BarButtonState(Sufijo, "addNew");
        alert(Datos);
    }
}