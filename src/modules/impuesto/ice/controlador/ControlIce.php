<?php
         require_once("src/rules/impuesto/servicio/ServicioIce.php");
         require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/impuesto/ice/render/RenderIce.php");        
         require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlIce
         {     private  $Sufijo; 
               private  $ServicioIce;
               private  $ServicioPlanCuentas;
               private  $RenderIce;
               private  $RenderPlanCuentas;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioIce = new ServicioIce();
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        $this->RenderIce = new RenderIce($Sufijo);
                        $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
               }

               function CargaIceBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaIceMantenimiento()
               {        echo $this->RenderIce->CreaIceMantenimiento();
               }

               function CargaIceSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoIce = $this->ServicioIce->BuscarIceByDescripcion($prepareDQL);
                        echo $this->RenderIce->CreaIceSearchGrid($datoIce);
               }         
               
               function MuestraIceByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoIce = $this->ServicioIce->BuscarIceByID($prepareDQL);
                        return $this->RenderIce->MuestraIce($ajaxRespon,$datoIce);
               }

               function MuestraIceByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoIce = $this->ServicioIce->BuscarIceByDescripcion($prepareDQL);
                        return $this->RenderIce->MuestraIceGrid($ajaxRespon,$datoIce);
               }

               function GuardaIce($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Ice  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioIce->GuardaDBIce($Ice);
                        if (is_numeric($id)){
                            $function = (empty($Ice->id) ? "MuestraIceGuardado" : "MuestraIceEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioIce->BuscarIceByID($prepareDQL);
                            return $this->RenderIce->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderIce->MuestraIceExcepcion($ajaxRespon,intval($id));
                        }    
               }
               
               function EliminaIce($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioIce->DesactivaIce(intval($id));
                        $Datos = $this->ServicioIce->BuscarIceByDescripcion($prepareDQL);
                        return $this->RenderIce->MuestraIceEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridIce($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '');
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca Cuentas";
                        $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                        $jsonModal['Ancho'] = "517";
                           
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                                
                function ConsultaModalGridIceByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                        $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        return $this->RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);                                                    
                }
         }

?>

