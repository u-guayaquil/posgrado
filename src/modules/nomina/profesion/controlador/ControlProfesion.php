<?php
         require_once("src/rules/nomina/servicio/ServicioProfesion.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/nomina/profesion/render/RenderProfesion.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlProfesion
         {     private  $Sufijo; 
               private  $ServicioProfesion;
               private  $RenderProfesion;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioProfesion = new ServicioProfesion();
                        $this->RenderProfesion = new RenderProfesion($Sufijo);
               }

               function CargaProfesionBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaProfesionMantenimiento()
               {        echo $this->RenderProfesion->CreaProfesionMantenimiento();
               }

               function CargaProfesionSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoProfesion = $this->ServicioProfesion->BuscarProfesionByDescripcion($prepareDQL);
                        echo $this->RenderProfesion->CreaProfesionSearchGrid($datoProfesion); 
               }         
               
               function MuestraProfesionByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoProfesion = $this->ServicioProfesion->BuscarProfesionByID($prepareDQL);
                        return $this->RenderProfesion->MuestraProfesion($ajaxRespon,$datoProfesion);
               }

               function MuestraProfesionByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoProfesion = $this->ServicioProfesion->BuscarProfesionByDescripcion($prepareDQL);
                        return $this->RenderProfesion->MuestraProfesionGrid($ajaxRespon,$datoProfesion);
               }

               function GuardaProfesion($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Profesion  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioProfesion->GuardaDBProfesion($Profesion);
                        if (is_numeric($id)){
                            $function = (empty($Profesion->id) ? "MuestraProfesionGuardado" : "MuestraProfesionEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioProfesion->BuscarProfesionByID($prepareDQL);
                            return $this->RenderProfesion->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderProfesion->MuestraProfesionExcepcion($ajaxRespon,intval($id));
                        }                   
               } 
               
               function EliminaProfesion($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioProfesion->DesactivaProfesion(intval($id));
                        $Datos = $this->ServicioProfesion->BuscarProfesionByDescripcion($prepareDQL);
                        return $this->RenderProfesion->MuestraProfesionEliminado($ajaxRespon,$Datos);
                }
         }

?>

