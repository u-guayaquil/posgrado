<?php
         $Sufijo = '_Profesion';
         
         require_once('src/modules/nomina/profesion/controlador/ControlProfesion.php');
         $ControlProfesion = new ControlProfesion($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlProfesion,'GuardaProfesion'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlProfesion,'MuestraProfesionByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlProfesion,'MuestraProfesionByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlProfesion,'EliminaProfesion'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/nomina/profesion/js/JSProfesion.js"></script>
         </head>
<body>
         <div class="FormBasic" style="width:500px">
               <div class="FormSectionMenu">              
              <?php  $ControlProfesion->CargaProfesionBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlProfesion->CargaProfesionMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlProfesion->CargaProfesionSearchGrid();  ?>
              </div>  
         </div> 
         </body>
</html>
              

