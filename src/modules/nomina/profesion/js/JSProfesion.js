        function ButtonClick_Profesion(Sufijo, Operacion)
        {
            var objProfesion = "id:descripcion:codigo:estado";
            var frmProfesion = "descripcion:codigo:estado";

            if (Operacion == 'addNew')
            {
                BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, objProfesion, "id:estado");
                ElementClear(Sufijo, objProfesion);
            } else if (Operacion == 'addMod')
            {
                BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, objProfesion, "id");
                if(ElementGetValue(Sufijo,"estado")==='0'){
                    ElementStatus(Sufijo, "estado", "");
                }else{
                    ElementStatus(Sufijo, "", "estado");
                }
            } else if (Operacion == 'addDel')
            {
                if (ElementGetValue(Sufijo, "estado") == 1)
                {
                    if (confirm("Desea inactivar este registro?"))
                        xajax_Elimina_Profesion(ElementGetValue(Sufijo, "id"));
                } else
                    alert("El registro se encuentra inactivo");
            } else if (Operacion == 'addSav')
            {
                if (ElementValidateBeforeSave(Sufijo, "descripcion"))
                {
                    if (BarButtonState(Sufijo, "Inactive"))
                    {
                        var Forma = PrepareElements(Sufijo, objProfesion);
                        xajax_Guarda_Profesion(JSON.stringify({Forma}));
                    }
                }
            } else if (Operacion == 'addCan')
            {
                BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, "id", frmProfesion);
                ElementClear(Sufijo, objProfesion);
            } else if (Operacion == 'addImp')
            {
            }
            return false;
        }

        function SearchByElement_Profesion(Sufijo, Elemento)
        {
            Elemento.value = TrimElement(Elemento.value);
            if (Elemento.name == "id" + Sufijo)
            {
                if (Elemento.value.length != 0)
                {
                    ContentFlag = document.getElementById("descripcion" + Sufijo);
                    if (ContentFlag.value.length == 0)
                    {
                        if (BarButtonState(Sufijo, "Inactive"))
                            xajax_MuestraByID_Profesion(Elemento.value);
                    }
                } else
                    BarButtonState(Sufijo, "Default");
            } else
            {
                xajax_MuestraByTX_Profesion(Elemento.value);
            }
        }

        function SearchGetData_Profesion(Sufijo, Grilla)
        {       if (IsDisabled(Sufijo,"addSav")) 
                {   BarButtonState(Sufijo, "Active");
                    document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                    document.getElementById("codigo" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                    document.getElementById("descripcion" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                    document.getElementById("estado" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                }    
                return false;
        }

        function XAJAXResponse_Profesion(Sufijo, Mensaje, Datos)
        {
            var objProfesion = "descripcion:codigo:estado";
            if (Mensaje === "GUARDADO")
            {
                BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, "btpadre");
                ElementSetValue(Sufijo, "id", Datos);
                ElementStatus(Sufijo, "id", objProfesion);
                alert('Los datos se guardaron correctamente.');
            } else if (Mensaje === 'ELIMINADO')
            {
                ElementSetValue(Sufijo, "estado", Datos);
                alert("Los Datos se eliminaron correctamente");
            } else if (Mensaje === 'NOEXISTEID')
            {
                BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "id");
                alert(Datos);
            } else if (Mensaje === 'EXCEPCION')
            {
                BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
        }