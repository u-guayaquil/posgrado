<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderProfesion
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;  
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 3;                     
                }
              
                function CreaOpcionBarButton($Buttons)
                {       $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
                }
               
                function CreaProfesionMantenimiento()
                {       $Profesion = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $Profesion.= '       <tr height="30">';
                        $Profesion.= '           <td class="Form-Label" style="width:25%">Id</td>';
                        $Profesion.= '           <td class="Form-Label" style="width:75%">';
                        $Profesion.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:codigo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Profesion.= '           </td>';
                        $Profesion.= '       </tr>';
                        $Profesion.= '       <tr height="30">';
                        $Profesion.= '           <td class="Form-Label">C&oacute;digo</td>';
                        $Profesion.= '           <td class="Form-Label">';
                        $Profesion.= '                 <input type="text" class="txt-upper t03" id="codigo'.$this->Sufijo.'" name="codigo'.$this->Sufijo.'" value="" maxlength="60" disabled/>';
                        $Profesion.= '           </td>';
                        $Profesion.= '       </tr>'; 
                        $Profesion.= '       <tr height="30">';
                        $Profesion.= '           <td class="Form-Label">Descripci&oacute;n</td>';
                        $Profesion.= '           <td class="Form-Label">';
                        $Profesion.= '               <input type="text" class="txt-upper t14" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="60" disabled/>';
                        $Profesion.= '           </td>';
                        $Profesion.= '       </tr>';
                        $Profesion.= '       </tr>';                       
                        $Profesion.= '       <tr height="30">';
                        $Profesion.= '           <td class="Form-Label">Estado</td>';
                        $Profesion.= '           <td class="Form-Label">';
                        $Profesion.=            $this->CreaComboEstado();
                        $Profesion.= '           </td>';
                        $Profesion.= '       </tr>';
                        $Profesion.= '</table>';
                        return $Profesion;
                }
              
                private function SearchGridValues()
                {       $Columns['Id']            = array('30px','center','');
                        $Columns['C&oacute;digo'] = array('0px','left','none');
                        $Columns['Descripci&oacute;n'] = array('280px','left','');
                        $Columns['Creacion']      = array('70px','center','');
                        $Columns['Estado']        = array('70px','left',''); 
                        $Columns['idEstado']      = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
                }

                function CreaProfesionSearchGrid($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->ProfesionGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }
              
                private function ProfesionGridHTML($ObjSchGrid,$Datos)
                {       foreach ($Datos as $cargo)
                        {       $fecreacion = $this->Fechas->changeFormatDate($cargo['fecreacion'],"DMY")[1];
                                $id = str_pad($cargo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['codigo']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['txtestado']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['idestado']); 
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($cargo['idestado']==0 ? "red" : ""));
                        }
                        return $ObjSchGrid->CreaSearchTableDetalle(); 
                }

                function TraeDatosEstadoByArray($IdArray)
                {       $ServicioEstado = new ServicioEstado();
                        $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $datoEstado;
                }
              
                private function CreaComboEstado()
                {       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                        $Select = new ComboBox($this->Sufijo);                        
                        return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
                }
              
                function MuestraProfesion($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("codigo".$this->Sufijo,"value", trim($Dato['codigo']));    
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion'])); 
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaProfesion($Ajax,"NOEXISTEID","No existe una profesion con el ID ingresado.");
                }
              
                function MuestraProfesionGrid($Ajax,$Datos)
                {      $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->ProfesionGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
                }   
               
                function MuestraProfesionGuardado($Ajax,$Profesion)
                {       if (count($Profesion)>0)
                        {   $id = str_pad($Profesion[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraProfesionGrid($Ajax,$Profesion);
                            return $this->RespuestaProfesion($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaProfesion($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraProfesionEditado($Ajax,$Profesion)
                {       foreach ($Profesion as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraProfesionRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaProfesion($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaProfesion($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
              
                function MuestraProfesionEliminado($Ajax,$Profesion)
                {       foreach ($Profesion as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraProfesionRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaProfesion($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->RespuestaProfesion($xAjax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraProfesionRowGrid($Ajax,$Profesion,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Profesion['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($Profesion['fecreacion'],"DMY")[1];
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Profesion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Profesion['codigo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Profesion['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($fecreacion));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Profesion['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Profesion['idestado']));
                        return $Ajax;
                }

                function MuestraProfesionExcepcion($Ajax,$Msg)
                {       return $this->RespuestaProfesion($Ajax,"EXCEPCION",$Msg);    
                }

                private function RespuestaProfesion($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
        }
?>

