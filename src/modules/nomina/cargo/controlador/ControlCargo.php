<?php
         require_once("src/rules/nomina/servicio/ServicioCargo.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/nomina/cargo/render/RenderCargo.php");


         class ControlCargo
         {     private  $Sufijo; 
               private  $ServicioCargo;
               private  $RenderCargo;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioCargo = new ServicioCargo();
                        $this->RenderCargo = new RenderCargo($Sufijo);
               }

               function CargaCargoBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaCargoMantenimiento()
               {        echo $this->RenderCargo->CreaCargoMantenimiento();
               }

               function CargaCargoSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoCargo = $this->ServicioCargo->BuscarCargoByDescripcion($prepareDQL);
                        echo $this->RenderCargo->CreaCargoSearchGrid($datoCargo); 
               }         
               
               function MuestraCargoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoCargo = $this->ServicioCargo->BuscarCargoByID($prepareDQL);
                        return $this->RenderCargo->MuestraCargo($ajaxRespon,$datoCargo);
               }

               function MuestraCargoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoCargo = $this->ServicioCargo->BuscarCargoByDescripcion($prepareDQL);
                        return $this->RenderCargo->MuestraCargoGrid($ajaxRespon,$datoCargo);
               }

               function GuardaCargo($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Cargo  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioCargo->GuardaDBCargo($Cargo);
                        if (is_numeric($id)){
                            $function = (empty($Cargo->id) ? "MuestraCargoGuardado" : "MuestraCargoEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioCargo->BuscarCargoByID($prepareDQL);
                            return $this->RenderCargo->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderCargo->MuestraCargoExcepcion($ajaxRespon,intval($id));
                        }                   
               } 
               
               function EliminaCargo($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioCargo->DesactivaCargo(intval($id));
                        $Datos = $this->ServicioCargo->BuscarCargoByDescripcion($prepareDQL);
                        return $this->RenderCargo->MuestraCargoEliminado($ajaxRespon,$Datos);
                }
         }

?>

