<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/nomina/servicio/ServicioDepartamento.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderCargo
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
            function __construct($Sufijo = "")
            {           $this->Sufijo = $Sufijo;  
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 3;                     
            }
              
            function CreaOpcionBarButton($Buttons)
            {           $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
            }
               
            function CreaCargoMantenimiento()
            {           $Cargo = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $Cargo.= '       <tr height="30">';
                        $Cargo.= '           <td class="Form-Label" style="width:25%">Id</td>';
                        $Cargo.= '           <td class="Form-Label" style="width:75%">';
                        $Cargo.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Cargo.= '           </td>';
                        $Cargo.= '       </tr>';
                        $Cargo.= '       <tr height="30">';
                        $Cargo.= '           <td class="Form-Label">Descripci&oacute;n</td>';
                        $Cargo.= '           <td class="Form-Label">';
                        $Cargo.= '               <input type="text" class="txt-upper t10" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="60" disabled/>';
                        $Cargo.= '           </td>';
                        $Cargo.= '       </tr>';
                        $Cargo.= '       <tr height="30">';
                        $Cargo.= '           <td class="Form-Label">Departamento</td>';
                        $Cargo.= '           <td class="Form-Label">';
                        $Cargo.=                 $this->CreaComboDepartamento();
                        $Cargo.= '           </td>';
                        $Cargo.= '       </tr>';                       
                        $Cargo.= '       <tr height="30">';
                        $Cargo.= '           <td class="Form-Label">Estado</td>';
                        $Cargo.= '           <td class="Form-Label">';
                        $Cargo.=                 $this->CreaComboEstado();
                        $Cargo.= '           </td>';
                        $Cargo.= '       </tr>';
                        $Cargo.= '</table>';
                        return $Cargo;
              }
              
            private function SearchGridValues()
            {           $Columns['Id'] = array('30px','center','');
                        $Columns['Descripci&oacute;n'] = array('163px','left','');
                        $Columns['Departamento'] = array('163px','left','');
                        $Columns['Creacion'] = array('70px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado'] = array('0px','left','none');
                        $Columns['iddepartamento'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }

            function CreaCargoSearchGrid($Datos)
            {           $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->CargoGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
            }
              
            private function CargoGridHTML($ObjSchGrid,$Datos)
            {           foreach ($Datos as $cargo)
                        {       $fecreacion = $this->Fechas->changeFormatDate($cargo['fecreacion'],"DMY");
                                $id = str_pad($cargo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['departamento']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['txtestado']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['idestado']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['iddepartamento']); 
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($cargo['idestado']==0 ? "red" : ""));
                        }
                        return $ObjSchGrid->CreaSearchTableDetalle(); 
            }

            function TraeDatosEstadoByArray($IdArray)
            {           $ServicioEstado = new ServicioEstado();
                        $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $datoEstado;
            }
              
            private function CreaComboEstado()
            {           $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                        $Select = new ComboBox($this->Sufijo);                        
                        return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
            }
              
            function TraeDatosDepartamento()
            {           $ServicioDepartamento = new ServicioDepartamento();
                        $PrepareSQL = array('limite' => 50,'estado' => 1);
                        $datoDepartamento = $ServicioDepartamento->BuscarDepartamentoByDescripcion($PrepareSQL);
                        return $datoDepartamento;
            }
              
            private function CreaComboDepartamento()
            {           $datosDepartamento= $this->TraeDatosDepartamento();
                        $Select = new ComboBox($this->Sufijo);                        
                        return $Select->Combo("departamento",$datosDepartamento)->Selected(1)->Create("s10");
            }
              
            function MuestraCargo($Ajax,$Datos)
            {           foreach ($Datos as $Dato)
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));   
                            $Ajax->Assign("departamento".$this->Sufijo,"value", $Dato['iddepartamento']);
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }    
                        return $this->RespuestaCargo($Ajax,"NOEXISTEID","No existe una cargo con el ID ingresado.");
            }
              
            function MuestraCargoGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->CargoGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }   
               
            function MuestraCargoGuardado($Ajax,$Cargo)
            {       if (count($Cargo)>0)
                    {   $id = str_pad($Cargo[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraCargoGrid($Ajax,$Cargo);
                        return $this->RespuestaCargo($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaCargo($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraCargoEditado($Ajax,$Cargo)
            {       foreach ($Cargo as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraCargoRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaCargo($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaCargo($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraCargoEliminado($Ajax,$Cargo)
            {       foreach ($Cargo as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraCargoRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaCargo($xAjax,"ELIMINADO",$datos['idestado']);
                    }  
                    return $this->RespuestaCargo($xAjax,"EXCEPCION","No se eliminó la información.");
            }
              
            function MuestraCargoRowGrid($Ajax,$Cargo,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($Cargo['id'],$estado);
                    $fecreacion = $this->Fechas->changeFormatDate($Cargo['fecreacion'],"DMY");
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Cargo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Cargo['descripcion']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Cargo['departamento']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$fecreacion[1]);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Cargo['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$Cargo['idestado']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$Cargo['iddepartamento']);
                    return $Ajax;
            }

            function MuestraCargoExcepcion($Ajax,$Msg)
            {       return $this->RespuestaCargo($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaCargo($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
                
                /**** Modal ****/
            private function SearchGridModalValues()
            {       $Columns['Id']                 = array('30px','center','');
                    $Columns['Cargo'] = array('180px','left','');
                    $Columns['Creacion']           = array('70px','center','');
                    $Columns['Estado']             = array('70px','left',''); 
                    $Columns['idEstado']           = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }
            
            function WidthModalGrid()
            {       return 380;
            }

            function CreaModalGridCargo($Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalValues());
                    $ObjHTML = $this->CargoModalGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }        
                
            function MuestraModalGridCargo($Ajax,$Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalValues());
                    $ObjHTML = $this->CargoModalGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }

            private function CargoModalGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $cargo)
                    {       $fecreacion = $this->Fechas->changeFormatDate($cargo['fecreacion'],"DMY");
                            $id = str_pad($cargo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['descripcion']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['txtestado']); 
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,($cargo['idestado']==0 ? "red" : ""));
                    }
                    return $ObjSchGrid->CreaSearchTableDetalle(); 
            }
                
        }
?>

