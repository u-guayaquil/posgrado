    var frmRubrosRol = "id:descripcion:variablerubros:movimiento:estado:orden";
    var newRubrosRol = "descripcion:variablerubros:movimiento:orden";
    var valRubrosRol = "descripcion:orden";
    var clsRubrosRol = "id:descripcion:orden";

    function ButtonClick_RubrosRol(Sufijo, Operacion)
    {       if (Operacion == 'addNew')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, newRubrosRol, "id");
                ElementClear(Sufijo, clsRubrosRol);
                ElementGetValue(Sufijo,"estado",1);
            }
            else if (Operacion == 'addMod')
            {   BarButtonState(Sufijo, Operacion);
                if(ElementGetValue(Sufijo,"estado")==0)
                {   ElementStatus(Sufijo, newRubrosRol+":estado", "id");
                }
                else
                {   ElementStatus(Sufijo, newRubrosRol, "id");
                }
            } 
            else if (Operacion == 'addDel')
            {   if (ElementGetValue(Sufijo, "estado")==1)
                {   if (confirm("¿Desea inactivar este registro?"))
                    xajax_Elimina_RubrosRol(ElementGetValue(Sufijo, "id"));
                } 
                else
                alert("El registro se encuentra inactivo");
            } 
            else if (Operacion == 'addSav')
            {   if (ElementValidateBeforeSave(Sufijo, valRubrosRol))
                {   if (BarButtonState(Sufijo, "Inactive"))
                    {   var Forma = PrepareElements(Sufijo, frmRubrosRol);
                        xajax_Guarda_RubrosRol(JSON.stringify({Forma}));
                    }
                }
            } 
            else if (Operacion == 'addCan')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, "id", newRubrosRol+":estado");
                ElementClear(Sufijo, clsRubrosRol);
            }
            return false;
    }

    function SearchByElement_RubrosRol(Sufijo, Elemento)
    {       Elemento.value = TrimElement(Elemento.value);
            if (Elemento.name == "id" + Sufijo)
            {   if (Elemento.value.length != 0)
                {   ContentFlag = document.getElementById("descripcion" + Sufijo);
                    if (ContentFlag.value.length == 0)
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        xajax_MuestraByID_RubrosRol(Elemento.value);
                    }
                } 
                else
                BarButtonState(Sufijo, "Default");
            }
            else
            xajax_MuestraByTX_RubrosRol(Elemento.value);
    }

    function SearchGetData_RubrosRol(Sufijo, Grilla)
    {       if (IsDisabled(Sufijo, "addSav"))
            {   BarButtonState(Sufijo, "Active");
                document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                document.getElementById("variablerubros" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                document.getElementById("descripcion" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                document.getElementById("movimiento" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                document.getElementById("estado" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                document.getElementById("orden" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
            }
            return false;
    }

    function XAJAXResponse_RubrosRol(Sufijo, Mensaje, Datos)
    {       if (Mensaje === "GUARDADO")
            {   BarButtonState(Sufijo, "Active");
                ElementStatus(Sufijo, "id", newRubrosRol+":estado");
                ElementSetValue(Sufijo, "id", Datos);
                alert('Los datos se guardaron correctamente.');
            } 
            else if (Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
                alert("Los Datos se eliminaron correctamente");
            }
            else if (Mensaje === 'NOEXISTEID')
            {   BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "id");
                alert(Datos);
            } 
            else if (Mensaje === 'EXCEPCION')
            {   BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
    }
