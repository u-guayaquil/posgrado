<?php
         $Sufijo = '_RubrosRol';
         
         require_once('src/modules/nomina/rubrosrol/controlador/ControlRubrosRol.php');
         $ControlRubrosRol = new ControlRubrosRol($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlRubrosRol,'GuardaRubrosRol'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlRubrosRol,'MuestraRubrosRolByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlRubrosRol,'MuestraRubrosRolByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlRubrosRol,'EliminaRubrosRol'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/nomina/rubrosrol/js/JSRubrosRol.js"></script>
         </head>
<body>
         <div class="FormBasic" style="width:500px">
               <div class="FormSectionMenu">              
              <?php  $ControlRubrosRol->CargaRubrosRolBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlRubrosRol->CargaRubrosRolMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlRubrosRol->CargaRubrosRolSearchGrid();  ?>
              </div>  
         </div> 
         </body>
</html>
              

