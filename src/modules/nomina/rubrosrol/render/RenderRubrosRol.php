<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/nomina/servicio/ServicioMovimiento.php");
        require_once("src/rules/nomina/servicio/ServicioVariableRubros.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderRubrosRol
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Fechas = new DateControl();
                    $this->Maxlen['id'] = 3;                     
            }
              
            function CreaOpcionBarButton($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);
                    return $BarButton->CreaBarButton($Buttons);
            }
               
            function CreaRubrosRolMantenimiento()
            {       $RubrosRol = '<table border="0" class="Form-Frame" cellpadding="0">';
                    $RubrosRol.= '       <tr height="30">';
                    $RubrosRol.= '           <td class="Form-Label" style="width:20%">Id</td>';
                    $RubrosRol.= '           <td class="Form-Label" style="width:35%">';
                    $RubrosRol.= '                <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $RubrosRol.= '           </td>';
                    $RubrosRol.= '           <td class="Form-Label" style="width:20%">Estado</td>';
                    $RubrosRol.= '           <td class="Form-Label" style="width:25%">';  
                    $RubrosRol.=                 $this->CreaComboEstado();
                    $RubrosRol.= '           </td>';
                    $RubrosRol.= '       </tr>';
                    $RubrosRol.= '       <tr height="30">';
                    $RubrosRol.= '           <td class="Form-Label">Variable</td>';
                    $RubrosRol.= '           <td class="Form-Label">';
                    $RubrosRol.=                 $this->CreaComboVariableRubros();
                    $RubrosRol.= '           </td>';
                    $RubrosRol.= '           <td class="Form-Label">Orden Rol</td>';
                    $RubrosRol.= '           <td class="Form-Label">';
                    $RubrosRol.= '               <input type="text" class="txt-input t01" id="orden'.$this->Sufijo.'" name="orden'.$this->Sufijo.'" value="" maxlength="3" onKeyDown="return soloNumeros(event); " disabled/>';
                    $RubrosRol.= '           </td>';
                    $RubrosRol.= '       </tr>';
                    $RubrosRol.= '       <tr height="30">';
                    $RubrosRol.= '           <td class="Form-Label">Descripci&oacute;n</td>';
                    $RubrosRol.= '           <td class="Form-Label">';
                    $RubrosRol.= '               <input type="text" class="txt-upper t06" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="60" disabled/>';
                    $RubrosRol.= '           </td>';
                    $RubrosRol.= '           <td class="Form-Label">Movimiento</td>';
                    $RubrosRol.= '           <td class="Form-Label">';
                    $RubrosRol.=                 $this->CreaComboMovimiento();
                    $RubrosRol.= '           </td>';
                    $RubrosRol.= '       </tr>';   
                    $RubrosRol.= '</table>';
                    return $RubrosRol;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']       = array('30px','center','');
                    $Columns['Descripci&oacute;n'] = array('200px','left','');                        
                    $Columns['Movimiento']   = array('80px','left','');
                    $Columns['Creacion']     = array('70px','center','');
                    $Columns['Estado']       = array('65px','left',''); 
                    $Columns['Variable']     = array('0px','left','none');
                    $Columns['idMovimiento'] = array('0px','left','none');
                    $Columns['idEstado']     = array('0px','left','none');
                    $Columns['Orden']        = array('0px','left','none');
                        
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }

            private function RubrosRolGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $Rubro) 
                    {       $fecreacion = $this->Fechas->changeFormatDate($Rubro['fecreacion'],"DMY")[1];
                            $id = str_pad($Rubro['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Rubro['descripcion']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Rubro['movimiento']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Rubro['txtestado'])); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Rubro['variable']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Rubro['idmovimiento']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Rubro['idestado']); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Rubro['ordenrol']); 
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,($Rubro['idestado']==0 ? "red" : ""));
                    }
                    return $ObjSchGrid->CreaSearchTableDetalle(); 
            }

            function CreaRubrosRolSearchGrid($Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->RubrosRolGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }
 
            private function CreaComboEstado()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioEstado = new ServicioEstado();
                    $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));                    
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
            }
              
            private function CreaComboMovimiento()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioMovimiento = new ServicioMovimiento();
                    $datosMovimiento = $ServicioMovimiento->BuscarMovimiento();                    
                    return $Select->Combo("movimiento",$datosMovimiento)->Selected(1)->Create("s04");
            }
              
            private function CreaComboVariableRubros()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioVariableRubros= new ServicioVariableRubros();
                    $datosVariableRubros = $ServicioVariableRubros->BuscarVariableRubros();
                    return $Select->Combo("variablerubros",$datosVariableRubros)->Selected(1)->Create("s06");
            }
              
            function MuestraRubrosRol($Ajax,$Datos)
            {       foreach ($Datos as $Dato)
                    {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign("variablerubros".$this->Sufijo,"value", $Dato['variable']);  
                        $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion']));    
                        $Ajax->Assign("movimiento".$this->Sufijo,"value", trim($Dato['idmovimiento']));  
                        $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                        $Ajax->Assign("orden".$this->Sufijo,"value", $Dato['ordenrol']);
                        $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                        return $Ajax;
                    }    
                    return $this->RespuestaRubrosRol($Ajax,"NOEXISTEID","No existe un Rubro con el ID ingresado.");
            }
              
            function MuestraRubrosRolGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->RubrosRolGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }   
               
            function MuestraRubrosRolGuardado($Ajax,$RubrosRol)
            {       if (count($RubrosRol)>0)
                    {   $id = str_pad($RubrosRol[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraRubrosRolGrid($Ajax,$RubrosRol);
                        return $this->RespuestaRubrosRol($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaRubrosRol($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraRubrosRolEditado($Ajax,$RubrosRol)
            {       foreach ($RubrosRol as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraRubrosRolRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaRubrosRol($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaRubrosRol($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraRubrosRolEliminado($Ajax,$RubrosRol)
            {       foreach ($RubrosRol as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraRubrosRolRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaRubrosRol($xAjax,"ELIMINADO",$datos['idestado']);
                    }  
                    return $this->RespuestaRubrosRol($xAjax,"EXCEPCION","No se eliminó la información.");
            }
              
            function MuestraRubrosRolRowGrid($Ajax,$RubrosRol,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($RubrosRol['id'],$estado);
                    $fecreacion = $this->Fechas->changeFormatDate($RubrosRol['fecreacion'],"DMY")[1];
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($RubrosRol['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($RubrosRol['descripcion']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($RubrosRol['movimiento']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$fecreacion);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($RubrosRol['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$RubrosRol['variable']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$RubrosRol['idmovimiento']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$RubrosRol['idestado']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",$RubrosRol['ordenrol']);
                    return $Ajax;
            }

            function MuestraRubrosRolExcepcion($Ajax,$Msg)
            {       return $this->RespuestaRubrosRol($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaRubrosRol($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
                
             /***** Modal *****/

            private function SearchGridModalConfig()
            {       $Columns['Id']         = array('40px','center','');
                    $Columns['Rubro']      = array('150px','left','');
                    $Columns['Variable']   = array('138px','left','');
                    $Columns['Movimiento'] = array('80px','left','');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '120px','AltoRow' => '20px');
            }

            function WidthModalGrid()
            {       return 440;
            }
                
            function CreaModalGridRubrosRol($Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $ObjHTML = $this->ModalGridDataHTMLRubrosRol($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }
                
            function MuestraModalGridRubrosRol($Ajax,$Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $ObjHTML = $this->ModalGridDataHTMLRubrosRol($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }        

            private function ModalGridDataHTMLRubrosRol($Grid,$Datos)
            {       foreach ($Datos as $RubrosRol)
                    {       $id = str_pad($RubrosRol['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($RubrosRol['descripcion']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($RubrosRol['txvariable']));
                            $Grid->CreaSearchCellsDetalle($id,'',$RubrosRol['movimiento']);
                            $Grid->CreaSearchRowsDetalle($id,"");
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
            
        }
?>

