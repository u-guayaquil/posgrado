<?php
         require_once("src/rules/nomina/servicio/ServicioRubrosRol.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/nomina/rubrosrol/render/RenderRubrosRol.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlRubrosRol
         {     private  $Sufijo; 
               private  $ServicioRubrosRol;
               private  $RenderRubrosRol;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioRubrosRol = new ServicioRubrosRol();
                        $this->RenderRubrosRol = new RenderRubrosRol($Sufijo);
               }

               function CargaRubrosRolBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaRubrosRolMantenimiento()
               {        echo $this->RenderRubrosRol->CreaRubrosRolMantenimiento();
               }

               function CargaRubrosRolSearchGrid()
               {        $prepareDQL = array('limite' => 50,'orden' => ' r.idmovimiento, r.ordenrol ','texto' => '');
                        $datoRubrosRol = $this->ServicioRubrosRol->BuscarRubrosRol($prepareDQL);
                        echo $this->RenderRubrosRol->CreaRubrosRolSearchGrid($datoRubrosRol); 
               }         
               
               function MuestraRubrosRolByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoRubrosRol = $this->ServicioRubrosRol->BuscarRubrosRol($prepareDQL);
                        return $this->RenderRubrosRol->MuestraRubrosRol($ajaxRespon,$datoRubrosRol);
               }

               function MuestraRubrosRolByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'orden' => ' r.idmovimiento, r.ordenrol ','texto' => strtoupper($texto));
                        $datoRubrosRol = $this->ServicioRubrosRol->BuscarRubrosRol($prepareDQL);
                        return $this->RenderRubrosRol->MuestraRubrosRolGrid($ajaxRespon,$datoRubrosRol);
               }

               function GuardaRubrosRol($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $RubrosRol  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioRubrosRol->GuardaDBRubrosRol($RubrosRol);
                        if (is_numeric($id)){
                            $function = (empty($RubrosRol->id) ? "MuestraRubrosRolGuardado" : "MuestraRubrosRolEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioRubrosRol->BuscarRubrosRol($prepareDQL);
                            return $this->RenderRubrosRol->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderRubrosRol->MuestraRubrosRolExcepcion($ajaxRespon,intval($id));
                        }                   
               } 
               
               function EliminaRubrosRol($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioRubrosRol->DesactivaRubrosRol(intval($id));
                        $Datos = $this->ServicioRubrosRol->BuscarRubrosRol($prepareDQL);
                        return $this->RenderRubrosRol->MuestraRubrosRolEliminado($ajaxRespon,$Datos);
                }
         }

?>

