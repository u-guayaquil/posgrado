<?php   require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         

        require_once("src/rules/nomina/servicio/ServicioEmpleadoCargas.php");
        require_once("src/modules/nomina/empleadocargas/render/RenderEmpleadoCargas.php");
        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/modules/general/empleado/render/RenderEmpleado.php");
        require_once("src/libs/clases/File.php");
        
        
         class ControlEmpleadoCargas
         {     private  $Sufijo; 
               private  $ServicioEmpleadoCargas;
               private  $ServicioEmpleado;
               private  $RenderEmpleadoCargas;
               private  $RenderEmpleado;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->ServicioEmpleadoCargas = new ServicioEmpleadoCargas();
                        $this->ServicioEmpleado = new ServicioEmpleado();
                        $this->RenderEmpleadoCargas = new RenderEmpleadoCargas($Sufijo);
                        $this->RenderEmpleado = new RenderEmpleado($Sufijo);
                        $this->Utils = new Utils();
                }

                function CargaEmpleadoCargasBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaEmpleadoCargasMantenimiento()
                {       echo $this->RenderEmpleadoCargas->CreaEmpleadoCargasMantenimiento();
                }

                function CargaEmpleadoCargasSearchGrid($IdEmpleado)
                {       $prepareDQL = array('empleado'=>$IdEmpleado);
                        $datoEmpleadoCargas = $this->ServicioEmpleadoCargas->BuscarEmpleadoCargas($prepareDQL);
                        echo $this->RenderEmpleadoCargas->CreaEmpleadoCargasSearchGrid($datoEmpleadoCargas); 
                }         
               
                function ConsultaByEmpleadoID($IdEmpleado)
                {       $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('empleado'=>$IdEmpleado);
                        $datoEmpleadoCargas = $this->ServicioEmpleadoCargas->BuscarEmpleadoCargas($prepareDQL);
                        return $this->RenderEmpleadoCargas->MuestraEmpleadoCargasBuscado($ajaxRespon,$datoEmpleadoCargas);                        
                }

                function GuardaEmpleadoCargas($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $EmpleadoCargas  = json_decode($Form)->Forma;
                        if (empty($EmpleadoCargas->id)){
                            $id = $this->ServicioEmpleadoCargas->Guarda($EmpleadoCargas);
                        }
                        else{
                            $id = $this->ServicioEmpleadoCargas->Edita($EmpleadoCargas);
                        }    
                        $Datos = $this->ServicioEmpleadoCargas->BuscarEmpleadoCargas(array('empleado'=>$EmpleadoCargas->idempleado));
                        return $this->RenderEmpleadoCargas->MuestraEmpleadoCargasGuardado($ajaxRespon,$id,$Datos);                            
                }                         

                function EliminaEmpleadoCargas($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioEmpleadoCargas->DesactivaEmpleadoCargas(intval($id));
                        $Datos = $this->ServicioEmpleadoCargas->BuscarEmpleadoCargas(array('id' => $id));
                        return $this->RenderEmpleadoCargas->MuestraEmpleadoCargasEliminado($ajaxRespon,$Datos);
                }
                        
                function CargaModalGridEmpleadoCargas($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');

                        if ($Operacion==="btempleado")
                        {   $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);                           
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Empleado";
                            $jsonModal['Carga'] = $this->RenderEmpleado->CreaModalGrid($Operacion,$Datos);                        
                            $jsonModal['Ancho'] = "563";
                        }    
                        elseif ($Operacion==="btdocumento")
                        {   $ruta = $this->Utils->Encriptar("src/utils/carga/carga.php");
                            $path = $this->Utils->Encriptar("src/upload/empleados");
                            $clss = $this->Utils->Encriptar("src/modules/nomina/empleadocargas/render/RenderEmpleadoCargas.php");
                            $type = $this->Utils->Encriptar("[pdf]");
                            $suff = $this->Utils->Encriptar($this->Sufijo);
                                
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Carga Documentos";
                            $jsonModal['Carga'] = "<iframe name='loadfile' src='guns.php?ruta=".$ruta."&clss=".$clss."&type=".$type."&path=".$path."&suff=".$suff."' height='100%' width='100%' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe>";
                            $jsonModal['Ancho'] = "300";
                        }
                        
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                         
                function ConsultaModalGridEmpleadoCargasByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);                        
                        $ajaxResp->alert($texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);
                        return $this->RenderEmpleado->MuestraModalGrid($ajaxResp,$Operacion,$Datos);      
                }
                
                function EliminaFileEmpleadoCargas($ruta)
                {       $FileManager = new File();                
                        $FileManager->DeleteFile($ruta);
                }
                
         }

?>

