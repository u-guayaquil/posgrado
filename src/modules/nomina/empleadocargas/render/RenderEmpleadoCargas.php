<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/rules/nomina/servicio/ServicioParentesco.php");

        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderEmpleadoCargas
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;  
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 4;                     
                }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
            function CreaEmpleadoCargasMantenimiento()
            {       $Search = new SearchInput($this->Sufijo);
                    $EmpleadoCargas = '<table border="0" class="Form-Frame" cellpadding="0">';
                    $EmpleadoCargas.= '       <tr height="30">';
                    $EmpleadoCargas.= '           <td class="Form-Label">Empleado</td>';
                    $EmpleadoCargas.= '           <td class="Form-Label" colspan="3">';
                    $EmpleadoCargas.=             $Search->TextSch("empleado","","")->Enabled(true)->Create("t13");
                    $EmpleadoCargas.= '           </td>';
                    $EmpleadoCargas.= '       </tr>';  
                    $EmpleadoCargas.= '       <tr height="30">';
                    $EmpleadoCargas.= '           <td class="Form-Label" style="width:20%">Id</td>';
                    $EmpleadoCargas.= '           <td class="Form-Label" style="width:30%">';
                    $EmpleadoCargas.= '               <input type="text" class="txt-input t02" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'txempleado:idempleado:nombrepariente:fenacimiento:sexo:parentesco:estado\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);" disabled/>';
                    $EmpleadoCargas.= '           </td>';                          
                    $EmpleadoCargas.= '           <td class="Form-Label" style="width:17%"></td>';
                    $EmpleadoCargas.= '           <td class="Form-Label" style="width:33%"></td>';
                    $EmpleadoCargas.= '       </tr>';
                    $EmpleadoCargas.= '         <tr height="30">';
                    $EmpleadoCargas.= '             <td class="Form-Label">Nombre</td>';
                    $EmpleadoCargas.= '             <td class="Form-Label" colspan="3">';
                    $EmpleadoCargas.= '                 <input type="text" class="txt-upper t14" id="nombrepariente'.$this->Sufijo.'" name="nombrepariente'.$this->Sufijo.'" value="" maxlength="60" disabled/>';
                    $EmpleadoCargas.= '             </td>';
                    $EmpleadoCargas.= '         </tr>'; 
                    
                    $EmpleadoCargas.= '         <tr height="30">';                       
                    $EmpleadoCargas.= '             <td class="Form-Label">F.Nacimiento</td>';
                    $EmpleadoCargas.= '             <td class="Form-Label"><input class="txt-upper t04" name = "fenacimiento'.$this->Sufijo.'" id = "fenacimiento'.$this->Sufijo.'" disabled/></td>';
                    $EmpleadoCargas.= '             <td class="Form-Label">Sexo</td>';
                    $EmpleadoCargas.= '             <td class="Form-Label">';
                    $EmpleadoCargas.= '                 <select id="sexo'.$this->Sufijo.'" name="sexo'.$this->Sufijo.'" class="sel-input s04" disabled>';
                    $EmpleadoCargas.= '                         <option value="M">MASCULINO</option>';
                    $EmpleadoCargas.= '                         <option value="F">FEMENINO</option>';
                    $EmpleadoCargas.= '                 </select>';
                    $EmpleadoCargas.= '             </td>';
                    $EmpleadoCargas.= '         </tr>';
                    
                    
                    $EmpleadoCargas.= '         <tr height="30">';  
                    $EmpleadoCargas.= '             <td class="Form-Label">Parentesco</td>';
                    $EmpleadoCargas.= '             <td class="Form-Label" style="width:36%">';
                    $EmpleadoCargas.=               $this->CreaComboParentesco();
                    $EmpleadoCargas.= '             </td>';
                    $EmpleadoCargas.= '             <td class="Form-Label">Estado</td>';
                    $EmpleadoCargas.= '             <td class="Form-Label" style="width:30%">';
                    $EmpleadoCargas.=               $this->CreaComboEstado();
                    $EmpleadoCargas.= '             </td>';
                    $EmpleadoCargas.= '         </tr>';

                    $EmpleadoCargas.= '         <tr height="30">';
                    $EmpleadoCargas.= '             <td class="Form-Label">Documentos</td>';
                    $EmpleadoCargas.= '             <td class="Form-Label" colspan="3">';
                    $EmpleadoCargas.=                   $Search->TextSch("documento","","")->Enabled(false)->Create("t13");
                    $EmpleadoCargas.= '             </td>';
                    $EmpleadoCargas.= '         </tr>';
                    
                    $EmpleadoCargas.= '</table>';
                    return $EmpleadoCargas;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']         = array('30px','center','');
                    $Columns['Empleado']   = array('0px','left','none');                        
                    $Columns['Nombre']   = array('250px','left','center');
                    $Columns['Parentesco'] = array('100px','left','');
                    $Columns['Estado']     = array('70px','left',''); 
                    $Columns['idEstado']   = array('0px','left','none');
                    $Columns['idEmpleado'] = array('0px','left','none');                        
                    $Columns['idParentesco'] = array('0px','left','none');
                    $Columns['fenac']        = array('0px','left','none');
                    $Columns['sexo']         = array('0px','left','none');
                    $Columns['docs']         = array('0px','left','none');
                    
                    return array('FindTxt'=>false,'AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
            }

            function CreaEmpleadoCargasSearchGrid($Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->EmpleadoCargasGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }
              
            private function EmpleadoCargasGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $cargo)
                    {       $fenacimiento = $this->Fechas->changeFormatDate($cargo['fenacimiento'],"DMY")[1];
                            $id = str_pad($cargo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['empleado']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['nombre']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['parentesco']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['txtestado'])); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['idestado']); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['idempleado']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['idparentesco']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fenacimiento);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['sexo']);
                            
                            $documento = "_";
                            if (trim($cargo['documentos'])!='')
                            $documento = trim($cargo['documentos']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento);
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,($cargo['idestado']==0 ? "red" : ""));
                    }
                    return $ObjSchGrid->CreaSearchTableDetalle(); 
            }

            function TraeDatosEstado($IdArray)
            {       $ServicioEstado = new ServicioEstado();
                    $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $datoEstado;
            }
              
            private function CreaComboEstado()
            {       $datosEstado= $this->TraeDatosEstado(array(0,1));
                    $Select = new ComboBox($this->Sufijo);                        
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
            }
              
            function TraeDatosParentesco()
            {       $ServicioParentesco = new ServicioParentesco();
                    $datoParentesco = $ServicioParentesco->BuscarParentesco();
                    return $datoParentesco;
            }
              
            private function CreaComboParentesco()
            {       $datosParentesco= $this->TraeDatosParentesco(array(0,1));
                    $Select = new ComboBox($this->Sufijo);                        
                    return $Select->Combo("parentesco",$datosParentesco)->Selected(1)->Create("s04");
            }
            
            function MuestraEmpleadoCargasGuardado($Ajax,$id,$EmpleadoCargas)
            {       if (count($EmpleadoCargas)>0)
                    {   $idx = str_pad($id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraEmpleadoCargasGrid($Ajax,$EmpleadoCargas);
                        return $this->RespuestaEmpleadoCargas($xAjax,"GUARDADO",$idx);
                    }
                    return $this->RespuestaEmpleadoCargas($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraEmpleadoCargasBuscado($Ajax,$EmpleadoCargas)
            {       $Ajax->call("BarButtonState",$this->Sufijo,"Default");
                    $xAjax = $this->MuestraEmpleadoCargasGrid($Ajax,$EmpleadoCargas);
                    return $xAjax;
            }

            function MuestraEmpleadoCargasGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->EmpleadoCargasGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }   

            function MuestraEmpleadoCargasEliminado($Ajax,$EmpleadoCargas)
            {       foreach ($EmpleadoCargas as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraEmpleadoCargasRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaEmpleadoCargas($xAjax,"ELIMINADO",$datos['idestado']);
                    }  
                    return $this->RespuestaEmpleadoCargas($xAjax,"EXCEPCION","No se eliminó la información.");
            }
            
            function MuestraEmpleadoCargasRowGrid($Ajax,$EmpleadoCargas,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($EmpleadoCargas['id'],$estado);
                    $fenacimiento = $this->Fechas->changeFormatDate($EmpleadoCargas['fenacimiento'],"DMY")[1];
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($EmpleadoCargas['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($EmpleadoCargas['empleado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($EmpleadoCargas['nombre']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($EmpleadoCargas['parentesco']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($EmpleadoCargas['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($EmpleadoCargas['idestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($EmpleadoCargas['idempleado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($EmpleadoCargas['idparentesco']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($fenacimiento));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",trim($EmpleadoCargas['sexo']));

                    $documento = "_";
                    if (trim($EmpleadoCargas['documentos'])!="")
                    $documento = trim($EmpleadoCargas['documentos']);
                    
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",$documento);
                    return $Ajax;
            }
            
            private function RespuestaEmpleadoCargas($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
            
            public function RutaDocumento($RutaDoc)
            {       $script = '<script language="javaScript" type="text/javascript">';
                    $script.= '        var obj = window.parent.document;';
                    $script.= '        var txt = obj.getElementById("txdocumento'.$this->Sufijo.'");';
                    $script.= '        txt.value = "'.$RutaDoc.'";';
                    $script.= '</script>';
                    $script.= '<div style="border:0px dotted #000; width:92%; padding:10px; font-family: tahoma; font-size:9pt">';
                    $script.= '     <b>El documento se subio correctamente.</b>';
                    $script.= '</div>';
                    return $script;
            }
            
        }
?>

