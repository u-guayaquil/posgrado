<?php   $Sufijo = '_EmpleadoCargas';
         
        require_once('src/modules/nomina/empleadocargas/controlador/ControlEmpleadoCargas.php');
        $ControlEmpleadoCargas = new ControlEmpleadoCargas($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlEmpleadoCargas,'GuardaEmpleadoCargas'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlEmpleadoCargas,'EliminaEmpleadoCargas'));         
        $xajax->register(XAJAX_FUNCTION,array('BuscaByEmpleadoID'.$Sufijo, $ControlEmpleadoCargas,'ConsultaByEmpleadoID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlEmpleadoCargas,'CargaModalGridEmpleadoCargas'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlEmpleadoCargas,'ConsultaModalGridEmpleadoCargasByTX')); 
        $xajax->register(XAJAX_FUNCTION,array('EliminaFile'.$Sufijo, $ControlEmpleadoCargas,'EliminaFileEmpleadoCargas'));         
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); 
                ?>
            <script type="text/javascript" src="src/modules/nomina/empleadocargas/js/JSEmpleadoCargas.js"></script>
        </head>
        <body>
        <div class="FormBasic" style="width:500px">
                <div class="FormSectionMenu">              
                <?php  $ControlEmpleadoCargas->CargaEmpleadoCargasBarButton($_GET['opcion']);
                        echo '<script type="text/javascript">';
                        echo '        BarButtonState(\''.$Sufijo.'\',"Inactive"); ';
                        echo '</script>';
                ?>
                </div>    
                <div class="FormSectionData">              
                        <form id="<?php echo 'Form'.$Sufijo; ?>">
                        <?php  $ControlEmpleadoCargas->CargaEmpleadoCargasMantenimiento();  ?>
                        </form>
                </div>    
                <div class="FormSectionGrid"> 
                    <?php echo $ControlEmpleadoCargas->CargaEmpleadoCargasSearchGrid(0);  ?>
                </div>  
        </div> 
        </body>
        </html>
              

