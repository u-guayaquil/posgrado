        $(document).ready(function()
        {      $('#fenacimiento_EmpleadoCargas').datetimepicker({
                        timepicker:false,
                        format: 'd/m/Y'
                });
        });

        function ButtonClick_EmpleadoCargas(Sufijo, Operacion)
        {       if (Operacion == 'addNew')
                {   BarButtonState(Sufijo, Operacion);
                    BarButtonStateDisabled(Sufijo, "btempleado");    
                    BarButtonStateEnabled(Sufijo, "btdocumento");    
                    ElementStatus(Sufijo, "nombrepariente:fenacimiento:sexo:parentesco:txdocumento", "idempleado:txempleado");
                    ElementClear(Sufijo, "id:nombrepariente:fenacimiento:txdocumento");
                    ElementSetValue(Sufijo, "estado", "1");
                }
                else if (Operacion == 'addMod')
                {   BarButtonState(Sufijo, Operacion);
                    BarButtonStateDisabled(Sufijo, "btempleado");
                    BarButtonStateEnabled(Sufijo, "btdocumento");    
                    if (ElementGetValue(Sufijo, "estado") == 0){
                        ElementStatus(Sufijo, "nombrepariente:fenacimiento:sexo:parentesco:estado:txdocumento", "idempleado:txempleado");
                    }else{   
                        ElementStatus(Sufijo, "nombrepariente:fenacimiento:sexo:parentesco:txdocumento", "idempleado:txempleado");
                    }                    
                } 
                else if (Operacion == 'addDel')
                {   if (ElementGetValue(Sufijo, "estado") == 1)
                    {   if (confirm("Desea inactivar este registro?"))
                        xajax_Elimina_EmpleadoCargas(ElementGetValue(Sufijo, "id"));
                    }else
                        alert("El registro se encuentra inactivo");
                
                }
                else if (Operacion == 'addSav')
                {   if (ElementValidateBeforeSave(Sufijo, "nombrepariente:fenacimiento"))
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        {   var Forma = PrepareElements(Sufijo, "id:idempleado:txempleado:nombrepariente:fenacimiento:sexo:parentesco:estado:txdocumento");
                            xajax_Guarda_EmpleadoCargas(JSON.stringify({Forma}));
                        }
                    }
                } 
                else if (Operacion == 'addCan')
                {   if (ElementGetValue(Sufijo, "id")=='')
                    {   var link = ElementGetValue(Sufijo, "txdocumento");
                        if (link!='')
                        xajax_EliminaFile_EmpleadoCargas(link);
                    }
                    BarButtonState(Sufijo, Operacion);
                    BarButtonStateEnabled(Sufijo, "btempleado");
                    BarButtonStateDisabled(Sufijo, "btdocumento");    
                    ElementStatus(Sufijo, "idempleado:txempleado", "nombrepariente:fenacimiento:sexo:parentesco:estado:txdocumento");
                    ElementClear(Sufijo, "id:nombrepariente:fenacimiento:txdocumento");
                }
                else
                {   xajax_CargaModal_EmpleadoCargas(Operacion);
                }
                return false;
        }

        function SearchGetData_EmpleadoCargas(Sufijo, Grilla)
        {       if (IsDisabled(Sufijo,"addSav"))
                {   BarButtonState(Sufijo, "Active");
                    document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                    document.getElementById("nombrepariente" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                    document.getElementById("sexo" + Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue;
                    document.getElementById("fenacimiento" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
                    document.getElementById("parentesco" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                    document.getElementById("estado" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                    
                    var documento = Grilla.cells[10].childNodes[0].nodeValue;
                    document.getElementById("txdocumento" + Sufijo).value = (documento=="_" ? "" : documento);
                }    
                return false;
        }

        function SearchByElement_EmpleadoCargas(Sufijo, Elemento)
        {       Elemento.value = TrimElement(Elemento.value);
                if (Elemento.id=="FindEmpleadoTextBx_EmpleadoCargas")
                {   xajax_BuscaModalByTX_EmpleadoCargas("btempleado",Elemento.value);
                } 
                return false;
        }        

        function SearchEmpleadoGetData_EmpleadoCargas(Sufijo, DatosGrid)
        {       ElementClear(Sufijo, "id:nombrepariente:fenacimiento");
                var Id = DatosGrid.cells[0].childNodes[0].nodeValue;
                document.getElementById("idempleado" + Sufijo).value = Id;
                document.getElementById("txempleado" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue.trim();
                xajax_BuscaByEmpleadoID_EmpleadoCargas(Id);    
                cerrar();
                return false;
        }

        function XAJAXResponse_EmpleadoCargas(Sufijo, Mensaje, Datos)
        {       if (Mensaje === "GUARDADO")
                {   BarButtonState(Sufijo, "Active");
                    BarButtonStateEnabled(Sufijo, "btempleado");
                    BarButtonStateDisabled(Sufijo, "btdocumento");
                    ElementStatus(Sufijo, "txempleado:idempleado", "nombrepariente:fenacimiento:sexo:parentesco:estado:txdocumento");
                    ElementSetValue(Sufijo, "id", Datos);
                    alert('Los datos se guardaron correctamente.');
                } 
                else if (Mensaje === 'ELIMINADO')
                {   ElementSetValue(Sufijo, "estado", Datos);
                    alert("Los Datos se eliminaron correctamente");
                }
                else if (Mensaje === 'NOEXISTEID')
                {   BarButtonState(Sufijo, 'Default');
                    ElementClear(Sufijo, "id");
                    alert(Datos);
                }
                else if (Mensaje === 'EXCEPCION')
                {   BarButtonState(Sufijo, "addNew");
                    alert(Datos);
                }
        }