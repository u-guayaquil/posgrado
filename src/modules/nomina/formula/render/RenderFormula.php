<?php       
    
    class RenderFormula
    {       private $Sufijo;
            
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
            }
        
            function CreaEditorFormulas($ServicioFormula,$Referencia="")
            {       $Medio = '<table class="Form-Frame" cellpadding="0" border="0">';
                    $Medio.= '      <tr height="28">';
                    $Medio.= '          <td class="Form-Label-Center" style="width:20%"><p class="p-txt-label">:: Funciones ::</p></td>';
                    $Medio.= '          <td class="Form-Label-Center" style="width:25%"><p class="p-txt-label">:: Variables ::</p></td>';
                    $Medio.= '          <td class="Form-Label-Center" style="width:25%"><p class="p-txt-label">:: Constantes ::</p></td>';
                    $Medio.= '          <td class="Form-Label-Center" style="width:30%"><p class="p-txt-label">:: Procesos ::</p></td>';
                    $Medio.= '      </tr>';            
                    $Medio.= '      <tr>';
                    $Medio.= '          <td class="Form-Label">';
                    $Medio.=                $this->CreaListaFormulas("F",$ServicioFormula->ArrayRolMethods("F"));
                    $Medio.= '          </td>';
                    $Medio.= '          <td class="Form-Label">';
                    $Medio.=                $this->CreaListaFormulas("V",$ServicioFormula->ArrayVariablesGeneral());
                    $Medio.= '          </td>';
                    $Medio.= '          <td class="Form-Label">';
                    $Medio.=                $this->CreaListaFormulas("C",array_keys($ServicioFormula->ArrayConstanteGeneral()));
                    $Medio.= '          </td>';
                    $Medio.= '          <td class="Form-Label">';
                    $Medio.=                $this->CreaListaFormulas("P",$ServicioFormula->ArrayRolMethods("P"));
                    $Medio.= '          </td>';
                    $Medio.= '      </tr>';            
                    $Medio.= '      <tr height="3">';
                    $Medio.= '          <td class="Form-Label" colspan="4"></td>';
                    $Medio.= '      </tr>';            
                    $Medio.= '      <tr>';
                    $Medio.= '          <td class="Form-Label" colspan="4">';
                    $Medio.= '              <table class="Form-Frame" cellpadding="0" border="0">';
                    $Medio.= '              <tr>';
                    $Medio.= '                     <td class="Form-Label" style="width:88%" rowspan="3">';
                    $Medio.= '                     <textarea class="txt-input" style="width: 98%; height: 50px; resize: none;" id="editor" name="editor" maxlength="550" onKeyDown="return SoloFormula(event); ">'.trim($Referencia).'</textarea>';
                    $Medio.= '                     </td>';
                    $Medio.= '              </tr>';            
                    $Medio.= '              <tr>';
                    $Medio.= '                     <td class="Form-Label" style="width:6%">';
                    $Medio.= '                     <input type="button" id="btEvalua'.$this->Sufijo.'" class="txt-boton-sintaxis" value="" onclick=" return EvaluaFormula'.$this->Sufijo.'(\''.$this->Sufijo.'\',editor); ">';
                    $Medio.= '                     </td>';
                    $Medio.= '              </tr>';            
                    $Medio.= '              <tr>';
                    $Medio.= '                     <td class="Form-Label" style="width:6%">';
                    $Medio.= '                     <input type="button" id="btAplica'.$this->Sufijo.'" class="txt-boton-aplicar" value="" onclick=" return AplicaFormula'.$this->Sufijo.'(\''.$this->Sufijo.'\',editor); ">';
                    $Medio.= '                     </td>';
                    $Medio.= '              </tr>';            
                    $Medio.= '              <tr height="28px">';
                    $Medio.= '                     <td class="Form-Label-Center" colspan="2"><p id="debug'.$this->Sufijo.'" class="p-txt-label-Red"></p></td>';                    
                    $Medio.= '              </tr>';            
                    $Medio.= '              </table>';
                    $Medio.= '          </td>';
                    $Medio.= '      </tr>';            
                    $Medio.= '</table>';
                    return $Medio;
            }
            
            function CreaListaFormulas($Tarea,$Datos)
            {       $Medio.= '<select class = "sel-input" multiple="0" style = "width:100%; height:110px" ondblclick=" return WriteWorkGridFila(\''.$Tarea.'\',this,editor); ">';
                                foreach ($Datos as $Elemento){                    
                    $Medio.= '  <option value='.$Elemento.'>'.$Elemento.'</option>';
                                }
                    $Medio.= '</select>';
                    return $Medio;
            }
            
            function MuestraResultado($xAjax,$Cierra,$Respuesta)
            {       if ($Respuesta[0]){    
                        if ($Cierra){
                            $xAjax->Assign("txformula".$this->Sufijo,"value",$Respuesta[1]);
                            $xAjax->Call("cerrar","");
                        }
                        else
                        $xAjax->Assign("debug".$this->Sufijo,"innerHTML","Formula Ok.");
                            
                    }else{
                        $xAjax->Assign("debug".$this->Sufijo,"innerHTML",$Respuesta[2]);
                    }    
                    return $xAjax;
            }
            
            private function Respuesta($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
    }        
            
?>