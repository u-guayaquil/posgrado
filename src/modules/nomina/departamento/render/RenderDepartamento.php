<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");

        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderDepartamento
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;  
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 2;                      
                }
               
                function CreaDepartamentoMantenimiento()
                {       $Departamento = '<table border="0" class="Form-Frame" cellpadding="0">';
                        $Departamento.= '        <tr height="30">';
                        $Departamento.= '            <td class="Form-Label" style="width:25%">Id</td>';
                        $Departamento.= '            <td class="Form-Label" style="width:75%">';
                        $Departamento.= '                <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $Departamento.= '            </td>';
                        $Departamento.= '        </tr>';
                        $Departamento.= '        <tr height="30">';
                        $Departamento.= '            <td class="Form-Label">Descripci&oacute;n</td>';
                        $Departamento.= '            <td class="Form-Label">';
                        $Departamento.= '                <input type="text" class="txt-upper t09" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="60" disabled/>';
                        $Departamento.= '            </td>';
                        $Departamento.= '        </tr>';
                        $Departamento.= '        </tr>';                       
                        $Departamento.= '        <tr height="30">';
                        $Departamento.= '            <td class="Form-Label">Estado</td>';
                        $Departamento.= '            <td class="Form-Label">';
                        $Departamento.=              $this->CreaComboEstado();
                        $Departamento.= '            </td>';
                        $Departamento.= '        </tr>';
                        $Departamento.= '</table>';
                        return $Departamento;
                }
              
                private function SearchGridValues()
                {       $Columns['Id']                 = array('30px','center','');
                        $Columns['Descripci&oacute;n'] = array('180px','left','');
                        $Columns['Creacion'] = array('70px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
                }

                function CreaDepartamentoSearchGrid($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->DepartamentoGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }
              
                private function DepartamentoGridHTML($ObjSchGrid,$Datos)
                {           foreach ($Datos as $departamento)
                            {       $fecreacion = $this->Fechas->changeFormatDate($departamento['fecreacion'],"DMY")[1];
                                    $id = str_pad($departamento['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($departamento['descripcion']));
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$departamento['txtestado']); 
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$departamento['idestado']); 
                                    $ObjSchGrid->CreaSearchRowsDetalle ($id,($departamento['idestado']==0 ? "red" : ""));

                            }
                            return $ObjSchGrid->CreaSearchTableDetalle(); 
                }                  
              
                private function CreaComboEstado()
                {          $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                           $Select = new ComboBox($this->Sufijo);                        
                           return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
                }              

                function TraeDatosEstadoByArray($IdArray)
                {          $ServicioEstado = new ServicioEstado();
                           $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                           return $datoEstado;
                }
              
                function MuestraDepartamento($Ajax,$Datos)
                {       foreach ($Datos as $Dato)
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));                     
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaDepartamento($Ajax,"NOEXISTEID","No existe un departamento con el ID ingresado.");
                }
            
                function MuestraDepartamentoGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->DepartamentoGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }     
               
                function MuestraDepartamentoGuardado($Ajax,$Departamento)
                {       if (count($Departamento)>0)
                        {   $id = str_pad($Departamento[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraDepartamentoGrid($Ajax,$Departamento);
                            return $this->RespuestaDepartamento($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaDepartamento($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraDepartamentoEditado($Ajax,$Departamento)
                {       foreach ($Departamento as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraDepartamentoRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaDepartamento($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaDepartamento($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
              
                function MuestraDepartamentoEliminado($Ajax,$Departamento)
                {       foreach ($Departamento as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraDepartamentoRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaDepartamento($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->RespuestaDepartamento($xAjax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraDepartamentoRowGrid($Ajax,$Departamento,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($Departamento['id'],$estado);
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Departamento['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Departamento['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($this->Fechas->changeFormatDate($Departamento['fecreacion'],"DMY")[1]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Departamento['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Departamento['idestado']));
                        return $Ajax;
                }

                function MuestraDepartamentoExcepcion($Ajax,$Msg)
                {       return $this->RespuestaDepartamento($Ajax,"EXCEPCION",$Msg);    
                }

                private function RespuestaDepartamento($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
                /**** Modal ****/
                private function SearchGridModalValues()
                {       $Columns['Id']                 = array('30px','center','');
                        $Columns['Departamento'] = array('180px','left','');
                        $Columns['Creacion']           = array('70px','center','');
                        $Columns['Estado']             = array('70px','left','');
                        $Columns['idEstado']           = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
                }
                
                function WidthModalGrid()
                {       return 380;
                }

                function CreaModalGridDepartamento($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalValues());
                        $ObjHTML = $this->DepartamentoGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }

                function MuestraModalGridDepartamento($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalValues());
                        $ObjHTML = $this->DepartamentoGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }
        }
?>

