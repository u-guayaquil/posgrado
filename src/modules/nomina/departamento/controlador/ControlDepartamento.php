<?php
         require_once("src/rules/nomina/servicio/ServicioDepartamento.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/nomina/departamento/render/RenderDepartamento.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlDepartamento
         {     private  $Sufijo; 
               private  $ServicioDepartamento;
               private  $RenderDepartamento;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioDepartamento = new ServicioDepartamento();
                        $this->RenderDepartamento = new RenderDepartamento($Sufijo);
               }

               function CargaDepartamentoBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaDepartamentoMantenimiento()
               {        echo $this->RenderDepartamento->CreaDepartamentoMantenimiento();
               }

               function CargaDepartamentoSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoDepartamento = $this->ServicioDepartamento->BuscarDepartamentoByDescripcion($prepareDQL);
                        echo $this->RenderDepartamento->CreaDepartamentoSearchGrid($datoDepartamento); 
               }         
               
               function MuestraDepartamentoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoDepartamento = $this->ServicioDepartamento->BuscarDepartamentoByID($prepareDQL);                        
                        return $this->RenderDepartamento->MuestraDepartamento($ajaxRespon,$datoDepartamento);
               }

               function MuestraDepartamentoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoDepartamento = $this->ServicioDepartamento->BuscarDepartamentoByDescripcion($prepareDQL);
                        return $this->RenderDepartamento->MuestraDepartamentoGrid($ajaxRespon,$datoDepartamento);
               }

               function GuardaDepartamento($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Departamento  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioDepartamento->GuardaDBDepartamento($Departamento);
                        if (is_numeric($id)){
                            $function = (empty($Departamento->id) ? "MuestraDepartamentoGuardado" : "MuestraDepartamentoEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioDepartamento->BuscarDepartamentoByID($prepareDQL);
                            return $this->RenderDepartamento->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderDepartamento->MuestraDepartamentoExcepcion($ajaxRespon,intval($id));
                        }                 
               } 
               
               function EliminaDepartamento($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioDepartamento->DesactivaDepartamento(intval($id));
                        $Datos = $this->ServicioDepartamento->BuscarDepartamentoByDescripcion($prepareDQL);
                        return $this->RenderDepartamento->MuestraDepartamentoEliminado($ajaxRespon,$Datos);
                }
         }

?>

