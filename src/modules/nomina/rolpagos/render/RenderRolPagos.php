<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/nomina/servicio/ServicioFrecuencia.php");
        require_once('src/rules/general/servicio/ServicioSucursal.php');
        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once('src/libs/clases/SearchInput.php');

        class RenderRolPagos
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Fechas = new DateControl();
                    $this->Maxlen['id'] = 6;                     
            }
              
            function CreaOpcionBarButton($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);
                    return $BarButton->CreaBarButton($Buttons);
            }
            
            function CreaMantenimiento()
            {       $Search = new SearchInput($this->Sufijo);   
                    $Select = new ComboBox($this->Sufijo);
                    
                    $ServicioSucursal = new ServicioSucursal();
                    $datosSucursal = $ServicioSucursal->BuscarSucursalByDescripcion(array('limite' => 50,'texto' => ''));
                    
                    $Rol = '<table class="Form-Frame" cellpadding="0" border="0">';
                    $Rol.= '       <tr height="28">';
                    $Rol.= '           <td class="Form-Label" style="width:97px">Rol.ID</td>';
                    $Rol.= '           <td class="Form-Label" style="width:97px">';
                    $Rol.=                 $Search->TextSch("rol","","")->Enabled(true)->MaxLength($this->Maxlen['id'])->ReadOnly(false)->Create("t02");
                    $Rol.= '           </td>';
                    $Rol.= '           <td class="Form-Label" style="width:97px"></td>';
                    $Rol.= '           <td class="Form-Label" style="width:97px"></td>';
                    $Rol.= '           <td class="Form-Label" style="width:97px"></td>';
                    $Rol.= '           <td class="Form-Label" style="width:97px"></td>';
                    $Rol.= '           <td class="Form-Label" style="width:90px"></td>';
                    $Rol.= '           <td class="Form-Label" style="width:108px"></td>';
                    $Rol.= '       </tr>';
                    $Rol.= '       <tr height="28">';
                    $Rol.= '           <td class="Form-Label">Detalle</td>';
                    $Rol.= '           <td class="Form-Label" colspan="3">';
                    $Rol.= '               <input type="text" class="txt-upper t11" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="100" disabled/>';
                    $Rol.= '           </td>';
                    $Rol.= '           <td class="Form-Label">Fecha</td>';
                    $Rol.= '           <td class="Form-Label">';
                    $Rol.= '               <input type="text" class="txt-center t03" id="fecha'.$this->Sufijo.'" name="fecha'.$this->Sufijo.'" value="" maxlength="10" disabled/>';
                    $Rol.= '           </td>';
                    $Rol.= '           <td class="Form-Label">Frecuencia</td>';
                    $Rol.= '           <td class="Form-Label">';
                    $Rol.=                 $this->CreaComboFrecuencia();
                    $Rol.= '           </td>';
                    $Rol.= '       </tr>';
                    $Rol.= '       <tr height="28">';
                    $Rol.= '           <td class="Form-Label">Plantilla</td>';
                    $Rol.= '           <td class="Form-Label" colspan="3">';
                    $Rol.=                 $Search->TextSch("plantilla","","")->Enabled(false)->Create("t10");                    
                    $Rol.= '           </td>';
                    $Rol.= '           <td class="Form-Label">Asiento</td>';
                    $Rol.= '           <td class="Form-Label" style="width:97px">';
                    $Rol.= '               <input type="text" class="txt-center t03" id="asiento'.$this->Sufijo.'" name="asiento'.$this->Sufijo.'" value="" maxlength="10" disabled readonly/>';
                    $Rol.= '           </td>';
                    $Rol.= '           <td class="Form-Label">Sucursal</td>';
                    $Rol.= '           <td class="Form-Label">';
                    $Rol.=                 $Select->Combo('sucursal',$datosSucursal)->Fields('id', 'descripcion')->Evento()->Create("s04",true);                                                                        
                    $Rol.= '           </td>';
                    $Rol.= '       </tr>';
                    $Rol.= '       <tr height="28">';
                    $Rol.= '           <td class="Form-Label">Medio pago</td>';
                    $Rol.= '           <td class="Form-Label" colspan="3">';
                    $Rol.=                 $Search->TextSch("mpago","","")->Enabled(false)->Create("t10");
                    $Rol.= '           </td>';
                    $Rol.= '           <td class="Form-Label">Estado</td>';
                    $Rol.= '           <td class="Form-Label">';
                    $Rol.=                 $this->CreaComboEstado(array(0,1));
                    $Rol.= '           </td>';
                    $Rol.= '           <td class="Form-Label"></td>';
                    $Rol.= '           <td class="Form-Label">';
                    $Rol.= '               <input type="button" id="addExe'.$this->Sufijo.'" value="Pre-Procesa" style="width:100px; height:27px" onclick=" return ButtonClick'.$this->Sufijo.'(\''.$this->Sufijo.'\',\'addExe\'); " >';
                    $Rol.='            </td>';
                    $Rol.= '       </tr>';
                    $Rol.= '</table>';
                    return $Rol;
            }
              
            private function CreaComboEstado($opciones)
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioEstado = new ServicioEstado();
                    $datosEstado = $ServicioEstado->BuscarEstadoByArrayID($opciones);
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
            }
              
            private function CreaComboFrecuencia()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioFrecuencia = new ServicioFrecuencia();
                    $datosFrecuencia = $ServicioFrecuencia->BuscarFrecuencia();
                       
                    return $Select->Combo("frecuencia",$datosFrecuencia)->Selected(4)->Create("s04");
            }
              
            function MuestraRolPagos($Ajax,$Datos)
            {       foreach ($Datos as $Dato)
                    {       $fecharol = $this->Fechas->changeFormatDate($Dato['fecharol'],"DMY")[1];
                    
                            $Ajax->Assign("idrol".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($Dato['descripcion'])); 
                            $Ajax->Assign("fecha".$this->Sufijo,"value", $fecharol);   
                            
                            $idasiento = str_pad($Dato['idasiento'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Ajax->Assign("asiento".$this->Sufijo,"value", ($Dato['idasiento']==0 ? "" : $idasiento));   
                            $Ajax->Assign("frecuencia".$this->Sufijo,"value", $Dato['idfrecuencia']);    
                            $Ajax->Assign("idplantilla".$this->Sufijo,"value",$Dato['idplantilla']);
                            $Ajax->Assign("txplantilla".$this->Sufijo,"value",trim($Dato['plantilla']));
                            $Ajax->Assign("sucursal".$this->Sufijo,"value",$Dato['idsucursal']);
                            $Ajax->Assign("idmpago".$this->Sufijo,"value", $Dato['idmediopago']); 
                            $Ajax->Assign("txmpago".$this->Sufijo,"value", trim($Dato['mediopago'])); 
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                            
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                    }
                    return $this->RespuestaRol($Ajax,"NOEXISTEID","No existe un Rol de Pago con el ID.");
            }

            function MuestraRolPagosDetalle($Ajax,$Datos)
            {       $sumprov = 0;
                    $sumingr = 0;
                    $sumegre = 0;
                    $sumtota = 0;
                    foreach ($Datos as $Dato)
                    {       $Celda[0] = str_pad($Dato['idempleado'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Celda[1] = trim($Dato['empleado']); //." ".trim($Dato['nombre']);
                            $Celda[2] = number_format($Dato['provision'],2,".",",");
                            $Celda[3] = number_format($Dato['ingreso'],2,".",",");
                            $Celda[4] = number_format($Dato['egreso'],2,".",",");
                            $Celda[5] = number_format($Dato['ingreso']-$Dato['egreso'],2,".",",");
                            $sumprov = $sumprov + $Dato['provision'];
                            $sumingr = $sumingr + $Dato['ingreso'];
                            $sumegre = $sumegre + $Dato['egreso'];                            
                            $sumtota = $sumtota + ($Dato['ingreso']-$Dato['egreso']);
                            $Ajax->call("CreaWorkGridFila","RL",$Celda);                                
                    }
                    return $Ajax;
            }
            
            function MuestraRolGuardado($xAjax,$IdRol)
            {       $id = str_pad($IdRol,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    return $this->RespuestaRol($xAjax,"GUARDADO",$id);
            }
                
            function MuestraRolEliminado($xAjax,$IdRol)
            {       $id = str_pad($IdRol,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                    return $this->RespuestaRol($xAjax,"ELIMINADO",0);
            }
            
            function MuestraRolExcepcion($Ajax,$Msg)
            {       return $this->RespuestaRol($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaRol($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
            
                
            /***** Modal *****/
            
            private function SearchGridModalConfig()
            {       $Columns['Id']         = array('40px','center','');
                    $Columns['Plantilla']  = array('277px','left','');
                    $Columns['Frecuencia'] = array('100px','left','');
                    $Columns['Creaci&oacute;n'] = array('80px','center','');
                    $Columns['Estado']          = array('70px','left','');
                    $Columns['IdFrecuencia']    = array('0px','left','none');
                    $Columns['IdEstado']        = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '120px','AltoRow' => '20px');
            }
            
            function WidthModalGrid()
            {       return 600;
            }
            
            function CreaModalGrid($Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $ObjHTML = $this->ModalGridDataHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }

            function MuestraModalGrid($Ajax,$Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $ObjHTML = $this->ModalGridDataHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }

            private function ModalGridDataHTML($Grid,$Datos)
            {       foreach ($Datos as $Dato)
                    {       $id = str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $fe = $this->Fechas->changeFormatDate($Dato['fecreacion'],"DMY")[1];
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['descripcion']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['frecuencia']));
                            $Grid->CreaSearchCellsDetalle($id,'',$fe);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Dato['txtestado']));
                            $Grid->CreaSearchCellsDetalle($id,'',$Dato['idfrecuencia']);
                            $Grid->CreaSearchCellsDetalle($id,'',$Dato['idestado']);
                            $Grid->CreaSearchRowsDetalle($id,"");
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
            
            //********************************************************************************************************
            
            private function WorkGridCellValues()
            {       $Search = new SearchInput($this->Sufijo);
                    $Columns[0] = array(50,'center','Id','');
                    $Columns[1] = array(262,'left','Empleado',''); 
                    $Columns[2] = array(100,'right','Provision','');
                    $Columns[3] = array(100,'right','Ingreso','');
                    $Columns[4] = array(100,'right','Egreso','');
                    $Columns[5] = array(100,'right','Total','');
                  //$Columns[6] = array(0,'left','IdDebe','');
                  //$Columns[7] = array(0,'left','IdHaber','');
                  //$Columns[8] = array(0,'left','TxFormula','');
                    return $Columns;
            }
            
            function CreaWorkGrid($Ajax,$Capa)                
            {       $WorkGrid["Capa"] = $Capa.$this->Sufijo;
                    $WorkGrid["Hcab"] = "30";
                    $WorkGrid["Hcob"] = "0";
                    $WorkGrid["Hdet"] = "200";
                    $WorkGrid["Sufy"] = $this->Sufijo;
                    $WorkGrid["Name"] = "RL";
                    $WorkGrid["Hrow"] = "24";
                    $WorkGrid["Erow"] = false;
                    $WorkGrid["btEd"] = true;
                    $WorkGrid["btDl"] = false;
                    $WorkGrid["Cell"] = $this->WorkGridCellValues();
                    $Ajax->call("CreaWorkGrid",json_encode($WorkGrid));
                    return $Ajax; 
            }        
            
        }
?>


