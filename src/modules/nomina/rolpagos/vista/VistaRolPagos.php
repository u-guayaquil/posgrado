<?php
        $Sufijo = '_RolPagos';
        require_once('src/modules/nomina/rolpagos/controlador/ControlRolPagos.php');
        $ControlRolPagos = new ControlRolPagos($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlRolPagos,'Guarda'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlRolPagos,'Elimina'));        
        $xajax->register(XAJAX_FUNCTION,array('BuscaByID'.$Sufijo, $ControlRolPagos,'ConsultaByID'));
        $xajax->register(XAJAX_FUNCTION,array('CargaRolpg'.$Sufijo, $ControlRolPagos,'CargaRolPago'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlRolPagos,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('CargaWorkGrid'.$Sufijo, $ControlRolPagos,'CargaWorkGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlRolPagos,'ConsultaGridByTX')); 
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/nomina/rolpagos/js/JSRolPagos.js"></script>
        </head>
        <body>
                <div class="FormBasic" style="width:800px">
                    <div class="FormSectionMenu">              
                        <?php   $ControlRolPagos->CargaBarButton($_GET['opcion']);
                                echo '<script type="text/javascript">';
                                echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                                echo '</script>';
                        ?>
                    </div>  
                    <form id="<?php echo 'Form'.$Sufijo; ?>">
                        <div class="FormSectionData">   
                             <?php  $ControlRolPagos->CargaMantenimiento();  ?>
                        </div> 
                        <div id="<?php echo 'WrkGrd'.$Sufijo; ?>" class="FormSectionGrid">  
                        </div>     
                    </form>
                </div>       
        </body>
        </html>
              

