<?php

    require('src/libs/plugins/pdf/fpdf.php');
    
    class MyDocument
    {       private $myDoc;
            private $MargnY = 15;
            private $MargnX = 15;
            private $Spacio = 5;
            private $MitadX;
            private $MitadY;
            
            private $FontDf = "ARIAL";
            private $FontWd = "";
            private $FontTm = "9";
        
            function __construct()
            {      ob_end_clean();        
                   $this->myDoc = new FPDF(); 
                   $this->myDoc->AddPage();       
                   $this->myDoc->SetFont($this->FontDf,$this->FontWd,$this->FontTm);
                   $this->MitadX = Round($this->myDoc->GetPageWidth()/2,0);
                   $this->MitadY = Round($this->myDoc->GetPageHeight()/2,0); 
            }
            
            function MyDoc()
            {       return $this->myDoc; 
            }
            
            function MyDocFont($Tama,$Weig) 
            {       $this->myDoc->SetFont($this->FontDf,$Weig,$Tama);
            }
            
            private function MyDocMarginBottom()
            {       return ($this->MitadY * 2)-$this->MargnY;
            }
            
            function BlankMyDocText($Linea)
            {       $PosY = $this->MargnY + ($Linea * $this->Spacio);
                    $PosX = $this->MargnX;
                    if ($PosY <= $this->MyDocMarginBottom())
                    {   $this->myDoc->Text($PosX,$PosY,"");
                        return $Linea+1;
                    }
                    else
                    {   $this->myDoc->AddPage();        
                        $PosY = $this->MargnY;
                        $this->myDoc->Text($PosX,$PosY,"");
                        return 1;
                    }
            }

            function CenterMyDocText($Linea,$Texto)
            {       $PosY = $this->MargnY + ($Linea * $this->Spacio);
                    $PosX = $this->MitadX - Round($this->myDoc->GetStringWidth($Texto)/2,0);
                    if ($PosY <= $this->MyDocMarginBottom())
                    {   $this->myDoc->Text($PosX,$PosY,$Texto);
                        return $Linea+1;
                    }
                    else
                    {   $this->myDoc->AddPage();        
                        $PosY = $this->MargnY;
                        $this->myDoc->Text($PosX,$PosY,$Texto);
                        return 1;
                    }
            }

            function RightMyDocText($Linea,$Texto,$LeftX = 0)
            {       $PosY = $this->MargnY + ($Linea * $this->Spacio);
                    $PosX = ($this->MitadX * 2) - $this->MargnX - $LeftX - Round($this->myDoc->GetStringWidth($Texto),0);
                    if ($PosY <= $this->MyDocMarginBottom())
                    {   $this->myDoc->Text($PosX,$PosY,$Texto);
                        return $Linea+1;
                    }
                    else
                    {   $this->myDoc->AddPage();        
                        $PosY = $this->MargnY;
                        $this->myDoc->Text($PosX,$PosY,$Texto);
                        return 1;
                    }
            }

            function LeftMyDocText($Linea,$Texto,$AddX=0)
            {       $PosY = $this->MargnY + ($Linea * $this->Spacio);
                    $PosX = $this->MargnX + $AddX;
                    if ($PosY <= $this->MyDocMarginBottom())
                    {   $this->myDoc->Text($PosX,$PosY,$Texto);
                        return $Linea+1;
                    }
                    else
                    {   $this->myDoc->AddPage();        
                        $PosY = $this->MargnY;
                        $this->myDoc->Text($PosX,$PosY,$Texto);
                        return 1;
                    }
            }
            
            function LeftLineMyDocText($Grz,$Linea,$Width,$AddX=0)
            {       $this->myDoc->SetLineWidth($Grz);
                    $PosY1 = $this->MargnY + ($Linea * $this->Spacio);
                    $PosY2 = $PosY1;
                    $PosX1 = $this->MargnX + $AddX;
                    $PosX2 = $PosX1 + $Width;
                    if ($PosY1 <= $this->MyDocMarginBottom())
                    {   $this->myDoc->Line($PosX1,$PosY1+1.5,$PosX2,$PosY2+1.5);
                        return $Linea+1;
                    }
                    else
                    {   $this->myDoc->AddPage();        
                        $PosY1 = $this->MargnY;
                        $PosY2 = $this->MargnY;
                        $this->myDoc->Line($PosX1,$PosY1+1.5,$PosX2,$PosY2+1.5);
                        return 1;
                    }
            }

            function RightLineMyDocText($Grz,$Linea,$Width,$LeftX = 0)
            {       $PosY1 = $this->MargnY + ($Linea * $this->Spacio);
                    $PosY2 = $PosY1;
                    $PosX2 = ($this->MitadX * 2) - $this->MargnX - $LeftX;
                    $PosX1 = $PosX2 - $Width;
                    if ($PosY1 <= $this->MyDocMarginBottom())
                    {   $this->myDoc->Line($PosX1,$PosY1+1.5,$PosX2,$PosY2+1.5);
                        return $Linea+1;
                    }
                    else
                    {   $this->myDoc->AddPage();        
                        $PosY1 = $this->MargnY;
                        $PosY2 = $this->MargnY;
                        $this->myDoc->Line($PosX1,$PosY1+1.5,$PosX2,$PosY2+1.5);
                        return 1;
                    }
            }
            
            function CreaMyDoc()
            {       $this->myDoc->Output();
            }
    }

    function SeccionSubTotales($PDF,$Ln,$Sm)
    {       if (count($Sm)>0)
            {         $PDF->MyDocFont(9,'B');
                      $PDF->LeftMyDocText($Ln,"TOTALES :",21);
                      $PDF->RightLineMyDocText("0.5",$Ln-1,22,100);
                      $PDF->RightLineMyDocText("0.5",$Ln-1,22,50);
                      $PDF->RightMyDocText($Ln,number_format($Sm[1]-$Sm[2],2,".",","),100);
                $Ln = $PDF->RightMyDocText($Ln,number_format($Sm[3],2,".",","),50);
                $Ln = $PDF->BlankMyDocText($Ln);
            }
            return $Ln;
    }

    if (isset($_GET['clss']) && isset($_GET['type']) && isset($_GET['path']) && isset($_GET['suff']))
    {   require_once("src/libs/clases/File.php");

        $date = new DateControl();
        $file = new File();
        
        $clss = $Utils->Desencriptar($_GET['clss']);
        $type = $Utils->Desencriptar($_GET['type']);
        $path = $Utils->Desencriptar($_GET['path']);
        $suff = $Utils->Desencriptar($_GET['suff']);
        $reff = $Utils->Desencriptar($_GET['reff']);
        
        if (strtolower($type)=="pdf")
        {   require_once($clss);    
            $clsn = $file->getFileParts($clss)[2];
            $Serv = new $clsn();
            $Datos = $Serv->BuscarRolPagosDetalle($reff);
            
            $MyDoc = new MyDocument();
            $Ln = 0;       //Linea
            $Q1 = 0;       //Quiebre 1
            $Q2 = 0;       //Quiebre 2
            $Sm = array(); //Sumador
            foreach($Datos as $Dato)
            {       if ($Ln==0)
                    {   $Ln = $MyDoc->RightMyDocText($Ln,$date->getNowDateTime("FECHAHORA"),0); 
                        $MyDoc->MyDocFont(11,'B');
                        $Ln = $MyDoc->CenterMyDocText($Ln,"ROL DE PAGOS ".trim($Dato['frecuencia'])); 
                        $MyDoc->MyDocFont(10,'B');
                        $Ln = $MyDoc->CenterMyDocText($Ln,"CORTE AL ".$date->changeFormatDate($Dato['fecharol'], "DMY")[1]);
                        $Ln = $MyDoc->BlankMyDocText($Ln);
                        $MyDoc->MyDocFont(9,'B');
                        $MyDoc->LeftMyDocText($Ln,"CONCEPTO :");
                        $MyDoc->MyDocFont(9,'');
                        $Ln = $MyDoc->LeftMyDocText($Ln,trim($Dato['txrol']),21);
                    }
                    if ($Q1!=$Dato['idempleado'])
                    {   $Ln = SeccionSubTotales($MyDoc,$Ln,$Sm);
                        $Q1 = $Dato['idempleado'];
                        $Sm[1] = 0;
                        $Sm[2] = 0;
                        $Sm[3] = 0;
                        $MyDoc->MyDocFont(9,'B');
                        $MyDoc->LeftMyDocText($Ln,"EMPLEADO :");
                        $MyDoc->MyDocFont(9,'');
                        $Ln = $MyDoc->LeftMyDocText($Ln,trim($Dato['empleado']),21);
                    }    
                    
                    if ($Q2!=$Dato['idmovmnto'])
                    {   $Q2 = $Dato['idmovmnto'];                     
                        $MyDoc->MyDocFont(9,'B');
                        if ($Dato['idmovmnto']==1)
                        {   $Ln = $MyDoc->LeftMyDocText($Ln,"INGRESOS :",21);
                            $Lf = 100;
                        }
                        elseif ($Dato['idmovmnto']==2)
                        {       $Ln = $MyDoc->LeftMyDocText($Ln,"EGRESOS :",21);                            
                                $Lf = 100;
                        }
                        elseif ($Dato['idmovmnto']==3)
                        {       $Ln = $MyDoc->LeftMyDocText($Ln,"PROVISION :",21);                            
                                $Lf = 50;
                        }
                    }    
                    $MyDoc->MyDocFont(9,'');
                    $MyDoc->LeftMyDocText($Ln,trim($Dato['txrubro']),21);
                    $Ln = $MyDoc->RightMyDocText($Ln, number_format($Dato['valor'],2,".",","),$Lf);
                    $Sm[$Dato['idmovmnto']] = $Sm[$Dato['idmovmnto']] + $Dato['valor'];
            }
            $Ln = SeccionSubTotales($MyDoc,$Ln,$Sm);
            $MyDoc->CreaMyDoc();
        }

        
    }    
    
    
?>