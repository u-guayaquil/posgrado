<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");   
        
        require_once("src/rules/nomina/servicio/ServicioRolPagos.php");
        require_once("src/modules/nomina/rolpagos/render/RenderRolPagos.php");        
        
        require_once("src/rules/nomina/servicio/ServicioPlantillaRol.php");
        require_once("src/modules/nomina/plantillarol/render/RenderPlantillaRol.php");
        
        require_once("src/rules/caja/servicio/ServicioMediosPago.php");
        require_once("src/modules/caja/medios_pago/render/RenderMediosPago.php");
        
        require_once("src/rules/nomina/servicio/ServicioFormula.php");
        require_once("src/modules/nomina/formula/render/RenderFormula.php");
        
        require_once("src/rules/nomina/servicio/ServicioRolMetodos.php");
        
        class ControlRolPagos
        {       private $Sufijo; 
                private $ServicioRolPagos;
                private $RenderRolPagos;
                private $ServicioPlantillaRol;
                private $RenderPlantillaRol;
                private $ServicioMediosPago;
                private $RenderMediosPago;
                private $ServicioFormula;
                private $RenderFormula;
                private $Utils;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->ServicioRolPagos = new ServicioRolPagos();
                        $this->RenderRolPagos = new RenderRolPagos($Sufijo);
                        $this->ServicioPlantillaRol =  new ServicioPlantillaRol();
                        $this->RenderPlantillaRol = new RenderPlantillaRol($Sufijo);
                        $this->ServicioMediosPago = new ServicioMediosPago();
                        $this->RenderMediosPago = new RenderMediosPago($Sufijo);
                        $this->ServicioFormula = new ServicioFormula();
                        $this->RenderFormula = new RenderFormula($this->Sufijo);
                        $this->Utils = new Utils();
                }

                function CargaBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaMantenimiento()
                {       echo $this->RenderRolPagos->CreaMantenimiento();
                }
                
                function CargaWorkGrid($Capa)
                {       $ajaxRespon = new xajaxResponse();    
                        return $this->RenderRolPagos->CreaWorkGrid($ajaxRespon,$Capa);
                }
               
                function ConsultaByID($id)
                {       $xAjax = new xajaxResponse();
                        $DatoC = $this->ServicioRolPagos->BuscarRolPagos(array('id' => intval($id)));
                        $DatoD = $this->ServicioRolPagos->BuscarRolPagosDetalleResumen(intval($id));
                        $xAjax = $this->RenderRolPagos->MuestraRolPagosDetalle($xAjax,$DatoD); 
                        return $this->RenderRolPagos->MuestraRolPagos($xAjax,$DatoC);
                }
                
                function CargaRolPago($idRl,$idFr,$idPl,$idSc)
                {       $xAjax = new xajaxResponse();
                        if (intval($idSc)!=0)  $TxPar['Sucursal']    = intval($idSc);
                        if (intval($idPl)!=0)  $TxPar['idplantilla'] = intval($idPl);
                        if (intval($idPl)==0)  $TxPar['idfrecuencia']= intval($idFr);                            
                        $Datos = $this->ServicioPlantillaRol->BuscarPlantillaRolEmpleado($TxPar);
                        $DatoR = $this->ServicioRolPagos->ProcesoCalculaRolPagos(intval($idRl),$Datos,new ServicioRolMetodos());
                        return $this->RenderRolPagos->MuestraRolPagosDetalle($xAjax,$DatoR);
                }
                
                function Guarda($Form) 
                {       $xAjax = new xajaxResponse();
                        $Forma  = json_decode($Form)->Forma;
                        $id = $this->ServicioRolPagos->GuardaDB($Forma); 
                        if (is_numeric($id)){
                            return $this->RenderRolPagos->MuestraRolGuardado($xAjax,$id);
                        }else{   
                            return $this->RenderRolPagos->MuestraRolExcepcion($xAjax,$id);
                        }        
                } 
               
                function Elimina($id)
                {       $xAjax = new xajaxResponse();
                        $this->ServicioRolPagos->DesactivaRolPagos($id);
                        return $this->RenderRolPagos->MuestraRolEliminado($xAjax,$id);
                }
                
                function CargaModalGrid($Operacion,$Referencia=0)
                {       $ajaxResp = new xajaxResponse();    
                        
                        if ($Operacion=="btrubro")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'estado' => '1','orden'=>' m.descripcion ');
                            $Datos = $this->ServicioRubrosRol->BuscarRubrosRol($prepareDQL);                            
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Rubros";
                            $jsonModal['Carga'] = $this->RenderRubrosRol->CreaModalGridRubrosRol($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderRubrosRol->WidthModalGrid();
                        }
                        elseif ($Operacion==="btplantilla")    
                        {   $prepareDQL = array('limite' => 50,'estado' => '1','frecuencia'=>intval($Referencia));
                            $ServicioPlantillaRol = new ServicioPlantillaRol();
                            $RenderPlantillaRol = new RenderPlantillaRol($this->Sufijo);
                            $Datos = $ServicioPlantillaRol->BuscarPlantillaRolByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Plantilla Rol";
                            $jsonModal['Carga'] = $RenderPlantillaRol->CreaModalGrid($Operacion,$Datos);
                            $jsonModal['Ancho'] = $RenderPlantillaRol->WidthModalGrid();
                        }   
                        elseif ($Operacion==="btmpago")    
                        {   if (intval($Referencia)!=0)
                                $prepareDQL = array('limite' => 50,'estado'=>'1', 'sucursal'=>$Referencia);
                            else    
                                $prepareDQL = array('limite' => 50,'estado'=>'1');
                            $Datos = $this->ServicioMediosPago->BuscarPagoByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Medio Pagos";
                            $jsonModal['Carga'] = $this->RenderMediosPago->CreaModalGridPago($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderMediosPago->WidthModalGrid();
                        }   
                        elseif ($Operacion==="addImp")
                        {   $ruta = $this->Utils->Encriptar("src/modules/nomina/rolpagos/reportes/ReportRolPagos.php");
                            $reff = $this->Utils->Encriptar($Referencia);
                            $path = "";     //$this->Utils->Encriptar("src/upload/empleados");
                            $clss = $this->Utils->Encriptar("src/rules/nomina/servicio/ServicioRolPagos.php");
                            $type = $this->Utils->Encriptar("pdf");
                            $suff = $this->Utils->Encriptar($this->Sufijo);

                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Rol de Pago";
                            $jsonModal['Carga'] = "<iframe name='report".$this->Sufijo."' src='guns.php?ruta=".$ruta."&clss=".$clss."&type=".$type."&path=".$path."&suff=".$suff."&reff=".$reff."' height='100%' width='100%' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe>";
                            $jsonModal['Ancho'] = "1000";
                        }
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaGridByTX($Operacion,$Texto,$Referencia=0)
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);                        
                        if ($Operacion==="btmpago")    
                        {   if (intval($Referencia)!=0)
                                $prepareDQL = array('limite' => 50, 'estado'=>'1', 'texto'=>strtoupper($texto), 'sucursal'=>$Referencia);
                            else    
                                $prepareDQL = array('limite' => 50, 'estado'=>'1', 'texto'=>strtoupper($texto));
                            $Datos = $this->ServicioMediosPago->BuscarPagoByDescripcion($prepareDQL);
                            return $this->RenderMediosPago->MuestraModalGridPago($ajaxResp,$Operacion,$Datos);                                        
                        }
                        else if ($Operacion=="btplantilla")    
                        {       $ServicioPlantillaRol = new ServicioPlantillaRol();
                                $RenderPlantillaRol = new RenderPlantillaRol($this->Sufijo);
                                $Datos = $ServicioPlantillaRol->BuscarPlantillaRolByDescripcion(array('limite' => 50,'frecuencia'=>intval($Referencia),'texto'=>strtoupper($texto)));
                                return $RenderPlantillaRol->MuestraModalGrid($ajaxResp,$Operacion,$Datos);         
                        }
                }
        }

?>

