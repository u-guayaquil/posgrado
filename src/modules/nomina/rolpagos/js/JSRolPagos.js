    var Wrk = "RL";
    
    $(document).ready(function()
    {       xajax_CargaWorkGrid_RolPagos("WrkGrd");              
            
            $('#fecha_RolPagos').datetimepicker({
                        timepicker:false,
                        format: 'd/m/Y'
            });        
    });
    var frmRolPagos = "idrol:descripcion:fecha:frecuencia:idplantilla:txplantilla:asiento:sucursal:idmpago:txmpago:estado";
    var newRolPagos = "descripcion:fecha:frecuencia:idplantilla:txplantilla:asiento:sucursal:idmpago:txmpago";
    var edtRolPagos = "descripcion:fecha:sucursal:idmpago:txmpago";
    var valRolPagos = "descripcion:fecha:idmpago:txmpago";
    var clsRolPagos = "descripcion:fecha:idplantilla:txplantilla:asiento:idmpago:txmpago";
    var btnRolPagos = "btplantilla:btmpago";
    
    function KeyDown_RolPagos(Evento, Sufijo, Elemento)
    {       
            if (Elemento.id == "idrol" + Sufijo)  
            {   Params = Dependencia_II(Evento,"NUM");
                if (Params[0])
                {   BarButtonState(Sufijo, "Default");
                    ElementClear(Sufijo,clsRolPagos);    
                    DeleteWorkGrid(Wrk);
                }   
                return Params[2];
            }
    }
    
    function ButtonClick_RolPagos(Sufijo, Operacion)
    {       
            if (Operacion=='addNew')
            {       BarButtonState(Sufijo,Operacion);
                    BarButtonStateEnabled(Sufijo, btnRolPagos);
                    ElementStatus(Sufijo,newRolPagos, "idrol:btrol");
                    ElementClear(Sufijo, clsRolPagos+":idrol");
                    ElementSetValue(Sufijo,"estado",1);
                    DeleteWorkGrid(Wrk);
            } 
            else if (Operacion=='addMod')
            {       BarButtonState(Sufijo, Operacion);
                    BarButtonStateEnabled(Sufijo, "btmpago");
                    if (ElementGetValue(Sufijo,"estado")==1)
                        ElementStatus(Sufijo, edtRolPagos, "idrol:btrol");
                    else
                        ElementStatus(Sufijo, edtRolPagos+":estado", "idrol:btrol");
            }
            else if (Operacion=='addDel')
            {       if (ElementGetValue(Sufijo, "estado")==1)
                    {   if (confirm("Desea inactivar este registro?"))
                        xajax_Elimina_RolPagos(ElementGetValue(Sufijo, "idrol"));
                    } 
                    else
                    alert("El registro se encuentra inactivo");
            } 
            else if (Operacion=='addSav')
            {       if (ElementValidateBeforeSave(Sufijo, valRolPagos))
                    {   if (ExistWorkGridRows(Wrk))
                        {   if (BarButtonState(Sufijo, "Inactive"))
                            {   var Forma = PrepareElements(Sufijo,frmRolPagos);
                                xajax_Guarda_RolPagos(JSON.stringify({Forma})); 
                            }
                        }
                        else
                        alert("Debe procesar datos para el Rol de Pagos");    
                    }
            }
            else if (Operacion=='addCan')
            {       BarButtonState(Sufijo, Operacion);
                    BarButtonStateDisabled(Sufijo, btnRolPagos);
                    ElementStatus(Sufijo, "idrol:btrol", newRolPagos+":estado");
                    ElementClear(Sufijo, clsRolPagos+":idrol");
                    DeleteWorkGrid(Wrk);
            } 
            else if (Operacion=='addImp')
            {       IdRef = ElementGetValue(Sufijo, "idrol"); 
                    xajax_CargaModal_RolPagos(Operacion,IdRef);
            }
            else if (Operacion=='addExe')
            {       var sigue = true;    
                    if (ExistWorkGridRows(Wrk))
                    sigue = confirm("Ya existen datos procesados, si vuelve a procesar\nse perderan los cambios. ¿Desea Continuar?");
                    if (sigue)
                    {   DeleteWorkGrid(Wrk);
                        var idRl = ElementGetValue(Sufijo, "idrol"); 
                        var idFr = ElementGetValue(Sufijo, "frecuencia"); 
                        var idPl = ElementGetValue(Sufijo, "idplantilla");
                        var idSc = ElementGetValue(Sufijo, "sucursal");
                        xajax_CargaRolpg_RolPagos(idRl,idFr,idPl,idSc);       
                    }    
            }
            else{ 
                    var IdRef = 0;
                    if (Operacion=="btsucursal")  IdRef = ElementGetValue(Sufijo, "sucursal");
                    if (Operacion=="btplantilla") IdRef = ElementGetValue(Sufijo, "frecuencia");
                    if (Operacion=="btmpago")     IdRef = ElementGetValue(Sufijo, "sucursal");
                    xajax_CargaModal_RolPagos(Operacion,IdRef);
            }
            return false;
    }
    
    function ControlSucursal_RolPagos(Sufijo, Elemento)
    {       if (ExistWorkGridRows(Wrk))
            {   DeleteWorkGrid(Wrk);
                alert("El cambio de sucursal obliga a pre-procesar nuevamente.\nSi desea revertir esta accion, cancele y vuelva a consultar.");
            }
            ElementSetValue(Sufijo, "idmpago","");
            ElementSetValue(Sufijo, "txmpago","");
            return false;
    }

    function SearchByElement_RolPagos(Sufijo, Elemento)
    {       Elemento.value = TrimElement(Elemento.value);
            if (Elemento.id=="idrol"+Sufijo)
            {   if (Elemento.value.length!=0)
                {   ContentFlag = document.getElementById("descripcion" + Sufijo);
                    if (ContentFlag.value.length == 0)
                    {   if (BarButtonState(Sufijo, "Inactive"))
                        xajax_BuscaByID_RolPagos(Elemento.value);
                    }
                }
                else
                BarButtonState(Sufijo,"Default");
            }
            else if (Elemento.id=="FindMpagoTextBx"+Sufijo)
            {       xajax_BuscaModalByTX_RolPagos("btmpago",Elemento.value,ElementGetValue(Sufijo, "sucursal"));
            }
            else if (Elemento.id=="FindPlantillaTextBx"+Sufijo)
            {       xajax_BuscaModalByTX_RolPagos("btplantilla",Elemento.value,ElementGetValue(Sufijo, "frecuencia"));
            }
            return false;    
    }

    function SearchPlantillaGetData_RolPagos(Sufijo, DatosGrid)
    {       document.getElementById("idplantilla" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txplantilla" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchMpagoGetData_RolPagos(Sufijo, Grilla)
    {       var documento = Grilla.cells[1].childNodes[0].nodeValue;
                documento = documento +" "+ Grilla.cells[3].childNodes[0].nodeValue;
                documento = documento +" "+ Grilla.cells[2].childNodes[0].nodeValue;
            var idmedpago = Grilla.cells[0].childNodes[0].nodeValue;   
            document.getElementById("idmpago" + Sufijo).value = idmedpago;
            document.getElementById("txmpago" + Sufijo).value = documento;
            cerrar();
            return false;
    }

    function WorkGridEditRL_RolPagos(Sufijo,Grilla,Celda)
    {       if (!IsDisabled(Sufijo,"addSav"))
            {   /*
                ElementSetValue(Sufijo,"idobjeto",Grilla.id);        
                ElementSetValue(Sufijo,"txrubro",Grilla.cells[0].childNodes[0].nodeValue);
                ElementSetValue(Sufijo,"txcuentadebe",Grilla.cells[2].childNodes[0].nodeValue);
                ElementSetValue(Sufijo,"txcuentahaber",Grilla.cells[3].childNodes[0].nodeValue);
                ElementSetValue(Sufijo,"iddetalle",Grilla.cells[4].childNodes[0].nodeValue);
                ElementSetValue(Sufijo,"idrubro",Grilla.cells[5].childNodes[0].nodeValue);
                ElementSetValue(Sufijo,"idcuentadebe",Grilla.cells[6].childNodes[0].nodeValue);
                ElementSetValue(Sufijo,"idcuentahaber",Grilla.cells[7].childNodes[0].nodeValue);        
                ElementSetValue(Sufijo,"txformula",Grilla.cells[8].childNodes[0].nodeValue);
                */
                alert("Si");           
            }    
            return false;
    }
    
    function XAJAXResponse_RolPagos(Sufijo, Mensaje, Datos)
    {       if (Mensaje=="GUARDADO")
            {   BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, btnRolPagos);
                ElementSetValue(Sufijo, "idrol", Datos);
                ElementStatus(Sufijo,"idrol:btrol",frmRolPagos + ":estado");
                alert('Los datos se guardaron correctamente.');
            } 
            else if (Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
                alert("Los datos se eliminaron correctamente");
            } 
            else if (Mensaje === 'NOEXISTEID')
            {   BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "idrol");
                alert(Datos);
            } 
            else if (Mensaje === 'EXCEPCION')
            {   BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
    }

