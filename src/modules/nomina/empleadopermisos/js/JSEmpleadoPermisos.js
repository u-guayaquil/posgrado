function CargaFecha_EmpleadoPermisos(Sufijo){
    $('#fedesde'+Sufijo).datetimepicker({
            timepicker:false,
            format: 'd/m/Y'
    });
    $('#fehasta'+Sufijo).datetimepicker({
            timepicker:false,
            format: 'd/m/Y'
    });
    
     $("#hdesde"+Sufijo).mask("99:99");
     $("#hhasta"+Sufijo).mask("99:99");
}             

function ButtonClick_EmpleadoPermisos(Sufijo, Operacion)
{
    var objEmpleadoPermisos = "id:txempleado:idempleado:tipopermiso:motivo:fedesde:fehasta:hdesde:hhasta:remunerado"
                             +":detalle:estadopermiso:estado";
    var frmEmpleadoPermisos = "txempleado:idempleado:txreemplazo:idreemplazo:tipopermiso:motivo:fedesde:fehasta:hdesde:hhasta:remunerado"
                             +":detalle:estadopermiso:observaciones:estado";

    if (Operacion == 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, frmEmpleadoPermisos, "id:estado");
        BarButtonStateEnabled(Sufijo, "btempleado:btreemplazo");
        ElementClear(Sufijo, objEmpleadoPermisos);
        CargaFecha_EmpleadoPermisos(Sufijo);
    } else if (Operacion == 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, frmEmpleadoPermisos, "id");
        ElementStatus(Sufijo, "", "txempleado:detalle:fedesde:fehasta:hdesde:hhasta:motivo:tipopermiso");
        if(ElementGetValue(Sufijo,"estado")==='0'){
            ElementStatus(Sufijo, "estado", "");
        }else{
            ElementStatus(Sufijo, "", "estado");
        }
        CargaFecha_EmpleadoPermisos(Sufijo);
        BarButtonStateEnabled(Sufijo, "btreemplazo");
    } else if (Operacion == 'addDel')
    {
        if (ElementGetValue(Sufijo, "estado") == 1)
        {
            if (confirm("Desea inactivar este registro?"))
                xajax_Elimina_EmpleadoPermisos(ElementGetValue(Sufijo, "id"));
        } else
            alert("El registro se encuentra inactivo");
    } else if (Operacion == 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, "detalle"))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {
                var Forma = PrepareElements(Sufijo, frmEmpleadoPermisos+':id');
                xajax_Guarda_EmpleadoPermisos(JSON.stringify({Forma}));
            }
        }
    } else if (Operacion == 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, "id", frmEmpleadoPermisos);
        ElementClear(Sufijo, objEmpleadoPermisos);
        BarButtonStateDisabled(Sufijo, "btempleado:btreemplazo");
    } else if (Operacion == 'addImp')
    {  
        var id = document.getElementById('id_EmpleadoPermisos').value;
        xajax_PdfPermiso_EmpleadoPermisos(id);
        return false;
    } else {
        xajax_CargaModal_EmpleadoPermisos(Operacion);
    }
    return false;
}

function SearchByElement_EmpleadoPermisos(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    if (Elemento.name == "id" + Sufijo)
    {
        if (Elemento.value.length != 0)
        {
            ContentFlag = document.getElementById("observaciones" + Sufijo);
            if (ContentFlag.value.length == 0)
            {
                if (BarButtonState(Sufijo, "Inactive")){
                    xajax_MuestraByID_EmpleadoPermisos(Elemento.value);
                }
            }
        } else
            BarButtonState(Sufijo, "Default");
    } else
    {
        xajax_MuestraByTX_EmpleadoPermisos(Elemento.value);
    }
    
}

function validaEstado(Sufijo) {
        BarButtonStateDisabled(Sufijo, "addMod");
}

function SearchGetData_EmpleadoPermisos(Sufijo, Grilla)
{   if (IsDisabled(Sufijo,"addSav")) {
            BarButtonState(Sufijo, "Active");

            document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
            document.getElementById("idempleado" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
            document.getElementById("txempleado" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
            document.getElementById("tipopermiso" + Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
            document.getElementById("motivo" + Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue;
            document.getElementById("fedesde" + Sufijo).value = Grilla.cells[17].childNodes[0].nodeValue;
            document.getElementById("hdesde" + Sufijo).value = Grilla.cells[18].childNodes[0].nodeValue;
            document.getElementById("fehasta" + Sufijo).value = Grilla.cells[19].childNodes[0].nodeValue;
            document.getElementById("hhasta" + Sufijo).value = Grilla.cells[20].childNodes[0].nodeValue;
            if(Grilla.cells[11].childNodes[0].nodeValue !== '0'){
                document.getElementById("idreemplazo" + Sufijo).value = Grilla.cells[11].childNodes[0].nodeValue;
                document.getElementById("txreemplazo" + Sufijo).value = Grilla.cells[12].childNodes[0].nodeValue;
            }else{
                document.getElementById("idreemplazo" + Sufijo).value = '';
                document.getElementById("txreemplazo" + Sufijo).value = '';
            }
            document.getElementById("remunerado" + Sufijo).value = Grilla.cells[13].childNodes[0].nodeValue;
            document.getElementById("detalle" + Sufijo).value = Grilla.cells[14].childNodes[0].nodeValue;
            document.getElementById("estadopermiso" + Sufijo).value = Grilla.cells[15].childNodes[0].nodeValue;
            if(Grilla.cells[16].childNodes[0]){
                document.getElementById("observaciones" + Sufijo).value = Grilla.cells[16].childNodes[0].nodeValue;
            }else{
                document.getElementById("observaciones" + Sufijo).value = '';
            }            
            document.getElementById("estado" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
            if(Grilla.cells[15].childNodes[0].nodeValue.trim() === '4'){
                BarButtonStateDisabled(Sufijo,"addMod");
            }                
    }
    return false;
}

function SearchEmpleadoGetData_EmpleadoPermisos(Sufijo, DatosGrid)
{
    document.getElementById("idempleado" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txempleado" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue.trim();
    cerrar();
    return false;
}

function SearchReemplazoGetData_EmpleadoPermisos(Sufijo, DatosGrid)
{
    document.getElementById("idreemplazo" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txreemplazo" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue.trim();
    cerrar();
    return false;
}

function XAJAXResponse_EmpleadoPermisos(Sufijo, Mensaje, Datos)
{
    var objEmpleadoPermisos = "txempleado:idempleado:txreemplazo:idreemplazo:tipopermiso:motivo:fedesde:fehasta:hdesde:hhasta:remunerado"
                             +":detalle:estadopermiso:observaciones:estado";
    if (Mensaje === "GUARDADO")
    {
        BarButtonState(Sufijo, "Active");
        BarButtonStateDisabled(Sufijo, "btreemplazo:btempleado");
        ElementSetValue(Sufijo, "id", Datos);
        ElementStatus(Sufijo, "id", objEmpleadoPermisos);
        alert('Los datos se guardaron correctamente.');
    } else if (Mensaje === 'ELIMINADO')
    {
        ElementSetValue(Sufijo, "estado", Datos);
        alert("Los Datos se eliminaron correctamente");
    } else if (Mensaje === 'NOEXISTEID')
    {
        BarButtonState(Sufijo, 'Default');
        ElementClear(Sufijo, "id");
        alert(Datos);
    } else if (Mensaje === 'EXCEPCION')
    {
        BarButtonState(Sufijo, "addNew");
        alert(Datos);
    }
}

function pdfPermiso() {
    if(document.getElementById('addSav_EmpleadoPermisos').disabled === false){
        var id = document.getElementById('id_EmpleadoPermisos').value;
        xajax_PdfPermiso_EmpleadoPermisos(id);
        return false;
    }  else{
        return false;
    } 
}

function LlenarDivOculto_EmpleadoPermisos() {
    document.getElementById('botonpermiso').style.display='none'
    window.print();
}