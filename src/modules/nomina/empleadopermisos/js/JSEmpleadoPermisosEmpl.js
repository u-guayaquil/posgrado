function CargaFecha_SolicitudPermisos(Sufijo) {
    $('#fedesde' + Sufijo).datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });
    $('#fehasta' + Sufijo).datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });
    $("#hdesde" + Sufijo).mask("99:99");
    $("#hhasta" + Sufijo).mask("99:99");
}

function ButtonClick_SolicitudPermisos(Sufijo, Operacion)
{
    var objSolicitudPermisos = "id:txempleado:idempleado:tipopermiso:motivo:fedesde:fehasta:hdesde:hhasta:remunerado"
            + ":detalle:estadopermiso:observaciones:estado:idreemplazo";
    var visSolicitudPermisos = "id:txempleado:fedesde:fehasta:hdesde:hhasta:detalle:estado";
    var frmSolicitudPermisos = "txempleado:idempleado:tipopermiso:motivo:fedesde:fehasta:hdesde:hhasta:remunerado"
            + ":detalle:estadopermiso:observaciones:estado:idreemplazo";

    if (Operacion == 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, objSolicitudPermisos, "id:txempleado:estado");
        BarButtonStateEnabled(Sufijo, "btempleado");
        ElementClear(Sufijo, visSolicitudPermisos);
        CargaFecha_SolicitudPermisos(Sufijo);
        traeEmpleado();
    } else if (Operacion == 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, objSolicitudPermisos, "id:txempleado");
        CargaFecha_SolicitudPermisos(Sufijo);
        BarButtonStateEnabled(Sufijo, "btempleado");
        if(ElementGetValue(Sufijo,"estado")==='0'){
            ElementStatus(Sufijo, "estado", "");
        }else{
            ElementStatus(Sufijo, "", "estado");
        }
    } else if (Operacion == 'addDel')
    {
        if (ElementGetValue(Sufijo, "estado") == 1)
        {
            if (confirm("Desea inactivar este registro?"))
                xajax_Elimina_SolicitudPermisos(ElementGetValue(Sufijo, "id"));
        } else
            alert("El registro se encuentra inactivo");
    } else if (Operacion == 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, "detalle"))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {
                var Forma = PrepareElements(Sufijo, objSolicitudPermisos);
                xajax_Guarda_SolicitudPermisos(JSON.stringify({Forma}));
            }
        }
    } else if (Operacion == 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, "id", frmSolicitudPermisos);
        ElementClear(Sufijo, visSolicitudPermisos);
        BarButtonStateDisabled(Sufijo, "btempleado:btreemplazo");
    } else if (Operacion == 'addImp')
    {
        var id = document.getElementById('id' + Sufijo).value;
        xajax_PdfPermiso_SolicitudPermisos(id);
        return false;
    } else {
        xajax_CargaModal_SolicitudPermisos(Operacion);
    }
    return false;
}

function validaEstado(Sufijo) {
        BarButtonStateDisabled(Sufijo, "addMod");
}

function SearchByElement_SolicitudPermisos(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    if (Elemento.name == "id" + Sufijo)
    {
        if (Elemento.value.length != 0)
        {
            ContentFlag = document.getElementById("observaciones" + Sufijo);
            if (ContentFlag.value.length == 0)
            {
                if (BarButtonState(Sufijo, "Inactive"))
                    xajax_MuestraByID_SolicitudPermisos(Elemento.value);
            }
        } else
            BarButtonState(Sufijo, "Default");
    } else
    {
        xajax_MuestraByTX_SolicitudPermisos(Elemento.value);
    }
}

function SearchGetData_SolicitudPermisos(Sufijo, Grilla)
{
    if (IsDisabled(Sufijo, "addSav")) {
        BarButtonState(Sufijo, "Active");

        document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("idempleado" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
        document.getElementById("txempleado" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
        document.getElementById("motivo" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
        document.getElementById("fedesde" + Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
        document.getElementById("hdesde" + Sufijo).value = Grilla.cells[11].childNodes[0].nodeValue;
        document.getElementById("fehasta" + Sufijo).value = Grilla.cells[12].childNodes[0].nodeValue;
        document.getElementById("hhasta" + Sufijo).value = Grilla.cells[13].childNodes[0].nodeValue;
        document.getElementById("detalle" + Sufijo).value = Grilla.cells[9].childNodes[0].nodeValue;
        document.getElementById("estado" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
            if(Grilla.cells[14].childNodes[0].nodeValue.trim() === '4'){
                BarButtonStateDisabled(Sufijo,"addMod");
            }                
    }
    return false;
}

function SearchEmpleadoGetData_SolicitudPermisos(Sufijo, DatosGrid)
{
    document.getElementById("idempleado" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txempleado" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue.trim();
    cerrar();
    return false;
}

function SearchReemplazoGetData_SolicitudPermisos(Sufijo, DatosGrid)
{
    document.getElementById("idreemplazo" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txreemplazo" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue.trim();
    cerrar();
    return false;
}

function XAJAXResponse_SolicitudPermisos(Sufijo, Mensaje, Datos)
{
    var objSolicitudPermisos = "txempleado:idempleado:txreemplazo:idreemplazo:tipopermiso:motivo:fedesde:fehasta:hdesde:hhasta:remunerado"
            + ":detalle:estadopermiso:observaciones:estado";
    if (Mensaje === "GUARDADO")
    {
        BarButtonState(Sufijo, "Active");
        BarButtonStateDisabled(Sufijo, "btreemplazo:btempleado");
        ElementSetValue(Sufijo, "id", Datos);
        ElementStatus(Sufijo, "id", objSolicitudPermisos);
        alert('Los datos se guardaron correctamente.');
    } else if (Mensaje === 'ELIMINADO')
    {
        ElementSetValue(Sufijo, "estado", Datos);
        alert("Los Datos se eliminaron correctamente");
    } else if (Mensaje === 'NOEXISTEID')
    {
        BarButtonState(Sufijo, 'Default');
        ElementClear(Sufijo, "id");
        alert(Datos);
    } else if (Mensaje === 'EXCEPCION')
    {
        BarButtonState(Sufijo, "addNew");
        alert(Datos);
    }
}

function pdfPermiso() {
    if (document.getElementById('addSav_SolicitudPermisos').disabled === false) {

        xajax_PdfPermiso_SolicitudPermisos(id);
        return false;
    } else {
        return false;
    }
}

function traeEmpleado() {
    xajax_TraeEmpleado_SolicitudPermisos();
    return false;

}

function LlenarDivOculto_SolicitudPermisos() {
    document.getElementById('botonpermiso').style.display = 'none'
    window.print();
}