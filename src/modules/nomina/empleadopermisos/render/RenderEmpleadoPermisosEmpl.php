<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/rules/nomina/servicio/ServicioParentesco.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");        
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderEmpleadoPermisosEmpl
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 4;                     
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaEmpleadoPermisosMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);
                       $frmEmpleadoPermisos = "txempleado:idempleado:txreemplazo:idreemplazo:tipopermiso:motivo:fedesde:fehasta:hdesde:hhasta:remunerado"
                                             .":detalle:estadopermiso:observaciones:estado";
                       $EmpleadoPermisos = '<table class="Form-Frame" cellpadding="0">';
                       $EmpleadoPermisos.= '         <tr height="30">';
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:1%">Id</td>';
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:5%">';
                       $EmpleadoPermisos.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\''.$frmEmpleadoPermisos.'\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       //$EmpleadoPermisos.= '                 <input id="btn_pdfpermiso_'.$this->Sufijo.'" src="src/public/img/valido.png" type="image" name="pdfpermiso'.$this->Sufijo.'" onclick="return pdfPermiso();"  title="Imprimir"/>';
                       $EmpleadoPermisos.= '           </td>'; 
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:10%">Fecha Desde</td>';
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:20%">'
                                                        . '<input class="txt-upper t03" name = "fedesde'.$this->Sufijo.'" id = "fedesde'.$this->Sufijo.'" disabled/> &nbsp;- &nbsp;'
                                                        . '<input class="txt-upper t02" name = "hdesde'.$this->Sufijo.'" id = "hdesde'.$this->Sufijo.'" disabled/></td>';                                     
                       $EmpleadoPermisos.= '         </tr>';
                       $EmpleadoPermisos.= '         <tr height="30">';
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:1%">Empleado</td>';
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:5%">';
                       $EmpleadoPermisos.= '                 <input type="text" class="txt-upper t11" id="txempleado'.$this->Sufijo.'" name="txempleado'.$this->Sufijo.'" value="" maxlength="40" disabled/>';
                       $EmpleadoPermisos.= '                 <input type="hidden" id="idempleado'.$this->Sufijo.'" name="idempleado'.$this->Sufijo.'" value="">';
                       $EmpleadoPermisos.= '                 <input type="hidden" id="tipopermiso'.$this->Sufijo.'" name="tipopermiso'.$this->Sufijo.'" value="S">';
                       $EmpleadoPermisos.= '                 <input type="hidden" id="estadopermiso'.$this->Sufijo.'" name="estadopermiso'.$this->Sufijo.'" value="3">';
                       $EmpleadoPermisos.= '                 <input type="hidden" id="remunerado'.$this->Sufijo.'" name="remunerado'.$this->Sufijo.'" value="N">';
                       $EmpleadoPermisos.= '                 <input type="hidden" id="txreemplazo'.$this->Sufijo.'" name="txreemplazo'.$this->Sufijo.'" value="">';
                       $EmpleadoPermisos.= '                 <input type="hidden" id="idreemplazo'.$this->Sufijo.'" name="idreemplazo'.$this->Sufijo.'" value="0">';
                       $EmpleadoPermisos.= '                 <input type="hidden" id="observaciones'.$this->Sufijo.'" name="observaciones'.$this->Sufijo.'" value="">';
                       $EmpleadoPermisos.= '             </td>';
                       $EmpleadoPermisos.= '           </td>';
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:10%">Fecha Hasta</td>';
                       $EmpleadoPermisos.= '            <td class="Form-Label" style="width:20%">'
                                                        . '<input class="txt-upper t03" name = "fehasta'.$this->Sufijo.'" id = "fehasta'.$this->Sufijo.'" disabled/> &nbsp;- &nbsp;'
                                                        . '<input class="txt-upper t02" name = "hhasta'.$this->Sufijo.'" id = "hhasta'.$this->Sufijo.'" disabled/></td>';                        
                       $EmpleadoPermisos.= '         </tr>'; 
                       $EmpleadoPermisos.= '         <tr height="30">';  
                       $EmpleadoPermisos.= '             <td class="Form-Label" style="width:1%" rowspan="2">Detalle</td>';
                       $EmpleadoPermisos.= '              <td class="Form-Label" style="width:5%" rowspan="2">';
                       $EmpleadoPermisos.= '                 <textarea id="detalle'.$this->Sufijo.'" name="detalle'.$this->Sufijo.'" rows="2" cols="35" disabled></textarea>';
                       $EmpleadoPermisos.= '              </td>';  
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:1%">Motivo</td>'; 
                       $EmpleadoPermisos.= '           <td class="Form-Label" style="width:20%">';
                                                                $EmpleadoPermisos.= $this->CreaComboMotivo();
                       $EmpleadoPermisos.= '           </td>';     
                       $EmpleadoPermisos.= '         </tr>';
                       $EmpleadoPermisos.= '         <tr height="30">';  
                       $EmpleadoPermisos.= '             <td class="Form-Label" style="width:10%" >Estado</td>';
                       $EmpleadoPermisos.= '             <td class="Form-Label" style="width:20%">';
                                                                $EmpleadoPermisos.= $this->CreaComboEstado();
                       $EmpleadoPermisos.= '             </td>'; 
                       $EmpleadoPermisos.= '         </tr>';
                       $EmpleadoPermisos.= '</table>';
                       return $EmpleadoPermisos;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Empleado'] = array('200px','left','');
                        $Columns['Motivo'] = array('100px','left','center');
                        $Columns['Fecha Desde']   = array('130px','center','');
                        $Columns['Fecha Hasta']   = array('130px','center','');
                        $Columns['Estado']   = array('70px','left','center'); 
                        $Columns['idEstado']   = array('0px','left','none');
                        $Columns['idEmpleado']   = array('0px','left','none');                        
                        $Columns['idmotivo']   = array('0px','left','none');
                        $Columns['detalle']   = array('0px','left','none');
                        $Columns['fdesde']   = array('0px','left','none');
                        $Columns['hdesde']   = array('0px','left','none');
                        $Columns['fhasta']   = array('0px','left','none');
                        $Columns['hhasta']   = array('0px','left','none');
                        $Columns['EstadoPermiso']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaEmpleadoPermisosSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->EmpleadoPermisosGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function EmpleadoPermisosGridHTML($ObjSchGrid,$Datos)
              {       
                  if(count($Datos)>0){
                     foreach ($Datos as $permisos)
                      {        $id = str_pad($permisos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                               $fdesde = explode(' ',trim($permisos['fedesde']));
                               $fechadesde = $this->Fechas->changeFormatDate($fdesde[0],"DMY")[1];
                               $horadesde = $fdesde[1];
                               $fhasta = explode(' ',trim($permisos['fehasta']));
                               $fechahasta = $this->Fechas->changeFormatDate($fhasta[0],"DMY")[1];
                               $horahasta = $fhasta[1];
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($permisos['empleado']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($permisos['motivo']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($permisos['fedesde']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($permisos['fehasta']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$permisos['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$permisos['idestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($permisos['idempleado']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($permisos['idmotivo']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($permisos['detalle']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($fechadesde));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($horadesde));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($fechahasta));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($horahasta));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($permisos['stestado']));
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($permisos['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstado($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstado(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function TraeDatosMotivo()
              {        
                       $ServicioMotivoPermisos = new ServicioMotivoPermisos();
                       $datoMotivo = $ServicioMotivoPermisos->BuscarMotivoPermisos();
                       return $datoMotivo;
              }
              
              private function CreaComboMotivo()
              {       
                       $datosMotivo= $this->TraeDatosMotivo(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("motivo",$datosMotivo)->Selected(1)->Create("s04");
              }
              
              function MuestraEmpleadoPermisos($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $fdesde = explode(' ',trim($Datos[0]['fedesde']));
                            $fechadesde = $this->Fechas->changeFormatDate($fdesde[0],"DMY")[1];
                            $horadesde = $fdesde[1];
                            $fhasta = explode(' ',trim($Datos[0]['fehasta']));
                            $fechahasta = $this->Fechas->changeFormatDate($fhasta[0],"DMY")[1];
                            $horahasta = $fhasta[1];

                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("txempleado".$this->Sufijo,"value", trim($Datos[0]['empleado']));    
                            $Ajax->Assign("idempleado".$this->Sufijo,"value", trim($Datos[0]['idempleado']));
                            $Ajax->Assign("motivo".$this->Sufijo,"value", trim($Datos[0]['idmotivo'])); 
                            $Ajax->Assign("fedesde".$this->Sufijo,"value", trim($fechadesde)); 
                            $Ajax->Assign("hdesde".$this->Sufijo,"value", trim($horadesde)); 
                            $Ajax->Assign("fehasta".$this->Sufijo,"value", trim($fechahasta)); 
                            $Ajax->Assign("hhasta".$this->Sufijo,"value", trim($horahasta));
                            $Ajax->Assign("detalle".$this->Sufijo,"value", trim($Datos[0]['detalle'])); 
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            if(trim($Datos[0]['stestado']) == 4){
                                $Ajax->call("BarButtonStateDisabled",$this->Sufijo,"addMod");
                            }
                            return $Ajax;
                       }else{
                            return $this->RespuestaEmpleadoPermisos($Ajax,"NOEXISTEID","No existe Permiso con ese ID.");
                       }
              }
              
              function MuestraEmpleadoPermisosGrid($Ajax,$Datos)
              {                              
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->EmpleadoPermisosGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }   
               
              function MuestraEmpleadoPermisosGuardado($Ajax,$EmpleadoPermisos)
              {       if (count($EmpleadoPermisos)>0)
                        {   $id = str_pad($EmpleadoPermisos[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraEmpleadoPermisosGrid($Ajax,$EmpleadoPermisos);
                            return $this->RespuestaEmpleadoPermisos($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaEmpleadoPermisos($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraEmpleadoPermisosEditado($Ajax,$EmpleadoPermisos)
              {       foreach ($EmpleadoPermisos as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraEmpleadoPermisosRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaEmpleadoPermisos($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaEmpleadoPermisos($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraEmpleadoPermisosEliminado($Ajax,$EmpleadoPermisos)
              {        foreach ($EmpleadoPermisos as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraEmpleadoPermisosRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaEmpleadoPermisos($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaEmpleadoPermisos($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraEmpleadoPermisosRowGrid($Ajax,$EmpleadoPermisos,$estado=1)
              {         $Fila = $this->SearchGrid->GetRow($EmpleadoPermisos['id'],$estado);
                        $tipo = trim($EmpleadoPermisos['idtipo']);
                        $fdesde = explode(' ',trim($EmpleadoPermisos['fedesde']));
                        $fechadesde = $this->Fechas->changeFormatDate($fdesde[0],"DMY")[1];
                        $horadesde = $fdesde[1];
                        $fhasta = explode(' ',trim($EmpleadoPermisos['fehasta']));
                        $fechahasta = $this->Fechas->changeFormatDate($fhasta[0],"DMY")[1];
                        $horahasta = $fhasta[1];
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($EmpleadoPermisos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($EmpleadoPermisos['empleado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($EmpleadoPermisos['motivo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($EmpleadoPermisos['fedesde']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($EmpleadoPermisos['fehasta']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($EmpleadoPermisos['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($EmpleadoPermisos['idestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($EmpleadoPermisos['idempleado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($EmpleadoPermisos['idmotivo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",trim($EmpleadoPermisos['detalle']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",trim($fechadesde));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],11),"innerHTML",trim($horadesde));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],12),"innerHTML",trim($fechahasta));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],13),"innerHTML",trim($horahasta));
                        return $Ajax;
              }

              function MuestraEmpleadoPermisosExcepcion($Ajax,$Msg)
              {         return $this->RespuestaEmpleadoPermisos($Ajax,"EXCEPCION",$Msg);    
              }

              private function RespuestaEmpleadoPermisos($Ajax,$Tipo,$Data)
              {         $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
              }
              
              /***** Modal *****/

                private function SearchGridModalConfig()
                {       $Columns['Id']      = array('40px','center','');
                        $Columns['Empleado']= array('150px','left','');
                        $Columns['Observaci&oacute;n']  = array('170px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '120px','AltoRow' => '20px');
                }

                function CreaModalGridEmpleadoPermisos($Operacion,$Datos)
                {     
                        $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $ObjHTML = $this->ModalGridDataHTMLEmpleadoPermisos($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }
                
                function MuestraModalGridEmpleadoPermisos($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $ObjHTML = $this->ModalGridDataHTMLEmpleadoPermisos($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }        

                private function ModalGridDataHTMLEmpleadoPermisos($Grid,$Datos)
                {   foreach ($Datos as $EmpleadoPermisos)
                        {       $id = str_pad($EmpleadoPermisos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($EmpleadoPermisos['empleado']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($EmpleadoPermisos['observaciones']));
                                $Grid->CreaSearchRowsDetalle($id,"");
                        }
                        return $Grid->CreaSearchTableDetalle();
                }

                function CreaModalPdfPermiso($Datos)
                {      $EmpleadoPermisos.= '<div id="botonpermiso" align="right">   <input id="btn_pdfpermiso_'.$this->Sufijo.'" src="src/public/img/impresora.png" width="40px"; type="image" onclick="return LlenarDivOculto'.$this->Sufijo.'();"  title="Imprimir"/> </div>';
                       $EmpleadoPermisos.= '     <div id="SeccionCabIngresoDetallePedido'.$this->Sufijo.'" class="SchCab"> ';
                       $EmpleadoPermisos.= '        <table class="SchGrdCab" id="CabIngresoDetallePedido'.$this->Sufijo.'" name="CabIngresoDetallePedido'.$this->Sufijo.'" cellpadding="0">';
                       $EmpleadoPermisos.= '             <tbody>';                       
                       $EmpleadoPermisos.= '                 <tr class="SchGrdCabCell">';
                       $EmpleadoPermisos.= '                     <td class="SchGrdCabCell" style="width: 900px; height: 30px">MAGICNEGSA</td>';
                       $EmpleadoPermisos.= '                 </tr>'; 
                       $EmpleadoPermisos.= '                 <tr class="SchGrdCabCell">';
                       $EmpleadoPermisos.= '                     <td class="SchGrdCabCell" style="width: 900px; height: 30px">SOLICITUD DE PERMISO LABORAL</td>';
                       $EmpleadoPermisos.= '                 </tr>';
                       $EmpleadoPermisos.= '                 <tr class="SchGrdCabCell">';
                       $EmpleadoPermisos.= '                     <td class="SchGrdCabCell" style="width: 900px; height: 30px">INFORMACI&Oacute;N SOBRE EL PERMISO</td>';
                       $EmpleadoPermisos.= '                 </tr>';
                       $EmpleadoPermisos.= '             </tbody>';
                       $EmpleadoPermisos.= '        </table>';
                       $EmpleadoPermisos.= '    </div>';
                       $EmpleadoPermisos.= '        <table class="SchGrdCab" id="CabIngresoDetallePedido'.$this->Sufijo.'" name="CabIngresoDetallePedido'.$this->Sufijo.'" cellpadding="0">';
                       $EmpleadoPermisos.= '             <tbody>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:30px;"><span style="color: black;"> NOMBRE DEL EMPLEADO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2"><span style="color: green;">'.trim($Datos[0]['empleado']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;"> C.I.</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;"><span style="color: green;">'.trim($Datos[0]['numero']).'</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 160px; height:20px;"><span style="color: black;"> DEPARTAMENTO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;"><span style="color: green;">'.trim($Datos[0]['departamento']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;"> JEFE INMEDIATO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2"><span style="color: green;">'.trim($Datos[0]['jefe']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       //$EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;">FECHA DEL PERMISO</span></td>';
                       //$EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;"><span style="color: green;">'.trim($Datos[0]['fecreacion']).'</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 180px; height:20px;"><span style="color: black;"> DESDE</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;"><span style="color: green;">'.trim($Datos[0]['fedesde']).'</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 180px; height:20px;"><span style="color: black;">HASTA</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;"><span style="color: green;">'.trim($Datos[0]['fehasta']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;"> TIPO DE PERMISO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2"><span style="color: green;">'.trim($Datos[0]['motivo']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;"> MOTIVO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2"><span style="color: green;">'.trim($Datos[0]['detalle']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;"> ESTADO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2"><span style="color: green;">'.trim($Datos[0]['estadopermiso']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;"> REEMPLAZO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2"><span style="color: green;">'.trim($Datos[0]['reemplazo']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';                        
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                   </tr>';                     
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:20px;" colspan="4"><span style="color: black;">'
                               . 'IMPORTANTE.- Las solicitudes de permiso para consultas m&eacute;dicas o tr&aacute;mites personales</span></td></tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:20px;" colspan="4"><span style="color: black;">'
                               . ' se deben presentar 2 d&iacute;as antes del primer d&iacute;a en que se estar&aacute; ausente y para solicitar </span></td></tr>';                       
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:20px;" colspan="4"><span style="color: black;">'
                               . ' sus vacaciones la misma debe ser entregada con 15 d&iacute;as antes, a excepci&oacute;n del permiso</span></td></tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:20px;" colspan="4"><span style="color: black;">'
                               . ' por calamidad dom&eacute;stica.</br> </span></td></tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:20px;" colspan="4"><span style="color: black;">'
                               . 'Al retorno del permiso traer los justificados pertinentes.'
                               . '</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';                       
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                   </tr>';     
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;"> COMENTARIOS</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="3"><span style="color: green;">'.trim($Datos[0]['observaciones']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';                     
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                   </tr>';                       
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                   </tr>';                      
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;" colspan="2" align="center"><img src="'.trim($Datos[0]['firma']).'" width="280px" height="120px"></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;" align="center" colspan="2"><span style="color: black;">FIRMA DEL EMPLEADO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;" align="center" colspan="2"><span style="color: black;">FIRMA DEL JEFE INMEDIATO</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';                                              
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                       $EmpleadoPermisos.= '                   </tr>';                     
                       $EmpleadoPermisos.= '                  <tr class="Form-Label">';
                       $EmpleadoPermisos.= '                      <td style="width: 200px; height:30px;"><span style="color: black;">FECHA DEL PERMISO</span></td>';
                       $EmpleadoPermisos.= '                      <td style="width: 230px; height:20px;" colspan="2"><span style="color: green;">'.trim($Datos[0]['fecreacion']).'</span></td>';
                       $EmpleadoPermisos.= '                   </tr>';
                       $EmpleadoPermisos.= '              </tbody>';
                       $EmpleadoPermisos.= '        </table>';
                       return $EmpleadoPermisos;
                }        

                private function ModalPdfPermiso($Grid,$Datos)
                {   foreach ($Datos as $EmpleadoPermisos)
                        {       $id = str_pad($EmpleadoPermisos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($EmpleadoPermisos['empleado']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($EmpleadoPermisos['observaciones']));
                                $Grid->CreaSearchRowsDetalle($id,"");
                        }
                        return $Grid->CreaSearchTableDetalle();
                }  
                
              function MuestraEmpleadoPorId($Ajax,$Datos)
              {        $Ajax->Assign("idempleado".$this->Sufijo,"value", $Datos[0]['idempleado']);
                       $Ajax->Assign("txempleado".$this->Sufijo,"value", trim($Datos[0]['nombre']).' '.trim($Datos[0]['apellido']));
                       return $Ajax;
              }
        }
?>

