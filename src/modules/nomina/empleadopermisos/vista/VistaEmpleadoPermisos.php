<?php
         $Sufijo = '_EmpleadoPermisos';
         
         require_once('src/modules/nomina/empleadopermisos/controlador/ControlEmpleadoPermisos.php');
         $ControlEmpleadoPermisos = new ControlEmpleadoPermisos($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlEmpleadoPermisos,'GuardaEmpleadoPermisos'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlEmpleadoPermisos,'MuestraEmpleadoPermisosByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlEmpleadoPermisos,'MuestraEmpleadoPermisosByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlEmpleadoPermisos,'EliminaEmpleadoPermisos'));         
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlEmpleadoPermisos,'CargaModalGridEmpleadoPermisos'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlEmpleadoPermisos,'ConsultaModalGridEmpleadoPermisosByTX')); 
         $xajax->register(XAJAX_FUNCTION,array('PdfPermiso'.$Sufijo, $ControlEmpleadoPermisos,'PdfPermisoEmpleadoPermisos'));
         $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/nomina/empleadopermisos/js/JSEmpleadoPermisos.js"></script>
         </head>
        <body>
         <div class="FormBasic" style="width:720px">
               <div class="FormSectionMenu">              
              <?php  $ControlEmpleadoPermisos->CargaEmpleadoPermisosBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlEmpleadoPermisos->CargaEmpleadoPermisosMantenimiento();  ?>
                   </form>
                  <div id="divImprime<?php echo $Sufijo; ?>" style=" display: none;"></div>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlEmpleadoPermisos->CargaEmpleadoPermisosSearchGrid();  ?>
              </div>  
         </div> 
        </body>
        </html>
              

