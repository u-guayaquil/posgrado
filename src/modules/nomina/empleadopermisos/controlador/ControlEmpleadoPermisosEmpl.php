<?php
         require_once("src/rules/nomina/servicio/ServicioEmpleadoPermisos.php");
         require_once("src/rules/general/servicio/ServicioEmpleado.php");
         require_once("src/rules/nomina/servicio/ServicioMotivoPermisos.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php"); 
         require_once("src/rules/sistema/servicio/ServicioUsuario.php");
         require_once("src/modules/nomina/empleadopermisos/render/RenderEmpleadoPermisosEmpl.php");
         require_once("src/modules/general/empleado/render/RenderEmpleado.php");         
         require_once('src/libs/clases/DateControl.php');   


         class ControlEmpleadoPermisosEmpl
         {     private  $Sufijo; 
               private  $ServicioEmpleadoPermisos;
               private  $ServicioEmpleado;               
               private  $ServicioUsuario;
               private  $RenderEmpleadoPermisos;
               private  $RenderEmpleado;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioEmpleadoPermisos = new ServicioEmpleadoPermisos();
                        $this->ServicioEmpleado = new ServicioEmpleado();
                        $this->ServicioUsuario = new ServicioUsuario();
                        $this->RenderEmpleadoPermisos = new RenderEmpleadoPermisosEmpl($Sufijo);
                        $this->RenderEmpleado = new RenderEmpleado($Sufijo);
               }

               function CargaEmpleadoPermisosBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaEmpleadoPermisosMantenimiento()
               {        echo $this->RenderEmpleadoPermisos->CreaEmpleadoPermisosMantenimiento();
               }

               function CargaEmpleadoPermisosSearchGrid()
               {        
                        $datos = $this->ServicioUsuario->BuscarUsuarioByID($_SESSION['IDUSER']);
                        $datoEmpleadoPermisos = $this->ServicioEmpleadoPermisos->BuscarEmpleadoPermisosByEmpleado(intval($datos[0]['idempleado']));
                        echo $this->RenderEmpleadoPermisos->CreaEmpleadoPermisosSearchGrid($datoEmpleadoPermisos); 
               }         
               
               function MuestraEmpleadoPermisosByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $datoEmpleadoPermisos = $this->ServicioEmpleadoPermisos->BuscarEmpleadoPermisosById(intval($id));
                        return $this->RenderEmpleadoPermisos->MuestraEmpleadoPermisos($ajaxRespon,$datoEmpleadoPermisos);
               }

               function MuestraEmpleadoPermisosByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $datos = $this->ServicioUsuario->BuscarUsuarioByID($_SESSION['IDUSER']); 
                        $prepareDQL = array('limite' => 50,'empleado' => intval($datos[0]['idempleado']), 'texto' => strtoupper($texto));
                        $datoEmpleadoPermisos = $this->ServicioEmpleadoPermisos->BuscarPermisosByEmpleadoyTexto($prepareDQL);
                        return $this->RenderEmpleadoPermisos->MuestraEmpleadoPermisosGrid($ajaxRespon,$datoEmpleadoPermisos);
               }

               function GuardaEmpleadoPermisos($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $EmpleadoPermisos  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioEmpleadoPermisos->GuardaDBEmpleadoPermisos($EmpleadoPermisos);

                        if (is_numeric($id)){
                            $function = (empty($EmpleadoPermisos->id) ? "MuestraEmpleadoPermisosGuardado" : "MuestraEmpleadoPermisosEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioEmpleadoPermisos->BuscarEmpleadoPermisos($prepareDQL);
                            return $this->RenderEmpleadoPermisos->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderEmpleadoPermisos->MuestraEmpleadoPermisosExcepcion($ajaxRespon,($id));
                        }                   
               } 
               
               function EliminaEmpleadoPermisos($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioEmpleadoPermisos->DesactivaEmpleadoPermisos(intval($id));
                        $Datos = $this->ServicioEmpleadoPermisos->BuscarEmpleadoPermisos($prepareDQL);
                        return $this->RenderEmpleadoPermisos->MuestraEmpleadoPermisosEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridEmpleadoPermisos($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);                           
                        $Nombre="Empleado";
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca ".$Nombre;
                        $jsonModal['Carga'] = $this->RenderEmpleado->CreaModalGrid($Operacion,$Datos);                        
                        $jsonModal['Ancho'] = "503";
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalEmpleadoPermisosGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);                        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);
                        return $this->RenderEmpleado->MuestraModalGridEmpleado($ajaxResp,$Operacion,$Datos);      
                }
                
                function PdfPermisoEmpleadoPermisos($id){
                        $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'id' => intval($id));
                        $Datos = $this->ServicioEmpleadoPermisos->BuscarEmpleadoPermisosImprime($prepareDQL);
                        $jsonModal['Modal'] = "Modal Pdf de Permiso"; 
                        $jsonModal['Title'] = "";
                        $jsonModal['Carga'] = $this->RenderEmpleadoPermisos->CreaModalPdfPermiso($Datos);                        
                        $jsonModal['Ancho'] = "700";
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function TraeEmpleado()
                {       $ajaxResp = new xajaxResponse(); 
                        $Datos = $this->ServicioUsuario->BuscarUsuarioByID($_SESSION['IDUSER']);
                        return $this->RenderEmpleadoPermisos->MuestraEmpleadoPorId($ajaxResp,$Datos);      
                }
         }

?>

