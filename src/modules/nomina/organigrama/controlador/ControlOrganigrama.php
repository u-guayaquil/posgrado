<?php
         require_once("src/rules/nomina/servicio/ServicioOrganigrama.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/nomina/organigrama/render/RenderOrganigrama.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlOrganigrama
         {     private  $Sufijo; 
               private  $ServicioOrganigrama;
               private  $RenderOrganigrama;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioOrganigrama = new ServicioOrganigrama();
                        $this->RenderOrganigrama = new RenderOrganigrama($Sufijo);
               }

               function CargaOrganigramaBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaOrganigramaMantenimiento()
               {        echo $this->RenderOrganigrama->CreaOrganigramaMantenimiento();
               }

               function CargaOrganigramaSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoOrganigrama = $this->ServicioOrganigrama->BuscarOrganigramaByDescripcion($prepareDQL);
                        echo $this->RenderOrganigrama->CreaOrganigramaSearchGrid($datoOrganigrama); 
               }         
               
               function MuestraOrganigramaByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoOrganigrama = $this->ServicioOrganigrama->BuscarOrganigramaByID($prepareDQL);
                        return $this->RenderOrganigrama->MuestraOrganigrama($ajaxRespon,$datoOrganigrama);
               }

               function MuestraOrganigramaByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoOrganigrama = $this->ServicioOrganigrama->BuscarOrganigramaByDescripcion($prepareDQL);
                        return $this->RenderOrganigrama->MuestraOrganigramaGrid($ajaxRespon,$datoOrganigrama);
               }

               function GuardaOrganigrama($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Organigrama  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioOrganigrama->GuardaDBOrganigrama($Organigrama);
                        if (is_numeric($id)){
                            $function = (empty($Organigrama->id) ? "MuestraOrganigramaGuardado" : "MuestraOrganigramaEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioOrganigrama->BuscarOrganigramaByID($prepareDQL);
                            return $this->RenderOrganigrama->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderOrganigrama->MuestraOrganigramaExcepcion($ajaxRespon,($id));
                        }                    
               } 
               
               function EliminaOrganigrama($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioOrganigrama->DesactivaOrganigrama(intval($id));
                        $Datos = $this->ServicioOrganigrama->BuscarOrganigramaByDescripcion($prepareDQL);
                        return $this->RenderOrganigrama->MuestraOrganigramaEliminado($ajaxRespon,$Datos);
                }
               
         }

?>

