<?php 
         $Sufijo = '_Organigrama';

         require_once('src/modules/nomina/organigrama/controlador/ControlOrganigrama.php');
         $ControlOrganigrama = new ControlOrganigrama($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlOrganigrama,'GuardaOrganigrama'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlOrganigrama,'MuestraOrganigramaByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlOrganigrama,'MuestraOrganigramaByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlOrganigrama,'EliminaOrganigrama'));
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
                <?php $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
                ?>
            <script type="text/javascript"> 

            $(document).ready(function()
            {       $('#fedesde_Organigrama').datetimepicker({
                            timepicker:false,
                            format: 'd/m/Y'
                    });
                    $('#fehasta_Organigrama').datetimepicker({
                            timepicker:false,
                            format: 'd/m/Y'
                    });
            });

            function ButtonClick_Organigrama(Sufijo,Operacion)
            {       if (Operacion=='addNew')
                    {   BarButtonState(Sufijo,Operacion);
                        ElementStatus(Sufijo,"version:comentario:fedesde:fehasta","id");
                        ElementClear(Sufijo,"id:version:comentario:fedesde:fehasta");
                        ElementSetValue(Sufijo, "estado", "1");
                    }
                    else if (Operacion=='addMod')
                    {   BarButtonState(Sufijo,Operacion);
                        if (ElementGetValue(Sufijo, "estado") == 0){
                            ElementStatus(Sufijo,"version:comentario:fedesde:fehasta:estado","id");
                        }else{
                            ElementStatus(Sufijo,"version:comentario:fedesde:fehasta","id");
                        }
                    }
                    else if (Operacion=='addDel')
                    {   if (ElementGetValue(Sufijo,"estado")==1)
                        {   if (confirm("Desea inactivar este registro?"))
                            xajax_Elimina_Organigrama(ElementGetValue(Sufijo,"id"));
                        }
                        else
                        alert("El registro se encuentra inactivo");
                    }
                    else if (Operacion=='addSav')
                    {   if (ElementValidateBeforeSave(Sufijo,"version:fedesde:fehasta"))
                        {   if (BarButtonState(Sufijo,"Inactive"))
                            {   var Forma = PrepareElements(Sufijo,"id:version:fedesde:fehasta:comentario:estado");
                                xajax_Guarda_Organigrama(JSON.stringify({Forma}));
                            }
                        }
                    }
                    else if (Operacion=='addCan')
                    {   BarButtonState(Sufijo,Operacion);
                        ElementStatus(Sufijo,"id","version:comentario:fedesde:fehasta");
                        ElementClear(Sufijo,"id:version:comentario:fedesde:fehasta");
                    }
                    return false;
            }

            function SearchByElement_Organigrama(Sufijo,Elemento)
            {       Elemento.value = TrimElement(Elemento.value);
                    if (Elemento.name=="id"+Sufijo)
                    {   if (Elemento.value.length != 0)
                        {   ContentFlag = document.getElementById("version"+Sufijo); 
                            if (ContentFlag.value.length==0)
                            {   if (BarButtonState(Sufijo,"Inactive"))
                                xajax_MuestraByID_Organigrama(Elemento.value);
                            }
                        }
                        else
                        BarButtonState(Sufijo,"Default");
                    }
                    else
                    {   xajax_MuestraByTX_Organigrama(Elemento.value);    
                    }    
            }
                 
            function SearchGetData_Organigrama(Sufijo,Grilla)
            {       if (IsDisabled(Sufijo,"addSav"))   
                    {   BarButtonState(Sufijo,"Active");
                        var fechai = Grilla.cells[2].childNodes[0].nodeValue.split(' ');
                        var fechainicial = fechai[0];
                          
                        var fechaf = Grilla.cells[3].childNodes[0].nodeValue.split(' ');
                        if(fechaf[0] === '01/01/1900'){
                            var fechafinal = '';
                        }else{
                            var fechafinal = fechaf[0];
                        } 
                        document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                        document.getElementById("version"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;        
                        document.getElementById("fedesde"+Sufijo).value = fechainicial; 
                        document.getElementById("fehasta"+Sufijo).value = fechafinal; 
                        document.getElementById("comentario"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue; 
                        document.getElementById("estado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue; 
                    }    
                    return false;
            }
                 
            function XAJAXResponse_Organigrama(Sufijo,Mensaje,Datos)
            {       var objOrganigrama = "comentario:version:fedesde:fehasta:estado";
                    if (Mensaje==="GUARDADO")
                    {   BarButtonState(Sufijo,"Active");
                        ElementSetValue(Sufijo,"id",Datos);
                        ElementStatus(Sufijo,"id",objOrganigrama);
                        alert('Los datos se guardaron correctamente.');
                    }
                    else if(Mensaje==='ELIMINADO')
                    {   ElementSetValue(Sufijo,"estado",Datos);
                        alert("Los Datos se eliminaron correctamente");
                    }
                    else if(Mensaje==='NOEXISTEID')
                    {   BarButtonState(Sufijo,'Default');
                        ElementClear(Sufijo,"id");
                        alert(Datos);
                    }
                    else if(Mensaje==='EXCEPCION')
                    {   BarButtonState(Sufijo,"addNew"); 
                        alert(Datos);
                    } 
            }
            </script>
        </head>
        <body>
        <div class="FormBasic" style="width:518px">
                <div class="FormSectionMenu">              
                <?php   $ControlOrganigrama->CargaOrganigramaBarButton($_GET['opcion']);
                        echo '<script type="text/javascript">';
                        echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                        echo '</script>';
                ?>
                </div>    
                <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <?php  $ControlOrganigrama->CargaOrganigramaMantenimiento();  ?>
                </form>
                </div>    
                <div class="FormSectionGrid"> 
                    <?php echo $ControlOrganigrama->CargaOrganigramaSearchGrid();  ?>
                </div>  
        </div> 
        </body>
        </html>
              

