<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderOrganigrama
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Fechas = new DateControl();
                    $this->Maxlen['id'] = 2; 
            }
               
            function CreaOrganigramaMantenimiento()
            {       $obj0017 = "version:comentario:estado:fedesde:fehasta";
                         
                    $Organigrama = '<table border="0" class="Form-Frame" cellpadding="0">';
                    $Organigrama.= '       <tr height="30">';
                    $Organigrama.= '           <td class="Form-Label" style="width:20%">Id</td>';
                    $Organigrama.= '           <td class="Form-Label" style="width:35%">';
                    $Organigrama.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\''.$obj0017.'\',\'version\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Organigrama.= '           </td>';
                    $Organigrama.= '           <td class="Form-Label" style="width:20%">Estado</td>';
                    $Organigrama.= '           <td class="Form-Label" style="width:25%">';
                    $Organigrama.=             $this->CreaComboEstado();
                    $Organigrama.='            </td>';
                    $Organigrama.= '       </tr>';
                    $Organigrama.= '       <tr height="30">';
                    $Organigrama.= '           <td class="Form-Label">Fecha Inicio</td>';
                    $Organigrama.= '           <td class="Form-Label"><input class="txt-upper t03" name = "fedesde'.$this->Sufijo.'" id = "fedesde'.$this->Sufijo.'" disabled/></td>';
                    $Organigrama.= '           <td class="Form-Label">Fecha Fin</td>';
                    $Organigrama.= '           <td class="Form-Label"><input class="txt-upper t03" name = "fehasta'.$this->Sufijo.'" id = "fehasta'.$this->Sufijo.'" disabled/></td>';
                    $Organigrama.= '       </tr>';
                    $Organigrama.= '       <tr height="30">';
                    $Organigrama.= '           <td class="Form-Label">Versi&oacute;n</td>';
                    $Organigrama.= '           <td class="Form-Label" colspan="3">';
                    $Organigrama.= '               <input type="text" class="txt-upper t06" id="version'.$this->Sufijo.'" name="version'.$this->Sufijo.'" value="" maxlength="30" disabled/>';
                    $Organigrama.= '           </td>';
                    $Organigrama.= '       </tr>';                  
                    $Organigrama.= '       <tr height="30">';
                    $Organigrama.= '           <td class="Form-Label">Comentario</td>';
                    $Organigrama.= '           <td class="Form-Label" colspan="3">';
                    $Organigrama.= '               <input class="txt-upper t14" name = "comentario'.$this->Sufijo.'" id = "comentario'.$this->Sufijo.'"  value="" maxlength="250" disabled/>';
                    $Organigrama.= '           </td>';
                    $Organigrama.= '       </tr>';
                    $Organigrama.= '</table>';
                    return $Organigrama;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']     = array('30px','center','');                       
                    $Columns['Version']= array('150px','left','');
                    $Columns['Desde']  = array('70px','center','');
                    $Columns['Hasta']  = array('70px','center','');
                    $Columns['Creaci&oacute;n'] = array('70px','center','');
                    $Columns['Estado']   = array('70px','left',''); 
                    $Columns['idEstado'] = array('0px','left','none');
                    $Columns['comentario'] = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
            }

            function CreaOrganigramaSearchGrid($Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->OrganigramaGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }
              
            private function OrganigramaGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $organigrama)
                    {       $fecreacion = $this->Fechas->changeFormatDate($organigrama['fecreacion'],"DMY")[1];
                            $fedesde = $this->Fechas->changeFormatDate($organigrama['fedesde'],"DMY")[1];
                            $fehasta = $this->Fechas->changeFormatDate($organigrama['fehasta'],"DMY")[1];
                            
                            $id = str_pad($organigrama['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($organigrama['version']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fedesde);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fehasta);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($organigrama['txtestado'])); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$organigrama['idestado']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($organigrama['comentario']));
                               
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,($organigrama['idestado']==0 ? "red" : ""));
                    }
                    return $ObjSchGrid->CreaSearchTableDetalle(); 
            }                  

            function TraeDatosEstadoByArray($IdArray)
            {       $ServicioEstado = new ServicioEstado();
                    $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                    return $datoEstado;
            }
              
            private function CreaComboEstado()
            {       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                    $Select = new ComboBox($this->Sufijo);                        
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
            } 
              
            function MuestraOrganigrama($Ajax,$Datos)
            {       foreach ($Datos as $Dato)   
                    {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign("version".$this->Sufijo,"value", trim($Dato['version']));
                        $Ajax->Assign("fedesde".$this->Sufijo,"value", trim($this->Fechas->changeFormatDate($Dato['fedesde'],"DMY")[1]));
                        
                        $fehasta = trim($this->Fechas->changeFormatDate($Dato['fehasta'],"DMY")[1]);
                        if($fehasta == '01/01/1900') $fehasta = '';

                        $Ajax->Assign("fehasta".$this->Sufijo,"value", $fehasta);
                        $Ajax->Assign("comentario".$this->Sufijo,"value", trim($Dato['comentario']));
                        $Ajax->Assign("estado".$this->Sufijo,"value", $Dato['idestado']);
                        $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                        return $Ajax;
                    }
                    return $this->RespuestaOrganigrama($Ajax,"NOEXISTEID","No existe un organigrama con el ID ingresado.");
            }
              
            function MuestraOrganigramaGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->OrganigramaGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }  
              
            function MuestraOrganigramaGuardado($Ajax,$Organigrama)
            {       foreach ($Organigrama as $Dato)   
                    {   $id = str_pad($Dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraOrganigramaGrid($Ajax,$Organigrama);
                        return $this->RespuestaOrganigrama($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaOrganigrama($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraOrganigramaEditado($Ajax,$Organigrama)
            {       foreach ($Organigrama as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraOrganigramaRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaOrganigrama($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaOrganigrama($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraOrganigramaEliminado($Ajax,$Organigrama)
            {       foreach ($Organigrama as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraOrganigramaRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaOrganigrama($xAjax,"ELIMINADO",$datos['idestado']);
                    }  
                    return $this->RespuestaOrganigrama($xAjax,"EXCEPCION","No se eliminó la información.");
            }
              
            function MuestraOrganigramaRowGrid($Ajax,$Organigrama,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($Organigrama['id'],$estado);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Organigrama['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Organigrama['version']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($this->Fechas->changeFormatDate($Organigrama['fedesde'],"DMY")[1]));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($this->Fechas->changeFormatDate($Organigrama['fehasta'],"DMY")[1]));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($this->Fechas->changeFormatDate($Organigrama['fecreacion'],"DMY")[1]));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Organigrama['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$Organigrama['idestado']); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($Organigrama['comentario']));
                    return $Ajax;
            }

            function MuestraOrganigramaExcepcion($Ajax,$Msg)
            {       return $this->RespuestaOrganigrama($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaOrganigrama($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
        }
?>

