<?php
         require_once("src/rules/nomina/servicio/ServicioOrgDepartamento.php");
         require_once("src/rules/nomina/servicio/ServicioOrganigrama.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/nomina/orgdepartamento/render/RenderOrgDepartamento.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlOrgDepartamento
         {     private  $Sufijo; 
               private  $ServicioOrgDepartamento;
               private  $RenderOrgDepartamento;

               function __construct($Sufijo = "")
               {        
                        $this->ServicioOrgDepartamento = new ServicioOrgDepartamento();
                        $this->RenderOrgDepartamento = new RenderOrgDepartamento($Sufijo);
               }

               function CargaOrgDepartamentoBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $this->RenderOrgDepartamento->CreaOpcionBarButton($Datos);
                }
               
               function CargaOrgDepartamentoMantenimiento()
               {        
                        echo $this->RenderOrgDepartamento->CreaOrgDepartamentoMantenimiento();
               }
               
               function DepartamentosPrimeraVez()
               {        $ajaxRespon = new xajaxResponse();
                        $tablaDepartamentos = $this->RenderOrgDepartamento->CreaOrgDepartamentoSearchGridPrimera();
                        $ajaxRespon->Assign("SeccionDepartamentos","innerHTML",$tablaDepartamentos);
                        return $ajaxRespon;
               }
               
               function ControlDepartamento()
               {        $ajaxRespon = new xajaxResponse();
                        $tablaDepartamentos = $this->RenderOrgDepartamento->CreaDepartamentosPorPadre();
                        $ajaxRespon->Assign("SeccionDepartamentos","innerHTML",$tablaDepartamentos);
                        return $ajaxRespon;
               }
               
               function TraeDepartamentosHijos($padre,$organigrama)
               {        $ajaxRespon = new xajaxResponse();
                        $tablaDepartamentos = $this->RenderOrgDepartamento->CreaDepartamentosHijos($padre,$organigrama);
                        $ajaxRespon->Assign("SeccionDepartamentos","innerHTML",$tablaDepartamentos);
                        return $ajaxRespon;
               }
               
               function CargaDepartamentos()
               {        echo $this->RenderOrgDepartamento->CreaDepartamentosPadres();
               }

               function CargaOrgDepartamentoSearchGrid()
               {        $ServicioOrganigrama = new ServicioOrganigrama();
                        $Organigrama = $ServicioOrganigrama->BuscarOrganigramaByArrayID('');
                        $datoOrgDepartamento = $this->ServicioOrgDepartamento->BuscarOrgDepartamentosGeneral(0,$Organigrama);
                        echo $this->RenderOrgDepartamento->CreaOrgDepartamentoSearchGrid($datoOrgDepartamento); 
               }         
               
               function MuestraOrgDepartamentoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $datoOrgDepartamento = $this->ServicioOrgDepartamento->BuscarOrgDepartamentoByID($id);
                        return $this->RenderOrgDepartamento->MuestraOrgDepartamento($ajaxRespon,$datoOrgDepartamento);
               }

               function MuestraOrgDepartamentoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoOrgDepartamento = $this->ServicioOrgDepartamento->BuscarOrgDepartamentoByDescripcion($prepareDQL);
                        return $this->RenderOrgDepartamento->MuestraOrgDepartamentoGrid(new SearchGrid($this->Sufijo),$ajaxRespon,$datoOrgDepartamento);
               }

               function GuardaOrgDepartamento($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $OrgDepartamento  = json_decode($Form)->Forma;
                        
                        if (empty($OrgDepartamento->id))
                        {   list($estado,$Id) = $this->ServicioOrgDepartamento->CreaOrgDepartamento($OrgDepartamento);                            
                        }else{
                            list($estado,$Id) = $this->ServicioOrgDepartamento->EditaOrgDepartamento($OrgDepartamento);
                        }    
                        $Datos = $this->ServicioOrgDepartamento->BuscarOrgDepartamentosGeneral();
                        
                        if($estado){
                            return $this->RenderOrgDepartamento->MuestraDespuesdeGuardar($ajaxRespon,$Id,($Datos));                    
                        }else{
                            return $this->RenderOrgDepartamento->MuestraErroralGuardar($ajaxRespon,$Id,($Datos));                    
                        }                        
               } 
               
               function EliminaOrgDepartamento($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $this->ServicioOrgDepartamento->DesactivaOrgDepartamento($id);
                        $Datos = $this->ServicioOrgDepartamento->BuscarOrgDepartamentoByDescripcion($prepareDQL);
                        return $this->RenderOrgDepartamento->MuestraDespuesdeEliminar($ajaxRespon,$Datos);
                }
               
         }

?>

