<?php    

        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/nomina/servicio/ServicioOrganigrama.php");
        require_once("src/rules/nomina/servicio/ServicioDepartamento.php");
        require_once("src/rules/nomina/servicio/ServicioOrgDepartamento.php");
        
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderOrgDepartamento
        {     private $Sufijo;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;                       
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');                       
                        $Columns['Organigrama'] = array('100px','left','');
                        $Columns['Departamento Padre'] = array('0px','left','none');
                        $Columns['Departamento'] = array('250px','left','');                        
                        $Columns['idDepartamentoPadre'] = array('0px','left','none');
                        $Columns['idOrganigrama'] = array('0px','left','none');
                        
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '440px','AltoRow' => '20px','FindTxt' =>false);
              }
               
              function CreaOrgDepartamentoMantenimiento()
              {                          
                       $OrgDepartamento = '<table  class="Form-Frame" cellpadding="0">';
                       $OrgDepartamento.= '       <tr height="30" style="display:none">';
                       $OrgDepartamento.= '           <td class="Form-Label" style="width:40%">Id</td>';
                       $OrgDepartamento.= '             <td class="Form-Label" style="width:60%">';
                       $OrgDepartamento.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $OrgDepartamento.= '             </td>';
                       $OrgDepartamento.= '         </tr>';                      
                       $OrgDepartamento.= '         <tr height="30">';
                       $OrgDepartamento.= '             <td class="Form-Label">Organigrama</td>';
                       $OrgDepartamento.= '             <td class="Form-Label">';
                                                   $OrgDepartamento.= $this->CreaComboOrganigrama();
                       $OrgDepartamento.= '             </td>';
                       $OrgDepartamento.= '         </tr>';
                       $OrgDepartamento.= '         <tr height="30">';
                       $OrgDepartamento.= '             <td class="Form-Label">Departamento Padre</td>';
                       $OrgDepartamento.= '             <td class="Form-Label">';
                                                   $OrgDepartamento.= $this->CreaComboDepartamentoPadre();
                       $OrgDepartamento.= '             </td>';
                       $OrgDepartamento.= '         </tr>';
                       $OrgDepartamento.= '         <tr height="30">';
                       $OrgDepartamento.= '             <td class="Form-Label">Departamentos</td>';
                       $OrgDepartamento.= '             <td class="Form-Label"><div id="SeccionDepartamentos"> </div></td>';
                       $OrgDepartamento.= '         </tr>';
                       $OrgDepartamento.= '</table>';
                       return $OrgDepartamento;
              }
              
              private function SearchGridValuesDepartamentos()
              {         $Columns['Id']       = array('0px','center','none');                       
                        $Columns['Chk'] = array('50px','center','');
                        $Columns['Descripci&oacute;n'] = array('150px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '240px','AltoRow' => '20px','FindTxt' =>false);
              }
              
              function CreaDepartamentosPorPadre()
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesDepartamentos());
                       $DepIndependientes = $this->TraeDatosDepartamentosIndependientes();
                       $ObjHTML = $this->CreaDepartamentosPrimeraVezGrid($SearchGrid,$DepIndependientes);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function CreaDepartamentosHijos($padre,$organigrama)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesDepartamentos());
                       $DepIndependientes = $this->TraeDatosDepartamentosIndependientes(); 
                       $DepRelacionados = $this->TraeDatosDepartamentosHijos($padre,$organigrama);                       
                       $Departamentos = $this->RecorreHijos($DepRelacionados,$DepIndependientes);
                       $ObjHTML = $this->CreaDepartamentosHijosGrid($SearchGrid,$Departamentos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function RecorreHijos($DepRelacionados,$DepIndependientes){
                       $Resultado = array();
                       $Resultado = $this->RecorreRelacionados($DepRelacionados,$Resultado);
                       $Resultado = $this->RecorreIndependientes($DepIndependientes,$Resultado);

                       return $Resultado;
              }
              
              function RecorreRelacionados($DepRelacionados,$Resultado)
              {      
                       foreach ($DepRelacionados as $relacionados)
                       {    array_push($Resultado,$relacionados);                            
                       } 
                       return $Resultado;                        
              }
              
              function RecorreIndependientes($DepIndependientes,$Resultado)
              {      
                       foreach ($DepIndependientes as $independientes)
                       {    array_push($Resultado,$independientes);                            
                       } 
                       return $Resultado;
              }
              
              function CreaOrgDepartamentoSearchGridPrimera()
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesDepartamentos());
                       $DepIndependientes = $this->TraeDatosDepartamentosPrimeraVez();
                       $ObjHTML = $this->CreaDepartamentosPrimeraVezGrid($SearchGrid,$DepIndependientes);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function CreaDepartamentosPrimeraVezGrid($ObjSchGrid,$DepIndependientes)
              {      
                       foreach ($DepIndependientes as $orgdepart)
                       {    $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'',$orgdepart['id']);
                            $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'','<input type="checkbox" value="'.$orgdepart['id'].'" id="chk'.$orgdepart['id'].'" name="chk'.$orgdepart['id'].'">');
                            $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'',trim($orgdepart['descripcion']));
                            $ObjSchGrid->CreaSearchRowsDetalle ($orgdepart['id'],"");
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                       
              }
              
              function CreaDepartamentosHijosGrid($ObjSchGrid,$DepIndependientes)
              {      
                       foreach ($DepIndependientes as $orgdepart)
                       {    $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'',$orgdepart['id']);
                     
                            if(isset($orgdepart['idOrganigrama'])){
                                $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'','<input type="hidden" value="'.$orgdepart['id'].'" id="id'.$orgdepart['id'].'" name="id'.$orgdepart['id'].'"><input type="checkbox" disabled value="'.$orgdepart['idDepartamento'].'" id="chk'.$orgdepart['idDepartamento'].'" name="chk'.$orgdepart['idDepartamento'].'" checked>');
                            }else{
                                $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'','<input type="checkbox" value="'.$orgdepart['id'].'" id="chk'.$orgdepart['id'].'" name="chk'.$orgdepart['id'].'" disabled>');
                            }

                            if(isset($orgdepart['departamento'])){
                                $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'',trim($orgdepart['departamento']));
                            }else{
                                $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'',trim($orgdepart['descripcion']));
                            }                               
                            $ObjSchGrid->CreaSearchRowsDetalle ($orgdepart['id'],"");
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                       
              }

              function CreaOrgDepartamentoSearchGrid($Datos)
              {                               
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());                       
                       $ObjHTML = $this->OrgDepartamentoGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
                              
              private function CreaComboOrganigrama()
                {       
                        $datosOrganigrama= $this->TraeDatosOrganigramas(array(1));
                        $Select = new ComboBox($this->Sufijo);
                        return $Select->Combo("Organigrama",$datosOrganigrama)->Selected(1)->Fields('id','version')->Create("s04");
                }
                
              private function CreaComboDepartamentoPadre()
                {       
                        $datosOrganigrama= $this->TraeDatosDepartamentos(array(1));
                        $Select = new ComboBox($this->Sufijo);                        
                        return $Select->Combo("Departamento",$datosOrganigrama)->Evento()->Selected(1)->Create("s04");
                }
                            
              function TraeDatosOrganigramas($Id) 
              {        
                       $ServicioOrganigrama = new ServicioOrganigrama();
                       $datoOrganigrama = $ServicioOrganigrama->BuscarOrganigramaByArrayID($Id);
                       return $datoOrganigrama;
              }
              
              function TraeDatosDepartamentos()
              {        
                       $ServicioOrgDepartamento = new ServicioOrgDepartamento();
                       $datoDepartamento = $ServicioOrgDepartamento->BuscarDepartamentosPadres();
                       return $datoDepartamento;
              }
              
              function TraeDatosOrgDepartamentos()
              {        
                       $ServicioOrgDepartamento = new ServicioOrgDepartamento();
                       $datoDepartamento = $ServicioOrgDepartamento->BuscarOrgDepartamentosGeneral();
                       return $datoDepartamento;
              }
              
              function TraeDatosDepartamentosIndependientes()
              {        
                       $ServicioDepartamento = new ServicioDepartamento();
                       $orgDeptGeneral = $this->TraeDatosOrgDepartamentos();                       
                       $datoDepartamento = $ServicioDepartamento->BuscarDepartamentosIndependientes($orgDeptGeneral);
                       return $datoDepartamento;
              }
              
              function TraeDatosDepartamentosHijos($padre,$organigrama)
              {        
                       $ServicioOrgDepartamento = new ServicioOrgDepartamento();                 
                       $datoDepartamento = $ServicioOrgDepartamento->BuscarDepartamentosRelacionados($padre,$organigrama);
                       return $datoDepartamento;
              }
              
              function TraeDatosDepartamentosPrimeraVez()
              {        $prepareDQL = array(1);
                       $ServicioDepartamento = new ServicioDepartamento();                     
                       $datoDepartamento = $ServicioDepartamento->BuscarDepartamentoByArrayID($prepareDQL);
                       return $datoDepartamento;
              }
              
              
              function MuestraOrgDepartamentoGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->OrgDepartamentoGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }                  
              
              private function OrgDepartamentoGridHTML($ObjSchGrid,$Datos)
              {   $padre = 0;
                  $padreprincipal = 0;
                  $sangria = '';
                  if(count($Datos)>0){
                     foreach ($Datos as $orgdepart)
                      {         
                               $id = str_pad($orgdepart['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($orgdepart['organigrama']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($orgdepart['departamentopadre']));
                               $departamento = trim($orgdepart['departamento']);
                               if($padre === 0){
                                   $padre = trim($orgdepart['idDepartamento']);
                                   $padreprincipal = $padre ;
                                   $ObjSchGrid->CreaSearchCellsDetalle($id,'',' '.$departamento);
                               }else{
                                   if($padre === trim($orgdepart['iddepartamentopadre'])){
                                       $sangria .= '......';
                                       $ObjSchGrid->CreaSearchCellsDetalle($id,'',$sangria.' '.$departamento);
                                   }elseif($padreprincipal === trim($orgdepart['iddepartamentopadre'])){
                                       $sangria = '......';
                                       $ObjSchGrid->CreaSearchCellsDetalle($id,'',$sangria.' '.$departamento);
                                       $padre = $padreprincipal ;                                       
                                   }else{
                                       $ObjSchGrid->CreaSearchCellsDetalle($id,'',$sangria.' '.$departamento);
                                   }
                                   $padre = trim($orgdepart['idDepartamento']);                                    
                               }                                
                               if(trim($orgdepart['iddepartamentopadre'])!=''){
                                   $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($orgdepart['iddepartamentopadre']));
                               }else{
                                   $ObjSchGrid->CreaSearchCellsDetalle($id,'',0); 
                               }                                  
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($orgdepart['idOrganigrama']));
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,"");                            
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }
               
              function MuestraDespuesdeGuardar($Ajax,$Id,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;
                        if (count($Datos)>0)
                        {   $Ajax->call($jsFun,$this->Sufijo,"GUARDADO",str_pad($Id,2,'0',STR_PAD_LEFT)); 
                            return $Ajax;
                        }
                }
                
              function MuestraErroralGuardar($Ajax,$Id,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;                
                        if (count($Datos)>0)
                        {   $Ajax->call($jsFun,$this->Sufijo,"EXCEPCION",$Id);    
                            return $Ajax;
                        }
                        
                }
                
              function MuestraDespuesdeEliminar($Ajax,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;
                        if (count($Datos)>0)  
                        {   $Ajax->call($jsFun,$this->Sufijo,"ELIMINADO",0);
                            return $Ajax;
                        }
                }
              
              
        }
?>

