<?php 
         $Sufijo = '_OrgDepartamento';

         require_once('src/modules/nomina/orgdepartamento/controlador/ControlOrgDepartamento.php');
         $ControlOrgDepartamento = new ControlOrgDepartamento($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlOrgDepartamento,'GuardaOrgDepartamento'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlOrgDepartamento,'MuestraOrgDepartamentoByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlOrgDepartamento,'MuestraOrgDepartamentoByTX'));
         $xajax->register(XAJAX_FUNCTION,array('ControlDepartamento'.$Sufijo, $ControlOrgDepartamento,'ControlDepartamento'));
         $xajax->register(XAJAX_FUNCTION,array('DepartamentosPrimeraVez'.$Sufijo, $ControlOrgDepartamento,'DepartamentosPrimeraVez'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlOrgDepartamento,'EliminaOrgDepartamento'));
         $xajax->register(XAJAX_FUNCTION,array('TraeDepartamentosHijos'.$Sufijo, $ControlOrgDepartamento,'TraeDepartamentosHijos'));
         $xajax->register(XAJAX_FUNCTION, manejadorExcepciones);
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript"> 
             
                 function ButtonClick_OrgDepartamento(Sufijo,Operacion)
                 {        var objOrgDepartamento = "id:Organigrama:Departamento";
                          var frmOrgDepartamento = "Organigrama:Departamento";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              if(document.getElementById("Departamento"+Sufijo)){
                                  ElementStatus(Sufijo,objOrgDepartamento,"id");
                                  ElementClear(Sufijo,objOrgDepartamento);                                  
                                  xajax_ControlDepartamento_OrgDepartamento();
                              }else{
                                  ElementStatus(Sufijo,"id:Organigrama","id");
                                  ElementClear(Sufijo,"id:Organigrama");
                                  xajax_DepartamentosPrimeraVez_OrgDepartamento();
                              }
                              setTimeout(function(){MuestraalGuardar_OrgDepartamento(1);},50);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objOrgDepartamento,"id");
                               MuestraalGuardar_OrgDepartamento(1)
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_OrgDepartamento(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"Organigrama"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = xajax.getFormValues('FormDatos');
                                       xajax_Guarda_OrgDepartamento(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",frmOrgDepartamento);
                               ElementClear(Sufijo,objOrgDepartamento);
                               document.getElementById('SeccionDepartamentos').innerHTML='';
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_OrgDepartamento(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_OrgDepartamento(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_OrgDepartamento(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_OrgDepartamento(Sufijo,Grilla)
                 {   
                    var Boton=document.getElementById('addSav'+Sufijo);
                        if(Boton.disabled===true){
                           if(Grilla.cells[4] && Grilla.cells[4].childNodes[0].nodeValue !== ''){
                                BarButtonState(Sufijo,"Active");
                                var padre = Grilla.cells[4].childNodes[0].nodeValue; 
                                var Organigrama = Grilla.cells[5].childNodes[0].nodeValue;
                                
                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue; 
                                document.getElementById("Organigrama"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                                document.getElementById("Departamento"+Sufijo).value = padre;

                                TraeDepartamentosHijos_OrgDepartamento(padre,Organigrama);
                           }
                          return false;
                        }else if(Boton.disabled===false){
                          Grilla.checked=true; 
                        }
                 }
                 
                 function ControlDepartamento_OrgDepartamento(Sufijo,Elemento){
                     var idPadre = Elemento.value;
                     xajax_ControlDepartamento_OrgDepartamento(idPadre);
                 }
                 
                 function TraeDepartamentosHijos_OrgDepartamento(Padre,Organigrama){
                     xajax_TraeDepartamentosHijos_OrgDepartamento(Padre,Organigrama);
                 }
                 
                 function MuestraalGuardar_OrgDepartamento(Opcion)
                 {        
                          var input = document.getElementsByTagName("input");
                          for (var i = 0; i < input.length; i++)
                          {  
                             if(input[i].id.substring(0,3)==='chk'){ 
                                 if(Opcion===0)
                                     input[i].disabled=true;
                                 else
                                     input[i].disabled=false;
                             }
                          }
                 }
                 
                 function XAJAXResponse_OrgDepartamento(Sufijo,Mensaje,Datos)
                 {       
                        var objOrgDepartamento = "descripcion:estado";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateDisabled(Sufijo,"btpadre");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objOrgDepartamento);
                            MuestraalGuardar_OrgDepartamento(0);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
<body>
         <div class="FormContainer" style="width:965px">
            <div class="FormContainerSeccion">
                <div class="FormBasic" style="width:400px" id="FormDatos">
                    <div class="FormSectionMenu">  
                        
                        <?php  $ControlOrgDepartamento->CargaOrgDepartamentoBarButton($_GET['opcion']);
                               echo '<script type="text/javascript">';
                               echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                               echo '</script>';
                        ?>
                   </div>    
                   <div class="FormSectionData">              
                       <form id="<?php echo 'Form'.$Sufijo; ?>">
                             <?php  $ControlOrgDepartamento->CargaOrgDepartamentoMantenimiento();  ?>
                        </form>
                   </div> 
                </div>               
            </div> 
            <div class="FormContainerSeccion" style="width:500px"> 
                <div class="FormSectionGrid">                  
                        <?php $ControlOrgDepartamento->CargaOrgDepartamentoSearchGrid();  ?>
                </div>
            </div>
         </div> 
        </body>
</html>
              

