<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
        require_once("src/rules/nomina/servicio/ServicioEmpleadoRubro.php");
        require_once("src/modules/nomina/empleadorubro/render/RenderEmpleadoRubro.php");
        require_once("src/rules/nomina/servicio/ServicioRubrosRol.php");
        require_once("src/modules/nomina/rubrosrol/render/RenderRubrosRol.php");
        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/modules/general/empleado/render/RenderEmpleado.php");
         
        class ControlEmpleadoRubro
        {       private  $Sufijo; 
                private  $ServicioEmpleadoRubro;
                private  $ServicioRubrosRol;
                private  $ServicioEmpleado;
                private  $RenderEmpleadoRubro;
                private  $RenderEmpleado;
                private  $RenderRubrosRol;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->ServicioEmpleadoRubro = new ServicioEmpleadoRubro();
                        $this->ServicioRubrosRol = new ServicioRubrosRol();
                        $this->ServicioEmpleado = new ServicioEmpleado();
                        $this->RenderEmpleadoRubro = new RenderEmpleadoRubro($Sufijo);
                        $this->RenderRubrosRol = new RenderRubrosRol($Sufijo);
                        $this->RenderEmpleado = new RenderEmpleado($Sufijo);
                }

                function CargaEmpleadoRubroBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaEmpleadoRubroMantenimiento()
                {       echo $this->RenderEmpleadoRubro->CreaEmpleadoRubroMantenimiento();
                }

                function CargaEmpleadoRubroSearchGrid($IdEmpleado)
                {       $datoEmpleadoRubro = $this->ServicioEmpleadoRubro->BuscarEmpleadoRubro(array('empleado'=>$IdEmpleado));
                        echo $this->RenderEmpleadoRubro->CreaEmpleadoRubroSearchGrid($datoEmpleadoRubro); 
                }         
               
                function ConsultaByEmpleadoID($IdEmpleado)
                {       $ajaxRespon = new xajaxResponse();                   
                        $datoEmpleadoRubro = $this->ServicioEmpleadoRubro->BuscarEmpleadoRubro(array('empleado'=>intval($IdEmpleado)));
                        return $this->RenderEmpleadoRubro->MuestraEmpleadoRubroBuscado($ajaxRespon,$datoEmpleadoRubro);                        
                }

                function GuardaEmpleadoRubro($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $EmpleadoRubro  = json_decode($Form)->Forma;

                        if (empty($EmpleadoRubro->id)){
                            $id = $this->ServicioEmpleadoRubro->Guarda($EmpleadoRubro);
                        }
                        else{
                            $id = $this->ServicioEmpleadoRubro->Edita($EmpleadoRubro);
                        }    
                        $Datos = $this->ServicioEmpleadoRubro->BuscarEmpleadoRubro(array('empleado'=>$EmpleadoRubro->idempleado));
                        return $this->RenderEmpleadoRubro->MuestraEmpleadoRubroGuardado($ajaxRespon,$id,$Datos);                            
                } 
                
                function EliminaEmpleadoRubro($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioEmpleadoRubro->DesactivaEmpleadoRubro(intval($id));
                        $Datos = $this->ServicioEmpleadoRubro->BuscarEmpleadoRubro(array('id' => $id));
                        return $this->RenderEmpleadoRubro->MuestraEmpleadoRubroEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridEmpleadoRubro($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        if ($Operacion==="btrubrorol")    
                        {   $prepareDQL = array('limite' => 50,'orden'=>' r.idmovimiento, r.ordenrol ','estado' => '1','variable'=>0,'movimnto'=>'1,2,4');
                            $Nombre="Rubros";    
                            $Datos = $this->ServicioRubrosRol->BuscarRubrosRol($prepareDQL);                            
                            $jsonModal['Carga'] = $this->RenderRubrosRol->CreaModalGridRubrosRol($Operacion,$Datos);
                            $jsonModal['Ancho'] = "438";
                        }else{
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                            $Nombre="Empleado";
                            $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);                           
                            $jsonModal['Carga'] = $this->RenderEmpleado->CreaModalGrid($Operacion,$Datos);
                            $jsonModal['Ancho'] = "563";
                        }  
                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca ".$Nombre;
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                         
                function ConsultaModalGridEmpleadoRubroByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);                        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto),'estado' => '1','variable'=>0,'movimnto'=>'1,2');
                        if ($Operacion==="btrubrorol")    
                        {   $Datos = $this->ServicioRubrosRol->BuscarRubrosRol($prepareDQL);
                            return $this->RenderRubrosRol->MuestraModalGridRubrosRol($ajaxResp,$Operacion,$Datos);                                        
                        }else{
                            $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);
                            return $this->RenderEmpleado->MuestraModalGrid($ajaxResp,$Operacion,$Datos);     
                        }    
                }
         }

?>

