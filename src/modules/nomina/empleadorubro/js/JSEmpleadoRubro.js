        function ButtonClick_EmpleadoRubro(Sufijo,Operacion)
        {       if (Operacion=='addNew')
                {   BarButtonState(Sufijo,Operacion);
                    ElementStatus(Sufijo,"txrubrorol:idrubrorol:valor:aportaIESS","txempleado:idempleado");
                    BarButtonStateDisabled(Sufijo,"btempleado");
                    BarButtonStateEnabled(Sufijo,"btrubrorol");
                    ElementClear(Sufijo,"id:txrubrorol:idrubrorol:valor");
                    ElementSetValue(Sufijo, "estado", "1");
                }
                else if (Operacion=='addMod')
                {   BarButtonState(Sufijo, Operacion);
                    BarButtonStateDisabled(Sufijo, "btempleado");
                    BarButtonStateEnabled(Sufijo,"btrubrorol");
                    if (ElementGetValue(Sufijo, "estado") == 0){
                        ElementStatus(Sufijo, "txrubrorol:idrubrorol:valor:estado:aportaIESS", "idempleado:txempleado");
                    }else{   
                        ElementStatus(Sufijo, "txrubrorol:idrubrorol:valor:aportaIESS", "idempleado:txempleado");
                    }                    
                }
                else if (Operacion=='addDel')
                {   if (ElementGetValue(Sufijo,"estado")==1)
                    {   if (confirm("Desea inactivar este registro?"))
                        xajax_Elimina_EmpleadoRubro(ElementGetValue(Sufijo,"id"));
                    }
                    else
                    alert("El registro se encuentra inactivo");
                }
                else if (Operacion=='addSav')
                {   if (ElementValidateBeforeSave(Sufijo,"txrubrorol:idrubrorol:valor"))
                    {   if (BarButtonState(Sufijo,"Inactive"))
                        {   var Forma = PrepareElements(Sufijo,"id:txrubrorol:idrubrorol:valor:idempleado:txempleado:estado:aportaIESS");
                            xajax_Guarda_EmpleadoRubro(JSON.stringify({Forma}));
                        }
                    }
                }
                else if (Operacion=='addCan')
                {   BarButtonState(Sufijo, Operacion);
                    BarButtonStateEnabled(Sufijo, "btempleado");
                    BarButtonStateDisabled(Sufijo,"btrubrorol");
                    ElementStatus(Sufijo, "idempleado:txempleado", "txrubrorol:idrubrorol:valor:estado:aportaIESS");
                    ElementClear(Sufijo, "id:txrubrorol:idrubrorol:valor");
                }
                else 
                xajax_CargaModal_EmpleadoRubro(Operacion);  
                
                return false;
        }

        function SearchByElement_EmpleadoRubro(Sufijo,Elemento)
        {       Elemento.value = TrimElement(Elemento.value);
                if (Elemento.id === "FindEmpleadoTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_EmpleadoRubro("btempleado", Elemento.value);
                }
                else if (Elemento.id === "FindRubrorolTextBx" + Sufijo)
                {   xajax_BuscaModalByTX_EmpleadoRubro("btrubrorol", Elemento.value);
                }
                //else    
                //xajax_MuestraByTX_EmpleadoRubro(Elemento.value);    
                return false;
        }   
                 
        function SearchGetData_EmpleadoRubro(Sufijo,Grilla)
        {       if (IsDisabled(Sufijo,"addSav")) 
                {   BarButtonState(Sufijo,"Active");
                    document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;                                                   
                  //document.getElementById("idempleado"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;      
                  //document.getElementById("txempleado"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                    document.getElementById("idrubrorol"+Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;                          
                    document.getElementById("txrubrorol"+Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                    document.getElementById("valor"+Sufijo).value = Grilla.cells[3].childNodes[0].nodeValue;
                    document.getElementById("estado"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue; 
                    document.getElementById("aportaIESS"+Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;                          
                }    
                return false;
        }
                 
        function SearchEmpleadoGetData_EmpleadoRubro(Sufijo,DatosGrid)
        {       ElementClear(Sufijo,"id:txrubrorol:idrubrorol:valor");

                var Id = DatosGrid.cells[0].childNodes[0].nodeValue;
                document.getElementById("idempleado"+Sufijo).value = Id;
                document.getElementById("txempleado"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue; 
                xajax_BuscaByEmpleadoID_EmpleadoRubro(Id); 
                cerrar();
                return false;
        }
                 
        function SearchRubrorolGetData_EmpleadoRubro(Sufijo,DatosGrid)
        {       document.getElementById("idrubrorol"+Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
                document.getElementById("txrubrorol"+Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
                cerrar();
                return false;
        }                 
                 
        function XAJAXResponse_EmpleadoRubro(Sufijo,Mensaje,Datos)
        {       if (Mensaje==="GUARDADO")
                {   BarButtonState(Sufijo,"Active");
                    BarButtonStateEnabled(Sufijo,"btempleado");
                    BarButtonStateDisabled(Sufijo,"btrubrorol");
                    ElementSetValue(Sufijo,"id",Datos);
                    ElementStatus(Sufijo,"txempleado:idempleado","txrubrorol:idrubrorol:valor:estado:aportaIESS");
                    alert('Los datos se guardaron correctamente.');
                }
                else if(Mensaje==='ELIMINADO')
                {   ElementSetValue(Sufijo,"estado",Datos);
                    alert("Los Datos se eliminaron correctamente");
                }
                else if(Mensaje==='NOEXISTEID')
                {   BarButtonState(Sufijo,'Default');
                    ElementClear(Sufijo,"id");
                    alert(Datos);
                }
                else if(Mensaje==='EXCEPCION')
                {   BarButtonState(Sufijo,"addNew"); 
                    alert(Datos);
                } 
        }
