<?php
         $Sufijo = '_EmpleadoRubro';
         
        require_once('src/modules/nomina/empleadorubro/controlador/ControlEmpleadoRubro.php');
        $ControlEmpleadoRubro = new ControlEmpleadoRubro($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlEmpleadoRubro,'GuardaEmpleadoRubro'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlEmpleadoRubro,'EliminaEmpleadoRubro'));         
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlEmpleadoRubro,'CargaModalGridEmpleadoRubro'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlEmpleadoRubro,'ConsultaModalGridEmpleadoRubroByTX'));                                                                                       
        $xajax->register(XAJAX_FUNCTION,array('BuscaByEmpleadoID'.$Sufijo, $ControlEmpleadoRubro,'ConsultaByEmpleadoID'));
        $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/nomina/empleadorubro/js/JSEmpleadoRubro.js"></script>
         </head>
        <body>
         <div class="FormBasic" style="width:475px">
               <div class="FormSectionMenu">              
              <?php  $ControlEmpleadoRubro->CargaEmpleadoRubroBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Inactive"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlEmpleadoRubro->CargaEmpleadoRubroMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlEmpleadoRubro->CargaEmpleadoRubroSearchGrid(0);  ?>
              </div>  
         </div> 
        </body>
        </html>
              

