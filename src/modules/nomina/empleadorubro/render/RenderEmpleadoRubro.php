<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/rules/nomina/servicio/ServicioRubrosRol.php");
        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderEmpleadoRubro
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;  
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 3;                     
                }
              
                function CreaOpcionBarButton($Buttons)
                {       $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
                }
               
                function CreaEmpleadoRubroMantenimiento()
                {       $Search = new SearchInput($this->Sufijo);

                        $EmpleadoRubro = '<table border="0" class="Form-Frame" cellpadding="0">';
                        
                        $EmpleadoRubro.= '       <tr height="30">';
                        $EmpleadoRubro.= '           <td class="Form-Label">Empleado</td>';
                        $EmpleadoRubro.= '           <td class="Form-Label" colspan="3">';
                        $EmpleadoRubro.=                 $Search->TextSch("empleado","","")->Enabled(true)->Create("t12");
                        $EmpleadoRubro.= '           </td>';
                        $EmpleadoRubro.= '       </tr>';   

                        $EmpleadoRubro.= '       <tr height="30">';
                        $EmpleadoRubro.= '           <td class="Form-Label">Id</td>';
                        $EmpleadoRubro.= '           <td class="Form-Label">';
                        $EmpleadoRubro.= '               <input type="text" class="txt-input t02" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'txempleado:idempleado:txrubrorol:idrubrorol:valor\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);" disabled/>';
                        $EmpleadoRubro.= '           </td>';                        
                        $EmpleadoRubro.= '           <td class="Form-Label"></td>';
                        $EmpleadoRubro.= '           <td class="Form-Label">';
                        $EmpleadoRubro.= '           </td>';
                        $EmpleadoRubro.= '       </tr>';
                        
                        $EmpleadoRubro.= '       <tr height="30">';                       
                        $EmpleadoRubro.= '           <td class="Form-Label" style="width:22%">Rubro</td>';
                        $EmpleadoRubro.= '           <td class="Form-Label" style="width:35%">';
                        $EmpleadoRubro.=                 $Search->TextSch("rubrorol","","")->Enabled(false)->Create("t05");
                        $EmpleadoRubro.= '           </td>';
                        $EmpleadoRubro.= '           <td class="Form-Label" style="width:18%">Valor $</td>';
                        $EmpleadoRubro.= '           <td class="Form-Label" style="width:25%">';
                        $EmpleadoRubro.= '               <input type="text" onKeyDown="return soloFloat(event,this,2); " class="txt-right t03" id="valor'.$this->Sufijo.'" name="valor'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                        $EmpleadoRubro.= '           </td>';                        
                        $EmpleadoRubro.= '       </tr>';   

                        $EmpleadoRubro.= '       <tr height="30">';                       
                        $EmpleadoRubro.= '           <td class="Form-Label">Aporta IESS</td>';
                        $EmpleadoRubro.= '           <td class="Form-Label">';
                        $EmpleadoRubro.=                 $this->CreaComboAportaIESS("s02");
                        $EmpleadoRubro.= '           </td>';
                        $EmpleadoRubro.= '           <td class="Form-Label">Estado</td>';
                        $EmpleadoRubro.= '           <td class="Form-Label">';
                        $EmpleadoRubro.=                 $this->CreaComboEstado();
                        $EmpleadoRubro.= '           </td>';
                        $EmpleadoRubro.= '       </tr>';   


                        $EmpleadoRubro.= '</table>';
                        return $EmpleadoRubro;
                }
              
                private function SearchGridValues()
                {       $Columns['Id']         = array('40px','center','');
                        $Columns['Empleado']   = array('250px','left','none');                        
                        $Columns['Rubro']      = array('152px','left','');
                        $Columns['Valor']      = array('90px','right','');
                        $Columns['Estado']     = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        $Columns['idEmpleado'] = array('0px','left','none');                        
                        $Columns['idRubro']    = array('0px','left','none');
                        $Columns['idaporta']   = array('0px','left','none');
                        $Columns['Aporta']     = array('70px','left','');
                        return array('FindTxt'=>false,'AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
                }

                function CreaEmpleadoRubroSearchGrid($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->EmpleadoRubroGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }
              
                private function EmpleadoRubroGridHTML($ObjSchGrid,$Datos)
                {       foreach ($Datos as $cargo)
                        {       $id = str_pad($cargo['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $valor = number_format($cargo['valor'],"2",".",""); 

                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['empleado']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($cargo['rubro']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$valor);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['txtestado']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['idestado']); 
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['idempleado']);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['idrubrorol']);                               
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cargo['aportaiess']);                               
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',($cargo['aportaiess']==0 ? "NO" : "SI"));                               
                                
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($cargo['idestado']==0 ? "red" : ""));
                        }
                        return $ObjSchGrid->CreaSearchTableDetalle(); 
                }

                function TraeDatosEstado($IdArray)
                {       $ServicioEstado = new ServicioEstado();
                        $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                        return $datoEstado;
                }

                private function CreaComboEstado()
                {       $datosEstado= $this->TraeDatosEstado(array(0,1));
                        $Select = new ComboBox($this->Sufijo);                        
                        return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
                }
              
                function MuestraEmpleadoRubroGuardado($Ajax,$id,$EmpleadoRubro)
                {       if (count($EmpleadoRubro)>0)
                        {   $idx = str_pad($id,$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraEmpleadoRubroGrid($Ajax,$EmpleadoRubro);
                            return $this->RespuestaEmpleadoRubro($xAjax,"GUARDADO",$idx);
                        }
                        return $this->RespuestaEmpleadoCargas($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraEmpleadoRubroBuscado($Ajax,$EmpleadoRubro)
                {       $Ajax->call("BarButtonState",$this->Sufijo,"Default");
                        $xAjax = $this->MuestraEmpleadoRubroGrid($Ajax,$EmpleadoRubro);
                        return $xAjax;
                }

                function MuestraEmpleadoRubroEliminado($Ajax,$EmpleadoRubro)
                {       foreach ($EmpleadoRubro as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraEmpleadoRubroRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaEmpleadoRubro($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->RespuestaEmpleadoRubro($xAjax,"EXCEPCION","No se eliminó la información.");
                }
                
                function MuestraEmpleadoRubroGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->EmpleadoRubroGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }   
                
                function MuestraEmpleadoRubroRowGrid($Ajax,$EmpleadoRubro,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($EmpleadoRubro['id'],$estado);
                        $valor = number_format($EmpleadoRubro['valor'],"2",".",""); 
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($EmpleadoRubro['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($EmpleadoRubro['empleado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($EmpleadoRubro['rubro']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$valor);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($EmpleadoRubro['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$EmpleadoRubro['idestado']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$EmpleadoRubro['idempleado']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$EmpleadoRubro['idrubrorol']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",$EmpleadoRubro['aportaiess']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",($EmpleadoRubro['aportaiess']==0 ? "NO" : "SI"));
                        
                        return $Ajax;
                }

                private function RespuestaEmpleadoRubro($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
                private function CreaComboAportaIESS($Size)
                {       $Combo = '<select id="aportaIESS'.$this->Sufijo.'" name="aportaIESS'.$this->Sufijo.'" class="sel-input '.$Size.'" disabled>';
                        $Combo.= '<option value=1>SI</option>';
                        $Combo.= '<option value=0>NO</option>';
                        $Combo.= '</select>';
                        return $Combo; 
                }        
                
        }
?>

