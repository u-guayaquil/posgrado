<?php
         require_once("src/rules/nomina/servicio/ServicioEmpleadoAsistencia.php");
         require_once("src/rules/general/servicio/ServicioEmpleado.php");
         require_once("src/rules/nomina/servicio/ServicioEmpleadoPermisos.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");         
         require_once("src/modules/nomina/empleadoasistencia/render/RenderEmpleadoAsistencia.php");
         require_once("src/modules/general/empleado/render/RenderEmpleado.php"); 
         require_once("src/modules/nomina/empleadopermisos/render/RenderEmpleadoPermisos.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlEmpleadoAsistencia
         {     private  $Sufijo; 
               private  $ServicioEmpleadoAsistencia;
               private  $ServicioEmpleado;
               private  $ServicioEmpleadoPermisos;
               private  $RenderEmpleadoAsistencia;
               private  $RenderEmpleado;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioEmpleadoAsistencia = new ServicioEmpleadoAsistencia();
                        $this->ServicioEmpleado = new ServicioEmpleado();
                        $this->ServicioEmpleadoPermisos = new ServicioEmpleadoPermisos();
                        $this->RenderEmpleadoAsistencia = new RenderEmpleadoAsistencia($Sufijo);
                        $this->RenderEmpleado = new RenderEmpleado($Sufijo);
                        $this->RenderEmpleadoPermisos = new RenderEmpleadoPermisos($Sufijo);
               }

               function CargaEmpleadoAsistenciaBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaEmpleadoAsistenciaMantenimiento()
               {        echo $this->RenderEmpleadoAsistencia->CreaEmpleadoAsistenciaMantenimiento();
               }

               function CargaEmpleadoAsistenciaSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoEmpleadoAsistencia = $this->ServicioEmpleadoAsistencia->BuscarEmpleadoAsistencia($prepareDQL);
                        echo $this->RenderEmpleadoAsistencia->CreaEmpleadoAsistenciaSearchGrid($datoEmpleadoAsistencia); 
               }         
               
               function MuestraEmpleadoAsistenciaByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoEmpleadoAsistencia = $this->ServicioEmpleadoAsistencia->BuscarEmpleadoAsistencia($prepareDQL);
                        return $this->RenderEmpleadoAsistencia->MuestraEmpleadoAsistencia($ajaxRespon,$datoEmpleadoAsistencia);
               }

               function MuestraEmpleadoAsistenciaByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoEmpleadoAsistencia = $this->ServicioEmpleadoAsistencia->BuscarEmpleadoAsistencia($prepareDQL);
                        return $this->RenderEmpleadoAsistencia->MuestraEmpleadoAsistenciaGrid($ajaxRespon,$datoEmpleadoAsistencia);
               }
               
               function MuestraPermisos($id,$idp,$permiso)
               {        $ajaxRespon = new xajaxResponse();
                        if($idp == ''){
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $datoEmpleadoAsistencia = $this->ServicioEmpleadoPermisos->BuscarPermisosByEmpleadoyFechas($prepareDQL);
                        }else{
                            $datoEmpleadoAsistencia = $this->ServicioEmpleadoPermisos->BuscarEmpleadoPermisosById(intval($permiso));
                        }
                        
                        return $this->RenderEmpleadoAsistencia->MuestraPermisosCombo($ajaxRespon,$datoEmpleadoAsistencia);
               }

               function GuardaEmpleadoAsistencia($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $EmpleadoAsistencia  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioEmpleadoAsistencia->GuardaDBEmpleadoAsistencia($EmpleadoAsistencia);
                        if (is_numeric($id)){
                            $function = (empty($EmpleadoAsistencia->id) ? "MuestraEmpleadoAsistenciaGuardado" : "MuestraEmpleadoAsistenciaEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioEmpleadoAsistencia->BuscarEmpleadoAsistencia($prepareDQL);
                            return $this->RenderEmpleadoAsistencia->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderEmpleadoAsistencia->MuestraEmpleadoAsistenciaExcepcion($ajaxRespon,($id));
                        }                   
               } 
               
               function EliminaEmpleadoAsistencia($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioEmpleadoAsistencia->DesactivaEmpleadoAsistencia(intval($id));
                        $Datos = $this->ServicioEmpleadoAsistencia->BuscarEmpleadoAsistencia($prepareDQL);
                        return $this->RenderEmpleadoAsistencia->MuestraEmpleadoAsistenciaEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridEmpleadoAsistencia($Operacion)
                {       $ajaxResp = new xajaxResponse();   
                        if ($Operacion==="btpermiso")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'estado' => '1');
                            $Datos = $this->ServicioEmpleadoPermisos->BuscarEmpleadoPermisos($prepareDQL);                            
                            $Nombre="Permisos";
                        }else{
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                            $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);                           
                            $Nombre="Empleado";
                        }   

                        $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                        $jsonModal['Title'] = "Busca ".$Nombre;
                        if ($Operacion==="btpermiso")    
                        {   $jsonModal['Carga'] = $this->RenderEmpleadoPermisos->CreaModalGridEmpleadoPermisos($Operacion,$Datos);
                        }else{
                            $jsonModal['Carga'] = $this->RenderEmpleado->CreaModalGrid($Operacion,$Datos);
                        }              
                        $jsonModal['Ancho'] = "503";
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }

                function ConsultaModalEmpleadoAsistenciaGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);                        
                        /*$prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        $Datos = $this->ServicioEmpleado->BuscarEmpleado($prepareDQL);
                        return $this->RenderEmpleado->MuestraModalGridEmpleado($ajaxResp,$Operacion,$Datos);*/ 
                        
                        if ($Operacion==="btpermiso")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'estadomodal' => '1','texto' => strtoupper($texto));
                            $Datos = $this->ServicioEmpleadoPermisos->BuscarEmpleadoPermisos($prepareDQL);                            
                            return $this->RenderEmpleadoPermisos->MuestraModalGridEmpleadoPermisos($ajaxResp,$Operacion,$Datos);           
                        }else{
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                            $Datos = $this->ServicioEmpleado->BuscarByNombre($prepareDQL);                           
                            return $this->RenderEmpleado->MuestraModalGrid($ajaxResp,$Operacion,$Datos);           
                        }   
                }
         }

?>

