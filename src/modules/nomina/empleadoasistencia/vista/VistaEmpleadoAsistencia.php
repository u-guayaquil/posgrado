<?php
         $Sufijo = '_EmpleadoAsistencia';
         
         require_once('src/modules/nomina/empleadoasistencia/controlador/ControlEmpleadoAsistencia.php');
         $ControlEmpleadoAsistencia = new ControlEmpleadoAsistencia($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlEmpleadoAsistencia,'GuardaEmpleadoAsistencia'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlEmpleadoAsistencia,'MuestraEmpleadoAsistenciaByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlEmpleadoAsistencia,'MuestraEmpleadoAsistenciaByTX'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlEmpleadoAsistencia,'EliminaEmpleadoAsistencia'));         
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlEmpleadoAsistencia,'CargaModalGridEmpleadoAsistencia'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlEmpleadoAsistencia,'ConsultaModalEmpleadoAsistenciaGridByTX')); 
         $xajax->register(XAJAX_FUNCTION,array('MuestraPermisos'.$Sufijo, $ControlEmpleadoAsistencia,'MuestraPermisos')); 
         $xajax->processRequest();
?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/nomina/empleadoasistencia/js/JSEmpleadoAsistencia.js"></script>
         </head>
        <body>
         <div class="FormBasic" style="width:690px">
               <div class="FormSectionMenu">              
              <?php  $ControlEmpleadoAsistencia->CargaEmpleadoAsistenciaBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlEmpleadoAsistencia->CargaEmpleadoAsistenciaMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlEmpleadoAsistencia->CargaEmpleadoAsistenciaSearchGrid();  ?>
              </div>  
         </div> 
        </body>
        </html>
              

