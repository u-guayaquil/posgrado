<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/general/servicio/ServicioEmpleado.php");
        require_once("src/rules/nomina/servicio/ServicioParentesco.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once("src/libs/clases/DateControl.php");        
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderEmpleadoAsistencia
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;  
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 4;                     
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
               
              function CreaEmpleadoAsistenciaMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);
                       $frmEmpleadoAsistencia = "txempleado:idempleado:feentrada:fesalida:Alsalida:Alentrada:hentrada:hsalida:halsalida:halentrada"
                                               .":permiso:estado";
                       $EmpleadoAsistencia = '<table class="Form-Frame" cellpadding="0">';
                       $EmpleadoAsistencia.= '       <tr height="30">';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:17%">Id</td>';
                       $EmpleadoAsistencia.= '             <td class="Form-Label" style="width:30%">';
                       $EmpleadoAsistencia.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\''.$frmEmpleadoAsistencia.'\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $EmpleadoAsistencia.= '             </td>';   
                       $EmpleadoAsistencia.= '             <td class="Form-Label">Estado</td>';
                       $EmpleadoAsistencia.= '             <td class="Form-Label">';
                                                    $EmpleadoAsistencia.= $this->CreaComboEstado();
                       $EmpleadoAsistencia.= '             </td>';
                       $EmpleadoAsistencia.= '       </tr>';
                       $EmpleadoAsistencia.= '         <tr height="30">';
                       $EmpleadoAsistencia.= '           <td class="Form-Label">Empleado</td>';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" colspan="6">';
                                              $EmpleadoAsistencia.= $Search->TextSch("empleado","","")->Enabled(false)->Create("t12");
                       $EmpleadoAsistencia.= '           </td>';
                       $EmpleadoAsistencia.= '         </tr>';   
                       $EmpleadoAsistencia.= '         <tr height="30">';
                       $EmpleadoAsistencia.= '           <td class="Form-Label">Permiso</td>';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" colspan="3">';
                       $EmpleadoAsistencia.= '             <div id="divPermiso'.$this->Sufijo.'">     <select id="permiso'.$this->Sufijo.'" name="permiso'.$this->Sufijo.'" class="sel-input s04" disabled>';
                       $EmpleadoAsistencia.= '                         <option value="0">NINGUNO</option>';
                       $EmpleadoAsistencia.= '                 </select> </div>';
                       $EmpleadoAsistencia.= '           </td>';                       
                       $EmpleadoAsistencia.= '         </tr>';
                       $EmpleadoAsistencia.= '         <tr height="30">';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:20%">Entrada</td>';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:30%">'
                                                        . '<input class="txt-upper t03" name = "feentrada'.$this->Sufijo.'" id = "feentrada'.$this->Sufijo.'" disabled/> &nbsp;- &nbsp;'
                                                        . '<input class="txt-upper t02" name = "hentrada'.$this->Sufijo.'" id = "hentrada'.$this->Sufijo.'" disabled/></td>';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:23%">Salida</td>';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:40%">'
                                                        . '<input class="txt-upper t03" name = "fesalida'.$this->Sufijo.'" id = "fesalida'.$this->Sufijo.'" disabled/> &nbsp;- &nbsp;'
                                                        . '<input class="txt-upper t02" name = "hsalida'.$this->Sufijo.'" id = "hsalida'.$this->Sufijo.'" disabled/></td>';
                       $EmpleadoAsistencia.= '         </tr>'; 
                       $EmpleadoAsistencia.= '         <tr height="30">';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:20%">Salida Almuerzo</td>';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:30%">'
                                                        . '<input class="txt-upper t03" name = "Alsalida'.$this->Sufijo.'" id = "Alsalida'.$this->Sufijo.'" disabled/> &nbsp;- &nbsp;'
                                                        . '<input class="txt-upper t02" name = "halsalida'.$this->Sufijo.'" id = "halsalida'.$this->Sufijo.'" disabled/></td>';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:23%">Entrada Almuerzo</td>';
                       $EmpleadoAsistencia.= '           <td class="Form-Label" style="width:40%">'
                                                        . '<input class="txt-upper t03" name = "Alentrada'.$this->Sufijo.'" id = "Alentrada'.$this->Sufijo.'" disabled/> &nbsp;- &nbsp;'
                                                        . '<input class="txt-upper t02" name = "halentrada'.$this->Sufijo.'" id = "halentrada'.$this->Sufijo.'" disabled/></td>';
                       $EmpleadoAsistencia.= '         </tr>';
                       $EmpleadoAsistencia.= '</table>';
                       return $EmpleadoAsistencia;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('20px','center','');
                        $Columns['Empleado'] = array('180px','left','');                        
                        $Columns['Entrada'] = array('110px','left','center');
                        $Columns['salida'] = array('110px','left','center');
                        $Columns['Sal. Almuerzo']   = array('105px','center','');
                        $Columns['Ent. Almuerzo']   = array('105px','center','');
                        $Columns['idEstado']   = array('0px','left','none');
                        $Columns['idEmpleado']   = array('0px','left','none');                    
                        $Columns['feentrada']   = array('0px','left','none');
                        $Columns['fesalida']   = array('0px','left','none');
                        $Columns['hentrada']   = array('0px','left','none');
                        $Columns['hsalida']   = array('0px','left','none');
                        $Columns['alentrada']   = array('0px','left','none');
                        $Columns['alhentrada']   = array('0px','left','none');
                        $Columns['alsalida']   = array('0px','left','none');
                        $Columns['alhsalida']   = array('0px','left','none');
                        $Columns['permiso']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }

              function CreaEmpleadoAsistenciaSearchGrid($Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->EmpleadoAsistenciaGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              private function EmpleadoAsistenciaGridHTML($ObjSchGrid,$Datos)
              {       
                  if(count($Datos)>0){
                     foreach ($Datos as $asistencia)
                      {        $id = str_pad($asistencia['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                               $fechaentradaR = $this->Fechas->changeFormatDate($asistencia['feentrada'],"DMY");
                               $fechasalidaR = $this->Fechas->changeFormatDate($asistencia['fesalida'],"DMY");
                               $AlfechaentradaR = $this->Fechas->changeFormatDate($asistencia['entradaalmuerzo'],"DMY");
                               $AlfechasalidaR = $this->Fechas->changeFormatDate($asistencia['salidaalmuerzo'],"DMY");
                               
                               $feentrada = explode(' ',trim($asistencia['feentrada']));
                               $fechaentrada = $this->Fechas->changeFormatDate($feentrada[0],"DMY")[1];
                               $horaentrada = $feentrada[1];
                               $fesalida = explode(' ',trim($asistencia['fesalida']));
                               $fechasalida = $this->Fechas->changeFormatDate($fesalida[0],"DMY")[1];
                               $horasalida = $fesalida[1];
                               $Alfeentrada = explode(' ',trim($asistencia['entradaalmuerzo']));
                               $Alfechaentrada = $this->Fechas->changeFormatDate($Alfeentrada[0],"DMY")[1];
                               $Alhoraentrada = $Alfeentrada[1];
                               $Alfesalida = explode(' ',trim($asistencia['salidaalmuerzo']));
                               $Alfechasalida = $this->Fechas->changeFormatDate($Alfesalida[0],"DMY")[1];
                               $Alhorasalida = $Alfesalida[1];
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($asistencia['empleado']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($fechaentradaR[1].' '.$fechaentradaR[2]));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($fechasalidaR[1].' '.$fechasalidaR[2]));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($AlfechaentradaR[1].' '.$AlfechaentradaR[2]));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($AlfechasalidaR[1].' '.$AlfechasalidaR[2]));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$asistencia['idestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($asistencia['idempleado']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($fechaentrada));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($fechasalida));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($horaentrada));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($horasalida));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Alfechaentrada));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Alhoraentrada));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Alfechasalida));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Alhorasalida));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($asistencia['idpermiso']));
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($asistencia['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }

              function TraeDatosEstado($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstado(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function MuestraEmpleadoAsistencia($Ajax,$Datos)
              {        if($Datos[0]['id'] != ''){
                            $feentrada = explode(' ',trim($Datos[0]['feentrada']));
                            $fechaentrada = $this->Fechas->changeFormatDate($feentrada[0],"DMY")[1];
                            $horaentrada = $feentrada[1];
                            $fesalida = explode(' ',trim($Datos[0]['fesalida']));
                            $fechasalida = $this->Fechas->changeFormatDate($fesalida[0],"DMY")[1];
                            $horasalida = $fesalida[1];
                            $Alfeentrada = explode(' ',trim($Datos[0]['entradaalmuerzo']));
                            $Alfechaentrada = $this->Fechas->changeFormatDate($Alfeentrada[0],"DMY")[1];
                            $Alhoraentrada = $Alfeentrada[1];
                            $Alfesalida = explode(' ',trim($Datos[0]['salidaalmuerzo']));
                            $Alfechasalida = $this->Fechas->changeFormatDate($Alfesalida[0],"DMY")[1];
                            $Alhorasalida = $Alfesalida[1];
                            $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("txempleado".$this->Sufijo,"value", trim($Datos[0]['empleado']));    
                            $Ajax->Assign("idempleado".$this->Sufijo,"value", trim($Datos[0]['idempleado']));                        
                            $Ajax->Assign("feentrada".$this->Sufijo,"value", trim($fechaentrada)); 
                            $Ajax->Assign("hentrada".$this->Sufijo,"value", trim($horaentrada)); 
                            $Ajax->Assign("fesalida".$this->Sufijo,"value", trim($fechasalida)); 
                            $Ajax->Assign("hsalida".$this->Sufijo,"value", trim($horasalida)); 
                            $Ajax->Assign("Alsalida".$this->Sufijo,"value", trim($Alfechasalida)); 
                            $Ajax->Assign("halsalida".$this->Sufijo,"value", trim($Alhorasalida)); 
                            $Ajax->Assign("Alentrada".$this->Sufijo,"value", trim($Alfechaentrada)); 
                            $Ajax->Assign("halentrada".$this->Sufijo,"value", trim($Alhoraentrada)); 
                            $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active"); 
                            $Ajax->call("cargaCombo",$this->Sufijo,$Datos[0]['empleado'],$Datos[0]['idpermiso'],$Datos[0]['id']); 
                            return $Ajax;
                       }else{
                            return $this->RespuestaEmpleadoAsistencia($Ajax,"NOEXISTEID","No existe Asistencias con ese ID.");
                       }
              }
              
              function MuestraPermisosCombo($Ajax,$Datos)
              {        $ObjHTML ='<select id="permiso'.$this->Sufijo.'" name="permiso'.$this->Sufijo.'" class="sel-input s04" disabled>';
                       if(count($Datos)>0){
                            foreach ($Datos as $permisos)
                            {    $ObjHTML .='<option value="'.$permisos['idmotivo'].'">'.$permisos['motivo'].'</option>';
                            }
                       }else{
                            $ObjHTML .='<option value="0">NINGUNO</option>';
                       }
                       $ObjHTML .='</select>';
                       $Ajax->Assign('divPermiso'.$this->Sufijo,"innerHTML", $ObjHTML);
                       return $Ajax;
              }   
              
              function MuestraEmpleadoAsistenciaGrid($Ajax,$Datos)
              {                              
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->EmpleadoAsistenciaGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign('divPermiso',"innerHTML", $ObjHTML);
                       return $Ajax;
              }   
               
              function MuestraEmpleadoAsistenciaGuardado($Ajax,$EmpleadoAsistencia)
              {       if (count($EmpleadoAsistencia)>0)
                        {   $id = str_pad($EmpleadoAsistencia[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraEmpleadoAsistenciaGrid($Ajax,$EmpleadoAsistencia);
                            return $this->RespuestaEmpleadoAsistencia($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaEmpleadoAsistencia($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraEmpleadoAsistenciaEditado($Ajax,$EmpleadoAsistencia)
              {       foreach ($EmpleadoAsistencia as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraEmpleadoAsistenciaRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaEmpleadoAsistencia($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaEmpleadoAsistencia($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraEmpleadoAsistenciaEliminado($Ajax,$EmpleadoAsistencia)
              {        foreach ($EmpleadoAsistencia as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraEmpleadoAsistenciaRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaEmpleadoAsistencia($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaEmpleadoAsistencia($xAjax,"EXCEPCION","No se eliminó la información.");
              }
              
              function MuestraEmpleadoAsistenciaRowGrid($Ajax,$EmpleadoAsistencia,$estado=1)
              {         $Fila = $this->SearchGrid->GetRow($EmpleadoAsistencia['id'],$estado);
                        $fechaentradaR = $this->Fechas->changeFormatDate($EmpleadoAsistencia['feentrada'],"DMY");
                        $feentrada = explode(' ',trim($EmpleadoAsistencia['feentrada']));
                        $fechaentrada = $this->Fechas->changeFormatDate($feentrada[0],"DMY");
                        $horaentrada = $feentrada[1];
                        $fesalida = explode(' ',trim($EmpleadoAsistencia['fesalida']));
                        $fechasalida = $this->Fechas->changeFormatDate($fesalida[0],"DMY");
                        $horasalida = $fesalida[1];
                        $Alfeentrada = explode(' ',trim($EmpleadoAsistencia['entradaalmuerzo']));
                        $Alfechaentrada = $this->Fechas->changeFormatDate($Alfeentrada[0],"DMY");
                        $Alhoraentrada = $Alfeentrada[1];
                        $Alfesalida = explode(' ',trim($EmpleadoAsistencia['salidaalmuerzo']));
                        $Alfechasalida = $this->Fechas->changeFormatDate($Alfesalida[0],"DMY");
                        $Alhorasalida = $Alfesalida[1];
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($EmpleadoAsistencia['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($EmpleadoAsistencia['empleado']));                       
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($fechaentradaR[1]." ".$fechaentradaR[2]));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($EmpleadoAsistencia['fesalida']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($EmpleadoAsistencia['salidaalmuerzo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($EmpleadoAsistencia['entradaalmuerzo']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($EmpleadoAsistencia['idestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($EmpleadoAsistencia['idempleado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($EmpleadoAsistencia['idempleado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",trim($fechaentrada));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",trim($fechasalida));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],11),"innerHTML",trim($horaentrada));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],12),"innerHTML",trim($horasalida));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],13),"innerHTML",trim($Alfechaentrada));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],14),"innerHTML",trim($Alhoraentrada));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],15),"innerHTML",trim($Alfechasalida));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],16),"innerHTML",trim($Alhorasalida));
                        return $Ajax;
              }

              function MuestraEmpleadoAsistenciaExcepcion($Ajax,$Msg)
              {         return $this->RespuestaEmpleadoAsistencia($Ajax,"EXCEPCION",$Msg);    
              }

              private function RespuestaEmpleadoAsistencia($Ajax,$Tipo,$Data)
              {         $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
              }
        }
?>

