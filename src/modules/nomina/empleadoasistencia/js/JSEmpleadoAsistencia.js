function CargaFecha_EmpleadoAsistencia(Sufijo) {
    $('#feentrada' + Sufijo).datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });
    $('#fesalida' + Sufijo).datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });
    $('#Alsalida' + Sufijo).datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });
    $('#Alentrada' + Sufijo).datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });
    $("#hentrada" + Sufijo).mask("99:99");
    $("#hsalida" + Sufijo).mask("99:99");
    $("#halsalida" + Sufijo).mask("99:99");
    $("#halentrada" + Sufijo).mask("99:99");
}

function ButtonClick_EmpleadoAsistencia(Sufijo, Operacion)
{
    var objEmpleadoAsistencia = "id:txempleado:idempleado:feentrada:fesalida:Alsalida:Alentrada:hentrada:hsalida:halsalida:halentrada:estado:permiso";
    var frmEmpleadoAsistencia = "txempleado:idempleado:feentrada:fesalida:Alsalida:Alentrada:hentrada:hsalida:halsalida:halentrada:estado:permiso";

    if (Operacion == 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, objEmpleadoAsistencia, "id:estado");
        BarButtonStateEnabled(Sufijo, "btempleado");
        ElementClear(Sufijo, objEmpleadoAsistencia);
        CargaFecha_EmpleadoAsistencia(Sufijo);
        SeteaFechasAlDia(Sufijo);
        ElementStatus(Sufijo, "", "permiso");
    } else if (Operacion == 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, objEmpleadoAsistencia, "id");
        CargaFecha_EmpleadoAsistencia(Sufijo);
        BarButtonStateDisabled(Sufijo,"btempleado");
        ElementStatus(Sufijo, "", "txempleado:permiso");
        if(ElementGetValue(Sufijo,"estado")==='0'){
            ElementStatus(Sufijo, "estado", "");
        }else{
            ElementStatus(Sufijo, "", "estado");
        }
    } else if (Operacion == 'addDel')
    {
        if (ElementGetValue(Sufijo, "estado") == 1)
        {
            if (confirm("Desea inactivar este registro?"))
                xajax_Elimina_EmpleadoAsistencia(ElementGetValue(Sufijo, "id"));
        } else
            alert("El registro se encuentra inactivo");
    } else if (Operacion == 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, "txempleado"))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {
                var Forma = PrepareElements(Sufijo, objEmpleadoAsistencia);
                xajax_Guarda_EmpleadoAsistencia(JSON.stringify({Forma}));
            }
        }
    } else if (Operacion == 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, "id", frmEmpleadoAsistencia);
        ElementClear(Sufijo, objEmpleadoAsistencia);
        BarButtonStateDisabled(Sufijo, "btempleado");
        document.getElementById('divPermiso'+Sufijo).innerHTML='<select id="permiso'+Sufijo+'" name="permiso'+Sufijo+'" class="sel-input s04" disabled><option value="0">NINGUNO</option></select>';
    } else if (Operacion == 'addImp')
    {
    } else {
        xajax_CargaModal_EmpleadoAsistencia(Operacion);
    }
    return false;
}


function FechasporDefecto() {
    var d = new Date();
    var aa = d.getFullYear();
    var dd = d.getDate();
    var mm = d.getMonth() + 1;
    if (dd < 10) {
        dia = "0" + dd;
    } else {
        dia = dd;
    }
    if (mm < 10) {
        mes = "0" + mm;
    } else {
        mes = mm;
    }

    var fecha = dia + '/' + mes + '/' + aa;
    return  fecha;
}

function SeteaFechasAlDia(Sufijo) {

    document.getElementById("feentrada" + Sufijo).value = FechasporDefecto();
    document.getElementById("fesalida" + Sufijo).value = FechasporDefecto();
    document.getElementById("Alsalida" + Sufijo).value = FechasporDefecto();
    document.getElementById("Alentrada" + Sufijo).value = FechasporDefecto();
}

function SearchByElement_EmpleadoAsistencia(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    switch (Elemento.id) {
        case "id" + Sufijo:
            if (Elemento.value.length != 0)
            {
                if (BarButtonState(Sufijo, "Inactive")) {
                    xajax_MuestraByID_EmpleadoAsistencia(Elemento.value);
                }
            } else
                BarButtonState(Sufijo, "Default");
            break;
        case "FindEmpleadoTextBx" + Sufijo:
            xajax_BuscaModalByTX_EmpleadoAsistencia("btempleado", Elemento.value);
            break;
        case 'FindTextBx' + Sufijo:
            xajax_MuestraByTX_EmpleadoAsistencia(Elemento.value);
            break;
    }
}

function SearchGetData_EmpleadoAsistencia(Sufijo, Grilla)
{
    if (IsDisabled(Sufijo, "addSav")) {
        var frmEmpleadoAsistencia = "txempleado:idempleado:feentrada:fesalida:Alsalida:Alentrada:hentrada:hsalida:halsalida:halentrada:estado:permiso";
        BarButtonState(Sufijo, "Active");
        ElementClear(Sufijo, frmEmpleadoAsistencia);
        var fesalida = '';
        var hsalida = '';
        var Alsalida = '';
        var halsalida = '';
        var Alentrada = '';
        var halentrada = '';
        document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("idempleado" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
        document.getElementById("txempleado" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
        document.getElementById("feentrada" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
        document.getElementById("hentrada" + Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
        if (Grilla.cells[9].childNodes[0].nodeValue.trim() === '01/01/1900') {
            fesalida = '';
        } else {
            fesalida = Grilla.cells[9].childNodes[0].nodeValue.trim();
        }
        if (Grilla.cells[11].childNodes[0].nodeValue.trim() === '00:00:00') {
            hsalida = '';
        } else {
            hsalida = Grilla.cells[11].childNodes[0].nodeValue.trim();
        }
        if (Grilla.cells[12].childNodes[0].nodeValue.trim() === '01/01/1900') {
            Alsalida = '';
        } else {
            Alsalida = Grilla.cells[12].childNodes[0].nodeValue.trim();
        }
        if (Grilla.cells[13].childNodes[0].nodeValue.trim() === '00:00:00') {
            halsalida = '';
        } else {
            halsalida = Grilla.cells[13].childNodes[0].nodeValue.trim();
        }
        if (Grilla.cells[14].childNodes[0].nodeValue.trim() === '01/01/1900') {
            Alentrada = '';
        } else {
            Alentrada = Grilla.cells[14].childNodes[0].nodeValue.trim();
        }
        if (Grilla.cells[15].childNodes[0].nodeValue.trim() === '00:00:00') {
            halentrada = '';
        } else {
            halentrada = Grilla.cells[15].childNodes[0].nodeValue.trim();
        }
        document.getElementById("fesalida" + Sufijo).value = fesalida;
        document.getElementById("hsalida" + Sufijo).value = hsalida;
        document.getElementById("Alsalida" + Sufijo).value = Alsalida;
        document.getElementById("halsalida" + Sufijo).value = halsalida;
        document.getElementById("Alentrada" + Sufijo).value = Alentrada;
        document.getElementById("halentrada" + Sufijo).value = halentrada;
        document.getElementById("estado" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;  
        cargaCombo(Grilla.cells[7].childNodes[0].nodeValue
                  ,Grilla.cells[0].childNodes[0].nodeValue
                  ,Grilla.cells[16].childNodes[0].nodeValue);
        ElementStatus(Sufijo, "", "permiso");
    }
    return false;
}

function SearchEmpleadoGetData_EmpleadoAsistencia(Sufijo, DatosGrid)
{
    document.getElementById("idempleado" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txempleado" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue.trim();
    document.getElementById("divPermiso" + Sufijo).innerHTML = '';
    var id = document.getElementById('id'+Sufijo).value;
    cargaCombo(DatosGrid.cells[0].childNodes[0].nodeValue,id,0);
    cerrar();
    return false;
}

function cargaCombo(empleado,permiso,id){    
    xajax_MuestraPermisos_EmpleadoAsistencia(empleado,permiso,id);
}

function XAJAXResponse_EmpleadoAsistencia(Sufijo, Mensaje, Datos)
{
    var objEmpleadoAsistencia = "txempleado:idempleado:feentrada:fesalida:Alsalida:Alentrada:hentrada:hsalida:halsalida:halentrada:estado";
    if (Mensaje === "GUARDADO")
    {
        BarButtonState(Sufijo, "Active");
        BarButtonStateDisabled(Sufijo, "btempleado");
        ElementSetValue(Sufijo, "id", Datos);
        ElementStatus(Sufijo, "id", objEmpleadoAsistencia);
        alert('Los datos se guardaron correctamente.');
    } else if (Mensaje === 'ELIMINADO')
    {
        ElementSetValue(Sufijo, "estado", Datos);
        alert("Los Datos se eliminaron correctamente");
    } else if (Mensaje === 'NOEXISTEID')
    {
        BarButtonState(Sufijo, 'Default');
        ElementClear(Sufijo, "id");
        alert(Datos);
    } else if (Mensaje === 'EXCEPCION')
    {
        BarButtonState(Sufijo, "addNew");
        alert(Datos);
    }
}