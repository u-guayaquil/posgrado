<?php
         require_once("src/rules/nomina/servicio/ServicioOrgDepartamento.php");
         require_once("src/rules/nomina/servicio/ServicioOrgDepCargo.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/modules/nomina/orgdepcargo/render/RenderOrgDepCargo.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlOrgDepCargo
         {     private  $Sufijo; 
               private  $ServicioOrgDepCargo;
               private  $RenderOrgDepCargo;

               function __construct($Sufijo = "")
               {        
                        $this->ServicioOrgDepCargo = new ServicioOrgDepCargo();
                        $this->RenderOrgDepCargo = new RenderOrgDepCargo($Sufijo);
               }

               function CargaOrgDepCargoBarButton($Opcion)
                {       $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Datos = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $this->RenderOrgDepCargo->CreaOpcionBarButton($Datos);
                }
               
               function CargaOrgDepCargoMantenimiento()
               {        
                        echo $this->RenderOrgDepCargo->CreaOrgDepCargoMantenimiento();
               }
               
               function CargosPrimeraVez()
               {        $ajaxRespon = new xajaxResponse();
                        $tablaDepartamentos = $this->RenderOrgDepCargo->CreaCargosSearchGridPrimera();
                        $ajaxRespon->Assign("SeccionCargos","innerHTML",$tablaDepartamentos);
                        return $ajaxRespon;
               }
               
               function ControlDepCargo()
               {        $ajaxRespon = new xajaxResponse();
                        $tablaDepartamentos = $this->RenderOrgDepCargo->CreaCargosPorPadre();
                        $ajaxRespon->Assign("SeccionCargos","innerHTML",$tablaDepartamentos);
                        return $ajaxRespon;
               }
               
               function TraeDepartamentosHijos($padre,$organigrama)
               {        $ajaxRespon = new xajaxResponse();
                        $tablaDepartamentos = $this->RenderOrgDepCargo->CreaCargosHijos($padre,$organigrama);
                        $ajaxRespon->Assign("SeccionCargos","innerHTML",$tablaDepartamentos);
                        return $ajaxRespon;
               }
               
               function CargaDepartamentos()
               {        echo $this->RenderOrgDepCargo->CreaDepartamentosPadres();
               }

               function CargaOrgDepCargoSearchGrid()
               {        $ServicioOrganigrama = new ServicioOrganigrama();
                        $Organigrama = $ServicioOrganigrama->BuscarOrganigramaByArrayID('');
                        $datoOrgDepartamento = $this->ServicioOrgDepartamento->BuscarOrgDepartamentosGeneral(0,$Organigrama);
                        echo $this->RenderOrgDepCargo->CreaOrgDepartamentoSearchGrid($datoOrgDepartamento); 
               }         
               
               function MuestraOrgDepCargoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $datoOrgDepartamento = $this->ServicioOrgDepartamento->BuscarOrgDepartamentoByID($id);
                        return $this->RenderOrgDepCargo->MuestraOrgDepartamento($ajaxRespon,$datoOrgDepartamento);
               }

               function MuestraOrgDepCargoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoOrgDepartamento = $this->ServicioOrgDepartamento->BuscarOrgDepartamentoByDescripcion($prepareDQL);
                        return $this->RenderOrgDepCargo->MuestraOrgDepartamentoGrid(new SearchGrid($this->Sufijo),$ajaxRespon,$datoOrgDepartamento);
               }

               function GuardaOrgDepCargo($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $OrgDepCargo  = json_decode($Form)->Forma;
                        
                        if (empty($OrgDepCargo->id))
                        {   list($estado,$Id) = $this->ServicioOrgDepCargo->CreaOrgDepCargo($OrgDepCargo);                            
                        }else{
                            list($estado,$Id) = $this->ServicioOrgDepCargo->EditaOrgDepCargo($OrgDepCargo);
                        }    
                        //$Datos = $this->ServicioOrgDepartamento->BuscarOrgDepCargosGeneral();
                        /*
                        if($estado){
                            return $this->RenderOrgDepCargo->MuestraDespuesdeGuardar($ajaxRespon,$Id,($Datos));                    
                        }else{
                            return $this->RenderOrgDepCargo->MuestraErroralGuardar($ajaxRespon,$Id,($Datos));                    
                        }  */                      
               } 
               
               function EliminaOrgDepCargo($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $this->ServicioOrgDepartamento->DesactivaOrgDepartamento($id);
                        $Datos = $this->ServicioOrgDepartamento->BuscarOrgDepartamentoByDescripcion($prepareDQL);
                        return $this->RenderOrgDepCargo->MuestraDespuesdeEliminar($ajaxRespon,$Datos);
                }
               
         }

?>

