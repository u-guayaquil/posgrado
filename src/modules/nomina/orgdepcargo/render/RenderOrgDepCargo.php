<?php    

        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once("src/rules/nomina/servicio/ServicioOrgDepartamento.php");
        require_once("src/rules/nomina/servicio/ServicioCargo.php");
        require_once("src/rules/nomina/servicio/ServicioOrgDepCargo.php");
        
        require_once('src/libs/clases/ComboBox.php');
        require_once('src/libs/clases/SearchInput.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderOrgDepCargo
        {     private $Sufijo;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;                       
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');                       
                        $Columns['Departamento'] = array('100px','left','');
                        $Columns['Cargo Padre'] = array('0px','left','none');
                        $Columns['Cargo'] = array('250px','left','');                        
                        $Columns['idCargoPadre'] = array('0px','left','none');
                        $Columns['idDepartamento'] = array('0px','left','none');
                        
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '440px','AltoRow' => '20px','FindTxt' =>false);
              }
               
              function CreaOrgDepCargoMantenimiento()
              {                          
                       $OrgDepCargo = '<table  class="Form-Frame" cellpadding="0">';
                       $OrgDepCargo.= '       <tr height="30" style="display:none">';
                       $OrgDepCargo.= '           <td class="Form-Label" style="width:40%">Id</td>';
                       $OrgDepCargo.= '             <td class="Form-Label" style="width:60%">';
                       $OrgDepCargo.= '                 <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $OrgDepCargo.= '             </td>';
                       $OrgDepCargo.= '         </tr>';                      
                       $OrgDepCargo.= '         <tr height="30">';
                       $OrgDepCargo.= '             <td class="Form-Label">Departamento</td>';
                       $OrgDepCargo.= '             <td class="Form-Label">';
                                                   $OrgDepCargo.= $this->CreaComboDepartamento();
                       $OrgDepCargo.= '             </td>';
                       $OrgDepCargo.= '         </tr>';
                       $OrgDepCargo.= '         <tr height="30">';
                       $OrgDepCargo.= '             <td class="Form-Label">Cargo Padre</td>';
                       $OrgDepCargo.= '             <td class="Form-Label">';
                                                   $OrgDepCargo.= $this->CreaComboCargoPadre();
                       $OrgDepCargo.= '             </td>';
                       $OrgDepCargo.= '         </tr>';
                       $OrgDepCargo.= '         <tr height="30">';
                       $OrgDepCargo.= '             <td class="Form-Label">Cargos</td>';
                       $OrgDepCargo.= '             <td class="Form-Label"><div id="SeccionCargos"> </div></td>';
                       $OrgDepCargo.= '         </tr>';
                       $OrgDepCargo.= '</table>';
                       return $OrgDepCargo;
              }
              
              private function SearchGridValuesCargos()
              {         $Columns['Id']       = array('0px','center','none');                       
                        $Columns['Chk'] = array('50px','center','');
                        $Columns['Descripci&oacute;n'] = array('150px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '240px','AltoRow' => '20px','FindTxt' =>false);
              }
              
              function CreaCargosPorPadre()
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesCargos());
                       $CargosIndependientes = $this->TraeDatosCargosIndependientes();
                       $ObjHTML = $this->CreaCargosPrimeraVezGrid($SearchGrid,$CargosIndependientes);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function CreaCargosHijos($padre,$departamento)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesCargos());
                       $CargosIndependientes = $this->TraeDatosCargosIndependientes(); 
                       $CargosRelacionados = $this->TraeDatosCargosHijos($padre,$departamento);                       
                       $Cargos = $this->RecorreHijos($CargosRelacionados,$CargosIndependientes);
                       $ObjHTML = $this->CreaDepartamentosHijosGrid($SearchGrid,$Cargos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function RecorreHijos($CargosRelacionados,$Cargosndependientes){
                       $Resultado = array();
                       $Resultado = $this->RecorreRelacionados($CargosRelacionados,$Resultado);
                       $Resultado = $this->RecorreIndependientes($Cargosndependientes,$Resultado);

                       return $Resultado;
              }
              
              function RecorreRelacionados($CargosRelacionados,$Resultado)
              {      
                       foreach ($CargosRelacionados as $relacionados)
                       {    array_push($Resultado,$relacionados);                            
                       } 
                       return $Resultado;                        
              }
              
              function RecorreIndependientes($Cargosndependientes,$Resultado)
              {      
                       foreach ($Cargosndependientes as $independientes)
                       {    array_push($Resultado,$independientes);                            
                       } 
                       return $Resultado;
              }
              
              function CreaCargosSearchGridPrimera()
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesCargos());
                       $CargosIndependientes = $this->TraeDatosCargosPrimeraVez();
                       $ObjHTML = $this->CreaCargosPrimeraVezGrid($SearchGrid,$CargosIndependientes);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function CreaCargosPrimeraVezGrid($ObjSchGrid,$CargosIndependientes)
              {      
                       foreach ($CargosIndependientes as $cargos)
                       {    $ObjSchGrid->CreaSearchCellsDetalle($cargos['id'],'',$cargos['id']);
                            $ObjSchGrid->CreaSearchCellsDetalle($cargos['id'],'','<input type="checkbox" value="'.$cargos['id'].'" id="chk'.$cargos['id'].'" name="chk'.$cargos['id'].'">');
                            $ObjSchGrid->CreaSearchCellsDetalle($cargos['id'],'',trim($cargos['descripcion']));
                            $ObjSchGrid->CreaSearchRowsDetalle ($cargos['id'],"");
                       }
                       return $ObjSchGrid->CreaSearchTableDetalle(); 
                       
              }
              
              function CreaCargosHijosGrid($ObjSchGrid,$CargosIndependientes)
              {      
                       foreach ($CargosIndependientes as $orgdepart)
                       {    $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'',$orgdepart['id']);
                     
                            if(isset($orgdepart['idDepartamento'])){
                                $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'','<input type="hidden" value="'.$orgdepart['id'].'" id="id'.$orgdepart['id'].'" name="id'.$orgdepart['id'].'"><input type="checkbox" disabled value="'.$orgdepart['idCargo'].'" id="chk'.$orgdepart['idCargo'].'" name="chk'.$orgdepart['idCargo'].'" checked>');
                            }else{
                                $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'','<input type="checkbox" value="'.$orgdepart['id'].'" id="chk'.$orgdepart['id'].'" name="chk'.$orgdepart['id'].'" disabled>');
                            }

                            if(isset($orgdepart['cargo'])){
                                $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'',trim($orgdepart['cargo']));
                            }else{
                                $ObjSchGrid->CreaSearchCellsDetalle($orgdepart['id'],'',trim($orgdepart['descripcion']));
                            }                               
                            $ObjSchGrid->CreaSearchRowsDetalle ($orgdepart['id'],"");
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                       
              }

              function CreaOrgDepCargoSearchGrid($Datos)
              {                               
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());                       
                       $ObjHTML = $this->OrgDepCargoGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
                              
              private function CreaComboDepartamento()
                {       
                        $datosDepartamento = $this->TraeDatosDepartamentos(array(1));
                        $Select = new ComboBox($this->Sufijo);
                        return $Select->Combo("Departamento",$datosDepartamento)->Selected(1)->Create("s04");
                }
                
              private function CreaComboCargoPadre()
                {       
                        $datosCargo= $this->TraeDatosCargos(array(1));
                        $Select = new ComboBox($this->Sufijo);                        
                        return $Select->Combo("Cargo",$datosCargo)->Evento()->Selected(1)->Create("s04");
                }
                            
              function TraeDatosDepartamentos($Id) 
              {        
                       $ServicioDepartamento = new ServicioOrgDepartamento();
                       $datoDepartamento = $ServicioDepartamento->BuscarDepartamentosPadres($Id);
                       return $datoDepartamento;
              }
              
              function TraeDatosCargos()
              {        
                       $ServicioOrgDepCargo = new ServicioOrgDepCargo();
                       $datoCargo = $ServicioOrgDepCargo->BuscarCargosPadres();
                       return $datoCargo;
              }
              
              function TraeDatosOrgDepCargos()
              {        
                       $ServicioOrgDepCargo = new ServicioOrgDepCargo();
                       $datoDepartamento = $ServicioOrgDepCargo->BuscarOrgDepCargoGeneral();
                       return $datoDepartamento;
              }
              
              function TraeDatosCargosIndependientes()
              {        
                       $ServicioCargo = new ServicioCargo();
                       $orgDeptGeneral = $this->TraeDatosOrgDepCargos();                       
                       $datoCargo = $ServicioCargo->BuscarCargosIndependientes($orgDeptGeneral);
                       return $datoCargo;
              }
              
              function TraeDatosCargosHijos($padre,$organigrama)
              {        
                       $ServicioOrgDepCargo = new ServicioOrgDepCargo();                 
                       $datoCargo = $ServicioOrgDepCargo->BuscarCargosRelacionados($padre,$organigrama);
                       return $datoCargo;
              }
              
              function TraeDatosCargosPrimeraVez()
              {        $prepareDQL = array(1);
                       $ServicioCargo = new ServicioCargo();                     
                       $datoCargo = $ServicioCargo->BuscarCargoByArrayID($prepareDQL);
                       return $datoCargo;
              }
              
              
              function MuestraOrgDepCargoGrid($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->OrgDepCargoGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }                  
              
              private function OrgDepCargoGridHTML($ObjSchGrid,$Datos)
              {   $padre = 0;
                  $padreprincipal = 0;
                  $sangria = '';
                  if(count($Datos)>0){
                     foreach ($Datos as $orgdepart)
                      {         
                               $id = str_pad($orgdepart['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                             
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($orgdepart['departamento']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($orgdepart['cargopadre']));
                               $cargo = trim($orgdepart['cargo']);
                               if($padre === 0){
                                   $padre = trim($orgdepart['idCargo']);
                                   $padreprincipal = $padre ;
                                   $ObjSchGrid->CreaSearchCellsDetalle($id,'',' '.$departamento);
                               }else{
                                   if($padre === trim($orgdepart['idcargopadre'])){
                                       $sangria .= '......';
                                       $ObjSchGrid->CreaSearchCellsDetalle($id,'',$sangria.' '.$cargo);
                                   }elseif($padreprincipal === trim($orgdepart['idcargopadre'])){
                                       $sangria = '......';
                                       $ObjSchGrid->CreaSearchCellsDetalle($id,'',$sangria.' '.$cargo);
                                       $padre = $padreprincipal ;                                       
                                   }else{
                                       $ObjSchGrid->CreaSearchCellsDetalle($id,'',$sangria.' '.$cargo);
                                   }
                                   $padre = trim($orgdepart['idCargo']);                                    
                               }                                
                               if(trim($orgdepart['idcargopadre'])!=''){
                                   $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($orgdepart['idcargopadre']));
                               }else{
                                   $ObjSchGrid->CreaSearchCellsDetalle($id,'',0); 
                               }                                  
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($orgdepart['idDepartamento']));
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,"");                            
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle(); 
                  }                  
              }
               
              function MuestraDespuesdeGuardar($Ajax,$Id,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;
                        if (count($Datos)>0)
                        {   $Ajax->call($jsFun,$this->Sufijo,"GUARDADO",str_pad($Id,2,'0',STR_PAD_LEFT)); 
                            return $Ajax;
                        }
                }
                
              function MuestraErroralGuardar($Ajax,$Id,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;                
                        if (count($Datos)>0)
                        {   $Ajax->call($jsFun,$this->Sufijo,"EXCEPCION",$Id);    
                            return $Ajax;
                        }
                        
                }
                
              function MuestraDespuesdeEliminar($Ajax,$Datos)
                {       $jsFun = "XAJAXResponse".$this->Sufijo;
                        if (count($Datos)>0)  
                        {   $Ajax->call($jsFun,$this->Sufijo,"ELIMINADO",0);
                            return $Ajax;
                        }
                }
              
              
        }
?>

