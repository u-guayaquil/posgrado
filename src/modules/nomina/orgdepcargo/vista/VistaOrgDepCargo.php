<?php 
         $Sufijo = '_OrgDepCargo';

         require_once('src/modules/nomina/orgdepcargo/controlador/ControlOrgDepCargo.php');
         $ControlOrgDepCargo = new ControlOrgDepCargo($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlOrgDepCargo,'GuardaOrgDepCargo'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlOrgDepCargo,'MuestraOrgDepCargoByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlOrgDepCargo,'MuestraOrgDepCargoByTX'));
         $xajax->register(XAJAX_FUNCTION,array('ControlDepCargo'.$Sufijo, $ControlOrgDepCargo,'ControlDepCargo'));
         $xajax->register(XAJAX_FUNCTION,array('CargosPrimeraVez'.$Sufijo, $ControlOrgDepCargo,'CargosPrimeraVez'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlOrgDepCargo,'EliminaOrgDepCargo'));
         $xajax->register(XAJAX_FUNCTION,array('TraeDepCargosHijos'.$Sufijo, $ControlOrgDepCargo,'TraeDepCargosHijos'));
         $xajax->register(XAJAX_FUNCTION, manejadorExcepciones);
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>

                <?php $xajax->printJavascript(); ?>
                <?php require_once('src/utils/links.php'); ?>
         <script type="text/javascript"> 
             
                 function ButtonClick_OrgDepCargo(Sufijo,Operacion)
                 {        var objOrgDepCargo = "id:Departamento:Cargo";
                     
                          if (Operacion=='addNew')
                          {   BarButtonState(Sufijo,Operacion);
                              if(document.getElementById("Cargo"+Sufijo)){
                                  ElementStatus(Sufijo,objOrgDepCargo,"id");
                                  ElementClear(Sufijo,"id:"+objOrgDepCargo);                                  
                                  xajax_ControlDepCargo_OrgDepCargo();
                              }else{
                                  ElementStatus(Sufijo,"id:Departamento","id");
                                  ElementClear(Sufijo,"id:Departamento");
                                  xajax_CargosPrimeraVez_OrgDepCargo();
                              }
                              setTimeout(function(){MuestraalGuardar_OrgDepCargo(1);},50);
                          }
                          else if (Operacion=='addMod')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,objOrgDepCargo,"id");
                               MuestraalGuardar_OrgDepCargo(1)
                          }
                          else if (Operacion=='addDel')
                          {    if (ElementGetValue(Sufijo,"estado")==1)
                                {   if (confirm("Desea inactivar este registro?"))
                                    xajax_Elimina_OrgDepCargo(ElementGetValue(Sufijo,"id"));
                                }
                                else
                                alert("El registro se encuentra inactivo");
                          }
                          else if (Operacion=='addSav')
                          {    if (ElementValidateBeforeSave(Sufijo,"Departamento"))
                               {   if (BarButtonState(Sufijo,"Inactive"))
                                   {   var Forma = xajax.getFormValues('FormDatos');
                                       xajax_Guarda_OrgDepCargo(JSON.stringify({Forma}));
                                   }
                               }
                          }
                          else if (Operacion=='addCan')
                          {    BarButtonState(Sufijo,Operacion);
                               ElementStatus(Sufijo,"id",objOrgDepCargo);
                               ElementStatus(Sufijo,"id",'');
                               ElementClear(Sufijo,"id:"+objOrgDepCargo);
                               document.getElementById('SeccionCargos').innerHTML='';
                          }
                          else if (Operacion=='addImp')
                          {
                          }
                          return false;
                 }

                 function SearchByElement_OrgDepCargo(Sufijo,Elemento)
                 {        Elemento.value = TrimElement(Elemento.value);
                          if (Elemento.name=="id"+Sufijo)
                          {   if (Elemento.value.length != 0)
                              {   ContentFlag = document.getElementById("descripcion"+Sufijo);
                                  if (ContentFlag.value.length==0)
                                  {   if (BarButtonState(Sufijo,"Inactive"))
                                      xajax_MuestraByID_OrgDepCargo(Elemento.value);
                                  }
                              }
                              else
                              BarButtonState(Sufijo,"Default");
                          }
                          else
                          {   xajax_MuestraByTX_OrgDepCargo(Elemento.value);    
                          }    
                 }
                 
                 function SearchGetData_OrgDepCargo(Sufijo,Grilla)
                 {   
                    var Boton=document.getElementById('addSav'+Sufijo);
                        if(Boton.disabled===true){
                           if(Grilla.cells[4] && Grilla.cells[4].childNodes[0].nodeValue !== ''){
                                BarButtonState(Sufijo,"Active");
                                var padre = Grilla.cells[4].childNodes[0].nodeValue; 
                                var Organigrama = Grilla.cells[5].childNodes[0].nodeValue;
                                
                                document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue; 
                                document.getElementById("Departamento"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                                document.getElementById("Cargo"+Sufijo).value = padre;

                                TraeDepCargosHijos_OrgDepCargo(padre,Organigrama);
                           }
                          return false;
                        }else if(Boton.disabled===false){
                          Grilla.checked=true; 
                        }
                 }
                 
                 function ControlDepCargo_OrgDepCargo(Sufijo,Elemento){
                     var idPadre = Elemento.value;
                     xajax_ControlDepCargo_OrgDepCargo(idPadre);
                 }
                 
                 function TraeDepCargosHijos_OrgDepCargo(Padre,Organigrama){
                     xajax_TraeDepCargosHijos_OrgDepCargo(Padre,Organigrama);
                 }
                 
                 function MuestraalGuardar_OrgDepCargo(Opcion)
                 {        
                          var input = document.getElementsByTagName("input");
                          for (var i = 0; i < input.length; i++)
                          {  
                             if(input[i].id.substring(0,3)==='chk'){ 
                                 if(Opcion===0)
                                     input[i].disabled=true;
                                 else
                                     input[i].disabled=false;
                             }
                          }
                 }
                 
                 function XAJAXResponse_OrgDepCargo(Sufijo,Mensaje,Datos)
                 {       
                        var objOrgDepCargo = "Departamento:Cargo:estado";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objOrgDepCargo);
                            MuestraalGuardar_OrgDepCargo(0);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                 }
                 
         </script>
         </head>
<body>
         <div class="FormContainer" style="width:965px">
            <div class="FormContainerSeccion">
                <div class="FormBasic" style="width:400px" id="FormDatos">
                    <div class="FormSectionMenu">  
                        
                        <?php  $ControlOrgDepCargo->CargaOrgDepCargoBarButton($_GET['opcion']);
                               echo '<script type="text/javascript">';
                               echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                               echo '</script>';
                        ?>
                   </div>    
                   <div class="FormSectionData">              
                       <form id="<?php echo 'Form'.$Sufijo; ?>">
                             <?php  $ControlOrgDepCargo->CargaOrgDepCargoMantenimiento();  ?>
                        </form>
                   </div> 
                </div>               
            </div> 
            <div class="FormContainerSeccion" style="width:500px"> 
                <div class="FormSectionGrid">                  
                        <?php //$ControlOrgDepCargo->CargaOrgDepCargoSearchGrid();  ?>
                </div>
            </div>
         </div> 
        </body>
</html>
              

