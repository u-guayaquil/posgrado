function CargaFecha_Pedido(Sufijo) {
    $('#fecha'+Sufijo).datetimepicker({
            timepicker:false,
            format: 'd/m/Y'
    });
}

function TraeValor(Sufijo) {
    var unidad = document.getElementById('unidadpresentacion'+Sufijo).value;
    var valor = document.getElementById('cantidadpresentacion'+Sufijo).value;
    var producto = document.getElementById('idproducto'+Sufijo).value;
    xajax_TraeValor_Pedido(unidad,producto,valor);
}

function ButtonClick_Pedido(Sufijo, Operacion)
{
    var objPedido = "idpedido:estado:documento:fecha:comentario"
            + ":idproveedor:txproveedor:btproveedor:idproducto:txproducto:codigodetalle:unidadpresentacion"
            + ":cantidadpresentacion:unidadimportacion:cantidadimportacion:precio:idproducto:txproducto:btproducto:id";
    var frmPedido = "estado:documento:fecha:comentario"
            + ":idproveedor:txproveedor:btproveedor:idproducto:txproducto:codigodetalle:unidadpresentacion"
            + ":cantidadpresentacion:unidadimportacion:cantidadimportacion:precio:idproducto:txproducto:btproducto:id";
    if (Operacion == 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, objPedido, "idpedido:unidadimportacion:estado");
        ElementClear(Sufijo, objPedido);
        ElementSetValue(Sufijo, "estado", 1);      
        BarButtonStateEnabled(Sufijo, "btproducto:btproveedor");
        BarButtonStateDisabled(Sufijo, "btpedido");
        CargaFecha_Pedido(Sufijo);
        clean(Sufijo);
        document.getElementById('SeccionDetallePedido' + Sufijo).innerHTML='';
    } else if (Operacion == 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, objPedido, "idpedido:unidadimportacion");       
        BarButtonStateEnabled(Sufijo, "btproducto:btproveedor");
        BarButtonStateDisabled(Sufijo, "btpedido");
        CargaFecha_Pedido(Sufijo);
        var idPedido = document.getElementById("id"+Sufijo).value;
        xajax_LlenaTemporal_Pedido(idPedido);        
        clean(Sufijo);
        SumaValoresPedido(Sufijo);        
        if(ElementGetValue(Sufijo,"estado")==='0'){
            ElementStatus(Sufijo, "estado", "");
        }else{
            ElementStatus(Sufijo, "", "estado");
        }
    } else if (Operacion == 'addDel')
    {
        if (ElementGetValue(Sufijo, "estado") == 1)
        {
            if (confirm("Desea inactivar este registro?"))
                xajax_Elimina_Pedido(ElementGetValue(Sufijo, "idpedido"));
        } else
            alert("El registro se encuentra inactivo");
    } else if (Operacion == 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, "documento"))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {    var resul = confirm('¿Desea Guardar los datos?');
                 if (resul) {
                    var Forma = xajax.getFormValues('FormDatosPedido');
                    xajax_Guarda_Pedido(JSON.stringify({Forma})); 
                         
                    BarButtonStateEnabled(Sufijo, "btpedido");
                    BarButtonStateDisabled(Sufijo, "btproducto:btproveedor");
                 } else {
                    return false;
                 }
            }
        }
    } else if (Operacion == 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, "idpedido", frmPedido);
        ElementClear(Sufijo, objPedido);       
        BarButtonStateEnabled(Sufijo, "btpedido");
        BarButtonStateDisabled(Sufijo, "btproducto:btproveedor");
        document.getElementById('SeccionDetallePedido' + Sufijo).innerHTML = '';
        document.getElementById('totalGeneral' + Sufijo).innerHTML=' 0.00'; 
        document.getElementById('totalEmpaque' + Sufijo).innerHTML='0.00';
        document.getElementById('totalPedido' + Sufijo).innerHTML='0.00';       
        xajax_BorrarDatosCompletosTemporal_Pedido(idPedido);
    } else if (Operacion == 'addImp')
    {
    } else {
        if(Operacion === 'btpedido'){
            if(document.getElementById('documento'+ Sufijo).disabled === true){
                xajax_CargaModal_Pedido(Operacion);
            }
            
        }else{
            xajax_CargaModal_Pedido(Operacion);
        }
            
    }
    return false;
}

function SearchByElement_Pedido(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    document.getElementById('SeccionDetallePedido' + Sufijo).innerHTML = '';
    document.getElementById('totalGeneral' + Sufijo).innerHTML='$ 0.00';
    document.getElementById('totalEmpaque' + Sufijo).innerHTML='0.00';
    document.getElementById('totalPedido' + Sufijo).innerHTML='0.00';
    
    if (Elemento.name == "idpedido" + Sufijo)
    {
        if (Elemento.value.length != 0)
        {
            ContentFlag = document.getElementById("documento" + Sufijo);
            if (ContentFlag.value.length == 0)
            {
                if (BarButtonState(Sufijo, "Inactive"))
                    xajax_MuestraByID_Pedido(Elemento.value);
                var idPedido = document.getElementById('idpedido' + Sufijo).value;
                xajax_MuestraDetalle_Pedido(idPedido);
            }
        } else
            BarButtonState(Sufijo, "Default");
    } else if (Elemento.id === "FindProveedorTextBx" + Sufijo)
    {   xajax_BuscaModalByTX_Pedido("btproveedor", Elemento.value);
    }else if (Elemento.id === "FindProductoTextBx" + Sufijo){
        xajax_BuscaModalByTX_Pedido("btproducto", Elemento.value);
    }else {
        xajax_BuscaModalByTX_Pedido("btpedido", Elemento.value);
    }
}

function SearchGetData_Pedido(Sufijo, Grilla)
{
    BarButtonState(Sufijo, "Active");
    //document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
    document.getElementById("idpedido" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
    document.getElementById("idproveedor" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
    document.getElementById("txproveedor" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
    document.getElementById("documento" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
    document.getElementById("fecha" + Sufijo).value = Grilla.cells[10].childNodes[0].nodeValue;
    document.getElementById("comentario" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
    document.getElementById("estado" + Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
    xajax_MuestraDetalle_Pedido(Grilla.cells[0].childNodes[0].nodeValue);
    return false;
}

function SearchGetData_PedidoDetallePedido(Sufijo, Grilla)
{
    var SufijoActual = document.getElementById('NomSufijo').value;
    document.getElementById("txproducto" + SufijoActual).value = Grilla.cells[1].childNodes[0].nodeValue;
    document.getElementById("idproducto" + SufijoActual).value = Grilla.cells[2].childNodes[0].value;
    document.getElementById("codigodetalle" + SufijoActual).value = Grilla.cells[4].childNodes[0].value;
    document.getElementById("unidadpresentacion" + SufijoActual).value = Grilla.cells[6].childNodes[0].value;
    document.getElementById("cantidadpresentacion" + SufijoActual).value = Grilla.cells[8].childNodes[0].value;
    document.getElementById("unidadimportacion" + SufijoActual).value = Grilla.cells[10].childNodes[0].value;
    document.getElementById("cantidadimportacion" + SufijoActual).value = Grilla.cells[12].childNodes[0].value;
    document.getElementById("precio" + SufijoActual).value = Grilla.cells[14].childNodes[0].value;
    document.getElementById("FilaTrabajo" + SufijoActual).value = Grilla.cells[1].childNodes[0].parentNode.parentNode.id;
    return false;
}

function XAJAXResponse_Pedido(Sufijo, Mensaje, Datos)
{ 
    var objPedido = "estado:documento:fecha:comentario"
            + ":idproveedor:txproveedor:btproveedor:idproducto:txproducto:codigodetalle:unidadpresentacion"
            + ":cantidadpresentacion:unidadimportacion:cantidadimportacion:precio:total:idproducto:txproducto:btproducto";
    if (Mensaje === "GUARDADO")
    {
        BarButtonState(Sufijo, "Active");
        BarButtonStateDisabled(Sufijo, "btproveedor");
        ElementSetValue(Sufijo, "idpedido", Datos);
        ElementSetValue(Sufijo, "idpedido", Datos);
        ElementStatus(Sufijo, "idpedido", objPedido);
        xajax_MuestraDetalle_Pedido(Datos);
        alert('Los datos se guardaron correctamente.');
    } else if (Mensaje === 'ELIMINADO')
    {
        ElementSetValue(Sufijo, "estado", Datos);
        alert("Los Datos se eliminaron correctamente");
    } else if (Mensaje === 'NOEXISTEID')
    {
        BarButtonState(Sufijo, 'Default');
        ElementClear(Sufijo, "idpedido");
        alert(Datos);
    } else if (Mensaje === 'EXCEPCION')
    {
        BarButtonState(Sufijo, "addNew");
        alert(Datos);
    }
}

function cargaExcel(Operacion) {
    if(document.getElementById('addSav_Pedido').disabled === false){
        xajax_CargaModalExcel_Pedido(Operacion);
        return false;
    }  else{
        return false;
    }  
}

function guardarExcel(Operacion) {
    if(document.getElementById('addSav_Pedido').disabled === false){
        xajax_ExtraeExcel_Pedido(Operacion);
        return false;
    }  else{
        return false;
    } 
}

function refrescaExcel(Operacion) {
    if(document.getElementById('addSav_Pedido').disabled === false){
        var idPedido = document.getElementById("id_Pedido").value;
        xajax_RefrescaDetalle_Pedido(idPedido);
        return false;
    }  else{
        return false;
    } 
}

function SearchProveedorGetData_Pedido(Sufijo, DatosGrid)
{
    document.getElementById("idproveedor" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txproveedor" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}

function SearchProductoGetData_Pedido(Sufijo, DatosGrid)
{
    document.getElementById("idproducto" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txproducto" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    document.getElementById("codigodetalle" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
    document.getElementById("unidadimportacion" + Sufijo).value = DatosGrid.cells[3].childNodes[0].nodeValue;
    document.getElementById("unidadimportacion" + Sufijo).disabled=true;
    cerrar();
    return false;
}

function SearchPedidoGetData_Pedido(Sufijo, DatosGrid)
{   var objPedido = "idpedido:estado:documento:fecha:comentario"
            + ":idproveedor:txproveedor:btproveedor:idproducto:txproducto:codigodetalle:unidadpresentacion"
            + ":cantidadpresentacion:unidadimportacion:cantidadimportacion:precio:idproducto:txproducto:btproducto";
    ElementClear(Sufijo, objPedido);
    document.getElementById("idpedido" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    SearchByElement_Pedido(Sufijo,document.getElementById("idpedido" + Sufijo));
    cerrar();
    return false;
}

function addFilaPedido(e, Sufijo) {
    var idVerifica = document.getElementById("idpedido" + Sufijo);
    if (idVerifica.disabled === true) {
        agregafila(e, Sufijo);
        return false;
    } else {
        return false;
    }
}

function agregafila(e, Sufijo) {
    if(document.getElementById('addSav'+Sufijo).disabled == false){
	e.preventDefault();
         var codigo = document.getElementById('codigodetalle' + Sufijo).value;
         var cantidadpresentacion = document.getElementById('cantidadpresentacion' + Sufijo).value;
         var cantidadimportacion = document.getElementById('cantidadimportacion' + Sufijo).value;
         var unipresentacion = document.getElementById('unidadpresentacion' + Sufijo).value;
         var uniimportacion = document.getElementById('unidadimportacion' + Sufijo).value;
         var precio = document.getElementById('precio' + Sufijo).value;
         var nomprod = document.getElementById('txproducto' + Sufijo).value;
         var linea = $('#FilaTrabajo' + Sufijo).val();
         xajax_ActualizaCampoTemporalconDatosIngresados_Pedido(codigo,cantidadpresentacion,cantidadimportacion
                                                              ,precio,unipresentacion,uniimportacion,nomprod,linea);
         SumaValoresPedido(Sufijo);
         clean(Sufijo);
     } 
}


function clean(Sufijo) {
    $('#txproducto' + Sufijo).val('');
    $('#idproducto' + Sufijo).val('');
    $('#codigodetalle' + Sufijo).val('');
    $('#unidadpresentacion' + Sufijo).val($('#unidadpresentacion' + Sufijo+' > option:first').val());
    $('#cantidadpresentacion' + Sufijo).val('0.00');
    $('#unidadimportacion' + Sufijo).val($('#unidadimportacion' + Sufijo+' > option:first').val());
    $('#cantidadimportacion' + Sufijo).val('0.00');
    $('#precio' + Sufijo).val('0.00');
    $('#FilaTrabajo' + Sufijo).val('');
    document.getElementById('totalGeneral' + Sufijo).innerHTML=' 0.00';
}

function deleteFilaPedido() {
    if(document.getElementById('addSav_Pedido').disabled === false){
        $(document).on('click', '.borrar', function (event) {
            event.preventDefault();
            var codigo = document.getElementById('codigodetalle_Pedido').value;
            xajax_EliminaDatoenTablaTemporal_Pedido(codigo);
            $(this).closest('tr').remove();
	    clean('_Pedido');
            SumaValoresPedido('_Pedido');
        });
    }
}

function calculaTotal(Sufijo) {
    var cant = document.getElementById('cantidadimportacion' + Sufijo).value;
    var cantidad = new Number(cant);

    var prec = document.getElementById('precio' + Sufijo).value;
    var precio = new Number(prec);

    var total = cantidad * precio;
    //document.getElementById('total' + Sufijo).value = total.toFixed(2);
}

function SumaValoresPedido(Sufijo) {
    var Total = new Number(0);
    var Empaque = new Number(0);
    var Pedido = new Number(0);
    
    $('#DetSch' + Sufijo + 'DetallePedido tr').each(function () {
        var valor = $(this).find("td").eq(15).html().trim();
        var valemp = $(this).find("td").eq(7).html().trim();
        var valped = $(this).find("td").eq(11).html().trim();

            Total += parseFloat(valor);
            Empaque += parseFloat(valemp);
            Pedido += parseFloat(valped);
        
    });
    document.getElementById('totalGeneral' + Sufijo).innerHTML = Total.toFixed(2);
    document.getElementById('totalEmpaque' + Sufijo).innerHTML = Empaque.toFixed(2);
    document.getElementById('totalPedido' + Sufijo).innerHTML = Pedido.toFixed(2);
    
}

function RecorrerTablaPedido(Sufijo) {
    $('#DetSch' + Sufijo + 'DetallePedido tr').each(function () {
        var idCelda = document.getElementById('FilaTrabajo' + Sufijo).value;
        var celdaActual = this.id;
        if (idCelda === celdaActual) {
            var filaTrabajo = $('#FilaTrabajo' + Sufijo).val();
            if (filaTrabajo !== '') {
                this.remove();
                $('#FilaTrabajo' + Sufijo).val('');
            }
            return false;
        }
    });
    SumaValoresPedido(Sufijo);
    return false;
}