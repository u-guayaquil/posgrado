<?php
         require_once("src/rules/importacion/servicio/ServicioPedido.php");
         require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
         require_once("src/rules/inventario/servicio/ServicioUnidadPresentacion.php");
         require_once("src/modules/importacion/pedido/render/RenderPedido.php");
         require_once("src/modules/general/acreedor/render/RenderAcreedor.php");         
         require_once("src/modules/inventario/producto/render/RenderProducto.php");
         require_once("src/rules/general/servicio/ServicioAcreedor.php");
         require_once("src/rules/inventario/servicio/ServicioProducto.php");
         require_once('src/libs/clases/DateControl.php');


         class ControlPedido
         {     private  $Sufijo;
               private  $ServicioPedido;
               private  $ServicioAcreedor;
               private  $ServicioProducto;
               private  $ServicioUnidadPresentacion;
               private  $RenderPedido;
               private  $RenderAcreedor;
               private  $RenderProducto;
               private  $Utils;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioPedido = new ServicioPedido();
                        $this->ServicioAcreedor = new ServicioAcreedor();
                        $this->ServicioProducto = new ServicioProducto();
                        $this->ServicioUnidadPresentacion = new ServicioUnidadPresentacion();
                        $this->RenderPedido = new RenderPedido($Sufijo);
                        $this->RenderAcreedor = new RenderAcreedor($Sufijo);
                        $this->RenderProducto = new RenderProducto($Sufijo);
                        $this->Utils = new Utils();
               }
               
               function CargaPedidoBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
               function CargaPedidoMantenimiento()
               {        echo $this->RenderPedido->CreaPedidoMantenimiento();
               }

               function CargaPedidoSearchGrid()
               {        
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoPedido = $this->ServicioPedido->BuscarPedido($prepareDQL);
                        echo $this->RenderPedido->CreaPedidoSearchGrid(($datoPedido)); 
               }      
               
               function ControlPedido($unidad,$producto,$valor)
               {        $ajaxRespon = new xajaxResponse(); 
                        $datosValor = $this->ServicioUnidadPresentacion->BuscarUnidadPresentacion(array('unidad' => $unidad, 'producto' => $producto));
                        return $this->RenderPedido->MuestraDetalleUnidad($ajaxRespon,$datosValor,$valor);
               }
               
               function MuestraPedidoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoPedido = $this->ServicioPedido->BuscarPedido($prepareDQL);
                        return $this->RenderPedido->MuestraPedido($ajaxRespon,$datoPedido);
               }

               function MuestraPedidoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoPedido = $this->ServicioPedido->BuscarPedido($prepareDQL);
                        return $this->RenderPedido->MuestraPedidoGrid($ajaxRespon,$datoPedido);
               }
               
               function MuestraDetallePedido($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('id' => intval($id));
                        $datoAsientoContable = $this->ServicioPedido->BuscarDetallePedido($prepareDQL);
                        return $this->RenderPedido->MuestraDetallePedido($ajaxRespon,$datoAsientoContable);
               }  

               function GuardaPedido($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Pedido  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioPedido->GuardaDBPedido($Pedido);
                        if (is_numeric($id)){
                            $function = (empty($Pedido->id) ? "MuestraPedidoGuardado" : "MuestraPedidoEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioPedido->BuscarPedido($prepareDQL);
                            return $this->RenderPedido->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderPedido->MuestraPedidoExcepcion($ajaxRespon,$id);
                        }                      
               }
               
               function EliminaPedido($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $this->ServicioPedido->DesactivaPedido(intval($id));
                        $Datos = $this->ServicioPedido->BuscarPedido($prepareDQL);
                        return $this->RenderPedido->MuestraPedidoEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridPedido($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();  
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'tipo' => array(0,1));
                        switch($Operacion) {
                            case "btproveedor":
                                $Datos = $this->ServicioAcreedor->BuscarAcreedor($prepareDQL);
                                $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                                $jsonModal['Title'] = "Busca Proveedor";
                                $jsonModal['Carga'] = $this->RenderPedido->CreaModalGridPedido($Operacion,$Datos);
                                $jsonModal['Ancho'] = "517";
                                break;
                            case "btproducto":
                                $Datos = $this->ServicioProducto->ObtenerProductosporDescripcion($prepareDQL);
                                $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                                $jsonModal['Title'] = "Busca Producto";
                                $jsonModal['Carga'] = $this->RenderPedido->CreaModalGridPedido($Operacion,$Datos);
                                $jsonModal['Ancho'] = "517";
                                break;
                            case "btpedido":
                                $Datos = $this->ServicioPedido->BuscarPedido($prepareDQL);
                                $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                                $jsonModal['Title'] = "Busca Pedido";
                                $jsonModal['Carga'] = $this->RenderPedido->CreaModalGridPedido($Operacion,$Datos);
                                $jsonModal['Ancho'] = "517";
                                break;
                        } 
                            
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ConsultaModalGridPedidoByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        switch($Operacion){
                            case "btproveedor": 
                                $Datos = $this->ServicioAcreedor->BuscarAcreedor($prepareDQL);
                                $DatosPresenta = $this->RenderAcreedor->MuestraModalGridAcreedor($ajaxResp,$Operacion,$Datos); 
                            break;
                            case "btproducto": 
                                $Datos = $this->ServicioProducto->ObtenerProductosporDescripcion($prepareDQL);
                                $DatosPresenta = $this->RenderPedido->MuestraModalGridPedido($ajaxResp,$Operacion,$Datos);  
                            break;
                            case "btpedido": 
                                $Datos = $this->ServicioPedido->BuscarPedido($prepareDQL);
                                $DatosPresenta = $this->RenderPedido->MuestraModalGridPedido($ajaxResp,$Operacion,$Datos);  
                            break;
                        }    
                        return $DatosPresenta;
                }
               
                function CargaModalExcel($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                
                        if ($Operacion==="btexcel")    
                        {  
                            $ruta = $this->Utils->Encriptar("src/utils/carga/carga.php");
                            $path = $this->Utils->Encriptar("src/upload/importacion/pedidos");
                            $clss = $this->Utils->Encriptar("src/rules/importacion/servicio/ServicioPedido.php");
                            $type = $this->Utils->Encriptar("[xlsx]");
                            $suff = $this->Utils->Encriptar($this->Sufijo);

                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Carga Excel";
                            $jsonModal['Carga'] = "<iframe name='loadfile' src='guns.php?ruta=".$ruta."&clss=".$clss."&type=".$type."&path=".$path."&suff=".$suff."' height='100%' width='100%' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe>";
                            $jsonModal['Ancho'] = "300";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                
                function ExtraeExcel()
                {    
                        $ajaxRespon = new xajaxResponse();
                        $usuario = $_SESSION['IDUSER'];
                        $id = $this->ServicioPedido->GuardaDetallePedidoExcel($usuario); 
                        $Datos = $this->ServicioPedido->BuscarPedido(array('id' => intval($id)));
                        //print_r($Datos);
                         if (is_numeric($id)){
                            return $this->RenderPedido->MuestraDespuesdeGuardarExcel($ajaxRespon,($Datos));                    
                        }else{
                            return $this->RenderPedido->MuestraErroralGuardarExcel($ajaxRespon,($Datos));                    
                        }
  
               }
               
               function LlenaTablaTemporalconReales($pedido){
                        $datoTemporal = $this->ServicioPedido->TraeDatosPedidoTemporalRefrescaActual($pedido);
                        $this->ServicioPedido->IngresaCamposTemporalesdesdeReales($_SESSION['IDUSER'],$datoTemporal);
               }
                              
               function RefrescaDetalle($pedido)
               {        $ajaxRespon = new xajaxResponse();
                        if($pedido == ''){
                            $datoTemporal = $this->ServicioPedido->TraeDatosPedidoTemporalRefresca($_SESSION['IDUSER']);
                        }else{
                            $datoTemporal = $this->ServicioPedido->TraeDatosTemporalesSumados($_SESSION['IDUSER']);
                            $this->ServicioPedido->IngresaCamposTemporalesdesdeReales($_SESSION['IDUSER'],$datoTemporal);
                        }
                                                    
                        return $this->RenderPedido->MuestraDetallePedido($ajaxRespon,$datoTemporal);
               } 
               
               function ActualizaCampoTemporalconDatosIngresados($codigo,$cantidadpresentacion,$cantidadimportacion,$precio,$unipresentacion,$uniimportacion,$producto,$linea){
                        //Verifico si el producto esta en la tabla temporal para actualizar, caso contrario se debe ingresar
                        $prepareDQL = array('codigo' => $codigo, 'idusuario' => $_SESSION['IDUSER']);
                        $codtemp = $this->ServicioPedido->BuscarProductoByCodigoenTemporal($prepareDQL);
                        
                        if(count($codtemp) > 0){
                            
                            //Ingreso los datos en la tabla temporal.
                            $datoTemporal = array('codigo' => $codigo, 'cantpresenta' => $cantidadpresentacion, 'unipresenta' => $unipresentacion
                                                 ,'cantimpor' => $cantidadimportacion, 'uniimpor' => $uniimportacion
                                                 ,'precio' => $precio, 'idusuario' => $_SESSION['IDUSER']);  
                            if($linea != ''){
                                $this->ServicioPedido->ActualizaCampoTemporalconDatosIngresados($datoTemporal);
                            }else{
                                $this->ServicioPedido->ActualizaCantidadesTemporalesconReales($datoTemporal);
                            }
                            
                        }else{
                            
                            //Actualizo los datos en la tabla temporal. 
                            $datoTemporal = array('codigo' => $codigo, 'cantpresenta' => $cantidadpresentacion, 'unipresenta' => $unipresentacion
                                                 ,'cantimpor' => $cantidadimportacion, 'uniimpor' => $uniimportacion, 'producto' => $producto
                                                 ,'precio' => $precio, 'idusuario' => $_SESSION['IDUSER']);                        
                            $this->ServicioPedido->IngresaDatosDigitadosenTemporal($datoTemporal);                            
                            
                        }
                        
                        //Procedo a cargar el grid con datos temporales actualizados.
                        $ajaxRespon = new xajaxResponse();
                        $datosTemporales = $this->ServicioPedido->TraeDatosPedidoTemporalRefresca($_SESSION['IDUSER']);
                        return $this->RenderPedido->MuestraDetallePedido($ajaxRespon,$datosTemporales);
               }
               
               function EliminaDatoenTablaTemporal($codigo){
                        $this->ServicioPedido->EliminaDatoenTablaTemporal($_SESSION['IDUSER'],$codigo);
               }
               
               function BorrarDatosCompletosTemporal(){
                        $this->ServicioPedido->EliminasdatosTemporales($_SESSION['IDUSER']);
               }
         }

?>

