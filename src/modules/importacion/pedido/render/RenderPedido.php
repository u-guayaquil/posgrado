<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");   
        require_once("src/rules/general/servicio/ServicioUnidad.php"); 
        require_once("src/rules/inventario/servicio/ServicioUnidadPresentacion.php"); 
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderPedido
        {     private $Sufijo;
              private $SearchGrid;
              private $Maxlen;
              private $Fechas;
              
              function __construct($Sufijo = "")
              {        $this->Sufijo = $Sufijo;
                       $this->SearchGrid = new SearchGrid($this->Sufijo);
                       $this->Fechas = new DateControl();
                       $this->Maxlen['id'] = 10; 
              }
              
              function CreaOpcionBarButton($Buttons)
              {         $BarButton = new BarButton($this->Sufijo);
                        return $BarButton->CreaBarButton($Buttons);
              }
              
              function CreaPedidoMantenimiento()
              {        $Search = new SearchInput($this->Sufijo);
                       $objPedido = "estado:documento:fecha:comentario:idproveedor:txproveedor:btproveedor";                     
                       $Pedido = '<table border=0 class="Form-Frame" cellpadding="0">';
                       $Pedido.= '       <tr height="30">';
                       $Pedido.= '           <td class="Form-Label" style="width:25%">Id</td>';
                       $Pedido.= '             <td class="Form-Label" style="width:30%">';
                       $Pedido.= '                 <input type="hidden" class="txt-input t05" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="2" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\''.$objPedido.'\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                       $Pedido.=                  $Search->TextSch("pedido","","")->Enabled(true)->MaxLength($this->Maxlen)->ReadOnly(false)->Create("t03");
                       //$Pedido.= '                 <input type="hidden" id="idPedido'.$this->Sufijo.'" name="idPedido'.$this->Sufijo.'" value="">';
                       $Pedido.= '             </td>';                       
                       $Pedido.= '            <td class="Form-Label" style="width:33%">';
                       
                       $Pedido.= '            </td>';
                       $Pedido.= '             <td class="Form-Label" style="width:37%">';
                       $Pedido.= '                 <input id="btn_excel_'.$this->Sufijo.'" src="src/public/img/upload.png" type="image" name="excel'.$this->Sufijo.'" onclick="return cargaExcel(\'btexcel\');" title="Subir Excel"/>';
                       //$Pedido.= '                 <input id="btn_refrescaexcel_'.$this->Sufijo.'" src="src/public/img/valido.png" type="image" name="refrescaexcel'.$this->Sufijo.'" onclick="return refrescaExcel(\'btrefresh\');"  title="Actualizar Tabla"/>';
                       $Pedido.= '             </td>';                       
                       $Pedido.= '         </tr>';
                       $Pedido.= '       <tr height="28">';
                       $Pedido.= '           <td class="Form-Label">Proveedor</td>';
                       $Pedido.= '           <td class="Form-Label" >';
                       $Pedido.=                $Search->TextSch("proveedor","","")->Enabled(false)->Create("t11");
                       $Pedido.= '           </td>';
                       $Pedido.= '             <td class="Form-Label" style="width:30%">Documento</td>';
                       $Pedido.= '             <td class="Form-Label" style="width:70%">';
                       $Pedido.= '                 <input type="text" class="txt-upper t04" id="documento'.$this->Sufijo.'" name="documento'.$this->Sufijo.'" value="" maxlength="100" disabled/>';
                       $Pedido.= '             </td>';
                       $Pedido.= '             <td class="Form-Label" style="width:25%">Fecha</td>';
                       $Pedido.= '             <td class="Form-Label"><input class="txt-upper t04" name = "fecha'.$this->Sufijo.'" id = "fecha'.$this->Sufijo.'" disabled/></td>';
                       $Pedido.= '             </td>';
                       $Pedido.= '       </tr>';
                       
                       $Pedido.= '         <tr height="30">';                       
                       $Pedido.= '             <td class="Form-Label" style="width:25%">Comentario</td>';
                       $Pedido.= '             <td class="Form-Label" style="width:75%">';
                       $Pedido.= '                 <input type="text" class="txt-upper t15" id="comentario'.$this->Sufijo.'" name="comentario'.$this->Sufijo.'" value="" maxlength="100" disabled/>';
                       $Pedido.= '             </td>';                         
                       $Pedido.= '             <td class="Form-Label">Estado</td>';
                       $Pedido.= '             <td class="Form-Label">';
                                                   $Pedido.= $this->CreaComboEstado();
                       $Pedido.= '             </td>';                     
                       $Pedido.= '         </tr>';
                       $Pedido.= '         <tr height="30">';
                       $Pedido.= '             <td class="Form-Label" style="width:100%" colspan="6">';
                       $Pedido.= '                 <div id="SeccionCabIngresoDetallePedido'.$this->Sufijo.'" class="SchCab"> ';
                       $Pedido.= '                   <table class="SchGrdCab" id="CabIngresoDetallePedido'.$this->Sufijo.'" name="CabIngresoDetallePedido'.$this->Sufijo.'" cellpadding="0">';
                       $Pedido.= '                       <tbody>';                       
                       $Pedido.= '                       <tr class="SchGrdCabCell">';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 230px;" rowspan="2">Producto</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 120px;" rowspan="2">C&oacute;digo</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 225px;" colspan="2">Empaque</td>'; 
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 225px;" colspan="2">Pedido</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 125px;" rowspan="2">Precio</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 30px;" rowspan="2"></td>';
                       $Pedido.= '                       </tr>';    
                       $Pedido.= '                       <tr class="SchGrdCabCell">';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 125px;">Unidad</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 100px;">Cantidad</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 125px;">Unidad</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 100px;">Cantidad</td>';
                       $Pedido.= '                       </tr>';   
                       $Pedido.= '                       </tbody>';
                       $Pedido.= '                   </table>';
                       $Pedido.= '                 </div>';
                       $Pedido.= '                   <table class="SchGrdCab" id="CabIngresoDetallePedido'.$this->Sufijo.'" name="CabIngresoDetallePedido'.$this->Sufijo.'" cellpadding="0">';
                       $Pedido.= '                       <tbody>';
                       $Pedido.= '                       <tr class="Form-Label">';
                       $Pedido.= '                          <td class="Form-Label" style="width: 230px; height:50px;" align="center">';
                       $Pedido.=                                 $Search->TextSch("producto","","")->Enabled(false)->Create("t07");
                       $Pedido.= '                          </td>';
                       $Pedido.= '                          <td class="Form-Label" style="width:130px" align="center">';
                       $Pedido.= '                             <input type="text" class="txt-upper t04" id="codigodetalle'.$this->Sufijo.'" name="codigoetalle'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                       $Pedido.= '                          </td>';
                       $Pedido.= '                          <td class="Form-Label" style="width:125px" align="center">';
                                                                $Pedido.= $this->CreaComboUnidadPresentacion();
                       $Pedido.= '                          </td>';
                       $Pedido.= '                          <td class="Form-Label" style="width:90px" align="center">';// onchange="return TraeValor(\''.$this->Sufijo.'\');"
                       $Pedido.= '                             <input type="text" class="txt-upper t03" id="cantidadpresentacion'.$this->Sufijo.'" name="cantidadpresentacion'.$this->Sufijo.'" value="0.00" maxlength="20" disabled/>';
                       $Pedido.= '                          </td>';
                       $Pedido.= '                          <td class="Form-Label" style="width:135px" align="center">';
                                                                $Pedido.= $this->CreaComboUnidad();
                       $Pedido.= '                          </td>';
                       $Pedido.= '                          <td class="Form-Label" style="width:100px" align="center">';//onchange="return TraeValor(\''.$this->Sufijo.'\');"
                       $Pedido.= '                             <input type="text" class="txt-upper t03" id="cantidadimportacion'.$this->Sufijo.'" name="cantidadimportacion'.$this->Sufijo.'" value="0.00" maxlength="20"  disabled/>';
                       $Pedido.= '                          </td>';
                       $Pedido.= '                          <td class="Form-Label" style="width:125px" align="center">';
                       $Pedido.= '                             <input type="text" class="txt-upper t03" id="precio'.$this->Sufijo.'" name="precio'.$this->Sufijo.'" value="0.00" maxlength="20" onchange="return calculaTotal(\''.$this->Sufijo.'\');" disabled/>';
                       $Pedido.= '                          </td>';
                       $Pedido.= '                          <td class="Form-Label" style="width: 30px; height:30px;" align="center">';
                       $Pedido.= '                             <input id="btn_addPedido'.$this->Sufijo.'" src="src/modules/contabilidad/plantillacontable/img/add.png" '
                                                                         . ' type="image" name="addPedido'.$this->Sufijo.'" onclick="return addFilaPedido(event,\''.$this->Sufijo.'\');" />';
                       $Pedido.= '                          </td>';
                       $Pedido.= '                       </tr>';
                       $Pedido.= '                       </tbody>';
                       $Pedido.= '                   </table>';
                       $Pedido.= '                 <div id="SeccionCabDetallePedido'.$this->Sufijo.'" class="SchCab"> ';
                       $Pedido.= '                   <table class="SchGrdCab" id="CabDetallePedido'.$this->Sufijo.'" name="CabDetallePedido'.$this->Sufijo.'" cellpadding="0">';
                       $Pedido.= '                       <tbody>';
                       $Pedido.= '                       <tr class="SchGrdCabCell">';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 30px;" rowspan="2">&nbsp;&nbsp;&nbsp;</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 180px;" rowspan="2">Producto</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 120px;" rowspan="2">C&oacute;digo</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 200px;" colspan="2">Empaque</td>'; 
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 200px;" colspan="2">Pedido</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 100px;" rowspan="2">Precio</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 100px;" rowspan="2">Total</td>';
                       $Pedido.= '                       </tr>';
                       $Pedido.= '                       <tr class="SchGrdCabCell">';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 100px;">Unidad</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 80px;">Cantidad</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 100px;">Unidad</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 80px;">Cantidad</td>';
                       $Pedido.= '                       </tr>';  
                       $Pedido.= '                       </tbody>';
                       $Pedido.= '                   </table>';
                       $Pedido.= '                 </div>';
                       $Pedido.= '                 <div id="SeccionDetallePedido'.$this->Sufijo.'" class="SchDet" style=" overflow:scroll;height:200px;width:950px"> ';
                       $Pedido.= '                   <table class="SchGrdDet" id="DetSch'.$this->Sufijo.'DetallePedido" name="DetSch'.$this->Sufijo.'DetallePedido" cellpadding="0">';
                       $Pedido.= '                       <tbody>';
                       $Pedido.= '                       </tbody>';
                       $Pedido.= '                   </table>';
                       $Pedido.= '                 </div>';
                       $Pedido.= '             </td>';
                       $Pedido.= '         </tr>';
                       $Pedido.= '         <tr height="30">';
                       $Pedido.= '             <td class="Form-Label" style="width:100%" colspan="6">';
                       $Pedido.= '                 <div id="SeccionTotalDetallePedido'.$this->Sufijo.'" class="SchCab"> ';
                       $Pedido.= '                   <table class="SchGrdCab" id="TotalDetallePedido'.$this->Sufijo.'" name="TotalDetallePedido'.$this->Sufijo.'" cellpadding="0">';
                       $Pedido.= '                       <tbody>';                       
                       $Pedido.= '                       <tr class="SchGrdCabCell">';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 380px;"><span id="total'.$this->Sufijo.'" style="float: right;">Totales</span></td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 220px;"><span id="totalEmpaque'.$this->Sufijo.'" style="float: right; ">0.00</span></td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 225px;"><span id="totalPedido'.$this->Sufijo.'" style="float: right;">0.00</span></td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 117px;">&nbsp;&nbsp;&nbsp;</td>';
                       $Pedido.= '                          <td class="SchGrdCabCell" style="width: 120px;">$&nbsp;<span id="totalGeneral'.$this->Sufijo.'" style="float: right;">0.00</span></td>';
                       $Pedido.= '                       </tr>';                       
                       $Pedido.= '                       </tbody>';
                       $Pedido.= '                   </table>';
                       $Pedido.= '                 </div>';
                       $Pedido.= '             </td>';
                       $Pedido.= '         </tr>';
                       $Pedido.= '</table>';
                       return $Pedido;
              }
              
              private function SearchGridValues()
              {         $Columns['Id']       = array('30px','center','');
                        $Columns['Documento'] = array('100px','left','');
                        $Columns['Creaci&oacute;n']   = array('120px','center','');
                        $Columns['Estado']   = array('70px','left',''); 
                        $Columns['idEstado']   = array('0px','left','none');
                        $Columns['idproveedor']   = array('0px','left','none');  
                        $Columns['comentario']   = array('0px','left','none');
                        $Columns['fecha']   = array('0px','left','none');
                        $Columns['proveedor']   = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
              }
               
              function CreaPedidoSearchGrid($Datos)
              {                               
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->PedidoGridHTML($SearchGrid,$Datos);
                       return $SearchGrid->CreaSearchGrid($ObjHTML);
              }
              
              function MuestraPedidoGrid($Ajax,$Datos)
              {                              
                       $SearchGrid = new SearchGrid($this->Sufijo);
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                       $ObjHTML = $this->PedidoGridHTML($SearchGrid,$Datos);
                       $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                       return $Ajax;
              }        

              private function PedidoGridHTML($ObjSchGrid,$Datos)
              {  
                  if(count($Datos)>0){                      
                     foreach ($Datos as $documento)
                      {        $fecreacion = $documento['fecreacion'];
                               $id = str_pad($documento['id'],2,'0',STR_PAD_LEFT);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                              
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($documento['documento']));
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['txtestado']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['idestado']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['idproveedor']); 
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['comentario']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['fecha']);
                               $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['proveedor']);
                               
                               $ObjSchGrid->CreaSearchRowsDetalle ($id,($documento['idestado']==0 ? "red" : ""));
                      }
                      return $ObjSchGrid->CreaSearchTableDetalle();
                  }                  
              }

              function TraeDatosEstadoByArray($IdArray)
              {        
                       $ServicioEstado = new ServicioEstado();
                       $datoEstado = $ServicioEstado->BuscarEstadoByArrayID($IdArray);
                       return $datoEstado;
              }
              
              private function CreaComboEstado()
              {       
                       $datosEstado= $this->TraeDatosEstadoByArray(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
              }
              
              function TraeDatosUnidadPresentacion($IdArray)
              {        
                       $ServicioUnidadPresentacion = new ServicioUnidadPresentacion();
                       $datoUnidadPresentacion = $ServicioUnidadPresentacion->BuscarUnidadPresentacion($IdArray);
                       return $datoUnidadPresentacion;
              }
              
              private function CreaComboUnidadPresentacion()
              {       
                       $datosUnidadPresentacion= $this->TraeDatosUnidadPresentacion(array(0,1));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("unidadpresentacion",$datosUnidadPresentacion)->Selected(1)->Create("s04");
              }
              
              function TraeDatosUnidad($IdArray)
              {        
                       $ServicioUnidad = new ServicioUnidad();
                       $datoUnidad = $ServicioUnidad->BuscarUnidadByDescripcion($IdArray);
                       return $datoUnidad;
              }
              
              private function CreaComboUnidad()
              {       
                       $datosUnidad= $this->TraeDatosUnidad(array('estado' => '0,1'));
                       $Select = new ComboBox($this->Sufijo);                        
                       return $Select->Combo("unidadimportacion",$datosUnidad)->Selected(1)->Create("s04");
              }
              
              private function SearchGridValuesDetalle()
              {         $Columns['Opcion'] = array('31px','center','');
                        $Columns['Producto'] = array('180px','left','');
                        $Columns['idProducto'] = array('0px','left','none');
                        $Columns['C&oacute;digo'] = array('124px','left','');
                        $Columns['idCodigo'] = array('0px','left','none');
                        $Columns['Unidad Presentaci&oacute;n'] = array('112px','left','');                        
                        $Columns['idUnidad'] = array('0px','left','none');
                        $Columns['Cantidad Presentaci&oacute;n'] = array('85px','right','');
                        $Columns['idCantidad'] = array('0px','left','none');
                        $Columns['Unidad Importacion'] = array('112px','left','');
                        $Columns['idUnidadi'] = array('0px','center','none');                        
                        $Columns['Cantidad Importacion'] = array('90px','right','');
                        $Columns['idCantidadi'] = array('0px','left','none');
                        $Columns['Precio'] = array('100px','right','');
                        $Columns['idPrecio'] = array('0px','left','none');
                        $Columns['Total'] = array('95px','right','');
                        $Columns['idTotal'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
                        
              }
              
              function MuestraDetallePedido($Ajax,$Datos)
              {        $SearchGrid = new SearchGrid($this->Sufijo.'DetallePedido');
                       $SearchGrid->ConfiguraSearchGrid($this->SearchGridValuesDetalle());
                       $ObjHTML = $this->DetallePedido($SearchGrid,$Datos);
                       $Ajax->Assign('SeccionDetallePedido'.$this->Sufijo,"innerHTML", $ObjHTML);
                       $Ajax->call("SumaValoresPedido",$this->Sufijo);
                       return $Ajax;
              }
              
              private function DetallePedido($ObjSchGrid,$Datos)
              {       $contador = 0;
                      foreach ($Datos as $plantillacontable)
                      {        $contador++;
                               $id = str_pad($plantillacontable['id'],2,'0',STR_PAD_LEFT);
                               $producto = trim($plantillacontable['producto']);
                               $idproducto = trim($plantillacontable['id_producto']);
                               $codigo = trim(strtoupper($plantillacontable['codigo']));
                               $unidad_presentacion = trim(strtoupper($plantillacontable['presentacion']));
                               $idunidadprese = trim($plantillacontable['uni_presentacion']);
                               $cantidad_presentacion = number_format($plantillacontable['cant_presentacion'],2);
                               $unidad_importacion = trim(strtoupper($plantillacontable['importacion']));
                               $idunidadimpor = trim($plantillacontable['uni_importacion']);
                               $cantidad_importacion = number_format($plantillacontable['cant_importacion'],2);
                               $precio =  number_format($plantillacontable['precio'],2);
                               $total =  number_format($plantillacontable['total'],2,".","");
                               $idGeneral = $this->Sufijo.$codigo;
                                    
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input id="btn_eliPedido'.$this->Sufijo.'" class="borrar"'
                                      .'src="src/modules/contabilidad/plantillacontable/img/delete.png"'
                                      .'type="image" name="eliPedido'.$this->Sufijo.'"'
                                      .'onclick="return deleteFilaPedido(\''.$this->Sufijo.'\');" />');
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$producto);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detproducto'.$idGeneral.'" name="detproducto'.$idGeneral.'" value="'.$idproducto.'">');
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$codigo);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detcodigo'.$idGeneral.'" name="detcodigo'.$idGeneral.'" value="'.$codigo.'">');
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$unidad_presentacion);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detpunidad'.$idGeneral.'" name="detpunidad'.$idGeneral.'" value="'.$idunidadprese.'">');
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cantidad_presentacion);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detpcantidad'.$idGeneral.'" name="detpcantidad'.$idGeneral.'" value="'.$cantidad_presentacion.'">');
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$unidad_importacion);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detiunidad'.$idGeneral.'" name="detiunidad'.$idGeneral.'" value="'.$idunidadimpor.'">');
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$cantidad_importacion);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="deticantidad'.$idGeneral.'" name="deticantidad'.$idGeneral.'" value="'.$cantidad_importacion.'">');
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$precio);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="detprecio'.$idGeneral.'" name="detprecio'.$idGeneral.'" value="'.$precio.'">');
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'',$total);
                                    $ObjSchGrid->CreaSearchCellsDetalle($id,'','<input type="hidden" id="dettotal'.$idGeneral.'" name="dettotal'.$idGeneral.'" value="'.$total.'">');
                                    $ObjSchGrid->CreaSearchRowsDetalle ($id,"");  
                      }
                     
                      return $ObjSchGrid->CreaSearchTableDetalle(1);
              }
              
              function MuestraPedido($Ajax,$Datos)
              {        $Ajax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],10,'0',STR_PAD_LEFT));
                       $Ajax->Assign("idPedido".$this->Sufijo,"value", str_pad($Datos[0]['id'],10,'0',STR_PAD_LEFT));
                       $Ajax->Assign("idproveedor".$this->Sufijo,"value", trim($Datos[0]['idproveedor']));                     
                       $Ajax->Assign("txproveedor".$this->Sufijo,"value", trim($Datos[0]['proveedor']));
                       $Ajax->Assign("documento".$this->Sufijo,"value", trim($Datos[0]['documento']));                     
                       $Ajax->Assign("fecha".$this->Sufijo,"value", trim($Datos[0]['fecha']));      
                       $Ajax->Assign("comentario".$this->Sufijo,"value", trim($Datos[0]['comentario'])); 
                       $Ajax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                       $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                       return $Ajax;
              }
              
              function MuestraDetalleUnidad($Ajax,$Datos,$valor)
              {        
                       $Ajax->Assign("cantidadimportacion".$this->Sufijo,"value", number_format(($Datos[0]['valor']* $valor),2));
                       $Ajax->Assign("cantidadpresentacion".$this->Sufijo,"value", number_format(($Datos[0]['valor']* $valor)/$Datos[0]['valor'],2));
                       return $Ajax;
              }
              
              function MuestraPedidoGuardado($Ajax,$Pedido)
              {        
                        if (count($Pedido)>0)
                        {   $id = str_pad($Pedido[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            return $this->RespuestaPedido($Ajax,"GUARDADO",$id);
                        }
                        return $this->RespuestaPedido($Ajax,"EXCEPCION","No se guardó la información.");
              }

              function MuestraPedidoEditado($Ajax,$Pedido)
              {        foreach ($Pedido as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                return $this->RespuestaPedido($Ajax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaPedido($Ajax,"EXCEPCION","No se actualizó la información.");            
              }
              
              function MuestraPedidoEliminado($Ajax,$Pedido)
              {        foreach ($Pedido as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                return $this->RespuestaPedido($Ajax,"ELIMINADO",$datos['idestado']);
                        }  
                       return $this->RespuestaPedido($Ajax,"EXCEPCION","No se eliminó la información.");
              }
              

              function MuestraPedidoExcepcion($Ajax,$Msg)
              {        return $this->RespuestaPedido($Ajax,"EXCEPCION",$Msg);    
              }

              private function RespuestaPedido($Ajax,$Tipo,$Data)
              {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                      $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                      return $Ajax;
              }
                
              /*** MODAL ***/
                private function SearchGridModalConfig($Operacion)
                {       $Columns['Id']     = array('70px','center','');
                        switch($Operacion){
                            case  'btproveedor':
                                $Columns['Codigo']  = array('90px','center','');
                                $Columns['Descripci&oacute;n'] = array('170px','left','');
                                $Columns['Identificaci&oacute;n']  = array('150px','left','');
                                break;
                            case  'btproducto':
                                $Columns['Codigo']  = array('90px','center','');
                                $Columns['Descripci&oacute;n'] = array('300px','left','');
                                $Columns['Unidad'] = array('0px','left','none');
                            break;
                            case  'btpedido':
                                $Columns['Documento']  = array('120px','center','');
                                $Columns['Comentario'] = array('200px','left','');                                
                                $Columns['Fecha'] = array('100px','left','');
                            break;
                        }
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGridPedido($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModalPedido($SearchGrid,$Datos,$Operacion);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGridPedido($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig($Operacion));
                        $GrdDataHTML = $this->GridDataHTMLModalPedido($SearchGrid,$Datos,$Operacion);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalPedido($Grid,$Datos,$Operacion)
                {       foreach ($Datos as $Opcion)
                        {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                switch(trim($Operacion)){
                                    case  'btproveedor':
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['nombre']));
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['num_ident']));
                                    break;
                                    case  'btproducto':
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['descripcion']));
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['idunidad']));
                                    break;                                
                                    case  'btpedido':
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['documento']));
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['comentario']));
                                        $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['fecha']));
                                    break;
                                }
                                
                                $Grid->CreaSearchRowsDetalle ($id,"black");
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
                
              function CreaModalExcelPedido()
                {       $Pedido .= '<form method="" id="UploadFile">';
                        $Pedido .= '   <div id="subcont-form">';
                        $Pedido .= '       <table border="0" cellspacing="4" cellpadding="0">';
                        $Pedido .= '           <tr>';
                        $Pedido .= '               <td><input type="text" class="FileTexto" id="Path1" name="Path1" readonly/></td>';
                        $Pedido .= '               <td><div class="FileUpload">';
                        $Pedido .= '                       <input type="file" class="BtnUp" id="arch1" onchange="return carga();" name="archivo[]">';
                        $Pedido .= '                   </div>';
                        $Pedido .= '               </td>';
                        $Pedido .= '           </tr>';
                        $Pedido .= '           <tr>';
                        $Pedido .= '               <td align="left" colspan="2"> ';                                               
                        $Pedido .= '                   <input class="btn btn-success" type="button" value="Subir Archivo" onclick="return UploadFiles'.$this->Sufijo.'(UploadFile);"/>';
                        $Pedido .= '                   <button type="button" class="btn btn-warning" onclick="return cerrar();" style="align:center">Cancelar</button>';
                        $Pedido .= '               </td>';
                        $Pedido .= '           </tr>';
                        $Pedido .= '       </table>';
                        $Pedido .= '   </div>';
                        $Pedido .= '</form> ';
                        
                        return $Pedido;

                }
                                
                function MuestraDespuesdeGuardarExcel($Ajax,$Datos)
                {       $this->MuestraPedidoGuardado($Ajax,$Datos);
                }
                
                function MuestraErroralGuardarExcel($Ajax,$Datos)
                {       $jsFun = "XAJAXResponseExcel".$this->Sufijo;                
                        if (count($Datos)>0)
                        {   $Ajax = $this->MuestraPedidoGrid($Ajax,$Datos);                            
                            $Ajax->call($jsFun,"ERROR");    
                            return $Ajax;
                        }                        
                }
        }
?>

