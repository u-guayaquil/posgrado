<?php
         $Sufijo = '_Pedido';
         
         require_once('src/modules/importacion/pedido/controlador/ControlPedido.php');
         $ControlPedido = new ControlPedido($Sufijo);

         $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlPedido,'GuardaPedido'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlPedido,'MuestraPedidoByID'));
         $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlPedido,'MuestraPedidoByTX'));         
         $xajax->register(XAJAX_FUNCTION,array('MuestraDetalle'.$Sufijo, $ControlPedido,'MuestraDetallePedido'));
         $xajax->register(XAJAX_FUNCTION,array('PresentGridData'.$Sufijo, $ControlPedido,'PresentaGridData'));
         $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlPedido,'EliminaPedido'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlPedido,'CargaModalGridPedido'));
         $xajax->register(XAJAX_FUNCTION,array('CargaModalExcel'.$Sufijo, $ControlPedido,'CargaModalExcel'));
         $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlPedido,'ConsultaModalGridPedidoByTX'));                 
         $xajax->register(XAJAX_FUNCTION,array('TraeValor'.$Sufijo, $ControlPedido,'ControlPedido'));
         $xajax->register(XAJAX_FUNCTION,array('ExtraeExcel'.$Sufijo, $ControlPedido,'ExtraeExcel'));
         $xajax->register(XAJAX_FUNCTION,array('RefrescaDetalle'.$Sufijo, $ControlPedido,'RefrescaDetalle'));         
         $xajax->register(XAJAX_FUNCTION,array('LlenaTemporal'.$Sufijo, $ControlPedido,'LlenaTablaTemporalconReales'));         
         $xajax->register(XAJAX_FUNCTION,array('ActualizaCampoTemporalconDatosIngresados'.$Sufijo, $ControlPedido,'ActualizaCampoTemporalconDatosIngresados'));
         $xajax->register(XAJAX_FUNCTION,array('EliminaDatoenTablaTemporal'.$Sufijo, $ControlPedido,'EliminaDatoenTablaTemporal'));          
         $xajax->register(XAJAX_FUNCTION,array('BorrarDatosCompletosTemporal'.$Sufijo, $ControlPedido,'BorrarDatosCompletosTemporal'));
         
         $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
                <?php $xajax->printJavascript(); 
                      require_once('src/utils/links.php'); ?>
            <script type="text/javascript" src="src/modules/importacion/pedido/js/JSPedido.js"></script>
         </head>
         <body>
           <div class="FormBasic" style="width:970px" id="FormDatosPedido">
               <div class="FormSectionMenu">              
              <?php  $ControlPedido->CargaPedidoBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '        BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                    <input type="hidden" id="NomSufijo" name="NomSufijo" value="<?php echo $Sufijo; ?>">
                    <input type="hidden" id="FilaTrabajo<?php echo $Sufijo; ?>" name="FilaTrabajo<?php echo $Sufijo; ?>" value="">
                   <?php  $ControlPedido->CargaPedidoMantenimiento();  ?>
                   </form>
              </div>    
              <!-- <div class="FormSectionGrid">                   
              <?php //echo $ControlPedido->CargaPedidoSearchGrid();  ?>
              </div>  -->
         </div> 
         </body>
</html>
              

