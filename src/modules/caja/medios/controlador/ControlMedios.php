<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");

        require_once("src/rules/caja/servicio/ServicioTipo.php");
        require_once("src/rules/caja/servicio/ServicioOperacion.php");
         
        require_once("src/rules/caja/servicio/ServicioMedios.php");
        require_once("src/modules/caja/medios/render/RenderMedios.php");

        class ControlMedios
        {       private  $Sufijo;
                private  $ServicioMedios;
                private  $RenderMedios;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->ServicioMedios = new ServicioMedios();
                        $this->RenderMedios = new RenderMedios($Sufijo);
                }
               
                function CargaMedioBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaMedioMantenimiento()
                {        echo $this->RenderMedios->CreaMedioMantenimiento();
                }

                function CargaMedioSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoMedio = $this->ServicioMedios->BuscarMedioByDescripcion($prepareDQL);
                        echo $this->RenderMedios->CreaMedioSearchGrid(($datoMedio)); 
                }         
               
                function MuestraMedioByID($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => $id);
                        $datoMedio = $this->ServicioMedios->BuscarMedioByID($prepareDQL);
                        return $this->RenderMedios->MuestraMedio($ajaxRespon,$datoMedio);
                }

                function MuestraMedioByTX($texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoMedio = $this->ServicioMedios->BuscarMedioByDescripcion($prepareDQL);
                        return $this->RenderMedios->MuestraMedioGrid($ajaxRespon,$datoMedio);
                }

                function GuardaMedio($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $Medio  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioMedios->GuardaDBMedio($Medio);
                        if (is_numeric($id)){
                            $function = (empty($Medio->id) ? "MuestraMedioGuardado" : "MuestraMedioEditado");
                            $prepareDQL = array('limite' => 50,'id' => $id);
                            $Datos = $this->ServicioMedios->BuscarMedioByID($prepareDQL);
                            return $this->RenderMedios->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderMedios->MuestraMedioExcepcion($ajaxRespon,$id);
                        }                     
                }
               
                function EliminaMedio($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioMedios->DesactivaMedio($id);
                        $Datos = $this->ServicioMedios->BuscarMedioByDescripcion($prepareDQL);
                        return $this->RenderMedios->MuestraMedioEliminado($ajaxRespon,$Datos);
                }
               
        }

?>

