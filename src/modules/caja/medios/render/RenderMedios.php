<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");             
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderMedios
        {   private $Sufijo;
            private $SearchGrid;
            private $Maxlen;
            private $Fechas;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Fechas = new DateControl();
                    $this->Maxlen['id'] = 3; 
            }
              
            function CreaOpcionBarButton($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);
                    return $BarButton->CreaBarButton($Buttons);
            }
              
            function CreaMedioMantenimiento()
            {       $Medio = '<table class="Form-Frame" cellpadding="0" border="0">';
                    $Medio.= '      <tr height="30">';
                    $Medio.= '          <td class="Form-Label" style="width:25%">Id</td>';
                    $Medio.= '          <td class="Form-Label" style="width:35%">';
                    $Medio.= '              <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:operacion:tipo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Medio.= '          </td>';
                    $Medio.= '          <td class="Form-Label" style="width:15%"></td>';
                    $Medio.= '          <td class="Form-Label" style="width:25%"></td>';
                    $Medio.= '      </tr>';
                    $Medio.= '      <tr height="30">';
                    $Medio.= '          <td class="Form-Label">Tipo</td>';
                    $Medio.= '          <td class="Form-Label" colspan="3">';
                    $Medio.=            $this->CreaComboTipos();
                    $Medio.= '          </td>';
                    $Medio.= '      </tr>';
                    $Medio.= '      <tr height="30">';
                    $Medio.= '          <td class="Form-Label">Descripci&oacute;n</td>';
                    $Medio.= '          <td class="Form-Label" colspan="3">';
                    $Medio.= '              <input type="text" class="txt-upper t14" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                    $Medio.= '          </td>';
                    $Medio.= '      </tr>';
                    $Medio.= '      <tr height="30">';
                    $Medio.= '          <td class="Form-Label">Operaci&oacute;n</td>';
                    $Medio.= '          <td class="Form-Label">';
                    $Medio.=            $this->CreaComboOperaciones();
                    $Medio.= '          </td>';
                    $Medio.= '          <td class="Form-Label" style="width:20%">Estado</td>';
                    $Medio.= '          <td class="Form-Label" style="width:30%">';
                    $Medio.=            $this->CreaComboEstado();
                    $Medio.= '          </td>';
                    $Medio.= '      </tr>';
                    $Medio.= '</table>';
                    return $Medio;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']                 = array('30px','center','');
                    $Columns['Descripci&oacute;n'] = array('200px','left','');
                    $Columns['Operacion']          = array('130px','left','');
                    $Columns['Tipo']               = array('0px','left','none');
                    $Columns['Creacion']           = array('0px','center','none');
                    $Columns['Estado']             = array('70px','left',''); 
                    $Columns['idEstado']           = array('0px','left','none');
                    $Columns['idOperacion']        = array('0px','left','none');                        
                    $Columns['idTipo']             = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }
               
            function CreaMedioSearchGrid($Datos)
            {                               
                    $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->MedioGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }
              
            function MuestraMedioGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->MedioGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }        

            private function MedioGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $medio)
                    {       $fecreacion = $this->Fechas->changeFormatDate($medio['fecreacion'],"DMY");
                            $id = str_pad($medio['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                              
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($medio['descripcion']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($medio['operacion']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($medio['tipo']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$medio['txtestado']); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$medio['idestado']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$medio['idcajaoper']); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$medio['idcajatipo']);
                             
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,($medio['idestado']==0 ? "red" : ""));
                    }
                    return $ObjSchGrid->CreaSearchTableDetalle(); 
            }

            private function CreaComboEstado()
            {       $Select = new ComboBox($this->Sufijo);                        
            
                    $ServicioEstado = new ServicioEstado();
                    $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
            }
              
            private function CreaComboTipos()
            {       $Select = new ComboBox($this->Sufijo);  
            
                    $ServicioTipos = new ServicioTipo();
                    $datosTipos = $ServicioTipos->BuscarTipoTodos();
                    return $Select->Combo("tipo",$datosTipos)->Selected(1)->Create("s06");
            }
             
            private function CreaComboOperaciones()
            {       $Select = new ComboBox($this->Sufijo);                        
            
                    $ServicioOperaciones = new ServicioOperacion();
                    $datosOperaciones = $ServicioOperaciones->BuscarOperacionTodos();
                    return $Select->Combo("operacion",$datosOperaciones)->Selected(1)->Create("s06");
            }
              
            function MuestraMedio($Ajax,$Datos)
            {       foreach ($Datos as $dato) 
                    {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($dato['descripcion']));                     
                        $Ajax->Assign("operacion".$this->Sufijo,"value", trim($dato['idcajaoper']));                     
                        $Ajax->Assign("tipo".$this->Sufijo,"value", trim($dato['idcajatipo']));
                        $Ajax->Assign("estado".$this->Sufijo,"value", $dato['idestado']);
                        $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                        return $Ajax;
                    }
                    return $this->RespuestaMedio($Ajax,"NOEXISTEID","No existe un Medio con el ID ingresado.");
            }
              
            function MuestraMedioGuardado($Ajax,$Medio)
            {       foreach ($Medio as $datos) 
                    {   $id = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraMedioGrid($Ajax,$Medio);
                        return $this->RespuestaMedio($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaMedio($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraMedioEditado($Ajax,$Medio)
            {       foreach ($Medio as $datos) 
                    {   $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraMedioRowGrid($Ajax, $datos,$datos['idestado']);
                        return $this->RespuestaMedio($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaMedio($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraMedioEliminado($Ajax,$Medio)
            {       foreach ($Medio as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraMedioRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaMedio($xAjax,"ELIMINADO",$datos['idestado']);
                    }  
                    return $this->RespuestaMedio($xAjax,"EXCEPCION","No se eliminó la información.");
            }
              
            function MuestraMedioRowGrid($Ajax,$Medio,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($Medio['id'],$estado);
                    $fecreacion = $this->Fechas->changeFormatDate($Medio['fecreacion'],"DMY");
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Medio['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Medio['descripcion']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Medio['operacion']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Medio['tipo']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Medio['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($Medio['idestado'])); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($Medio['idcajaoper']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($Medio['idcajatipo']));
                    return $Ajax;
            }

            function MuestraMedioExcepcion($Ajax,$Msg)
            {       return $this->RespuestaMedio($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaMedio($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
        }
?>

