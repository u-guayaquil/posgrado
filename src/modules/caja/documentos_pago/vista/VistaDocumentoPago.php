<?php
        $Sufijo = '_documentopago';

        require_once('src/modules/caja/documentos_pago/controlador/ControlDocumentoPago.php');
        $ControlDocumentoPago = new ControlDocumentoPago($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlDocumentoPago,'GuardaDocumentoPago'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlDocumentoPago,'EliminaDocumentoPago'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlDocumentoPago,'MuestraDocumentoPagoByID'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlDocumentoPago,'MuestraDocumentoPagoByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlDocumentoPago,'CargaModalGrid'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlDocumentoPago,'ConsultaModalGridByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaCombo'.$Sufijo, $ControlDocumentoPago,'CargaComboDocumentoCajaTipo'));
        $xajax->processRequest();
?>
         <!doctype html>
         <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript">
                function ButtonClick_documentopago(Sufijo,Operacion)
                {       var frmDocumentoPago = "idmpago:txmpago:documento:formato:CTRL:secuencial";
                        var clsDocumentoPago = "id:idmpago:txmpago:formato:secuencial";
                        var valDocumentoPago = "idmpago:txmpago"; 

                        if (Operacion=='addNew')
                        {   xajax_CargaCombo_documentopago(0,0,true);
                            BarButtonState(Sufijo,Operacion);
                            BarButtonStateEnabled(Sufijo, "btmpago");
                            ElementStatus(Sufijo,frmDocumentoPago,"id");
                            ElementClear(Sufijo,clsDocumentoPago);
                        }
                        else if (Operacion=='addMod')
                        {   BarButtonState(Sufijo,Operacion);
                            BarButtonStateEnabled(Sufijo, "btmpago");
                            ElementStatus(Sufijo,frmDocumentoPago,"id");
                        }
                        else if (Operacion=='addDel')
                        {   if (confirm("¿Desea eliminar este registro?"))
                            xajax_Elimina_documentopago(ElementGetValue(Sufijo, "id"));
                        }
                        else if (Operacion=='addSav')
                        {   if (ElementValidateBeforeSave(Sufijo,valDocumentoPago))
                            {   if (BarButtonState(Sufijo,"Inactive"))
                                {   var Forma = PrepareElements(Sufijo,"id:"+frmDocumentoPago);
                                    xajax_Guarda_documentopago(JSON.stringify({Forma}));
                                }
                            }
                        }
                        else if (Operacion=='addCan')
                        {   xajax_CargaCombo_documentopago(0,0,false);
                            BarButtonState(Sufijo,Operacion);
                            BarButtonStateDisabled(Sufijo, "btmpago");
                            ElementStatus(Sufijo,"id",frmDocumentoPago);
                            ElementClear(Sufijo,clsDocumentoPago);
                        }
                        else
                        xajax_CargaModal_documentopago(Operacion);  
                        return false;
                }

                function SearchByElement_documentopago(Sufijo,Elemento)
                {       Elemento.value = TrimElement(Elemento.value);
                        if (Elemento.name=="id"+Sufijo)
                        {   if (Elemento.value.length != 0)
                            {   if (BarButtonState(Sufijo,"Inactive"))
                                xajax_MuestraByID_documentopago(Elemento.value);                                  
                            }
                            else
                            BarButtonState(Sufijo,"Default");
                        }
                        else if (Elemento.id === "FindTextBx"+Sufijo)
                        {   xajax_MuestraByTX_documentopago(Elemento.value);
                        }     
                        else
                        xajax_BuscaModalByTX_documentopago("btmpago",Elemento.value);                    
                }
                 
                function SearchMpagoGetData_documentopago(Sufijo,Grilla)
                {       var documento = Grilla.cells[1].childNodes[0].nodeValue;
                            documento = documento +" "+ Grilla.cells[3].childNodes[0].nodeValue;
                            documento = documento +" "+ Grilla.cells[2].childNodes[0].nodeValue;
                        var idmedpago = Grilla.cells[0].childNodes[0].nodeValue;   
                        var idcajatip = Grilla.cells[6].childNodes[0].nodeValue;   
                        
                        document.getElementById("idmpago"+Sufijo).value = idmedpago;
                        document.getElementById("txmpago"+Sufijo).value = documento;
                        xajax_CargaCombo_documentopago(idcajatip,0,true);
                        cerrar();
                        return false;
                }
    
                function SearchGetData_documentopago(Sufijo,Grilla)
                {       if (IsDisabled(Sufijo,"addSav"))
                        {   BarButtonState(Sufijo,"Active");
                            document.getElementById("id"+Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                            document.getElementById("idmpago"+Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                            document.getElementById("txmpago"+Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
                            xajax_CargaCombo_documentopago(Grilla.cells[8].childNodes[0].nodeValue,Grilla.cells[7].childNodes[0].nodeValue,false);
                            if (Grilla.cells[3].childNodes[0].nodeValue=="SI"){
                                document.getElementById("CTRL"+Sufijo).value = 1;
                            }else{
                                document.getElementById("CTRL"+Sufijo).value = 0;
                            }    
                            document.getElementById("secuencial"+Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue;
                            if (Grilla.cells[5].childNodes[0].nodeValue=="|"){
                                document.getElementById("formato"+Sufijo).value = "";
                            }else{
                                document.getElementById("formato"+Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
                            }    
                        }
                        return false;
                }
                                                  
                function XAJAXResponse_documentopago(Sufijo,Mensaje,Datos)
                {       var objDocumentoPago = "CTRL:secuencial:formato:documento:idmpago:txmpago";
                        if (Mensaje==="GUARDADO")
                        {   BarButtonState(Sufijo,"Active");
                            BarButtonStateEnabled(Sufijo, "btmpago");
                            ElementSetValue(Sufijo,"id",Datos);
                            ElementStatus(Sufijo,"id",objDocumentoPago);
                            alert('Los datos se guardaron correctamente.');
                        }
                        else if(Mensaje==='ELIMINADO')
                        {   ElementSetValue(Sufijo,"estado",Datos);
                            alert("Los Datos se eliminaron correctamente");
                        }
                        else if(Mensaje==='NOEXISTEID')
                        {   BarButtonState(Sufijo,'Default');
                            ElementClear(Sufijo,"id");
                            alert(Datos);
                        }
                        else if(Mensaje==='EXCEPCION')
                        {    BarButtonState(Sufijo,"addNew"); 
                             alert(Datos);
                        } 
                }
                 
                 
         </script>
         </head>
<body>        
         <div class="FormBasic" style="width:550px">
               <div class="FormSectionMenu">              
              <?php  $ControlDocumentoPago->CargaDocumentoPagoBarButton($_GET['opcion']);
                     echo '<script type="text/javascript">';
                     echo '    BarButtonState(\''.$Sufijo.'\',"Default"); ';
                     echo '</script>';
              ?>
              </div>    
              <div class="FormSectionData">              
                  <form id="<?php echo 'Form'.$Sufijo; ?>">
                   <?php  $ControlDocumentoPago->CargaDocumentoPagoMantenimiento();  ?>
                   </form>
              </div>    
              <div class="FormSectionGrid"> 
                  
              <?php echo $ControlDocumentoPago->CargaDocumentoPagoSearchGrid();  ?>
              </div>  
         </div> 
        </body>
</html> 
              

