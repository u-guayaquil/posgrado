<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        
        require_once("src/rules/caja/servicio/ServicioDocumentoPago.php");
        require_once("src/modules/caja/documentos_pago/render/RenderDocumentoPago.php");
        
        require_once("src/rules/caja/servicio/ServicioMediosPago.php");
        require_once("src/modules/caja/medios_pago/render/RenderMediosPago.php");
        
        class ControlDocumentoPago
        {       private  $Sufijo; 
                private  $ServicioDocumentoPago;
                private  $RenderDocumentoPago;
                private  $ServicioMediosPago;
                private  $RenderMediosPago;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->ServicioDocumentoPago = new ServicioDocumentoPago();
                        $this->RenderDocumentoPago = new RenderDocumentoPago($Sufijo);
                        $this->ServicioMediosPago = new ServicioMediosPago();
                        $this->RenderMediosPago = new RenderMediosPago($Sufijo);
                }

                function CargaDocumentoPagoBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaDocumentoPagoMantenimiento()
                {       echo $this->RenderDocumentoPago->CreaDocumentoPagoMantenimiento();
                }

                function CargaDocumentoPagoSearchGrid()
                {       $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoDocumentoPago = $this->ServicioDocumentoPago->BuscarDocumentoPagoByDescripcion($prepareDQL);
                        echo $this->RenderDocumentoPago->CreaDocumentoPagoSearchGrid($datoDocumentoPago);
                }         
               
                function CargaComboDocumentoCajaTipo($IdCajaTipo,$IdDocumento,$Active=false)
                {       $ajaxRespon = new xajaxResponse();
                        return $this->RenderDocumentoPago->CreaComboDocumentoCajaTipo($ajaxRespon,$IdCajaTipo,$IdDocumento,$Active);
                }
               
                function MuestraDocumentoPagoByID($id)
                {       $ajaxRespon = new xajaxResponse();                        
                        $prepareDQL = array('id' => intval($id));
                        $Datos = $this->ServicioDocumentoPago->BuscarDocumentoPagoByID($prepareDQL);
                        return $this->RenderDocumentoPago->MuestraDocumentoPago($ajaxRespon,$Datos);
                }

                function MuestraDocumentoPagoByTX($texto)
                {       $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $Datos = $this->ServicioDocumentoPago->BuscarDocumentoPagoByDescripcion($prepareDQL);
                        return $this->RenderDocumentoPago->MuestraDocumentoPagoGrid($ajaxRespon,$Datos);
                }

                function GuardaDocumentoPago($Form)
                {       $ajaxRespon = new xajaxResponse();
                        $DocumentoPago  = json_decode($Form)->Forma;
                        $id = $this->ServicioDocumentoPago->GuardaDBDocumentoPago($DocumentoPago);
                        
                        if (is_numeric($id)){
                            $function = (empty($DocumentoPago->id) ? "MuestraDocumentoPagoGuardado" : "MuestraDocumentoPagoEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioDocumentoPago->BuscarDocumentoPagoByID($prepareDQL);
                            return $this->RenderDocumentoPago->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderDocumentoPago->MuestraDocumentoPagoExcepcion($ajaxRespon,$id);
                        }
                        return $ajaxRespon; 
                }
                        
                function EliminaDocumentoPago($id)
                {       $ajaxRespon = new xajaxResponse();
                        $this->ServicioDocumentoPago->DesactivaDocumentoPago(intval($id));
                        $Datos = $this->ServicioDocumentoPago->BuscarDocumentoPagoByDescripcion(array('limite' => 50, 'texto'=>''));
                        return $this->RenderDocumentoPago->MuestraDocumentoPagoEliminado($ajaxRespon,$Datos);
                }

                function CargaModalGrid($Operacion)
                {       $ajaxResp = new xajaxResponse();    
                        $prepareDQL = array('limite' => 50,'inicio' => 0, 'texto' => '', 'estado'=>'1');
                        if ($Operacion==="btmpago")    
                        {   $Datos = $this->ServicioMediosPago->BuscarPagoByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Medio Pagos";
                            $jsonModal['Carga'] = $this->RenderMediosPago->CreaModalGridPago($Operacion,$Datos);
                            $jsonModal['Ancho'] = $this->RenderMediosPago->WidthModalGrid();
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                     
                function ConsultaModalGridByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                    
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto), 'estado'=>1);
                        if ($Operacion==="btmpago")    
                        {   $Datos = $this->ServicioMediosPago->BuscarPagoByDescripcion($prepareDQL);
                            return $this->RenderMediosPago->MuestraModalGridPago($ajaxResp,$Operacion,$Datos);
                        }   
                }
        }

?>

