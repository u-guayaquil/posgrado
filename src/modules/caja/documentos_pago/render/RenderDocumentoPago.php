<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");        
        require_once("src/rules/caja/servicio/ServicioDocumento.php");

        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderDocumentoPago
        {   private $Sufijo;
            private $SearchGrid;
            private $Maxlen;
            private $Search;
            private $clsDoc;

            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Search = new SearchInput($Sufijo);
                    $this->Maxlen['id'] = 3; 
                    $this->clsDoc = "secuencial:formato:idmpago:txmpago";
            }
               
            function CreaDocumentoPagoMantenimiento()
            {       $Search = new SearchInput($this->Sufijo);
                    $DocumentoPago = '<table border="0" class="Form-Frame" cellpadding="0">';
                    $DocumentoPago.= '       <tr height="30">';
                    $DocumentoPago.= '           <td class="Form-Label" style="width:24%">Id</td>';
                    $DocumentoPago.= '           <td class="Form-Label" style="width:26%">';
                    $DocumentoPago.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\''.$this->clsDoc.'\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $DocumentoPago.= '           </td>';
                    $DocumentoPago.= '           <td class="Form-Label" style="width:20%"></td>';
                    $DocumentoPago.= '           <td class="Form-Label" style="width:05%"></td>';
                    $DocumentoPago.= '           <td class="Form-Label" style="width:25%"></td>';
                    $DocumentoPago.= '       </tr>';
                    $DocumentoPago.= '       <tr height="30">';
                    $DocumentoPago.= '           <td class="Form-Label">Medio de pago</td>';
                    $DocumentoPago.= '           <td class="Form-Label" colspan="4">';
                    $DocumentoPago.=                 $this->Search->TextSch("mpago","","")->Enabled(false)->Create("t12");
                    $DocumentoPago.= '           </td>';
                    $DocumentoPago.= '       </tr>';
                    $DocumentoPago.= '       <tr height="30">';
                    $DocumentoPago.= '           <td class="Form-Label">Documento</td>';
                    $DocumentoPago.= '           <td class="Form-Label" id="lblDocTipo">';
                    $DocumentoPago.=             $this->CreaComboDocumento();
                    $DocumentoPago.= '           </td>';
                    $DocumentoPago.= '           <td class="Form-Label">Ctrl. Secuencia</td>';
                    $DocumentoPago.= '           <td class="Form-Label">';
                    $DocumentoPago.= '               <input type="checkbox" id="CTRL'.$this->Sufijo.'" name="CTRL'.$this->Sufijo.'" value="1" checked disabled/>'; 
                    $DocumentoPago.= '           </td>';
                    $DocumentoPago.= '           <td class="Form-Label">';
                    $DocumentoPago.= '               <input type="text" class="txt-right t02" id="secuencial'.$this->Sufijo.'" name="secuencial'.$this->Sufijo.'" value="" maxlength="7" onKeyDown="return soloNumeros(event); " disabled/>';
                    $DocumentoPago.= '           </td>';
                    $DocumentoPago.= '       </tr>';
                    $DocumentoPago.= '       <tr height="30">';
                    $DocumentoPago.= '           <td class="Form-Label">Formato</td>';
                    $DocumentoPago.= '           <td class="Form-Label" colspan="4">';
                    $DocumentoPago.= '               <input type="text" class="txt-upper t13" id="formato'.$this->Sufijo.'" name="formato'.$this->Sufijo.'" value="" maxlength="255" disabled/>';
                    $DocumentoPago.= '           </td>';
                    $DocumentoPago.= '       </tr>';
                    $DocumentoPago.= '</table>';
                    return $DocumentoPago;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']            = array('25px','center','');
                    $Columns['Medio de pago'] = array('280px','left','');
                    $Columns['Documento']  = array('112px','left','');
                    $Columns['Ctrl']       = array('30px','center','');
                    $Columns['Sec']        = array('50px','right','');
                    $Columns['Formato']    = array('0px','left','none');
                    $Columns['idCajaPago'] = array('0px','left','none');
                    $Columns['idDocumento']= array('0px','left','none');
                    $Columns['idCajaTipo']= array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '100px','AltoRow' => '20px');
            }

            function CreaDocumentoPagoSearchGrid($Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->DocumentoPagoGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }        

            private function DocumentoPagoGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $documentopago)
                    {       $id = str_pad($documentopago['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($documentopago['descripcion'])." ".trim($documentopago['subtipo'])." ".trim($documentopago['mpago']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($documentopago['documento']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',($documentopago['ctrlsecudoc']==1 ? "SI" : "NO"));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documentopago['iniciosecdoc']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',(trim($documentopago['formatodoc'])=="" ? "|" : trim($documentopago['formatodoc'])));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documentopago['idcajapag']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documentopago['idcajadoc']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documentopago['idcajatipo']);
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,"black");
                    }
                    return $ObjSchGrid->CreaSearchTableDetalle();
            }
              
            private function CreaComboDocumento()
            {       $Select = new ComboBox($this->Sufijo); 
                    $ServicioDocumento = new ServicioDocumento();
                    $datoDocumento = $ServicioDocumento->BuscarDocumentoByDescripcion(array('tipo'=>0,'estado'=>1));  
                    return $Select->Combo("documento",$datoDocumento)->Selected(1)->Create("s05");
            }
            
            function CreaComboDocumentoCajaTipo($Ajax,$IdCajaTipo,$IdDocumento,$Active)
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioDocumento = new ServicioDocumento();
                    $datoDocumento = $ServicioDocumento->BuscarDocumentoByDescripcion(array('estado'=>1,'tipo'=>$IdCajaTipo));  
                    if ($IdDocumento==0){
                        $Ajax->Assign("lblDocTipo","innerHTML",$Select->Combo("documento",$datoDocumento)->Enabled($Active)->Create("s05"));
                    }else{
                        $Ajax->Assign("lblDocTipo","innerHTML",$Select->Combo("documento",$datoDocumento)->Enabled($Active)->Selected($IdDocumento)->Create("s05"));
                    }    
                    return $Ajax; 
            }
              
            function MuestraDocumentoPago($Ajax,$Datos)
            {       foreach ($Datos as $Dato) 
                    {   $xAjax = $this->CreaComboDocumentoCajaTipo($Ajax,$Datos[0]['idcajatipo'],$Datos[0]['idcajadoc'],false);
                        $xAjax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $xAjax->Assign("txmpago".$this->Sufijo,"value", trim($Datos[0]['descripcion'])." ".trim($Datos[0]['subtipo'])." ".trim($Datos[0]['mpago']));
                        $xAjax->Assign("idmpago".$this->Sufijo,"value", $Datos[0]['idcajapag']);
                        $xAjax->Assign("CTRL".$this->Sufijo,"checked", ($Datos[0]['ctrlsecudoc']==1 ? true : false));
                        $xAjax->Assign("secuencial".$this->Sufijo,"value", $Datos[0]['iniciosecdoc']);
                        $xAjax->Assign("formato".$this->Sufijo,"value", trim($Datos[0]['formatodoc']));
                        $xAjax->call("BarButtonState",$this->Sufijo,"Active");
                        return $xAjax;
                    }    
                    return $this->RespuestaDocumentoPago($Ajax,"NOEXISTEID","No existe un término con el ID ingresado.");
                    
            }
              
            function MuestraDocumentoPagoGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->DocumentoPagoGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }
               
            function MuestraDocumentoPagoGuardado($Ajax,$DocumentoPago)
            {       if (count($DocumentoPago)>0)
                    {   $id = str_pad($DocumentoPago[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraDocumentoPagoGrid($Ajax,$DocumentoPago);
                        return $this->RespuestaDocumentoPago($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaDocumentoPago($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraDocumentoPagoEditado($Ajax,$DocumentoPago)
            {       foreach ($DocumentoPago as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraDocumentoPagoRowGrid($Ajax, $datos);
                            return $this->RespuestaDocumentoPago($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaDocumentoPago($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraDocumentoPagoEliminado($Ajax,$DocumentoPago)
            {       $xAjax = $this->MuestraDocumentoPagoGrid($Ajax,$DocumentoPago);
                    $xAjax->call("BarButtonState",$this->Sufijo,"Default");
                    $xAjax->call("ElementClear",$this->Sufijo,$this->clsDoc.":id");  
                    return $xAjax;
            }

            function MuestraDocumentoPagoRowGrid($Ajax,$DocumentoPago,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($DocumentoPago['id'],$estado);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($DocumentoPago['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($DocumentoPago['descripcion'])." ".trim($DocumentoPago['subtipo'])." ".trim($DocumentoPago['mpago']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($DocumentoPago['documento']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",($DocumentoPago['ctrlsecudoc']==1 ? "SI" : "NO"));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",$DocumentoPago['iniciosecdoc']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",(trim($DocumentoPago['formatodoc'])=="" ? "|" : trim($documentopago['formatodoc']))); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$DocumentoPago['idcajapag']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$DocumentoPago['idcajadoc']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",$DocumentoPago['idcajatipo']);
                    return $Ajax;
            }

            private function RespuestaDocumentoPago($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
                
            function MuestraDocumentoPagoExcepcion($Ajax,$Msg)
            {       return $this->RespuestaDocumentoPago($Ajax,"EXCEPCION",$Msg);    
            }
        }
?>

