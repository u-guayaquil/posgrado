<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");        
        require_once("src/rules/caja/servicio/ServicioMedios.php");
        require_once("src/rules/caja/servicio/ServicioSubTipo.php");
        
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderMediosPago
        {   private $Sufijo;
            private $SearchGrid;
            private $Maxlen;
            private $Fechas;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Fechas = new DateControl();
                    $this->Maxlen['id'] = 3;
            }
               
            function CreaPagoMantenimiento()
            {       $Search = new SearchInput($this->Sufijo);
                    $FrmPago = "estado:descripcion:txSucursal:idSucursal:txCuentaPago:idCuentaPago:medio:subtipo";
                       
                    $Pago = '<table class="Form-Frame" cellpadding="0" border="0">';
                    $Pago.= '       <tr height="30">';
                    $Pago.= '           <td class="Form-Label" style="width:20%">Id</td>';
                    $Pago.= '           <td class="Form-Label" style="width:40%">';
                    $Pago.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\''.$FrmPago.'\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Pago.= '           </td>';
                    $Pago.= '           <td class="Form-Label" style="width:14%"></td>';
                    $Pago.= '           <td class="Form-Label" style="width:26%"></td>';
                    $Pago.= '       </tr>';
                    $Pago.= '       <tr height="30">';
                    $Pago.= '           <td class="Form-Label">Medio de pago</td>';
                    $Pago.= '           <td class="Form-Label">';
                    $Pago.=             $this->CreaComboMedio();
                    $Pago.= '           </td>';
                    $Pago.= '           <td class="Form-Label">Clase</td>';
                    $Pago.= '           <td class="Form-Label" id="lblSubTipo">';
                    $Pago.=             $this->CreaComboSubTipo(array('tipo'=>0));
                    $Pago.= '           </td>';
                    $Pago.= '       </tr>';
                    $Pago.= '       <tr height="30">';
                    $Pago.= '           <td class="Form-Label">Descripci&oacute;n</td>';
                    $Pago.= '           <td class="Form-Label">';
                    $Pago.= '           <input type="text" class="txt-upper t09" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                    $Pago.= '           </td>';
                    $Pago.= '           <td class="Form-Label">Sucursal</td>';
                    $Pago.= '           <td class="Form-Label">';
                    $Pago.=             $Search->TextSch("Sucursal","","")->Enabled(false)->Create("t05");
                    $Pago.= '           </td>';
                    $Pago.= '       </tr>';
                    $Pago.= '       <tr height="30">';
                    $Pago.= '           <td class="Form-Label">Cuenta</td>';
                    $Pago.= '           <td class="Form-Label">';
                    $Pago.=             $Search->TextSch("CuentaPago","","")->Enabled(false)->Create("t08");
                    $Pago.= '           </td>';
                    $Pago.= '           <td class="Form-Label">Estado</td>';
                    $Pago.= '           <td class="Form-Label">';
                    $Pago.=             $this->CreaComboEstado();
                    $Pago.= '           </td>';
                    $Pago.= '       </tr>';
                    $Pago.= '</table>';
                    return $Pago;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']          = array('30px','center','');
                    $Columns['Medio']       = array('120px','left','');
                    $Columns['descripcion'] = array('130px','left','');
                    $Columns['SubTipo']     = array('100px','left','');
                    $Columns['Sucursal']    = array('100px','left','');
                    $Columns['Estado']      = array('100px','left','');
                    $Columns['idsucursal']  = array('0px','left','none');
                    $Columns['idctacontable'] = array('0px','left','none');
                    $Columns['cuenta']        = array('0px','left','none');
                    $Columns['idMedio']       = array('0px','left','none');
                    $Columns['idSubtipo']     = array('0px','left','none');
                    $Columns['estado']        = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
            }

            function CreaPagoSearchGrid($Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->PagoGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }        

            private function PagoGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $Pago)
                    {   $id = str_pad($Pago['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Pago['medio']));
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Pago['descripcion']));
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Pago['subtipo']));
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Pago['sucursal']));
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($Pago['txtestado']));
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Pago['idsucursal']);
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',strtoupper(trim($Pago['cuentacontable'])));
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Pago['idctacontable']);
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Pago['idcajamed']);
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Pago['idsubtipo']);
                        $ObjSchGrid->CreaSearchCellsDetalle($id,'',$Pago['idestado']);
                        $ObjSchGrid->CreaSearchRowsDetalle ($id,($Pago['idestado']==0 ? "red" : ""));
                    }
                    return $ObjSchGrid->CreaSearchTableDetalle();
            }

            private function CreaComboEstado()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioEstado = new ServicioEstado();
                    $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s03");
            }
            
            private function CreaComboMedio()
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioMedios = new ServicioMedios();
                    $datoMedio = $ServicioMedios->BuscarMedioByArray(array('oper' => '1,3','order'=>'m.idcajatipo'));
                    return $Select->Combo("medio",$datoMedio)->Evento()->Create("s09");
            }
            
            function CreaComboSubTipoMedio($Ajax,$IdMedio,$IdSubTp,$Active)
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioSubTipo = new ServicioSubTipo();
                    $datosSubTipoMed = $ServicioSubTipo->BuscarSubTipoMedios($IdMedio);
                    if ($IdSubTp==0){
                        $Ajax->Assign("lblSubTipo","innerHTML",$Select->Combo("subtipo",$datosSubTipoMed)->Enabled($Active)->Create("s06"));
                    }else{
                        $Ajax->Assign("lblSubTipo","innerHTML",$Select->Combo("subtipo",$datosSubTipoMed)->Enabled($Active)->Selected($IdSubTp)->Create("s06"));
                    }    
                    return $Ajax; 
            }
            
            private function CreaComboSubTipo($Params)
            {       $Select = new ComboBox($this->Sufijo);                        
                    $ServicioSubTipo = new ServicioSubTipo();
                    $datoSubTipo = $ServicioSubTipo->BuscarSubTipoTodos($Params);
                    return $Select->Combo("subtipo",$datoSubTipo)->Create("s06");
            }
              
            function MuestraPago($Ajax,$Datos)
            {       if($Datos[0]['id'] != ''){
                        $xAjax = $this->CreaComboSubTipoMedio($Ajax,$Datos[0]['idcajamed'],$Datos[0]['idsubtipo'],false);
                        
                        $xAjax->Assign("id".$this->Sufijo,"value", str_pad($Datos[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $xAjax->Assign("descripcion".$this->Sufijo,"value", trim($Datos[0]['descripcion']));
                        $xAjax->Assign("txSucursal".$this->Sufijo,"value", trim($Datos[0]['sucursal']));
                        $xAjax->Assign("idSucursal".$this->Sufijo,"value", $Datos[0]['idsucursal']);
                        $xAjax->Assign("txCuentaPago".$this->Sufijo,"value", strtoupper(trim($Datos[0]['cuentacontable'])));
                        $xAjax->Assign("idCuentaPago".$this->Sufijo,"value", $Datos[0]['idctacontable']);
                        $xAjax->Assign("medio".$this->Sufijo,"value", $Datos[0]['idcajamed']);
                        $xAjax->Assign("subtipo".$this->Sufijo,"value", $Datos[0]['idsubtipo']);
                        $xAjax->Assign("estado".$this->Sufijo,"value", $Datos[0]['idestado']);
                        $xAjax->call("BarButtonState",$this->Sufijo,"Active");
                        return $xAjax;
                    }
                    else{
                        return $this->RespuestaPago($Ajax,"NOEXISTEID","No existe un Pago con ese ID.");
                    }
            }
              
            function MuestraPagoGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->PagoGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }
               
            function MuestraPagoGuardado($Ajax,$Pago)
            {       if (count($Pago)>0)
                    {   $id = str_pad($Pago[0]['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraPagoGrid($Ajax,$Pago);
                        return $this->RespuestaPago($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaPago($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraPagoEditado($Ajax,$Pago)
            {       foreach ($Pago as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraPagoRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaPago($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaPago($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraPagoEliminado($Ajax,$Pago)
            {       foreach ($Pago as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraPagoRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaPago($xAjax,"ELIMINADO",0);
                    }  
                    return $this->RespuestaPago($xAjax,"EXCEPCION","No se eliminó la información.");
            }
              
            function MuestraPagoRowGrid($Ajax,$Pago,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($Pago['id'],$estado);
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Pago['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Pago['medio']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Pago['descripcion']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Pago['subtipo']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($Pago['sucursal']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Pago['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",$Pago['idsucursal']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",strtoupper(trim($Pago['cuentacontable']))); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",$Pago['idctacontable']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],9),"innerHTML",$Pago['idcajamed']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],10),"innerHTML",$Pago['idsubtipo']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],11),"innerHTML",$Pago['idestado']);
                    return $Ajax;
            }

            function MuestraPagoExcepcion($Ajax,$Msg)
            {       return $this->RespuestaPago($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaPago($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
                
            /*** MODAL ***/
            private function SearchGridModalConfig()
            {       $Columns['Id']          = array('40px','center','');
                    $Columns['Medio']       = array('180px','left','');
                    $Columns['Descripción'] = array('180px','left','');
                    $Columns['SubTipo']     = array('132px','left','');
                    $Columns['IdCajaMedio'] = array('0px','left','none');
                    $Columns['IdSubTipo']   = array('0px','left','none');
                    $Columns['IdCajTipo']   = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
            }

            function CreaModalGridPago($Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->GridDataHTMLModalPago($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($GrdDataHTML);
            }
            
            function WidthModalGrid()
            {       return 563;
            }
            
            function MuestraModalGridPago($Ajax,$Operacion,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                    $GrdDataHTML = $this->GridDataHTMLModalPago($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                    return $Ajax;
            }        

            private function GridDataHTMLModalPago($Grid,$Datos)
            {       foreach ($Datos as $Opcion)
                    {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $Grid->CreaSearchCellsDetalle($id,'',$id);
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['medio']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['descripcion']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['subtipo']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['idcajamed']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['idsubtipo']));
                            $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['idcajatipo']));
                            $Grid->CreaSearchRowsDetalle ($id,"black");
                    }
                    return $Grid->CreaSearchTableDetalle();
            }
        }
?>

