<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
        require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
        
        require_once("src/rules/caja/servicio/ServicioMediosPago.php");
        require_once("src/modules/caja/medios_pago/render/RenderMediosPago.php");        

        require_once("src/rules/general/servicio/ServicioSucursal.php");
        require_once("src/modules/general/sucursal/render/RenderSucursal.php");        

         class ControlMediosPago
         {      private  $Sufijo; 
                private  $ServicioMediosPago;
                private  $ServicioPlanCuentas;
                private  $ServicioSucursal;
                private  $RenderMediosPago;
                private  $RenderPlanCuentas;
                private  $RenderSucursal;

                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->ServicioMediosPago = new ServicioMediosPago();
                        $this->RenderMediosPago = new RenderMediosPago($Sufijo);
                        
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
                        
                        $this->ServicioSucursal = new ServicioSucursal();
                        $this->RenderSucursal = new RenderSucursal($Sufijo);
                }

                function CargaPagoBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
                function CargaPagoMantenimiento()
                {        echo $this->RenderMediosPago->CreaPagoMantenimiento();
                }

                function CargaComboSubTipoMedio($IdMedio,$IdSubTp,$Active=false)
                {       $ajaxRespon = new xajaxResponse();
                        return $this->RenderMediosPago->CreaComboSubTipoMedio($ajaxRespon,$IdMedio,$IdSubTp,$Active);
                }
               
               function CargaPagoSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoPago = $this->ServicioMediosPago->BuscarPagoByDescripcion($prepareDQL);
                        echo $this->RenderMediosPago->CreaPagoSearchGrid($datoPago);
               }         
               
               function MuestraPagoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoPago = $this->ServicioMediosPago->BuscarPagoByID($prepareDQL);
                        return $this->RenderMediosPago->MuestraPago($ajaxRespon,$datoPago);
               }

               function MuestraPagoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoPago = $this->ServicioMediosPago->BuscarPagoByDescripcion($prepareDQL);
                        return $this->RenderMediosPago->MuestraPagoGrid($ajaxRespon,$datoPago);
               }

               function GuardaPago($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Pago  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioMediosPago->GuardaDBPago($Pago);
                        if (is_numeric($id)){
                            $function = (empty($Pago->id) ? "MuestraPagoGuardado" : "MuestraPagoEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioMediosPago->BuscarPagoByID($prepareDQL);
                            return $this->RenderMediosPago->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderMediosPago->MuestraPagoExcepcion($ajaxRespon,intval($id));
                        }     
               }
               
               function EliminaPago($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioMediosPago->DesactivaPago(intval($id));
                        $Datos = $this->ServicioMediosPago->BuscarPagoByDescripcion($prepareDQL);
                        return $this->RenderMediosPago->MuestraPagoEliminado($ajaxRespon,$Datos);
                }
                
                function CargaModalGridPago($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                        if ($Operacion==="btCuentaPago")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '','estado' => '1');
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cuentas";
                            $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                            $jsonModal['Ancho'] = "750";
                        }
                        else
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '','estado' => array(1));
                            $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Sucursal";
                            $jsonModal['Carga'] = $this->RenderSucursal->CreaModalGridSucursal($Operacion,$Datos);
                            $jsonModal['Ancho'] = "605";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                                
                function ConsultaModalGridPagoByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        
                        if ($Operacion==="btCuentaPago")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            return $this->RenderPlanCuentas->MuestraModalGridPlanCuentas($ajaxResp,$Operacion,$Datos);   
                        }else{
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                            $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                            return $this->RenderSucursal->MuestraModalGridSucursal($ajaxResp,$Operacion,$Datos);
                        }                           
                }
                
         }

?>

