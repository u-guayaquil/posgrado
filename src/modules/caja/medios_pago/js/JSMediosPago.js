
    function ButtonClick_pago(Sufijo, Operacion)
    {       var objPago = "id:estado:descripcion:txSucursal:idSucursal:txCuentaPago:idCuentaPago:medio:subtipo";
            var frmPago = "estado:descripcion:txSucursal:idSucursal:txCuentaPago:idCuentaPago:medio:subtipo";

            if (Operacion=='addNew')
            {   BarButtonState(Sufijo, Operacion);
                ElementStatus(Sufijo, frmPago, "id:estado");
                BarButtonStateEnabled(Sufijo, "btCuentaPago:btSucursal");
                ElementClear(Sufijo, objPago);
                xajax_CargaCombo_pago(ElementGetValue(Sufijo,"medio"),0,true);
            } 
            else if (Operacion=='addMod')
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateEnabled(Sufijo, "btCuentaPago:btSucursal");
                ElementStatus(Sufijo, frmPago, "id");
                if(ElementGetValue(Sufijo,"estado")==='0'){
                   ElementStatus(Sufijo, "estado", "");
                }else{
                   ElementStatus(Sufijo, "", "estado");
                }
                xajax_CargaCombo_pago(ElementGetValue(Sufijo,"medio"),ElementGetValue(Sufijo,"subtipo"),true);
            } 
            else if (Operacion == 'addDel')
            {   if (ElementGetValue(Sufijo, "estado") == 1)
                {   if (confirm("Desea inactivar este registro?"))
                    xajax_Elimina_pago(ElementGetValue(Sufijo, "id"));
                } 
                else
                alert("El registro se encuentra inactivo");
            } 
            else if (Operacion == 'addSav')
            {   if (ElementValidateBeforeSave(Sufijo, frmPago))
                {   if (BarButtonState(Sufijo, "Inactive"))
                    {   var Forma = PrepareElements(Sufijo, objPago);
                        xajax_Guarda_pago(JSON.stringify({Forma}));
                    }
                }
            } 
            else if (Operacion == 'addCan')
            {   BarButtonState(Sufijo, Operacion);
                BarButtonStateDisabled(Sufijo, "btCuentaPago:btSucursal");
                ElementStatus(Sufijo, "id", frmPago);
                ElementClear(Sufijo, objPago);
            } 
            else if (Operacion == 'addImp')
            {
            } 
            else 
            xajax_CargaModal_pago(Operacion);
        
        return false;
    }

    function SearchByElement_pago(Sufijo, Elemento)
    {       Elemento.value = TrimElement(Elemento.value);
            switch (Elemento.id) 
            {   case "id" + Sufijo:
                if (Elemento.value.length!=0)
                {   if (BarButtonState(Sufijo, "Inactive"))
                    xajax_MuestraByID_pago(Elemento.value);
                }
                else
                    BarButtonState(Sufijo, "Default");
                break;
                case "FindCuentaPagoTextBx" + Sufijo:
                    xajax_BuscaModalByTX_pago("btCuentaPago", Elemento.value);
                break;
                case 'FindTextBx' + Sufijo:
                    xajax_MuestraByTX_pago(Elemento.value);
                break;
                case 'FindSucursalTextBx' + Sufijo:
                    xajax_BuscaModalByTX_pago("btSucursal", Elemento.value);
                break;
            }
    }

    function SearchGetData_pago(Sufijo, Grilla)
    {       if (IsDisabled(Sufijo,"addSav"))
            {   BarButtonState(Sufijo, "Active");
                Medio = Grilla.cells[9].childNodes[0].nodeValue;
                SubTp = Grilla.cells[10].childNodes[0].nodeValue;
                xajax_CargaCombo_pago(Medio,SubTp,false);
                document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
                document.getElementById("descripcion" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
                document.getElementById("medio" + Sufijo).value = Medio; //3
                document.getElementById("txSucursal" + Sufijo).value = Grilla.cells[4].childNodes[0].nodeValue; //4
                document.getElementById("idCuentaPago" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
                document.getElementById("txCuentaPago" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
                document.getElementById("idSucursal" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
                document.getElementById("subtipo" + Sufijo).value = SubTp;
                document.getElementById("estado" + Sufijo).value = Grilla.cells[11].childNodes[0].nodeValue;
            }
            return false;
    }

    function SearchCuentaPagoGetData_pago(Sufijo, DatosGrid)
    {       document.getElementById("idCuentaPago" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txCuentaPago" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function SearchSucursalGetData_pago(Sufijo, DatosGrid)
    {       document.getElementById("idSucursal" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
            document.getElementById("txSucursal" + Sufijo).value = DatosGrid.cells[1].childNodes[0].nodeValue;
            cerrar();
            return false;
    }

    function XAJAXResponse_pago(Sufijo, Mensaje, Datos)
    {       var objPago = "estado:porcentaje:impuesto:txSucursal:idSucursal:txCuentaPago:idCuentaPago:descripcion:medio:subtipo";
            if (Mensaje === "GUARDADO")
            {   BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, "btCuentaPago:btSucursal");
                ElementSetValue(Sufijo, "id", Datos);
                ElementStatus(Sufijo, "id", objPago);
                alert('Los datos se guardaron correctamente.');
            }
            else if(Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
                alert("Los Datos se eliminaron correctamente");
            } 
            else if(Mensaje === 'NOEXISTEID')
            {   BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "id");
                alert(Datos);
            }
            else if (Mensaje === 'EXCEPCION')
            {   BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
    }
    
    function ControlMedio_pago(Sufijo, Combo)
    {       xajax_CargaCombo_pago(Combo.value,0,true);
    }