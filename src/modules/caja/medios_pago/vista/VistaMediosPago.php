<?php
         $Sufijo = '_pago';

         require_once('src/modules/caja/medios_pago/controlador/ControlMediosPago.php');
         $ControlMediosPago = new ControlMediosPago($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlMediosPago,'GuardaPago'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlMediosPago,'MuestraPagoByID'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlMediosPago,'MuestraPagoByTX'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlMediosPago,'EliminaPago'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlMediosPago,'CargaModalGridPago'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlMediosPago,'ConsultaModalGridPagoByTX'));
        $xajax->register(XAJAX_FUNCTION,array('CargaCombo'.$Sufijo, $ControlMediosPago,'CargaComboSubTipoMedio'));
         
        $xajax->processRequest();

?>
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title></title>
            <?php   $xajax->printJavascript(); 
                    require_once('src/utils/links.php'); 
            ?>
            <script type="text/javascript" src="src/modules/caja/medios_pago/js/JSMediosPago.js"></script>
        </head>
        
        <body>        
        <div class="FormBasic" style="width:635px">
            <div class="FormSectionMenu">              
            <?php   $ControlMediosPago->CargaPagoBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '    BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Sufijo; ?>">
                <?php  $ControlMediosPago->CargaPagoMantenimiento();  ?>
                </form>
            </div>    
            <div class="FormSectionGrid"> 
            <?php echo $ControlMediosPago->CargaPagoSearchGrid();  ?>
            </div>  
         </div> 
        </body>
</html> 
              

