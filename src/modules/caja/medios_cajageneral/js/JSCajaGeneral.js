function ButtonClick_cajageneral(Sufijo, Operacion)
{
    var objCajaGeneral = "id:estado:descripcion:txSucursal:idSucursal:txCuentaCajaGeneral:idCuentaCajaGeneral";
    var frmCajaGeneral = "descripcion:txSucursal:idSucursal:txCuentaCajaGeneral:idCuentaCajaGeneral";

    if (Operacion == 'addNew')
    {
        BarButtonState(Sufijo, Operacion);
        ElementStatus(Sufijo, frmCajaGeneral, "id:estado");
        BarButtonStateEnabled(Sufijo, "btCuentaCajaGeneral:btSucursal");
        ElementClear(Sufijo, frmCajaGeneral);
    } else if (Operacion == 'addMod')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateEnabled(Sufijo, "btCuentaCajaGeneral:btSucursal");
        if (ElementGetValue(Sufijo,"estado")==='0')
            ElementStatus(Sufijo,frmCajaGeneral+":estado","id");
        else
            ElementStatus(Sufijo,frmCajaGeneral,"id");
    } else if (Operacion == 'addDel')
    {
        if (ElementGetValue(Sufijo, "estado") == 1)
        {
            if (confirm("¿Desea inactivar este registro?"))
                xajax_Elimina_cajageneral(ElementGetValue(Sufijo, "id"));
        } else
            alert("El registro se encuentra inactivo");
    } else if (Operacion == 'addSav')
    {
        if (ElementValidateBeforeSave(Sufijo, frmCajaGeneral))
        {
            if (BarButtonState(Sufijo, "Inactive"))
            {
                var Forma = PrepareElements(Sufijo, objCajaGeneral);
                xajax_Guarda_cajageneral(JSON.stringify({Forma}));
            }
        }
    } else if (Operacion == 'addCan')
    {
        BarButtonState(Sufijo, Operacion);
        BarButtonStateDisabled(Sufijo, "btCuentaCajaGeneral:btSucursal");
        ElementStatus(Sufijo, "id", frmCajaGeneral+':estado');
        ElementClear(Sufijo, objCajaGeneral);
    } else if (Operacion == 'addImp')
    {
    } else {
        xajax_CargaModal_cajageneral(Operacion);
    }

    return false;
}

function SearchByElement_cajageneral(Sufijo, Elemento)
{
    Elemento.value = TrimElement(Elemento.value);
    if (Elemento.name == "id" + Sufijo)
    {
        if (Elemento.value.length != 0)
        {
            ContentFlag = document.getElementById("descripcion" + Sufijo);
            if (ContentFlag.value.length == 0)
            {
                if (BarButtonState(Sufijo, "Inactive"))
                    xajax_MuestraByID_cajageneral(Elemento.value);
            }
        } else
            BarButtonState(Sufijo, "Default");
    } else if (Elemento.id === "FindCuentaCajaGeneralTextBx" + Sufijo)
    {
        xajax_BuscaModalByTX_cajageneral("btCuentaCajaGeneral", Elemento.value);
    } else if (Elemento.id === "FindSucursalTextBx" + Sufijo)
    {
        xajax_BuscaModalByTX_cajageneral("btSucursal", Elemento.value);
    } else {
        xajax_MuestraByTX_cajageneral(Elemento.value);
    }
}

function SearchGetData_cajageneral(Sufijo, Grilla)
{   if (IsDisabled(Sufijo,"addSav")){
        BarButtonState(Sufijo, "Active");

        document.getElementById("id" + Sufijo).value = Grilla.cells[0].childNodes[0].nodeValue;
        document.getElementById("descripcion" + Sufijo).value = Grilla.cells[1].childNodes[0].nodeValue;
        document.getElementById("idCuentaCajaGeneral" + Sufijo).value = Grilla.cells[7].childNodes[0].nodeValue;
        document.getElementById("txCuentaCajaGeneral" + Sufijo).value = Grilla.cells[6].childNodes[0].nodeValue;
        document.getElementById("idSucursal" + Sufijo).value = Grilla.cells[5].childNodes[0].nodeValue;
        document.getElementById("txSucursal" + Sufijo).value = Grilla.cells[2].childNodes[0].nodeValue;
        document.getElementById("estado" + Sufijo).value = Grilla.cells[8].childNodes[0].nodeValue;
    }
    return false;
}

function SearchCuentaCajaGeneralGetData_cajageneral(Sufijo, DatosGrid)
{
    document.getElementById("idCuentaCajaGeneral" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txCuentaCajaGeneral" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}

function SearchSucursalGetData_cajageneral(Sufijo, DatosGrid)
{
    document.getElementById("idSucursal" + Sufijo).value = DatosGrid.cells[0].childNodes[0].nodeValue;
    document.getElementById("txSucursal" + Sufijo).value = DatosGrid.cells[2].childNodes[0].nodeValue;
    cerrar();
    return false;
}

    function XAJAXResponse_cajageneral(Sufijo, Mensaje, Datos)
    {       var objCajaGeneral = "estado:porcentaje:impuesto:txSucursal:idSucursal:txCuentaCajaGeneral:idCuentaCajaGeneral:descripcion";
            if (Mensaje === "GUARDADO")
            {   BarButtonState(Sufijo, "Active");
                BarButtonStateDisabled(Sufijo, "btCuentaCajaGeneral:btSucursal");
                ElementSetValue(Sufijo, "id", Datos);
                ElementStatus(Sufijo, "id", objCajaGeneral);
                alert('Los datos se guardaron correctamente.');
            }
            else if (Mensaje === 'ELIMINADO')
            {   ElementSetValue(Sufijo, "estado", Datos);
                alert("Los datos se eliminaron correctamente");
            } 
            else if (Mensaje === 'NOEXISTEID')
            {   BarButtonState(Sufijo, 'Default');
                ElementClear(Sufijo, "id");
                alert(Datos);
            } 
            else if (Mensaje === 'EXCEPCION')
            {   BarButtonState(Sufijo, "addNew");
                alert(Datos);
            }
    }