<?php
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        
        require_once("src/rules/caja/servicio/ServicioCajaGeneral.php");
        require_once("src/modules/caja/medios_cajageneral/render/RenderCajaGeneral.php");
        
        require_once("src/rules/general/servicio/ServicioPlanCuentas.php");
        require_once("src/modules/general/plancuentas/render/RenderPlanCuentas.php");
        
        require_once("src/rules/general/servicio/ServicioSucursal.php");
        require_once("src/modules/general/sucursal/render/RenderSucursal.php");        
        
         class ControlCajaGeneral
         {     private  $Sufijo; 
               private  $ServicioCajaGeneral;
               private  $ServicioPlanCuentas;
               private  $ServicioSucursal;
               private  $RenderCajaGeneral;
               private  $RenderPlanCuentas;
               private  $RenderSucursal;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioCajaGeneral = new ServicioCajaGeneral();
                        $this->ServicioPlanCuentas = new ServicioPlanCuentas();
                        $this->ServicioSucursal = new ServicioSucursal();
                        $this->RenderCajaGeneral = new RenderCajaGeneral($Sufijo);
                        $this->RenderSucursal = new RenderSucursal($Sufijo);
                        $this->RenderPlanCuentas = new RenderPlanCuentas($Sufijo);
               }

               function CargaCajaGeneralBarButton($Opcion)
               {        $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
               }
               
               function CargaCajaGeneralMantenimiento()
               {        echo $this->RenderCajaGeneral->CreaCajaGeneralMantenimiento();
               }

               function CargaCajaGeneralSearchGrid()
               {        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => '');
                        $datoCajaGeneral = $this->ServicioCajaGeneral->BuscarCajaGeneralByDescripcion($prepareDQL);
                        echo $this->RenderCajaGeneral->CreaCajaGeneralSearchGrid($datoCajaGeneral);
               }         
               
               function MuestraCajaGeneralByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $datoCajaGeneral = $this->ServicioCajaGeneral->BuscarCajaGeneralByID($prepareDQL);
                        return $this->RenderCajaGeneral->MuestraCajaGeneral($ajaxRespon,$datoCajaGeneral);
               }

               function MuestraCajaGeneralByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                        $datoCajaGeneral = $this->ServicioCajaGeneral->BuscarCajaGeneralByDescripcion($prepareDQL);
                        return $this->RenderCajaGeneral->MuestraCajaGeneralGrid($ajaxRespon,$datoCajaGeneral);
               }

               function GuardaCajaGeneral($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $CajaGeneral  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioCajaGeneral->GuardaDBCajaGeneral($CajaGeneral);
                        if (is_numeric($id)){
                            $function = (empty($CajaGeneral->id) ? "MuestraCajaGeneralGuardada" : "MuestraCajaGeneralEditada");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioCajaGeneral->BuscarCajaGeneralByID($prepareDQL);
                            return $this->RenderCajaGeneral->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderCajaGeneral->MuestraCajaGeneralExcepcion($ajaxRespon,intval($id));
                        }   
               }
               
               function EliminaCajaGeneral($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioCajaGeneral->DesactivaCajaGeneral(intval($id));
                        $Datos = $this->ServicioCajaGeneral->BuscarCajaGeneralByDescripcion($prepareDQL);
                        return $this->RenderCajaGeneral->MuestraCajaGeneralEliminada($ajaxRespon,$Datos);
                }
                
                function CargaModalGridCajaGeneral($Operacion)
                {       $ajaxResp = new xajaxResponse(); 
                        $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => '');
                        if ($Operacion==="btCuentaCajaGeneral")    
                        {   $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Cuentas";
                            $jsonModal['Carga'] = $this->RenderPlanCuentas->CreaModalGridPlanCuentas($Operacion,$Datos);
                            $jsonModal['Ancho'] = "750";
                        }else
                        {   $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                            $jsonModal['Modal'] = "Modal".strtoupper($Operacion); 
                            $jsonModal['Title'] = "Busca Sucursal";
                            $jsonModal['Carga'] = $this->RenderSucursal->CreaModalGridSucursal($Operacion,$Datos);
                            $jsonModal['Ancho'] = "605";
                        }   
                        $ajaxResp->call("CreaModal", json_encode($jsonModal));
                        return $ajaxResp; 
                }
                                
                function ConsultaModalGridCajaGeneralByTX($Operacion,$Texto = "")
                {       $ajaxResp = new xajaxResponse();    
                        $texto = trim($Texto);
                        
                        if ($Operacion==="btCuentaCajaGeneral")    
                        {   $prepareDQL = array('limite' => 50,'inicio' => 0,'movimiento' => strtoupper($texto));
                            $Datos = $this->ServicioPlanCuentas->BuscarPlanCuentasdeMovimiento($prepareDQL);
                        }else{
                            $prepareDQL = array('limite' => 50,'inicio' => 0,'texto' => strtoupper($texto));
                            $Datos = $this->ServicioSucursal->BuscarSucursalByDescripcion($prepareDQL);
                        }
                        return $this->RenderCajaGeneral->MuestraModalGridCajaGeneral($ajaxResp,$Operacion,$Datos);
                           
                }
         }

?>

