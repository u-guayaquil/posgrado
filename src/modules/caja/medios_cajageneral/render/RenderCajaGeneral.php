<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");
        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
        require_once('src/libs/clases/SearchInput.php');
         
        class RenderCajaGeneral
        {       private $Sufijo;
                private $SearchGrid;
                private $Maxlen;
                private $Fechas;
              
                function __construct($Sufijo = "")
                {       $this->Sufijo = $Sufijo;
                        $this->SearchGrid = new SearchGrid($this->Sufijo);
                        $this->Fechas = new DateControl();
                        $this->Maxlen['id'] = 2; 
                }
               
                function CreaCajaGeneralMantenimiento()
                {       $Search = new SearchInput($this->Sufijo);
              
                        $CajaGeneral = '<table class="Form-Frame" cellpadding="0" border="0">';
                        $CajaGeneral.= '       <tr height="30">';
                        $CajaGeneral.= '           <td class="Form-Label" style="width:17%">Id</td>';
                        $CajaGeneral.= '           <td class="Form-Label" style="width:44%">';
                        $CajaGeneral.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:txCuentaCajaGeneral:idCuentaCajaGeneral:txSucursal:idSucursal\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                        $CajaGeneral.= '           </td>';
                        $CajaGeneral.= '           <td class="Form-Label" style="width:14%"></td>';
                        $CajaGeneral.= '           <td class="Form-Label" style="width:25%"></td>';
                        $CajaGeneral.= '       </tr>';
                        $CajaGeneral.= '       <tr height="30">';
                        $CajaGeneral.= '           <td class="Form-Label">Descripci&oacute;n</td>';
                        $CajaGeneral.= '           <td class="Form-Label">';
                        $CajaGeneral.= '               <input type="text" class="txt-upper t08" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="30" disabled/>';
                        $CajaGeneral.= '           </td>';
                        $CajaGeneral.= '           <td class="Form-Label">Sucursal</td>';
                        $CajaGeneral.= '           <td class="Form-Label">';
                        $CajaGeneral.=             $Search->TextSch("Sucursal","","")->Enabled(false)->Create("t04");
                        $CajaGeneral.= '           </td>';
                        $CajaGeneral.= '       </tr>';
                        $CajaGeneral.= '       <tr height="30">';
                        $CajaGeneral.= '           <td class="Form-Label">Cuenta</td>';
                        $CajaGeneral.= '           <td class="Form-Label">';
                        $CajaGeneral.=             $Search->TextSch("CuentaCajaGeneral","","")->Enabled(false)->Create("t07");
                        $CajaGeneral.= '           </td>';
                        $CajaGeneral.= '           <td class="Form-Label">Estado</td>';
                        $CajaGeneral.= '           <td class="Form-Label">';
                        $CajaGeneral.=             $this->CreaComboEstado();
                        $CajaGeneral.= '           </td>';
                        $CajaGeneral.= '       </tr>';
                        $CajaGeneral.= '</table>';
                        return $CajaGeneral;
                }
              
                private function SearchGridValues()
                {       $Columns['Id']       = array('30px','center','');
                        $Columns['descripci&oacute;n'] = array('162px','left','');
                        $Columns['Sucursal'] = array('120px','left','');
                        $Columns['Creaci&oacute;n']    = array('115px','center','');
                        $Columns['Estado']   = array('70px','left','');
                        $Columns['idsucursal']    = array('0px','left','none');
                        $Columns['idctacontable'] = array('0px','left','none');
                        $Columns['cuenta'] = array('0px','left','none');
                        $Columns['estado'] = array('0px','left','none');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '150px','AltoRow' => '20px');
                }

                function CreaCajaGeneralSearchGrid($Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->CajaGeneralGridHTML($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($ObjHTML);
                }        

                private function CajaGeneralGridHTML($ObjSchGrid,$Datos)
                {       foreach ($Datos as $CajaGeneral)
                        {       $fecreacion = $this->Fechas->changeFormatDate($CajaGeneral['fecreacion'],"DMY");
                                $id = str_pad($CajaGeneral['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($CajaGeneral['descripcion']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($CajaGeneral['sucursal']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($CajaGeneral['txtestado']));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$CajaGeneral['idsucursal']);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'', strtoupper(trim($CajaGeneral['cuentacontable'])));
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$CajaGeneral['idctacontable']);
                                $ObjSchGrid->CreaSearchCellsDetalle($id,'',$CajaGeneral['idestado']);
                                $ObjSchGrid->CreaSearchRowsDetalle ($id,($CajaGeneral['idestado']==0 ? "red" : ""));
                        }
                        return $ObjSchGrid->CreaSearchTableDetalle();
                }
              
                private function CreaComboEstado()
                {       $Select = new ComboBox($this->Sufijo);                        
                        $ServicioEstado = new ServicioEstado();
                        $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                        return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
                }
              
                function MuestraCajaGeneral($Ajax,$Datos)
                {       foreach ($Datos as $datos) 
                        {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                            $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($datos['descripcion']));
                            $Ajax->Assign("txSucursal".$this->Sufijo,"value", trim($datos['sucursal']));
                            $Ajax->Assign("idSucursal".$this->Sufijo,"value", $datos['idsucursal']);
                            $Ajax->Assign("txCuentaCajaGeneral".$this->Sufijo,"value", strtoupper(trim($datos['cuentacontable'])));
                            $Ajax->Assign("idCuentaCajaGeneral".$this->Sufijo,"value", $datos['idctacontable']);
                            $Ajax->Assign("estado".$this->Sufijo,"value", $datos['idestado']);
                            $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                            return $Ajax;
                        }
                        return $this->RespuestaCajaGeneral($Ajax,"NOEXISTEID","No existe una Caja con ese ID.");
                }
                
                function MuestraCajaGeneralGrid($Ajax,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                        $ObjHTML = $this->CajaGeneralGridHTML($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                        return $Ajax;
                }
               
                function MuestraCajaGeneralGuardada($Ajax,$CajaGeneral)
                {       foreach ($CajaGeneral as $datos) 
                        {   $id = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraCajaGeneralGrid($Ajax,$CajaGeneral);
                            return $this->RespuestaCajaGeneral($xAjax,"GUARDADO",$id);
                        }
                        return $this->RespuestaCajaGeneral($Ajax,"EXCEPCION","No se guardó la información.");
                }

                function MuestraCajaGeneralEditada($Ajax,$CajaGeneral)
                {       foreach ($CajaGeneral as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraCajaGeneralRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaCajaGeneral($xAjax,"GUARDADO",$datos['id']);
                        }    
                        return $this->RespuestaCajaGeneral($Ajax,"EXCEPCION","No se actualizó la información.");            
                }
              
                function MuestraCajaGeneralEliminada($Ajax,$CajaGeneral)
                {       foreach ($CajaGeneral as $datos) 
                        {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $xAjax = $this->MuestraCajaGeneralRowGrid($Ajax, $datos,$datos['idestado']);
                                return $this->RespuestaCajaGeneral($xAjax,"ELIMINADO",$datos['idestado']);
                        }  
                        return $this->RespuestaCajaGeneral($xAjax,"EXCEPCION","No se eliminó la información.");
                }
              
                function MuestraCajaGeneralRowGrid($Ajax,$CajaGeneral,$estado=1)
                {       $Fila = $this->SearchGrid->GetRow($CajaGeneral['id'],$estado);
                        $fecreacion = $this->Fechas->changeFormatDate($CajaGeneral['fecreacion'],"DMY");
                        $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($CajaGeneral['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($CajaGeneral['descripcion']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($CajaGeneral['sucursal']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",$fecreacion[1]." ".$fecreacion[2]);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($CajaGeneral['txtestado']));
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",$CajaGeneral['idsucursal']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML", strtoupper(trim($CajaGeneral['cuentacontable']))); 
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",$CajaGeneral['idctacontable']);
                        $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",$CajaGeneral['idestado']);
                        return $Ajax;
                }

                function MuestraCajaGeneralExcepcion($Ajax,$Msg)
                {       return $this->RespuestaCajaGeneral($Ajax,"EXCEPCION",$Msg);    
                }

                private function RespuestaCajaGeneral($Ajax,$Tipo,$Data)
                {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                        $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                        return $Ajax;
                }
                
              /*** MODAL ***/
                private function SearchGridModalConfig()
                {       $Columns['Id']     = array('40px','center','');
                        $Columns['Codigo']  = array('45px','center','');
                        $Columns['Descripcion'] = array('200px','left','');
                        return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '180px','AltoRow' => '20px');
                }

                function CreaModalGridCajaGeneral($Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalCajaGeneral($SearchGrid,$Datos);
                        return $SearchGrid->CreaSearchGrid($GrdDataHTML);
                }
                
                function MuestraModalGridCajaGeneral($Ajax,$Operacion,$Datos)
                {       $SearchGrid = new SearchGrid($this->Sufijo,$Operacion);
                        $SearchGrid->ConfiguraSearchGrid($this->SearchGridModalConfig());
                        $GrdDataHTML = $this->GridDataHTMLModalCajaGeneral($SearchGrid,$Datos);
                        $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $GrdDataHTML);
                        return $Ajax;
                }        

                private function GridDataHTMLModalCajaGeneral($Grid,$Datos)
                {       foreach ($Datos as $Opcion)
                        {       $id = str_pad($Opcion['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                                $Grid->CreaSearchCellsDetalle($id,'',$id);
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['codigo']));
                                $Grid->CreaSearchCellsDetalle($id,'',trim($Opcion['descripcion']));
                                $Grid->CreaSearchRowsDetalle ($id,"black");
                        }
                        return $Grid->CreaSearchTableDetalle();
                }
        }
?>

