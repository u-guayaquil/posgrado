<?php
        $Sufijo = '_cajageneral';

        require_once('src/modules/caja/medios_cajageneral/controlador/ControlCajaGeneral.php');
        $ControlCajaGeneral = new ControlCajaGeneral($Sufijo);

        $xajax->register(XAJAX_FUNCTION,array('Guarda'.$Sufijo, $ControlCajaGeneral,'GuardaCajaGeneral'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByID'.$Sufijo, $ControlCajaGeneral,'MuestraCajaGeneralByID'));
        $xajax->register(XAJAX_FUNCTION,array('MuestraByTX'.$Sufijo, $ControlCajaGeneral,'MuestraCajaGeneralByTX'));
        $xajax->register(XAJAX_FUNCTION,array('Elimina'.$Sufijo, $ControlCajaGeneral,'EliminaCajaGeneral'));
        $xajax->register(XAJAX_FUNCTION,array('CargaModal'.$Sufijo, $ControlCajaGeneral,'CargaModalGridCajaGeneral'));
        $xajax->register(XAJAX_FUNCTION,array('BuscaModalByTX'.$Sufijo, $ControlCajaGeneral,'ConsultaModalGridCajaGeneralByTX'));
         
        $xajax->processRequest();

?>
<!doctype html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <?php   $xajax->printJavascript(); 
                require_once('src/utils/links.php'); 
        ?>
        <script type="text/javascript" src="src/modules/caja/medios_cajageneral/js/JSCajaGeneral.js"></script>
</head>
<body>        
        <div class="FormBasic" style="width:550px">
            <div class="FormSectionMenu">              
            <?php   $ControlCajaGeneral->CargaCajaGeneralBarButton($_GET['opcion']);
                    echo '<script type="text/javascript">';
                    echo '    BarButtonState(\''.$Sufijo.'\',"Default"); ';
                    echo '</script>';
            ?>
            </div>    
            <div class="FormSectionData">              
                <form id="<?php echo 'Form'.$Sufijo; ?>">
                <?php  $ControlCajaGeneral->CargaCajaGeneralMantenimiento();  ?>
                </form>
            </div>    
            <div class="FormSectionGrid"> 
            <?php echo $ControlCajaGeneral->CargaCajaGeneralSearchGrid();  ?>
            </div>  
        </div> 
</body>
</html>  
              

