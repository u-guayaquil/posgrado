<?php    
        require_once("src/rules/general/servicio/ServicioEstado.php");  
        require_once("src/rules/caja/servicio/ServicioValidacion.php");          
        require_once("src/rules/caja/servicio/ServicioTipo.php");

        require_once('src/libs/clases/ComboBox.php');
        require_once("src/libs/clases/BarButton.php");
        require_once("src/libs/clases/SearchGrid.php");
         
        class RenderDocumento
        {   private $Sufijo;
            private $SearchGrid;
            private $Maxlen;
            private $Fechas;
              
            function __construct($Sufijo = "")
            {       $this->Sufijo = $Sufijo;  
                    $this->SearchGrid = new SearchGrid($this->Sufijo);
                    $this->Fechas = new DateControl();
                    $this->Maxlen['id'] = 2; 
            }
              
            function CreaOpcionBarButton($Buttons)
            {       $BarButton = new BarButton($this->Sufijo);
                    return $BarButton->CreaBarButton($Buttons);
            }
              
            function CreaDocumentoMantenimiento()
            {                               
                    $Documento = '<table class="Form-Frame" cellpadding="0" border="0">';
                    $Documento.= '       <tr height="30">';
                    $Documento.= '           <td class="Form-Label" style="width:18%">Id</td>';
                    $Documento.= '           <td class="Form-Label" style="width:32%">';
                    $Documento.= '               <input type="text" class="txt-input t01" id="id'.$this->Sufijo.'" name="id'.$this->Sufijo.'" value="" maxlength="'.$this->Maxlen['id'].'" onKeyDown="return Dependencia(event,\''.$this->Sufijo.'\',\'descripcion:validacion:tipo\',\'NUM\'); " onBlur="return SearchByElement'.$this->Sufijo.'(\''.$this->Sufijo.'\',this);"/>';
                    $Documento.= '           </td>';
                    $Documento.= '           <td class="Form-Label" style="width:18%"></td>';
                    $Documento.= '           <td class="Form-Label" style="width:32%"></td>';
                    $Documento.= '       </tr>';
                    $Documento.= '       <tr height="30">';
                    $Documento.= '           <td class="Form-Label">Descripci&oacute;n</td>';
                    $Documento.= '           <td class="Form-Label">';
                    $Documento.= '                <input type="text" class="txt-upper t06" id="descripcion'.$this->Sufijo.'" name="descripcion'.$this->Sufijo.'" value="" maxlength="20" disabled/>';
                    $Documento.= '           </td>';
                    $Documento.= '           <td class="Form-Label">Validaci&oacute;n</td>';
                    $Documento.= '           <td class="Form-Label" colspan="3">';
                    $Documento.=                 $this->CreaComboValidaciones();
                    $Documento.= '           </td>';
                    $Documento.= '       </tr>';
                    $Documento.= '       <tr height="30">';
                    $Documento.= '           <td class="Form-Label">Tipo</td>';
                    $Documento.= '           <td class="Form-Label">';
                    $Documento.=                 $this->CreaComboTipos();
                    $Documento.= '           </td>';
                    $Documento.= '           <td class="Form-Label">Estado</td>';
                    $Documento.= '           <td class="Form-Label">';
                    $Documento.=                 $this->CreaComboEstado();
                    $Documento.= '           </td>';
                    $Documento.= '       </tr>';
                    $Documento.= '</table>';
                    return $Documento;
            }
              
            private function SearchGridValues()
            {       $Columns['Id']                 = array('30px','center','');
                    $Columns['Descripci&oacute;n'] = array('160px','left','');
                    $Columns['Validacion']  = array('130px','left','');
                    $Columns['Tipo']        = array('90px','left','');
                    $Columns['Creacion']    = array('0px','center','none');
                    $Columns['Estado']      = array('70px','left',''); 
                    $Columns['idEstado']    = array('0px','left','none');
                    $Columns['idValidacion']= array('0px','left','none');                        
                    $Columns['idTipo']      = array('0px','left','none');
                    return array('AltoCab' => '25px','DatoCab' => $Columns,'AltoDet' => '200px','AltoRow' => '20px');
            }
               
            function CreaDocumentoSearchGrid($Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->DocumentoGridHTML($SearchGrid,$Datos);
                    return $SearchGrid->CreaSearchGrid($ObjHTML);
            }
              
            function MuestraDocumentoGrid($Ajax,$Datos)
            {       $SearchGrid = new SearchGrid($this->Sufijo);
                    $SearchGrid->ConfiguraSearchGrid($this->SearchGridValues());
                    $ObjHTML = $this->DocumentoGridHTML($SearchGrid,$Datos);
                    $Ajax->Assign($SearchGrid->CapaDataGrilla(),"innerHTML", $ObjHTML);
                    return $Ajax;
            }        

            private function DocumentoGridHTML($ObjSchGrid,$Datos)
            {       foreach ($Datos as $documento)
                    {       $fecreacion = $this->Fechas->changeFormatDate($documento['fecreacion'],"DMY");
                            $id = str_pad($documento['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$id);                              
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($documento['descripcion']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($documento['validacion']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',trim($documento['tipo']));
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$fecreacion[1]." ".$fecreacion[2]);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['txtestado']); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['idestado']);
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['idcajaval']); 
                            $ObjSchGrid->CreaSearchCellsDetalle($id,'',$documento['idcajatipo']);
                            $ObjSchGrid->CreaSearchRowsDetalle ($id,($documento['idestado']==0 ? "red" : ""));
                    }
                    return $ObjSchGrid->CreaSearchTableDetalle(); 
            }

            private function CreaComboEstado()
            {       $Select = new ComboBox($this->Sufijo);                        
            
                    $ServicioEstado = new ServicioEstado();
                    $datosEstado = $ServicioEstado->BuscarEstadoByArrayID(array(0,1));
                    return $Select->Combo("estado",$datosEstado)->Selected(1)->Create("s04");
            }
              
            private function CreaComboTipos()
            {       $Select = new ComboBox($this->Sufijo);                        

                    $ServicioTipos = new ServicioTipo();
                    $datosTipos = $ServicioTipos->BuscarTipoTodos();
                    return $Select->Combo("tipo",$datosTipos)->Selected(1)->Create("s06");
            }
              
            private function CreaComboValidaciones()
            {       $Select = new ComboBox($this->Sufijo);                        

                    $ServicioValidaciones = new ServicioValidacion();
                    $datosValidaciones = $ServicioValidaciones->BuscarValidacionTodos();
                    return $Select->Combo("validacion",$datosValidaciones)->Selected(1)->Create("s06");
            }
              
            function MuestraDocumento($Ajax,$Datos)
            {       foreach ($Datos as $dato) 
                    {   $Ajax->Assign("id".$this->Sufijo,"value", str_pad($dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                        $Ajax->Assign("descripcion".$this->Sufijo,"value", trim($dato['descripcion']));                     
                        $Ajax->Assign("validacion".$this->Sufijo,"value", $dato['idcajaval']);                     
                        $Ajax->Assign("tipo".$this->Sufijo,"value", $dato['idcajatipo']);
                        $Ajax->Assign("estado".$this->Sufijo,"value", $dato['idestado']);
                        $Ajax->call("BarButtonState",$this->Sufijo,"Active");
                        return $Ajax;                       
                    }
                    return $this->RespuestaDocumento($Ajax,"NOEXISTEID","No existe un Documento con el ID ingresado.");
            }
              
            function MuestraDocumentoGuardado($Ajax,$Documento)
            {       foreach ($Documento as $dato) 
                    {   $id = str_pad($dato['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                        $xAjax = $this->MuestraDocumentoGrid($Ajax,$Documento);
                        return $this->RespuestaDocumento($xAjax,"GUARDADO",$id);
                    }
                    return $this->RespuestaDocumento($Ajax,"EXCEPCION","No se guardó la información.");
            }

            function MuestraDocumentoEditado($Ajax,$Documento)
            {       foreach ($Documento as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraDocumentoRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaDocumento($xAjax,"GUARDADO",$datos['id']);
                    }    
                    return $this->RespuestaDocumento($Ajax,"EXCEPCION","No se actualizó la información.");            
            }
              
            function MuestraDocumentoEliminado($Ajax,$Documento)
            {       foreach ($Documento as $datos) 
                    {       $datos['id'] = str_pad($datos['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT);
                            $xAjax = $this->MuestraDocumentoRowGrid($Ajax, $datos,$datos['idestado']);
                            return $this->RespuestaDocumento($xAjax,"ELIMINADO",$datos['idestado']);
                    }  
                    return $this->RespuestaDocumento($xAjax,"EXCEPCION","No se eliminó la información.");
            }
              
            function MuestraDocumentoRowGrid($Ajax,$Documento,$estado=1)
            {       $Fila = $this->SearchGrid->GetRow($Documento['id'],$estado);
                    $fecreacion = $this->Fechas->changeFormatDate($Medio['fecreacion'],"DMY");
                    $Ajax->Assign($Fila['Name'],"style",$Fila['Color']);
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],0),"innerHTML",str_pad($Documento['id'],$this->Maxlen['id'],'0',STR_PAD_LEFT));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],1),"innerHTML",trim($Documento['descripcion']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],2),"innerHTML",trim($Documento['validacion']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],3),"innerHTML",trim($Documento['tipo']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],4),"innerHTML",trim($fecreacion[1]." ".$fecreacion[2]));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],5),"innerHTML",trim($Documento['txtestado']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],6),"innerHTML",trim($Documento['idestado'])); 
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],7),"innerHTML",trim($Documento['idcajaval']));
                    $Ajax->Assign($this->SearchGrid->GetCell($Fila['Id'],8),"innerHTML",trim($Documento['idcajatipo']));
                    return $Ajax;
            }
            
            function MuestraDocumentoExcepcion($Ajax,$Msg)
            {       return $this->RespuestaDocumento($Ajax,"EXCEPCION",$Msg);    
            }

            private function RespuestaDocumento($Ajax,$Tipo,$Data)
            {       $jsFuncion = "XAJAXResponse".$this->Sufijo;
                    $Ajax->call($jsFuncion,$this->Sufijo,$Tipo,$Data);
                    return $Ajax;
            }
        }
?>

