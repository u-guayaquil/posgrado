<?php
        require_once("src/rules/caja/servicio/ServicioDocumento.php");
        require_once("src/rules/sistema/servicio/ServicioPerfilOpcion.php");
        require_once("src/modules/caja/documentos/render/RenderDocumento.php");

         class ControlDocumento
         {     private  $Sufijo;
               private  $ServicioDocumento;
               private  $RenderDocumento;

               function __construct($Sufijo = "")
               {        $this->Sufijo = $Sufijo;
                        $this->ServicioDocumento = new ServicioDocumento();
                        $this->RenderDocumento = new RenderDocumento($Sufijo);
               }
               
               function CargaDocumentoBarButton($Opcion)
                {       $BarButton = new BarButton($this->Sufijo);
                        $ServicioPerfilOpcion = new ServicioPerfilOpcion();
                        $Buttons = $ServicioPerfilOpcion->ObtenerPermisosPorOpcion($Opcion);
                        echo $BarButton->CreaBarButton($Buttons);
                }
               
               function CargaDocumentoMantenimiento()
               {        echo $this->RenderDocumento->CreaDocumentoMantenimiento();
               }

               function CargaDocumentoSearchGrid()
               {        $prepareDQL = array('limite' => 50, 'inicio' => 0, 'texto' => '', 'orden'=>'t.descripcion');
                        $datoDocumento = $this->ServicioDocumento->BuscarDocumentoByDescripcion($prepareDQL);
                        echo $this->RenderDocumento->CreaDocumentoSearchGrid(($datoDocumento)); 
               }         
               
               function MuestraDocumentoByID($id)
               {        $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('id' => intval($id));
                        $datoDocumento = $this->ServicioDocumento->BuscarDocumentoByID($prepareDQL);
                        return $this->RenderDocumento->MuestraDocumento($ajaxRespon,$datoDocumento);
               }

               function MuestraDocumentoByTX($texto)
               {        $ajaxRespon = new xajaxResponse();                   
                        $prepareDQL = array('limite' => 50, 'inicio' => 0, 'texto' => strtoupper($texto), 'orden'=>'t.descripcion');
                        $datoDocumento = $this->ServicioDocumento->BuscarDocumentoByDescripcion($prepareDQL);
                        return $this->RenderDocumento->MuestraDocumentoGrid($ajaxRespon,$datoDocumento);
               }

               function GuardaDocumento($Form)
               {        $ajaxRespon = new xajaxResponse();
                        $Documento  = json_decode($Form)->Forma;
                        
                        $id = $this->ServicioDocumento->GuardaDBDocumento($Documento);
                        if (is_numeric($id)){
                            $function = (empty($Documento->id) ? "MuestraDocumentoGuardado" : "MuestraDocumentoEditado");
                            $prepareDQL = array('limite' => 50,'id' => intval($id));
                            $Datos = $this->ServicioDocumento->BuscarDocumentoByID($prepareDQL);
                            return $this->RenderDocumento->{$function}($ajaxRespon,$Datos);
                        }else{    
                            return $this->RenderDocumento->MuestraDocumentoExcepcion($ajaxRespon,intval($id));
                        }                      
               }
               
                function EliminaDocumento($id)
                {       $ajaxRespon = new xajaxResponse();
                        $prepareDQL = array('limite' => 50,'id' => intval($id));
                        $this->ServicioDocumento->DesactivaDocumento(intval($id));
                        $Datos = $this->ServicioDocumento->BuscarDocumentoByDescripcion($prepareDQL);
                        return $this->RenderDocumento->MuestraDocumentoEliminado($ajaxRespon,$Datos);
                }
              
        }

?>

