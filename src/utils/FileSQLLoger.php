<?php

use Doctrine\DBAL\Logging\SQLLogger;

class FileSQLLogger implements SQLLogger {
    public function startQuery($sql, array $parametros = null, array $tipos = null){
        $archivoLog = fopen("logSqlsDoctrine.txt", "a");    
    
        fwrite($archivoLog , date("d/m/Y H:i:s") . PHP_EOL);
        fwrite($archivoLog , $sql . PHP_EOL);
        
        if($parametros){
            fwrite($archivoLog , "Parametros:" . PHP_EOL . print_r($parametros, true) . PHP_EOL);
        }
        
        if($tipos){
            fwrite($archivoLog , "Tipos:" . PHP_EOL . print_r($tipos, true) . PHP_EOL);
        }
         
        fclose($archivoLog);
    }

    public function stopQuery(){
        
    }
}