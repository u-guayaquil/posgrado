  
            <link rel="stylesheet" type="text/css" href="src/libs/css/loadfile.css" />
            <link rel="stylesheet" type="text/css" href="src/libs/css/body.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/css/buttonbar.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/css/buttons.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/css/searchgrid.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/css/searchInput.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/css/forms.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/css/modal.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/css/elements.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/plugins/calendar/css/dcalendar.picker.css"/>
            <link rel="stylesheet" type="text/css" href="src/libs/plugins/sweetalert2/sweetalert2.css"/>
                
            <script type="text/javascript" src="src/libs/js/jquery.js"></script>
            <script type="text/javascript" src="src/libs/js/combo.js"></script>
            <script type="text/javascript" src="src/libs/js/ajax.js"></script>
            <script type="text/javascript" src="src/libs/js/buttonbar.js"></script>
            <script type="text/javascript" src="src/libs/js/modal.js"></script>
            <script type="text/javascript" src="src/libs/js/workgrid.js"></script>
            <script type="text/javascript" src="src/libs/js/elements.js"></script>
            <script type="text/javascript" src="src/libs/js/validaciones.js"></script>
            <script type="text/javascript" src="src/libs/js/alert.js"></script>
            <script type="text/javascript" src="src/libs/plugins/calendar/js/dcalendar.picker.js"></script>
            <script type="text/javascript" src="src/libs/plugins/sweetalert2/sweetalert2.all.js"></script>
            
            
            <script src="src/libs/js/jquery.maskedinput.js" type="text/javascript"></script>
            <script>            $.datetimepicker.setLocale('es');            </script>
            
              
  