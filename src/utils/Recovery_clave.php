<?php
error_reporting(E_ALL);
ini_set('display_errors','On');

    require_once("src/rules/seguridad/servicio/ServicioActivacionClave.php");
    require_once("src/libs/clases/Session.php");
        if ( isset( $_SESSION[ 'ULTIMA_ACTIVIDAD' ] ) &&
        ( time() - $_SESSION[ 'ULTIMA_ACTIVIDAD' ] > MAX_SESSION_TIEMPO ) ) {
        destruir_session();
        }

        $_SESSION[ 'ULTIMA_ACTIVIDAD' ] = time();

    class Recovery_Clave
    {
            private $ServicioActivacionClave;
            private $DateControl;

            function __construct()
            {   $this->ServicioActivacionClave = new ServicioActivacionClave();
                $this->DateControl = new DateControl();
            }

            function HaCaducadoToken($caducidad)
            {       if ($caducidad != "1900-01-01")
                        {   return (strtotime($this->DateControl->getNowDateTime('DATETIME')) < strtotime($caducidad));
                        }
                        return false;
            }
    }

    $mensaje = '';

    if(isset($_GET['token'])) {
        $token = $_GET['token'];
        $ServicioActivacionClave = new ServicioActivacionClave();
        $ExisteToken = $ServicioActivacionClave->ExisteToken($token);
        if($ExisteToken[0])
        {   $Datos = $ExisteToken[1];
            $Estado = $Datos[0]['IDESTADO'];
            if($Estado == 1)
            {   $caducidad = $Datos[0]['FECADUCIDAD']->format('Y-m-d H:i:s');
                $Recovery_Clave = new Recovery_Clave();
                $FechaCaducidad = $Recovery_Clave->HaCaducadoToken($caducidad);
                if($FechaCaducidad)
                {   $IDUSUARIO = $Datos[0]['IDUSUARIO'];
                    $NUEVACLAVE = $Datos[0]['CLAVE'];
                    $ServicioActivacionClave->UpdateClaveUsuario($IDUSUARIO,$NUEVACLAVE);
                    $ID = $Datos[0]['ID'];
                    $ServicioActivacionClave->TokenEstadoProcesado($ID);
                    $mensaje = 'La nueva clave a sido activada. Ingrese al sistema de posgrado con la nueva clave por favor.';
                }
                else
                {   $ID = $Datos[0]['ID'];
                    $ServicioActivacionClave->TokenEstadoCaducado($ID);
                    $mensaje = 'El enlace ha caducado. Por favor solicite una nueva clave.';
                }
            }
            else if($Estado == 9)
            {   $mensaje = 'La clave ya se activo anteriormente.';    }
            else if($Estado == 8)
            {   $mensaje = 'El enlace ha caducado. Por favor solicite una nueva clave.';    }
        } else
        $mensaje = $ExisteToken[1];
    }
 ?>


<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Enlace de Activación</title>

        <link rel="apple-touch-icon" sizes="144x144" href="src/public/img/logo_ug_small.png">
        <link rel="shortcut icon" href="src/public/img/favicon.png">
        <meta name="theme-color" content="#3063A0">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600">
        <link rel="stylesheet" href="vendor/fontawesome/css/all.css">
        <link rel="stylesheet" href="src/libs/css/theme.min.css" data-skin="default">
        <link rel="stylesheet" href="src/libs/css/custom.css">
        <link rel="stylesheet" type="text/css" href="src/libs/css/modal.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="src/libs/js/elements.js"></script>
        <script type="text/javascript" src="src/libs/js/modal.js"></script>
        <?php   $xajax->printJavascript(); ?>

    </head>
    <body>
        <main class="auth">
            <form class="auth-form auth-form-reflow" id="Recovery" method="POST">
              <div class="text-center mb-4">
                <div class="mb-4">
                  <img class="rounded" src="src/public/img/logo_ug_small.png" alt="" height="150">
                </div>
                <h1 class="h3"> Activación de Contraseña </h1>
              </div>
              <div class="loader" id="loading"></div>
              <p class="mb-0 px-3 text-center">
                  <?php
                    echo $mensaje;
                  ?>
              </p>
              <br/>
              <div class=" form-group mb-4">
                <a href="CMD_LOGIN" class="btn btn-lg btn-primary btn-block">Volver al Inicio</a>
              </div>
              <p class="mb-0 px-3 text-muted text-center">© 2019 Todos los Derechos Reservados Universidad de Guayaquil.</p>
            </form>
            <!--<footer class="auth-footer mt-5">© 2019 Todos los Derechos Reservados Universidad de Guayaquil.</footer>-->
        </main>
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/popper.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/particles.js/particles.min.js"></script>
        <script src="src/libs/js/theme.min.js"></script>
    </body>
</html>