<?php
    
    if (isset($_GET['type']) && isset($_GET['path']))
    {   
        require_once("src/libs/clases/File.php");
        $FileManager = new File();                
        
        $Destinity = $Utils->Desencriptar($_GET['path']);
        $TextoExtn = $Utils->Desencriptar($_GET['type']);
      //Extensiones validas para upload
        $ArrayExtn = explode(",", str_replace(array("[","]"), "",$TextoExtn)); 
        
        $ClassName = "";
        if (isset($_GET['clss'])) 
        {   $ClassPath = $Utils->Desencriptar($_GET['clss']);
            require_once($ClassPath);    
            $ClassName = $FileManager->getFileParts($ClassPath)[2];
        }
        
        $ObjSufijo = "";
        if (isset($_GET['suff'])) 
        {   $ObjSufijo = $Utils->Desencriptar($_GET['suff']);
        }    
        
        if (!empty($_FILES))
        {   $ExistingFile = false;
            foreach ($_FILES as $uploadedFile=>$dataFile)
            {       for ($i=0;$i<count($dataFile["name"]);$i++)
                    {    if ($dataFile["name"][$i]!="") { $ExistingFile = true; }
                    }
            }
            
            if ($ExistingFile)
            {   $FilesUploaded = $FileManager->UploadFiles($_FILES,$ArrayExtn,$Destinity);
                if ($FilesUploaded[0])
                {   switch ($ClassName)
                    {       case "ServicioPedido":
                                $ServicioPedido = new $ClassName();
                                echo $ServicioPedido->LeeExcelPedido($_SESSION['IDUSER'],$Destinity.'/'.$FilesUploaded[1][0]);
                            break;
                            case "RenderEmpleado":
                                $RenderEmpleado = new $ClassName($ObjSufijo);
                                echo $RenderEmpleado->RutaFirma($Destinity.'/'.$FilesUploaded[1][0]);                                
                            break;
                            case "RenderEmpleadoCargas":
                                $RenderEmpleadoCargas = new $ClassName($ObjSufijo);
                                echo $RenderEmpleadoCargas->RutaDocumento($Destinity.'/'.$FilesUploaded[1][0]);                                
                            break;
                            case "ControlPlanCuentas":
                                $ControlPlanCuentas = new $ClassName($ObjSufijo);
                                echo $ControlPlanCuentas->CargaFilePlanCuenta($Destinity.'/'.$FilesUploaded[1][0]);                                
                            break;
                    }
                }
                else{   
                    echo $FilesUploaded[1];
                }
            }
        }
    }
    else{
        echo "No ha iniciado sesiÃ³n correctamente.";        
    }

?>


