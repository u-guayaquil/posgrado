<?php

    class Templates
    {
        static function Bienvenida($empresa,$ctauser,$pssword)
        {       if ($empresa)
                {   $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                                <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                                <tr>
                                        <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                            <b>Bienvenido a ".trim($empresa->getRazonsocial())."</b>
                                        </td>
                                </tr>
                                <tr>
                                        <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                        Para que usted pueda acceder a los servicios e información que <b>".trim($empresa->getRazonsocial())."</b>
                                        pone a disposición de sus usuarios, le solicitamos completar el proceso de registro
                                        <b>haciendo click en el link</b> que se encuentra en la parte inferior de este correo.
                                        <br><br>
                                        Tiene un plazo de 24 horas para completar esta tarea, posterior a esto, su solicitud
                                        caducará y deberá realizar una nueva.
                                        <br><br>
                                        Por favor no responda a esta dirección de correo. Cualquier inquietud le pedimos
                                        comunicarse con nuestro <b>Call Center</b>
                                        </td>
                                </tr>
                                <tr>
                                        <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                            <br>
                                            <b>Usuario: </b>".$ctauser."<br>
                                            <b>Password: </b>".$pssword."<br>
                                        </td>
                                </tr>
                                <tr>
                                        <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                            <br><b>Saludos</b>
                                            <br>Dpto de Sistemas
                                        </td>
                                </tr>
                                <tr>
                                        <td style='width:53px; text-align:justify; font-family:arial; font-size:11pt; border:0'>
                                            <img src='cid:mail_logo' border='0'>
                                        </td>
                                        <td style='text-align:justify; font-family:arial; font-size:8pt; border:0'>
                                	    <b style='color: #003C6F'>PBX:</b> ".trim($empresa->getSucursales()->getTelefono())."
                                            <br>
 	                                    <b style='color: #003C6F'>Dirección:</b> ".trim($empresa->getSucursales()->getDireccion())."
                                            <br>
                         	            <b style='color: #003C6F'>".ucwords(strtolower($empresa->getSucursales()->getCiudades()->getDescripcion()))." - ".ucwords(strtolower($empresa->getSucursales()->getCiudades()->getProvincia()->getPais()->getDescripcion()))."</b>
                                        </td>
                                </tr>
                                </table>";
                    return $mnj;
                }
                else{
                    return false;
                }
        }
    }    
    //<br><b style='color: #003C6F'> ".strtolower($ob[1]->website)."</b>
?>
