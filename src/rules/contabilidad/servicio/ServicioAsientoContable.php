<?php
            require_once("src/rules/contabilidad/dao/DAOAsientoContable.php");
            
            class ServicioAsientoContable
            {     private $DAOAsientoContable;
     
                  function __construct()
                  {        $this->DAOAsientoContable = new DAOAsientoContable();
                  }
                  
                  function GuardaDBAsientoContable($Form)
                  {       $fecha = explode('/', $Form->fecha_asientocontable);                 
                          $Form->fecha_asientocontable = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                          if (empty($Form->idAsiento_asientocontable)){
                            return $this->DAOAsientoContable->InsertaAsientoContable($Form);
                          }else{
                            $this->DAOAsientoContable->EliminaDetalle($Form->idAsiento_asientocontable);
                            return $this->DAOAsientoContable->ActualizaAsientoContable($Form);
                          }
                  }
                  
                  function DesactivaAsientoContable($Id)
                  {       return $this->DAOAsientoContable->DesactivaAsientoContable($Id);
                  }
           
                  function BuscarAsientoContable($prepareDQL)
                  {        return $this->DAOAsientoContable->BuscarAsientoContable($prepareDQL);
                  }
                  
                  function BuscarDetalleAsientoContable($prepareDQL)
                  {        return $this->DAOAsientoContable->BuscarDetalleAsientoContable($prepareDQL);
                  }
            }
?>

