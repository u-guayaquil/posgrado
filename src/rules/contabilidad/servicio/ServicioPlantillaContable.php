<?php
            require_once("src/rules/contabilidad/dao/DAOPlantillaContable.php");
            
            class ServicioPlantillaContable
            {     private $DAOPlantillaContable;
     
                  function __construct()
                  {        $this->DAOPlantillaContable = new DAOPlantillaContable();
                  }
                  
                  function GuardaDBPlantillaContable($Form)
                  {       if (($Form->idPlantilla_plantillacontable)==''){
                            return $this->DAOPlantillaContable->InsertaPlantillaContable($Form);
                          }else{
                            $this->DAOPlantillaContable->EliminaDetalle($Form->idPlantilla_plantillacontable);
                            return $this->DAOPlantillaContable->ActualizaPlantillaContable($Form);
                          }
                  }
                  
                  function DesactivaPlantillaContable($Id)
                  {       return $this->DAOPlantillaContable->DesactivaPlantillaContable($Id);
                  }
           
                  function BuscarPlantillaContable($prepareDQL)
                  {        return $this->DAOPlantillaContable->BuscarPlantillaContable($prepareDQL);
                  }
                  
                  function BuscarDetallePlantillaContable($prepareDQL)
                  {        return $this->DAOPlantillaContable->BuscarDetallePlantillaContable($prepareDQL);
                  }
            }
?>

