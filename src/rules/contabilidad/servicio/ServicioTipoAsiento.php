<?php
            require_once("src/rules/contabilidad/dao/DAOTipoAsiento.php");
            
            class ServicioTipoAsiento
            {     private $DAOTipoAsiento;
     
                  function __construct()
                  {        $this->DAOTipoAsiento = new DAOTipoAsiento();
                  }
                  
                  function GuardaDBTipoAsiento($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOTipoAsiento->InsertaTipoAsiento($Form);
                          }else{
                            return $this->DAOTipoAsiento->ActualizaTipoAsiento($Form);
                          }
                  }
                  
                  function DesactivaTipoAsiento($Id)
                  {       return $this->DAOTipoAsiento->DesactivaTipoAsiento($Id);
                  }
           
                  function BuscarTipoAsiento($prepareDQL)
                  {        return $this->DAOTipoAsiento->BuscarTipoAsiento($prepareDQL);
                  }
                  
                  

            }
?>

