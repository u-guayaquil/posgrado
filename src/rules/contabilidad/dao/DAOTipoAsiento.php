<?php
        require_once("src/base/DBDAO.php");

        class DAOTipoAsiento extends DBDAO
        {       private static $entityTipoAsiento;
                private $Funciones;
                private $Fecha;
              
                public function __construct()
                {       self::$entityTipoAsiento = DBDAO::getConnection('PgProvider');
                        $this->Funciones = self::$entityTipoAsiento->MyFunction();
                        $this->Fecha  = new DateControl();
                }

                function BuscarTipoAsiento($PrepareDQL)
                {       $JOIN = array_key_exists("id", $PrepareDQL) ? " and t.id =".$PrepareDQL['id']: "";
                        $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and t.idestado in (".$PrepareDQL['estado'].")" : "";
                        $JOIN.= array_key_exists("texto", $PrepareDQL) ? " and upper(t.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                        $LIMT = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                        
                        $DSQL = "SELECT t.id, t.descripcion,t.codigo, t.fecreacion, e.id as idestado, e.descripcion as txtestado ";
                        $DSQL.= "FROM contable.tipo_asiento t, general.Estado e ";
                        $DSQL.= "WHERE t.idestado=e.id ".$JOIN;
                        $DSQL.= "ORDER BY t.descripcion ".$LIMT;
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
              
                public function InsertaTipoAsiento($datos)
                {       $descripcion = strtoupper(trim($datos->descripcion));
                        $codigo = strtoupper(trim($datos->codigo));
                        $estado = $datos->estado;
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $usuars = $_SESSION['IDUSER'];
                        
                        $Query = "INSERT INTO contable.tipo_asiento(descripcion,codigo,idestado,iduscreacion,fecreacion) VALUES ('$descripcion', '$codigo','$estado','$usuars','$fechai') returning id ";
                        return intval($this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id"));
                }
    
                public function ActualizaTipoAsiento($datos)
                {       $id = $datos->id;
                        $descripcion = strtoupper(trim($datos->descripcion));
                        $codigo = strtoupper(trim($datos->codigo));
                        $estado = $datos->estado;    
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $usuars = $_SESSION['IDUSER'];
                        
                        $Query = "UPDATE contable.tipo_asiento "
                               . "SET descripcion='$descripcion', idestado='$estado', codigo='$codigo', idusmodifica='$usuars', femodificacion='$fecham' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                }
    
                public function DesactivaTipoAsiento($id)
                {       $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE contable.tipo_asiento SET idestado=0, idusmodifica=".$_SESSION['IDUSER'].", femodificacion='$fecham' WHERE id=".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                }
        }
