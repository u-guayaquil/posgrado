<?php
        require_once("src/base/DBDAO.php");

        class DAOPlantillaContable extends DBDAO
        {     private static $entityPlantillaContable;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityPlantillaContable = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityPlantillaContable->MyFunction();
                     $this->Fecha  = new DateControl();
              }

              function BuscarPlantillaContable($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(p.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and p.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT p.id, p.descripcion,p.glosa,p.idtipoasto, p.fecreacion, e.id as idestado"
                           . ", e.descripcion as txtestado,t.descripcion as tipoasiento "
                           . " FROM contable.plantilla_asiento p"
                           . " INNER JOIN contable.tipo_asiento t ON p.idtipoasto=t.id"
                           . " INNER JOIN general.Estado e ON p.idestado=e.id".$JOIN.$LIKE.$IDD
                           . " ORDER BY p.descripcion".$LIM;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarDetallePlantillaContable($PrepareDQL)
              {      $IDD  = array_key_exists("id", $PrepareDQL) ? " and p.id =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT d.id, d.idplantilla,d.idcuenta,d.idmovnto,c.descripcion as cuenta"
                           . " FROM contable.plantilla_asiento_dt d"
                           . " INNER JOIN contable.plantilla_asiento p ON d.idplantilla=p.id".$IDD
                           . " INNER JOIN general.planctas c ON d.idcuenta = c.id"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaPlantillaContable($datos)
              {    try{ 
                        $estado = 1;
                        $descripcion = strtoupper($datos->descripcion_plantillacontable);
                        $glosa = strtoupper($datos->glosa_plantillacontable);
                        $tipoasiento = ($datos->tipoasiento_plantillacontable);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO contable.plantilla_asiento (descripcion,idtipoasto,glosa,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".($descripcion)."', ".$tipoasiento.", '".($glosa)."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");   
                        if (is_numeric($id)){
                                $idDet = $this->InsertaDetallePlantillaContable($datos,$id);
                                if(is_numeric($idDet)){                                    
                                    return intval($id);
                                }else{
                                    return $idDet;
                                }
                        }else{
                                return $id;
                        }
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo crear la Plantilla..');
                    }                     
              }
              
              public function InsertaDetallePlantillaContable($Form,$idPlantilla,$tipoaccion=0) {
                            $cuenta=0;
                            $idR=0;
                            foreach ($Form as $datos=>$informacion){
                                switch (substr($datos, 0,16)){ 
                                    case 'idtipomovimiento':    
                                            $tipo = $informacion;
                                            if($tipoaccion == 0){
                                                $Rsid =$this->InsertarDetalle($idPlantilla,$cuenta,$tipo);
                                            }                                          
                                             if (is_numeric($Rsid)){
                                                $idR = $this->Funciones->FieldDataByName($Rsid,"id");
                                             }else{ break; $idR='Error al Insertar los Datos...';}
                                        break;
                                    case 'idCuentaContable':                                        
                                            $cuenta = $informacion;                                            
                                        break;
                                }
                            } 
                            if (is_numeric($idR)){
                                return intval($idR);
                            }else{
                                return $idR;
                            }
              }
              
              public function InsertarDetalle($idPlantilla,$cuenta,$tipo)
              {      $Query = "INSERT INTO contable.plantilla_asiento_dt (idplantilla,idcuenta,idmovnto) "
                            . "VALUES (".$idPlantilla."::INTEGER,".$cuenta.",'".$tipo."') returning id ";
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
              }
    
              public function EliminaDetalle($plantilla)
              {      $Query = "DELETE FROM contable.plantilla_asiento_dt WHERE idplantilla= ".$plantilla;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
              }
              
              public function ActualizaPlantillaContable($datos)
              {      try{
                        $id = $datos->idPlantilla_plantillacontable;
                        $estado = $datos->estado_plantillacontable;
                        $descripcion = strtoupper($datos->descripcion_plantillacontable);                        
                        $glosa = strtoupper($datos->glosa_plantillacontable);
                        $tipoasiento = ($datos->tipoasiento_plantillacontable);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE contable.plantilla_asiento "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.", glosa= '".$glosa."', idtipoasto= ".$tipoasiento
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        if (is_numeric($id)){
                                $idDet = $this->InsertaDetallePlantillaContable($datos,$id);
                                if(is_numeric($idDet)){                                    
                                    return intval($id);
                                }else{
                                    return $idDet;
                                }
                        }else{
                                return $id;
                        }
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Actualizar los Datos..');
                    }
              }
    
              public function DesactivaPlantillaContable($id)
              {   try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE contable.plantilla_asiento SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Eliminar la Plantilla..');
                    }
              }
        }
