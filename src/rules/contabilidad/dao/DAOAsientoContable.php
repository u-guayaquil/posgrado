<?php
        require_once("src/base/DBDAO.php");

        class DAOAsientoContable extends DBDAO
        {     private static $entityAsientoContable;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityAsientoContable = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityAsientoContable->MyFunction();
                     $this->Fecha  = new DateControl();
              }

              function BuscarAsientoContable($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and a.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(a.glosa) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and a.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT a.id, a.nutipo_asiento,a.idsucursal,a.fecha,a.glosa,a.idtipo_asiento, a.fecreacion, e.id as idestado"
                           . ", e.descripcion as txtestado,t.descripcion as tipoasiento,s.descripcion as sucursal "
                           . " FROM contable.asiento a"
                           . " INNER JOIN contable.tipo_asiento t ON a.idtipo_asiento=t.id"
                           . " INNER JOIN general.sucursal s ON a.idsucursal=s.id"
                           . " INNER JOIN general.Estado e ON a.idestado=e.id".$JOIN.$LIKE.$IDD
                           . " ORDER BY a.id".$LIM; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarDetalleAsientoContable($PrepareDQL)
              {      $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE d.idasiento =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT d.id, d.idasiento,d.idcuenta,d.idmovnto,c.descripcion as cuenta,d.valor,d.glosa"
                           . " FROM contable.asiento_dt d"
                           . " INNER JOIN contable.asiento p ON d.idasiento=p.id"
                           . " INNER JOIN general.planctas c ON d.idcuenta = c.id".$IDD
                           . " ORDER BY d.id"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaAsientoContable($datos)
              {    try{
                        $estado = 1;
                        $glosa = strtoupper($datos->glosa_asientocontable);
                        $tipoasiento = ($datos->tipoasiento_asientocontable);
                        $numtipoasiento = ($datos->numtipoasiento_asientocontable);
                        $fecha = ($datos->fecha_asientocontable);
                        $sucursal = ($datos->idsucursal_asientocontable);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO contable.asiento (idtipo_asiento,nutipo_asiento,idsucursal,glosa,fecha"
                               . ",idestado,iduscreacion,fecreacion)"
                               . "VALUES (".$tipoasiento.", '".$numtipoasiento."', ".$sucursal.", '".$glosa."', to_timestamp('".$fecha."','YYYY-MM-DD')"
                               . ", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";                        
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        if (is_numeric($id)){
                                $idDet = $this->InsertaDetalleAsientoContable($datos,$id);
                                if(is_numeric($idDet)){                                    
                                    return intval($id);
                                }else{
                                    return $idDet;
                                }
                        }else{
                                return $id;
                        }
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo crear el Asiento.');
                    }                     
              }
              
              public function InsertaDetalleAsientoContable($Form,$idAsiento,$tipoaccion=0) {
                            $cuenta=0;
                            $debe=0;
                            $haber=0;
                            $tipo=0;
                            $idR=0;
                            foreach ($Form as $datos=>$informacion){
                                switch (substr($datos, 0,4)){ 
                                    case 'detg':    
                                            $glosa = $informacion;
                                            if($tipoaccion == 0){
                                                $Rsid =$this->InsertarDetalle($idAsiento,$cuenta,$tipo,$debe,$haber,$glosa);
                                            }                                          
                                            if (is_numeric($Rsid)){
                                                $idR = $this->Funciones->FieldDataByName($Rsid,"id");
                                            }else{ break; $idR='Error al Insertar los Datos...';}
                                        break;
                                    case 'detc':                                        
                                            $cuenta = $informacion;                                            
                                        break;
                                    case 'detd':                                        
                                            $debe = $informacion;                                            
                                        break;
                                    case 'deth':                                        
                                            $haber = $informacion;                                            
                                        break;
                                    case 'dett':                                        
                                            $tipo = $informacion;                                            
                                        break;
                                }
                            } 
                            if (is_numeric($idR)){
                                return intval($idR);
                            }else{
                                return $idR;
                            }
              }
              
              public function InsertarDetalle($idAsiento,$cuenta,$tipo,$debe,$haber,$glosa)
              {      if($debe=='0.00'){
                        $valor = $haber;
                     }else{
                        $valor = $debe;
                     }
                     $Query = "INSERT INTO contable.asiento_dt (idasiento,idcuenta,idmovnto,valor,glosa) "
                            . "VALUES (".$idAsiento.",".$cuenta.",'".$tipo."',".$valor.",'".$glosa."') returning id "; //echo $Query;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
              }
    
              public function EliminaDetalle($asiento)
              {      $Query = "DELETE FROM contable.asiento_dt WHERE idasiento= ".$asiento;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
              }
    
              public function ActualizaAsientoContable($datos)
              {      try{
                        $id = $datos->idAsiento_asientocontable;
                        $estado = $datos->estado_asientocontable;
                        $glosa = strtoupper($datos->glosa_asientocontable);
                        $tipoasiento = ($datos->tipoasiento_asientocontable);
                        $numtipoasiento = ($datos->numtipoasiento_asientocontable);
                        $fecha = ($datos->fecha_asientocontable);
                        $sucursal = ($datos->idsucursal_asientocontable);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE contable.asiento "
                               . "SET idtipo_asiento= ".$tipoasiento.", idestado= ".$estado.", glosa= '".$glosa."', nutipo_asiento= '".$numtipoasiento."'"
                               . ", idsucursal= '".$sucursal."', fecha = to_timestamp('".$fecha."','YYYY-MM-DD') "
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        if (is_numeric($id)){
                                $idDet = $this->InsertaDetalleAsientoContable($datos,$id);
                                if(is_numeric($idDet)){                                    
                                    return intval($id);
                                }else{
                                    return $idDet;
                                }
                        }else{
                                return $id;
                        }
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Actualizar los Datos..');
                    }
              }
    
              public function DesactivaAsientoContable($id)
              {   try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE contable.asiento SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Eliminar el Asiento..');
                    }
              }
        }
