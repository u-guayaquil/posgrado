<?php
require_once("src/base/EntidadGlobal.php");

    /**
     *
     * @Entity
     * @Table(name="tipo_asiento", schema="contable")
     */
class TipoAsiento extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;
    /**
      * @Column(name="codigo", type="string", length=10)
    */
    private $codigo;
    /**
      * @Column(name="descripcion", type="string", length=50)
    */
    private $descripcion = '';

    public function getId()
    {
        return $this->id;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

}

