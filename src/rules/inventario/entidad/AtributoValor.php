<?php
require_once("src/base/EntidadGlobal.php");
require_once("src/rules/inventario/entidad/Atributo.php");

    /**
     *
     * @Entity
     * @Table(name="atributo_valor", schema="inventario")
     */
class AtributoValor extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion = '';
    
    /**
      * @Column(name="idatributo", type="integer")
    */
    private $idatributo;
    
    /**
     * @ManyToOne(targetEntity="Atributo")
     * @JoinColumn(name="idatributo", referencedColumnName="id", nullable=false)
     */
    private $Atributos;

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }
    function getAtributos() {
        return $this->Atributos;
    }

    function setAtributos(Atributo $Atributos) {
        $this->Atributos = $Atributos;
    }
    
    function getIdatributo() {
        return $this->idatributo;
    }

    function setIdatributo($idatributo) {
        $this->idatributo = $idatributo;
    }



}

