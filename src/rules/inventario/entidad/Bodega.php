<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/inventario/entidad/TipoBodega.php");
    require_once("src/rules/general/entidad/Sucursal.php");

    /**
     * @Table(name="bodega", schema="inventario")
     * @Entity
     */
    
    class Bodega extends EntidadGlobal
    {
            /**
             * @Column(name="id", type="integer", nullable=false)
             * @Id
             * @GeneratedValue
             */
            private $id;

            /**
             * @Column(name="codigo", type="string", length=10, nullable=false)
             */
            private $codigo;

            /**
             * @Column(name="descripcion", type="string", length=20, nullable=false)
             */
            private $descripcion;

            /**
             * @ManyToOne(targetEntity="Sucursal")
             * @JoinColumn(name="idsucursal", referencedColumnName="id")
             */
            private $Sucursal;

            /**
             * @ManyToOne(targetEntity="TipoBodega")
             * @JoinColumn(name="idtipobodega", referencedColumnName="id")
             */
            private $TipoBodega;

            public function getId()
            {      return $this->id;
            }

            public function setCodigo($codigo)
            {      $this->codigo = $codigo;
            }

            public function getCodigo()
            {      return $this->codigo;
            }

            public function setDescripcion($descripcion)
            {      $this->descripcion = $descripcion;
            }

            public function getDescripcion()
            {      return $this->descripcion;
            }

            public function setSucursal(Sucursal $sucursal)
            {      $this->Sucursal = $sucursal;
            }

            public function getSucursal()
            {      return $this->Sucursal;
            }

            public function setTipoBodega(TipoBodega $tipobodega)
            {      $this->TipoBodega = $tipobodega;
            }

            public function getTipoBodega()
            {      return $this->TipoBodega;
            }
}

