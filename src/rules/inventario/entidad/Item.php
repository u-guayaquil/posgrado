<?php
require_once("src/base/EntidadGlobal.php");

    /**
     *
     * @Entity
     * @Table(name="item", schema="inventario")
     */
class Item extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion = '';
    
    /**
      * @Column(name="categoria", type="string", length=1)
    */
    private $categoria = 'B';

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

}

