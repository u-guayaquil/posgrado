<?php
    
    require_once ('src/rules/inventario/entidad/Item.php');
    require_once ('src/rules/inventario/entidad/Atributo.php');
    /**
     *
     * @Entity
     * @Table(name="item_atributo", schema="inventario")
     */
    class ItemAtributo extends EntidadGlobal
    {
    
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;
    
    /**
      * @Column(name="iditem", type="integer", length=10)
    */
    private $idItem;
    
    /**
      * @Column(name="idatributo", type="integer", length=10)
    */
    private $idAtributo;
    
    /**
     * @ManyToOne(targetEntity="Item")
     * @JoinColumn(name="iditem", referencedColumnName="id")
     */
    private $Items;
    
    /**
     * @ManyToOne(targetEntity="Atributo")
     * @JoinColumn(name="idatributo", referencedColumnName="id")
     */
    private $Atributos;
    

    public function getId()
    {      return $this->id;
    }
    function getIdItem() {
        return $this->idItem;
    }

    function getIdAtributo() {
        return $this->idAtributo;
    }

    function getItems() {
        return $this->Items;
    }

    function getAtributos() {
        return $this->Atributos;
    }

    function setIdItem($idItem) {
        $this->idItem = $idItem;
    }

    function setIdAtributo($idAtributo) {
        $this->idAtributo = $idAtributo;
    }

    function setItems($Items) {
        $this->Items = $Items;
    }

    function setAtributos($Atributos) {
        $this->Atributos = $Atributos;
    }


    }

