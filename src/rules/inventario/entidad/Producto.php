<?php
    
    require_once("src/base/EntidadGlobal.php");

    /**
      * @Entity
      * @Table(name="producto", schema="inventario")
      */
    class Producto extends EntidadGlobal
    {
           /**
            *
            * @Id
            * @Column(name="id", type="integer", nullable=false)
            * @GeneratedValue
            */
            private $id;

           /**
            *
            * @Column(name="codigo", type="string", length=15, nullable=false)
            */
            private $codigo;

           /**
            * @ManyToOne(targetEntity="Item") 
            * @JoinColumn(name="iditem", referencedColumnName="id")
            */
            private $items; 

            /**
            *
            * @Column(name="iditem", type="integer", nullable=false)
            */
            private $iditem;

           /**
            * @Column(name="descripcion", type="string", length=60, nullable=false)
            */
            private $descripcion = '';

           /**
            *
            * @Column(name="idproveedor", type="integer", nullable=false)
            */
            private $idproveedor;
            
           /**
            * @Column(name="ctamercaderia", type="integer", nullable=false)
            */
            private $ctamercaderia = 0;

           /**
            * @Column(name="ctaventas", type="integer", nullable=false)
            */
            private $ctaventas = 0;
            
           /**
            * @Column(name="ctacostos", type="integer", nullable=false)
            */
            private $ctacostos = 0;

           /**
            * @Column(name="ctadevolucion", type="integer", nullable=false)
            */
            private $ctadevolucion = 0;

           /**
            * @Column(name="ivacompras", type="integer", nullable=false)
            */
            private $ivacompras = 0;

           /**
            * @Column(name="ivaventas", type="integer", nullable=false)
            */
            private $ivaventas = 0;
            
           /**
            * @Column(name="iceventas", type="integer", nullable=false)
            */
            private $iceventas = 0;

           /**
            * @Column(name="serventas", type="integer", nullable=false)
            */
            private $serventas = 0;
            
           /**
            * @ManyToOne(targetEntity="Unidad") 
            * @JoinColumn(name="idunidad", referencedColumnName="id")
            */
            private $unidades; 
            
           /**
            * @Column(name="idunidad", type="integer", nullable=false)
            */
            private $idunidad = 0;
            
           /**
            * @Column(name="factcantidad", type="integer", nullable=false)
            */
            private $factcantidad = 2;

           /**
            * @Column(name="factcosto", type="integer", nullable=false)
            */
            private $factcosto = 2;

           /**
            * @Column(name="factprecio", type="integer", nullable=false)
            */
            private $factprecio = 2;

           /**
            * @Column(name="stockmin", type="float", nullable=false)
            */
            private $stockmin = 0;
            
           /**
            * @Column(name="stockmax", type="float", nullable=false)
            */
            private $stockmax = 0;
            
           /**
            * @Column(name="dsctomax", type="float", nullable=false)
            */
            private $dsctomax = 0;

           /**
            * @Column(name="pagacoms", type="float", nullable=false)
            */
            private $pagacoms = 0;
            
            public function getId()
            {      return $this->id;
            }

            public function setCodigo($Codigo)
            {      $this->codigo = $Codigo;
            }

            public function getCodigo()
            {      return $this->codigo;
            }

            
            public function setIditem($Iditem)
            {      $this->iditem = $Iditem;
            }

            public function getIditem()
            {      return $this->iditem;
            }

            public function setItems(Item $items)
            {      $this->items = $items;
            }

            public function getItems()
            {      return $this->items;
            }
            
            public function setIdproveedor($idproveedor)
            {      $this->idproveedor = $idproveedor;
            }

            public function getIdproveedor()
            {      return $this->idproveedor;
            }

            public function setDescripcion($Descripcion)
            {      $this->descripcion = $Descripcion;
            }

            public function getDescripcion()
            {      return $this->descripcion;
            }

            public function getCtamercaderia()
            {      return $this->ctamercaderia;
            }

            public function setCtamercaderia($Ctamercaderia)
            {      $this->ctamercaderia = $Ctamercaderia;
            }

            public function getCtaventas()
            {      return $this->ctaventas;
            }

            public function setCtaventas($Ctaventas)
            {      $this->ctaventas = $Ctaventas;
            }

            public function getCtacostos()
            {      return $this->ctacostos;
            }

            public function setCtacostos($Ctacostos)
            {      $this->ctacostos = $Ctacostos;
            }

            public function getCtadevolucion()
            {      return $this->ctadevolucion;
            }

            public function setCtadevolucion($Ctadevolucion)
            {      $this->ctadevolucion = $Ctadevolucion;
            }
            
            public function getIvacompras()
            {      return $this->ivacompras;
            }

            public function setIvacompras($Ivacompras)
            {      $this->ivacompras = $Ivacompras;
            }

            public function getIvaventas()
            {      return $this->ivaventas;
            }

            public function setIvaventas($Ivaventas)
            {      $this->ivaventas = $Ivaventas;
            }
            
            public function getIceventas()
            {      return $this->iceventas;
            }

            public function setIceventas($Iceventas)
            {      $this->iceventas = $Iceventas;
            }
            
            public function getServentas()
            {      return $this->serventas;
            }

            public function setServentas($Serventas)
            {      $this->serventas = $Serventas;
            }

            public function getIdunidad()
            {      return $this->idunidad;
            }

            public function setIdunidad($Idunidad)
            {      $this->idunidad = $Idunidad;
            }

            function getUnidades()
            {       return $this->unidades;
            }
	
            function setUnidades(Unidad $unidades)
            {       $this->unidades = $unidades;
            }
            
            public function getFactcantidad()
            {      return $this->factcantidad;
            }

            public function setFactcantidad($Factcantidad)
            {      $this->factcantidad = $Factcantidad;
            }

            public function getFactcosto()
            {      return $this->factcosto;
            }

            public function setFactcosto($Factcosto)
            {      $this->factcosto = $Factcosto;
            }
            
            public function getFactprecio()
            {      return $this->factprecio;
            }

            public function setFactprecio($Factprecio)
            {      $this->factprecio = $Factprecio;
            }

            public function getStockmin()
            {      return $this->stockmin;
            }

            public function setStockmin($Stockmin)
            {      $this->stockmin = $Stockmin;
            }
            
            public function getStockmax()
            {      return $this->stockmax;
            }

            public function setStockmax($Stockmax)
            {      $this->stockmax = $Stockmax;
            }
            
            public function getDsctomax()
            {      return $this->dsctomax;
            }

            public function setDsctomax($Dsctomax)
            {      $this->dsctomax = $Dsctomax;
            }
            
            public function getPagacoms()
            {      return $this->pagacoms;
            }

            public function setPagacoms($Pagacoms)
            {      $this->pagacoms = $Pagacoms;
            }
            
            
    }
?>

