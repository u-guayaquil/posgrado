<?php
    
    /**
      * @Entity
      * @Table(name="producto_atributo_valor", schema="inventario")
      */
    class ProductoAtributoValor
    {
           /**
            * @Id
            * @Column(name="id", type="integer", nullable=false)
            * @GeneratedValue
            */
            private $id;

           /**
            *
            * @Column(name="idproducto", type="integer", nullable=false)
            */
            private $idproducto; 

           /**
            *
            * @Column(name="idatributo_valor", type="integer", nullable=false)
            */
            private $idatributo_valor; 

            
            public function getId()
            {      return $this->id;
            }

            public function setProducto($idproducto)
            {      $this->idproducto = $idproducto;
            }

            public function getProducto()
            {      return $this->idproducto;
            }
            
            function setAtributoValor($atribValor)
            {       $this->idatributo_valor = $atribValor;
            }

            function getAtributoValor()
            {       return $this->idatributo_valor;
            }
    }
?>

