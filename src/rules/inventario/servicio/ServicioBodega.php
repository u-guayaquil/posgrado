<?php
        require_once("src/rules/inventario/dao/DAOBodega.php");
        require_once("src/rules/general/dao/DAOSucursal.php");
        require_once("src/rules/inventario/dao/DAOTipoBodega.php");
        
        require_once("src/rules/inventario/entidad/Bodega.php"); 

        class ServicioBodega 
        {       private $DAOBodega;
     
                function __construct()
                {        $this->DAOBodega = new DAOBodega();
                }
            
                function BuscarBodegaByDescripcion($prepareDQL)
                {       return $this->DAOBodega->ObtenerBodega($prepareDQL);
                }
                function BuscarBodegaByID($id)
                {       return $this->DAOBodega->ObtenerBodega(array('id' => intval($id)));
                }

                function GuardaDBBodega($Bodega)
                {       if (empty($Bodega->id)){
                            return $this->DAOBodega->InsertaBodega($Bodega);
                        }else{
                            if (intval($Bodega->id) == intval($Bodega->idpadre)){
                                return "La bodega no puede ser padre de si mismo.";
                            }else{
                                return $this->DAOBodega->ActualizaBodega($Bodega);
                            }
                        }
                }
                
                function DesactivaBodega($Id)
                {       return $this->DAOBodega->DesactivaBodega(intval($Id));
                }
        }
?>