<?php
    require_once("src/rules/inventario/dao/DAOProductoAtributoValor.php");  
    
    class ServicioProductoAtributoValor 
    {       private $DAOProductoAtributoValor;
        
            function __construct()
            {       $this->DAOProductoAtributoValor = new DAOProductoAtributoValor();
            }
        
            function CreaProductoAtributoValor($ProductoAtributoValor)
            {       if (trim($ProductoAtributoValor->valatrib)!="")
                    {   $atributos = explode(":",$ProductoAtributoValor->valatrib);   
                        for($i=0; $i<count($atributos); $i++) 
                        {   $prdID = $ProductoAtributoValor->ProductoID;
                            $atrID = $ProductoAtributoValor->{$atributos[$i]};
                            $this->DAOProductoAtributoValor->InsertaProductoAtributoValor($prdID,$atrID);        
                        }
                    }
            }
            
            function EditaProductoAtributoValor($ProductoAtributoValor)
            {       if ($this->DAOProductoAtributoValor->EliminaProductoAtributoValor($ProductoAtributoValor->ProductoID))
                    {   $this->CreaProductoAtributoValor($ProductoAtributoValor);
                    }
            }
            
            function BuscarProductoAtributoValorByProducto($IdProducto)
            {       return $this->DAOProductoAtributoValor->ObtenerProductoAtributoValorByProducto(intval($IdProducto));        
            }
    }
    
    
?>