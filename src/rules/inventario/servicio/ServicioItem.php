<?php
            require_once("src/rules/inventario/dao/DAOItem.php");
            require_once("src/rules/inventario/entidad/Item.php");
            
            class ServicioItem
            {     private $DAOItem;
     
                  function __construct()
                  {        $this->DAOItem = new DAOItem();
                  }
           
                  function GuardaDBItem($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOItem->InsertaItem($Form);
                          }else{
                            return $this->DAOItem->ActualizaItem($Form);
                          }
                  }
                  
                  function DesactivaItem($Id)
                  {       return $this->DAOItem->DesactivaItem($Id);
                  }
           
                  function BuscarItem($prepareDQL)
                  {        return $this->DAOItem->ObtenerItem($prepareDQL);
                  }
                  
                  function BuscarItemByCodigo($codigo)
                  {        return $this->DAOItem->ObtenerItemByCodigo($codigo);
                  }
            }
?>

