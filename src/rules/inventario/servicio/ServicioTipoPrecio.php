<?php
            require_once("src/rules/inventario/dao/DAOTipoPrecio.php");
            
            class ServicioTipoPrecio
            {     private $DAOTipoPrecio;
     
                  function __construct()
                  {        $this->DAOTipoPrecio = new DAOTipoPrecio();
                  }
           
                  function GuardaDBTipoPrecio($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOTipoPrecio->InsertaTipoPrecio($Form);
                          }else{
                            return $this->DAOTipoPrecio->ActualizaTipoPrecio($Form);
                          }
                  }
                  
                  function DesactivaTipoPrecio($Id)
                  {       return $this->DAOTipoPrecio->DesactivaTipoPrecio($Id);
                  }
           
                  function BuscarTipoPrecio($PrepareDQL)
                  {        return $this->DAOTipoPrecio->ObtenerTipoPrecio($PrepareDQL);
                  }
                  
                  

            }
?>

