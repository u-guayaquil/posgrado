<?php
            require_once("src/rules/inventario/dao/DAOUnidadPresentacion.php");
            
            class ServicioUnidadPresentacion
            {     private $DAOUnidadPresentacion;
     
                  function __construct()
                  {        $this->DAOUnidadPresentacion = new DAOUnidadPresentacion();
                  }
           
                  function BuscarUnidadPresentacion()
                  {        return $this->DAOUnidadPresentacion->BuscarUnidadPresentacion();
                  }
                  
                  function BuscarUnidadPresentacionById($Array)
                  {        return $this->DAOUnidadPresentacion->BuscarUnidadPresentacionById($Array);
                  }
                  
                  function BuscarUnidadPresentacionByProducto($Array)
                  {        return $this->DAOUnidadPresentacion->BuscarUnidadPresentacionByProducto($Array);
                  }
            }
?>

