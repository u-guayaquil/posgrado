<?php
            require_once("src/rules/inventario/dao/DAOAtributoValor.php");
            
            class ServicioAtributoValor
            {     private $DAOAtributoValor;
     
                  function __construct()
                  {        $this->DAOAtributoValor = new DAOAtributoValor();
                  }
           
                  function GuardaDBAtributoValor($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOAtributoValor->InsertaAtributoValor($Form);
                          }else{
                            return $this->DAOAtributoValor->ActualizaAtributoValor($Form);
                          }
                  }
                  
                  function DesactivaAtributoValor($Id)
                  {       return $this->DAOAtributoValor->DesactivaAtributoValor($Id);
                  }
           
                  function BuscarAtributoValor($PrepareDQL)
                  {        return $this->DAOAtributoValor->ObtenerAtributoValor($PrepareDQL);
                  }
                  
                  function BuscarAtributoValorByAtribID($idAributo)
                  {        return $this->DAOAtributoValor->ObtenerAtributoValorByAtribID($idAributo);
                  }
                  
                  function BuscarAtributoValorByProductoAtributoID($IdProducto,$idAributo)
                  {        return $this->DAOAtributoValor->ObtenerAtributoValorByProductoAtributoID(intval($IdProducto),intval($idAributo)); 
                  }                  

            }
?>

