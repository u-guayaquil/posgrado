<?php
            require_once("src/rules/inventario/dao/DAOItemAtributo.php");
            require_once("src/rules/inventario/entidad/ItemAtributo.php");
            require_once("src/rules/inventario/servicio/ServicioItem.php");
            require_once("src/rules/inventario/servicio/ServicioAtributo.php");
            
            class ServicioItemAtributo
            {       private $DAOItemAtributo;
                    private $ServicioItem;
                    private $ServicioAtributo;
     
                    function __construct()
                    {       $this->DAOItemAtributo = new DAOItemAtributo();
                            $this->ServicioItem = new ServicioItem();
                            $this->ServicioAtributo = new ServicioAtributo();
                    }
                  
                    function GuardaDBItemAtributo($Form)
                    {       if (empty($Form->iditem_ItemAtributo)){ 
                                return $this->DAOItemAtributo->InsertaItemAtributo($Form);
                            }else{
                                return $this->DAOItemAtributo->ActualizaItemAtributo($Form);
                            }
                    }
                      
                    function BuscarItemAtributo($prepareDQL='')
                    {       return $this->DAOItemAtributo->ObtenerDatosGeneral($prepareDQL);
                    }
                  
                    function BuscarItemsconAtributos($item)
                    {       return $this->DAOItemAtributo->ObtenerItemsconAtributos($item);
                    }
                  
                    function BuscarAtributosxItem($item)
                    {       return $this->DAOItemAtributo->ObtenerAtributosxItem($item);
                    }
                  
            }
?>

