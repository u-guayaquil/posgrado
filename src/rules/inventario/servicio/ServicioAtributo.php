<?php
            require_once("src/rules/inventario/dao/DAOAtributo.php");
            
            class ServicioAtributo
            {     private $DAOAtributo;
     
                  function __construct()
                  {        $this->DAOAtributo = new DAOAtributo();
                  }
           
                  function GuardaDBAtributo($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOAtributo->InsertaAtributo($Form);
                          }else{
                            return $this->DAOAtributo->ActualizaAtributo($Form);
                          }
                  }
                  
                  function DesactivaAtributo($Id)
                  {       return $this->DAOAtributo->DesactivaAtributo($Id);
                  }
           
                  function BuscarAtributo($PrepareSQL)
                  {        return $this->DAOAtributo->BuscarAtributo($PrepareSQL);
                  }
            }
?>

