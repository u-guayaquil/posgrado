<?php
            require_once("src/rules/inventario/dao/DAOTipoBodega.php");
            
            class ServicioTipoBodega
            {     private $DAOTipoBodega;
     
                  function __construct()
                  {        $this->DAOTipoBodega = new DAOTipoBodega();
                  }
           
                  function GuardaDBTipoBodega($Form)
                  {       if (empty($Form->id)){
                              return $this->DAOTipoBodega->InsertaTipoBodega($Form);
                            }else{
                              return $this->DAOTipoBodega->ActualizaTipoBodega($Form);
                            }
                  }
           
                  function BuscarTipoBodega($Array)
                  {        return $this->DAOTipoBodega->ObtenerTipoBodega($Array);
                  }
            }
?>

