<?php
        require_once("src/rules/inventario/dao/DAOProducto.php");  
        
        class ServicioProducto
        {       private $DAOProducto;
        
                function __construct()
                {       $this->DAOProducto = new DAOProducto();
                }
                
                function BuscarProductoByID($id)
                {        return $this->DAOProducto->ObtenerObjetoProductoByID(intval($id));
                }
                
                function CreaProducto($Forma)
                {       return $this->DAOProducto->InsertaProducto($Forma);
                }
                
                function EditaProducto($Forma)
                {       return $this->DAOProducto->EditaProducto($Forma);
                }
                
                
                function DesactivaProducto($Id)
                {       return $this->DAOProducto->DesactivaProducto(intval($Id));
                }

                function ObtenerProductosporDescripcion($PrepareSQL)
                {       return $this->DAOProducto->ObtenerProductosporDescripcion($PrepareSQL);
                }
                
        }        
?>                