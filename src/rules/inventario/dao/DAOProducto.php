<?php

    require_once("src/base/DBDAO.php");

    class DAOProducto extends DBDAO
    {       private static $entityProducto;
    
            public function __construct()
            {       self::$entityProducto= DBDAO::getConnection('PgProvider');
                    $this->Funciones = self::$entityProducto->MyFunction();
                    $this->Fecha  = new DateControl();
            }
            
            function ObtenerObjetoProductoByID($id)
            {       $DQL = "Select p.id,
                                   p.codigo,
                                   p.iditem,i.descripcion as item,
                                   p.idproveedor, pv.nombre as proveedor,
                                   p.descripcion,
                                   p.ctamercaderia,cm.descripcion as mercaderia,
                                   p.ctaventas,vt.descripcion as ventas,
                                   p.ctacostos,ct.descripcion as costos,
                                   p.ctadevolucion,dv.descripcion as devolucion,
                                   p.ivacompras,
                                   p.ivaventas,
                                   p.idunidad,u.descripcion as unidad,
                                   p.factcantidad,
                                   p.factcosto,
                                   p.factprecio,
                                   p.stockmin,
                                   p.stockmax,
                                   p.dsctomax,
                                   p.pagacoms,
                                   p.idestado,e.descripcion as estado
                            From 
                                   general.estado as e inner join 
                                   (general.unidad as u inner join 
                                   (inventario.item as i inner join 
                                   (inventario.producto as p left join general.planctas as cm ON cm.id=p.ctamercaderia
                                   left join general.planctas as vt ON vt.id=p.ctaventas
                                   left join general.planctas as ct ON ct.id=p.ctacostos
                                   left join general.planctas as dv ON dv.id=p.ctadevolucion
                                   left join impuesto.iva as ic ON ic.id=p.ivacompras
                                   left join impuesto.iva as iv ON iv.id=p.ivaventas
                                   left join general.acreedor as pv ON pv.id=p.idproveedor
                                   ) ON p.iditem=i.id) ON p.idunidad=u.id) ON p.idestado= e.id
                            Where
                                   p.id=$id";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DQL));  
            }
            
            public function InsertaProducto($Forma)
            {       $codigo = strtoupper($Forma->Codigo);
                    $iditem = intval($Forma->iditem);
                    $txdscp = strtoupper(trim($Forma->descripcion));
                    $ctamer = intval($Forma->idctamercaderia); 
                    $ctavta = intval($Forma->idctaventas); 
                    $ctacos = intval($Forma->idctacostos); 
                    $ctadev = intval($Forma->idctadevolucion); 
                    $idunid = intval($Forma->idunidad);
                    $facant = floatval($Forma->Cant);
                    $facost = (floatval($Forma->Cost)==0 ? 2 : floatval($Forma->Cost));
                    $faprec = (floatval($Forma->Prec)==0 ? 2 : floatval($Forma->Prec));
                    $stkmin = floatval($Forma->Stkmin);
                    $stkmax = floatval($Forma->Stkmax);
                    $descto = floatval($Forma->Dscto);
                    $pagcom = floatval($Forma->Comis);
                    $fechac = $this->Fecha->getNowDateTime('DATETIME');
                    $Query = "insert into inventario.producto(codigo,iditem,idproveedor,descripcion,ctamercaderia,ctaventas,ctacostos,ctadevolucion,ivacompras,ivaventas,iceventas,serventas,idunidad,factcantidad,factcosto,factprecio,stockmin,stockmax,dsctomax,pagacoms,idestado,iduscreacion,idusmodifica,fecreacion,femodificacion) ";
                    $Query.= " values('$codigo','$iditem','$Forma->idproveedor','$txdscp','$ctamer','$ctavta','$ctacos','$ctadev','$Forma->idivacompra','$Forma->idivaventa',0,0,'$idunid','$facant','$facost','$faprec','$stkmin','$stkmax','$descto','$pagcom',1,".$_SESSION['IDUSER'].",0,'$fechac','1900-01-01 00:00:00') returning id ";
                    $resourceID = $this->Funciones->Query($Query);
                    return $this->Funciones->FieldDataByName($resourceID,'id'); 
            }
            
            public function EditaProducto($Forma)
            {       $id = intval($Forma->ProductoID);
                    $codigo = strtoupper($Forma->Codigo);
                    $descrp = strtoupper(trim($Forma->descripcion));
                    $ctamer = intval($Forma->idctamercaderia);
                    $ctavta = intval($Forma->idctaventas);
                    $ctacto = intval($Forma->idctacostos);
                    $ctadev = intval($Forma->idctadevolucion);
                    $idunid = intval($Forma->idunidad);
                    $fecham = $this->Fecha->getNowDateTime('DATETIME');
                    $facost = (floatval($Forma->Cost)==0 ? 2 : floatval($Forma->Cost));
                    $facant = floatval($Forma->Cant);
                    $facpre = (floatval($Forma->Prec)==0 ? 2 : floatval($Forma->Prec));
                    $comisi = floatval($Forma->Comis);
                    $stkmin = floatval($Forma->Stkmin);
                    $stkmax = floatval($Forma->Stkmax);
                    $descto = floatval($Forma->Dscto); 
                    
                    $Query="update inventario.producto set 
                            codigo='$codigo', 
                            iditem='$Forma->iditem', 
                            descripcion= '$descrp', 
                            idproveedor='$Forma->idproveedor', 
                            ctamercaderia='$ctamer', 
                            ctaventas='$ctavta', 
                            ctacostos='$ctacto', 
                            ctadevolucion='$ctadev', 
                            idunidad='$idunid', 
                            ivacompras='$Forma->idivacompra',
                            ivaventas='$Forma->idivaventa', 
                            iceventas=0, 
                            serventas=0, 
                            factcosto='$facost',
                            factcantidad='$facant',
                            factprecio='$facpre',
                            stockmin='$stkmin',
                            stockmax='$stkmax',
                            dsctomax='$descto',
                            pagacoms='$comisi',
                            idestado='$Forma->estado', 
                            idusmodifica=".$_SESSION['IDUSER'].", 
                            femodificacion='$fecham' 
                            where id= ".$id;                
                    $this->Funciones->Query($Query);
                    return $id;
            }
            
            public function DesactivaProducto($id)
            {       $Query = "update inventario.producto set idestado=0 where id=$id ";
                    $this->Funciones->Query($Query);
                    return true;
            }
            
            public function ObtenerProductosporDescripcion($PrepareSQL)
            {       $SDQL = "select p.id, p.codigo, p.descripcion,p.idunidad from inventario.producto as p, general.estado as e "
                          . "where p.idestado=e.id and e.id=1 and (p.codigo like '%".$PrepareSQL['texto']."%' OR p.descripcion like '%".$PrepareSQL['texto']."%')";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($SDQL));  
            }
   }


