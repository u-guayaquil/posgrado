<?php
        require_once("src/base/DBDAO.php");

        class DAOUnidadPresentacion extends DBDAO
        {     private static $entityUnidadPresentacion;
              private $Funciones;
              
              public function __construct()
              {      self::$entityUnidadPresentacion = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityUnidadPresentacion->MyFunction();
              }
              
              function BuscarUnidadPresentacion()
              {      $DSQL = "SELECT p.idproducto,p.idunidad, p.valor, u.id,u.descripcion "
                           . "FROM inventario.producto_presentacion p "
                           . "INNER JOIN general.unidad u ON p.idunidad = u.id";           
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarUnidadPresentacionById($Array)
              {      $DSQL = "SELECT p.idproducto,p.idunidad, p.valor, u.id,u.descripcion "
                           . "FROM inventario.producto_presentacion p "
                           . "INNER JOIN general.unidad u ON p.idunidad = u.id WHERE p.unidad=".$Array['unidad']." and p.producto=".$Array['producto'];           
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarUnidadPresentacionByProducto($Array)
              {      $DSQL = "SELECT p.idproducto,p.idunidad, p.valor "
                           . "FROM inventario.producto_presentacion p  WHERE p.idproducto=".$Array['producto'];           
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
        }
