<?php
        require_once("src/base/DBDAO.php");

        class DAOItem extends DBDAO
        {   private static $entityItem;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {       self::$entityItem = DBDAO::getConnection('PgProvider');
                    $this->Funciones = self::$entityItem->MyFunction();
                    $this->Fecha  = new DateControl();
            }
    
            public function ObtenerItem($PrepareDQL)
            {       $JOIN = array_key_exists("estado", $PrepareDQL) ? " and i.idestado in (".$PrepareDQL['estado'].")" : "";
                    $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(i.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                    $IDD  = array_key_exists("id", $PrepareDQL) ? " and i.id =".$PrepareDQL['id']: "";
                    $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                    
                    $DSQL = "SELECT i.id, i.descripcion,i.categoria, e.descripcion as txtestado, i.fecreacion, e.id as idestado,i.codigo, ";
                    $DSQL.= "i.ctamercaderia,coalesce(cm.descripcion,'_') as mercaderia, ";
                    $DSQL.= "i.ctaventas,coalesce(vt.descripcion,'_') as ventas, ";
                    $DSQL.= "i.ctacostos,coalesce(ct.descripcion,'_') as costos, ";
                    $DSQL.= "i.ctadevolucion,coalesce(dv.descripcion,'_') as devolucion ";
                    $DSQL.= "FROM general.estado e, (inventario.item i ";
                    $DSQL.= "LEFT JOIN general.planctas as cm ON cm.id=i.ctamercaderia ";
                    $DSQL.= "LEFT JOIN general.planctas as vt ON vt.id=i.ctaventas ";
                    $DSQL.= "LEFT JOIN general.planctas as ct ON ct.id=i.ctacostos ";
                    $DSQL.= "LEFT JOIN general.planctas as dv ON dv.id=i.ctadevolucion) ";
                    $DSQL.= "WHERE i.idestado=e.id ".$JOIN.$LIKE.$IDD." ";
                    $DSQL.= "ORDER BY i.descripcion ".$LIM;                   
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            public function ObtenerItemByCodigo($codigo)
            {       $DSQL = "SELECT coalesce(id,0) as id, descripcion,categoria, fecreacion,codigo"
                          . " FROM inventario.item WHERE codigo = '".$codigo."'"
                          . " ORDER BY id";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            public function InsertaItem($datos)
            {   try{    
                    $codigo = strtoupper($datos->codigo);
                    $descripcion = strtoupper($datos->descripcion);
                    $categoria = strtoupper($datos->categoria);
		    $estado = $datos->estado;	
                    $idctamercaderia = intval($datos->idctamercaderia);
                    $idctaventas = intval($datos->idctaventas);
                    $idctacostos = intval($datos->idctacostos);
                    $idctadevolucion = intval($datos->idctadevolucion);
                    $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        
                    $Query = "INSERT INTO inventario.item (descripcion,categoria,codigo,idestado,iduscreacion,fecreacion,ctamercaderia,ctaventas,ctacostos,ctadevolucion) "
                            . "VALUES ('$descripcion','$categoria','$codigo','$estado',".$_SESSION['IDUSER'].",'$fechai','$idctamercaderia','$idctaventas','$idctacostos','$idctadevolucion') returning id";
                        
                    return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                    } 
                catch (Exception $ex) 
                {   $this->manejadorExcepciones($ex);
                    return 'No se pudo crear el Item.';
                }
            }
    
            public function ActualizaItem($datos)
            {   try{ 
                        $id = $datos->id;
                        $codigo = strtoupper($datos->codigo);
                        $descripcion = strtoupper($datos->descripcion);						
                        $categoria = strtoupper($datos->categoria);
                        $estado = $datos->estado;						
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $idctamercaderia = intval($datos->idctamercaderia);
                        $idctaventas = intval($datos->idctaventas);
                        $idctacostos = intval($datos->idctacostos);
                        $idctadevolucion = intval($datos->idctadevolucion);
                        $usuario = $_SESSION['IDUSER'];
 
                        $Query = "update inventario.item SET "
                               . "codigo = '$codigo', "
                               . "descripcion = '$descripcion', "
                               . "categoria = '$categoria', "    
                               . "idestado = '$estado', "
                               . "idusmodifica = '$usuario', "
                               . "femodificacion= '$fecham', "
                               . "ctamercaderia = '$idctamercaderia', "
                               . "ctaventas = '$idctaventas', "
                               . "ctacostos = '$idctacostos', "
                               . "ctadevolucion = '$idctadevolucion' "
                               . "where id = ".$id;
                    
                        $this->Funciones->Query($Query);
                        return $id;
                    } 
                    catch (Exception $ex) 
                    {   $this->manejadorExcepciones($ex);
                        return 'No se pudo actualizar los datos del Item.';
                    }
            }
    
            public function DesactivaItem($id)
            {   try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE inventario.item SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return $id;
                    } 
                    catch (Exception $ex) 
                    {   $this->manejadorExcepciones($ex);
                        return 'No se pudo eliminar el Item.';
                    }
            }
        }
