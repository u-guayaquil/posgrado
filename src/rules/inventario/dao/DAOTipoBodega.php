<?php
        require_once("src/base/DBDAO.php");

        class DAOTipoBodega extends DBDAO
        {     private static $entityTipoBodega;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityTipoBodega = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityTipoBodega->MyFunction();
                     $this->Fecha  = new DateControl();
              }
    
              function ObtenerTipoBodega($PrepareDQL)
                {    $JOIN = array_key_exists("estado", $PrepareDQL) ? " and t.idestado in (".$PrepareDQL['estado'].")" : "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and t.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                     $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(t.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $DSQL = "SELECT t.id,t.descripcion, t.fecreacion, t.idestado,e.descripcion as txtestado"
                           . " FROM inventario.tipo_bodega t"
                           . " INNER JOIN general.Estado e ON t.idestado = e.id".$JOIN.$IDD.$TXT
                           . " ORDER BY t.id".$LIM;                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));           
                } 
              
              public function InsertaTipoBodega($datos)
              {     try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO inventario.tipo_bodega (descripcion,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Tipo de Bodega..';
                    } 
              }
    
              public function ActualizaTipoBodega($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');  
                        $Query = "UPDATE inventario.tipo_bodega "
                               . "SET descripcion = '".$descripcion."', idestado= ".$estado
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    } 
              }
    
              public function eliminaTipoBodega(TipoBodega $perfil)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE inventario.tipo_bodega SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                      } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Tipo de Bodega..';
                      }
              }
        }
