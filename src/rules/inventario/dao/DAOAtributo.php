<?php
        require_once("src/base/DBDAO.php");

        class DAOAtributo extends DBDAO
        {     private static $entityAtributo;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityAtributo = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityAtributo->MyFunction();
                     $this->Fecha  = new DateControl();
              }
    
              function BuscarAtributo($PrepareDQL)
              {     $JOIN = array_key_exists("estado", $PrepareDQL) ? " and a.idestado in (".$PrepareDQL['estado'].")" : "";
                    $IDD  = array_key_exists("id", $PrepareDQL) ? " and a.id =".$PrepareDQL['id']: "";
                    $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                    $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(a.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                    $DSQL = "SELECT a.id,a.descripcion, a.fecreacion, a.idestado,e.descripcion as txtestado,a.codigo"
                           . " FROM inventario.atributo a"
                           . " INNER JOIN general.Estado e ON a.idestado = e.id".$JOIN.$IDD.$TXT
                           . " ORDER BY a.id".$LIM;                  
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));  
              }
              
              public function InsertaAtributo($datos)
              {     try{        
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO inventario.atributo (descripcion,codigo,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."','".$codigo."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Atributo..';
                    }
              }
    
              public function ActualizaAtributo($datos)
              {      try{ 
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE inventario.atributo "
                               . "SET descripcion= '".$descripcion."',codigo= '".$codigo."', idestado= ".$estado.""
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaAtributo($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE inventario.atributo SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                     } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Atributo..';
                     }
              }
        }
