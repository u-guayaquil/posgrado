<?php
        require_once("src/base/DBDAO.php");

        class DAOTipoPrecio extends DBDAO
        {     private static $entityTipoPrecio;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityTipoPrecio = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityTipoPrecio->MyFunction();
                     $this->Fecha  = new DateControl();
              }
    
              public function ObtenerTipoPrecio($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and t.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(t.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and t.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT t.id, t.descripcion, e.descripcion as txtestado, t.fecreacion, e.id as idestado"
                          . " FROM inventario.tipo_precio t, general.Estado e"
                          . " WHERE t.idestado=e.id ".$JOIN.$LIKE.$IDD
                          . " ORDER BY t.descripcion".$LIM;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaTipoPrecio($datos)
              {     try{        
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO inventario.tipo_precio (descripcion,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Tipo..';
                    }
              }
    
              public function ActualizaTipoPrecio($datos)
              {      try{ 
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE inventario.tipo_precio "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.""
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaTipoPrecio($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE inventario.tipo_precio SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                     } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Tipo..';
                     }
              }
        }
