<?php
        require_once("src/base/DBDAO.php");

        class DAOItemAtributo extends DBDAO
        {       private static $entityItemAtributo;
                private $padre = array();
                private $Funciones;
                private $Fecha;
              
                public function __construct()
                {       self::$entityItemAtributo = DBDAO::getConnection('PgProvider');
                        $this->Funciones = self::$entityItemAtributo->MyFunction();
                        $this->Fecha  = new DateControl();
                }
    
                public function ObtenerItemAtributo($PrepareDQL)
                {       $IDD  = array_key_exists("iditem", $PrepareDQL) ? " where t.iditem =".$PrepareDQL['iditem']: "";
                        $TXT  = array_key_exists("texto", $PrepareDQL) ? " where i.descripcion like '%".$PrepareDQL['texto']."%'"
                                                                    . " OR a.descripcion like '%".$PrepareDQL['texto']."%'": "";
                        $DST  = array_key_exists("items", $PrepareDQL) ? " distinct t.iditem ":
                                                                    " t.iditem,t.idatributo,t.id"
                                                                  . ",i.descripcion as item,a.descripcion as atributo";
                        $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                        $DSQL = "SELECT ".$DST
                          . " FROM inventario.item_atributo t"
                          . " INNER JOIN inventario.item i ON t.iditem=i.id"
                          . " INNER JOIN inventario.atributo a ON t.idatributo=a.id".$IDD.$TXT
                          . " ORDER BY t.iditem".$LIM; 
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
              
              public function ObtenerItemsconAtributos($item)
              {      $DSQL = "SELECT i.id, i.descripcion as atributo,coalesce(t.id,0) as item"
                          . " FROM inventario.atributo i"
                          . " LEFT JOIN inventario.item_atributo t ON i.id = t.idatributo and t.iditem = ".$item
                          . " ORDER BY i.id "; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function VerificaAtributos($item,$atributo)
              {      $DSQL = "SELECT i.id"
                          . " FROM inventario.item_atributo i"
                          . " INNER JOIN inventario.producto p ON i.iditem = p.iditem"
                          . " INNER JOIN inventario.producto_atributo_valor a ON a.idproducto = p.id"
                          . " INNER JOIN inventario.atributo_valor v ON v.id = a.idatributo_valor and v.idatributo=".$atributo
                          . " WHERE i.iditem=".$item; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaItemAtributo($Form)
              {     try{ 
                            $Item=0;
                            $id=0;
                            foreach ($Form as $datos=>$informacion){ 
                                switch (substr($datos, 0,3)){
                                    case 'chk':    
                                            $atributo = $informacion;     
                                            $Rsid =$this->InsertarDatos($Item,$atributo);
                                            $id = $this->Funciones->FieldDataByName($Rsid,"iditem");
                                        break;
                                    case 'Ite':                                        
                                            $Item = $informacion;  
                                            $this->EliminaDatos($Item);
                                        break;
                                }
                            } 
                            return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear la relacion..';
                    }                     
              }
    
              public function ActualizaItemAtributo($Form)
              {      try{ 
                            $Item=0;
                            $id=0;
                            $valida ='';
                            $banElimina=0;
                            foreach ($Form as $datos=>$informacion){ 
                                switch (substr($datos, 0,3)){
                                    case 'chk':    
                                            $atributo = $informacion; 
                                            $verifica = $this->VerificaAtributos($Item,$atributo);
                                            if(count($verifica)>0){
                                                $valida ='Un producto tiene asignada esta relacion';
                                                break;
                                            }else{
                                                if($banElimina==0){ $this->EliminaDatos($Item); $banElimina=1; }
                                                $Rsid =$this->InsertarDatos($Item,$atributo);
                                            }
                                        break;
                                    case 'Ite':                                        
                                            $Item = $informacion;                                            
                                        break;
                                }
                            } 
                            if(trim($valida) == ''){
                                return intval($Item);
                            }else{
                                return $valida;
                            }
                            
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear la relacion..';
                    }    
              }
              
              public function InsertarDatos($item,$atributo)
              {      $Query = "INSERT INTO inventario.item_atributo (iditem,idatributo) VALUES (".$item.",".$atributo.")";
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
              }
              
              public function EliminaDatos($item)
              {      $Query = "DELETE FROM inventario.item_atributo WHERE iditem= ".$item; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
              }

              public function ObtenerDatosGeneral($id=0)
                {     if($id === 0 || $id === ''){
                          $Datos = $this->ObtenerItemAtributo(array('items' => 1));
                          if(count($Datos)>0){
                            foreach ($Datos as $atritem){                                                        
                                $hijos = $this->ObtenerItemAtributo(array('iditem' => $atritem['iditem'])); 
                                foreach ($hijos as $hijosatritem){ 
                                  array_push($this->padre,$hijosatritem);
                                }
                            }  
                          }
                      }else{
                          $Datos = $this->ObtenerItemAtributo($id);
                          foreach ($Datos as $hijosatritem){ 
                                array_push($this->padre,$hijosatritem);
                              }
                      }
                    return $this->padre;
              }
              
                public function ObtenerAtributosxItem($item)
                {       $DSQL = "select ia.id, ia.iditem, ia.idatributo, a.descripcion ";
                        $DSQL.= "from inventario.item_atributo as ia, inventario.atributo as a ";
                        $DSQL.= "where a.id = ia.idatributo and ia.iditem = $item ";
                        $DSQL.= "order by ia.id asc ";
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
              
        }
