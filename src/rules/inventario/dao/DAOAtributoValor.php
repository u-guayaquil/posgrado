<?php
        require_once("src/base/DBDAO.php");

        class DAOAtributoValor extends DBDAO
        {     private static $entityAtributoValor;
              
              public function __construct()
              {      self::$entityAtributoValor = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityAtributoValor->MyFunction();
                     $this->Fecha  = new DateControl();
              }
    
              public function ObtenerAtributoValor($PrepareDQL) 
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and a.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(a.descripcion) LIKE '%".$PrepareDQL['texto']."%' "
                                                                        . "OR upper(t.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and a.id =".$PrepareDQL['id']: "";
                     $ATR  = array_key_exists("atributo", $PrepareDQL) ? " WHERE a.idatributo =".$PrepareDQL['atributo']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT a.id,a.descripcion,e.descripcion as txtestado,a.fecreacion,e.id as idestado,t.descripcion as atributo,a.idatributo,a.codigo"
                          . " FROM inventario.atributo_valor a"
                          . " INNER JOIN general.Estado e ON a.idestado=e.id"
                          . " INNER JOIN inventario.atributo t ON a.idatributo=t.id ".$JOIN.$LIKE.$IDD.$ATR
                          . " ORDER BY a.descripcion".$LIM;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaAtributoValor($datos)
              {     try{        
                        $estado = $datos->estado;
                        $atributo = $datos->atributo;
						$codigo = strtoupper($datos->codigo);
                        $descripcion = strtoupper($datos->descripcion);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO inventario.atributo_valor (descripcion,idatributo,codigo,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', ".$atributo.",'".$codigo."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear la Relacion..';
                    }
              }
    
              public function ActualizaAtributoValor($datos)
              {      try{ 
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $atributo = $datos->atributo;
						$codigo = strtoupper($datos->codigo);
                        $descripcion = strtoupper($datos->descripcion);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE inventario.atributo_valor "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.", idatributo= ".$atributo
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."', codigo= '".$codigo."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaAtributoValor($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE inventario.atributo_valor SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                     } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar la Relación..';
                     }
              }
              
              
            public function ObtenerAtributoValorByAtribID($IdAtributo)
            {       $DSQL = "select av.id, av.descripcion ";
                    $DSQL.= "from inventario.atributo as at, inventario.atributo_valor as av ";
                    $DSQL.= "where at.id=av.idatributo and at.id=$IdAtributo ";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            public function ObtenerAtributoValorByProductoAtributoID($IdProducto,$IdAtributo)
            {       $DSQL = "select av.id, av.descripcion, pav.idproducto, pav.idatributo_valor ";
                    $DSQL.= "from inventario.atributo_valor as av left join inventario.producto_atributo_valor as pav ";
                    $DSQL.= "on av.id=pav.idatributo_valor and pav.idproducto=$IdProducto ";
                    $DSQL.= "where av.idatributo=$IdAtributo  ";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
        }
