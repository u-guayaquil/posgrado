<?php
        require_once("src/base/DBDAO.php");

        class DAOBodega extends DBDAO
        {       private static $entityBodega;
                private $Funciones;
                private $Fecha;
              
                public function __construct()
                {      self::$entityBodega = DBDAO::getConnection('PgProvider');
                       $this->Funciones = self::$entityBodega->MyFunction();
                       $this->Fecha  = new DateControl();
                }

                public function ObtenerBodega($PrepareDQL)
                {       $JOIN = array_key_exists("estado", $PrepareDQL) ? " and b.idestado in (".$PrepareDQL['estado'].")" : "";
                        $IDD  = array_key_exists("id", $PrepareDQL) ? " and b.id =".$PrepareDQL['id']: "";
                        $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                        $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(b.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";

                        $DSQL = "SELECT b.id,b.idtipobodega as idxtipobog,b.idsucursal as idxsucursal,b.descripcion,b.codigo"
                              . ", b.fecreacion, b.idestado as idxestado,e.descripcion as txtestado"
                              . ", t.descripcion as txttipobog, s.descripcion as txtsucursal, coalesce(p.id,0) as idpadre, coalesce(p.descripcion,'_') as txpadre"
                              . " FROM inventario.bodega b "
                              . " LEFT JOIN inventario.bodega p ON b.idbodegapadre = p.id "
                              . " INNER JOIN general.estado e ON b.idestado = e.id"
                              . " INNER JOIN inventario.tipo_bodega t ON b.idtipobodega = t.id"
                              . " INNER JOIN general.sucursal s ON b.idsucursal = s.id".$JOIN.$IDD.$TXT
                              . " ORDER BY b.id".$LIM;
                        
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));     
                }

                public function InsertaBodega($datos)
                {       try{
                            $estado = $datos->estado;
                            $idtipobodega = $datos->idtipobodega;
                            $idsucursal = $datos->idsucursal;
                            $codigo = $datos->codigo;
                            $descripcion = strtoupper($datos->descripcion);
                            $idpadre = intval($datos->idpadre);
                            $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                            
                            $Query = "INSERT INTO inventario.bodega (descripcion,idtipobodega,idsucursal,codigo,idestado,iduscreacion,fecreacion,idbodegapadre) "
                                   . "VALUES ('".$descripcion."',".$idtipobodega.", ".$idsucursal.", '".$codigo."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."','$idpadre') returning id";
                            
                            return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                        } 
                        catch (Exception $ex) 
                        {   $this->manejadorExcepciones($ex);
                            return 'No se pudo crear la bodega.';
                        }
                }

                public function ActualizaBodega($datos)
                {       try{
                            $id = intval($datos->id);
                            $estado = $datos->estado;
                            $idtipobodega = $datos->idtipobodega;
                            $idsucursal = $datos->idsucursal;
                            $codigo = $datos->codigo;
                            $descripcion = strtoupper($datos->descripcion);
                            $idpadre = intval($datos->idpadre);
                            $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                            
                            $Query = "UPDATE inventario.bodega "
                                    . "SET idbodegapadre='$idpadre', descripcion = '".$descripcion."', idtipobodega= ".$idtipobodega.",idsucursal = ".$idsucursal.","
                                    . "idestado= ".$estado.", codigo= '".$codigo."', idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                                    . "WHERE id= ".$id;
                            $this->Funciones->Query($Query);
                            return $id;
                        } 
                        catch (Exception $ex) 
                        {   $this->manejadorExcepciones($ex);
                            return 'No se pudo actualizar los datos de la bodega.';
                        }  
                }
                
                public function DesactivaBodega($id)
                {       try
                        {
                            $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                            $Query = "UPDATE inventario.bodega SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                            $this->Funciones->Query($Query);
                            return intval($id);
                        } 
                        catch (Exception $ex) 
                        {   $this->manejadorExcepciones($ex);
                            return 'No se pudo eliminar la bodega.';
                        }
                }
                
        }        
?>