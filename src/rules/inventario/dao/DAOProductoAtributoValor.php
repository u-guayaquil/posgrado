<?php

    require_once("src/base/DBDAO.php");

    class DAOProductoAtributoValor extends DBDAO
    {       private static $entityProductoAtributoValor;
        
            public function __construct()
            {      self::$entityProductoAtributoValor = DBDAO::getConnection('PgProvider');
                   $this->Works = self::$entityProductoAtributoValor->MyFunction();
                   $this->Fecha  = new DateControl();            
            }
    
            public function ObtenerObjetoProductoAtributoValor($id)
            {      return  $this->entityProductoAtributoValor->findOneById($id);
            } 
        
            public function ObtenerProductoAtributoValorByProducto($idproducto)
            {       $SDQL = "SELECT x.id, y.id as idatribv, y.descripcion as txatribv, z.id as idatrib, z.descripcion as txatrib "
                           ."FROM   inventario.producto_atributo_valor x, inventario.atributo_valor y, inventario.atributo as z "
                           ."WHERE  x.idproducto=$idproducto and x.idatributo_valor=y.id and y.idatributo=z.id ";
                    return $this->Works->RecorreDatos($this->Works->Query($SDQL));  
            }
            
            public function EliminaProductoAtributoValor($idproducto)
            {      $resourceID = $this->Works->Query("delete from inventario.producto_atributo_valor where idproducto=$idproducto");    
                   return true;                                                        
            }
        
            public function InsertaProductoAtributoValor($prdID,$atrID)
            {      $resourceID = $this->Works->Query("insert into inventario.producto_atributo_valor(idproducto,idatributo_valor,idestado) values('$prdID','$atrID',1) ");    
                   return true;                                                                        
            }
       
}

