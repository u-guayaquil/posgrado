<?php
        require_once("src/base/DBDAO.php");
        
        class DAORolModulo extends DBDAO
        {       private static $entityRolModulo;
                private $Works;                      
        
                public function __construct()
                {      self::$entityRolModulo = DBDAO::getConnection();
                       $this->Works = self::$entityRolModulo->MyFunction();                
                }
                
                public function ObtenerRolesByModulo()
                {       $Modulo = json_decode(Session::getValue('Sesion'));  
                        $SQL = "SELECT Y.ID,Y.DESCRIPCION FROM BdCert..SEG_MODULO_ROL as X,BdCert..SEG_ROL as Y WHERE X.IDMODULO='$Modulo->Idmodulo' and X.IDROL=Y.ID"; 
                      
                        $resourceID = $this->Works->Query($SQL); 
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existe roles asociados al modulo actual.");
                }
                
                /*
                public function ObtenerRolModuloByUserModuloID($id,$idmod)
                {       $DSQL = "SELECT X.ID, X.IDPERFIL, X.FEDESDE, X.FEHASTA
                                 FROM BdCert..SEG_USUARIO_PERFIL AS X, BdCert..SEG_PERFIL AS Y
                                 WHERE X.IDUSROL='$id' AND Y.ID=X.IDPERFIL AND Y.IDMODULO='$idmod' AND Y.IDESTADO=1 ";  //AND Y.IDMODULO=
                   
                        $resourceID = $this->Works->Query($DSQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"No tiene perfiles definidos."); 
                }
                
                public function CreaRolModuloDefault($Login)
                {       $DataCount = 0;
                        $DSQL = "SELECT P.ID FROM BdCert..SEG_PERFIL AS P 
                                 WHERE P.IDDEFAULTBYROL='".$Login['rol']."' AND P.IDESTADO=1 AND P.IDMODULO='".$Login['modulo']."' AND 
                                       P.ID NOT IN (SELECT UP.IDPERFIL FROM BdCert..SEG_USUARIO_PERFIL AS UP WHERE UP.IDUSROL='".$Login['id']."')";
                        $QueryID = $this->Works->Query($DSQL);
                        while($Perfil = $this->Works->NextRecordAssoc($QueryID))
                        {     $InsertID   = $this->Works->Query("INSERT INTO BdCert..SEG_USUARIO_PERFIL (IDUSROL,IDPERFIL,FEDESDE,USCREACION,FECREACION) OUTPUT INSERTED.ID VALUES('".$Login['id']."','".$Perfil["ID"]."',getDate(),'".$Login['id']."',getDate())");    
                              $DataFill[] = $this->Works->FieldDataByName($InsertID,'ID');
                              $DataCount++;  
                        }
                        $this->Works->CloseResource($QueryID);
                        
                        $Result = ($DataCount>0);
                        return array($Result,($Result ? $DataFill : "No existen Perfiles por Default para su Rol."));
                }
                
                
                /*
                public function ObtenerRolModuloByID($id)
                {       $SQL = "SELECT up.id, u.id idxusuario, u.usuario txtusuario, p.id as idxperfil, p.descripcion txtperfil , up.fedesde, up.fehasta, u.idempleado, e.nombre, e.apellido ";
                        $SQL.= "FROM sistema.usuarioperfil as up, sistema.perfil as p, sistema.usuario as u, general.empleado as e ";
                        $SQL.= "WHERE up.idusuario=u.id and up.idperfil=p.id and u.idempleado=e.id and up.id='$id' ";
                        $SQL.= "ORDER BY upper(u.usuario) ";
                        
                        $resourceID = $this->Works->Query($SQL);
                        $Registro = array();
                        while($Filas = $this->Works->NextRecordAssoc($resourceID))
                        {     $Registro[] = $Filas;  
                        }
                        return $Registro; 
                }
                */
                /*
                public function ObtenerByDescripcion($prepareDQL)
                {       $SQL = "SELECT up.id, u.id idxusuario, u.usuario txtusuario, p.id as idxperfil, p.descripcion txtperfil , up.fedesde, up.fehasta, u.idempleado, e.nombre, e.apellido ";
                        $SQL.= "FROM sistema.usuarioperfil as up, sistema.perfil as p, sistema.usuario as u, general.empleado as e ";
                        $SQL.= "WHERE up.idusuario=u.id and up.idperfil=p.id and u.idempleado=e.id and upper(u.usuario) like '%".$prepareDQL['texto']."%' ";
                        $SQL.= "ORDER BY upper(u.usuario) ";
                        $SQL.= "LIMIT ".$prepareDQL['limite'];
                        
                        $resourceID = $this->Works->Query($SQL);
                        $Registro = array();
                        while($Filas = $this->Works->NextRecordAssoc($resourceID))
                        {     $Registro[] = $Filas;  
                        }
                        return $Registro; 
                }*/

               /*

                public function ActualizaRolModulo($Usuarioperfil)
                {      $id = $Usuarioperfil['id']; 
                       $usuario = $Usuarioperfil['usuario'];
                       $perfils = $Usuarioperfil['perfils'];
                       $Fedesde = $Usuarioperfil['Fedesde'];
                       $Fehasta = $Usuarioperfil['Fehasta'];                       
                       $resourceID = $this->Works->Query("update sistema.usuarioperfil set idusuario='$usuario',idperfil='$perfils',fedesde='$Fedesde',fehasta='$Fehasta' where id='$id' ");    
                       return $id;
                }
                
                public function EliminaRolModulo($id)
                {      $resourceID = $this->Works->Query("Delete From sistema.usuarioperfil as up Where up.id='$id' ");    
                       return 1;                                                            
                }*/
                
        }
