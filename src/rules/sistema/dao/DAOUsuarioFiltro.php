<?php
        require_once("src/base/DBDAO.php");
        
        class DAOUsuarioFiltro extends DBDAO
        {       private static $entityUsuarioFiltro;
                private $Works;                  

                public function __construct()
                {      self::$entityUsuarioFiltro = DBDAO::getConnection();
                       $this->Works = self::$entityUsuarioFiltro->MyFunction();                
                }
            
                public function ObtenerUsuarioFiltroByUsRol($IdUsRol)
                {      $Registro = array();
                       
                       $SQL = "SELECT Y.IDFACULTAD FROM BdCert..SEG_USUARIO_ROL as X, BdCert..SEG_USUARIO_FILTRO as Y WHERE X.ID=$IdUsRol AND X.ID=Y.IDUSROL";
                       $resourceID = $this->Works->Query($SQL);                    
                       while($Filas = $this->Works->NextRecordAssoc($resourceID))
                       {     $Registro[] = $Filas['IDFACULTAD'];  
                       }
                       return (count($Registro)==0 ? array('00') : $Registro); 
                }
                
                public function ObtenerDocenteFiltroByCedula($Cedula)
                {      $Registro = array();
                
                       $SQL = "SELECT X.IDFACULTAD FROM POSTGRADO..COL_DOCENTE_FACULTAD AS X,POSTGRADO..COL_DOCENTE AS Y WHERE Y.CEDULA='$Cedula' AND Y.ID=X.IDDOCENTE";
                       $resourceID = $this->Works->Query($SQL);                    
                       while($Filas = $this->Works->NextRecordAssoc($resourceID))
                       {     $Registro[] = $Filas['IDFACULTAD'];  
                       }
                       return (count($Registro)==0 ? array('00') : $Registro); 
                }
                
        }
