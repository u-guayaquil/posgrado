<?php
        require_once("src/base/DBDAO.php");

        class DAOUsuario extends DBDAO
        {       private static $entityUsuario;
                private $Works;                  

                public function __construct()
                {      self::$entityUsuario = DBDAO::getConnection();
                       $this->Works = self::$entityUsuario->MyFunction();                
                }
                
                public function BuscarCuentaByUsuarioPasswordSIUG($User)
                {      $resourceID = $this->Works->Query("SELECT FECADUCIDAD,CONFIRMACION,IDESTADO,PASSWORD FROM BdCert..SEG_USUARIO WHERE IDUSUARIO='$User'");
                       return $this->Works->FillDataRecordAssoc($resourceID,"CMD_CREAR_CUENTA");
                }
                
                public function CreaCuentaByUsuarioPasswordSIUG($User,$Pass)
                {      $Pass = md5($Pass); 
                       $resourceID = $this->Works->Query("INSERT INTO BdCert..SEG_USUARIO(IDUSUARIO,PASSWORD,CONFIRMACION,FECADUCIDAD,IDUSCREA,FEUSCREA,IDESTADO) VALUES('$User','$Pass','S',getDate()+4,0,getDate(),1) "); 
                       if ($resourceID) 
                           return array(true,"CMD_MAIL_CUENTA");
                       else
                           return array(false,"No se puedo crear la cuenta.");
                }

                public function CrearNuevoPasswordSIUG($User,$Pass,$IP,$Email)
                {      $resource = $this->Works->Query("INSERT INTO BdCert..SEG_RECUPERACION_CLAVE(IDUSUARIO,TOKEN,CLAVE,FECADUCIDAD,IPACCESO,EMAIL,IDESTADO) OUTPUT INSERTED.ID VALUES('$User',' ','$Pass',getDate()+1,'$IP','$Email',1)"); 
                       if ($resource) 
                       {   $ID = $this->Works->FieldDataByName($resource,'ID');
                           $resourceID = $this->Works->Query("SELECT IDUSUARIO,CLAVE,FECADUCIDAD FROM BdCert..SEG_RECUPERACION_CLAVE WHERE ID = $ID");
                           $Data = $this->Works->FillDataRecordAssoc($resourceID,"No se inserto el dato.");
                           $Datos = $Data[1];
                           $FeCaducidad = $Datos[0]['FECADUCIDAD'];
                           $Token = $Datos[0]['IDUSUARIO'].utf8_encode(trim($Datos[0]['CLAVE'])).$FeCaducidad->format('d/m/Y H:i:s');
                           $Token_Cifrado = md5($Token);
                           $this->Works->Query("Update BdCert..SEG_RECUPERACION_CLAVE set 
                                                       TOKEN='$Token_Cifrado'
                                                       WHERE ID='$ID'");
                           return array(true,"CMD_MAIL_CUENTA",$Token_Cifrado);                       
                       }   
                       else
                           return array(false,"No se pudo procesar la cuenta");
                }
                
                public function ConfirmaCuentaSIUG($Usuario,$HaCaducado)
                {       if ($HaCaducado)
                        {   $resourceID = $this->Works->Query("UPDATE BdCert..SEG_USUARIO SET IDESTADO=0, IDUSMODIFICA=0, FEUSMODIFICA=getDate() WHERE IDUSUARIO='$Usuario'");
                            return array(false,"Ha caducado el tiempo de activación. Su cuenta será desactivada."); 
                        }    
                        else
                        {   $resourceID = $this->Works->Query("UPDATE BdCert..SEG_USUARIO SET FECADUCIDAD='01/01/1900', CONFIRMACION='N', IDUSMODIFICA=0, FEUSMODIFICA=getDate() WHERE IDUSUARIO='$Usuario'");
                            return array(true,"Su cuenta se confirmo con exito.");
                        }
                }
                
                public function ActualizaCuentaSIUG($Usuario)
                {       $resourceID = $this->Works->Query("UPDATE BdCert..SEG_USUARIO SET LLAVE='".md5($Usuario['clave'])."',PASSWORD='".md5($Usuario['npassword'])."',FECADUCIDAD='01/01/1900', CONFIRMACION='N', IDUSMODIFICA=".$Usuario['idusrol'].", FEUSMODIFICA=getDate() WHERE IDUSUARIO='".$Usuario['usuario']."'");
                        if ($resourceID)
                            return array(true,"Su cuenta se actualizó con exito.");
                        else
                            return array(false,"Su cuenta no se pudo actualizar.");
                }
                
                public function ObtenerDatosEstudiante($prepareDQL) //OK
                {       $RolID = (array_key_exists("rol", $prepareDQL) ? $prepareDQL['rol']:0);
                        $AdmTX = (array_key_exists("txt", $prepareDQL) ? $prepareDQL['txt']:"");
                        $Modulo = json_decode(Session::getValue('Sesion'));  

                        $SQL = "SELECT TOP 50 Y.ID,Y.IDUSUARIO AS CEDULA, Z.APELLIDOS+' '+Z.NOMBRES AS USUARIO
                                FROM BdCert..SEG_MODULO_ROL AS X, 
                                     BdCert..SEG_USUARIO_ROL AS Y, 
                                     POSTGRADO..COL_ESTUDIANTE AS Z
                                WHERE X.IDROL=$RolID and X.IDMODULO=$Modulo->Idmodulo and X.ID=Y.IDMODROL and Y.IDUSUARIO=Z.CEDULA and Y.IDESTADO=1 AND 
                                     (Z.APELLIDOS+' '+Z.NOMBRES LIKE '%".$AdmTX."%') ";
                        
                        $resourceID = $this->Works->Query($SQL); 
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen usuarios con el rol seleccionado.");
                }   
                
                public function ObtenerDatosDocente($prepareDQL)
                {       $RolID = (array_key_exists("rol", $prepareDQL) ? $prepareDQL['rol']:0);
                        $AdmTX = (array_key_exists("txt", $prepareDQL) ? $prepareDQL['txt']:"");
                        $Modulo = json_decode(Session::getValue('Sesion'));  
                
                        $SQL = "SELECT TOP 50 Y.ID,Y.IDUSUARIO AS CEDULA,Z.APELLIDOS+' '+Z.NOMBRES AS USUARIO
                                FROM BdCert..SEG_MODULO_ROL AS X, 
                                     BdCert..SEG_USUARIO_ROL AS Y, 
                                     POSTGRADO..COL_DOCENTE AS Z
                                WHERE X.IDROL=$RolID and X.IDMODULO=$Modulo->Idmodulo and X.ID=Y.IDMODROL and Y.IDUSUARIO=Z.CEDULA and Y.IDESTADO=1 AND
                                     (Z.APELLIDOS+' '+Z.NOMBRES LIKE'%".$AdmTX."%') ";  
                        
                        $resourceID = $this->Works->Query($SQL); 
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen usuarios con el rol seleccionado.");
                }
                
                public function ObtenerDatosAdministrativo($prepareDQL)
                {       $RolID = (array_key_exists("rol", $prepareDQL) ? $prepareDQL['rol']:0);
                        $AdmTX = (array_key_exists("txt", $prepareDQL) ? $prepareDQL['txt']:"");
                        $Modulo = json_decode(Session::getValue('Sesion'));  
                        
                        $SQL = "SELECT DISTINCT TOP 40 Z.ID,Z.IDUSUARIO AS CEDULA,Y.APELLIDOS+' '+Y.NOMBRES AS USUARIO
                                FROM BdCert..SEG_MODULO_ROL AS W,  
                                        ".LINKSVR."ADMISION AS X, 
                                        ".LINKSVR."DATOS_PERSONAL AS Y,
                                         BdCert..SEG_USUARIO_ROL AS Z
                                WHERE W.IDROL=$RolID and W.IDMODULO=$Modulo->Idmodulo AND W.ID=Z.IDMODROL and X.CEDULA=Z.IDUSUARIO COLLATE Modern_Spanish_BIN AND X.ESTADOREG=0 
                                      AND (UPPER(X.DESCR_CAT_CARGO)='ADMINISTRATIVO' OR UPPER(X.DESCR_CAT_CARGO)='DOCENTE') AND X.CEDULA=Y.CEDULA AND (Y.APELLIDOS+' '+Y.NOMBRES LIKE'%".$AdmTX."%') ";  
                        $resourceID = $this->Works->Query($SQL); 
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen usuarios con el rol seleccionado.");
                }
                
                public function InsertaFacultad($UsrID,$FacID) //ok
                {       $resourceID = $this->Works->Query("SELECT COUNT(IDUSROL) AS US FROM BdCert..SEG_USUARIO_FILTRO WHERE IDUSROL=$UsrID AND IDFACULTAD='$FacID'");
                        if ($this->Works->FieldDataByName($resourceID,'US')==0)
                            $SQL = "INSERT INTO BdCert..SEG_USUARIO_FILTRO (IDUSROL,IDFACULTAD) VALUES($UsrID,'$FacID')"; 
                        else
                            $SQL = "DELETE BdCert..SEG_USUARIO_FILTRO WHERE IDUSROL=$UsrID AND IDFACULTAD='$FacID'";
                        $resourceID = $this->Works->Query($SQL);
                        return array(true,"Los datos se guardaron correctamente.");
                }                
                
                public function CrearUsuarioRolByLogin($Login)
                {      $ipaccess = $Login['ipaccess'];
                       $idmodrol = $Login['rolmodulo'];
                       $idusuario= $Login['usuario'] ;
                       
                       $resourceID = $this->Works->Query("insert into BdCert..SEG_USUARIO_ROL(IDMODROL,IDUSUARIO,FEUSCREA,IPCREACION) OUTPUT INSERTED.ID VALUES('$idmodrol','$idusuario',getDate(),'$ipaccess')");
                       return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
                }
    
                function ObtenerUsuarioRolByLogin($Login)
                {       $User = $Login["usuario"];
                        $Mrol = $Login["rolmodulo"];
                        $SQLD = "SELECT * FROM BdCert..SEG_USUARIO_ROL WHERE IDUSUARIO='$User' AND IDMODROL='$Mrol'"; 
                        $resourceID = $this->Works->Query($SQLD); 
                        return $this->Works->FillDataRecordAssoc($resourceID,"CMD_CREAR_USUARIO_LOGIN");
                }

                function CambiaEstadoUsuarioRol($Estado,$IdUsRol)
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        $SQL = "UPDATE BdCert..SEG_USUARIO_ROL 
                                SET IDESTADO=$Estado,IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate() WHERE ID='$IdUsRol' ";
                        $resourceID = $this->Works->Query($SQL);     
                        if ($Estado==1)
                            return array(true,"Habilitada la gestión administrativa.");
                        else
                            return array(true,"Deshabilitada la gestión administrativa.");
                }
                
                function ObtenerUsuarioRol($Usuario,$Rol) 
                {       $Modulo = json_decode(Session::getValue('Sesion')); 
                        $SQL = "SELECT X.ID 
                                FROM BdCert..SEG_USUARIO_ROL AS X, BdCert..SEG_MODULO_ROL AS Y 
                                WHERE X.IDESTADO=1 AND X.IDUSUARIO='$Usuario' AND Y.IDROL=$Rol AND Y.IDMODULO='$Modulo->Idmodulo' AND X.IDMODROL=Y.ID";
                        $resourceID = $this->Works->Query($SQL);        
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existe el rol para el usuario.");
                }
                
        }
?>
