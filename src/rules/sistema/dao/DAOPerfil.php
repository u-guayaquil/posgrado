<?php
        require_once("src/base/DBDAO.php");

        class DAOPerfil extends DBDAO
        {   private static $entityPerfil;
            private $Works;                
            
            public function __construct()
            {      self::$entityPerfil = DBDAO::getConnection();
                   $this->Works = self::$entityPerfil->MyFunction();                            
            }
    
            public function ObtenerPerfilByID($id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));   
                    $DSQL = "SELECT TOP 50 X.ID,X.DESCRIPCION,Y.DESCRIPCION AS DEFECTO,X.IDDEFAULTBYROL AS IDDEFECTO,X.IDESTADO,Z.DESCRIPCION AS ESTADO
                             FROM BdCert..SEG_PERFIL AS X, BdCert..SEG_ROL AS Y, BdCert..GEN_ESTADO AS Z
                             WHERE X.IDMODULO='".$Modulo->Idmodulo."' AND X.IDDEFAULTBYROL=Y.ID AND X.IDESTADO=Z.ID AND X.ID='$id'"; 
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }

            public function ObtenerPerfiles()
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $SQL = "SELECT X.ID,X.DESCRIPCION,(CASE Y.DESCRIPCION WHEN 'NINGUNO' THEN '' ELSE Y.DESCRIPCION END) AS ROLAUTO
                            FROM BdCert..SEG_PERFIL as X LEFT JOIN BdCert..SEG_ROl as Y ON Y.ID=X.IDDEFAULTBYROL 
                            WHERE X.IDESTADO=1 AND X.IDMODULO=$Modulo->Idmodulo ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos perfiles.");
            }

            public function ObtenerPerfilByParams($prepareDQL) //OK
            {       $DflID = (array_key_exists("dfl", $prepareDQL) ? $prepareDQL['dfl']:0);
                    $AdmTX = (array_key_exists("txt", $prepareDQL) ? $prepareDQL['txt']:"");
                    $SQLAD = " AND (IDDEFAULTBYROL=0 OR IDDEFAULTBYROL=$DflID) ";
                    
                    $Modulo = json_decode(Session::getValue('Sesion')); 
                    $SQL = "SELECT TOP 50 X.ID,X.DESCRIPCION,(CASE Y.DESCRIPCION WHEN 'NINGUNO' THEN '' ELSE Y.DESCRIPCION END) AS ROLAUTO
                            FROM BdCert..SEG_PERFIL as X LEFT JOIN BdCert..SEG_ROl as Y ON Y.ID=X.IDDEFAULTBYROL 
                            WHERE X.IDESTADO=1 AND X.IDMODULO=$Modulo->Idmodulo ".$SQLAD." AND (X.DESCRIPCION LIKE '%".$AdmTX."%')";
                    
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos perfiles.");
            }
            
            public function ObtenerPerfilByDescripcion($PrepareDQL) //OK
            {       $Modulo = json_decode(Session::getValue('Sesion'));   
                    $DSQL = "SELECT TOP 50 X.ID,X.DESCRIPCION,Y.DESCRIPCION AS DEFECTO,X.IDDEFAULTBYROL,X.IDESTADO,Z.DESCRIPCION AS ESTADO
                             FROM BdCert..SEG_PERFIL AS X, BdCert..SEG_ROL AS Y, BdCert..GEN_ESTADO AS Z
                             WHERE X.IDMODULO='".$Modulo->Idmodulo."' AND X.IDDEFAULTBYROL=Y.ID AND X.IDESTADO=Z.ID AND 
                             X.DESCRIPCION LIKE '%".$PrepareDQL['texto']."%'";
 
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen perfiles definido."); 
            }

            public function ObtenerPerfilEstadoByID($id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));   
                    $DSQL = "SELECT TOP 50 X.ID,X.DESCRIPCION,Y.DESCRIPCION AS DEFECTO,X.IDDEFAULTBYROL,X.IDESTADO,Z.DESCRIPCION AS ESTADO
                             FROM BdCert..SEG_PERFIL AS X, BdCert..SEG_ROL AS Y, BdCert..GEN_ESTADO AS Z
                             WHERE X.ID='$id' AND X.IDMODULO='".$Modulo->Idmodulo."' AND X.IDDEFAULTBYROL=Y.ID AND X.IDESTADO=Z.ID";
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen perfiles definido."); 
            }
            
            public function InsertaPerfil($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = strtoupper($Form->descripcion);
                    $Defaul = $Form->rol;
                    $resourceID = $this->Works->Query("insert into BdCert..SEG_PERFIL(IDMODULO,DESCRIPCION,IDDEFAULTBYROL,IDUSCREA,FEUSCREA) OUTPUT INSERTED.ID values('".$Modulo->Idmodulo."','$Descrp','$Defaul','".$Modulo->Idusrol."',getDate())"); 
                    return $this->Works->FieldDataByName($resourceID,'ID');
            }
    
            public function ActualizaPerfil($perfil)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = strtoupper($perfil->descripcion);
                    $Defaul = $perfil->rol;
                    $resourceID = $this->Works->Query("Update BdCert..SEG_PERFIL set IDMODULO='".$Modulo->Idmodulo."',
                                                       DESCRIPCION='$Descrp',
                                                       IDDEFAULTBYROL='$Defaul',
                                                       IDUSMODIFICA='".$Modulo->Idusrol."',
                                                       IDESTADO='$perfil->estado',    
                                                       FEUSMODIFICA=getDate() 
                                                       WHERE ID='$perfil->id'"); 
                    return intval($perfil->id);
            }
            
            public function DesactivaPerfil($id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("update BdCert..SEG_PERFIL set IDESTADO=0, IDUSMODIFICA='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() where id='$id' ");    
                    return 0;                    
            }

        }
