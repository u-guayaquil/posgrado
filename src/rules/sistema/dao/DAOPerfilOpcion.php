<?php
        require_once("src/base/DBDAO.php");        

        class DAOPerfilOpcion extends DBDAO
        {       
                private $Works;
                private static $entityPerfilOpcion;
                
                public function __construct()
                {       self::$entityPerfilOpcion = DBDAO::getConnection();
                        $this->Works = self::$entityPerfilOpcion->MyFunction();                
                }
                
                function ObtenerObjetoPerfilOpcionByID($Id)
                {       $resourceID = $this->Works->Query("SELECT po.id, po.idperfil, po.idopcion, po.opnuevo, po.opedita, po.opinactiva, po.opimprime, po.opexporta, po.oppdf, po.idestado FROM BdCert..SEG_PERFIL_OPCION po WHERE po.id='$Id'");
                        return $this->Works->NextRecordObject($resourceID); 
                }
        
                function ObtenerOpcionesPorPerfil($perfil,$padre)
                {       $Usperf = implode(',',$perfil);
                        $Modulo = json_decode(Session::getValue('Sesion')); 
                        
                        $SQL = "SELECT distinct o.ID, o.DESCRIPCION, o.IDTIPOPCION, o.RUTA, o.ORDEN 
                                FROM BdCert..SEG_OPCION as o
                                LEFT JOIN (BdCert..SEG_PERFIL_OPCION as p 
				INNER JOIN BdCert..SEG_USUARIO_PERFIL as up ON up.ID in ($Usperf) and up.IDPERFIL=p.IDPERFIL) ON o.ID=p.IDOPCION
                                WHERE o.IDMODULO='".$Modulo->Idmodulo."' and o.IDESTADO=1 and o.IDPADRE='$padre' and (case when (p.ID is null) then (case when o.IDTIPOPCION=2 then 1 else 0 end) else 0 end)=0 
                                ORDER BY o.ORDEN "; 
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidas opciones.");
                         
                }  
            
                function ObtenerPerfilOpcionButton($perfil,$opcion)
                {       $arrayID = implode(',',$perfil);
                        $SQL = "SELECT sum(po.opnuevo)    addnew, 
                                       sum(po.opedita)    addmod, 
                                       sum(po.opinactiva) addcon, 
                                       sum(po.opimprime)  addimp, 
                                       sum(po.opexporta)  addexp, 
                                       sum(po.oppdf)      addpdf 
                                FROM BdCert..SEG_PERFIL_OPCION po, BdCert..SEG_USUARIO_PERFIL up
                                WHERE 
                                up.ID IN ($arrayID) and up.IDPERFIL = po.IDPERFIL and po.idopcion = '$opcion' "; 
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"Operaciones no habilitadas.");
                }

                function ObtenerPerfilOpcionPermisos($perfil,$padre)
                {       $Modulo = json_decode(Session::getValue('Sesion'));
                        $SQL = "SELECT o.ID, o.DESCRIPCION, o.IDTIPOPCION, po.ID IDXOPPER, po.OPNUEVO, po.OPEDITA, po.OPINACTIVA, po.OPIMPRIME, po.OPEXPORTA, po.OPPDF 
                                FROM BdCert..SEG_OPCION o LEFT JOIN BdCert..SEG_PERFIL_OPCION po ON o.ID = po.IDOPCION and po.IDPERFIL = $perfil 
                                WHERE o.IDPADRE = $padre and o.IDESTADO=1 and o.IDMODULO='$Modulo->Idmodulo'
                                ORDER BY o.ORDEN"; 
                        $resourceID = $this->Works->Query($SQL);
                        
                        $Registro = array();
                        while($Filas = $this->Works->NextRecordAssoc($resourceID))
                        {     $Registro[] = $Filas;  
                        }
                        return $Registro; 
                }   
            
                function ActualizaPerfilOpcion($Form)
                {       $id = $Form['id'];
                        $opnuevo = $Form['opnuevo'];
                        $opedita = $Form['opedita'];
                        $opinact = $Form['opinact'];
                        $opimprime = $Form['opimprime'];
                        $opexporta = $Form['opexporta'];
                        $oppdf = $Form['oppdf'];
                        $usmodifica = $Form['usmodifica'];
                        $estado = $Form['estado'];
                        $resourceID = $this->Works->Query("update BdCert..SEG_PERFIL_OPCION set opnuevo=$opnuevo,opedita=$opedita,opinactiva=$opinact,opimprime=$opimprime,opexporta=$opexporta,oppdf=$oppdf,idestado=$estado,idusmodifica=$usmodifica,femodificacion=getDate() Where id=$id");
                        return $id;
                }
                
                function InsertaPerfilOpcion($Form)
                {       $idperfil = $Form['IdPerfil'];
                        $idopcion = $Form['IdOpcion'];
                        $opnuevo = $Form['opnuevo'];
                        $opedita = $Form['opedita'];
                        $opinact = $Form['opinact'];
                        $opimprime = $Form['opimprime'];  
                        $opexporta = $Form['opexporta'];
                        $oppdf = $Form['oppdf'];                    
                        $uscreacion = $Form['uscreacion'];
                        $modificacion = $Form['modificacion'];
                        $estado = $Form['estado'];    
                        $resourceID = $this->Works->Query("Insert Into BdCert..SEG_PERFIL_OPCION (idperfil,idopcion,opnuevo,opedita,opinactiva,opimprime,opexporta,oppdf,idestado,iduscreacion,fecreacion,femodificacion) OUTPUT INSERTED.ID values('$idperfil','$idopcion','$opnuevo','$opedita','$opinact','$opimprime','$opexporta','$oppdf','$estado','$uscreacion',getDate(),'$modificacion')");                        
                        return $this->Works->FieldDataByName($resourceID,'ID');
                }

                public function ObtenerPerfilByRol($Idusrol)
                {       $SQL = "SELECT IDPERFIL FROM BdCert..SEG_USUARIO_PERFIL WHERE IDUSROL = '$Idusrol' AND IDESTADO = 1 AND IDPERFIL = 0";
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->NextRecordObject($resourceID);
                }
        }
?>        
