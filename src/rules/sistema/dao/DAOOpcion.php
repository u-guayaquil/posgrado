<?php
        require_once("src/base/DBDAO.php");
        
        class DAOOpcion extends DBDAO
        {   private static $entityOpcion;
            private $Works;  
            
            public function __construct()
            {      self::$entityOpcion = DBDAO::getConnection();
                   $this->Works = self::$entityOpcion->MyFunction();                
            }

            public function ObtenerObjetoOpcionByID($id)
            {      $Modulo = json_decode(Session::getValue('Sesion'));
                   $SQL = "SELECT o.ID,o.DESCRIPCION,o.ORDEN,o.IDTIPOPCION,o.IDMODULO,o.IDPADRE,o.IDESTADO,o.RUTA 
                           FROM BdCert..SEG_OPCION as o 
                           WHERE o.ID='$id' AND o.IDMODULO='$Modulo->Idmodulo' AND o.IDESTADO=1"; 
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->NextRecordObject($resourceID); 
            }
            
            function ObtenerOpcionesByTipo($idTipo,$idEstado=1) //ok
            {      $Filtro = ($idEstado==1 ? "and o.IDESTADO=1" : ($idEstado==0 ? "and o.IDESTADO=0" : "") );
                   $Modulo = json_decode(Session::getValue('Sesion')); 
                   $resourceID = $this->Works->Query("SELECT o.ID,o.DESCRIPCION,o.ORDEN,o.IDTIPOPCION  FROM BdCert..SEG_OPCION o WHERE o.IDMODULO='".$Modulo->Idmodulo."' and o.IDTIPOPCION='$idTipo' ".$Filtro." ORDER BY o.ORDEN");
                   return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidas opciones");
            }   

            public function ObtenerOpcionByID($Id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $SQL = "SELECT o.ID, o.DESCRIPCION, o.RUTA, o.IDTIPOPCION, o.ORDEN, o.IDESTADO, u.ID IDXPADRE, u.DESCRIPCION TXTPADRE     
                            FROM BdCert..SEG_OPCION o LEFT JOIN BdCert..SEG_OPCION u ON u.id=o.IDPADRE 
                            WHERE o.ID = '$Id' and o.IDMODULO='".$Modulo->Idmodulo."' ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidas opciones");            }

            public function ObtenerOpcionByIDPadre($Id)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
            
                    $SQL = "SELECT o.ID, o.DESCRIPCION, o.IDTIPOPCION, o.ORDEN, o.IDPADRE, o.RUTA, o.IDESTADO ";
                    $SQL.= "FROM BdCert..SEG_OPCION o ";
                    $SQL.= "WHERE o.IDPADRE='$Id' AND o.IDMODULO='".$Modulo->Idmodulo."' ";
                    $SQL.= "ORDER BY o.ORDEN ASC ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos opciones.");
            }

            public function ObtenerOpcionTipoByDescripcion($PrepareDQL)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $arrayID = implode(',',$PrepareDQL['tipo']);
                     
                    $SQL = "SELECT o.ID, o.IDTIPOPCION, o.DESCRIPCION, o.ORDEN, u.DESCRIPCION txtpadre ";
                    $SQL.= "FROM BdCert..SEG_OPCION o LEFT JOIN BdCert..SEG_OPCION u ON u.id=o.IDPADRE ";
                    $SQL.= "WHERE o.IDMODULO='$Modulo->Idmodulo' and o.IDTIPOPCION in($arrayID) and o.IDESTADO=1 and upper(o.DESCRIPCION) like '%".$PrepareDQL['texto']."%' ";
                    $SQL.= "ORDER BY o.ORDEN ASC ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidas opciones.");
            }

            public function InsertaOpcion($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Ordens = intval($Form->orden);
                    $Idtipo = intval($Form->idtipo);
                    $Descrp = strtoupper($Form->descripcion);
                    $IPadre = intval($Form->idpadre);
                    $resourceID = $this->Works->Query("INSERT INTO BdCert..SEG_OPCION (IDMODULO,DESCRIPCION,RUTA,ORDEN,IDPADRE,IDESTADO,IDUSCREA,FEUSCREA,IDTIPOPCION) OUTPUT Inserted.ID VALUES('".$Modulo->Idmodulo."','$Descrp','$Form->ruta','$Ordens','$IPadre','$Form->estado','".$Modulo->Idusrol."',getDate(),'$Idtipo')  ");    
                    return array(true,$this->Works->FieldDataByName($resourceID,'ID')); 
            }

            private function OpcionTieneHijos($id,$idmod)
            {       $resourceID = $this->Works->Query("select count(X.ID) AS EXISTE from BdCert..SEG_OPCION as X where X.IDPADRE='$id' and X.IDMODULO='".$idmod."'");    
                    return $this->Works->FieldDataByName($resourceID,'EXISTE');
            }
            
            public function ActualizaOpcion($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $id = intval($Form->id);
                    if ($this->OpcionTieneHijos($id,$Modulo->Idmodulo)>0 && $Form->idtipo==2)
                        return array(false,"La opción tiene hijos no puede cambiar su tipo.");
                    else{
                        $SQL = "Update BdCert..SEG_OPCION
                                Set descripcion='".strtoupper($Form->descripcion)."',
                                    ruta='".trim($Form->ruta)."',
                                    orden='".intval($Form->orden)."',
                                    idpadre='".intval($Form->idpadre)."',
                                    idestado='$Form->estado',
                                    idusmodifica='$Modulo->Idusrol',
                                    feusmodifica=getDate(),
                                    idtipopcion='$Form->idtipo' 
                                where id='$id'";
                        $resourceID = $this->Works->Query($SQL);    
                        return array(true,intval($Form->id));   
                    }    
            }

            public function DesactivaOpcion($id)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $resourceID = $this->Works->Query("Update BdCert..SEG_OPCION Set IDESTADO=0,IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate() Where id='$id'");    
                    return true;                                        
            }
            
        }
?>        
