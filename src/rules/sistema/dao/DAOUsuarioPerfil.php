<?php
        require_once("src/base/DBDAO.php");
        
        class DAOUsuarioPerfil extends DBDAO
        {       private static $entityUsuarioPerfil;
                private $Works;                      
        
                public function __construct()
                {      self::$entityUsuarioPerfil = DBDAO::getConnection();
                       $this->Works = self::$entityUsuarioPerfil->MyFunction();                
                }
                
                public function ObtenerUsuarioPerfilByUserModuloID($id,$idmod)
                {       $DSQL = "SELECT X.ID, X.IDPERFIL, X.FEDESDE, X.FEHASTA
                                 FROM BdCert..SEG_USUARIO_PERFIL AS X, BdCert..SEG_PERFIL AS Y
                                 WHERE X.IDESTADO=1 AND X.IDUSROL='$id' AND Y.ID=X.IDPERFIL AND Y.IDMODULO='$idmod' AND Y.IDESTADO=1 ";
                        $resourceID = $this->Works->Query($DSQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"No tiene perfiles definidos."); 
                }
                
                public function CreaUsuarioPerfilDefault($Login) //OK
                {       $DataCount = 0;
                        $DSQL = "SELECT P.ID FROM BdCert..SEG_PERFIL AS P 
                                 WHERE P.IDDEFAULTBYROL='".$Login['rol']."' AND P.IDESTADO=1 AND P.IDMODULO='".$Login['modulo']."' AND 
                                       P.ID NOT IN (SELECT UP.IDPERFIL FROM BdCert..SEG_USUARIO_PERFIL AS UP WHERE UP.IDUSROL='".$Login['id']."')";
                        $QueryID = $this->Works->Query($DSQL);
                        while($Perfil = $this->Works->NextRecordAssoc($QueryID))
                        {     $InsertID   = $this->Works->Query("INSERT INTO BdCert..SEG_USUARIO_PERFIL (IDUSROL,IDPERFIL,FEDESDE,USCREACION,FECREACION) OUTPUT INSERTED.ID VALUES('".$Login['id']."','".$Perfil["ID"]."',getDate(),'".$Login['id']."',getDate())");    
                              $DataFill[] = $this->Works->FieldDataByName($InsertID,'ID');
                              $DataCount++;  
                        }
                        $this->Works->CloseResource($QueryID);

                        $IDX = $this->CrearUsuarioPerfilEspecial($Login); 
                        if ($IDX!=0)
                        {   $DataFill[] = $IDX;
                            $DataCount++;
                        }

                        $Result = ($DataCount>0);
                        return array($Result,($Result ? $DataFill : "No existen Perfiles por Default para su Rol."));
                }

                private function CrearUsuarioPerfilEspecial($Login)
                {       if ($Login['rol']==2)
                        {   if ($Login['modulo']==2)
                            {   $DSQL = "SELECT Top 1 
                                                convert(char(10),GETDATE(),120) DESDE, 
                                                convert(char(10),DATEADD(DAY,-1,X.FEINICIO),120) HASTA
                                         FROM   POSTGRADO..COL_CICLOS_MATERIA AS X, POSTGRADO..COL_DOCENTE AS Y
                                         WHERE  DATEDIFF(d,GETDATE(),X.FEINICIO)>0 AND X.IDESTADO=1 AND X.IDDOCENTE=Y.ID AND Y.CEDULA='".$Login['usuario']."' AND Y.IDESTADO=1
                                         ORDER BY X.FEINICIO ASC";
                            
                                $QUID = $this->Works->Query($DSQL);
                                while($Calendario = $this->Works->NextRecordAssoc($QUID))
                                {     $SQID = $this->Works->Query("SELECT ID,convert(char(10),FEHASTA,120) HASTA,IDPERFIL FROM BdCert..SEG_USUARIO_PERFIL WHERE IDPERFIL=3 AND IDUSROL='".$Login['id']."' AND IDESTADO=1");
                                      while($Perfil = $this->Works->NextRecordAssoc($SQID))
                                      {   //if (strtotime($Perfil['HASTA']) < strtotime($Calendario['HASTA']))
                                            $this->Works->Query("UPDATE BdCert..SEG_USUARIO_PERFIL SET FEHASTA='".$Calendario['HASTA']."', FEMODIFICA=getDate(), USMODIFICA='".$Login['id']."' WHERE ID='".$Perfil['ID']."'");    
                                            return 0;
                                      }
                                      $InsertID   = $this->Works->Query("INSERT INTO BdCert..SEG_USUARIO_PERFIL (IDUSROL,IDPERFIL,FEDESDE,FEHASTA,USCREACION,FECREACION) OUTPUT INSERTED.ID VALUES('".$Login['id']."',3,'".$Calendario["DESDE"]."','".$Calendario["HASTA"]."','".$Login['id']."',getDate())");    
                                      return $this->Works->FieldDataByName($InsertID,'ID');
                                      $this->Works->CloseResource($SQID);
                                }
                                $this->Works->CloseResource($QUID);
                            }    
                        }
                        return 0;
                }

                public function ObtenerUsuarioPerfilByID($idusrol) //Ok
                {       $SQL = "SELECT X.ID,X.IDPERFIL,Y.DESCRIPCION,X.FEDESDE,X.FEHASTA,X.IDESTADO  
                                FROM BdCert..SEG_USUARIO_PERFIL AS X, BdCert..SEG_PERFIL AS Y
                                WHERE X.IDUSROL=$idusrol AND X.IDPERFIL=Y.ID AND Y.IDESTADO=1"; 
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"El usuario no tiene perfiles definidos."); 
                }

                public function InsertaUsuarioPerfil($Datos) //OK
                {      $Modulo = json_decode(Session::getValue('Sesion'));
                       $SQL = "INSERT INTO BdCert..SEG_USUARIO_PERFIL (IDUSROL,IDPERFIL,FEDESDE,FEHASTA,USCREACION,FECREACION,IDESTADO) OUTPUT INSERTED.ID ";
                       $SQL.= "VALUES('$Datos->idusuario','$Datos->idperfil','$Datos->desde','$Datos->hasta','$Modulo->Idusrol',getDate(),1)"; 
                       $resourceID = $this->Works->Query($SQL);
                       if ($resourceID)
                           return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
                       else
                           return array(false,"No se puedo guardar la información.");
                }
                
                public function ActualizaUsuarioPerfil($Datos) //OK
                {       $Modulo = json_decode(Session::getValue('Sesion'));
                        $SQL = "UPDATE BdCert..SEG_USUARIO_PERFIL SET IDPERFIL='$Datos->idperfil',FEDESDE='$Datos->desde',FEHASTA='$Datos->hasta',IDESTADO='$Datos->estado',USMODIFICA='$Modulo->Idusrol',FEMODIFICA=getDate() ";
                        $SQL.= "WHERE ID=$Datos->id AND IDUSROL='$Datos->idusuario'"; 
                        $resourceID = $this->Works->Query($SQL);
                        if ($resourceID)
                            return array(true,$Datos->id);
                        else
                            return array(false,"No se puedo guardar la información.");
                }
                
                public function Elimina($id) //OK
                {       $Modulo = json_decode(Session::getValue('Sesion'));
                        $resourceID = $this->Works->Query("UPDATE BdCert..SEG_USUARIO_PERFIL SET IDESTADO=0,USMODIFICA='$Modulo->Idusrol',FEMODIFICA=getDate()  WHERE ID='$id' ");    
                        if ($resourceID) 
                            return array(true,$id);  
                        else
                            return array(false,"No se puedo desactivar el registro");
                }
                
        }
