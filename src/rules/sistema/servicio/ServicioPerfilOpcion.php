<?php
    require_once("src/rules/sistema/dao/DAOPerfil.php");
    require_once("src/rules/sistema/dao/DAOOpcion.php");
    require_once("src/rules/sistema/dao/DAOPerfilOpcion.php");

    class ServicioPerfilOpcion
    {       private $DAOOpcion; 
            private $DAOPerfilOpcion; 
            private $Fechas;
           
            function __construct()
            {       $this->DAOOpcion = new DAOOpcion();
                    $this->DAOPerfilOpcion = new DAOPerfilOpcion();
                    $this->Fechas = new DateControl();
            }

            function ObtenerMenu()
            {       $perfil = Session::getValue('Perfil');
                    
                    if (is_array($perfil))
                    {   $menu = '';
                        $root = $this->DAOOpcion->ObtenerOpcionesByTipo(0);
                        if ($root[0])
                        {   foreach ($root[1] as $sheet)
                            {       $child = $this->ObtenerMenuSgte($perfil,$sheet['ID']); 
                                    if (is_array($child))
                                    {   $descripcion = strtolower(trim($sheet['DESCRIPCION']));
                                        $menu[] = array('M',$sheet['ID'],ucwords($descripcion),$child);
                                    }
                            }
                            return json_encode($menu);
                        }
                        return $menu;
                    }    
            }

            private function ObtenerMenuSgte($perfil,$padre) 
            {       $subm = ''; 
                    $root = $this->DAOPerfilOpcion->ObtenerOpcionesPorPerfil($perfil,$padre);
                    
                    if ($root[0])    
                    {   foreach ($root[1] as $sheet)
                        {       
                            if ($sheet['IDTIPOPCION']==2)
                            {   $descripcion = strtolower(trim($sheet['DESCRIPCION']));
                                $ruta = trim($sheet['RUTA']);    
                                $subm[] = array('O',$sheet['ID'],ucwords($descripcion),$ruta);
                            }    
                            else
                            {   $child = $this->ObtenerMenuSgte($perfil,$sheet['ID']);
                                if (is_array($child))
                                {   $descripcion = strtolower(trim($sheet['DESCRIPCION']));
                                    $subm[] = array('S',$sheet['ID'],ucwords($descripcion),$child);
                                }
                            }
                        }
                    }    
                    return $subm;
            }
            
            public function ObtenerPermisosPorOpcion($opcion)
            {      return $this->DAOPerfilOpcion->ObtenerPerfilOpcionButton(Session::getValue('Perfil'),$opcion);
            }
            
            public  function BuscarPerfilOpcionPermisos($perfil = 0,$Menu = 1)
            {       $menus = array();
                    $level = 0;
                    $sheet = $this->DAOOpcion->ObtenerObjetoOpcionByID($Menu);
                    if ($sheet)
                    {       $descripcion = trim($sheet->DESCRIPCION);
                            $child = $this->BuscarPerfilOpcionPermisosSgte($level+1,$perfil,$sheet->ID); 
                            $menus[] = array('nivel' => $level,'id' => $sheet->ID,'idtipo' => $sheet->IDTIPOPCION,'descripcion' => $descripcion,'hijos' => $child,'boton' => array());
                    }
                    return json_encode($menus);
            }

            private function BuscarPerfilOpcionPermisosSgte($level,$perfil,$padre) 
            {       $subms = array();
                    $roots = $this->DAOPerfilOpcion->ObtenerPerfilOpcionPermisos($perfil,$padre);
                    foreach ($roots as $sheet)
                    {       $descripcion = trim($sheet['DESCRIPCION']);
                            $child = array();
                            $boton = array();
                            if ($sheet['IDTIPOPCION']==2){
                                $boton = array($sheet['IDXOPPER'],$sheet['OPNUEVO'],$sheet['OPEDITA'],$sheet['OPINACTIVA'],$sheet['OPIMPRIME'],$sheet['OPEXPORTA'],$sheet['OPPDF']);
                            }else{   
                                $child = $this->BuscarPerfilOpcionPermisosSgte($level+1,$perfil,$sheet['ID']);
                            }
                            $subms[] = array('nivel' => $level,'id' => $sheet['ID'], 'idtipo' => $sheet['IDTIPOPCION'],'descripcion' => $descripcion,'hijos' => $child,'boton' => $boton);                                        
                    }
                    return $subms;
            }
            
            function EditaPerfilOpcion($Forma)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $perfOpcion = $this->DAOPerfilOpcion->ObtenerObjetoPerfilOpcionByID($Forma->Id);
                    
                    $Opnuevo =   ($Forma->IdBoton==4 ? $Forma->Value : $perfOpcion->opnuevo);
                    $Opedita =   ($Forma->IdBoton==5 ? $Forma->Value : $perfOpcion->opedita);
                    $Opinactiva =($Forma->IdBoton==6 ? $Forma->Value : $perfOpcion->opinactiva);
                    $Opimprime = ($Forma->IdBoton==7 ? $Forma->Value : $perfOpcion->opimprime); 
                    $Opexporta = ($Forma->IdBoton==8 ? $Forma->Value : $perfOpcion->opexporta);
                    $Oppdf =     ($Forma->IdBoton==9 ? $Forma->Value : $perfOpcion->oppdf);       
                    $Optotal = $Opnuevo+$Opedita+$Opinactiva+$Opimprime+$Opexporta+$Oppdf;
                    
                    $Editar['id']= $Forma->Id;
                    $Editar['opnuevo']= $Opnuevo;
                    $Editar['opedita']= $Opedita;
                    $Editar['opinact']= $Opinactiva;
                    $Editar['opimprime']= $Opimprime;  
                    $Editar['opexporta']= $Opexporta;
                    $Editar['oppdf']= $Oppdf;                    
                    $Editar['usmodifica']= $Modulo->Idusrol;
                    $Editar['estado']= ($Optotal==0 ? 0:1);
                    
                    return $this->DAOPerfilOpcion->ActualizaPerfilOpcion($Editar);
            }
            
            function CreaPerfilOpcion($Forma)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $Editar['IdPerfil']= $Forma->IdPerfil;
                    $Editar['IdOpcion']= $Forma->IdOpcion;
                    $Editar['opnuevo']= ($Forma->IdBoton==4 ? 1:0);
                    $Editar['opedita']= ($Forma->IdBoton==5 ? 1:0);
                    $Editar['opinact']= ($Forma->IdBoton==6 ? 1:0);
                    $Editar['opimprime']= ($Forma->IdBoton==7 ? 1:0);  
                    $Editar['opexporta']= ($Forma->IdBoton==8 ? 1:0);
                    $Editar['oppdf']= ($Forma->IdBoton==9 ? 1:0);                    
                    $Editar['uscreacion']= $Modulo->Idusrol;
                    $Editar['modificacion']= '1900-01-01 00:00:00';
                    $Editar['estado']= 1;
                    return $this->DAOPerfilOpcion->InsertaPerfilOpcion($Editar);
            }

            function ConsultarPerfilByRol($Idusrol)
            {       return $this->DAOPerfilOpcion->ObtenerPerfilByRol($Idusrol);
            }
     }
?>
