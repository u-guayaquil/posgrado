<?php  
        require_once("src/rules/sistema/dao/DAOUsuarioPerfil.php");

        class ServicioUsuarioPerfil 
        {       private $DAOUsuarioPerfil;
                
                function __construct()
                {       $this->DAOUsuarioPerfil = new DAOUsuarioPerfil();
                        $this->Fechas = new DateControl();
                }
               
                function BuscarUsuarioPerfilByID($idusrol) //ok
                {       return $this->DAOUsuarioPerfil->ObtenerUsuarioPerfilByID(intval($idusrol));
                }
                
                function GuardaDB($Datos)
                {       $Resp = $this->ValidaFecha($Datos->desde,$Datos->hasta);
                        if ($Resp[0])
                        {   $Datos->hasta = $Resp[1]; 
                            if (empty($Datos->id)){
                                return $this->DAOUsuarioPerfil->InsertaUsuarioPerfil($Datos);
                            }else{
                                return $this->DAOUsuarioPerfil->ActualizaUsuarioPerfil($Datos);
                            }
                        }
                        return $Resp;
                }
                    
                function Elimina($id) //Ok
                {       return $this->DAOUsuarioPerfil->Elimina(intval($id));
                }
                
                private function ValidaFecha($desde,$hasta) //OK
                {       if (strlen($desde))
                        {   $hasta = (strlen($hasta)==0 ? "01/01/1900" : $hasta);    
                            if ($hasta!="01/01/1900")
                            {   $fedesde = $this->Fechas->changeFormatDate($desde, "YMD")[1];
                                $fehasta = $this->Fechas->changeFormatDate($hasta, "YMD")[1];
                                if (strtotime($fehasta) >= strtotime($fedesde)){
                                    return array(true,$hasta); 
                                }else{ 
                                    return array(false,"La fecha de hasta debe ser mayor o igual a la desde.");
                                }    
                            }
                            return array(true,$hasta);
                        }
                        return array(false,"La fecha desde no puede ser vacia.");
                }        
        }
?>