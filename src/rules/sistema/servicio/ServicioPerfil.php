<?php
            require_once("src/rules/sistema/dao/DAOPerfil.php");
            class ServicioPerfil 
            {       private $DAOPerfil;
     
                    function __construct()
                    {        $this->DAOPerfil = new DAOPerfil();
                    }
                    
                    function GuardaDBPerfil($Perfil)
                    {       if (empty($Perfil->id)){
                                return $this->CreaPerfil($Perfil);
                            }else{
                                return $this->EditaPerfil($Perfil);
                            }
                    }
                    
                    function CreaPerfil($Form)
                    {       return $this->DAOPerfil->InsertaPerfil($Form);
                    }
           
                    function EditaPerfil($Form)
                    {       return $this->DAOPerfil->ActualizaPerfil($Form);
                    }
                    
                    function DesactivaPerfil($id)
                    {       return $this->DAOPerfil->DesactivaPerfil(intval($id));
                    }
                    
                    function BuscarPerfilByID($id)
                    {       return $this->DAOPerfil->ObtenerPerfilByID(intval($id));
                    }

                    function BuscarPerfilByDescripcion($prepareDQL) //OK
                    {       return $this->DAOPerfil->ObtenerPerfilByDescripcion($prepareDQL);
                    }
                    
                    function BuscarPerfilEstadoByID($id) //ok
                    {       return $this->DAOPerfil->ObtenerPerfilEstadoByID(intval($id));
                    }
                    
                    function BuscarPerfilByParams($prepareDQL)
                    {       return $this->DAOPerfil->ObtenerPerfilByParams($prepareDQL);
                    }
            }
?>

