<?php
        require_once('src/libs/clases/Mailer.php');
        require_once("src/rules/seguridad/servicio/ServicioLogin.php");
        require_once('src/utils/Templates.php');
        require_once("src/rules/general/servicio/ServicioEmpresa.php");
        
        class ServicioUsuario
        {       private $ServicioLogin;
                private $DAOUsuario;
                
                function __construct()
                {       $this->DAOUsuario = new DAOUsuario();
                        $this->ServicioLogin = new ServicioLogin();
                }
               
                function BuscarUsuarioByParams($prepareDQL)
                {       if (array_key_exists("rol", $prepareDQL)) $RolID = $prepareDQL['rol'];
                        if ($RolID==1)
                            return $this->DAOUsuario->ObtenerDatosEstudiante($prepareDQL);   
                        elseif ($RolID==2)
                            return $this->DAOUsuario->ObtenerDatosDocente($prepareDQL);
                        elseif ($RolID==3)
                            return $this->DAOUsuario->ObtenerDatosAdministrativo($prepareDQL);
                        elseif ($RolID==4)
                        { }
                        else
                        return array(false,"Debe ingresar un rol correcto.");    
                }
  
                function GuardaFacultad($UsrID,$FacID) //ok
                {       return $this->DAOUsuario->InsertaFacultad($UsrID,$FacID);       
                }
                
                /*
                function BuscarUsuarioByID($id)
                {       return $this->DAOUsuario->ObtenerUsuarioByID(intval($id));
                }

                function BuscarUsuarioByDescripcion($prepareDQL)
                {       return $this->DAOUsuario->ObtenerUsuarioByDescripcion($prepareDQL);
                }
                
                function GuardaDBUsuario($Usuario)
                {       $Usuario->usuario = strtolower($Usuario->usuario);
                        $Usuario->caducidad = trim($Usuario->caducidad);
                        
                        if ($this->ServicioLogin->EsUsuario($Usuario->usuario))
                        {   if (empty($Usuario->id))
                            {   return $this->CreaUsuario($Usuario);
                            }
                            else
                            {   $Resp = $this->ValidaFechaCaducidad($Usuario->estado,$Usuario->caducidad);
                                if($Resp[0])
                                {   $Usuario->caducidad = $Resp[1];
                                    return $this->EditaUsuario($Usuario);
                                }
                                return $Resp[1];
                            }
                        }
                        return "Debe ingresar una cuenta de usuario correcta.";
                }
                    
                function CreaUsuario($Form)
                {       $creado = $this->Fechas->getNowDateTime('DATETIME');
                        $caduca = $this->GeneraFechaCaducidad($this->Fechas,"Y-m-d H:i:s");
                        $passwd = $this->ServicioLogin->GenerarPasswordRandom();
                        
                        $Usuario['usuario'] = $Form->usuario;
                        $Usuario['passwd']  = md5($passwd);
                        $Usuario['empledo'] = intval($Form->idempleado);
                        $Usuario['pregnta'] = 0;
                        $Usuario['caduca'] = $caduca;
                        $Usuario['hodesde'] = $Form->desde;
                        $Usuario['hohasta'] = $Form->hasta;
                        $Usuario['estado']  = $Form->estado;
                        $Usuario['usecrea'] = $_SESSION['IDUSER'];                                
                        $Usuario['creacio'] = $creado;
                        $Usuario['modific'] = '1900-01-01 00:00:00';                                
                        $id = $this->DAOUsuario->InsertaUsuario($Usuario);
                        
                        $mailresp = $this->EnviaMailBienvenidaUsuario($Form->usuario,$passwd);
                        return (is_bool($mailresp) ? $id : $mailresp);
                }
           
                function EditaUsuario($Form)
                {       $Usuario['id'] = $Form->id;
                        $Usuario['usuario'] = $Form->usuario;
                        $Usuario['emplead'] = intval($Form->idempleado);
                        $Usuario['caducad'] = $Form->caducidad;
                        $Usuario['hodesde'] = $Form->desde;
                        $Usuario['hohasta'] = $Form->hasta;    
                        $Usuario['sestado'] = $Form->estado;
                        $Usuario['usmodif'] = $_SESSION['IDUSER'];
                        $Usuario['femodif'] = $this->Fechas->getNowDateTime('DATETIME');
                        return $this->DAOUsuario->ActualizaUsuario($Usuario);
                }
                
                function DesactivaUsuario($id)
                {       return $this->DAOUsuario->DesactivaUsuario(intval($id));
                }

                function GeneraFechaCaducidad($Fecha,$Salida = "d/m/Y")
                {       if (strtoupper($Fecha->getDayName())==="VIERNES"){
                            return $Fecha->AddDayToDate($Fecha->getNowDateTime('FECHA'),3,$Salida);        
                        }else{
                            return $Fecha->AddDayToDate($Fecha->getNowDateTime('FECHA'),2,$Salida);
                        }    
                }
                
                function ValidaFechaCaducidad($estado,$caducidad)
                {       switch ($estado)
                        {       case 0: return array(true,"1900-01-01 00:00:00");
                                case 1: return $this->ValidaFechaEstadoActivo($caducidad);
                                case 2: return $this->ValidaFechaEstadoConfirmar($caducidad);
                        }
                }        
                
                private function ValidaFechaEstadoConfirmar($caducidad)
                {       if (strlen($caducidad))
                        {   $fechahoy = $this->Fechas->getNowDateTime('DATE'); 
                            $fecaduca = $this->Fechas->changeFormatDate($caducidad, "YMD")[1];
                            if (strtotime($fecaduca) >= strtotime($fechahoy)){
                                return array(true,$fecaduca); 
                            }else{ 
                                return array(false,"La fecha de caducidad debe ser mayor o igual a la actual.");
                            }    
                        }
                        return array(false,"La fecha de caducidad no puede ser vacia.");
                }

                private function ValidaFechaEstadoActivo($caducidad)
                {       if (strlen($caducidad) && $caducidad!="01/01/1900")
                        {   $fechahoy = $this->Fechas->getNowDateTime('DATE'); 
                            $fecaduca = $this->Fechas->changeFormatDate($caducidad, "YMD")[1];
                            if (strtotime($fecaduca) >= strtotime($fechahoy)){
                                return array(true,$fecaduca); 
                            }else{ 
                                return array(false,"La fecha de caducidad debe ser mayor o igual a la actual.");
                            }    
                        }
                        return array(true,"1900-01-01 00:00:00");
                }
                
                private function EnviaMailBienvenidaUsuario($User,$Pwd)
                {       $Mail = new Mailer(); 
                        $ServicioEmpresa = new ServicioEmpresa();
                        
                        $Empresa = $ServicioEmpresa->ObtenerDatosEmpresa();
                        $Respsta = Templates::Bienvenida($Empresa, $User,$Pwd);
                        
                        if (is_bool($Respsta)){
                            return "Existe un problema en el envío de este mail. Favor comunéquese con el Administrador.";
                        }else{
                            $Mail->setOperate("USUARIOS");
                            $Mail->setFromName("CARLOS");
                            $Mail->setSubject("PRUEBA");
                            $Mail->setBodyHTML($Respsta);
                            $Mail->addTo($User);
                            return $Mail->sendMail();
                        }    
                }
                */
        }

?>
