<?php

        
        require_once("src/rules/sistema/dao/DAOUsuarioFiltro.php");
        
        class ServicioUsuarioFiltro 
        {       private $DAOUsuarioFiltro;
                
                function __construct()
                {       $this->DAOUsuarioFiltro = new DAOUsuarioFiltro();
                }
               
                function BuscarUsuarioFiltroByUsRol($IdUsRol)
                {       return $this->DAOUsuarioFiltro->ObtenerUsuarioFiltroByUsRol($IdUsRol);
                }
                
                function BuscarDocenteFiltroByCedula($Cedula)
                {       return $this->DAOUsuarioFiltro->ObtenerDocenteFiltroByCedula($Cedula);
                }

        }
?>