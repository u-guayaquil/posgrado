<?php   
        require_once("src/rules/sistema/dao/DAORolModulo.php");

        class ServicioRolModulo 
        {       private $DAORolModulo;
                
                function __construct()
                {       $this->DAORolModulo = new DAORolModulo();
                }
               
                function BuscarRolesByModulo()
                {       return $this->DAORolModulo->ObtenerRolesByModulo();
                }
                /*
                function BuscarRolModuloByID($id)
                {       return $this->DAORolModulo->ObtenerRolModuloByID(intval($id));
                }

                function BuscarByDescripcion($prepareDQL)
                {       return $this->DAORolModulo->ObtenerByDescripcion($prepareDQL);
                }
                
                function GuardaDBRolModulo($RolModulo)
                {       $resp = $this->ValidaFecha($RolModulo->desde,$RolModulo->hasta);
                        if (is_bool($resp))
                        {   if (empty($RolModulo->id)){
                                return $this->CreaRolModulo($RolModulo);
                            }else{
                                return $this->EditaRolModulo($RolModulo);
                            }
                        }
                        return $resp;
                }
                    
                function CreaRolModulo($Form)
                {       $Usuario['usuario'] = $Form->idusuario;
                        $Usuario['perfils'] = $Form->perfil;
                        $Usuario['Fedesde'] = $this->Fechas->changeFormatDate($Form->desde, "YMD")[1];
                        $Usuario['Fehasta'] = $this->Fechas->changeFormatDate($Form->hasta, "YMD")[1];
                        return $this->DAORolModulo->InsertaRolModulo($Usuario);
                }
           
                function EditaRolModulo($Form)
                {       $Usuario['id'] = $Form->id;
                        $Usuario['usuario'] = $Form->idusuario;
                        $Usuario['perfils'] = $Form->perfil;
                        $Usuario['Fedesde'] = $this->Fechas->changeFormatDate($Form->desde, "YMD")[1];
                        $Usuario['Fehasta'] = $this->Fechas->changeFormatDate($Form->hasta, "YMD")[1];                        
                        return $this->DAORolModulo->ActualizaRolModulo($Usuario);
                }
                
                function EliminaRolModulo($id)
                {       return $this->DAORolModulo->EliminaRolModulo(intval($id));
                }

                function ValidaFecha($desde,$hasta)
                {       if (strlen($desde))
                        {   if (strlen($hasta))    
                            {   if ($hasta!="01/01/1900")
                                {   $fedesde = $this->Fechas->changeFormatDate($desde, "YMD")[1];
                                    $fehasta = $this->Fechas->changeFormatDate($hasta, "YMD")[1];
                                    if (strtotime($fehasta) >= strtotime($fedesde)){
                                        return true; 
                                    }else{ 
                                        return "La fecha de hasta debe ser mayor o igual a la desde.";
                                    }    
                                }
                                return true; 
                            }
                            return "La fecha hasta no puede ser vacia.";
                        }
                        return "La fecha desde no puede ser vacia.";
                } */       
        }
?>