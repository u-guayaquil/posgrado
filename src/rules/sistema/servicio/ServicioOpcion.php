<?php
            require_once("src/rules/sistema/dao/DAOOpcion.php");
            
            class ServicioOpcion 
            {       private $DAOOpcion;
     
                    function __construct()
                    {        $this->DAOOpcion = new DAOOpcion();
                    }
           
                    function GuardaDBOpcion($JsDatos)
                    {       if (empty($JsDatos->id)){
                                return $this->CreaOpcion($JsDatos);
                            }    
                            else{
                                return $this->EditaOpcion($JsDatos);
                            }
                    }
                    
                    function CreaOpcion($Form)
                    {       return $this->DAOOpcion->InsertaOpcion($Form);
                    }
           
                    function EditaOpcion($Form)
                    {       if (intval($Form->id)!=intval($Form->idpadre)){
                                return $this->DAOOpcion->ActualizaOpcion($Form);
                            }    
                            else{      
                                return "La opción no puede ser padre de si mismo.";
                            }    
                    }
                    
                    function DesactivaOpcion($id)
                    {       return $this->DAOOpcion->DesactivaOpcion(intval($id)); 
                    }
                    
                    function BuscarOpcionByID($id)
                    {       return $this->DAOOpcion->ObtenerOpcionByID(intval($id));
                    }

                    function BuscarOpcionTipoByDescripcion($prepareDQL)
                    {       return $this->DAOOpcion->ObtenerOpcionTipoByDescripcion($prepareDQL);
                    }
                    
                    function BuscarOpcionEstadoByID($id)
                    {       return $this->DAOOpcion->ObtenerOpcionEstadoByID(intval($id));
                    }
                    
                    function ObtenerArbolOpciones()
                    {       $menus = array();
                            $level = 0;
                            $roots = $this->DAOOpcion->ObtenerOpcionesByTipo(0,2);
                            foreach ($roots[1] as $sheet)
                            {       $descripcion = trim($sheet['DESCRIPCION']);
                                    $child = array();
                                    $child = $this->ObtenerArbolOpcionesSgte($level+1,$sheet['ID'],$descripcion); 

                                    $menus[] = array('nivel' => $level,'id' => $sheet['ID'],'orden' => $sheet['ORDEN'],
                                                         'idtipo' => $sheet['IDTIPOPCION'],'descripcion' => $descripcion,'ruta' => ' ',
                                                         'idpadre' => 0,'txpadre' => ' ','estado' => 1,'hijos' => $child);
                            }
                            return json_encode($menus);
                    }

                    private function ObtenerArbolOpcionesSgte($level,$idpadre,$txpadre) 
                    {       $subms = array();
                            $roots = $this->DAOOpcion->ObtenerOpcionByIDPadre($idpadre);
                            if ($roots[0])
                            {   foreach ($roots[1] as $sheet)
                                {       $descripcion = trim($sheet['DESCRIPCION']);
                                        $child = array();

                                        $rutas = ' ';
                                        if ($sheet['IDTIPOPCION']==2)
                                        {   $rutas = trim($sheet['RUTA']);    
                                        }    
                                        else
                                        {   $child = $this->ObtenerArbolOpcionesSgte($level+1,$sheet['ID'],$descripcion);
                                        }
                                        $subms[] = array('nivel' => $level,'id' => $sheet['ID'],'orden' => $sheet['ORDEN'],
                                                     'idtipo' => $sheet['IDTIPOPCION'],'descripcion' => $descripcion,'ruta' => $rutas,
                                                     'idpadre' => $idpadre,'txpadre' => $txpadre,'estado' => $sheet['IDESTADO'],'hijos' => $child);                                        
                                }
                            }    
                            return $subms;
                    }
}        
?>        
