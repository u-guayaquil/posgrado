<?php

require_once("src/base/EntidadGlobal.php");
require_once("src/rules/sistema/entidad/PerfilOpcion.php");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="perfil", schema="sistema")
 */
class Perfil extends EntidadGlobal 
{   /**
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(name="descripcion", type="string", length=20)
     */
    private $descripcion;

    /**
     * @OneToMany(targetEntity="PerfilOpcion" , mappedBy="perfil")
     */
    private $perfilOpcion;

    public function __construct() 
    {      $this->perfilOpcion = new ArrayCollection();
    }

    public function getId()
    {      return $this->id;
    }

    public function getDescripcion()
    {      return $this->descripcion;
    }

    public function setDescripcion($descripcion)
    {      $this->descripcion = strtoupper($descripcion);
    }

    function getPerfilOpcion()
    {        return $this->perfilOpcion;
    }

    function setPerfilOpcion(ArrayCollection $perfilOpcion)
    {        $this->perfilOpcion = $perfilOpcion;
    }
}
