<?php

    require_once("src/rules/sistema/entidad/Perfil.php");
    require_once("src/rules/sistema/entidad/Usuario.php");

    /**
     *
     * @Entity
     * @Table(name="usuarioperfil", schema="sistema")
     */

    class Usuarioperfil 
    {   /**
          * @Id
          * @Column(name="id", type="integer")
          * @GeneratedValue
          */
        private $id;
        
        /**
         * @ManyToOne(targetEntity="Usuario")
         * @JoinColumn(name="idusuario", referencedColumnName="id")
         */
        private $Usuarios;
        
        /**
         * @ManyToOne(targetEntity="Perfil")
         * @JoinColumn(name="idperfil", referencedColumnName="id")
         */
        private $Perfiles;
        
        /**
          * @Column(name="fedesde", type="datetime")
          */
        private $fedesde;
        
        /**
          * @Column(name="fehasta", type="datetime")
          */
        private $fehasta;

        public function getId()
        {      return $this->id; 
        }

        public function setUsuarios($usuario)
        {      $this->Usuarios = $usuario; 
        }
        public function getUsuarios()
        {      return $this->Usuarios; 
        }

        public function setPerfiles($perfil)
        {      $this->Perfiles = $perfil; 
        }
        public function getPerfiles()
        {      return $this->Perfiles; 
        }
        
        public function setFedesde($fedesde)
        {      $this->fedesde = $fedesde; 
        }
        public function getFedesde()
        {      return $this->fedesde; 
        }
        
        public function setFehasta($fehasta)
        {      $this->fehasta = $fehasta; 
        }
        public function getFehasta()
        {      return $this->fehasta; 
        }
        
    }
?>        
