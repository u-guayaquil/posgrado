<?php

require_once("src/base/EntidadGlobal.php");

/**
 * @Entity
 * @Table(name="perfil_opcion", schema = "sistema")
 */
class PerfilOpcion extends EntidadGlobal 
{   /**
     * @Id
     * @Column(name="id", type="integer", nullable=false)
     * @GeneratedValue
     */
    private $id;

    /**
     *
     * @Column(name="opnuevo", type="integer", nullable=false)
     */
    private $opnuevo = 0;

    /**
     * @Column(name="opedita", type="integer", nullable=false)
     */
    private $opedita = 0;

    /**
     * @Column(name="opinactiva", type="integer",  nullable=false)
     */
    private $opinactiva = 0;

    /**
     * @Column(name="opimprime", type="integer", nullable=false)
     */
    private $opimprime = 0;

    /**
     * @Column(name="opexporta", type="integer", nullable=false)
     */
    private $opexporta = 0;

    /**
     * @Column(name="oppdf", type="integer", nullable=false)
     */
    private $oppdf = 0;

    /**
     * @ManyToOne(targetEntity="Perfil", inversedBy="perfilOpcion")
     * @JoinColumn(name="idperfil", referencedColumnName="id")
     */
    private $perfil;
    
    /**
     * @ManyToOne(targetEntity="Opcion", inversedBy="perfilOpcion")
     * @JoinColumn(name="idopcion", referencedColumnName="id")
     */
    private $opcion;

    public function getId()
    {      return $this->id;
    }

    public function setOpnuevo($opnuevo)
    {      $this->opnuevo = $opnuevo;
    }

    public function getOpnuevo()
    {      return $this->opnuevo;
    }

    public function setOpedita($opedita)
    {      $this->opedita = $opedita;
    }

    public function getOpedita()
    {      return $this->opedita;
    }

    public function setOpinactiva($opinactiva)
    {      $this->opinactiva = $opinactiva;
    }

    public function getOpinactiva()
    {      return $this->opinactiva;
    }

    public function setOpimprime($opimprime)
    {      $this->opimprime = $opimprime;
    }

    public function getOpimprime()
    {      return $this->opimprime;
    }

    public function setOpexporta($opexporta)
    {      $this->opexporta = $opexporta;
    }

    public function getOpexporta()
    {      return $this->opexporta;
    }

    public function setOppdf($oppdf)
    {      $this->oppdf = $oppdf;
    }

    public function getOppdf()
    {      return $this->oppdf;
    }

    function getOpcion()
    {        return $this->opcion;
    }

    function getPerfil()
    {        return $this->perfil;
    }

    function setOpcion($opcion)
    {        $this->opcion = $opcion;
    }

    function setPerfil($perfil)
    {        $this->perfil = $perfil;
    }

}
