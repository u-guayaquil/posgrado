<?php
    
    require_once("src/base/EntidadGlobal.php");
    
    /**
     *
     * @Entity
     * @Table(name="usuario", schema="sistema")
     */

    class Usuario extends EntidadGlobal
    {   /**
          * @Id
          * @Column(name="id", type="integer")
          * @GeneratedValue
          */
        private $id;
        /**
          *
          * @Column(name="usuario", type="string", length=80, nullable=false)
          */
        private $usuario;

        /**
          *
          * @Column(name="password", type="string", length=255, nullable=false)
          */
        private $password;

        /**
          * @Column(name="idempleado", type="integer")
          */
        private $idempleado;

        /**
          * @Column(name="idpregunta", type="integer")
          */
        private $idpregunta;

        /**
          * @Column(name="respuesta", type="string", length=20)
          */
        private $respuesta = '';

        /**
          * @Column(name="horadesde", type="string", length=8)
          */
        private $horadesde = '';

        /**
          * @Column(name="horahasta", type="string", length=8)
          */
        private $horahasta = '';
        
        /**
          * @Column(name="fecaducidad", type="datetime")
          */
        private $fecaducidad;

        public function getId()
        {      return $this->id;
        }

        public function setUsuario($usuario)
        {      $this->usuario = $usuario;
        }

        public function getUsuario()
        {      return $this->usuario;
        }

        public function setPassword($password)
        {      $this->password = $password;
        }

        public function getPassword()
        {      return $this->password;
        }

        public function setIdempleado($idempleado)
        {      $this->idempleado = $idempleado;
        }

        public function getIdempleado()
        {      return $this->idempleado;
        }

        public function setIdpregunta($idpregunta)
        {      $this->idpregunta = $idpregunta;
        }

        public function getIdpregunta()
        {      return $this->idpregunta;
        }

        public function setRespuesta($respuesta)
        {      $this->respuesta = $respuesta;
        }

        public function getRespuesta()
        {      return $this->respuesta;
        }

        public function setHoradesde($horadesde)
        {      $this->horadesde = $horadesde;
        }

        public function getHoradesde()
        {      return $this->horadesde;
        }

        public function setHorahasta($horahasta)
        {      $this->horahasta = $horahasta;
        }

        public function getHorahasta()
        {      return $this->horahasta;
        }
        
        public function setFecaducidad($fecaducidad)
        {      $this->fecaducidad = $fecaducidad;
        }

        public function getFecaducidad()
        {      return $this->fecaducidad;
        }
        
    }

