<?php

    require_once("src/base/EntidadGlobal.php");
    use Doctrine\Common\Collections\ArrayCollection;

    /**
     * @Entity
     * @Table(name="opcion",schema ="sistema")
     */
    class Opcion extends EntidadGlobal 
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="descripcion", type="string", length=30, nullable=false)
         */
        private $descripcion;

        /**
         * @Column(name="ruta", type="string", length=255, nullable=true)
         */
        private $ruta = '';

        /**
         * @Column(name="orden", type="integer", nullable=false)
         */
        private $orden = 1;

        /**
         * @Column(name="idmenu", type="integer", nullable=false)
         */
        private $idmenu = 0;

        /**
         * @Column(name="idtipo", type="integer", nullable=false)
         */
        private $idtipo = 1;

        /**
         * @Column(name="classicon", type="string", length=20, nullable=true)
         */
        private $classicon = '';

        /**
         * @OneToMany(targetEntity="PerfilOpcion", 
         *            mappedBy="opcion")
         */
        private $perfilOpcion;

        public function __construct()
        {      $this->perfilOpcion = new ArrayCollection();
        }

        public function getId()
        {      return $this->id;
        }

        public function setDescripcion($descripcion)
        {      $this->descripcion = $descripcion;
        }

        public function getDescripcion()
        {      return $this->descripcion;
        }

        public function setRuta($ruta)
        {      $this->ruta = $ruta;
        }

        public function getRuta()
        {      return $this->ruta;
        }

        public function setOrden($orden)
        {      $this->orden = $orden;
        }

        public function getOrden()
        {      return $this->orden;
        }

        public function setIdmenu($idmenu)
        {      $this->idmenu = $idmenu;
        }

        public function getIdmenu()
        {      return $this->idmenu;
        }

        public function setIdtipo($idtipo)
        {      $this->idtipo = $idtipo;
        }

        public function getIdtipo()
        {      return $this->idtipo;
        }

        public function setClassicon($classicon)
        {      $this->classicon = $classicon;
        }

        public function getClassicon()
        {      return $this->classicon;
        }

        function getPerfilOpcion()
        {        return $this->perfilOpcion;
        }

        function setPerfilOpcion(ArrayCollection $perfilOpcion)
        {        $this->perfilOpcion = $perfilOpcion;
        }
    }
