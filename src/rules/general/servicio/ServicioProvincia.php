<?php
            require_once("src/rules/general/dao/DAOProvincia.php");
            
            class ServicioProvincia
            {   private $DAOProvincia;
     
                function __construct()
                {       $this->DAOProvincia = new DAOProvincia();
                }
           
                function GuardaDBProvincia($Form)
                {       if (empty($Form->id)){
                            return $this->DAOProvincia->InsertaProvincia($Form);
                        }else{
                            return $this->DAOProvincia->ActualizaProvincia($Form);
                        }
                }
                  
                function DesactivaProvincia($Id)
                {       return $this->DAOProvincia->DesactivaProvincia($Id);
                }
           
                function BuscarProvinciaByID($prepareDQL)
                {        return $this->DAOProvincia->ObtenerProvinciaByDescripcion($prepareDQL);
                }

                function BuscarProvinciaByDescripcion($prepareDQL)
                {        return $this->DAOProvincia->ObtenerProvinciaByDescripcion($prepareDQL); 
                }
                  
                function BuscarProvinciaByArrayID($prepareDQL)
                {        return $this->DAOProvincia->ObtenerProvinciaByDescripcion($prepareDQL); 
                }
           
            }
?>

