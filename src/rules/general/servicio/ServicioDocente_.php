<?php
            require_once("src/rules/general/dao/DAODocente.php");
            
            class ServicioDocenteQ 
            {       private $DAODocente;
     
                    function __construct()
                    {        $this->DAODocente = new DAODocente();
                    }
                    
                    function GuardaDBDocente($Form)
                    {    $Valida = $this->ValidaDatosDocente($Form);
                         if(count(trim($Valida))==1){
                            $datos = $this->ExtraeNumIdentificacionRepetido($Form->numidentificacion,$Form->id);
                            if(count($datos)==0){                                
                                if (empty($Form->id)){
                                    return $this->DAODocente->InsertaDocente($Form);
                                  }else{
                                    return $this->DAODocente->ActualizaDocente($Form);
                                  }
                            }else{
                                return 'Ya existe un Docente con esa Identificación..';
                            }
                         }else{
                            return $Valida; 
                         }
                    }
                    
                    function ExtraeNumIdentificacionRepetido($Identificacion,$id)
                    {       
                            return $this->DAODocente->ObtenerDatosGeneral(array('numero' => ($Identificacion),'iddis' => ($id)));                                
                    }
                    
                    function DesactivaDocente($id)
                    {       return $this->DAODocente->DesactivaDocente(intval($id)); 
                    }

                    function BuscarDocente($prepareDQL)
                    {     
                            return $this->DAODocente->ObtenerDatosGeneral($prepareDQL);
                    }
                    
                    function BuscarDocenteByCodigo($codigo)
                    {     
                            return $this->DAODocente->ObtenerDatosByCodigo($codigo);
                    }
                    
                    function ValidaDatosDocente($Form)
                    {   $Mensajes='';
                            
                        if(trim($Form->codigo)===''){
                            $Mensajes.='--> Debe ingresar el Codigo del Docente..!!!<\br>'; 
                        }
                        if(trim($Form->direccion)===''){
                            $Mensajes.='--> Debe ingresar la Direccion del Docente..!!!\n'; 
                        }
                        if(trim($Form->numidentificacion)===''){
                            $Mensajes.='--> Debe ingresar el Numero de Identificacion..!!!\n'; 
                        }
                        if(trim($Form->idCiudad)===''){
                            $Mensajes.='--> Debe ingresar la Ciudad del Docente..!!!\n'; 
                        }
                        if(trim($Form->idCuentaPorPagar)===''){
                            $Mensajes.='--> Debe ingresar la Cuenta por Pagar..!!!\n'; 
                        }
                        if(trim($Form->idCuentaAnticipo)===''){
                            $Mensajes.='--> Debe ingresar la Cuenta de Anticipo..!!!\n'; 
                        }                        
                        
                        return $Mensajes;
                    }
                    

}        
?>        
