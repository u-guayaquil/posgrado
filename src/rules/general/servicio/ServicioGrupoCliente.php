<?php
            require_once("src/rules/general/dao/DAOGrupoCliente.php");
            require_once("src/rules/general/entidad/GrupoCliente.php");
            
            class ServicioGrupoCliente
            {     private $DAOGrupoCliente;
     
                  function __construct()
                  {        $this->DAOGrupoCliente = new DAOGrupoCliente();
                  }
                  
                  function GuardaDBGrupoCliente($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOGrupoCliente->InsertaGrupoCliente($Form);
                          }else{
                            return $this->DAOGrupoCliente->ActualizaGrupoCliente($Form);
                          }
                  }
                  
                  function DesactivaGrupoCliente($Id)
                  {       return $this->DAOGrupoCliente->DesactivaGrupoCliente($Id);
                  }
           
                  function BuscarGrupoClienteByID($id)
                  {        return $this->DAOGrupoCliente->BuscarGrupoClienteByDescrip($id);
                  }

                  function BuscarGrupoClienteByDescripcion($prepareDQL)
                  {        return $this->DAOGrupoCliente->BuscarGrupoClienteByDescrip($prepareDQL);
                  }
                  
                  function BuscarGrupoClienteByArrayID($idArray)
                  {       return $this->DAOGrupoCliente->BuscarGrupoClienteByDescrip($idArray);
                  }
                  
                  

            }
?>

