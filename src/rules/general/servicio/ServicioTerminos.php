<?php
            require_once("src/rules/general/dao/DAOTerminos.php");
            require_once("src/rules/general/dao/DAOTiempo.php");
            
            class ServicioTerminos 
            {       private $DAOTerminos;
                    private $DAOTiempo;
     
                    function __construct()
                    {        $this->DAOTerminos = new DAOTerminos();
                             $this->DAOTiempo = new DAOTiempo();
                    }
           
                    
                    function GuardaDBTerminos($Terminos)
                    {       if (empty($Terminos->id)){
                                return $this->DAOTerminos->InsertaTerminos($Terminos);
                            }else{
                                return $this->DAOTerminos->ActualizaTerminos($Terminos);
                            }
                    }
                    
                    function DesactivaTerminos($id)
                    {       return $this->DAOTerminos->DesactivaTerminos(intval($id));
                    }
                    
                    function BuscarTerminosByID($id)
                    {       return $this->DAOTerminos->ObtenerTerminosByID(intval($id));
                    }

                    function BuscarTerminosByDescripcion($prepareDQL)
                    {       return $this->DAOTerminos->ObtenerTerminosByDescripcion($prepareDQL);
                    }
                    
                    function BuscarTerminosEstadoByID($id)
                    {       return $this->DAOTerminos->ObtenerTerminosEstadoByID(intval($id));
                    }
                    
                    function BuscarTerminosByEstadoID($id)
                    {        return $this->DAOTerminos->ObtenerTerminosByEstadoID(intval($id));
                    }    
                        
            }
?>

