<?php
        require_once("src/rules/general/dao/DAOEstado.php");
         
        class ServicioEstado
        {     private $DAOEstado;

              function __construct()
              {        $this->DAOEstado = new DAOEstado();
              }

              function BuscarEstadoByID($id)
              {        return $this->DAOEstado->ObtenerEstadoByID($id);
              }
              
              function BuscarEstadoByArrayID($idArray)
              {        return $this->DAOEstado->ObtenerEstadoArrayByID($idArray);
              }
               
              function SeleccionarTodosEstados()
              {        return $this->DAOEstado->ObtenerTodosEstados();
              }

        }
         
?>

