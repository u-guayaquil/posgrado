<?php
        require_once("src/rules/general/dao/DAOZonaCiudad.php");
        require_once("src/rules/general/dao/DAOCiudad.php");
        require_once("src/rules/general/dao/DAOZonas.php");

        class ServicioZonaCiudad 
        {       private $DAOZonaCiudad;
                private $DAOCiudad;
                private $DAOZonas;
                
                function __construct()
                {       $this->DAOZonaCiudad = new DAOZonaCiudad();
                        $this->DAOZonas = new DAOZonas();
                        $this->DAOCiudad = new DAOCiudad();
                }
               
                function BuscarZonaCiudadByID($prepareDQL)
                {       return $this->DAOZonaCiudad->ObtenerZonaCiudad($prepareDQL);
                }

                function BuscarZonaCiudadByDescripcion($prepareDQL)
                {       return $this->DAOZonaCiudad->ObtenerZonaCiudad($prepareDQL);
                }
                
                function GuardaDBZonaCiudad($ZonaCiudad)
                {       if (empty($ZonaCiudad->id)){
                            return $this->DAOZonaCiudad->InsertaZonaCiudad($ZonaCiudad);
                        }else{
                            return $this->DAOZonaCiudad->ActualizaZonaCiudad($ZonaCiudad);
                        }
                }
                
                function EliminaZonaCiudad($id)
                {       return $this->DAOZonaCiudad->EliminaZonaCiudad(intval($id));
                }

        }
?>