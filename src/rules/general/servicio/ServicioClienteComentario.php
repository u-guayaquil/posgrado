<?php
            require_once("src/rules/general/dao/DAOClienteComentario.php");
            
            class ServicioClienteComentario
            {     private $DAOClienteComentario;
                  private $ServicioCliente;
     
                  function __construct()
                  {        $this->DAOClienteComentario = new DAOClienteComentario();                
                  }
                  
                  function GuardaDBClienteComentario($Form)
                  {       $fecha = explode('/', $Form->fecha);                      
                          $Form->fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                          if (empty($Form->id)){
                            return $this->DAOClienteComentario->InsertaClienteComentario($Form);
                          }else{
                            return $this->DAOClienteComentario->ActualizaClienteComentario($Form);
                          }
                  }
                  
                  function DesactivaClienteComentario($Id)
                  {       return $this->DAOClienteComentario->DesactivaClienteComentario($Id);
                  }
           
                  function BuscarClienteComentarioByID($id)
                  {        return $this->DAOClienteComentario->BuscarClienteComentarioByDescripcion($id);
                  }

                  function BuscarClienteComentarioByDescripcion($prepareDQL)
                  {        return $this->DAOClienteComentario->BuscarClienteComentarioByDescripcion($prepareDQL);
                  }
            }
?>

