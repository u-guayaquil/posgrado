<?php
            require_once("src/rules/general/dao/DAOParroquia.php");
            
            class ServicioParroquia
            {     private $DAOParroquia;
     
                  function __construct()
                  {        $this->DAOParroquia = new DAOParroquia();
                  }
           
                  function GuardaDBParroquia($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOParroquia->InsertaParroquia($Form);
                          }else{
                            return $this->DAOParroquia->ActualizaParroquia($Form);
                          }
                  }
                  
                  function DesactivaParroquia($Id)
                  {       return $this->DAOParroquia->DesactivaParroquia($Id);
                  }
           
                  function BuscarParroquiaByID($prepareDQL)
                  {        return $this->DAOParroquia->ObtenerParroquiaByDescripcion($prepareDQL);
                  }

                  function BuscarParroquiaByDescripcion($prepareDQL)
                  {        return $this->DAOParroquia->ObtenerParroquiaByDescripcion($prepareDQL);
                  }
                  
                  function BuscarParroquiaParaCombo($prepareDQL)
                  {        return $this->DAOParroquia->ObtenerParroquiaByDescripcion($prepareDQL);
                  }
           
            }
?>

