<?php
            require_once("src/rules/general/dao/DAOClienteIdentificacion.php");
            require_once("src/rules/impuesto/servicio/ServicioIdentificacion.php");
            require_once("src/libs/clases/Utils.php");
            
            class ServicioClienteIdentificacion
            {     private $DAOClienteIdentificacion;
                  private $ServicioIdentificacion;
                  private $Utils;
     
                  function __construct()
                  {        $this->DAOClienteIdentificacion = new DAOClienteIdentificacion();
                           $this->ServicioIdentificacion = new ServicioIdentificacion();
                           $this->Utils = new Utils();
                  }

                  function BuscarClienteIdentByTipoIdent($numero,$tipoident,$cliente=0)
                  {        return $this->DAOClienteIdentificacion->ObtenerClienteIdentificacion(array('tipo' => intval($tipoident),
                                                                                                      'numero' => ($numero)),$cliente);                        
                  }
                  
                  public function ObjetoClienteIdentificacionByIDCliente($id)
                  {        return $this->DAOClienteIdentificacion->ObtenerClienteIdentificacion(array('cliente' => intval($id)));
                  }
                  
                  public function BuscarClienteIdentificacionByIDCliente($id)
                  {        return $this->DAOClienteIdentificacion->ObtenerClienteIdentificacionByCliente(array('cliente' => intval($id)));
                  }
                  
                  function GuardaDBClienteIdentificacion($Form)
                    {       $Guarda=0;
                            $ban=0;
                            foreach  ($Form as $clave => $valor) {
                                    if($clave != 'cedula' && $clave != 'ruc'&& $clave != 'pasaporte'){  break;  }
                                        if($valor != 0){
                                            $Mensajes = $this->VerificaDatosIdentificacion($Form,$clave,$Form->id);
                                            if(($Mensajes) == ''){ 
                                                if($ban == 0){$this->EliminaDatos($Form->id); $ban=1;}
                                                $IdIdentificacion = $this->ServicioIdentificacion->BuscarIdentificacionByDescription(strtoupper($clave));  
                                                $Identificacion = $IdIdentificacion[0]['id'];  
                                                $resultado = $this->DAOClienteIdentificacion->InsertaClienteIdentificacion($Form->id,$Identificacion,$valor);
                                            if(is_numeric($resultado)){ $Guarda=0; }else{ $Guarda=1;  break; }  
                                        }else{
                                            $Guarda=1;  break; 
                                        } 
                                    }
                            } 
                            if($Guarda==0){
                                return $Guarda;
                            }else{
                                return $Mensajes;
                            }          
                    }
                  
                  
                  function EliminaDatos($id){  
                        return $this->DAOClienteIdentificacion->EliminaClienteIdentificacion(intval($id));
                  }
                  
                  function SacaObjetoIdentificacion($Form,$clave){
                      $ClienteIdentificacion = '';                       
                        switch ($clave){
                            case 'cedula':                                                      
                                $ClienteIdentificacion = $this->ObjetoClienteIdentificacionByIDCliente(intval($Form->idcedula));
                                break;
                            case 'ruc':                             
                                $ClienteIdentificacion = $this->ObjetoClienteIdentificacionByIDCliente(intval($Form->idruc));
                                break;
                            case 'pasaporte':                                 
                                $ClienteIdentificacion = $this->ObjetoClienteIdentificacionByIDCliente(intval($Form->idpasaporte));
                                break;
                        }
                        return $ClienteIdentificacion;
                  }
                  
                  function VerificaDatosIdentificacion($Form,$clave){
                      $Mensajes = ''; 
                      $IdIdentificacion = $this->ServicioIdentificacion->BuscarIdentificacionByDescription(strtoupper($clave));                                        
                      $Identificacion = $IdIdentificacion[0]['id'];
                        switch ($clave){
                            case 'cedula':  
                                list($estado,$mensaje) = $this->Utils->validaIndentificacion(2,$Form->cedula);
                                if(!$estado){ $Mensajes = $mensaje; break; }
                                //Verifico si hay datos de cedula de ese cliente.                                
                                $datosCliente = $this->DAOClienteIdentificacion->ObtenerClienteIdentificacionByCliente(array('cliente' => intval($Form->id)));
                                
                                if(count($datosCliente) > 0){
                                    $Datos = $this->BuscarClienteIdentByTipoIdent(($Form->cedula),$Identificacion,intval($Form->id));
                                }else{
                                    $Datos = $this->BuscarClienteIdentByTipoIdent(($Form->cedula),$Identificacion);
                                }                                
                                if(count($Datos)>0){
                                    $Mensajes = 'La Cedula Ingresada ya existe...'; 
                                }                                
                                break;
                            case 'ruc': 
                                list($estado,$mensaje) = $this->Utils->validaIndentificacion(1,$Form->ruc);
                                if(!$estado){ $Mensajes = $mensaje; break; }
                                $Datos = $this->BuscarClienteIdentByTipoIdent(($Form->ruc),$Identificacion);
                                if(count($Datos)>0){
                                   $Mensajes = 'El Ruc Ingresado ya existe...'; 
                                }
                                break;
                            case 'pasaporte':   
                                list($estado,$mensaje) = $this->Utils->validaIndentificacion(3,$Form->pasaporte);
                                if(!$estado){ $Mensajes = $mensaje; break; }
                                $Datos = $this->BuscarClienteIdentByTipoIdent(($Form->pasaporte),$Identificacion);
                                if(count($Datos)>0){
                                   $Mensajes = 'El Pasaporte Ingresado ya existe...'; 
                                }
                                break;
                        }
                        return $Mensajes;
                  }                 
                  
                  function EditaClienteIdentificacion($Form)
                  {        
                        return $this->CreaClienteIdentificacion($Form);                        
                  }
            }
?>

