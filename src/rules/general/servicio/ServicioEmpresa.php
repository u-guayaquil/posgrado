<?php
        require_once("src/rules/general/dao/DAOEmpresa.php");    

        class ServicioEmpresa
        {       private $DAOEmpresa;
                private $Utils;
        
                function __construct()
                {       $this->DAOEmpresa = new DAOEmpresa();
                        $this->Utils = new Utils();
                }
                
                function ObtenerDatosEmpresa()
                {        return $this->DAOEmpresa->ObtenerEmpresa();
                }
                
                function ActualizaEmpresa($Form)
                {       $empresa = $this->Utils->validaIndentificacion($Form->idrazon, $Form->nurazon);
                        if($empresa[0])
                        {   $rplegal = $this->Utils->validaIndentificacion($Form->idlegal, $Form->nulegal);
                            if($rplegal[0])
                            {   $auditor = $this->Utils->validaIndentificacion($Form->idcontador, $Form->nucontador);
                                if($auditor[0])
                                {  return $this->preparaDatos($Form);
                                }
                                else
                                return "Contador: ".$auditor[1];                                     
                            }
                            else
                            return "Representante Legal: ".$rplegal[1];                                     
                        }
                        else
                        return "Razón Social: ".$empresa[1];     
                }
                    
                private function preparaDatos($Form)
                {       return $this->DAOEmpresa->EditaEmpresa($Form);
                }
        }        
?>                