<?php
            require_once("src/rules/general/dao/DAOCiudad.php");
            
            class ServicioCiudad
            {   private $DAOCiudad;
     
                function __construct()
                {        $this->DAOCiudad = new DAOCiudad();
                }
           
                function GuardaDBCiudad($Form)
                {       if (empty($Form->id)){
                            return $this->DAOCiudad->InsertaCiudad($Form);
                        }else{
                            return $this->DAOCiudad->ActualizaCiudad($Form);
                        }
                }
                  
                function DesactivaCiudad($Id)
                {       return $this->DAOCiudad->DesactivaCiudad($Id);
                }
           
                function BuscarCiudadByDescripcion($prepareDQL)
                {        return $this->DAOCiudad->ObtenerCiudadByDescripcion($prepareDQL);
                }
                  
                function BuscarCiudadParaCombo($prepareDQL)
                {        return $this->DAOCiudad->ObtenerCiudadByDescripcion($prepareDQL);
                }
           
            }
?>

