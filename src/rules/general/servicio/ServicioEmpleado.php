<?php
        require_once("src/rules/general/dao/DAOEmpleado.php");
        
        class ServicioEmpleado 
        {       private $DAOEmpleado;
                private $Utils;
                
                function __construct()
                {        $this->DAOEmpleado = new DAOEmpleado();
                         $this->Utils = new Utils();
                }

                function BuscarByID($id)
                {        return $this->DAOEmpleado->ObtenerByID(intval($id));
                }
                
                function BuscarByNombre($prepareDQL)
                {        return $this->DAOEmpleado->ObtenerByNombre($prepareDQL);
                }
                  
                function Crea($Forma)
                {       $empleado = $this->Utils->validaIndentificacion(2, $Forma->cedula);
                        if($empleado[0]){
                           return $this->DAOEmpleado->Inserta($Forma);
                        }
                        else{
                           return "Empleado: ".$empleado[1];     
                        }
                }
                
                function Edita($Forma)
                {       $empleado = $this->Utils->validaIndentificacion(2, $Forma->cedula);
                        if($empleado[0]){
                           return $this->DAOEmpleado->Actualiza($Forma);
                        }
                        else{
                           return "Empleado: ".$empleado[1];     
                        }
                }

                function Desactiva($id)
                {       return $this->DAOEmpleado->Elimina(intval($id));
                }
                
                  
        }    

?>        