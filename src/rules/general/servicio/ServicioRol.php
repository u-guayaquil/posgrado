<?php
        require_once("src/rules/general/dao/DAORol.php");
         
        class ServicioRol
        {     private $DAORol;

              function __construct()
              {        $this->DAORol = new DAORol();
              }

              function BuscarRolByArrayID($idArray)
              {        return $this->DAORol->ObtenerRolArrayByID($idArray);
              }
               
              function SeleccionarTodosRoles()
              {        return $this->DAORol->ObtenerTodosRoles();
              }
              
              function SeleccionarRolesModulo($WithDefault=0)
              {        return $this->DAORol->ObtenerRolModulo($WithDefault); 
              }

        }
         
?>
