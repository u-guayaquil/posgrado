<?php
            require_once("src/rules/general/dao/DAOSucursalEmision.php");
            
            class ServicioSucursalEmision 
            {       private $DAOSucursalEmision;
     
                    function __construct()
                    {        $this->DAOSucursalEmision = new DAOSucursalEmision();
                    }
                    
                    function GuardaDBSucursalEmision($Form)
                    {       $fechai = explode('/', $Form->fevigencia);
                            $fechaf = explode('/', $Form->fecaducidad);                  
                            $Form->fevigencia = $fechai[2].'-'.$fechai[1].'-'.$fechai[0];
                            $Form->fecaducidad = $fechaf[2].'-'.$fechaf[1].'-'.$fechaf[0];
                            $fechaInicial = $fechai[2].$fechai[1].$fechai[0];
                            $fechaFinal = $fechaf[2].$fechaf[1].$fechaf[0];  
                            $compare_ingreso = strtotime($fechaInicial);
                            $compare_final = strtotime($fechaFinal);
                            $bandera =0;
                            if($Form->fecaducidad != '1900-01-01'){
                                if ($compare_ingreso < $compare_final){ 
                                        $bandera=0;
                                 } else {
                                     $bandera=1;
                                    return 'La Fecha de Vigencia debe ser menor a la Fecha de Caducidad...';
                                 }
                            }
                            if ($bandera==0){ 
                                    if (empty($Form->id)){
                                      return $this->DAOSucursalEmision->InsertaSucursalEmision($Form);
                                    }else{
                                      return $this->DAOSucursalEmision->ActualizaSucursalEmision($Form);
                                    }
                             } 
                    }
                    
                    function DesactivaSucursalEmision($id)
                    {       return $this->DAOSucursalEmision->DesactivaSucursalEmision(intval($id)); 
                    }
                    
                    function BuscarSucursalEmision($PrepareDQL)
                    {       return $this->DAOSucursalEmision->ObtenerDatosGeneral($PrepareDQL);
                    }
}        
?>        
