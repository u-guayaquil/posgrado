<?php
        require_once("src/rules/general/entidad/Tiempo.php");
        require_once("src/rules/general/dao/DAOTiempo.php");
         
        class ServicioTiempo
        {     private $DAOTiempo;

              function __construct()
              {        $this->DAOTiempo = new DAOTiempo();
              }

              function BuscarTiempoByID($id)
              {        return $this->DAOTiempo->ObtenerTiempoByID($id);
              }

              function BuscarTiempoByArrayID($idArray)
              {        return $this->DAOTiempo->ObtenerTiempoByArrayID($idArray);
              }
               
              function SeleccionarTodosTiempos()
              {        return $this->DAOTiempo->ObtenerTodosTiempos();
              }

        }
         
?>

