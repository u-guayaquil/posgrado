<?php
            require_once("src/rules/general/dao/DAOUnidadConversion.php");
            
            class ServicioUnidadConversion
            {     private $DAOUnidadConversion;
     
                  function __construct()
                  {        $this->DAOUnidadConversion = new DAOUnidadConversion();
                  }
                  
                  function GuardaDBUnidadConversion($Form)
                  {     
                      if(count($Verifica)<=0){
                          if (empty($Form->id)){
                              $Verifica = $this->VerificaUnidades($Form->Origen,$Form->Destino); 
                              return $this->DAOUnidadConversion->InsertaUnidadConversion($Form);
                          }else{
                              $Verifica = $this->VerificaUnidades($Form->Origen,$Form->Destino,$Form->id); 
                              return $this->DAOUnidadConversion->ActualizaUnidadConversion($Form);
                          }
                      }else{
                          return 'La Combinacion ya existe';
                      }
                  }
                  
                  function DesactivaUnidadConversion($Id)
                  {       return $this->DAOUnidadConversion->DesactivaUnidadConversion($Id);
                  }
           
                  function BuscarUnidadConversionByID($id)
                  {        return $this->DAOUnidadConversion->BuscarUnidadConversion($id,0);
                  }

                  function BuscarUnidadConversionByDescripcion($prepareDQL)
                  {        return $this->DAOUnidadConversion->BuscarUnidadConversion($prepareDQL,0);
                  } 
                  
                  function BuscarUnidadConversionByArrayID($idArray)
                  {       return $this->DAOUnidadConversion->BuscarUnidadConversion($idArray,0);
                  }
                  
                  function VerificaUnidades($Origen,$Destino,$id=0)
                  {       return $this->DAOUnidadConversion->BuscarUnidadConversion(array('origen'=>$Origen,'destino'=>$Destino),$id);
                  }

            }
?>

