<?php
            require_once("src/rules/general/dao/DAOCarrera.php");
            class ServicioCarrera 
            {       private $DAOCarrera;
     
                    function __construct()
                    {        $this->DAOCarrera = new DAOCarrera();
                    }

                    function BuscarByID($id)
                    {        return $this->DAOCarrera->ObtenerByID($id);
                    }
                    
                    function BuscarByDescripcion($prepareDQL)
                    {        return $this->DAOCarrera->ObtenerByDescripcion($prepareDQL);
                    }

                    function BuscarByFacultadID($IdFacultad)
                    {       return $this->DAOCarrera->ObtenerByFacultadID($IdFacultad);
                    }
            }        
?>        
