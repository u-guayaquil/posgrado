<?php
            require_once("src/rules/general/dao/DAOPlanCuentas.php");
            require_once("src/libs/clases/File.php");
            
            class ServicioPlanCuentas 
            {       private $DAOPlanCuentas;
            
                    function __construct()
                    {       $this->DAOPlanCuentas = new DAOPlanCuentas();
                    }
                    
                    function GuardaCuenta($Form)
                    {       if (empty($Form->idplancta)){
                                return $this->DAOPlanCuentas->InsertaCuenta($Form);
                            }
                            else
                            {   if (intval($Form->idplancta) != intval($Form->idpadre))
                                {   if ($this->PermiteCambioTipo($Form->idplancta,$Form->tipo))
                                    {   if ($this->PermiteCambioPadre($Form->idplancta,intval($Form->idpadre)))    
                                            return $this->DAOPlanCuentas->ActualizaCuenta($Form);
                                        else
                                        return "La cuenta no puede tener como padre a un hijo de el mismo.";    
                                    }
                                    else
                                    return "No puede cambiar el tipo de grupo a movimiento. La cuenta tiene hijos.";
                                }    
                                else
                                return "La cuenta no puede tener como padre a si mismo.";
                            }
                    }

                    function PermiteCambioPadre($Cuenta,$Npadre)    
                    {       $Cuenta = intval($Cuenta);
                            $Npadre = intval($Npadre);
                            $Datos  = $this->DAOPlanCuentas->ObtenerCuentas(array('id'=>$Cuenta));
                            if ($Datos[0]['tipo']!='M')
                            {   $root = $this->DAOPlanCuentas->ObtenerCuentasPorPadre($Cuenta,"G");
                                foreach ($root as $sheet)
                                {       $id = intval($sheet['id']);
                                        if ($id!=$Npadre){
                                            if ($this->PermiteCambioPadreSgte($id,$Npadre)==1)
                                            return false;
                                        }
                                        else
                                        return false;    
                                }
                            }
                            return true;
                    }

                    private function PermiteCambioPadreSgte($Cuenta,$Npadre)
                    {       $root = $this->DAOPlanCuentas->ObtenerCuentasPorPadre($Cuenta,"G");
                            foreach ($root as $sheet)
                            {       $id = intval($sheet['id']);
                                    if ($id!=$Npadre){
                                        if ($this->PermiteCambioPadreSgte($id,$Npadre)==1)
                                        return 1;
                                    }
                                    else
                                    return 1;
                            }
                            return 0;
                    }
                    
                    function PermiteCambioTipo($Id,$TipoActual)
                    {       $Datos = $this->DAOPlanCuentas->ObtenerCuentas(array('id'=>$Id));
                            $TipoAnterior = $Datos[0]['tipo'];
                            if ($TipoAnterior=='G' && $TipoActual=='M')
                            {   $NumeroHijos = $this->DAOPlanCuentas->ObtenerCuentaHijosPadre($Id);
                                if ($NumeroHijos!=0) return false;
                            }
                            return true;
                    }

                    function DesactivaCuenta($id)
                    {       return $this->DAOPlanCuentas->DesactivaCuenta($id); 
                    }
                    
                    function BuscarCuenta($prepareDQL)
                    {       return $this->DAOPlanCuentas->ObtenerCuentas($prepareDQL);
                    }
                    
                    function BuscarCuentaByID($id)
                    {       return $this->DAOPlanCuentas->ObtenerCuentas(array('id' =>intval($id)));
                    }

                    function BuscarPlanCuenta($idPadre)
                    {       $main = array();
                            $root = $this->DAOPlanCuentas->ObtenerCuentasPorPadre($idPadre);
                            foreach ($root as $sheet)
                            {       $child = $this->BuscarPlanCuentaSgte(1,$sheet['id']); 
                                    $txcta = strtoupper(trim($sheet['descripcion']));
                                    $codig = trim($sheet['codigo']);
                                    $main[] = array('id'=>$sheet['id'],'level'=>0,'type'=>$sheet['tipo'],'codigo'=>$codig,'ctas'=>$txcta, 'status'=>$sheet['idestado'], 'hijos'=>$child);
                            }
                            return json_encode($main);                    
                    }

                    private function BuscarPlanCuentaSgte($level,$idPadre)
                    {       $main = array();
                            $root = $this->DAOPlanCuentas->ObtenerCuentasPorPadre($idPadre);
                            foreach ($root as $sheet)
                            {       $child = array();
                                    $codig = trim($sheet['codigo']);
                                    $txcta = strtoupper(trim($sheet['descripcion']));
                                    if ($sheet['tipo']=="G")
                                    {   $child = $this->BuscarPlanCuentaSgte($level+1,$sheet['id']); 
                                    }
                                    $main[] = array('id'=>$sheet['id'],'level'=>$level,'type'=>$sheet['tipo'],'codigo'=>$codig,'ctas'=>$txcta, 'status'=>$sheet['idestado'], 'hijos'=>$child);
                            }
                            return $main;
                    }
                    
                    private function ValidaFormatoFileCuentas($cols)
                    {       $MyCSVFile = new File();
                            $MyCSVFile->setFileName("src/public/formatos/carga_plan.csv");
                            $MyCSVFile->openFile("r");
                            while(!$MyCSVFile->endOfFile())
                            {       $colToken = explode("|", $MyCSVFile->getDataFile());  
                                    if (count($colToken)==count($cols))
                                    {   for ($i=0; $i<count($colToken); $i++)  
                                        {   if (trim($cols[$i])!=trim($colToken[$i]))         
                                            return "El nombre de la columna ".$cols[$i]." no coincide con el formato establecido.";   
                                        }    
                                    }
                                    else
                                    return "El número de columnas del archivo no es válido.";
                            }
                            $MyCSVFile->closFile();
                            return true;
                    }
                    
                    function LeerFileCuentas($Ruta)
                    {       $MyCSVArry = array();
                            $MyCSVFile = new File();
                            $MyCSVFile->setFileName($Ruta);
                            $MyCSVFile->openFile("r");
                            $MyCSVHead = 0;
                            
                            while(!$MyCSVFile->endOfFile())
                            {   $cptokens = explode("|", utf8_decode($MyCSVFile->getDataFile()));
                                if ($MyCSVHead==0)
                                {   $mssg = $this->ValidaFormatoFileCuentas($cptokens);
                                    if (!is_bool($mssg)) return $mssg;
                                }    
                                else
                                {   if (!empty($cptokens[0])) $MyCSVArry[] = $cptokens;     
                                }
                                $MyCSVHead++;
                            }
                            $MyCSVFile->closFile();
                            return $this->DAOPlanCuentas->InsertaFileCuenta($MyCSVArry);
                    }
                    
            }        
?>        
