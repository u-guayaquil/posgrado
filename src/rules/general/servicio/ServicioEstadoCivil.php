<?php
            require_once("src/rules/general/dao/DAOEstadoCivil.php");
            
            class ServicioEstadoCivil
            {     private $DAOEstadoCivil;
     
                  function __construct()
                  {        $this->DAOEstadoCivil = new DAOEstadoCivil();
                  }
           
                  function BuscarEstadoCivil($PrepareDQL)
                  {        return $this->DAOEstadoCivil->ObtenerEstadoCivil($PrepareDQL);
                  }
            }
?>

