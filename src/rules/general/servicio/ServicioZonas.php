<?php
            require_once("src/rules/general/dao/DAOZonas.php");
            require_once("src/rules/general/entidad/Zonas.php");
            
            class ServicioZonas
            {     private $DAOZonas;
     
                  function __construct()
                  {        $this->DAOZonas = new DAOZonas();
                  }
                  
                  function GuardaDBZonas($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOZonas->InsertaZonas($Form);
                          }else{
                            return $this->DAOZonas->ActualizaZonas($Form);
                          }
                  }
                  
                  function DesactivaZonas($Id)
                  {       return $this->DAOZonas->DesactivaZonas($Id);
                  }
           
                  function BuscarZonasByID($prepareDQL)
                  {        return $this->DAOZonas->BuscarZonasByDescripcion($prepareDQL);
                  }

                  function BuscarZonasByDescripcion($prepareDQL)
                  {        return $this->DAOZonas->BuscarZonasByDescripcion($prepareDQL);
                  }
                  
                  function BuscarZonasByArrayID($idArray)
                  {       return $this->DAOZonas->ObtenerZonasCombo($idArray);
                  }
                  
                  

            }
?>

