<?php
        require_once("src/rules/general/dao/DAOFacultad.php");
        
        class ServicioFacultad 
        {       private $DAOFacultad;
     
                function __construct()
                {        $this->DAOFacultad = new DAOFacultad();
                }

                function BuscarFacultadByID($id)
                {       return $this->DAOFacultad->ObtenerFacultadByID($id);
                }
            
                function BuscarFacultadByDescripcion($prepareDQL)
                {       return $this->DAOFacultad->ObtenerFacultadByDescripcion($prepareDQL);
                }
                
                function BuscarFacultadByDocente($prepareDQL)
                {       return $this->DAOFacultad->ObtenerFacultadByDocente($prepareDQL);
                }
                
                function BuscarFacultadByUsuario($prepareDQL)
                {       return $this->DAOFacultad->ObtenerFacultadByUsuario($prepareDQL);
                }
        }
?>