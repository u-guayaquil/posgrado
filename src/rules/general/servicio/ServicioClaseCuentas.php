<?php
            require_once("src/rules/general/dao/DAOClaseCuentas.php");
            
            class ServicioClaseCuentas
            {     private $DAOClaseCuentas;
     
                  function __construct()
                  {        $this->DAOClaseCuentas = new DAOClaseCuentas();
                  }
           
                  function BuscarClaseCuentas($prepareDQL)
                  {        return $this->DAOClaseCuentas->ObtenerClaseCuentas($prepareDQL);
                  }
            }
?>

