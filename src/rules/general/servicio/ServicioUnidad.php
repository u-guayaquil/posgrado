<?php
            require_once("src/rules/general/dao/DAOUnidad.php");
            require_once("src/rules/general/entidad/Unidad.php");
            
            class ServicioUnidad
            {     private $DAOUnidad;
     
                  function __construct()
                  {        $this->DAOUnidad = new DAOUnidad();
                  }
                  
                  function GuardaDBUnidad($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOUnidad->InsertaUnidad($Form);
                          }else{
                            return $this->DAOUnidad->ActualizaUnidad($Form);
                          }
                  }
                  
                  function DesactivaUnidad($Id)
                  {       return $this->DAOUnidad->DesactivaUnidad($Id);
                  }
           
                  function BuscarUnidadByID($id)
                  {        return $this->DAOUnidad->BuscarUnidadByDescripcion($id);
                  }

                  function BuscarUnidadByDescripcion($prepareDQL)
                  {        return $this->DAOUnidad->BuscarUnidadByDescripcion($prepareDQL);
                  } 
                  
                  function BuscarUnidadByArrayID($idArray)
                  {       return $this->DAOUnidad->ObtenerUnidadCombo($idArray);
                  }
                  
                  function BuscarUnidadByProducto($id)
                  {       return $this->DAOUnidad->BuscarUnidadByProducto($id);
                  }

            }
?>

