<?php
            require_once("src/rules/general/dao/DAOClaseAcreedor.php");
            require_once("src/rules/general/entidad/ClaseAcreedor.php");

            
            class ServicioClaseAcreedor
            {     private $DAOClaseAcreedor;
     
                  function __construct()
                  {        $this->DAOClaseAcreedor = new DAOClaseAcreedor();
                  }
           
                  function BuscarClaseAcreedorByID($id)
                  {        return $this->DAOClaseAcreedor->ObtenerClaseAcreedorByID($id);
                  }

                  function BuscarClaseAcreedorByDescripcion($prepareDQL)
                  {        return $this->DAOClaseAcreedor->BuscarClaseAcreedorByDescripcion($prepareDQL);                        
                  }
                  
                  function BuscarClaseAcreedorCombo()
                  {        return $this->DAOClaseAcreedor->ObtenerClaseAcreedorCombo();
                  }
            }
?>

