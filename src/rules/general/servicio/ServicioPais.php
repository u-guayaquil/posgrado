<?php
            require_once("src/rules/general/dao/DAOPais.php");
            
            class ServicioPais
            {       private $DAOPais;
                    function __construct()
                    {        $this->DAOPais = new DAOPais();
                    }
           
                    function GuardaDB($Form)
                    {       if (empty($Form->id)){
                                return $this->DAOPais->Inserta($Form);
                            }else{
                                return $this->DAOPais->Actualiza($Form);
                            }
                    }
                  
                    function Desactiva($Id)
                    {       return $this->DAOPais->Elimina($Id);
                    }
           
                    function BuscarByID($prepareDQL)
                    {       return $this->DAOPais->ObtenerByDescripcion($prepareDQL);   
                    }

                    function BuscarByDescripcion($prepareDQL)
                    {       return $this->DAOPais->ObtenerByDescripcion($prepareDQL);   
                    }
                  
                    function BuscarByArrayID($prepareDQL)
                    {       return $this->DAOPais->ObtenerByDescripcion($prepareDQL);   
                    }
            }
?>

