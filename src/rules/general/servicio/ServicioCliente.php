<?php
            require_once("src/rules/general/dao/DAOCliente.php");
            
            class ServicioCliente 
            {       private $DAOCliente;
                    private $Utils;
                    
                    function __construct()
                    {       $this->DAOCliente = new DAOCliente();
                            $this->Utils = new Utils();
                    }
                    
                    function GuardaDB($Form)
                    {       $Cedula[0]=true;
                            if ($Form->cedula!="9999999999" && $Form->cedula!="" && $Form->tipocliente!="J"){
                                $Cedula = $this->Utils->validaIndentificacion(2, $Form->cedula);
                            }    
                            $RUC[0]=true;
                            if ($Form->RUC!="9999999999999" && $Form->RUC!=""){
                                $RUC = $this->Utils->validaIndentificacion(1, $Form->RUC);
                            }                            
                            
                            
                            if ($Cedula[0])
                            {   if ($RUC[0])
                                {   $EsMail = strtolower(trim($Form->email)); 
                                    if ($this->EsMail($EsMail)) 
                                    {   $Respuesta = $this->EsCedulaRepetida($Form->idCliente,$Form->cedula,$Form->RUC);
                                        if ($Respuesta[0])
                                        {   if (empty($Form->idCliente))
                                                return $this->DAOCliente->InsertaCliente($Form);
                                            else
                                                return $this->DAOCliente->ActualizaCliente($Form);
                                        }
                                        else 
                                        return $Respuesta[1];
                                    }
                                    return "Ingrese una cuenta de correo valida.";
                                }
                                else
                                return "Cliente: ".$RUC[1];         
                            }
                            else
                            return "Cliente: ".$Cedula[1];         
                    }
                    
                    private function EsMail($Mail)
                    {       if (filter_var($Mail, FILTER_VALIDATE_EMAIL)) 
                            {   return true; 
                            }
                            return false;
                    }
                    
                    private function EsCedulaRepetida($Id,$Cedula,$RUC)
                    {       if (empty($Id)) $Id=0;

                            if (trim($Cedula)!="")
                            {   if ($this->DAOCliente->BuscaDocumentoRepetido(array('id'=>$Id,'cedula'=>$Cedula))>0)
                                return array(false,"La cedula del cliente esta repetida.");
                            }
                            elseif (trim($RUC)!="")
                            {   if ($this->DAOCliente->BuscaDocumentoRepetido(array('id'=>$Id,'ruc'=>$RUC))>0)
                                return array(false,"El R.U.C del cliente esta repetido.");
                            }     
                            return array(true,"");
                    }
                    
                    function DesactivaCliente($id)
                    {       return $this->DAOCliente->DesactivaCliente(intval($id)); 
                    }
                    
                    function BuscarCliente($prepareDQL)
                    {       return $this->DAOCliente->ObtenerDatosGeneral($prepareDQL);
                    }
                    
            }        
?>        
