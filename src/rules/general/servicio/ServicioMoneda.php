<?php
            require_once("src/rules/general/dao/DAOMoneda.php");
            require_once("src/rules/general/entidad/Moneda.php"); 
            
            class ServicioMoneda
            {     private $DAOMoneda;
     
                  function __construct()
                  {        $this->DAOMoneda = new DAOMoneda();
                  }
                  
                  function GuardaDBMoneda($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOMoneda->InsertaMoneda($Form);
                          }else{
                            return $this->DAOMoneda->ActualizaMoneda($Form);
                          }
                  }
                  
                  function DesactivaMoneda($Id)
                  {       return $this->DAOMoneda->DesactivaMoneda($Id);
                  }
           
                  function BuscarMonedaByID($id)
                  {        return $this->DAOMoneda->BuscarMonedaByDescripcion($id);
                  }

                  function BuscarMonedaByDescripcion($prepareDQL)
                  {        return $this->DAOMoneda->BuscarMonedaByDescripcion($prepareDQL);
                  }
                  
                  function SeleccionarTodos($PrepareDQL)
                  {        return $this->DAOMoneda->ObtenerTodos($PrepareDQL);
                  }

            }
?>

