<?php
        require_once("src/base/DBDAO.php");
        
        require_once("src/rules/general/entidad/Empleado.php");
        require_once("src/rules/general/entidad/ZonasCiudad.php");
        
        class DAOZonaCiudad extends DBDAO
        {       private static $entityZonasCiudad;
                private $Funciones;    
            
                public function __construct()
                {      self::$entityZonasCiudad = DBDAO::getConnection('PgProvider');
                       $this->Funciones = self::$entityZonasCiudad->MyFunction();
                }
            
                public function ObtenerZonaCiudad($PrepareDQL)
                {       $IDD  = array_key_exists("id", $PrepareDQL) ? " and z.id =".$PrepareDQL['id']: "";
                        $DSQL = "SELECT z.id, z.idzona,z.idciudad"
                              . ",n.descripcion as zona,c.descripcion as ciudad "
                              . "FROM general.zonas_ciudad z,general.zonas n,general.ciudad c "
                              . "WHERE z.idzona = n.id and  z.idciudad = c.id ".$IDD
                              . " ORDER BY z.id"
                              . " limit ".$PrepareDQL['limite']; 
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                public function InsertaZonaCiudad($datos)
                {      try{
                            $zona = $datos->idzona;
                            $ciudad = $datos->idciudad;
                            $Query = "INSERT INTO general.zonas_ciudad (idzona,idciudad) VALUES (".$zona.", ".$ciudad.") returning id";
                            $Rsid = $this->Funciones->Query($Query);
                            $id = $this->Funciones->FieldDataByName($Rsid,"id");
                            return intval($id);
                       } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo crear la Relacion..';
                       }
                }

                public function ActualizaZonaCiudad($datos)
                {      try{
                            $id = $datos->id;
                            $zona = $datos->idzona;
                            $ciudad = $datos->idciudad;
                            $Query = "UPDATE general.zonas_ciudad SET idzona= ".$zona.", idciudad= ".$ciudad."WHERE id= ".$id;
                            $this->Funciones->Query($Query);
                            return intval($datos->id);
                       } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Actualizar los Datos..';
                       }
                }
                
        }
