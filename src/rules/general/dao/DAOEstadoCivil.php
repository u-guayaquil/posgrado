<?php
        require_once("src/base/DBDAO.php");

        class DAOEstadoCivil extends DBDAO
        {     private static $entityEstadoCivil;
              private $Funciones;
              
              public function __construct()
              {      self::$entityEstadoCivil = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityEstadoCivil->MyFunction();
              }
    
              public function ObtenerEstadoCivil($PrepareDQL)
              {      $ID  = array_key_exists("id", $PrepareDQL) ? " WHERE e.id =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT e.id, e.descripcion"                           
                          . " FROM general.estadocivil e".$ID
                          . " ORDER BY e.id"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL)); 
              }
        }
