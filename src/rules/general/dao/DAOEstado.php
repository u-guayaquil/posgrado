<?php
        require_once("src/base/DBDAO.php");

        class DAOEstado extends DBDAO
        {       private static $entityEstado;
                private $Works;  
        
                public function __construct()
                {      self::$entityEstado = DBDAO::getConnection();
                       $this->Works = self::$entityEstado->MyFunction();                                  
                }

                public function ObtenerEstadoByID($id)
                {      $resourceID = $this->Works->Query("SELECT id,descripcion FROM BdCert.[dbo].[GEN_ESTADO] WHERE id='$id'");
                       return $this->Works->NextRecordObject($resourceID);                   
                }
              
                public function ObtenerEstadoArrayByID($IdArray)
                {      $arrayID = implode(',',$IdArray);
                       $SQL = "SELECT e.id, e.descripcion FROM BdCert..GEN_ESTADO e WHERE e.id in($arrayID) ORDER BY e.descripcion Asc "; 
                       $resourceID = $this->Works->Query($SQL);
                       
                       $Registro = array();
                       while($Filas = $this->Works->NextRecordAssoc($resourceID))
                       {     $Registro[] = $Filas;  
                       }
                       return $Registro; 
                }
              
                public function ObtenerTodosEstados()
                {      $SQL = "SELECT e.id, e.descripcion FROM BdCert.[dbo].[GEN_ESTADO] e ORDER BY e.descripcion Asc "; 
                       $resourceID = $this->Works->Query($SQL);
                       $Registro = array();
                       while($Filas = $this->Works->NextRecordAssoc($resourceID))
                       {     $Registro[] = $Filas;  
                       }
                       return $Registro; 
                }
        }

?>

