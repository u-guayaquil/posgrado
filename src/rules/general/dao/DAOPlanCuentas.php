<?php
        require_once("src/base/DBDAO.php");
        
        class DAOPlanCuentas extends DBDAO
        {   private static $entityPlanCuentas;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {       self::$entityPlanCuentas = DBDAO::getConnection('PgProvider');
                    $this->Funciones = self::$entityPlanCuentas->MyFunction();
                    $this->Fecha  = new DateControl();
            }
            
            public function ObtenerCuentaHijosPadre($IdPadre)
            {       $SQL = "select count(id) from general.planctas Where idpadre='$IdPadre'"; 
                    $RCD = $this->Funciones->Query($SQL);
                    return $this->Funciones->FieldData($RCD,0);
            }
            
            public function ObtenerCuentasPorPadre($IdPadre,$Tipo="")
            {       $Registro = array();
                    $SQL = "select id,codigo,descripcion,tipo,clase,idpadre,idestado from general.planctas Where idpadre='$IdPadre' ".($Tipo!="" ? " and tipo='$Tipo' ": ""); 
                    $RCD = $this->Funciones->Query($SQL);
                    while($Filas = $this->Funciones->NextRecordAssoc($RCD))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }
            
            public function ObtenerCuentas($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL)     ? " and p.id=".$PrepareDQL['id']: "";
                    $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("padre", $PrepareDQL)  ? " and p.idpadre = ".$PrepareDQL['padre']: "";
                    $JOIN.= array_key_exists("clase", $PrepareDQL)  ? " and p.clase in (".$PrepareDQL['clase'].") ": "";
                    $JOIN.= array_key_exists("tipo", $PrepareDQL)   ? " and p.tipo='".$PrepareDQL['tipo']."' ": "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ?  " and upper(p.descripcion) LIKE '%".$PrepareDQL['texto']."%' ": "";
                    $JOIN.= array_key_exists("codigo", $PrepareDQL) ? " and p.codigo LIKE '%".$PrepareDQL['codigo']."%' ": "";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                   
                    $DSQL = "SELECT p.id, ";
                    $DSQL.= "       p.codigo,p.descripcion, ";
                    $DSQL.= "       p.tipo, ";
                    $DSQL.= "       p.clase, ";
                    $DSQL.= "       p.idestado,e.descripcion as txtestado, ";
                    $DSQL.= "       p.idpadre,n.descripcion as txpadre, ";
                    $DSQL.= "       p.fecreacion ";
                    $DSQL.= "FROM general.estado e, general.planctas p ";
                    $DSQL.= "LEFT JOIN general.planctas n ON n.id=p.idpadre ";
                    $DSQL.= "WHERE e.id=p.idestado ".$JOIN;
                    $DSQL.= "ORDER BY p.codigo ".$LIMT;
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                               
            }
	
            public function InsertaCuenta($datos,$opcion=0)
            {       $codigo = $datos->codigo;
                    $descrp = strtoupper(trim($datos->descripcion));
                    $tipo  = $datos->tipo;
                    $clase = $datos->clase;
                    $padre = $datos->idpadre;
                    $users = $_SESSION['IDUSER'];
                    $fecha = $this->Fecha->getNowDateTime('DATETIME');
                    $statu = 1;
                    if ($this->ExisteCodigo($codigo)==0)
                    {   $DSQL = "INSERT INTO general.planctas (codigo,descripcion,tipo,clase,idpadre,idestado,iduscreacion,fecreacion) ";
                        $DSQL.= "VALUES ('$codigo','$descrp','$tipo','$clase','$padre','$statu','$users','$fecha') returning id ";
                        return intval($this->Funciones->FieldDataByName($this->Funciones->Query($DSQL),"id"));
                    }
                    else
                    return "El código de la cuenta ya existe.";
            }

            public function InsertaFileCuenta($Datos)
            {       $RCID = $this->Funciones->Query("DELETE FROM general.planctas");

                    self::$entityPlanCuentas->MyTransaction('BEGIN');
                    for ($j=0; $j<count($Datos); $j++)
                    {   $idcta = $Datos[$j][0];
                        $codigo = trim($Datos[$j][1]);
                        $descrp = strtoupper(trim($Datos[$j][2]));
                        $tipo  = (trim($Datos[$j][3])=='D' || trim($Datos[$j][3])=='M' ? 'M' : 'G');
                        $clase = $Datos[$j][4];
                        $padre = $Datos[$j][5];
                        $statu = $Datos[$j][6];
                        $users = $_SESSION['IDUSER'];
                        $fecha = $this->Fecha->getNowDateTime('DATETIME');
                        if ($this->ExisteCodigo($codigo)==0)
                        {   $DSQL = "INSERT INTO general.planctas (id,codigo,descripcion,tipo,clase,idpadre,idestado,iduscreacion,fecreacion) ";
                            $DSQL.= "VALUES ('$idcta','$codigo','$descrp','$tipo','$clase','$padre','$statu','$users','$fecha') returning id ";
                            $this->Funciones->Query($DSQL);
                        }
                        else
                        {   self::$entityPlanCuentas->MyTransaction('ROLLBACK');
                            return "El código de la cuenta ya existe.";
                        }
                    }
                    self::$entityPlanCuentas->MyTransaction('COMMIT');
                    return "El plan de cuentas se cargó correctamente.";
            }
                
            public function ActualizaCuenta($datos)
            {       $id = $datos->idplancta;
                    $codigo= $datos->codigo;
                    $descrp= strtoupper(trim($datos->descripcion));
                    $tipo  = $datos->tipo;
                    $clase = $datos->clase;
                    $padre = $datos->idpadre;
                    $fecha = $this->Fecha->getNowDateTime('DATETIME');
                    $statu = $datos->estado;
                    $users = $_SESSION['IDUSER'];
                    if ($this->ExisteCodigo($codigo,$id)==0)
                    {   $Dato = $this->ObtenerCuentas(array('id'=>$id));
                        if ($Dato[0]['idestado']==0 && $statu==1) $this->DesactivaCuenta($id,1);
                        
                        $DSQL = "UPDATE general.planctas ";
                        $DSQL.= "SET codigo='$codigo', descripcion='$descrp', tipo='$tipo', clase='$clase', idpadre='$padre', idestado='$statu', idusmodifica='$users', femodificacion='$fecha' ";
                        $DSQL.= "WHERE id='$id' "; 
                        $this->Funciones->Query($DSQL);
                        return intval($id);
                    }
                    else
                    return "El código de la cuenta ya existe.";    
            }
            
            private function ExisteCodigo($Codigo,$Excluir=0)
            {       $Registro = 0;
                    $RCD = $this->Funciones->Query("select id from general.planctas Where codigo='$Codigo'".($Excluir==0 ? "" : " and id<>'$Excluir' ") );
                    while($Filas = $this->Funciones->NextRecord($RCD))
                    {     $Registro++;  
                    }
                    return $Registro; 
            }
            
            public function DesactivaCuenta($id,$status=0)
            {       $Users = $_SESSION['IDUSER'];
                    $Fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "Update general.planctas Set idestado='$status', idusmodifica='$Users', femodificacion='$Fecha' Where id='$id'";
                    $this->Funciones->Query($Query);
                    $this->DesactivaCuentaSgte($id,$status);
                    return intval($id);
            }
            
            private function DesactivaCuentaSgte($IdPadre,$status=0)
            {       $Users = $_SESSION['IDUSER'];
                    $Fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Roots = $this->ObtenerCuentasPorPadre($IdPadre);
                    foreach ($Roots as $Sheet)
                    {       $id = intval($Sheet['id']);
                            $qr = "Update general.planctas Set idestado='$status', idusmodifica='$Users', femodificacion='$Fecha' Where id='$id'";    
                            $this->Funciones->Query($qr);
                            if ($Sheet['tipo']=="G") $this->DesactivaCuentaSgte($id,$status);
                    }
            }
            
        }
?>        
