<?php

        require_once("src/base/DBDAO.php");

        class DAOTiempo extends DBDAO
        {     private static $entityTiempo;
              private $Funciones;

              public function __construct()
              {      self::$entityTiempo = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityTiempo->MyFunction();
              }
              
              public function ObtenerTiempoByID($id)
              {      $DSQL = "SELECT t.id, t.descripcion"                           
                          . " FROM general.Tiempo t where t.id=".$id
                          . " ORDER BY t.id"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function ObtenerTiempoByArrayID($IdArray)
              {      $DSQL = "SELECT t.id, t.descripcion"                           
                          . " FROM general.tiempo t where t.id in(".$IdArray.")"
                          . " ORDER BY t.id"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function ObtenerTodosTiempos()
              {      $DSQL = "SELECT t.id, t.descripcion"                           
                          . " FROM general.Tiempo t"
                          . " ORDER BY t.id"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }

        }

?>

