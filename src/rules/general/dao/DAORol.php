<?php
        require_once("src/base/DBDAO.php");

        class DAORol extends DBDAO
        {       private static $entityRol;
                private $Works;  
        
                public function __construct()
                {      self::$entityRol = DBDAO::getConnection();
                       $this->Works = self::$entityRol->MyFunction();                                  
                }

                public function ObtenerRolArrayByID($IdArray)
                {      $arrayID = implode(',',$IdArray);
                       $SQL = "SELECT e.ID, e.DESCRIPCION FROM BdCert..SEG_ROL e WHERE e.ID in($arrayID) ORDER BY e.ID Asc"; 
                       $resourceID = $this->Works->Query($SQL);
                       return $this->Works->FillDataRecordAssoc($resourceID,"No existen roles definidos."); 
                }
                
                public function ObtenerTodosRoles()
                {      $SQL = "SELECT e.ID, e.DESCRIPCION FROM BdCert..SEG_ROL e ORDER BY e.ID Asc"; 
                       $resourceID = $this->Works->Query($SQL);
                       return $this->Works->FillDataRecordAssoc($resourceID,"No existen roles definidos.");  
                }
                
                public function ObtenerRolModulo($WithDefault = 0)
                {       $Modulo = json_decode(Session::getValue('Sesion'));
                        $SQL = "SELECT Y.ID, Y.DESCRIPCION 
                                 FROM BdCert..SEG_MODULO_ROL AS X INNER JOIN BdCert..SEG_ROL AS Y ON X.IDROL=Y.ID 
                                 WHERE X.IDMODULO='".$Modulo->Idmodulo."' ";
                        
                        if ($WithDefault==1)
                        {   $SQL.= "UNION        
                                     SELECT Y.ID, Y.DESCRIPCION
                                     FROM BdCert..SEG_ROL AS Y
                                     WHERE Y.ID=0
                                     ORDER BY Y.ID";
                        }
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen roles definidos.");  
                }        
                
        }

?>