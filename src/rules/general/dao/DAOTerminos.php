<?php
        require_once("src/base/DBDAO.php");

        class DAOTerminos extends DBDAO
        {   private static $entityTerminos;
            private $Works;
            private $Fecha;
        
            public function __construct()
            {      self::$entityTerminos = DBDAO::getConnection('PgProvider');
                   $this->Works = self::$entityTerminos->MyFunction();                
                   $this->Fecha = new DateControl();
            }

            public function ObtenerTerminosByID($id)
            {      $resourceID = $this->Works->Query("SELECT t.* FROM general.terminos t WHERE t.id='$id' ");
                   return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function ObtenerTerminosByDescripcion($PrepareDQL)
            {       $JOIN = "";
                    if (array_key_exists("estado", $PrepareDQL))
                    {   $arrayID = implode(',',$PrepareDQL['estado']);
                        $JOIN = " and s.idestado in ($arrayID) ";
                    }
                    $SDQL = "SELECT t.id, t.descripcion, t.valor, t.interes, t.fecreacion ,d.id idxtiempo , e.id as idxestado, e.descripcion as txtestado "
                          . "FROM general.estado e, general.terminos t, general.tiempo d "
                          . "WHERE t.id>0 and t.idestado=e.id and t.idtiempo=d.id and upper(t.descripcion) "
                          . "LIKE '%".$PrepareDQL['texto']."%' "
                          . "ORDER BY t.id " 
                          . "LIMIT ".$PrepareDQL['limite'];
                    
                    $Registro = array();
                    $resourceID = $this->Works->Query($SDQL);
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }

            public function ObtenerTerminosEstadoByID($id)
            {       $Registro = array();
                    $DSQL = "Select  t.id,t.descripcion,t.valor,t.interes,t.fecreacion,t.idestado as idxestado,e.descripcion as txtestado,d.id as idxtiempo "
                         . "From general.terminos as t, general.tiempo as d, general.estado as e "
                         . "Where t.idtiempo = d.id and t.idestado=e.id and t.id='$id' ";   

                    $resourceID = $this->Works->Query($DSQL);
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }
            
            public function ObtenerTerminosByEstadoID($id)
            {       $Registro = array();
                    $DSQL = "Select  t.id,t.descripcion,t.valor,t.interes,t.fecreacion,t.idestado as idxestado,e.descripcion as txtestado,d.id as idxtiempo "
                         . "From general.terminos as t, general.tiempo as d, general.estado as e "
                         . "Where t.idtiempo = d.id and t.idestado=e.id and e.id='$id' "
                         . "Order by t.id ";

                    $resourceID = $this->Works->Query($DSQL);
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }


            public function InsertaTerminos($Form)
            {       $Fecha  = new DateControl();

                    $Descrp = strtoupper($Form->descripcion);
                    $ValorT = intval($Form->valor);
                    $Vatasa = floatval($Form->interes);
                    $Fechas = $Fecha->getNowDateTime('DATETIME');
                    $resourceID = $this->Works->Query("insert into general.terminos(descripcion,idtiempo,valor,interes,idestado,iduscreacion,fecreacion) values('$Descrp','$Form->idtiempo','$ValorT','$Vatasa','$Form->estado',".$_SESSION['IDUSER'].",'$Fechas')  returning id ");    
                    return $this->Works->FieldDataByName($resourceID,'id');                
            }
    
            public function ActualizaTerminos($Form)
            {       $Fechas = $this->Fecha->getNowDateTime('DATETIME');
                    $Descrp = strtoupper($Form->descripcion);
                    $ValorT = intval($Form->valor);
                    $Vatasa = floatval($Form->interes);
                    $resourceID = $this->Works->Query("update general.terminos set valor ='$ValorT' ,interes='$Vatasa',idtiempo='$Form->idtiempo',descripcion='$Descrp', idestado='$Form->estado', femodificacion='$Fechas', idusmodifica=".$_SESSION['IDUSER']." where id='$Form->id' ");    
                    return intval($Form->id);                            
            }
            
            public function DesactivaTerminos($id)
            {       $Fechas = $this->Fecha->getNowDateTime('DATETIME');
                    $resourceID = $this->Works->Query("update general.terminos set idestado=0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion='$Fechas' where id='$id' ");    
                    return 1;                                        
            }
        }
