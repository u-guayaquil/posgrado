<?php
        require_once("src/base/DBDAO.php");

        class DAOPais extends DBDAO
        {     private static $entityPais;
              private $Funciones;
              private $Fecha;
              
                public function __construct()
                {       self::$entityPais = DBDAO::getConnection('PgProvider');
                        $this->Funciones = self::$entityPais->MyFunction();
                        $this->Fecha  = new DateControl();
                }
    
                public function ObtenerByDescripcion($PrepareDQL)
                {       $JOIN = array_key_exists("id", $PrepareDQL) ? " and p.id=".$PrepareDQL['id'] : "";
                        $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estado'].")" : "";
                        $JOIN.= array_key_exists("texto", $PrepareDQL) ?  " and upper(p.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                        $ORDN = array_key_exists("orden", $PrepareDQL) ?  $PrepareDQL['orden'] : " p.descripcion ";
                        $LIMT = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                        
                        $DSQL = "SELECT ";
                        $DSQL.= "p.id,p.descripcion,";
                        $DSQL.= "p.codigo,p.abreviatura,";
                        $DSQL.= "p.idestado,e.descripcion as txtestado,";
                        $DSQL.= "p.fecreacion "; 
                        $DSQL.= "FROM general.pais p, general.estado e ";
                        $DSQL.= "WHERE p.idestado=e.id ".$JOIN;
                        $DSQL.= "ORDER BY ".$ORDN.$LIMT;
                        
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                public function Inserta($datos)
                {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                        $codigo = strtoupper($datos->codigo);
                        $descripcion = strtoupper(trim($datos->descripcion));
                        $abreviatura= strtoupper(trim($datos->abreviatura));
                        
                        $Query = "INSERT INTO general.pais (descripcion,codigo,abreviatura,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('$descripcion', '$codigo','$abreviatura', '$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id";
                        return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                }
    
                public function Actualiza($datos)
                {       $fecha = $this->Fecha->getNowDateTime('DATETIME');
                        $codigo = strtoupper($datos->codigo);
                        $descripcion = strtoupper(trim($datos->descripcion));
                        $abreviatura = strtoupper(trim($datos->abreviatura));
                        
                        $Query = "UPDATE general.pais "
                               . "SET "
                               . "codigo='$codigo', "
                               . "descripcion='$descripcion', "
                               . "abreviatura='$abreviatura', "
                               . "idestado='$datos->estado', "
                               . "idusmodifica= ".$_SESSION['IDUSER'].", "
                               . "femodificacion='$fecha' "
                               . "WHERE id= ".$datos->id;
                        $this->Funciones->Query($Query);
                        return $datos->id;
                }
    
                public function Elimina($id)
                {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.pais SET idestado=0, idusmodifica=".$_SESSION['IDUSER'].", femodificacion= '$fecha' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                }
              
        }
