<?php
        require_once("src/base/DBDAO.php");

        class DAOUnidadConversion extends DBDAO
        {     private static $entityUnidadConversion;
              private $Funciones;
              
              public function __construct()
              {      self::$entityUnidadConversion = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityUnidadConversion->MyFunction();
              }
              
              function BuscarUnidadConversion($PrepareDQL,$id)
              {      $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE c.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $ORG  = array_key_exists("origen", $PrepareDQL) ? " WHERE c.und_origen = ".$PrepareDQL['origen']." "
                                                                        . " and c.und_destino=".$PrepareDQL['destino']: " ";
                     if($id!=0){
                         $DIF = " and c.id<>".$id;
                     }else{ $DIF = ""; }
                     
                     $DSQL = "SELECT c.id,c.und_origen,c.und_destino,c.und_factor"
                           . ", u.descripcion as origen, n.descripcion as destino"
                           . " FROM general.unidad_conversion c"
                           . " INNER JOIN general.unidad u ON c.und_origen = u.id"
                           . " INNER JOIN general.unidad n ON c.und_destino = n.id ".$IDD.$ORG.$DIF
                           . " ORDER BY c.id".$LIM;                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));  
              }
              
              public function InsertaUnidadConversion($datos)
              {     try{
                        $origen = $datos->Origen;
                        $destino = $datos->Destino;
                        $factor = $datos->factor;                      
                        $Query = "INSERT INTO general.unidad_conversion (und_origen,und_destino,und_factor)"
                               . "VALUES (".$origen.", ".$destino.", ".$factor.") returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el UnidadConversion..';
                    }
              }
    
              public function ActualizaUnidadConversion($datos)
              {      try{
                        $id = $datos->id;
                        $origen = $datos->Origen;
                        $destino = $datos->Destino;
                        $factor = $datos->factor;                           
                        $Query = "UPDATE general.unidad_conversion "
                               . "SET und_origen= ".$origen.",und_destino = ".$destino.", und_factor= ".$factor
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }  
              }   
              
        }
