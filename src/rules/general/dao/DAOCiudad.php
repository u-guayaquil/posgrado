<?php
        require_once("src/base/DBDAO.php");

        class DAOCiudad extends DBDAO
        {     private static $entityCiudad;
              private $Funciones;
              private $Fecha;
              
            public function __construct()
            {       self::$entityCiudad = DBDAO::getConnection('PgProvider');
                    $this->Funciones = self::$entityCiudad->MyFunction();
                    $this->Fecha  = new DateControl();
            }
    
            public function ObtenerCiudadByDescripcion($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ? " and c.id=".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and c.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ?  " and upper(c.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                    $ORDN = array_key_exists("orden", $PrepareDQL) ?  $PrepareDQL['orden'] : " c.descripcion ";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                    
                    $DSQL = "SELECT c.id, c.descripcion, c.fecreacion, e.id as idestado, e.descripcion as txtestado,c.codigo, p.descripcion as provincia, c.idprovincia, pa.descripcion as pais, p.idpais "
                          . "FROM general.ciudad c, general.estado e, general.provincia p, general.pais pa "
                          . "WHERE c.idestado=e.id and c.idprovincia = p.id and p.idpais=pa.id ".$JOIN
                          . "ORDER BY  ".$ORDN.$LIMT;            
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
            
            public function InsertaCiudad($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $codigo = strtoupper($datos->codigo);
                    $descripcion = strtoupper(trim($datos->descripcion));
                    $Query = "INSERT INTO general.ciudad (descripcion,codigo,idprovincia,idestado,iduscreacion,fecreacion)"
                           . "VALUES ('$descripcion', '$codigo','$datos->idprovincia','$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id";
                    return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
            }
    
            public function ActualizaCiudad($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $codigo = strtoupper($datos->codigo);
                    $descripcion = strtoupper(trim($datos->descripcion));
                    $Query = "UPDATE general.ciudad "
                           . "SET "
                           . "descripcion='$descripcion', "
                           . "codigo='$codigo', "
                           . "idprovincia='$datos->idprovincia', "
                           . "idusmodifica=".$_SESSION['IDUSER'].", "
                           . "femodificacion='$fecha', "
                           . "idestado='$datos->estado' "
                           . "WHERE id=".$datos->id;
                    $this->Funciones->Query($Query);
                    return intval($datos->id);
            }
    
            public function DesactivaCiudad($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE general.Ciudad SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion='$fecha' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }
            
        }
