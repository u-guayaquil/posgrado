<?php
        require_once("src/base/DBDAO.php");
        
        class DAOCliente extends DBDAO
        {   private static $entityCliente;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {      self::$entityCliente = DBDAO::getConnection('PgProvider');
                   $this->Funciones = self::$entityCliente->MyFunction();
                   $this->Fecha  = new DateControl();
            }

            public function ObtenerDatosGeneral($PrepareDQL)
            {       $JOIN = array_key_exists("id",    $PrepareDQL) ? " and c.id=".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("estado",$PrepareDQL) ? " and c.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ? " and upper(c.nombre) LIKE '%".$PrepareDQL['texto']."%'" : "";
                    $ORDN = array_key_exists("orden", $PrepareDQL) ? $PrepareDQL['orden'] : " c.nombre ";
                    $LIMT = array_key_exists("limite",$PrepareDQL) ? " limit ".$PrepareDQL['limite'] : "";

                    $DSQL = "SELECT c.id,c.codigo,
                                    c.fechacliente,
                                    c.nombre,
                                    c.representante,
                                    c.cedula,
                                    c.ruc,
                                    c.idtipocliente,c.idgrupo,c.idtermino,
                                    c.valcupo as cupo,
                                    c.idvendedor,m.nombre as nomvendedor, m.apellido as apevendedor,
                                    c.idcltefact,t.nombre as clientefactura,
                                    c.idciudad,d.descripcion as ciudad,
                                    c.idparroquia,q.descripcion as parroquia,
                                    c.direccion,c.email,
                                    c.telefono,c.movil,
                                    c.idestadocivil,c.sexo,
                                    c.oblcontabilidad,
                                    c.ctaxcobrar,p.descripcion as cuentaporcobrar,
                                    c.ctaanticip,n.descripcion as cuentaanticipo,
                                    c.ctaexcedente,x.descripcion as excedente,
                                    e.id as idestado,e.descripcion as txtestado,
                                    c.fecreacion
                            FROM    general.parroquia q,
                                    general.ciudad d,
                                    general.estadocivil v,
                                    general.estado e, 
                                    general.cliente c 
                                    LEFT JOIN general.planctas p ON c.ctaxcobrar=p.id
                                    LEFT JOIN general.planctas n ON c.ctaanticip=n.id
                                    LEFT JOIN general.planctas x ON c.ctaexcedente=x.id
                                    LEFT JOIN general.empleado m ON c.idvendedor=m.id
                                    LEFT JOIN general.cliente t  ON c.idcltefact=t.id
                            WHERE   c.idestado=e.id and c.idestadocivil=v.id and c.idciudad=d.id and c.idparroquia=q.id ".$JOIN."      
                            ORDER BY ".$ORDN.$LIMT;
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                               
            }

            public function InsertaCliente($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME'); 
                    $codigo = trim($datos->codigo);
                    $nombre = strtoupper(trim($datos->nombre));
                    $representante = strtoupper(trim($datos->replegal));
                    $cedula = trim($datos->cedula);
                    $ruc = trim($datos->RUC);
                    $tipocliente = $datos->tipocliente;

                    $obligacontabilidad = intval($datos->obligacontabilidad);
                    if ($tipocliente=="J"){
                        $obligacontabilidad = 1;
                    }    

                    $grupocliente = intval($datos->grupocliente);
                    $terminos = intval($datos->terminos);
                    $fechacliente = $datos->fechacliente;
                    $idVendedor = intval($datos->idVendedor);
                    $sexo = $datos->sexo;
                    $estadocivil = intval($datos->estadocivil);
                    $ciudad = intval($datos->idCiudad);
                    $direccion = strtoupper(trim($datos->direccion));
                    $telefono = trim($datos->telefono);
                    $email = strtoupper(trim($datos->email));
                    $movil = trim($datos->movil);
                    $porcobrar = intval($datos->idCuentaPorCobrar);
                    $anticipo = intval($datos->idCuentaAnticipo);
                    $cupo = floatval($datos->cupo);
                    $parroquia = intval($datos->idParroquia);
                    $excedente = intval($datos->idCuentaExcedente);
                    $clientefactura = intval($datos->idClienteFactura);
                    $estado = intval($datos->estado);
                    
                    $Query = "INSERT INTO 
                              general.cliente (codigo,nombre,representante,cedula,ruc,idtipocliente,oblcontabilidad,idgrupo,idtermino,idciudad,direccion,telefono,sexo,movil,fechacliente,email,ctaxcobrar,ctaanticip,idvendedor,idestadocivil,idestado,iduscreacion,fecreacion,valcupo,idparroquia,ctaexcedente,idcltefact)
                              VALUES ('$codigo','$nombre','$representante','$cedula','$ruc','$tipocliente','$obligacontabilidad','$grupocliente','$terminos','$ciudad','$direccion','$telefono','$sexo','$movil','$fechacliente','$email','$porcobrar','$anticipo','$idVendedor','$estadocivil','$estado',".$_SESSION['IDUSER'].",'$fecha','$cupo','$parroquia','$excedente','$clientefactura') returning id ";
                    
                    return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
            }     
            
            public function ActualizaCliente($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME'); 
                    $id = $datos->idCliente;
                    $codigo = trim($datos->codigo);
                    $nombre = strtoupper(trim($datos->nombre));
                    $representante = strtoupper(trim($datos->replegal));
                    $cedula = trim($datos->cedula);
                    $ruc = trim($datos->RUC);
                    $tipocliente = $datos->tipocliente;
                    
                    $obligacontabilidad = intval($datos->obligacontabilidad);
                    if ($tipocliente=="J"){
                        $obligacontabilidad = 1;
                    }    
                    
                    $grupocliente = intval($datos->grupocliente);
                    $terminos = intval($datos->terminos);
                    $fechacliente = $datos->fechacliente;
                    $idVendedor = intval($datos->idVendedor);
                    $sexo = $datos->sexo;
                    $estadocivil = intval($datos->estadocivil);
                    $ciudad = intval($datos->idCiudad);
                    $direccion = strtoupper(trim($datos->direccion));
                    $telefono = trim($datos->telefono);
                    $email = strtoupper(trim($datos->email));
                    $movil = trim($datos->movil);
                    $porcobrar = intval($datos->idCuentaPorCobrar);
                    $anticipo = intval($datos->idCuentaAnticipo);
                    $cupo = floatval($datos->cupo);
                    $parroquia = intval($datos->idParroquia);
                    $excedente = intval($datos->idCuentaExcedente);
                    $clientefactura = intval($datos->idClienteFactura);
                    $estado = intval($datos->estado);
    
                    $Query = "UPDATE general.cliente " 
                           . "SET "
                           . "codigo= '$codigo',"
                           . "nombre='$nombre',"
                           . "representante='$representante',"
                           . "cedula='$cedula',"
                           . "ruc='$ruc',"
                           . "idtipocliente='$tipocliente',"
                           . "oblcontabilidad='$obligacontabilidad',"
                           . "idgrupo='$grupocliente',"
                           . "idciudad='$ciudad',"
                           . "direccion='$direccion',"
                           . "idtermino='$terminos',"
                           . "movil='$movil',"
                           . "fechacliente='$fechacliente',"
                           . "telefono='$telefono',"
                           . "sexo='$sexo',"
                           . "email='$email',"
                           . "ctaxcobrar='$porcobrar',"
                           . "ctaanticip='$anticipo',"
                           . "idvendedor='$idVendedor',"
                           . "idestadocivil='$estadocivil',"
                           . "idestado='$estado',"
                           . "idusmodifica=".$_SESSION['IDUSER'].","
                           . "femodificacion='$fecha',"
                           . "valcupo='$cupo',"
                           . "idparroquia='$parroquia',"
                           . "ctaexcedente='$excedente',"
                           . "idcltefact='$clientefactura' "
                           . "WHERE id=".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }
            
            public function DesactivaCliente($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE general.cliente SET idestado=0, idusmodifica=".$_SESSION['IDUSER'].", femodificacion='$fecha' WHERE id='$id' ";
                    $this->Funciones->Query($Query);
                    return intval($id);
            }
            
            public function BuscaDocumentoRepetido($PrepareDQL)
            {       $JOIN = array_key_exists("id",    $PrepareDQL) ? "  and c.id<>".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("cedula", $PrepareDQL) ? " and c.cedula='".$PrepareDQL['cedula']."'" : "";
                    $JOIN.= array_key_exists("ruc",    $PrepareDQL) ? " and c.ruc='".$PrepareDQL['ruc']."'" : "";
                    $Query="Select count(c.id) as repetido from general.cliente as c, general.estado as e where c.idestado=e.id ".$JOIN;
                    return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"repetido");
            }
            
        }
?>        
