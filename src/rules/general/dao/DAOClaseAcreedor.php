<?php
        require_once("src/base/DBDAO.php");

        class DAOClaseAcreedor extends DBDAO
        {     private static $entityClaseAcreedor;
              private $Funciones;
              
              public function __construct()
              {      self::$entityClaseAcreedor = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityClaseAcreedor->MyFunction();
              }
    
              public function ObtenerClaseAcreedorByID($id)
              {      $DSQL = "SELECT id,descripcion"
                           . " FROM general.claseacreedor WHERE id = ".$id
                           . " ORDER BY id";                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }

              public function ObtenerClaseAcreedorByDescripcion($PrepareDQL,$DSQL)
              {      $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                     $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $DSQL = "SELECT id,descripcion"
                           . " FROM general.claseacreedor ".$TXT
                           . " ORDER BY id ".$LIM;                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function ObtenerClaseAcreedorCombo()
              {      $DSQL = "SELECT id,descripcion"
                           . " FROM general.claseacreedor "
                           . " ORDER BY id";                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }


        }
