<?php

require_once("src/base/DBDAO.php");

class DAOEmpresa extends DBDAO
{       private static $entityEmpresa;
        private $Works;
        
        public function __construct()
        {      self::$entityEmpresa = DBDAO::getConnection('PgProvider');
               $this->Works = self::$entityEmpresa->MyFunction();
        }   
    
        public function ObtenerEmpresa()
        {       $SDQL = "Select e.comercial,e.razonsocial,e.tipo_ident_razon,e.num_ident_razon,e.representante,e.tipo_ident_repres,e.num_ident_repres, ";
                $SDQL.= "e.idsucursal,s.descripcion,e.idmoneda,e.contador,e.tipo_ident_contador,e.num_ident_contador,e.esp_contribuyente,e.obl_contabilidad,e.idpais ";
                $SDQL.= "From general.empresa as e, general.sucursal as s where e.idsucursal=s.id and e.id=1 ";
                $resourceID = $this->Works->Query($SDQL);
                return $this->Works->NextRecordObject($resourceID); 
        } 
        
        public function EditaEmpresa($Form)
        {       $comercial = strtoupper($Form->comercial);
                $razon = strtoupper($Form->razon);
                $legal = strtoupper($Form->legal);
                $contador = strtoupper($Form->contador);
                $nucontribuyente = intval($Form->nucontribuyente);
            
                $DSQL = "update general.empresa set comercial='$comercial', razonsocial='$razon', ";
                $DSQL.= "tipo_ident_razon='$Form->idrazon', num_ident_razon='$Form->nurazon', representante='$legal',  ";
                $DSQL.= "tipo_ident_repres='$Form->idlegal', num_ident_repres='$Form->nulegal', idsucursal='$Form->idsucursal', idmoneda='$Form->idmoneda', ";
                $DSQL.= "contador='$contador', tipo_ident_contador='$Form->idcontador', num_ident_contador='$Form->nucontador', ";
                $DSQL.= "esp_contribuyente='$nucontribuyente', obl_contabilidad='$Form->obligado', idpais='$Form->idpais' ";
                $DSQL.= "where id=1 ";
                $resourceID = $this->Works->Query($DSQL);
                return true;
        }
}


