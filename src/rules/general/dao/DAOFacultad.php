<?php

        require_once("src/base/DBDAO.php");

        class DAOFacultad extends DBDAO
        {       private static $entityFacultad;
                private $Works;
        
                public function __construct()
                {      self::$entityFacultad = DBDAO::getConnection();
                       $this->Works = self::$entityFacultad->MyFunction();
                }
                
                public function ObtenerFacultadByID($id)
                {       $SDQL = "SELECT COD_FACULTAD,NOMBRE FROM TB_FACULTAD WHERE COD_FACULTAD='$id' AND COD_FACULTAD NOT IN('26','27','28','32') ";
                        return $this->Works->RecorreDatos($this->Funciones->Query($DSQL));                                   
                }
    
                public function ObtenerFacultadByDescripcion($PrepareDQL)
                {       $LIKE = array_key_exists("texto", $PrepareDQL) ?  " AND UPPER(NOMBRE) like '%".$PrepareDQL['texto']."%' ": " ";      
                        $LMIT = array_key_exists("limite", $PrepareDQL) ? " TOP ".$PrepareDQL['limite']: " ";      
                        $DSQL = "SELECT ".$LMIT." COD_FACULTAD,NOMBRE FROM TB_FACULTAD WHERE COD_FACULTAD NOT IN('26','27','28','32') ".$LIKE." ORDER BY NOMBRE ASC";
                        return $this->Works->RecorreDatos($this->Works->Query($DSQL));                           
                }
                
                public function ObtenerFacultadByDocente($prepareDQL)
                {       $SQL1="";
                        if (array_key_exists("facultad", $prepareDQL))
                        {   if (count($prepareDQL['facultad'])>0)
                            {   $FacultadID = implode(',',$prepareDQL['facultad']);
                                $SQL1 = "WHERE X.ID IN ($FacultadID) ";
                            }
                        }
                        $iddocente = $prepareDQL['docente'];
                        
                        $SQL = "SELECT X.ID,'$iddocente' AS DC,X.DESCRIPCION,ISNULL(Y.IDDOCENTE,0) AS AP
                                FROM POSTGRADO..ADM_FACULTAD AS X LEFT JOIN POSTGRADO..COL_DOCENTE_FACULTAD AS Y ON Y.IDDOCENTE=$iddocente AND X.ID=Y.IDFACULTAD ".$SQL1;
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existe facultades definida para su perfil.");       
                }
                
                public function ObtenerFacultadByUsuario($prepareDQL)
                {       $idusrol = $prepareDQL['idusrol'];
                      
                        $SQL = "SELECT X.ID,'$idusrol' AS US,X.DESCRIPCION,ISNULL(Y.IDUSROL,0) AS UR
                                FROM POSTGRADO..ADM_FACULTAD AS X LEFT JOIN BdCert..SEG_USUARIO_FILTRO AS Y ON Y.IDUSROL=$idusrol AND X.ID=Y.IDFACULTAD ";
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existe facultades definida.");       
                }
                
        }        
?>