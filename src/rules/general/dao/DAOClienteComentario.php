<?php
        require_once("src/base/DBDAO.php");

        class DAOClienteComentario extends DBDAO
        {     private static $entityClienteComentario;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityClienteComentario = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityClienteComentario->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarClienteComentarioByDescripcion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and c.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(c.comentario) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and c.id =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT c.id, c.comentario, c.fecreacion, e.id as idestado,c.fecha"
                           . ", e.descripcion as txtestado, l.nombre as cliente, c.idcliente "
                           . " FROM general.cliente_comentario c, general.Estado e, general.cliente l "
                           . " WHERE c.idestado=e.id and c.idcliente = l.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY c.id"
                           . " limit ".$PrepareDQL['limite'];
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));     
              }
              
              public function InsertaClienteComentario($datos)
              {     try{
                        $estado = $datos->estado;
                        $comentario = strtoupper($datos->comentario);
                        $idcliente = ($datos->idCliente);
                        $fecha = ($datos->fecha);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO general.cliente_comentario (comentario,idcliente,fecha,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$comentario."', ".$idcliente.", to_timestamp('".$fecha."','YYYY-MM-DD'), ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo guardar los datos..';
                    }
              }
              
              public function ActualizaClienteComentario($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $comentario = strtoupper($datos->comentario);
                        $idcliente = ($datos->idCliente);
                        $fecha = ($datos->fecha);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.cliente_comentario "
                               . "SET comentario= '".$comentario."', idcliente= ".$idcliente.", fecha= to_timestamp('".$fecha."','YYYY-MM-DD')"
                               . ",idestado = ".$estado.", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaClienteComentario($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.cliente_comentario SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Comentario..';
                        }
              }
        }
