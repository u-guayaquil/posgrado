<?php
        require_once("src/base/DBDAO.php");

        class DAOClaseCuentas extends DBDAO
        {       private static $entityClaseCuentas;
                private $Funciones;
              
              public function __construct()
              {      self::$entityClaseCuentas = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityClaseCuentas->MyFunction();
              }
    
              public function ObtenerClaseCuentas($PrepareDQL)
            {        $LIKE = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(p.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE p.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     
                     $DSQL = "SELECT p.id,p.descripcion"
                          . " FROM general.clasectas p ".$LIKE.$IDD
                          . " ORDER BY p.id".$LIM; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                               
            }
        }
