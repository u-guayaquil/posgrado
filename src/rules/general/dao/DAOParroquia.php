<?php
        require_once("src/base/DBDAO.php");

        class DAOParroquia extends DBDAO
        {   private static $entityParroquia;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {       self::$entityParroquia = DBDAO::getConnection('PgProvider');
                    $this->Funciones = self::$entityParroquia->MyFunction();
                    $this->Fecha  = new DateControl();
            }
    
            public function ObtenerParroquiaByDescripcion($PrepareDQL)
            {       $JOIN = array_key_exists("id",    $PrepareDQL) ? "  and c.id=".$PrepareDQL['id'] : "";
                    $JOIN = array_key_exists("ciudad",    $PrepareDQL) ? "  and c.idciudad=".$PrepareDQL['ciudad'] : "";
                    $JOIN.= array_key_exists("estado",$PrepareDQL) ? "  and c.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ? "  and upper(c.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                    $ORDN = array_key_exists("orden", $PrepareDQL) ? $PrepareDQL['orden'] : " c.descripcion ";
                    $LIMT = array_key_exists("limite",$PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
            
                    $DSQL = "SELECT c.id, c.descripcion, c.fecreacion, e.id as idestado, e.descripcion as txtestado,c.codigo,"
                          . "p.descripcion as ciudad, c.idciudad, r.descripcion as provincia "
                          . "FROM general.parroquia c, general.estado e, general.ciudad p, general.provincia r "
                          . "WHERE c.idestado=e.id and c.idciudad = p.id and p.idprovincia = r.id ".$JOIN
                          . "ORDER BY ".$ORDN.$LIMT;            
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            public function InsertaParroquia($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $codigo = strtoupper($datos->codigo);
                    $descripcion = strtoupper(trim($datos->descripcion));
                    $Query = "INSERT INTO general.parroquia (descripcion,codigo,idciudad,idestado,iduscreacion,fecreacion)"
                           . "VALUES ('$descripcion','$codigo','$datos->idciudad','$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id";
                    return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
            }
    
            public function ActualizaParroquia($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $codigo = strtoupper($datos->codigo);
                    $descripcion = strtoupper(trim($datos->descripcion));
                    
                    $Query = "UPDATE general.parroquia "
                           . "SET descripcion = '$descripcion', "
                           . "idestado='$datos->estado', "
                           . "codigo='$codigo', "
                           . "idciudad='$datos->idciudad', "
                           . "idusmodifica=".$_SESSION['IDUSER'].", "
                           . "femodificacion='$fecha' "
                           . "WHERE id= ".$datos->id;
                    $this->Funciones->Query($Query);
                    return intval($datos->id);
            }
    
            public function DesactivaParroquia($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE general.parroquia SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion='$fecha' WHERE id=".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }
            
        }
