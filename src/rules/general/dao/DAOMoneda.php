<?php
        require_once("src/base/DBDAO.php");

        class DAOMoneda extends DBDAO
        {     private static $entityMoneda;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityMoneda = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityMoneda->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              public function ObtenerTodos($PrepareDQL)
              {      return $this->BuscarMonedaByDescripcion($PrepareDQL);
              }
                
              public function BuscarMonedaByDescripcion($PrepareDQL)
                {    $JOIN = array_key_exists("estado", $PrepareDQL) ? " and m.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(m.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and m.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT m.id, m.descripcion,m.cambio, e.descripcion as txtestado, m.fecreacion, e.id as idestado,m.codigo,m.cambio "
                          . " FROM general.Moneda m, general.Estado e "
                          . " WHERE m.idestado=e.id ".$JOIN.$LIKE.$IDD
                          . " ORDER BY m.descripcion".$LIM;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
              
              
              public function InsertaMoneda($datos)
              {     try{        
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $cambio = $datos->cambio;
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO general.Moneda (descripcion,codigo,cambio,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', '".$codigo."',".$cambio.", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear la Moneda..';
                    }
              }
    
              public function ActualizaMoneda($datos)
              {      try{ 
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $cambio = $datos->cambio;
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.Moneda "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.""
                               . ", codigo= '".$codigo."', cambio= ".$cambio."  "
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaMoneda($id)
              {     try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.Moneda SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar la Moneda..';
                    }
              }
        }
