<?php
        require_once("src/base/DBDAO.php");
        
        class DAOSucursalEmision extends DBDAO
        {     private static $entitySucursalEmision;
              private $Funciones;
              private $Fecha;
              
            public function __construct()
            {        self::$entitySucursalEmision = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entitySucursalEmision->MyFunction();
                     $this->Fecha  = new DateControl();
            }

            public function ObtenerDatosGeneral($PrepareDQL)
            {        $JOIN = array_key_exists("estado", $PrepareDQL) ? " and s.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(l.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and s.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT s.id, s.idemidocs,s.idsucursal,s.idtipocomp,s.serie1,s.serie2,s.sec_inicio,s.sec_fin"
                           . ",s.autorizacion,s.fevigencia,s.fecaducidad,s.idambiente,s.idejecucion"
                           . ",l.descripcion as sucursal,t.descripcion as tipoemision, c.descripcion as tipocomprobante"
                           . ", e.descripcion as txtestado, s.fecreacion, e.id as idestado"
                          . " FROM general.sucursal_emision s"
                          . " INNER JOIN general.Estado e ON s.idestado=e.id"
                          . " INNER JOIN general.sucursal l ON s.idsucursal=l.id"
                          . " INNER JOIN impuesto.tipo_emision t ON s.idemidocs=t.id"
                          . " INNER JOIN impuesto.tipo_comprobante c ON s.idtipocomp=c.id ".$JOIN.$LIKE.$IDD
                          . " ORDER BY l.descripcion".$LIM;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                                 
            }
            

            public function InsertaSucursalEmision($datos)
            {       try{        
                        $emision = $datos->tipoemision;
                        $sucursal = $datos->idSucursal;
                        $comprobante = $datos->idTipoComprobante;
                        $serie1 = $datos->serie1;
                        $serie2 = $datos->serie2;
                        $inicio = $datos->secinicio;
                        $fin = $datos->secfinal;
                        $autorizacion = $datos->autorizacion;
                        $fevigencia = $datos->fevigencia;
                        $fecaducidad = $datos->fecaducidad;
                        $ambiente = $datos->ambiente;
                        $ejecucion = $datos->ejecucion;
                        $estado = $datos->estado;
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO general.sucursal_emision (idemidocs,idsucursal,idtipocomp,serie1,serie2,sec_inicio,sec_fin"
                               . ",autorizacion,fevigencia,fecaducidad,idambiente,idejecucion,idestado,iduscreacion,fecreacion)"
                               . "VALUES (".$emision.",".$sucursal.",".$comprobante.",'".$serie1."','".$serie2."',".$inicio.",".$fin
                               . ", '".$autorizacion."',to_timestamp('".$fevigencia."','YYYY-MM-DD'),to_timestamp('".$fecaducidad."','YYYY-MM-DD')"
                               . ",".$ambiente.",".$ejecucion.", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear la Relación..';
                    }
            }

            public function ActualizaSucursalEmision($datos)
            {       try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $emision = $datos->tipoemision;
                        $sucursal = $datos->idSucursal;
                        $comprobante = $datos->idTipoComprobante;
                        $serie1 = $datos->serie1;
                        $serie2 = $datos->serie2;
                        $inicio = $datos->secinicio;
                        $fin = $datos->secfinal;
                        $autorizacion = $datos->autorizacion;
                        $fevigencia = $datos->fevigencia;
                        $fecaducidad = $datos->fecaducidad;
                        $ambiente = $datos->ambiente;
                        $ejecucion = $datos->ejecucion;
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.sucursal_emision "
                               . "SET  idemidocs= ".$emision.", idsucursal= ".$sucursal.",idtipocomp= ".$comprobante.", serie1= '".$serie1."'"
                               . ", serie2= '".$serie2."', sec_inicio= ".$inicio.", sec_fin= ".$fin.", autorizacion= '".$autorizacion."'"
                               . ", fevigencia = to_timestamp('".$fevigencia."','YYYY-MM-DD'), fecaducidad= to_timestamp('".$fecaducidad."', 'YYYY-MM-DD')"
                               . ", idambiente= ".$ambiente.", idejecucion= ".$ejecucion.", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."', idestado= ".$estado
                               . "WHERE id= ".$id; 
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Actualizar los Datos..');
                    }
            }            

            public function DesactivaSucursalEmision($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.sucursal_emision SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar los datos..';
                        }
              }
        }
?>        
