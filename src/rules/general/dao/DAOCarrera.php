<?php
        require_once("src/base/DBDAO.php");
        
        class DAOCarrera extends DBDAO
        {   private static $entityCarrera;
            private $Works;
            private $Fecha;
              
            public function __construct()
            {      self::$entityCarrera = DBDAO::getConnection();
                   $this->Works = self::$entityCarrera->MyFunction();
                   $this->Fecha  = new DateControl();
            }

            public function ObtenerByID($id)
            {      $DSQL = "SELECT COD_CARRERA,NOMBRE,COD_MODALIDAD,COD_FACULTAD,ESTADO_CARRERA FROM TB_CARRERA WHERE COD_CCARRERA=1 AND COD_CARRERA='$id'";
                   return $this->Works->RecorreDatos($this->Works->Query($DSQL));                               
            }
            
            public function ObtenerByDescripcion($PrepareDQL)
            {      $LMIT = array_key_exists("limite",$PrepareDQL) ? " TOP ".$PrepareDQL['limite']: " ";      
                   $JOIN = array_key_exists("estado",$PrepareDQL) ? " AND ESTADO_CARRERA IN ('".$PrepareDQL['estado']."') " : " ";
                   $LIKE = array_key_exists("texto", $PrepareDQL) ? " AND (UPPER(NOMBRE) LIKE '%".$PrepareDQL['texto']."%') ": ") ";

                   $DSQL = "SELECT ".$LMIT." COD_CARRERA,NOMBRE,COD_MODALIDAD,COD_FACULTAD,ESTADO_CARRERA FROM TB_CARRERA WHERE COD_CCARRERA=1 ".$JOIN.$LIKE;
                   return $this->Works->RecorreDatos($this->Works->Query($DSQL));                               
            }
            
            public function ObtenerByFacultadID($IdFacultad)
            {      $DSQL = "SELECT COD_CARRERA,NOMBRE,COD_MODALIDAD,COD_FACULTAD,ESTADO_CARRERA FROM TB_CARRERA WHERE COD_CCARRERA=1 AND COD_FACULTAD='$IdFacultad' "; 
                   return $this->Works->RecorreDatos($this->Works->Query($DSQL));                               
            }
            
        }
?>        
