<?php
        require_once("src/base/DBDAO.php");

        class DAOGrupoCliente extends DBDAO
        {     private static $entityGrupoCliente;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityGrupoCliente = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityGrupoCliente->MyFunction();
                     $this->Fecha  = new DateControl();
              }
                    
              function BuscarGrupoClienteByDescrip($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and g.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(g.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and g.id = ".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT g.id, g.descripcion, g.fecreacion, e.id as idestado"
                          . ", e.descripcion as txtestado "
                          . " FROM general.grpcliente g, general.Estado e "
                          . " WHERE g.idestado=e.id ".$JOIN.$LIKE.$IDD
                          . " ORDER BY g.descripcion".$LIM;
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaGrupoCliente($datos)
              {     try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');
                        $Query = "INSERT INTO general.grpcliente (descripcion,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                     return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Grupo de Cliente..';
                    }
              }
    
              public function ActualizaGrupoCliente($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.grpcliente "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaGrupoCliente($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.grpcliente SET idestado= 0 , idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el GrupoCliente..';
                    }
              }
        }
