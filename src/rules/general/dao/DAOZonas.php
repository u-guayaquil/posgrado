<?php
        require_once("src/base/DBDAO.php");

        class DAOZonas extends DBDAO
        {   private static $entityZonas;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {       self::$entityZonas = DBDAO::getConnection('PgProvider');
                    $this->Funciones = self::$entityZonas->MyFunction();
                    $this->Fecha = new DateControl();
            }

            function BuscarZonasByDescripcion($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ? " and z.id=".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and z.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ?  " and upper(z.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                    $ORDN = array_key_exists("orden", $PrepareDQL) ?  $PrepareDQL['orden'] : " z.descripcion ";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     
                    $DSQL = "SELECT "; 
                    $DSQL.= "z.id, z.descripcion,";     
                    $DSQL.= "z.idestado, e.descripcion as txtestado,";
                    $DSQL.= "z.fecreacion ";
                    $DSQL.= "FROM general.zonas z, general.estado e ";
                    $DSQL.= "WHERE z.idestado=e.id ".$JOIN;
                    $DSQL.= "ORDER BY ".$ORDN.$LIMT;
                  
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            public function InsertaZonas($datos)
            {       $Fecha = $this->Fecha->getNowDateTime('DATETIME');
                    $descripcion = strtoupper(trim($datos->descripcion));

                    $Query = "INSERT INTO general.zonas (descripcion,idestado,iduscreacion,fecreacion)"
                           . "VALUES ('$descripcion','$datos->estado',".$_SESSION['IDUSER'].",'$Fecha') returning id";
                        
                    return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
            }
    
            public function ActualizaZonas($datos)
            {       $Fecha = $this->Fecha->getNowDateTime('DATETIME');
                    $descripcion = strtoupper($datos->descripcion);
                    
                    $Query = "UPDATE general.zonas "
                           . "SET descripcion='$descripcion', idestado='$datos->estado', idusmodifica=".$_SESSION['IDUSER'].", femodificacion='$Fecha' "
                           . "WHERE id= ".$datos->id;
                    $this->Funciones->Query($Query);
                    return $datos->id;
            }
    
            public function DesactivaZonas($id)
            {       $Fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE general.zonas SET idestado=0, idusmodifica=".$_SESSION['IDUSER'].", femodificacion='$Fecha' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }
              
            public function ObtenerZonasCombo($IdArray)
            {      return $this->BuscarZonasByDescripcion($IdArray);
            }
        }
