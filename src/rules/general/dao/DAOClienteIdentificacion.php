<?php
        require_once("src/base/DBDAO.php");

        class DAOClienteIdentificacion extends DBDAO
        {     private static $entityClienteIdentificacion;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityClienteIdentificacion = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityClienteIdentificacion->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              public function ObtenerClienteIdentificacion($PrepareDQL,$cliente=0)
              {      $TIP  = array_key_exists("tipo", $PrepareDQL) ? " WHERE c.tipo_ident_cliente = ".$PrepareDQL['tipo']
                                                                    ." and c.num_ident_cliente = '".$PrepareDQL['numero']."'" : "";
                     if($cliente != 0){
                         $CLI = ' and c.idcliente <> '.$cliente;
                     }else{
                         $CLI = '';
                     }
                     $DSQL = "SELECT c.id, c.idcliente,c.tipo_ident_cliente,c.num_ident_cliente"                           
                          . " FROM general.cliente_identificacion c".$TIP.$CLI
                          . " ORDER BY c.id"; //echo $DSQL;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                               
              }  
              
              public function ObtenerClienteIdentificacionByCliente($PrepareDQL)
              {      $DSQL = "SELECT c.id, c.idcliente,c.tipo_ident_cliente,c.num_ident_cliente"                           
                          . " FROM general.cliente_identificacion c WHERE c.idcliente = ".$PrepareDQL['cliente']
                          . " ORDER BY c.id";
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                               
              }          
              
              public function InsertaClienteIdentificacion($idcliente,$tipoidentificacion,$identificacion)
                {       try{                            
                            $Query = "INSERT INTO general.cliente_identificacion (idcliente,tipo_ident_cliente,num_ident_cliente)"
                                   . "VALUES (".$idcliente.", ".$tipoidentificacion.", '".$identificacion."') returning id";
                            $Rsid = $this->Funciones->Query($Query);
                            $id = $this->Funciones->FieldDataByName($Rsid,"id");
                            return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo crear la relacion..';
                        }
                }
                
              public function EliminaClienteIdentificacion($id)
              {      $Query = "DELETE FROM general.cliente_identificacion WHERE idcliente= ".$id;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
              }
        }
