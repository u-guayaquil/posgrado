<?php
        require_once("src/base/DBDAO.php");

        class DAOUnidad extends DBDAO
        {     private static $entityUnidad;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityUnidad = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityUnidad->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarUnidadByDescripcion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and u.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(u.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $ABRE = array_key_exists("abreviatura", $PrepareDQL) ? " and upper(u.abreviatura) = '".$PrepareDQL['abreviatura']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and u.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT u.id, u.descripcion, u.abreviatura, u.fecreacion, e.id as idestado"
                           . ", e.descripcion as txtestado "
                           . "FROM general.unidad u, general.Estado e "
                           . "WHERE u.idestado=e.id ".$JOIN.$LIKE.$IDD.$ABRE
                           . " ORDER BY u.descripcion ".$LIM;
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarUnidadByProducto($Producto)
              {      $DSQL = "SELECT u.id, u.descripcion, u.abreviatura "
                           . "FROM general.unidad u, inventario.producto p "
                           . "WHERE p.idunidad = u.id and p.id=".$Producto
                           . " ORDER BY u.id ";
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaUnidad($datos)
              {     try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $abreviatura = strtoupper($datos->abreviatura);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');
                        $Query = "INSERT INTO general.unidad (descripcion,abreviatura,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."','".$abreviatura."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear la Unidad..';
                    }
              }
    
              public function ActualizaUnidad($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $abreviatura = strtoupper($datos->abreviatura);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');
                        
                        $Query = "UPDATE general.unidad "
                               . "SET descripcion= '".$descripcion."',abreviatura= '".$abreviatura."', idestado= ".$estado.""
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaUnidad($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.unidad SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar la Unidad..';
                        }
              }
              
              public function ObtenerUnidadCombo($IdArray)
              {      return  $this->BuscarUnidadByDescripcion($IdArray);
              }
        }
