<?php
        require_once("src/base/DBDAO.php");
        
        class DAOEmpleado extends DBDAO
        {   private static $entityEmpleado;
            private $Funciones;
              
            public function __construct()
            {      self::$entityEmpleado = DBDAO::getConnection('PgProvider');
                   $this->Funciones = self::$entityEmpleado->MyFunction();
            }

            function ObtenerByID($id)
            {       $DQL = "select  x.id,
                                    x.nombre,x.apellido,x.beneficiario, 
                                    x.idsucursal,s.descripcion as txsucursal,
                                    x.tipoid,x.numero,
                                    x.fenacimiento,
                                    x.tipoempleado,
                                    x.sexo,
                                    x.idestcivil,
                                    x.idprofesion, p.descripcion as txprofesion,
                                    x.direccion,
                                    x.idciudad, c.descripcion as txciudad,
                                    x.fono_casa, x.fono_movil,
                                    x.email,
                                    x.iddepartamento, d.descripcion as txdepartamento,
                                    x.idcargo, g.descripcion as txcargo,
                                    x.idjefe, j.apellido||' '||j.nombre as txjefe,
                                    x.idbanco, 
                                    x.tipoctabco, b.descripcion as tipocta,
                                    x.ctabanco,
                                    x.horadesde, x.horahasta,
                                    x.feingreso, x.fesalida,
                                    x.pormulta,
                                    x.nucontrato,
                                    x.idplantilla, r.descripcion as txplantilla,
                                    x.firma,
                                    x.idctasueldo, sl.descripcion as txctasueldo,
                                    x.idctaanticipo, an.descripcion as txctaanticipo,
                                    x.idctaprestamo, pr.descripcion as txctaprestamo,
                                    x.idestado
                            from
                                    nomina.cargo as g inner join
                                    (nomina.departamento as d inner join
                                    (general.ciudad as c inner join 
                                    (nomina.profesion as p inner join
                                    (general.sucursal as s inner join 
                                    (general.empleado as x 
                                    left join general.empleado as j on x.idjefe=j.id
                                    left join nomina.plantilla_rol as r on x.idplantilla=r.id			
                                    left join caja.medios_subtipo as b  on x.tipoctabco=b.id
                                    left join general.planctas as sl ON sl.id=x.idctasueldo
                                    left join general.planctas as an ON an.id=x.idctaanticipo
                                    left join general.planctas as pr ON pr.id=x.idctaprestamo
                                    ) 
                                    on s.id=x.idsucursal) on
                                    p.id=x.idprofesion) on c.id=x.idciudad) on d.id=x.iddepartamento) on
                                    x.idcargo = g.id
                            where   x.id=$id";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DQL));  
            }
            
            public function ObtenerByNombre($PrepareDQL)
            {       $STAT = array_key_exists("estado", $PrepareDQL) ? " and e.idestado in (".$PrepareDQL['estado'].")" : " ";
                    $LIKE = array_key_exists("texto", $PrepareDQL) ?  " and upper(e.apellido||' '||e.nombre) like '%".$PrepareDQL['texto']."%' ": " ";      
                    $LMIT = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: " ";      
                    $DSQL = "select e.id, upper(e.apellido||' '||e.nombre) as empleado, e.numero, e.fecreacion ,st.descripcion  ";
                    $DSQL.= "from general.empleado as e, general.estado as st ";
                    $DSQL.= "where st.id=e.idestado".$STAT.$LIKE;
                    $DSQL.= "order by upper(e.apellido||' '||e.nombre)".$LMIT;
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));   
            }
            
            
            public function Inserta($Forma)
            {       $FileManager = new File();             
                    $FileFinal = "";
                    $FileParts = $FileManager->getFileParts(trim($Forma->txfirma));
                    if ($FileParts[0])
                    {   $Empleados = trim($Forma->cedula);
                        $Nacimento = str_replace("/", "", $Forma->nacio); 
                        $FileFinal = "src/upload/empleados/firmas/".$Empleados."_".$Nacimento.".".$FileParts[3];
                        $FileManager->MoveFile(trim($Forma->txfirma), $FileFinal);
                    }
                
                    $Fechas = new DateControl();
                    $nombre = strtoupper(trim($Forma->nombre));
                    $apelld = strtoupper(trim($Forma->apellido));
                    $benefs = strtoupper(trim($Forma->beneficia));
                    $isucur = intval($Forma->sucursal);
                    $direcc = strtoupper(trim($Forma->direccion));
                    $e_mail = strtoupper(trim($Forma->email)); 
                    $idjefe = intval($Forma->idjefe);
                    $pormul = floatval($Forma->multa);
                    $uscrea = $_SESSION['IDUSER']; 
                    $fnacio = $Fechas->changeFormatDate($Forma->nacio, "YMD")[1]; 
                    $feingr = $Fechas->changeFormatDate($Forma->ingreso, "YMD")[1]; 
                    $fesald = $Fechas->changeFormatDate($Forma->salida, "YMD")[1]; ;
                    $fechac = $Fechas->getNowDateTime('DATETIME');
                    $plnrol = intval($Forma->idplantilla);
                    
                    $Query = " insert into general.empleado(nombre,apellido,beneficiario,idsucursal,tipoid,numero,fenacimiento,tipoempleado,sexo,idestcivil,idprofesion,direccion,idciudad,fono_casa,fono_movil,email,iddepartamento,idcargo,idjefe,tipoctabco,idbanco,ctabanco,pormulta,horadesde,horahasta,feingreso,fesalida,nucontrato,idplantilla,firma,idctasueldo,idctaanticipo,idctaprestamo,idestado,iduscreacion,fecreacion) ";
                    $Query.= " values('$nombre','$apelld','$benefs','$isucur',2,'$Forma->cedula','$fnacio','$Forma->tipoemp','$Forma->sexoemp','$Forma->estadocivil','$Forma->profesion','$direcc','$Forma->idciudad','$Forma->fijo','$Forma->movil','$e_mail','$Forma->iddepartamento','$Forma->idcargo','$idjefe','$Forma->ctatipo','$Forma->banco','$Forma->ctanum','$pormul','$Forma->entrada','$Forma->hsalida','$feingr','$fesald','$Forma->contrato','$plnrol','$FileFinal','$Forma->idctasueldo','$Forma->idctaanticipo','$Forma->idctaprestamo',1,'$uscrea','$fechac') returning id ";
                    $resourceID = $this->Funciones->Query($Query);
                    return $this->Funciones->FieldDataByName($resourceID,'id'); 
            }
            
            public function Actualiza($Forma)
            {       
                    $FileManag = new File();             
                    $FileFinal = trim($Forma->txfirma);
                    $FileParts = $FileManag->getFileParts($FileFinal);
                    if ($FileParts[0])
                    {   if (file_exists($FileFinal))
                        {   $Empleados = trim($Forma->cedula);
                            $Nacimento = str_replace("/", "", $Forma->nacio); 
                            $FileFinal = "src/upload/empleados/firmas/".$Empleados."_".$Nacimento.".".$FileParts[3];
                            $FileManag->MoveFile(trim($Forma->txfirma), $FileFinal);
                        }    
                    }
                
                    $Fechas = new DateControl();
                    $id = intval($Forma->idempleado);
                    $nombre = strtoupper(trim($Forma->nombre));
                    $apelld = strtoupper(trim($Forma->apellido));
                    $benefs = strtoupper(trim($Forma->beneficia));
                    $isucur = intval($Forma->sucursal);
                    $direcc = strtoupper(trim($Forma->direccion));
                    $e_mail = strtoupper(trim($Forma->email)); 
                    $pormul = floatval($Forma->multa);
                    $usmody = $_SESSION['IDUSER']; 
                    $fnacio = $Fechas->changeFormatDate($Forma->nacio, "YMD")[1]; 
                    $feingr = $Fechas->changeFormatDate($Forma->ingreso, "YMD")[1]; 
                    $fesald = $Fechas->changeFormatDate($Forma->salida, "YMD")[1]; ;
                    $fecham = $Fechas->getNowDateTime('DATETIME');            
                    
                    $Querys = "update general.empleado set 
                                    nombre = '$nombre',
                                    apellido = '$apelld',
                                    beneficiario = '$benefs',
                                    idsucursal = '$isucur',
                                    tipoid = 2,
                                    numero = '$Forma->cedula',
                                    fenacimiento = '$fnacio',
                                    tipoempleado = '$Forma->tipoemp',
                                    sexo = '$Forma->sexoemp',
                                    idestcivil = '$Forma->estadocivil',
                                    idprofesion = '$Forma->profesion',
                                    direccion = '$direcc',
                                    idciudad = '$Forma->idciudad',
                                    fono_casa = '$Forma->fijo',
                                    fono_movil = '$Forma->movil',
                                    email = '$e_mail',
                                    iddepartamento = '$Forma->iddepartamento',
                                    idcargo = '$Forma->idcargo',
                                    idjefe = '$Forma->idjefe',
                                    tipoctabco = '$Forma->ctatipo',
                                    idbanco = '$Forma->banco',
                                    ctabanco = '$Forma->ctanum',
                                    pormulta = '$pormul',
                                    horadesde = '$Forma->entrada',
                                    horahasta = '$Forma->hsalida',
                                    feingreso = '$feingr',
                                    fesalida = '$fesald',
                                    nucontrato = '$Forma->contrato',
                                    idplantilla = '$Forma->idplantilla',
                                    firma = '$FileFinal',
                                    idctasueldo = '$Forma->idctasueldo',
                                    idctaanticipo = '$Forma->idctaanticipo',
                                    idctaprestamo = '$Forma->idctaprestamo',
                                    idestado = '$Forma->estado',
                                    idusmodifica = '$usmody',
                                    femodificacion = '$fecham'
                                where id= ".$id;                
                    $this->Funciones->Query($Querys);
                    return $id;
            }
    
            public function Elimina($id)
            {       $Fechas = new DateControl();
                    $fecham = $Fechas->getNowDateTime('DATETIME');                        
                    $usmody = $_SESSION['IDUSER']; 
                    $querys = "update general.empleado set idestado= 0, idusmodifica='$usmody', femodificacion= '$fecham' where id=".$id;
                    $this->Funciones->Query($querys);
                    return true;
            }
           
        }
?>