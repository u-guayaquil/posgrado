<?php
        require_once("src/base/DBDAO.php");
        
        class DAODocenteQ extends DBDAO
        {   private static $entityDocente;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {      self::$entityDocente = DBDAO::getConnection('PgProvider');
                   $this->Funciones = self::$entityDocente->MyFunction();
                   $this->Fecha  = new DateControl();
            }

            public function InsertaDocente($datos)
            {       try{        
                        $acreedor =  $this->SeteDatosDocente($datos);                      
                        $Query = "INSERT INTO general.acreedor (codigo,nombre,tipo_ident,num_ident,idclase,idciudad,direccion,telefono"
                               . ",fax,email,idctaxpagar,idctanticip,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$acreedor['codigo']."','".$acreedor['nombre']."',".$acreedor['identificacion'].",'".$acreedor['numidentificacion']."'"
                               . ",".$acreedor['clase'].",".$acreedor['ciudad'].",'".$acreedor['direccion']."','".$acreedor['telefono']."','".$acreedor['fax']."'"
                               . ",'".$acreedor['email']."',".$acreedor['porpagar'].",".$acreedor['anticipo'].", ".$acreedor['estado'].",".$_SESSION['IDUSER'].",'".$acreedor['fecha']."')"
                               . " returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Docente..';
                    }
            }

            public function ActualizaDocente($datos)
            {       try{ 
                        $id = $datos->id;     
                        $acreedor =  $this->SeteDatosDocente($datos);   
                        $Query = "UPDATE general.acreedor "
                               . "SET codigo= '".$acreedor['codigo']."',nombre= '".$acreedor['nombre']."',tipo_ident= ".$acreedor['identificacion']
                               .",num_ident= '".$acreedor['numidentificacion']."'"
                               . ", idclase= ".$acreedor['clase'].", idciudad= ".$acreedor['ciudad'].", direccion= '".$acreedor['direccion']."' "
                               . ", telefono= '".$acreedor['telefono']."', fax= '".$acreedor['fax']."', email= '".$acreedor['email']."' "
                               . ", idctaxpagar= ".$acreedor['porpagar'].", idctanticip= ".$acreedor['anticipo']
                               . ", idestado= ".$acreedor['estado'].", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$acreedor['fecha']."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
            }
            public function SeteDatosDocente($datos)
            {       $codigo = $datos->codigo;
                    $nombre = $datos->nombre;
                    $identificacion = $datos->identificacion;
                    $numidentificacion = $datos->numidentificacion;
                    $clase = $datos->clase;
                    $ciudad = $datos->idCiudad;
                    $direccion = $datos->direccion;
                    $telefono = $datos->telefono;
                    $fax = $datos->fax;
                    $email = $datos->email;
                    $porpagar = $datos->idCuentaPorPagar;
                    $anticipo = $datos->idCuentaAnticipo;
                    $estado = $datos->estado;
                    $fecha = $this->Fecha->getNowDateTime('DATETIME');
                    
                    return array('codigo' => $codigo,'nombre' => $nombre,'identificacion' => $identificacion,'numidentificacion' => $numidentificacion
                                ,'clase' => $clase,'ciudad' => $ciudad,'direccion' => $direccion
                                ,'telefono' => $telefono,'fax' => $fax,'email' => $email,'porpagar' => $porpagar
                                ,'anticipo' => $anticipo,'estado' => $estado,'fecha' => $fecha);
            
            }
            

            public function DesactivaDocente($id)
            {       try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.acreedor SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                     } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Docente..';
                     }
            }            
              
            public function ObtenerDatosGeneral($PrepareDQL)
            {        $JOIN = array_key_exists("estado", $PrepareDQL) ? " and a.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(a.nombre) LIKE '%".$PrepareDQL['texto']."%'": "";
                     if($PrepareDQL['iddis'] != ''){
                         $NUM  = array_key_exists("numero", $PrepareDQL) ? " WHERE a.num_ident = '".$PrepareDQL['numero']."' and a.id<>".$PrepareDQL['iddis'] : "";
                     }else{
                         $NUM  = array_key_exists("numero", $PrepareDQL) ? " WHERE a.num_ident = '".$PrepareDQL['numero']."'" : "";
                     }                     
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE a.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT a.id, a.codigo,UPPER(a.nombre) as nombre,a.tipo_ident,a.num_ident,a.idclase,a.idciudad,a.direccion"
                           . ",a.telefono,a.fax,a.email,a.idctaxpagar,a.idctanticip, e.descripcion as txtestado, a.fecreacion, e.id as idestado"
                           . ",c.descripcion as ciudad,p.descripcion as cuentaporpagar, n.descripcion as cuentaanticipo"
                          . " FROM general.acreedor a"
                          . " INNER JOIN impuesto.identificacion i ON a.tipo_ident=i.id"
                          . " INNER JOIN general.claseacreedor s ON a.idclase=s.id"
                          . " INNER JOIN general.ciudad c ON a.idciudad=c.id"
                          . " INNER JOIN general.planctas p ON a.idctaxpagar=p.id"
                          . " INNER JOIN general.planctas n ON a.idctanticip=n.id"
                          . " INNER JOIN general.Estado e ON a.idestado=e.id".$JOIN.$LIKE.$IDD.$NUM
                          . " ORDER BY a.nombre".$LIM; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                               
            }
            
            public function ObtenerDatosByCodigo($codigo)
            {        $DSQL = "SELECT id"
                          . " FROM general.acreedor WHERE codigo='".$codigo."'"
                          . " ORDER BY nombre"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                               
            }
        }
?>        
