<?php
        require_once("src/base/DBDAO.php");

        class DAOProvincia extends DBDAO
        {       private static $entityProvincia;
                private $Funciones;
                private $Fecha;
              
                public function __construct()
                {      self::$entityProvincia = DBDAO::getConnection('PgProvider');
                       $this->Funciones = self::$entityProvincia->MyFunction();
                       $this->Fecha  = new DateControl();
                }
    
                public function ObtenerProvinciaByID($id)
                {      $DSQL = "SELECT p.id, p.descripcion,p.codigo, p.fecreacion, p.idestado,idpais,s.descripcion as pais, e.descripcion as txtestado "
                             . " FROM general.provincia p, general.pais s,general.estado e"
                             . " WHERE p.idpais = s.id and p.idestado = e.id and p.id =".$id['id']
                             . " ORDER BY p.descripcion";
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                public function ObtenerProvinciaByDescripcion($PrepareDQL)
                {       $JOIN = array_key_exists("id",    $PrepareDQL) ? "  and p.id = ".$PrepareDQL['id'] : "";
                        $JOIN.= array_key_exists("pais",  $PrepareDQL) ? "  and p.idpais = ".$PrepareDQL['pais'] : "";
                        $JOIN.= array_key_exists("estado",$PrepareDQL) ? "  and p.idestado in (".$PrepareDQL['estado'].")" : "";
                        $JOIN.= array_key_exists("texto", $PrepareDQL) ? "  and upper(p.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                        $ORDN = array_key_exists("orden", $PrepareDQL) ? $PrepareDQL['orden'] : " p.descripcion ";
                        $LIMT = array_key_exists("limite",$PrepareDQL) ? "  limit ".$PrepareDQL['limite']: "";
                        
                        $DSQL = "SELECT p.id,p.descripcion,p.codigo,p.idpais,e.descripcion as txtestado, p.fecreacion,e.id as idestado,s.descripcion as pais "
                              . " FROM general.provincia p, general.estado e,general.pais s "
                              . " WHERE p.idestado=e.id and p.idpais = s.id ".$JOIN
                              . " ORDER BY ".$ORDN.$LIMT;
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                public function InsertaProvincia($datos)
                {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                        $codigo = strtoupper($datos->codigo);
                        $descripcion = strtoupper($datos->descripcion);
                        $Query = "INSERT INTO general.provincia (descripcion,codigo,idpais,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('$descripcion','$codigo','$datos->idpais','$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id";
                        $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                        return intval($id);
                }
    
                public function ActualizaProvincia($datos)
                {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                        $codigo = strtoupper($datos->codigo);
                        $descripcion = strtoupper($datos->descripcion);
                        $Query = "UPDATE general.provincia "
                                . "SET "
                                . "codigo='$codigo', "
                                . "descripcion='$descripcion', "
                                . "idpais='$datos->idpais', "
                                . "idestado='$datos->estado', "
                                . "idusmodifica=".$_SESSION['IDUSER'].", "
                                . "femodificacion='$fecha' "
                                . "WHERE id= ".$datos->id;
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                }
    
                public function DesactivaProvincia($id)
                {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE general.provincia SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion='$fecha' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                }
              
        }
