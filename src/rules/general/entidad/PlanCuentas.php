<?php

    require_once("src/base/EntidadGlobal.php");

    /**
     * @Entity
     * @Table(name="planctas",schema ="general")
     */
    class PlanCuentas extends EntidadGlobal 
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="descripcion", type="string", length=30, nullable=false)
         */
        private $descripcion;

        /**
         * @Column(name="codigo", type="string", length=50, nullable=true)
         */
        private $codigo;

        /**
         * @Column(name="tipo", type="string",length=1, nullable=false)
         */
        private $tipo;

        /**
         * @Column(name="clase", type="integer", nullable=false)
         */
        private $clase;

        /**
         * @Column(name="idpadre", type="integer", nullable=false)
         */
        private $idPadre;

        public function getId()
        {      return $this->id;
        }

        public function setDescripcion($descripcion)
        {      $this->descripcion = $descripcion;
        }

        public function getDescripcion()
        {      return $this->descripcion;
        }

        public function setCodigo($codigo)
        {      $this->codigo = $codigo;
        }

        public function getCodigo()
        {      return $this->codigo;
        }

        public function setTipo($tipo)
        {      $this->tipo = $tipo;
        }

        public function getTipo()
        {      return $this->tipo;
        }

        public function setClase($clase)
        {      $this->clase = $clase;
        }

        public function getClase()
        {      return $this->clase;
        }

        public function setIdPadre($idPadre)
        {      $this->idPadre = $idPadre;
        }

        public function getIdPadre()
        {      return $this->idPadre;
        }
        
        

    }
