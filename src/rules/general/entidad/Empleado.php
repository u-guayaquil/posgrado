<?php


    /**
     * @Table(name="empleado", schema="general")
     * @Entity
     */
    class Empleado
    {
        /**
         * @Column(name="id", type="integer", nullable=false)
         * @Id
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="nombre", type="string", length=30, nullable=false)
         */
        private $nombre;

        /**
         *
         * @Column(name="apellido", type="string", length=30, nullable=false)
         */
        private $apellido;

        public function getId()
        {      return $this->id;
        }

        public function setNombre($nombre)
        {      $this->nombre = $nombre;

        }

        public function getNombre()
        {      return $this->nombre;
        }

        public function setApellido($apellido)
        {      $this->apellido = $apellido;
        }

        public function getApellido()
        {      return $this->apellido;
        }
}

