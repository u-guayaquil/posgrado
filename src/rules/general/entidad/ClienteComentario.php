<?php

require_once("src/base/EntidadGlobal.php");
require_once("src/rules/general/entidad/Cliente.php");

    /**
     *
     * @Entity
     * @Table(name="cliente_comentario", schema="general")
     */
class ClienteComentario extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
    * @Column(name="fecha", type="datetime")
    */
    private $fecha;

    /**
      * @Column(name="comentario", type="string", length=20)
    */
    private $comentario = '';
    /**
     * @ManyToOne(targetEntity="Cliente")
     * @JoinColumn(name="idcliente", referencedColumnName="id", nullable=false)
     */
    private $Cliente;    

    
    public function getId()
    {
        return $this->id;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getComentario() {
        return $this->comentario;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    function setComentario($comentario) {
        $this->comentario = $comentario;
    }

    function getCliente() {
        return $this->Cliente;
    }

    function setCliente(Cliente $Cliente) {
        $this->Cliente = $Cliente;
    }



}

