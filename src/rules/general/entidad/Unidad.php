<?php
require_once("src/base/EntidadGlobal.php");

    /**
     *
     * @Entity
     * @Table(name="unidad", schema="general")
     */
class Unidad extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;
    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion;
/**
      * @Column(name="abreviatura", type="string", length=5)
    */
    private $abreviatura;

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setAbreviatura($abreviatura)
    {
        $this->abreviatura = $abreviatura;
    }

    public function getAbreviatura()
    {
        return $this->abreviatura;
    }
}

