<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/impuesto/entidad/TipoEmision.php");
    require_once("src/rules/impuesto/entidad/TipoComprobante.php");
    require_once("src/rules/general/entidad/Sucursal.php");

    /**
     * @Entity
     * @Table(name="sucursal_emision",schema ="general")
     */
    class SucursalEmision extends EntidadGlobal 
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="idemidocs", type="integer", nullable=false)
         */
        private $idEmiDocs;

        /**
         * @Column(name="idsucursal", type="integer", nullable=false)
         */
        private $idSucursal;
        
        /**
         * @Column(name="idtipocomp", type="integer", nullable=false)
         */
        private $idTipoComp;
        
        /**
         * @Column(name="serie1", type="string", length=3)
         */
        private $Serie1;
        
        /**
         * @Column(name="serie2", type="string", length=3)
         */
        private $Serie2;
        
        /**
         * @Column(name="sec_inicio", type="integer", nullable=false)
         */
        private $SecInicio;
        
        /**
         * @Column(name="sec_fin", type="integer", nullable=false)
         */
        private $SecFin;
        
        /**
         * @Column(name="autorizacion", type="string", length=20)
         */
        private $Autorizacion;
        
        /**
        * @Column(name="fevigencia", type="datetime")
        */
        private $FeVigencia;

        /**
        * @Column(name="fecaducidad", type="datetime")
        */
        private $FeCaducidad;
        
        /**
         * @Column(name="idambiente", type="integer", nullable=false)
         */
        private $idAmbiente;
        
        /**
         * @Column(name="idejecucion", type="integer", nullable=false)
         */
        private $idEjecucion;        
        
        /**
        * @ManyToOne(targetEntity="TipoEmision")
        * @JoinColumn(name="idemidocs", referencedColumnName="id", nullable=false)
        */        
        private $TipoEmision;
        
        /**
        * @ManyToOne(targetEntity="Sucursal")
        * @JoinColumn(name="idsucursal", referencedColumnName="id", nullable=false)
        */        
        private $Sucursal;
        
        /**
        * @ManyToOne(targetEntity="TipoComprobante")
        * @JoinColumn(name="idtipocomp", referencedColumnName="id", nullable=false)
        */        
        private $TipoComprobante;


        public function getId()
        {      return $this->id;
        }

        function getIdEmiDocs() {
            return $this->idEmiDocs;
        }

        function getIdSucursal() {
            return $this->idSucursal;
        }

        function getIdTipoComp() {
            return $this->idTipoComp;
        }

        function getSerie1() {
            return $this->Serie1;
        }

        function getSerie2() {
            return $this->Serie2;
        }

        function getSecInicio() {
            return $this->SecInicio;
        }

        function getSecFin() {
            return $this->SecFin;
        }

        function getAutorizacion() {
            return $this->Autorizacion;
        }

        function getFeVigencia() {
            return $this->FeVigencia;
        }

        function getFeCaducidad() {
            return $this->FeCaducidad;
        }

        function getIdAmbiente() {
            return $this->idAmbiente;
        }

        function getIdEjecucion() {
            return $this->idEjecucion;
        }

        function getTipoEmision() {
            return $this->TipoEmision;
        }

        function getSucursal() {
            return $this->Sucursal;
        }

        function getTipoComprobante() {
            return $this->TipoComprobante;
        }

        function setIdEmiDocs($idEmiDocs) {
            $this->idEmiDocs = $idEmiDocs;
        }

        function setIdSucursal($idSucursal) {
            $this->idSucursal = $idSucursal;
        }

        function setIdTipoComp($idTipoComp) {
            $this->idTipoComp = $idTipoComp;
        }

        function setSerie1($Serie1) {
            $this->Serie1 = $Serie1;
        }

        function setSerie2($Serie2) {
            $this->Serie2 = $Serie2;
        }

        function setSecInicio($SecInicio) {
            $this->SecInicio = $SecInicio;
        }

        function setSecFin($SecFin) {
            $this->SecFin = $SecFin;
        }

        function setAutorizacion($Autorizacion) {
            $this->Autorizacion = $Autorizacion;
        }

        function setFeVigencia($FeVigencia) {
            $this->FeVigencia = $FeVigencia;
        }

        function setFeCaducidad($FeCaducidad) {
            $this->FeCaducidad = $FeCaducidad;
        }

        function setIdAmbiente($idAmbiente) {
            $this->idAmbiente = $idAmbiente;
        }

        function setIdEjecucion($idEjecucion) {
            $this->idEjecucion = $idEjecucion;
        }

        function setTipoEmision(TipoEmision $TipoEmision) {
            $this->TipoEmision = $TipoEmision;
        }

        function setSucursal(Sucursal $Sucursal) {
            $this->Sucursal = $Sucursal;
        }

        function setTipoComprobante(TipoComprobante $TipoComprobante) {
            $this->TipoComprobante = $TipoComprobante;
        }

    }
