<?php


    /**
     * @Table(name="tiempo", schema="general")
     * @Entity
     */
    
    class Tiempo
    {
            /**
             *
             * @Column(name="id", type="integer", nullable=false)
             * @Id
             * @GeneratedValue
             */
            private $id;

            /**
             * @Column(name="descripcion", type="string", length=10, nullable=false)
             */
            private $descripcion;

            public function getId()
            {      return $this->id;
            }

            public function setDescripcion($descripcion)
            {      $this->descripcion = $descripcion;
            }

            public function getDescripcion()
            {      return $this->descripcion;
            }
}

