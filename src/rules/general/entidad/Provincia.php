<?php
    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/general/entidad/Pais.php");
    
    /**
     * @Entity
     * @Table(name="provincia", schema="general")
     */
    class Provincia extends EntidadGlobal
    {
        /**
         * @Id
         * @Column(name="id", type="integer")
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="idpais", type="integer")
         */
        private $idpais;

        /**
         * @Column(name="descripcion", type="string", length=20, nullable=false)
         */
        private $descripcion;    
        
        /**
         * @ManyToOne(targetEntity="Pais",inversedBy="provincia")
         * @JoinColumn(name="idpais", referencedColumnName="id", nullable=false)           
         */
        private $pais;
    
        /**
         * @Column(name="codigo", type="string", length=10, nullable=false)
         */
        private $codigo;

        public function getId()
        {      return $this->id;
        }

        public function setDescripcion($descripcion)
        {      $this->descripcion = $descripcion;
        }

        public function getDescripcion()
        {      return $this->descripcion;
        }

        public function setPais(Pais $pais)
        {      $this->pais = $pais;
        }
    
        public function getPais()
        {      return $this->pais;
        }
    
        public function setIdpais($idpais)
        {      $this->idpais = $idpais;
        }
    
        public function getIdpais()
        {      return $this->idpais;
        }
    
        public function setCodigo($codigo)
        {      $this->codigo = $codigo;
        }
    
        public function getCodigo()
        {      return $this->codigo;
        }
    }

