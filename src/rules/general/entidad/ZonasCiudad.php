<?php

    require_once("src/rules/general/entidad/Ciudad.php");
    require_once("src/rules/general/entidad/Zonas.php");

    /**
     * @Table(name="zonas_ciudad", schema="general")
     * @Entity
     */
    class ZonasCiudad
    {
        /**
         * @Column(name="id", type="integer", nullable=false)
         * @Id
         * @GeneratedValue
         */
        private $id;

        /**
         * @ManyToOne(targetEntity="Ciudad")
         * @JoinColumn(name="idciudad", referencedColumnName="id")
         */
        private $Ciudades;

        /**
         * @ManyToOne(targetEntity="Zonas")
         * @JoinColumn(name="idzona", referencedColumnName="id")
         */
        private $Zonas;
    
        public function getId()
        {      return $this->id;
        }

        public function setCiudades(Ciudad $ciudad)
        {      $this->Ciudades = $ciudad;
        }

        public function getCiudades()
        {      return $this->Ciudades;
        }

        public function setZonas(Zonas $zona)
        {      $this->Zonas = $zona;
        }

        public function getZonas()
        {      return $this->Zonas;
        }
}

