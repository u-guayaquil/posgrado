<?php
require_once("src/base/EntidadGlobal.php");
require_once("src/rules/general/entidad/Unidad.php");

    /**
     *
     * @Entity
     * @Table(name="unidad_conversion", schema="general")
     */
class UnidadConversion //extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Unidad")
     * @JoinColumn(name="und_origen", referencedColumnName="id", nullable=false)
     */
    private $UnidadOrigen;
    
    /**
     * @ManyToOne(targetEntity="Unidad")
     * @JoinColumn(name="und_destino", referencedColumnName="id", nullable=false)
     */
    private $UnidadDestino;
    
    /**
     * @Column(name="und_factor", type="float", precision=10, scale=0, nullable=false)
     */
    private $Factor;

    public function getId()
    {
        return $this->id;
    }
    
    function getUnidadOrigen() {
        return $this->UnidadOrigen;
    }

    function getUnidadDestino() {
        return $this->UnidadDestino;
    }

    function setUnidadOrigen(Unidad $UnidadOrigen) {
        $this->UnidadOrigen = $UnidadOrigen;
    }

    function setUnidadDestino(Unidad $UnidadDestino) {
        $this->UnidadDestino = $UnidadDestino;
    }

    function getFactor() {
        return $this->Factor;
    }

    function setFactor($Factor) {
        $this->Factor = $Factor;
    }


}

