<?php 
require_once("src/rules/general/entidad/Cliente.php");
require_once("src/rules/impuesto/entidad/Identificacion.php");

     /**
     * @Entity
     * @Table(name="cliente_identificacion", schema="general")
     */
class ClienteIdentificacion
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
     * @ManyToOne(targetEntity="Cliente")
     * @JoinColumn(name="idcliente", referencedColumnName="id", nullable=false)
     */
    private $Cliente;
    
    /**
     * @ManyToOne(targetEntity="Identificacion")
     * @JoinColumn(name="tipo_ident_cliente", referencedColumnName="id", nullable=false)
     */
    private $Identificacion;
    
    /**
     * @Column(name="num_ident_cliente", type="string", length=15)
     */
    private $NumeroIdent;
    

    public function getId()
    {
        return $this->id;
    }
    
    function getCliente() {
        return $this->Cliente;
    }

    function getIdentificacion() {
        return $this->Identificacion;
    }

    function getNumeroIdent() {
        return $this->NumeroIdent;
    }

    function setCliente(Cliente $Cliente) {
        $this->Cliente = $Cliente;
    }

    function setIdentificacion(Identificacion $Identificacion) {
        $this->Identificacion = $Identificacion;
    }

    function setNumeroIdent($NumeroIdent) {
        $this->NumeroIdent = $NumeroIdent;
    }


    
}

