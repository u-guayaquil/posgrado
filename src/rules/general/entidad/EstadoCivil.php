<?php 
     /**
     * @Entity
     * @Table(name="estadocivil", schema="general")
     */
class EstadoCivil
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion;
    

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
}

