<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/general/entidad/Tiempo.php");

    /**
     * @Table(name="terminos", schema="general" )
     * @Entity
     */
    class Terminos extends EntidadGlobal
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="descripcion", type="string", length=20, nullable=false)
         */
        private $descripcion;

        /**
         * @Column(name="valor", type="integer")
         */
        private $valor;

        /**
         * @Column(name="interes", type="float", precision=10, scale=0)
         */
        private $interes;

        /**
         * @ManyToOne(targetEntity="Tiempo")
         * @JoinColumn(name="idtiempo", referencedColumnName="id")
         */
        private $Tiempo;


        public function getId()
        {      return $this->id;
        }

        public function setDescripcion($descripcion)
        {      $this->descripcion = $descripcion;
        }

        public function getDescripcion()
        {      return $this->descripcion;
        }

        public function setValor($valor)
        {      $this->valor = $valor;
        }

        public function getValor()
        {      return $this->valor;
        }
        
        public function setInteres($interes)
        {      $this->interes = $interes;
        }

        public function getInteres()
        {      return $this->interes;
        }

        public function setTiempo(Tiempo $tiempo)
        {      $this->Tiempo = $tiempo;
        }

        public function getTiempo()
        {      return $this->Tiempo;
        }
}

