<?php

require_once("src/base/EntidadGlobal.php");

    /**
     *
     * @Entity
     * @Table(name="moneda", schema="general")
     */
class moneda extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="codigo", type="string", length=10)
    */
    private $codigo;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion = '';
    /**
      * @Column(name="cambio", type="float", precision=10, scale=0)
    */
    private $cambio = '0';

    
    public function getId()
    {
        return $this->id;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setCambio($cambio)
    {
        $this->cambio = $cambio;
    }
    
    public function getCambio()
    {
        return $this->cambio;
    }

}

