<?php

require_once("src/base/EntidadGlobal.php");
require_once("src/rules/general/entidad/GrupoCliente.php");
require_once("src/rules/general/entidad/Terminos.php");
require_once("src/rules/general/entidad/PlanCuentas.php");
require_once("src/rules/general/entidad/Ciudad.php");
require_once("src/rules/general/entidad/Empleado.php");
require_once("src/rules/general/entidad/EstadoCivil.php");

/**
 * @Entity
 * @Table(name="cliente", schema="general")
 */
class Cliente extends EntidadGlobal {

    /**
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(name="codigo", type="string", length=10)
     */
    private $Codigo;

    /**
     * @Column(name="nombre", type="string", length=80)
     */
    private $Nombre;
    
    /**
     * @Column(name="idtipocliente", type="string", length=1)
     */
    private $TipoCliente;

    /**
     * @Column(name="oblcontabilidad", type="string", length=15)
     */
    private $ObligaContabilidad;

    /**
     * @Column(name="direccion", type="string", length=150)
     */
    private $Direccion;

    /**
     * @Column(name="telefono", type="string", length=20)
     */
    private $Telefono;

    /**
     * @Column(name="email", type="string", length=60)
     */
    private $Email;

    /**
     * @Column(name="sexo", type="string", length=1)
     */
    private $Sexo;
    
    /**
     * @Column(name="movil", type="string", length=15)
     */
    private $Movil;
        
    /**
    * @Column(name="fechanacimiento", type="datetime")
    */
    private $FechaNacimiento;
    
    /**
    * @Column(name="fechacliente", type="datetime")
    */
    private $FechaCliente;
    
    /**
     * @ManyToOne(targetEntity="GrupoCliente")
     * @JoinColumn(name="idgrupo", referencedColumnName="id", nullable=false)
     */
    private $GrupoCliente;

    /**
     * @ManyToOne(targetEntity="PlanCuentas")
     * @JoinColumn(name="ctaxcobrar", referencedColumnName="id", nullable=false)
     */
    private $CuentaPorCobrar;
    
    /**
     * @ManyToOne(targetEntity="PlanCuentas")
     * @JoinColumn(name="ctaanticip", referencedColumnName="id", nullable=false)
     */
    private $CuentaDeAnticipo;

    /**
     * @ManyToOne(targetEntity="Terminos")
     * @JoinColumn(name="idtermino", referencedColumnName="id", nullable=false)
     */
    private $Terminos;

    /**
     * @ManyToOne(targetEntity="Ciudad")
     * @JoinColumn(name="idciudad", referencedColumnName="id", nullable=false)
     */
    private $Ciudades;
    
    /**
     * @ManyToOne(targetEntity="Empleado")
     * @JoinColumn(name="idvendedor", referencedColumnName="id", nullable=false)
     */
    private $Vendedor;
    
    /**
     * @ManyToOne(targetEntity="EstadoCivil")
     * @JoinColumn(name="idestadocivil", referencedColumnName="id", nullable=false)
     */
    private $EstadoCivil;
   
    public function getId() {
        return $this->id;
    }

    function getCodigo() {
        return $this->Codigo;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getTipoCliente() {
        return $this->TipoCliente;
    }

    function getObligaContabilidad() {
        return $this->ObligaContabilidad;
    }

    function getDireccion() {
        return $this->Direccion;
    }

    function getTelefono() {
        return $this->Telefono;
    }

    function getEmail() {
        return $this->Email;
    }

    function getSexo() {
        return $this->Sexo;
    }

    function getMovil() {
        return $this->Movil;
    }

    function getFechaNacimiento() {
        return $this->FechaNacimiento;
    }

    function getGrupoCliente() {
        return $this->GrupoCliente;
    }

    function getCuentaporCobrar() {
        return $this->CuentaPorCobrar;
    }

    function getCuentaDeAnticipo() {
        return $this->CuentaDeAnticipo;
    }

    function getTerminos() {
        return $this->Terminos;
    }

    function getCiudades() {
        return $this->Ciudades;
    }

    function getVendedor() {
        return $this->Vendedor;
    }

    function getEstadoCivil() {
        return $this->EstadoCivil;
    }

    function setCodigo($Codigo) {
        $this->Codigo = $Codigo;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setTipoCliente($TipoCliente) {
        $this->TipoCliente = $TipoCliente;
    }

    function setObligaContabilidad($ObligaContabilidad) {
        $this->ObligaContabilidad = $ObligaContabilidad;
    }

    function setDireccion($Direccion) {
        $this->Direccion = $Direccion;
    }

    function setTelefono($Telefono) {
        $this->Telefono = $Telefono;
    }

    function setEmail($Email) {
        $this->Email = $Email;
    }

    function setSexo($Sexo) {
        $this->Sexo = $Sexo;
    }

    function setMovil($Movil) {
        $this->Movil = $Movil;
    }

    function setFechaNacimiento($FechaNacimiento) {
        $this->FechaNacimiento = $FechaNacimiento;
    }

    function setGrupoCliente(GrupoCliente $GrupoCliente) {
        $this->GrupoCliente = $GrupoCliente;
    }

    function setCuentaporCobrar(PlanCuentas $CuentaporCobrar) {
        $this->CuentaPorCobrar = $CuentaporCobrar;
    }

    function setCuentaDeAnticipo(PlanCuentas $CuentaDeAnticipo) {
        $this->CuentaDeAnticipo = $CuentaDeAnticipo;
    }

    function setTerminos(Terminos $Terminos) {
        $this->Terminos = $Terminos;
    }

    function setCiudades(Ciudad $Ciudades) {
        $this->Ciudades = $Ciudades;
    }

    function setVendedor(Empleado $Vendedor) {
        $this->Vendedor = $Vendedor;
    }

    function setEstadoCivil(EstadoCivil $EstadoCivil) {
        $this->EstadoCivil = $EstadoCivil;
    }

    function getFechaCliente() {
        return $this->FechaCliente;
    }

    function setFechaCliente($FechaCliente) {
        $this->FechaCliente = $FechaCliente;
    }



}
