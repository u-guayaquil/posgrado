<?php

require_once("src/base/EntidadGlobal.php");
require_once("src/rules/general/entidad/ClaseAcreedor.php");
require_once("src/rules/impuesto/entidad/Identificacion.php");
require_once("src/rules/general/entidad/PlanCuentas.php");
require_once("src/rules/general/entidad/Ciudad.php");
require_once("src/rules/impuesto/entidad/Identificacion.php");

/**
 * @Entity
 * @Table(name="acreedor", schema="general")
 */
class Acreedor extends EntidadGlobal {

    /**
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(name="codigo", type="string", length=10)
     */
    private $Codigo;

    /**
     * @Column(name="nombre", type="string", length=80)
     */
    private $Nombre;

    /**
     * @Column(name="num_ident", type="string", length=15)
     */
    private $NumIdent;

    /**
     * @Column(name="direccion", type="string", length=150)
     */
    private $Direccion;

    /**
     * @Column(name="telefono", type="string", length=20)
     */
    private $Telefono;

    /**
     * @Column(name="fax", type="string", length=20)
     */
    private $Fax;

    /**
     * @Column(name="email", type="string", length=60)
     */
    private $Email;

    /**
     * @Column(name="idctaxpagar", type="string", length=20)
     */
    private $idCtaxPagar;
    
    /**
     * @ManyToOne(targetEntity="PlanCuentas")
     * @JoinColumn(name="idctaxpagar", referencedColumnName="id", nullable=false)
     */
    private $CuentaPorPagar;

    /**
     * @ManyToOne(targetEntity="PlanCuentas")
     * @JoinColumn(name="idctanticip", referencedColumnName="id", nullable=false)
     */
    private $CuentaDeAnticipo;

    /**
     * @ManyToOne(targetEntity="ClaseAcreedor")
     * @JoinColumn(name="idclase", referencedColumnName="id", nullable=false)
     */
    private $ClaseAcreedores;

    /**
     * @ManyToOne(targetEntity="Identificacion")
     * @JoinColumn(name="tipo_ident", referencedColumnName="id", nullable=false)
     */
    private $Identificaciones;
    
    /**
     * @ManyToOne(targetEntity="Ciudad")
     * @JoinColumn(name="idciudad", referencedColumnName="id", nullable=false)
     */
    private $Ciudades;

    public function getId() {
        return $this->id;
    }

    public function setCodigo($Codigo) {
        $this->Codigo = $Codigo;
    }

    public function getCodigo() {
        return $this->Codigo;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function setNumIdent($NumIdent) {
        $this->NumIdent = $NumIdent;
    }

    public function getNumIdent() {
        return $this->NumIdent;
    }

    public function setDireccion($Direccion) {
        $this->Direccion = $Direccion;
    }

    public function getDireccion() {
        return $this->Direccion;
    }

    public function setTelefono($Telefono) {
        $this->Telefono = $Telefono;
    }

    public function getTelefono() {
        return $this->Telefono;
    }

    public function setFax($Fax) {
        $this->Fax = $Fax;
    }

    public function getFax() {
        return $this->Fax;
    }

    public function setEmail($Email) {
        $this->Email = $Email;
    }

    public function getEmail() {
        return $this->Email;
    }

    function getCuentaPorPagar() {
        return $this->CuentaPorPagar;
    }

    function getCuentaDeAnticipo() {
        return $this->CuentaDeAnticipo;
    }

    function getClaseAcreedores() {
        return $this->ClaseAcreedores;
    }

    function getIdentificaciones() {
        return $this->Identificaciones;
    }

    function setClaseAcreedores($ClaseAcreedores) {
        $this->ClaseAcreedores = $ClaseAcreedores;
    }

    function setIdentificaciones($Identificaciones) {
        $this->Identificaciones = $Identificaciones;
    }

    function setCuentaPorPagar($CuentaPorPagar) {
        $this->CuentaPorPagar = $CuentaPorPagar;
    }

    function setCuentaDeAnticipo($CuentaDeAnticipo) {
        $this->CuentaDeAnticipo = $CuentaDeAnticipo;
    }
    
    function getCiudades() {
        return $this->Ciudades;
    }

    function setCiudades($Ciudades) {
        $this->Ciudades = $Ciudades;
    }


}
