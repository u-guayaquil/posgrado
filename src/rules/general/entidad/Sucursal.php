<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/general/entidad/Ciudad.php");

    /**
     * @Entity
     * @Table(name="sucursal", schema="general")
     */
    
    class Sucursal extends EntidadGlobal
    {   /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="idciudad", type="integer")
         */
        private $idciudad;
        
        /**
         * @Column(name="descripcion", type="string", length=30, nullable=false)
         */
        private $descripcion;

        /**
         * @Column(name="direccion", type="string", length=150, nullable=false)
         */
        private $direccion;

        /**
         * @Column(name="telefono", type="string", length=20, nullable=false)
         */
        private $telefono;

        /**
         * @Column(name="fax", type="string", length=20, nullable=false)
         */
        private $fax;
        
        /**
         * @ManyToOne(targetEntity="Ciudad")
         * @JoinColumn(name="idciudad", referencedColumnName="id")
         */
        private $ciudades;

        public function getId()
        {      return $this->id;
        }

        public function setDescripcion($descripcion)
        {      $this->descripcion = $descripcion;
        }

        public function getDescripcion()
        {      return $this->descripcion;
        }

        public function setDireccion($direccion)
        {      $this->direccion = $direccion;
        }

        public function getDireccion()
        {      return $this->direccion;
        }

        public function setTelefono($telefono)
        {      $this->telefono = $telefono;
        }

        public function getTelefono()
        {      return $this->telefono;
        }

        public function setFax($fax)
        {      $this->fax = $fax;
        }

        public function getFax()
        {      return $this->fax;
        }

        public function getCiudades()
        {      return $this->ciudades;
        }
        public function setCiudades(Ciudad $ciudades)
        {      $this->ciudades = $ciudades;
        }

        public function getIdciudad()
        {      return $this->idciudad;
        }
        
        public function setIdciudad($idciudad)
        {      $this->idciudad = $idciudad;
        }
        
        
}

