<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/general/entidad/Provincia.php");

    /**
     * @Entity
     * @Table(name="ciudad", schema="general")
     */
    class Ciudad extends EntidadGlobal
    {   /**
         * @Id
         * @Column(name="id", type="integer")
         * @GeneratedValue
         */
        private $id;
        /**
         * @Column(name="idprovincia", type="integer")
         */
        private $idprovincia;
        
        /**
         * @Column(name="descripcion", type="string", length=20, nullable=false)
         */
        private $descripcion;    
    
        /**
         * @ManyToOne(targetEntity="Provincia",inversedBy="ciudad")
         * @JoinColumn(name="idprovincia", referencedColumnName="id", nullable=false)
         */
        private $provincia;
        
        /**
         * @Column(name="codigo", type="string", length=10, nullable=false)
         */
        private $codigo;

        public function getId()
        {      return $this->id;
        }

        public function setDescripcion($descripcion)
        {      $this->descripcion = $descripcion;
        }

        public function getDescripcion()
        {      return $this->descripcion;
        }

        public function setProvincia(Provincia $provincia)
        {      $this->provincia = $provincia;
        }
    
        public function getProvincia()
        {      return $this->provincia;
        }
    
        public function setIdprovincia($idprovincia)
        {      $this->idprovincia = $idprovincia;
        }
    
        public function getIdprovincia()
        {      return $this->idprovincia;
        }
    
        public function setCodigo($codigo)
        {      $this->codigo = $codigo;
        }
    
        public function getCodigo()
        {      return $this->codigo;
        }
}

