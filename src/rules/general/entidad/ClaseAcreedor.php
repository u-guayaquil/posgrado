<?php
require_once("src/rules/general/entidad/Acreedor.php");
    /**
     * @Entity
     * @Table(name="claseacreedor", schema="general")
     */
class ClaseAcreedor
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion;
    
     /**
     * @OneToMany(targetEntity="Acreedor",
     *            mappedBy="ClaseAcreedores")
     */
    private $Acreedores;


    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function getAcreedores(){
	return $this->Acreedores;
    }
	
    public function setAcreedores(Acreedor $Acreedores){
           $this->Acreedores = $Acreedores;
    }
}

