<?php
        
        require_once ("src/rules/sistema/entidad/Usuario.php");
        require_once ("src/rules/general/entidad/Sucursal.php");

        /**
         *
         * @Table(name="sucursal_usuario", schema="general")
         * @Entity
         */
        class SucursalUsuario
        {
            /**
             * @Id
             * @Column(name="id", type="integer", nullable=false)
             * @GeneratedValue
             */
            private $id;

            /**
             * @ManyToOne(targetEntity="Sucursal")
             * @JoinColumn(name="idsucursal", referencedColumnName="id")
             */
            private $Sucursales;

            /**
             * @ManyToOne(targetEntity="Usuario")
             * @JoinColumn(name="idusuario", referencedColumnName="id")
             */
            private $Usuarios;


            public function getId()
            {      return $this->id;
            }

            public function setSucursales(Sucursal $sucursal)
            {      $this->Sucursales = $sucursal;
            }

            public function getSucursales()
            {      return $this->Sucursales;
            }

            public function setUsuarios(Usuario $usuario)
            {      $this->Usuarios = $usuario;
            }

            public function getUsuarios()
            {      return $this->Usuarios;
            }
}

