<?php

    /**
      * @Entity
      * @Table(name="empresa", schema="general")
      */
    class Empresa
    {
           /**
            *
            * @Column(name="id", type="integer", nullable=false)
            * @Id
            * @GeneratedValue
            */
            private $id;

           /**
            *
            * @Column(name="comercial", type="string", length=100, nullable=false)
            */
            private $comercial;

           /**
            * @Column(name="razonsocial", type="string", length=100, nullable=false)
            */
            private $razonsocial;

           /**
            * @Column(name="tipo_ident_razon", type="integer", nullable=false)
            */
            private $tipoIdentRazon = 1;

           /**
            * @Column(name="num_ident_razon", type="string", length=15, nullable=false)
            */
            private $numIdentRazon = '';

           /**
            * @Column(name="representante", type="string", length=80, nullable=false)
            */
            private $representante = '';

           /**
            * @Column(name="tipo_ident_repres", type="integer", nullable=false)
            */
            private $tipoIdentRepres = 1;

           /**
            * @Column(name="num_ident_repres", type="string", length=15, nullable=false)
            */
            private $numIdentRepres = '';

           /**
            *
            * @Column(name="contador", type="string", length=80, nullable=true)
            */
            private $contador = '';

           /**
            * @Column(name="tipo_ident_contador", type="integer", nullable=false)
            */
            private $tipoIdentContador = 1;

           /**
            *
            * @Column(name="num_ident_contador", type="string", length=15, nullable=true)
            */
            private $numIdentContador = '';

           /**
            * @Column(name="esp_contribuyente", type="integer", nullable=false)
            */
            private $espContribuyente = 0;

           /**
            * @Column(name="obl_contabilidad", type="integer", nullable=false)
            */
            private $oblContabilidad = 0;

           /**
            * @Column(name="idmoneda", type="integer", nullable=false)
            */
            private $idmoneda;

           /**
            * @ManyToOne(targetEntity="Sucursal") 
            * @JoinColumn(name="idsucursal", referencedColumnName="id")
            */
            private $sucursales; 
            
           /**
            * @Column(name="idsucursal", type="integer", nullable=false)
            */
            private $idsucursal;
            
            
            public function getId()
            {      return $this->id;
            }

            public function setComercial($comercial)
            {      $this->comercial = $comercial;
            }

            public function getComercial()
            {      return $this->comercial;
            }

            public function setRazonsocial($razonsocial)
            {      $this->razonsocial = $razonsocial;
            }

            public function getRazonsocial()
            {      return $this->razonsocial;
            }

            public function setTipoIdentRazon($tipoIdentRazon)
            {      $this->tipoIdentRazon = $tipoIdentRazon;
            }

            public function getTipoIdentRazon()
            {      return $this->tipoIdentRazon;
            }

            public function setNumIdentRazon($numIdentRazon)
            {      $this->numIdentRazon = $numIdentRazon;
            }

            public function getNumIdentRazon()
            {      return $this->numIdentRazon;
            }

            public function setRepresentante($representante)
            {      $this->representante = $representante;
            }

            public function getRepresentante()
            {      return $this->representante;
            }

            public function setTipoIdentRepres($tipoIdentRepres)
            {      $this->tipoIdentRepres = $tipoIdentRepres;
            }

            public function getTipoIdentRepres()
            {      return $this->tipoIdentRepres;
            }

            public function setNumIdentRepres($numIdentRepres)
            {      $this->numIdentRepres = $numIdentRepres;
            }

            public function getNumIdentRepres()
            {      return $this->numIdentRepres;
            }

            public function setContador($contador)
            {      $this->contador = $contador;
            }

            public function getContador()
            {      return $this->contador;
            }

            public function setTipoIdentContador($tipoIdentContador)
            {      $this->tipoIdentContador = $tipoIdentContador;
            }

            public function getTipoIdentContador()
            {      return $this->tipoIdentContador;
            }

            public function setNumIdentContador($numIdentContador)
            {      $this->numIdentContador = $numIdentContador;
            }

            public function getNumIdentContador()
            {      return $this->numIdentContador;
            }

            public function setEspContribuyente($espContribuyente)
            {      $this->espContribuyente = $espContribuyente;
            }

            public function getEspContribuyente()
            {      return $this->espContribuyente;
            }

            public function setOblContabilidad($oblContabilidad)
            {      $this->oblContabilidad = $oblContabilidad;
            }

            public function getOblContabilidad()
            {      return $this->oblContabilidad;
            }

            public function setIdmoneda($idmoneda)
            {      $this->idmoneda = $idmoneda;
            }

            public function getIdmoneda()
            {      return $this->idmoneda;
            }

            public function setIdsucursal($idsucursal)
            {      $this->idsucursal = $idsucursal;
            }

            public function getIdsucursal()
            {      return $this->idsucursal;
            }

            function getSucursales()
            {       return $this->sucursales;
            }
	
            function setSucursales(Sucursal $sucursales)
            {       $this->sucursales = $sucursales;
            }
            
    }
?>

