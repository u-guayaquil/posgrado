<?php
            require_once("src/rules/importacion/dao/DAOPedido.php");
            require_once("src/rules/general/servicio/ServicioAcreedor.php");
            require_once("src/rules/inventario/servicio/ServicioProducto.php");
            require_once("src/rules/inventario/servicio/ServicioItem.php");
            require_once("src/rules/inventario/servicio/ServicioAtributoValor.php");
            require_once("src/rules/general/servicio/ServicioUnidad.php");
            
            
            class ServicioPedido
            {     private $DAOPedido;
                  private $ServicioAtributoValor;
                  private $ServicioItem;
                  private $ServicioAcreedor;
                  private $ServicioProducto;
                  private $ServicioUnidad;
                  private $Fecha;
     
                  function __construct()
                  {         $this->DAOPedido = new DAOPedido();
                            $this->ServicioAtributoValor = new ServicioAtributoValor();
                            $this->ServicioItem = new ServicioItem();   
                            $this->ServicioAcreedor = new ServicioAcreedor(); 
                            $this->ServicioProducto = new ServicioProducto(); 
                            $this->ServicioUnidad = new ServicioUnidad();
                            $this->Fecha  = new DateControl();
                  }
                  
                  function GuardaDBPedido($Form)
                  {         $fecha = explode('/', $Form->fecha_Pedido);                 
                            $Form->fecha_Pedido = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                                    $temporal = $this->TraeValoresenCeroTemporal($_SESSION['IDUSER']);
                                    if(count($temporal) <= 0){
                                          if (empty($Form->id_Pedido)){                              
                                            return $this->DAOPedido->InsertaPedido($Form);
                                          }else{
                                            //$this->DAOPedido->EliminaDetalle($Form->idPedido_Pedido);
                                            return $this->DAOPedido->ActualizaPedido($Form);
                                          }
                                    }else{
                                        return 'Debe corregir los errores de los detalles..!!!';
                                    }
                              
                  }
                  
                  function DesactivaPedido($PrepareDQL)
                  {         return $this->DAOPedido->DesactivaPedido($PrepareDQL);
                  }
           
                  function BuscarPedido($PrepareDQL)
                  {         return $this->DAOPedido->ObtenerPedido($PrepareDQL);
                  }
                  
                  function BuscarDetallePedido($PrepareDQL)
                  {         return $this->DAOPedido->BuscarDetallePedido($PrepareDQL);
                  }
                  
                  function BuscarProductoByCodigo($PrepareDQL)
                  {         return $this->DAOPedido->BuscarProductoByCodigo($PrepareDQL);
                  }
                  
                  function BuscarProductoByCodigoenTemporal($PrepareDQL)
                  {         return $this->DAOPedido->BuscarProductoenTemporal($PrepareDQL);
                  }
                  
                  function TraeNombreExcel($usuario)
                  {         return $this->DAOPedido->TraeNombreExcel($usuario);
                  }
                  
                  function TraeDatosPedidoTemporal($usuario,$origen)
                  {         return $this->DAOPedido->TraeDatosPedidoTemporal($usuario,$origen);
                  }
                  
                  function TraeValoresenCeroTemporal($usuario)
                  {         return $this->DAOPedido->TraeValoresenCeroTemporal($usuario);
                  }
                  
                  function ActualizaOpcionTablaTemporal($usuario,$id,$opcion)
                  {         return $this->DAOPedido->ActualizaOpcionTablaTemporal($usuario,$id,$opcion);
                  }
                  
                  function TraeDatosPedidoTemporalRefresca($usuario)
                  {         return $this->DAOPedido->TraeDatosPedidoTemporalRefresca($usuario);
                  }
                  
                  function TraeDatosPedidoTemporalRefrescaActual($pedido)
                  {         return $this->DAOPedido->TraeDatosPedidoDetallesSumados($pedido);
                  }
                  
                  function TraeDatosTemporalesSumados($usuario)
                  {         return $this->DAOPedido->TraeDatosTemporalesSumados($usuario);
                  }
                  
                  function EliminaDatoenTablaTemporal($usuario,$codigo)
                  {         return $this->DAOPedido->EliminaDatoenTablaTemporal($usuario,$codigo);
                  }
                  
                  function ActualizaCampoTemporalconDatosIngresados($datos)
                  {         //extraigo la abreviaruta de la unidades de presentacion.                            
                                $preparePDQL = array('id' => $datos['unipresenta']);
                                $abunipres = $this->ServicioUnidad->BuscarUnidadByDescripcion($preparePDQL);
                                $abunipresent = $abunipres[0]['abreviatura'];

                            //extraigo la abreviaruta de la unidades de importacion.
                                $prepareIDQL = array('id' => $datos['uniimpor']);
                                $abuniimpo = $this->ServicioUnidad->BuscarUnidadByDescripcion($prepareIDQL);  
                                $abuniimporta = $abuniimpo[0]['abreviatura'];
                            
                            $datosActualizar = array('codigo' => $datos['codigo'], 'cantpresenta' => $datos['cantpresenta'], 'unipresenta' => $abunipresent
                                                 ,'cantimpor' => $datos['cantimpor'], 'uniimpor' => $abuniimporta
                                                 , 'precio' => $datos['precio'], 'idusuario' => $datos['idusuario']);
                           
                            return $this->DAOPedido->ActualizaCampoTemporalconDatosIngresados($datosActualizar);
                  }
                  
                  function EliminasdatosTemporales($usuario){
                            $this->DAOPedido->EliminaDetalleTemporal($usuario);
                  }
                  
                  function ActualizaCantidadesTemporalesconReales($datos){
                            //extraigo la abreviaruta de la unidades de presentacion.                            
                                $preparePDQL = array('id' => $datos['unipresenta']);
                                $abunipres = $this->ServicioUnidad->BuscarUnidadByDescripcion($preparePDQL);
                                $abunipresent = $abunipres[0]['abreviatura'];

                            //extraigo la abreviaruta de la unidades de importacion.
                                $prepareIDQL = array('id' => $datos['uniimpor']);
                                $abuniimpo = $this->ServicioUnidad->BuscarUnidadByDescripcion($prepareIDQL);  
                                $abuniimporta = $abuniimpo[0]['abreviatura'];
                            
                            $datosActualizar = array('codigo' => $datos['codigo'], 'cantpresenta' => $datos['cantpresenta'], 'unipresenta' => $abunipresent
                                                 ,'cantimpor' => $datos['cantimpor'], 'uniimpor' => $abuniimporta
                                                 , 'precio' => $datos['precio'], 'idusuario' => $datos['idusuario']);                      
                            $this->DAOPedido->SumaCampoTemporalconDatosIngresados($datosActualizar);
                  }
                   
                  function LeeExcelPedido($usuario,$direccion){
                            require_once("src/base/Excel.php");
                            //$this->DAOPedido->EliminaDetalleTemporal($usuario);
                            $objPHPExcel = new PHPExcel();
                            $objPHPExcel = PHPExcel_IOFactory::load(trim($direccion));
                            $sheetData   = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);                        
                            $banderaGeneral =0;
                            $letra ='';
                            for($i=2; $i<=count($sheetData); $i++){
                            
                                    $codig = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getFormattedValue(); 
                                    $nombr = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getFormattedValue();
                                    $prove = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getFormattedValue();
                                    $linea = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getFormattedValue();
                                    $grupo = $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getFormattedValue();
                                    $p_iva = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getFormattedValue();
                                    $marca = $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getFormattedValue();
                                    $comis = $objPHPExcel->getActiveSheet()->getCell('K'.$i)->getFormattedValue();
                                    $categ = $objPHPExcel->getActiveSheet()->getCell('M'.$i)->getFormattedValue();
                                    $canti = $objPHPExcel->getActiveSheet()->getCell('O'.$i)->getFormattedValue();
                                    $cajas = $objPHPExcel->getActiveSheet()->getCell('P'.$i)->getFormattedValue();
                                    $preci = $objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getFormattedValue();
                                    $unida = $objPHPExcel->getActiveSheet()->getCell('R'.$i)->getFormattedValue();
                                    $prese = $objPHPExcel->getActiveSheet()->getCell('S'.$i)->getFormattedValue();
                                    
                                    $prepareDQL = array('codigo' => $codig, 'idusuario' => $_SESSION['IDUSER']);
                                    $codtemp = $this->BuscarProductoByCodigoenTemporal($prepareDQL);

                                    if(count($codtemp) > 0){   
                                            //Ingreso los datos en la tabla temporal.
                                            $datosActualizar = array('codigo' => $codig, 'cantpresenta' => $cajas, 'unipresenta' => $prese
                                                                 ,'cantimpor' => $canti, 'uniimpor' => $unida
                                                                 ,'precio' => $preci, 'idusuario' => $_SESSION['IDUSER']);                        
                                           $id = $this->DAOPedido->SumaCampoTemporalconDatosIngresados($datosActualizar);
                                            
                                    }else{
                                            $datosIngresaDetalle = array('codigo' => strtoupper($codig)
                                                              ,'nombre' => $nombr,'prove' => ($prove),'linea' => $linea,'grupo' => $grupo,'p_iva' => $p_iva
                                                              ,'marca' => $marca,'comis' => $comis,'categ' => $categ,'canti' => $canti,'cajas' => $cajas
                                                              ,'precio' => $preci,'unidad' => $unida,'presenta' => $prese,'usuario' => $usuario,'origen' => 'E');
                                            //INSERTO LOS DATOS EN LA TABLA TEMPORAL.
                                            $id = $this->DAOPedido->InsertarDetalleTemporal($datosIngresaDetalle);
                                    }
                                    if (!is_numeric($id)){
                                        $banderaGeneral=1;
                                        break;
                                    }
                            }    
                            if($banderaGeneral == 0){
                                return $this->ValidaCamposTemporales($usuario);
                            }else{
                                return 'No se pudo subir el Excel...';
                            }
                    }
                    
                    function IngresaCamposTemporalesdesdeReales($usuario,$datos){
                            $this->DAOPedido->EliminaDetalleTemporal($usuario);                        
                            foreach ($datos as $temporales){
                                    $codig = $temporales['codigo'];             $p_iva = 1;
                                    $nombr = $temporales['producto'];           $comis = 1;
                                    $prove = 'PROVEEDOR';               
                                    $linea = 'TODOS';                           $marca = 'TODOS';
                                    $grupo = 'TODOS';                           $categ = 'TODOS';
                                    $canti = $temporales['cant_importacion'];
                                    $cajas = $temporales['cant_presentacion'];
                                    $preci = $temporales['precio'];
                                    $unida = $temporales['avbimportacion'];
                                    $prese = $temporales['avbpresentacion'];
                                    
                                    $datosIngresaDetalle = array('codigo' => strtoupper($codig)
                                                      ,'nombre' => $nombr,'prove' => ($prove),'linea' => $linea,'grupo' => $grupo,'p_iva' => $p_iva
                                                      ,'marca' => $marca,'comis' => $comis,'categ' => $categ,'canti' => $canti,'cajas' => $cajas
                                                      ,'precio' => $preci,'unidad' => $unida,'presenta' => $prese,'usuario' => $usuario,'origen' => 'B');
                                    //INSERTO LOS DATOS EN LA TABLA TEMPORAL.
                                    $this->DAOPedido->InsertarDetalleTemporal($datosIngresaDetalle);
                            }
                    }
                    
                    function IngresaDatosDigitadosenTemporal($datos){ 
                        
                                    //extraigo la abreviaruta de la unidades de presentacion.
                                    $preparePDQL = array('id' => $datos['unipresenta']);
                                    $abunipres = $this->ServicioUnidad->BuscarUnidadByDescripcion($preparePDQL);
                                    $abunipresent = $abunipres[0]['abreviatura'];

                                    //extraigo la abreviaruta de la unidades de importacion.
                                    $prepareIDQL = array('id' => $datos['uniimpor']);
                                    $abuniimpo = $this->ServicioUnidad->BuscarUnidadByDescripcion($prepareIDQL); 
                                    $abuniimporta = $abuniimpo[0]['abreviatura'];
                        
                                    $codig = $datos['codigo'];                  $p_iva = 1;
                                    $nombr = $datos['producto'];                $comis = 1;
                                    $usuar = $datos['idusuario'];               $prove = 'PROVEEDOR';  
                                    $canti = $datos['cantimpor'];               $linea = 'TODOS';
                                    $cajas = $datos['cantpresenta'];            $marca = 'TODOS';
                                    $preci = $datos['precio'];                  $grupo = 'TODOS'; 
                                    $unida = $abuniimporta;                     $categ = 'TODOS';
                                    $prese = $abunipresent;
                                    
                                    $datosIngresaDetalle = array('codigo' => strtoupper($codig)
                                                      ,'nombre' => $nombr,'prove' => ($prove),'linea' => $linea,'grupo' => $grupo,'p_iva' => $p_iva
                                                      ,'marca' => $marca,'comis' => $comis,'categ' => $categ,'canti' => $canti,'cajas' => $cajas
                                                      ,'precio' => $preci,'unidad' => $unida,'presenta' => $prese,'usuario' => $usuar,'origen' => 'B');
                                    //INSERTO LOS DATOS EN LA TABLA TEMPORAL.
                                    $this->DAOPedido->InsertarDetalleTemporal($datosIngresaDetalle);
                    }
                    
                    function ValidaCamposTemporales($usuario){
                            //leo la tabla temporal para la validacion de campos
                            $DatosValida = $this->TraeDatosPedidoTemporal($usuario,'E');
                            $Mensajes = array();
                            foreach ($DatosValida as $campo){
                                   $grupo = trim(strtoupper($campo['idgrupo']));  
                                   $desgrupo = trim(strtoupper($campo['grupo'])); 
                                   $linea = trim(strtoupper($campo['idlinea']));
                                   $deslinea = trim(strtoupper($campo['linea'])); 
                                   $marca = trim(strtoupper($campo['idmarca'])); 
                                   $desmarca = trim(strtoupper($campo['marca']));
                                   $categoria = trim(strtoupper($campo['idcategoria']));
                                   $descategoria = trim(strtoupper($campo['categoria']));
                                   $cantidad = trim(strtoupper($campo['cantidad']));
                                   $producto = trim(strtoupper($campo['codigoproducto']));
                                   $codproducto = trim(strtoupper($campo['codigo']));
                                   $ctamercaderia = trim(strtoupper($campo['ctamercaderia']));
                                   $ivacompras = trim(strtoupper($campo['ivacompras']));
                                   $presentacion = trim(strtoupper($campo['codunidadpres']));
                                   $id = trim(strtoupper($campo['id']));
                                   
                                        if($cantidad == 0){
                                            $presenta = array('mensaje' => 'No tiene cantidad ', 'producto' => $codproducto);
                                            array_push ( $Mensajes , $presenta );
                                            $this->ActualizaOpcionTablaTemporal($usuario,$id,0);
                                        }
                                       
                                        if($grupo == ''){
                                            $presenta = array('mensaje' => 'No pertenece al Grupo '.$desgrupo, 'producto' => $codproducto);
                                            array_push ( $Mensajes , $presenta );
                                            $this->ActualizaOpcionTablaTemporal($usuario,$id,0);
                                        }
                                        
                                        if($linea == ''){
                                            $presenta = array('mensaje' => 'No pertenece a la Linea '.$deslinea, 'producto' => $codproducto);
                                            array_push ( $Mensajes , $presenta );
                                            $this->ActualizaOpcionTablaTemporal($usuario,$id,0);
                                        }
                                        
                                        if($marca == ''){
                                            $presenta = array('mensaje' => 'No pertenece a la Marca '.$desmarca, 'producto' => $codproducto);
                                            array_push ( $Mensajes , $presenta );
                                            $this->ActualizaOpcionTablaTemporal($usuario,$id,0);
                                        }
                                        
                                        if($categoria == ''){
                                            $presenta = array('mensaje' => 'No pertenece a la Categoria '.$descategoria, 'producto' => $codproducto);
                                            array_push ( $Mensajes , $presenta );
                                            $this->ActualizaOpcionTablaTemporal($usuario,$id,0);
                                        }
                                        
                                        if($ctamercaderia == ''){
                                            $presenta = array('mensaje' => 'No tiene asignado cuenta de mercaderia ', 'producto' => $codproducto);
                                            array_push ( $Mensajes , $presenta );
                                            $this->ActualizaOpcionTablaTemporal($usuario,$id,0);
                                        }
                                        
                                        if($ivacompras == ''){
                                            $presenta = array('mensaje' => 'No tiene asignado el IVA para las compras ', 'producto' => $codproducto);
                                            array_push ( $Mensajes , $presenta );
                                            $this->ActualizaOpcionTablaTemporal($usuario,$id,0);
                                        }
                         
                                        if($presentacion == 0){
                                            $presenta = array('mensaje' => 'No tiene presentacion ', 'producto' => $codproducto);
                                            array_push ( $Mensajes , $presenta );
                                            $this->ActualizaOpcionTablaTemporal($usuario,$id,0);
                                        }
                            }  
                            if(count($Mensajes) == 0){
                                $this->ActualizaOpcionTablaTemporal($usuario,$id,1);
                                return $this->PresentaMensaje();
                            }else{
                                $this->ValidaResultadosExcel($Mensajes);
                            }
                    }
                  
                    function ValidaResultadosExcel($Resultado)
                    {       $this->CreaHojadeExcel($Resultado);
                    }
                    
                    function CreaHojadeExcel($Datos)
                        {   error_reporting(E_ALL);
                            ini_set('display_errors', TRUE);
                            ini_set('display_startup_errors', TRUE);
                            date_default_timezone_set('Europe/London');
                            define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

                            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition: attachment;filename="MensajesAlerta.xlsx"');
                            header('Cache-Control: max-age=0');
                            require_once 'src/libs/plugins/excel/Clases/PHPExcel.php';
                            $objPHPExcel = new PHPExcel();

                            $objPHPExcel->getProperties()->setCreator("Magicnegsa")
                                                        ->setTitle("Productos")
                                                        ->setSubject("Productos")
                                                        ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                                                        ->setKeywords("office PHPExcel php")
                                                        ->setCategory("Test result file");

                            $objPHPExcel->setActiveSheetIndex(0);
                            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'CODIGO');
                            $objPHPExcel->getActiveSheet()->mergeCells('B1:F1');   
                            $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MENSAJE'); 

                            $i=2;
                            foreach ($Datos as $campo){
                                $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $campo['producto']);
                                $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':F'.$i);
                                $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $campo['mensaje']);
                                $i++;
                            }

                            $objPHPExcel->getActiveSheet()->setTitle('Productos');
                            $objPHPExcel->setActiveSheetIndex(0);
                            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                            ob_end_clean();
                            ob_start();

                            $objWriter->save('php://output');                    
                    }

                    function PresentaMensaje()
                    {       $script = '<script language="javaScript" type="text/javascript">';
                            $script.= '        window.parent.refrescaExcel(\'btrefresh\');';
                            $script.= '</script>';
                            $script.= '<div style="border:0px dotted #000; width:92%; padding:10px; font-family: tahoma; font-size:9pt">';
                            $script.= '     <b>El excel se subio correctamente.</b>';
                            $script.= '</div>';
                           
                            return $script;
                    }
            }
?>

