<?php
        require_once("src/base/DBDAO.php");

        class DAOPedido extends DBDAO
        {     private static $entityPedido;        
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityPedido = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityPedido->MyFunction();
                     $this->Fecha  = new DateControl();
              }
    
              public function ObtenerPedido($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(p.documento) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and p.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT p.id, p.idproveedor,p.documento,p.comentario,to_char(p.fecha,'DD/MM/YYYY') as fecha"
                          . ", e.descripcion as txtestado, p.fecreacion, e.id as idestado,UPPER(a.nombre) as proveedor"
                          . " FROM importacion.importacion p"
                          . " INNER JOIN general.Estado e ON p.idestado=e.id"
                          . " INNER JOIN general.acreedor a ON p.idproveedor = a.id"
                          . " WHERE p.idestado=e.id ".$JOIN.$LIKE.$IDD
                          . " ORDER BY p.documento".$LIM; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarDetallePedido($PrepareDQL)
              {      $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE p.id_pedido =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT p.id, p.id_pedido,p.codigo,p.uni_presentacion,p.id_producto,p.cant_presentacion,p.uni_importacion,p.cant_importacion"
                           . ",p.precio,p.total,r.descripcion as producto,n.descripcion as presentacion,u.descripcion as importacion"
                           . " FROM importacion.importacion_detalle p"
                           . " INNER JOIN importacion.importacion d ON p.id_pedido=d.id"
                           . " INNER JOIN inventario.producto r ON p.id_producto = r.id"
                           . " INNER JOIN general.unidad n ON p.uni_presentacion = n.id"
                           . " INNER JOIN general.unidad u ON p.uni_importacion = u.id".$IDD
                           . " ORDER BY d.id"; //echo $DSQL;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarProductoByCodigo($PrepareDQL)
              {      $IDD  = array_key_exists("codigo", $PrepareDQL) ? " WHERE codigo ='".$PrepareDQL['codigo']."'": "";
                     $DSQL = "SELECT id FROM inventario.producto ".$IDD; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarProductoenPedido($producto,$idpedido)
              {      $DSQL = "SELECT id,cant_importacion,cant_presentacion,total "
                           . "FROM importacion.importacion_detalle  "
                           . "WHERE id_producto = ".$producto." and id_pedido = ".$idpedido.""; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarProductoenTemporal($PrepareDQL)
              {      $DSQL = "SELECT id "
                           . "FROM temporal.tmp_pedido  "
                           . "WHERE codigo = '".$PrepareDQL['codigo']."' and idusuario = ".$PrepareDQL['idusuario'].""; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function TraeValoresenCeroTemporal($usuario)
              {      $DSQL = "SELECT codigo FROM temporal.tmp_pedido WHERE idusuario ='".$usuario."' and opcion=0"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function TraeDatosPedidoTemporal($usuario,$origen='')
              {     if($origen!=''){ $condorigen = " and pt.origen='".$origen."'"; }else{ $condorigen = "";}
                    $DSQL = "select pt.id,pt.codigo,pt.cantidad,pt.cajas,pt.precio,pr.id as codigoproducto,pr.idunidad as codunidadprod,pt.idusuario
                            ,un.abreviatura as unidadprod,ud.id as codunidadpres,ud.abreviatura as unidadpres,(pt.cantidad * pt.precio) as total
                            ,i.id as idgrupo,av.id as idlinea,al.id as idmarca,ac.id as idcategoria,pt.grupo,pt.linea,pt.marca,pt.categoria,pt.nombre
                            ,i.ctamercaderia,i.ctaventas,i.ctacostos,i.ctadevolucion,iv.id as ivacompras,vi.id as ivaventas,a.id as proveedor,pt.nombre
                            ,pt.unidad,ui.id as idunidad
                            from temporal.tmp_pedido pt
                            LEFT JOIN inventario.producto pr ON pt.codigo = pr.codigo
                            LEFT JOIN general.unidad un ON pr.idunidad = un.id
                            LEFT JOIN general.unidad ud ON pt.unidadpresenta = ud.abreviatura
                            LEFT JOIN general.unidad ui ON pt.unidad = ui.abreviatura
                            LEFT JOIN inventario.item i ON pt.grupo = i.codigo and i.categoria='B'
                            LEFT JOIN inventario.atributo_valor av ON pt.linea = av.codigo
                            LEFT JOIN inventario.atributo_valor al ON pt.marca = al.codigo
                            LEFT JOIN inventario.atributo_valor ac ON pt.categoria = ac.codigo 
                            LEFT JOIN impuesto.iva iv ON pt.p_iva = 1 and iv.idimpuesto = 7
                            LEFT JOIN impuesto.iva vi ON pt.p_iva = 1 and vi.idimpuesto = 8
                            LEFT JOIN general.acreedor a ON pt.proveedor = a.codigo
                            WHERE pt.idusuario = ".$usuario.$condorigen.
                            "order by pt.id"; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function TraeDatosPedidoTemporalRefresca($usuario)
              {     $DSQL = "select pt.codigo,pt.precio,pr.id as id_producto,pr.descripcion as producto
                                    ,un.id as uni_importacion,un.descripcion as importacion
                                    ,pt.cantidad as cant_importacion
                                    ,ud.id as uni_presentacion,ud.descripcion as presentacion
                                    ,pt.cajas as cant_presentacion
                                    ,pt.cantidad * pt.precio as total
                                    from temporal.tmp_pedido pt
                                    INNER JOIN inventario.producto pr ON pt.codigo = pr.codigo
                                    INNER JOIN general.unidad un ON pt.unidad = un.abreviatura
                                    INNER JOIN general.unidad ud ON pt.unidadpresenta = ud.abreviatura
                                    WHERE pt.idusuario = ".$usuario." ";
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function TraeDatosPedidoDetallesSumados($pedido)
              {      
                    $DSQL = "select pt.codigo,pt.precio,pt.id_producto,pr.descripcion as producto
                             ,pt.uni_importacion,un.descripcion as importacion,pt.cant_importacion
                             ,pt.uni_presentacion,ud.descripcion as presentacion,pt.cant_presentacion
                             ,(pt.cant_importacion * pt.precio) as total
                             ,un.abreviatura as avbimportacion,ud.abreviatura as avbpresentacion
                             from importacion.importacion_detalle pt
                             LEFT JOIN inventario.producto pr ON pt.id_producto = pr.id
                             LEFT JOIN general.unidad un ON pt.uni_importacion = un.id
                             LEFT JOIN general.unidad ud ON pt.uni_presentacion = ud.id
                             WHERE pt.id_pedido = ".$pedido;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function TraeDatosTemporalesSumados($usuario)
              {      
                    $DSQL = "select pt.codigo,pt.precio,pr.id as id_producto,pr.descripcion as producto
                            ,pr.idunidad as uni_importacion,un.descripcion as importacion
                            ,sum(pt.cantidad) as cant_importacion
                            ,ud.id as uni_presentacion,ud.descripcion as presentacion
                            ,sum(pt.cajas) as cant_presentacion
                            ,sum(pt.cantidad * pt.precio) as total
                            ,un.abreviatura as avbimportacion,ud.abreviatura as avbpresentacion
                            from temporal.tmp_pedido pt
                            LEFT JOIN inventario.producto pr ON pt.codigo = pr.codigo
                            LEFT JOIN general.unidad un ON pr.idunidad = un.id
                            LEFT JOIN general.unidad ud ON pt.unidadpresenta = ud.abreviatura
                            WHERE pt.idusuario = ".$usuario
                          . "group by pt.codigo,pt.precio,pr.id,pr.descripcion
                            ,ud.id,ud.descripcion,un.descripcion,un.abreviatura,ud.descripcion";
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
                         
              public function InsertaPedido($datos)
              {      try{        
                        $estado = $datos->estado_Pedido;
                        $proveedor = $datos->idproveedor_Pedido;
                        $documento = strtoupper($datos->documento_Pedido);
                        $comentario = strtoupper($datos->comentario_Pedido);
                        $fecha = $datos->fecha_Pedido;
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO importacion.importacion (idproveedor,documento,comentario,fecha"
                               . ",idestado,iduscreacion,fecreacion)"
                               . "VALUES (".$proveedor.", '".$documento."', '".$comentario."'"
                               . ",to_timestamp('".$fecha."','YYYY-MM-DD'), ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        if (is_numeric($id)){
                            $datosTemporales = $this->TraeDatosPedidoTemporal($_SESSION['IDUSER']);
                              if(count($datosTemporales) >0 ){
                                    $idDet = $this->SeteaDatosDetalleExcel($datosTemporales,$id);
                              }else{
                                    $idDet = $this->InsertaDetallePedido($datos,$id);
                              }
                              if(is_numeric($idDet)){                                    
                                    return intval($id);
                              }else{
                                    return $idDet;
                              }
                        }else{
                                return $id;
                        }
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Pedido..';
                    }                   
              }
              
              function SeteaDatosDetalleExcel($datosTemporales,$idPedido){
                        $idR=0;
                        $DatosProducto ='';
                        foreach ($datosTemporales as $datos){ //$datos['codigoproducto'] , $datos['codunidadprod'] , $datos['codunidadpres']
                            
                            $idproducto =0;
                            //CREACION DE PRODUCTO NUEVO AL MOMENTO DE GUARDAR EL DETALLE DE LA IMPORTACION
                            if($datos['codigoproducto']  == ''){
                                $DatosProducto = array('codigo' => $datos['codigo'], 'item' => ($datos['idgrupo'])
                                      ,'proveedor' => $datos['proveedor'],'descripcion' => $datos['nombre']
                                      ,'ctamercaderia' => $datos['ctamercaderia'],'ctaventas' => $datos['ctaventas']
                                      ,'ctacostos' => $datos['ctacostos'],'ctadevolucion' => $datos['ctadevolucion']
                                      ,'ivacompras' => $datos['ivacompras'],'ivaventas' => $datos['ivaventas']
                                      ,'iceventas' => 0,'serventas' => 0
                                      ,'idunidad' => $datos['idunidad'],'factcantidad' => 0                                        
                                      ,'factcosto' => 0,'factprecio' => 0,'stockmin' => 1,'stockmax' => 10
                                      ,'dsctomax' => 0,'pagacoms' => 0,'estado' => 1);
                                $datos['codigoproducto'] = $this->IngresaProducto($DatosProducto);
                                $datos['codunidadprod'] = $datos['idunidad'];
                            }
                            
                            $datosIngresa = array('pedido' => $idPedido, 'codigo' => strtoupper($datos['codigo'])
                                                  ,'unidadpresentacion' => $datos['codunidadpres'],'producto' => ($datos['codigoproducto'])
                                                  ,'cantidadpresentacion' => $datos['cajas'],'unidadimportacion' => $datos['codunidadprod']
                                                  ,'cantidadimportacion' => $datos['cantidad'],'precio' => $datos['precio'],'total' => $datos['total']);
                            //evaluo si el producto a ingresar ya consta en un pedido.
                            $datproducto = $this->BuscarProductoenPedido($datos['codigoproducto'],$idPedido); 

                            if(count($datproducto) >0 ){
                                $cantimp =  $datos['cantidad'];
                                $cantpre =  $datos['cajas'];
                                $totimp =  $datos['total'];
                                $idimp = $datproducto[0]['id'];
                                $datosActualiza = array('id' => $idimp,'cantidad' => $cantimp,'presentacion' => $cantpre,'total' => $totimp);
                                $Rsid =$this->ActualizarDetalle($datosActualiza);
                            }else{
                                $Rsid =$this->InsertarDetalle($datosIngresa);
                            }
                            
                            if (is_numeric($Rsid)){
                                $idR = $Rsid;
                            }else{ $idR='Error al Insertar los Datos...'; break; }
                        }
                        
                        if (is_numeric($idR)){
                                $this->EliminaDetalleTemporal($_SESSION['IDUSER']);
                                return intval($idR);
                        }else{
                                return $idR;
                        }
              }
              
              public function InsertaDetallePedido($Form,$idPedido,$tipoaccion=0) {
                            $codigo=0;                      $producto=0;    
                            $unidadpresentacion=0;          $cantidadpresentacion=0;
                            $unidadimportacion=0;           $cantidadimportacion=0;
                            $precio=0;      $total=0;
                            $idR=0;
                            foreach ($Form as $datos=>$informacion){
                                switch (substr($datos, 0,6)){ 
                                    case 'dettot':    
                                            $total = $informacion;
                                            if($tipoaccion == 0){
                                              $datosIngresa = array('pedido' => $idPedido, 'codigo' => strtoupper($codigo)
                                                  ,'unidadpresentacion' => $unidadpresentacion,'producto' => ($producto)
                                                  ,'cantidadpresentacion' => $cantidadpresentacion,'unidadimportacion' => $unidadimportacion
                                                  ,'cantidadimportacion' => $cantidadimportacion,'precio' => $precio,'total' => $total);
                                                $Rsid =$this->InsertarDetalle($datosIngresa);
                                            }                                        
                                            if (is_numeric($Rsid)){
                                                $idR = $Rsid;
                                            }else{ $idR='Error al Insertar los Datos...';  break; }
                                        break;
                                    case 'detpro':                                        
                                            $producto = $informacion;                                            
                                        break;
                                     case 'detcod':                                        
                                            $codigo = $informacion;                                            
                                        break;
                                    case 'detpun':                                        
                                            $unidadpresentacion = $informacion;                                            
                                        break;
                                    case 'detpca':                                        
                                            $cantidadpresentacion = $informacion;                                            
                                        break;
                                    case 'detiun':                                        
                                            $unidadimportacion = $informacion;                                            
                                        break;
                                    case 'detica':                                        
                                            $cantidadimportacion = $informacion;                                            
                                        break;
                                    case 'detpre':                                        
                                            $precio = $informacion;                                            
                                        break;
                                }
                            } 
                            if (is_numeric($idR)){
                                return intval($idR);
                            }else{
                                return $idR;
                            }
              }
              
              public function InsertarDetalle($datos)
              {      try{ 
                        $Query = "INSERT INTO importacion.importacion_detalle (id_pedido,id_producto,codigo,uni_presentacion,cant_presentacion"
                            . ",uni_importacion,cant_importacion,precio,total) "
                            . "VALUES (".$datos['pedido'].",".$datos['producto'].",'".$datos['codigo']."',".$datos['unidadpresentacion'].""
                            . ",".$datos['cantidadpresentacion']." ,".$datos['unidadimportacion'].",".$datos['cantidadimportacion'].""
                            . ",".$datos['precio'].",".$datos['total'].") returning id ";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Pedido..';
                    }     
              }
              
              public function InsertarDetalleTemporal($datos)
              {      try{  
                        $Query = "INSERT INTO temporal.tmp_pedido (codigo,nombre,proveedor,linea,grupo,p_iva,marca,comision,categoria"
                            . ",cantidad,cajas,precio,unidad,unidadpresenta,idusuario,origen) "
                            . "VALUES ('".$datos['codigo']."','".$datos['nombre']."','".$datos['prove']."','".$datos['linea']."','".$datos['grupo']."'"
                            . ",".$datos['p_iva'].",'".$datos['marca']."',".$datos['comis'].",'".$datos['categ']."',".$datos['canti'].""
                            . ",".$datos['cajas'].",'".$datos['precio']."','".$datos['unidad']."','".$datos['presenta']."'"
                            . ",".$datos['usuario'].",'".$datos['origen']."') returning id";
                        $Rsid = $this->Funciones->Query($Query); 
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Pedido..';
                    }        
              }
              
              public function IngresaProducto($datos)
              {      try{ 
                        $fecha = $this->Fecha->getNowDateTime('DATETIME');      
                        $Query = "INSERT INTO inventario.producto (codigo,iditem,idproveedor,descripcion,ctamercaderia,ctaventas,ctacostos"
                            . ",ctadevolucion,ivacompras,ivaventas,iceventas,serventas,idunidad,factcantidad,factcosto,factprecio,stockmin"
                            . ",stockmax,dsctomax,pagacoms,idestado,iduscreacion,fecreacion) "
                            . "VALUES ('".$datos['codigo']."',".$datos['item'].",".$datos['proveedor'].",'".$datos['descripcion']."'"
                            . ",".$datos['ctamercaderia']." ,".$datos['ctaventas'].",".$datos['ctacostos'].",".$datos['ctadevolucion'].""
                            . ",".$datos['ivacompras'].",".$datos['ivaventas'].",".$datos['iceventas'].",".$datos['serventas'].""
                            . ",".$datos['idunidad'].",".$datos['factcantidad'].",".$datos['factcosto'].",".$datos['factprecio'].""
                            . ",'".$datos['stockmin']."','".$datos['stockmax']."','".$datos['dsctomax']."','".$datos['pagacoms']."'"
                            . ",".$datos['estado'].",".$_SESSION['IDUSER'].",'".$fecha."') returning id "; 
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Producto..';
                    }     
              }
              
              public function ActualizarDetalle($datos)
              {      try{
                        $Query = "UPDATE importacion.importacion_detalle "
                               . "SET cant_importacion = ".$datos['cantidad'].",cant_presentacion = ".$datos['presentacion'].", total = ".$datos['total'].""
                               . "WHERE id= ".$datos['id'];
                        $this->Funciones->Query($Query);
                        return intval($datos->id); 
                   } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function EliminaDetalle($pedido)
              {   try{     
                       $Query = "DELETE FROM importacion.importacion_detalle WHERE id_pedido= ".$pedido;
                       return $this->Funciones->Query($Query);
                     } catch (Exception $ex) {
                       $this->manejadorExcepciones($ex);
                       return 'No se pudo eliminar el detalle..';
                     }  
              }
              
              public function ActualizaCampoTemporalconDatosIngresados($datos)
              {      try{
                        $Query = "UPDATE temporal.tmp_pedido "
                               . "SET cantidad = ".$datos['cantimpor'].",cajas = ".$datos['cantpresenta'].", precio = ".$datos['precio'].""
                                . ", unidad = '".$datos['uniimpor']."', unidadpresenta = '".$datos['unipresenta']."'"
                               . "WHERE idusuario= ".$datos['idusuario']." and codigo='".$datos['codigo']."'";
                        $this->Funciones->Query($Query);
                        return intval($datos['idusuario']); 
                   } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
              
              public function SumaCampoTemporalconDatosIngresados($datos)
              {      try{
                        $Query = "UPDATE temporal.tmp_pedido "
                               . "SET cantidad = cantidad + ".$datos['cantimpor'].",cajas = cajas + ".$datos['cantpresenta'].", precio = ".$datos['precio'].""
                                . ", unidad = '".$datos['uniimpor']."', unidadpresenta = '".$datos['unipresenta']."'"
                               . "WHERE idusuario= ".$datos['idusuario']." and codigo='".$datos['codigo']."'";
                        $this->Funciones->Query($Query);
                        return intval($datos['idusuario']); 
                   } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }              
              
              public function EliminaDetalleTemporal($usuario,$origen='')
              {      try{
                            if($origen != ''){
                                $Query = "DELETE FROM temporal.tmp_pedido WHERE idusuario = '".$usuario."' and origen='".$origen."'";
                            }else{
                                $Query = "DELETE FROM temporal.tmp_pedido WHERE idusuario = '".$usuario."'";
                            }                          
                          return $this->Funciones->Query($Query);
                        } catch (Exception $ex) {
                          $this->manejadorExcepciones($ex);
                          return 'No se pudo eliminar el detalle..';
                        } 
              }
              
              public function EliminaDatoenTablaTemporal($usuario,$codigo)
              {      try{
                            $Query = "DELETE FROM temporal.tmp_pedido WHERE idusuario = '".$usuario."' and codigo ='".$codigo."'";                                                      
                          return $this->Funciones->Query($Query);
                        } catch (Exception $ex) {
                          $this->manejadorExcepciones($ex);
                          return 'No se pudo eliminar el detalle..';
                        } 
              }
    
              public function ActualizaPedido($datos)
              {      try{
                        $id = $datos->id_Pedido;
                        $estado = $datos->estado_Pedido;
                        $proveedor = $datos->idproveedor_Pedido;
                        $documento = strtoupper($datos->documento_Pedido);
                        $comentario = strtoupper($datos->comentario_Pedido);
                        $fecha = $datos->fecha_Pedido;
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE importacion.importacion "
                                . "SET idproveedor = ".$proveedor.", fecha= to_timestamp('".$fecha."', 'YYYY-MM-DD')"
                                . ", documento= '".$documento."', comentario= '".$comentario."', idestado= ".$estado.""
                                . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                                . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        if (is_numeric($id)){
                            $datosTemporales = $this->TraeDatosPedidoTemporal($_SESSION['IDUSER']);
                              if(count($datosTemporales) >0 ){
                                    $idDet = $this->SeteaDatosDetalleExcel($datosTemporales,$id);
                              }else{
                                    $this->EliminaDetalle($id);
                                    $idDet = $this->InsertaDetallePedido($datos,$id);
                              }

                                if(is_numeric($idDet)){                                    
                                    return intval($id);
                                }else{
                                    return $idDet;
                                }
                        }else{
                                return $id;
                        }
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Actualizar los Datos..');
                    }
              }
    
              public function DesactivaPedido($id)
              {   try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE importacion.importacion SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                     } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Pedido..';
                     }
              }
              
              public function ActualizaOpcionTablaTemporal($usuario,$id,$opcion)
              {   try{  
                        if($opcion == 0){ $ID = " and id =".$id; }else{ $ID = ""; }            
                        $Query = "UPDATE temporal.tmp_pedido SET opcion= ".$opcion." WHERE idusuario= ".$usuario.$ID;
                        $this->Funciones->Query($Query);
                        return intval($id);
                     } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Actualizar la Tabla..';
                     }
              }
        }
