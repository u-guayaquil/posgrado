<?php
            require_once("src/rules/maestria/dao/DAOReporte.php");
            
            class ServicioReporte 
            {       private $DAOReporte;
                    function __construct()
                    {        $this->DAOReporte = new DAOReporte();
                    }

                    function DatosCabeceraReporte($Params)
                    {        return $this->DAOReporte->ObtenerDatosCabeceraActas($Params);
                    }

                    function DatosCabeceraReporteEstudiante($Params)
                    {        return $this->DAOReporte->ObtenerDatosCabeceraEstudiante($Params);
                    }
            }        
?>        
