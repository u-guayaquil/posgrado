<?php
            require_once("src/rules/maestria/dao/DAOCiclo.php");
            
            class ServicioCiclo 
            {       private $DAOCiclo;
     
                    function __construct()
                    {        $this->DAOCiclo = new DAOCiclo();
                    }
           
                    function BuscarCicloByMaestriaVersion($idMaver)
                    {       return $this->DAOCiclo->ObtenerCicloByMaestriaVersion($idMaver);
                    }
            
                    function GuardaDBCiclo($Ciclo)
                    {       if (empty($Ciclo->id)){
                                return $this->CreaCiclo($Ciclo);
                            }else{
                                return $this->EditaCiclo($Ciclo);
                            }
                    }
                    
                    function CreaCiclo($Form)
                    {       return $this->DAOCiclo->InsertaCiclo($Form);
                    }
           
                    function EditaCiclo($Form)
                    {       return $this->DAOCiclo->ActualizaCiclo($Form);
                    }
                    
                    function DesactivaCiclo($id)
                    {       return $this->DAOCiclo->DesactivaCiclo(intval($id));
                    }
                    
                    function BuscarCicloByID($id)
                    {       return $this->DAOCiclo->ObtenerCicloByID(intval($id));
                    }

                    function BuscarCicloByDescripcion($prepareDQL)
                    {       return $this->DAOCiclo->ObtenerCicloByDescripcion($prepareDQL);
                    }
                    
                    function BuscarCicloEstadoByID($id)
                    {       return $this->DAOCiclo->ObtenerCicloEstadoByID(intval($id));
                    }
                    
                    function SeleccionarTodos()
                    {       return $this->DAOCiclo->ObtenerCiclos();
                    }
                    
                    function BuscarCohorteByDescripcion($prepareDQL)
                    {       return $this->DAOCiclo->ObtenerCohorteByDescripcion($prepareDQL);
                    }
                    
                    function BuscarCicloEstadoByCohorte($idCohorte)
                    {       return $this->DAOCiclo->ObtenerCicloEstadoByCohorte(intval($idCohorte));
                    }
                    
                    function BuscarCicloByDescripcionByCohorte($prepareDQL)
                    {       return $this->DAOCiclo->ObtenerCicloByDescripcionByCohorte($prepareDQL);
                    }

                    function BuscarCicloByIDByCohorte($prepareDQL)
                    {       return $this->DAOCiclo->ObtenerCicloByIDByCohorte($prepareDQL);
                    }
                    
                    function BuscarFechaFinUltimoCiclo($idCohorte)
                    {       return $this->DAOCiclo->ObtenerFechaFinUltimoCiclo($idCohorte);
                    }
                    
                    function BuscarRangoCicloByCohorte($idCohorte)
                    {       return $this->DAOCiclo->ObtenerRangoCicloByCohorte($idCohorte);
                    }
            }        
?>        
