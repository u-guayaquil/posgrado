<?php       require_once("src/rules/maestria/dao/DAOAsistencia.php");
            require_once("src/rules/maestria/dao/DAOHorario.php");
            
            class ServicioAsistencia  
            {       private $DAOAsistencia;
                    private $DAOHorario;
                    function __construct()
                    {        $this->DAOAsistencia = new DAOAsistencia();
                             $this->DAOHorario = new DAOHorario();
                    }
                    
                    function ConsultaAsistencia($Params)
                    {       $HorarioByHoras = $this->DAOHorario->ObtenerHorarioByHoras($Params[6],$Params[7]);
                            if (!$HorarioByHoras[0])
                            {   $HorarioByHoras = $this->DAOHorario->CrearHorarioByHoras($Params[6],$Params[7]);
                                if (!$HorarioByHoras[0])
                                return $HorarioByHoras;     
                            }       
                            return $this->DAOAsistencia->ObtenerAsistencia($Params,$HorarioByHoras);   
                    }
                    
                    function GuardaDB($Form)
                    {        return $this->DAOAsistencia->RegistraAsistencia($Form);
                    }
                    
                    function BuscarPermisoAsistencia($Params,$Compte)
                    {       return $this->DAOAsistencia->ObtenerPermisoAsistencia($Params,$Compte);
                    }
                    
                    function EjecutaPermisoAsistencia($Params,$Compte)
                    {       if ($Params[7]!=0) //PERMISO ACTIVO
                                return $this->DAOAsistencia->EjecutaPermisoAsistencia($Params,$Compte);
                            else
                                return false;
                        
                    }
                    
                    function GuardaPermisoAsistencia($Params,$Compte)
                    {       return $this->DAOAsistencia->RegistraPermisoAsistencia($Params,$Compte);
                    }
                    
                    function TotalAsistenciaEjecutada($Compte,$Params)
                    {       return $this->DAOAsistencia->ObtenerTotalAsistenciaEjecutada($Compte,$Params);
                    }
            }
?>

