<?php       require_once("src/rules/maestria/dao/DAOCiclosMateria.php");
            require_once("src/rules/maestria/dao/DAOHorario.php");
            
            class ServicioCiclosMateria
            {       private $DAOCiclosMateria;
                    private $DAOHorario;
                    
                    function __construct()
                    {        $this->DAOCiclosMateria = new DAOCiclosMateria();
                             $this->DAOHorario = new DAOHorario();
                    }
                    
                    function BuscarMaterias($Params)
                    {       return $this->DAOCiclosMateria->ObtenerMaterias($Params);   
                    }
                    
                    function GuardaHistorico($Datos,$Texto)
                    {       return $this->DAOCiclosMateria->GuardaHistorico($Datos,$Texto);
                    }
                    function GuardaNuevoRegistro($Params,$Datos)
                    {       return $this->DAOCiclosMateria->GuardaNuevoRegistro($Params,$Datos);
                    }
                    
                    function GuardarDB($Params,$Datos)
                    {       return $this->DAOCiclosMateria->GuardarDB($Params,$Datos);
                    }
                    
                    function ActualizarDB($Params,$Datos,$IdCicloMateria)
                    {       return $this->DAOCiclosMateria->ActualizarDB($Params,$Datos,$IdCicloMateria);
                    }

                    function CambiarEstado($IdCicloMateria)
                    {       return $this->DAOCiclosMateria->CambiarEstado($IdCicloMateria);   
                    }

                    function CambiarEstadoTemporal($IdTemporalCM)
                    {       return $this->DAOCiclosMateria->CambiarEstadoTemporal($IdTemporalCM);   
                    }
                    
                    function ConsultarExisteGrupoMateria($Datos)
                    {       return $this->DAOCiclosMateria->ObtenerExisteGrupoMateria($Datos);
                    }
                    
                    function ConsultarExisteCicloMateria($Datos,$IdCicloMateria)
                    {       return $this->DAOCiclosMateria->ObtenerExisteCicloMateria($Datos,$IdCicloMateria);
                    }
                    
                    function BuscarMaestria()
                    {       return $this->DAOCiclosMateria->ObtenerMaestria();
                    }
                    
                    function BuscarCohorteByMaestria($Params)
                    {       return $this->DAOCiclosMateria->ObtenerCohorteByMaestria($Params);
                    }
                    
                    function BuscarCicloByCohorte($Params)
                    {       return $this->DAOCiclosMateria->ObtenerCicloByCohorte($Params);
                    }
                                       
                    function BuscarMaestriaByTx($Params)
                    {       return $this->DAOCiclosMateria->ObtenerMaestriaByTx($Params);
                    }
                    
                    function BuscarCohorteByMaestriaByTx($Params)
                    {       return $this->DAOCiclosMateria->ObtenerCohorteByMaestriaByTx($Params);
                    }
                    
                    function BuscarCicloByCohorteByTx($Params)
                    {       return $this->DAOCiclosMateria->ObtenerCicloByCohorteByTx($Params);
                    }
                    
                    function BuscarDocenteByFacultad($Params)
                    {       return $this->DAOCiclosMateria->ObtenerDocenteByFacultad($Params);
                    }
                    
                    function BuscarParaleloByCohorte($Params)
                    {       return $this->DAOCiclosMateria->ObtenerParaleloByCohorte($Params);
                    }
            }
?>

