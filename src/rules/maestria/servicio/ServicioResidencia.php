<?php
require_once("src/rules/maestria/dao/DAOResidencia.php");
class ServicioResidencia
{	private $DAOResidencia;

	function __construct()
	{        $this->DAOResidencia = new DAOResidencia();
	}
	
	function GuardaDB($Residencia)
	{   if (empty($Residencia->id)){
			return $this->CreaResidencia($Residencia);
		}else{
			return $this->EditaResidencia($Residencia);
		}
	}
	
	function CreaResidencia($Form)
	{       return $this->DAOResidencia->InsertaResidencia($Form);
	}

	function EditaResidencia($Form)
	{       return $this->DAOResidencia->ActualizaResidencia($Form);
	}
	
	function DesactivaResidencia($id)
	{       return $this->DAOResidencia->DesactivaResidencia(intval($id));
	}

	function BuscarMaestriaByDescripcion($prepareDQL)
	{       return $this->DAOResidencia->ObtenerMaestriaByDescripcion($prepareDQL);
	}
	
	function BuscarCohorteByDescripcion($prepareDQL)
	{       return $this->DAOResidencia->ObtenerCohorteByDescripcion($prepareDQL);
	}

	function BuscarHospitalByDescripcion($prepareDQL)
	{       return $this->DAOResidencia->ObtenerHospitalByDescripcion($prepareDQL);
	}

	function BuscarResidenciaByCohorte($idCohorte)
	{       return $this->DAOResidencia->ObtenerResidenciaByCohorte(intval($idCohorte));
	}
	
	function BuscarResidenciaByID($prepareDQL)
	{       return $this->DAOResidencia->ObtenerResidenciaByID($prepareDQL);
	}

	function BuscarResidenciaByDescripcion($prepareDQL)
	{       return $this->DAOResidencia->ObtenerResidenciaByDescripcion($prepareDQL);
	}
	
	function BuscarResidenciaByIDProcesado($id)
	{       return $this->DAOResidencia->ObtenerResidenciaByIDProcesado(intval($id));
	}
}
?>

