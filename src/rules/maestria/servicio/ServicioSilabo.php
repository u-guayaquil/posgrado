<?php
            require_once("src/rules/maestria/dao/DAOSilabo.php");
            
            class ServicioSilabo 
            {       private $DAOSilabo;
     
                    function __construct()
                    {        $this->DAOSilabo = new DAOSilabo();
                    }
           
                    function GuardaDBSilabo($Accion,$JsDatos)
                    {       if ($Accion==1){
                                return $this->CreaSilabo($JsDatos);
                            }    
                            else{
                                return $this->EditaSilabo($JsDatos);
                            }
                    }
                    
                    private function PreparaDatos($Form)
                    {      $Form->requisito=strtoupper($Form->requisito);
                           $Form->orgcurricular=strtoupper($Form->requisito);
                           $Form->obgeneral=strtoupper(utf8_decode($Form->obgeneral));
                           $Form->obespecifico=strtoupper(utf8_decode($Form->obespecifico));
                           $Form->metodologia=strtoupper(utf8_decode($Form->metodologia));
                           $Form->linea=strtoupper(utf8_decode($Form->linea));
                           $Form->modulo=strtoupper(utf8_decode($Form->modulo));
                           $Form->resultado=strtoupper(utf8_decode($Form->resultado));
                           $Form->ctematico=strtoupper(utf8_decode($Form->ctematico));
                           $Form->cmetodos=strtoupper(utf8_decode($Form->cmetodos));
                           $Form->obaprende=strtoupper(utf8_decode($Form->obaprende));
                           $Form->evaprende=strtoupper(utf8_decode($Form->evaprende));
                           
                           $Form->Basname1=utf8_decode($Form->Basname1);
                           $Form->Basexis1=utf8_decode($Form->Basexis1);
                           $Form->Basname2=utf8_decode($Form->Basname2);
                           $Form->Basexis2=utf8_decode($Form->Basexis2);
                           $Form->Basname3=utf8_decode($Form->Basname3);
                           $Form->Basexis3=utf8_decode($Form->Basexis3);
                           
                           $Form->Comname1=utf8_decode($Form->Comname1);
                           $Form->Comexis1=utf8_decode($Form->Comexis1);
                           $Form->Comname2=utf8_decode($Form->Comname2);
                           $Form->Comexis2=utf8_decode($Form->Comexis2);
                           $Form->Comname3=utf8_decode($Form->Comname3);
                           $Form->Comexis3=utf8_decode($Form->Comexis3);

                           return $Form;
                    }
                    
                    function CreaSilabo($Form)
                    {       $Form=$this->PreparaDatos($Form);
                            return $this->DAOSilabo->InsertaSilabo($Form);
                    }
           
                    function EditaSilabo($Form)
                    {       $Form=$this->PreparaDatos($Form);
                            return $this->DAOSilabo->ActualizaSilabo($Form);
                    }
                    
                    function DesactivaSilabo($id)
                    {       return $this->DAOSilabo->DesactivaSilabo(intval($id)); 
                    }
                    
                    function BuscarSilaboByID($idMtr,$idMat)
                    {       return $this->DAOSilabo->ObtenerSilaboByID($idMtr,$idMat);
                    }

                    function BuscarSilaboBibliografiaByID($idMtr,$idMat)
                    {       return $this->DAOSilabo->ObtenerSilaboBibliografiaByID($idMtr,$idMat);
                    }        
                    
                    function BuscarSilaboByMaestria($id)
                    {       return $this->DAOSilabo->ObtenerSilaboByMaestria(intval($id));
                    }
                    
                    function BuscarSilaboByDescripcion($Texto)
                    {       return $this->DAOSilabo->ObtenerSilaboByDescripcion($Texto);
                    }
                    
                    function BuscarSilaboEstadoByID($id)
                    {       return $this->DAOSilabo->ObtenerSilaboEstadoByID(intval($id));
                    }
}        
?>        
