<?php
    require_once("src/rules/maestria/dao/DAOAprobacionDocente.php");

    class ServicioAprobacionDocente
    {       private $DAOAprobacionDocente; 

            function __construct()
            {   $this->DAOAprobacionDocente = new DAOAprobacionDocente();    
            }
           
            public  function BuscarAprobacionDocenteSinContrato()
            {   return $this->DAOAprobacionDocente->ObtenerDocentesSinContratos();
            }
            
            function EditaAprobacionDocente($Forma)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $AprobDocente = $this->DAOAprobacionDocente->ObtenerDocentesSinContratosById($Forma->Id);  
                    
                    $IdEstado   =   ($Forma->IdBoton==5 ? $Forma->Value : $AprobDocente->IDESTADO);     
                    
                    $Editar['id']= $Forma->Id;     
                    $Editar['observacion']= strtoupper($Forma->observacion);
                    $Editar['usmodifica']= $Modulo->Idusrol;
                    $Editar['estado']= $IdEstado;
                    
                    return $this->DAOAprobacionDocente->ActualizaDocenteSinContrato($Editar);
            }
     }
?>
