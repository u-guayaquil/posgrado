<?php
            require_once("src/rules/maestria/dao/DAOVMaestria.php");
            class ServicioVMaestria
            {       private $DAOVMaestria;
     
                    function __construct()
                    {        $this->DAOVMaestria = new DAOVMaestria();
                    }
                    
                    function GuardaDBVMaestriaSinVersion($VMaestria)
                    {       if (empty($VMaestria->id)){
                                return $this->CreaVMaestriaSinVersion($VMaestria);
                            }else{
                                return $this->EditaVMaestria($VMaestria);
                            }
                    }
                    
                    function GuardaDBVMaestriaConVersion($VMaestria)
                    {       if (empty($VMaestria->id)){
                                return $this->CreaVMaestriaConVersion($VMaestria);
                            }else{
                                return $this->EditaVMaestria($VMaestria);
                            }
                    }
                    
                    function CreaVMaestriaSinVersion($Form)
                    {       return $this->DAOVMaestria->InsertaVMaestriaSinVersion($Form);
                    }
                    
                    function CreaVMaestriaConVersion($Form)
                    {       return $this->DAOVMaestria->InsertaVMaestriaConVersion($Form);
                    }
           
                    function EditaVMaestria($Form)
                    {       return $this->DAOVMaestria->ActualizaVMaestria($Form);
                    }
                    
                    function DesactivaVMaestria($id)
                    {       return $this->DAOVMaestria->DesactivaVMaestria(intval($id));
                    }
                    
                    function BuscarVMaestriaByID($id)
                    {       return $this->DAOVMaestria->ObtenerVMaestriaByID(intval($id));
                    }

                    function BuscarVMaestriaByDescripcion($prepareDQL)
                    {       return $this->DAOVMaestria->ObtenerMaestriaByDescripcion($prepareDQL);
                    }
                    
                    function BuscarVMaestriaEstadoByID($id)
                    {       return $this->DAOVMaestria->ObtenerVMaestriaEstadoByID(intval($id));
                    }
                    
                    function SeleccionarTodos()
                    {       return $this->DAOVMaestria->ObtenerVMaestrias();
                    }
                    
                    function BuscarMaestriaByDescripcion($prepareDQL)
                    {       return $this->DAOVMaestria->ObtenerMaestriaByDescripcion($prepareDQL);
                    }
                    
                    function BuscarVMaestriaEstadoByMaestria($id)
                    {       return $this->DAOVMaestria->ObtenerVMaestriaEstadoByMaestria(intval($id));
                    }
                    
                    function BuscarVMaestriaByDescripcionByMaestria($prepareDQL)
                    {       return $this->DAOVMaestria->ObtenerVMaestriaByDescripcionByMateria($prepareDQL);
                    }

                    function BuscarVMaestriaByIDByMaestria($prepareDQL)
                    {       return $this->DAOVMaestria->ObtenerVMaestriaByIDByMaestria($prepareDQL);
                    }
                    
                    function ValidarVMaestriaByIdMaestria($prepareDQL)
                    {       return $this->DAOVMaestria->ValidarVMaestriaByIdMaestria($prepareDQL);
                    }
                    
                    function DesactivaVMaestriaVersionAnterior($idmaestria)
                    {
                            return $this->DAOVMaestria->DesactivaVMaestriaVersionAnterior($idmaestria);
                    }
            }
?>

