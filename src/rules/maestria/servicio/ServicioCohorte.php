<?php
            require_once("src/rules/maestria/dao/DAOCohorte.php");
            
            class ServicioCohorte 
            {       private $DAOCohorte;
     
                    function __construct()
                    {        $this->DAOCohorte = new DAOCohorte();
                    }
           
                    function BuscarCohorteCicloByMaestriaVersion($idMaver)
                    {       return $this->DAOCohorte->ObtenerCohorteCicloByMaestriaVersion($idMaver);
                    }

                    function GuardaDBCohorte($Cohorte)
                    {       if (empty($Cohorte->id)){
                                return $this->CreaCohorte($Cohorte);
                            }else{
                                return $this->EditaCohorte($Cohorte);
                            }
                    }
                    
                    function CreaCohorte($Form)
                    {       return $this->DAOCohorte->InsertaCohorte($Form);
                    }
           
                    function EditaCohorte($Form)
                    {       return $this->DAOCohorte->ActualizaCohorte($Form);
                    }
                    
                    function DesactivaCohorte($id)
                    {       return $this->DAOCohorte->DesactivaCohorte(intval($id));
                    }
                    
                    function BuscarCohorteByID($id)
                    {       return $this->DAOCohorte->ObtenerCohorteByID(intval($id));
                    }

                    function BuscarCohorteByDescripcion($prepareDQL)
                    {       return $this->DAOCohorte->ObtenerCohorteByDescripcion($prepareDQL);
                    }
                    
                    function BuscarCohorteEstadoByID($id)
                    {       return $this->DAOCohorte->ObtenerCohorteEstadoByID(intval($id));
                    }
                    
                    function SeleccionarTodos()
                    {       return $this->DAOCohorte->ObtenerCohortes();
                    }
                    
                    function BuscarCohortesByIdVersion($idversion)
                    {       return $this->DAOCohorte->ObtenerCohortesByIdVersion($idversion);
                    }
                    function BuscarCohortesDeVersionByIdVersion($idversion)
                    {       return $this->DAOCohorte->ObtenerCohortesDeVersionByIdVersion($idversion);
                    }
                    
                    function BuscarVersionByDescripcion($prepareDQL)
                    {       return $this->DAOCohorte->ObtenerVersionByDescripcion($prepareDQL);
                    }
                    
                    function BuscarCohorteEstadoByVersion($id)
                    {       return $this->DAOCohorte->ObtenerCohorteEstadoByVersion(intval($id));
                    }
                    
                    function BuscarCohorteByDescripcionByVersion($prepareDQL)
                    {       return $this->DAOCohorte->ObtenerCohorteByDescripcionByVersion($prepareDQL);
                    }

                    function BuscarCohorteByIDByVersion($prepareDQL)
                    {       return $this->DAOCohorte->ObtenerCohorteByIDByVersion($prepareDQL);
                    }
                    
                    function BuscarMallaByDescripcion($prepareDQL)
                    {       return $this->DAOCohorte->ObtenerMallaByDescripcion($prepareDQL);
                    }
                    
                    function BuscarMallaByVersionByDescripcion($Params)
                    {       return $this->DAOCohorte->ObtenerMallaByVersionByDescripcion($Params);
                    }
                    
                    function BuscarFechaFinUltimoCohorte($idVersion)
                    {       return $this->DAOCohorte->ObtenerFechaFinUltimoCohorte($idVersion);
                    }
            }        
?>        
