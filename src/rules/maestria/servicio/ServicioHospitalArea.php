<?php
require_once("src/rules/maestria/dao/DAOHospitalArea.php");

class ServicioHospitalArea 
{       private $DAOArea;

    function __construct()
    {        $this->DAOArea = new DAOHospitalArea();
    }

    function GuardaDBArea($Area)
    {       if (empty($Area->id)){
                return $this->CreaArea($Area);
            }else{
                return $this->EditaArea($Area);
            }
    }
    
    function CreaArea($Form)
    {   return $this->DAOArea->InsertaArea($Form);
    }

    function EditaArea($Form)
    {   return $this->DAOArea->ActualizaArea($Form);
    }
    
    function DesactivaArea($id)
    {       return $this->DAOArea->DesactivaArea(intval($id));
    }
    
    function BuscarAreaByID($id)
    {       return $this->DAOArea->ObtenerAreaByID(intval($id));
    }

    function BuscarAreaByIDGuardado($id)
    {       return $this->DAOArea->ObtenerAreaByIDGuardado(intval($id));
    }

    function BuscarAreaByDescripcion($prepareDQL)
    {       return $this->DAOArea->ObtenerAreaByDescripcion($prepareDQL);
    }
}        
?>        
