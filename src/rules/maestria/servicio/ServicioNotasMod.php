<?php       require_once("src/rules/maestria/servicio/ServicioAsistencia.php");
            require_once("src/rules/maestria/servicio/ServicioMatInformacion.php");
            require_once("src/rules/maestria/dao/DAONotasMod.php");
            
            class ServicioNotasMod  
            {       private $DAONotasMod;
                    private $DAOPoliticaEvaluacion;
                    
                    function __construct()
                    {       $this->DAONotasMod = new DAONotasMod();
                            $this->DAOPoliticaEvaluacion = new DAOPoliticaEvaluacion();
                    }
                    
                    function ConsultaCierreActa($Params)
                    {       return $this->DAONotasMod->ObtCierreActaCalificacion($Params);
                    }
                    
                    function ConsultarNotas($Params)
                    {       $ServicioMatInforma = new ServicioMatInformacion();
                            $ValorComponente = $ServicioMatInforma->BuscarValorComponente('CD', array($Params[2],$Params[3]));
                            if ($ValorComponente[0])
                            {   $Compte = $ValorComponente[1][0]['COMPTE'];
                                if ($Compte>0)
                                {   $ColsPolitica = $this->DAOPoliticaEvaluacion->ObtenerColumnsPoliticaEvaluacion($Params[5],$Params[6],$Params[7],$Params[3]);
                                    if ($ColsPolitica[0])
                                    {   $NotasMod = $this->DAONotasMod->ObtenerNotasMod($Params,$Compte);
                                        if ($NotasMod[0])
                                        {   return array(true,$ColsPolitica[1],$NotasMod[1]);
                                        }   
                                        return $NotasMod;
                                    }       
                                    return array(false,"No existe estructura de calificación definida para la maestria y materia.");
                                }    
                                return array(false,"El valor del componente CD no puede ser cero.");
                            }
                            return $ValorComponente;
                    }
                    
                    function CierreNotas($Params)
                    {       $ServicioAsistencia = new ServicioAsistencia();
                            $TotalAsistencia = $ServicioAsistencia->TotalAsistenciaEjecutada('CD',$Params);
                            if ($TotalAsistencia>0)
                            {   $ServicioMatInforma = new ServicioMatInformacion();
                                $ValorComponente = $ServicioMatInforma->BuscarValorComponente('CD', array($Params[2],$Params[3]));
                                if ($ValorComponente[0])
                                {   $Compte = $ValorComponente[1][0]['COMPTE'];
                                    if ($Compte==$TotalAsistencia)
                                    {   $ColsPolitica = $this->DAOPoliticaEvaluacion->ObtenerVariablesPoliticaEvaluacion(array($Params[5],$Params[6]));
                                        if ($ColsPolitica[0])
                                        {   $NotasEvalModulo = $this->DAONotasMod->ObtenerNotasMod($Params,$Compte);
                                            if ($NotasEvalModulo[0])
                                            {   $CierreEval = $this->ProcesoCierreNotas($ColsPolitica[1],$NotasEvalModulo[1]);
                                                if ($CierreEval[0])
                                                {   $this->DAONotasMod->RegCierreActaCalificacion($Params);
                                                    return array(true,$ColsPolitica[1],$CierreEval[1]);
                                                }
                                                return $CierreEval;
                                            }
                                            else
                                            return $NotasEvalModulo;    
                                        }
                                        return array(false,"No existe estructura de calificación definida para la maestria y materia.");
                                    }
                                    return array(false,"Total de horas del componente CD es diferente al total de asistencia.");
                                }    
                                return $ValorComponente;
                            }
                            return array(false,"No se ha ejecutado el total de asistencias programadas para la materia.");    
                    }
                        
                    private function ProcesoCierreNotas($Colum,$Notas)
                    {       $Num = 0;
                            $Reg = count($Notas);
                            $Cls = array();
                            foreach ($Notas as $Dato)
                            {       $Registro = array();
                                    if ($Dato['NOTAS']!="")
                                        $Cols = json_decode($Dato['NOTAS']);   
                                    else
                                        return array(false,"Falta definir las notas del estudiante: ".utf8_encode($Dato['ESTUDIANTE']));

                                    $Registro['ID'] = str_pad($Num,2,'0',STR_PAD_LEFT);
                                    $Registro['IDNOTAS'] = $Dato['IDNOTAS'];
                                    $Registro['CEDULA'] = $Dato['CEDULA'];
                                    $Registro['ESTUDIANTE'] = utf8_encode($Dato['ESTUDIANTE']);
                                    foreach ($Colum as $Col)
                                    {       $Var = $Col['VARIABLE'];
                                            if ($Col['TIPO']=="ASISTENCIA")
                                                $Registro[$Var] = number_format(($Dato['ASIST']/$Dato['TCD'])*100,0);
                                            else if ($Col['TIPO']=="OBSERVACION")
                                            {   $Registro[$Var] = "APROBADO";
                                                if (doubleval($Registro['FIN'])>=$Col['MINNOTA'])
                                                    $ValFin = $Registro['FIN'];
                                                else 
                                                    $ValFin = ($Registro['FIN']>$Registro['REC'] ? $Registro['FIN']:$Registro['REC']);
                                                
                                                if (doubleval($Registro['FIN'])<$Col['MINNOTA'] || doubleval($Registro['FASIS'])<$Col['MINASIS'])
                                                {   if (doubleval($Registro['FASIS'])>=$Col['MINASIS'] && doubleval($Registro['FIN'])<$Col['MINNOTA'])
                                                    $Registro[$Var] = (doubleval($Registro['REC'])<$Col['MINNOTA'] ? "REPROBADO":"APROBADO");
                                                    else
                                                    $Registro[$Var] = "REPROBADO";
                                                }
                                            }
                                            else
                                            $Registro[$Var] = $Cols->{$Var};       
                                    }
                                    //$Stat = $this->DAONotasMod->ActualizaNotasCierre($Dato['IDESTUDMATE'],$ValFin,$Registro['FASIS'],$Registro['EST']);
                                    //if (!$Stat) return array(false,"Error al registrar la nota del estudiante: ".utf8_encode($Dato['ESTUDIANTE']));
                                    
                                    $Num++;
                                    $Cls[] = $Registro;
                            }
                            return array(($Num==$Reg),($Num==$Reg ? $Cls : "No se pudo procesar el cierre de forma completa."));
                    }
                    
                    function CalculaNotasByFila($Param,$Notas)
                    {       $ServicioMatInforma = new ServicioMatInformacion();
                            $Numero = $ServicioMatInforma->ObtenerNumeroComponentes($Param[3],$Param[4]);
                        
                            $ColsVariables = $this->DAOPoliticaEvaluacion->ObtenerVariablesPoliticaEvaluacion($Param);
                            if ($ColsVariables[0])
                            {   $Compte = 0;
                              //$Numero = 0;
                                $Evalua = 0;
                                $Recupe = 0;
                                $Notafn = 0;
                                $Asistn = 0;
                                foreach ($ColsVariables[1] as $Col)
                                {       if ($Col['TIPO']=="COMPONENTE")
                                        {   $Vabase = doubleval($Notas->{$Param[2].$Col['VARIABLE']});
                                            $Compte = $Compte + $Vabase*($Col['PORCENTAJE']/100);
                                            $Notas->{$Param[2].$Col['VARIABLE']} = number_format($Vabase,$Col['DECIMALES']);
                                          //$Numero++;
                                        }
                                        else if ($Col['TIPO']=="CALCULO" && $Col['VARIABLE']=="TOT")
                                        {   $Compte = number_format($Compte/$Numero,$Col['DECIMALES']);
                                            $Notas->{$Param[2].$Col['VARIABLE']} = $Compte;
                                        }
                                        else if ($Col['TIPO']=="EVALUACION")
                                        {   $Evalua = number_format(doubleval($Notas->{$Param[2].$Col['VARIABLE']})*($Col['PORCENTAJE']/100),$Col['DECIMALES']);
                                            $Notas->{$Param[2].$Col['VARIABLE']} = number_format(doubleval($Notas->{$Param[2].$Col['VARIABLE']}),$Col['DECIMALES']);
                                        }
                                        else if ($Col['TIPO']=="RECUPERACION")
                                        {   $Notafn = number_format($Compte+$Evalua,$Col['DECIMALES']);
                                            if ($Notafn>=7)
                                            {   $Recupe = 0;
                                                $Notas->{$Param[2].$Col['VARIABLE']} = $Recupe;
                                            }
                                            else
                                            {   $Recupe = number_format(doubleval($Notas->{$Param[2].$Col['VARIABLE']}),$Col['DECIMALES']);       
                                                $Notas->{$Param[2].$Col['VARIABLE']} = $Recupe;
                                            }    
                                        }
                                        else if ($Col['TIPO']=="CALCULO" && $Col['VARIABLE']=="FIN")
                                        {   if ($Recupe==0)
                                            {   $Notafn = number_format($Compte+$Evalua,$Col['DECIMALES']);
                                                $Notas->{$Param[2].$Col['VARIABLE']} = $Notafn;
                                            }    
                                            else
                                            $Notas->{$Param[2].$Col['VARIABLE']} = $Recupe;
                                        }
                                        else if ($Col['TIPO']=="ASISTENCIA")
                                        {   $Asistn = doubleval($Notas->{$Param[2].$Col['VARIABLE']});
                                        }
                                        else if ($Col['TIPO']=="OBSERVACION")
                                        {   if (doubleval($Notafn)<$Col['MINNOTA'] || $Asistn<$Col['MINASIS'])
                                            {   if ($Asistn>=$Col['MINASIS'] && doubleval($Notafn)<$Col['MINNOTA'])
                                                    $Notas->{$Param[2].$Col['VARIABLE']}= (doubleval($Recupe<$Col['MINNOTA']) ? "REPROBADO":"APROBADO");
                                                else
                                                $Notas->{$Param[2].$Col['VARIABLE']}="REPROBADO";
                                            }
                                            else    
                                            $Notas->{$Param[2].$Col['VARIABLE']}="APROBADO";
                                        }
                                }
                                return array(true,$Notas);
                            }
                            return $ColsVariables;
                    }
                    
                    function GuardaDB($Form)
                    {       return $this->DAONotasMod->RegistraNotasMod($Form);
                    }
                    
                    function ActualizaNotaEstadoDB($idNotas,$JsonNotas)
                    {       return $this->DAONotasMod->ActualizaNotasEstadoMod($idNotas,$JsonNotas);
                    }

            }
?>

