<?php
            require_once("src/rules/maestria/dao/DAOMatInformacion.php");
            
            class ServicioMatInformacion 
            {       private $DAOMatInformacion;
                    private $Libros = 5;
                    
                    function __construct()
                    {        $this->DAOMatInformacion = new DAOMatInformacion();
                    }
           
                    function GuardaDB($Accion,$JsDatos) //ok
                    {       if ($JsDatos->nivcon>=$JsDatos->nivpln)
                            {   if ($Accion==1)
                                    return $this->Crea($JsDatos);
                                else
                                    return $this->Edita($JsDatos);
                            }
                            else
                            return array(false,"El nivel de contenido debe ser mayor o igual al de planificación.");    
                    }
                    
                    private function PreparaDatos($Form)
                    {      $Utils = new Utils();
                           $Form->requisito = utf8_decode($Utils->ConvertTildesToUpper($Form->requisito));
                           $Form->orgcurricular = utf8_decode($Utils->ConvertTildesToUpper($Form->orgcurricular));
                           $Form->obgeneral = utf8_decode($Utils->ConvertTildesToUpper($Form->obgeneral));
                           $Form->obespecifico = utf8_decode($Utils->ConvertTildesToUpper($Form->obespecifico));
                           $Form->metodologia = utf8_decode($Utils->ConvertTildesToUpper($Form->metodologia));
                           $Form->linea = utf8_decode($Utils->ConvertTildesToUpper($Form->linea));
                           
                           for ($u=1;$u<=$this->Libros;$u++)
                           {    $Basname = "Basname".$u;
                                $Comname = "Comname".$u;
                                $Lnkname = "Lnkname".$u;       
                                $Form->{$Basname}=utf8_decode($Utils->ConvertTildesToUpper($Form->{$Basname}));        
                                $Form->{$Comname}=utf8_decode($Utils->ConvertTildesToUpper($Form->{$Comname}));
                                $Form->{$Lnkname}=utf8_decode($Utils->ConvertTildesToUpper($Form->{$Lnkname}));
                           }
                           return $Form;
                    }
                    
                    function Crea($Form) //ok
                    {       $Form=$this->PreparaDatos($Form);
                            return $this->DAOMatInformacion->Inserta($Form,$this->Libros);
                    }
           
                    function Edita($Form) //ok
                    {       $Form=$this->PreparaDatos($Form); 
                            return $this->DAOMatInformacion->Actualiza($Form,$this->Libros);
                    }
                    
                    function Desactiva($idVer,$idMat) //ok
                    {       return $this->DAOMatInformacion->Desactiva($idVer,$idMat); 
                    }
                    
                    function BuscarObjetivosByMateriaVersion($idVer,$idMat) //ok
                    {       return $this->DAOMatInformacion->ObtenerObjetivosByMateriaVersion($idVer,$idMat);
                    }

                    function BuscarBibliografiaByMateriaVersion($idVer,$idMat) //ok
                    {       return $this->DAOMatInformacion->ObtenerBibliografiaByMateriaVersion($idVer,$idMat);
                    }        
                    
                    function BuscarMateriaByMaestriaVersion($prepareDQL) //Ok
                    {       return $this->DAOMatInformacion->ObtenerMateriaByMaestriaVersion($prepareDQL);
                    }
                    
                    function BuscarMateriaContenidoByMalla($prepareDQL) //Ok
                    {       return $this->DAOMatInformacion->ObtenerMateriaContenidoByMalla($prepareDQL);
                    }
                    
                    function BuscarMaestriaVersionByFacultad($prepareDQL) //Ok
                    {       return $this->DAOMatInformacion->ObtenerMaestriaVersionByFacultad($prepareDQL);
                    }

                    function BuscarMaestriaVersionByFacultadSinCohorte($prepareDQL) //Ok
                    {       return $this->DAOMatInformacion->ObtenerMaestriaVersionByFacultadSinCohorte($prepareDQL);
                    }
                    
                    function ObtenerNumeroComponentes($IdMsv,$IdMat) //OK
                    {       return $this->DAOMatInformacion->ObtenerNumeroComponentes($IdMsv,$IdMat);
                    }

                    function BuscarValorComponente($Compte,$Params)
                    {       return $this->DAOMatInformacion->ObtenerValorComponente($Compte,$Params);
                    }
        }            
?>        
