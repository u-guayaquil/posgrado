<?php
require_once("src/rules/maestria/servicio/ServicioAsistencia.php");
require_once("src/rules/maestria/dao/DAORecord.php");

class ServicioRecord
{   private $DAORecord;

	function __construct()
	{       $this->DAORecord = new DAORecord();
	}

	function BuscarMaestria($Params)
	{       return $this->DAORecord->ConsultarMaestria($Params);
	}

	function BuscarCohorteByMaestria($Params)
	{       return $this->DAORecord->ConsultarCohorteByMaestria($Params);
	}

	function BuscarEstudiantesByCohorte($Params)
	{       return $this->DAORecord->ConsultarEstudiantesByCohorte($Params);
	}

	function BuscarEstudiantesByTxt($Params)
	{       return $this->DAORecord->ConsultarEstudiantesByTxt($Params);
	}

	function BuscarRecords($Params)
	{       return $this->DAORecord->ConsultarRecords($Params);
	}

	function DatosCabeceraReporteEstudiante($Params)
	{        return $this->DAORecord->ObtenerDatosCabeceraRecordEstudiante($Params);
	}
}
?>

