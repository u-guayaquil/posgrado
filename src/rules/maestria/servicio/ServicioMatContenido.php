<?php
            require_once("src/rules/maestria/dao/DAOMatContenido.php");
            
            class ServicioMatContenido 
            {       private $DAOMatContenido;
     
                    function __construct()
                    {        $this->DAOMatContenido = new DAOMatContenido();
                    }
           
                    function GuardaDB($JsDatos)
                    {       if (empty($JsDatos->id)){
                                return $this->Crea($JsDatos);
                            }    
                            else{
                                return $this->Edita($JsDatos);
                            }
                    }
                    
                    private function PreparaDatos($Form)
                    {       $Utils = new Utils();
                            $Form->descripcion = utf8_decode($Utils->ConvertTildesToUpper($Form->descripcion));
                            $Form->resultado = utf8_decode($Utils->ConvertTildesToUpper($Form->resultado));
                            $Form->metodos = utf8_decode($Utils->ConvertTildesToUpper($Form->metodos));
                            $Form->objaprende = utf8_decode($Utils->ConvertTildesToUpper($Form->objaprende));
                            $Form->cd = utf8_decode($Utils->ConvertTildesToUpper($Form->cd));
                            $Form->cp = utf8_decode($Utils->ConvertTildesToUpper($Form->cp));
                            $Form->ca = utf8_decode($Utils->ConvertTildesToUpper($Form->ca));
                            return $Form;
                    }
                    
                    function Crea($Form)
                    {       return $this->DAOMatContenido->Inserta($this->PreparaDatos($Form));
                    }
           
                    function Edita($Form)
                    {       if (intval($Form->id)!=intval($Form->idpadre)){
                                return $this->DAOMatContenido->Actualiza($this->PreparaDatos($Form));
                            }    
                            else{      
                                return array(false,"El item no puede subordinarse de si mismo.");
                            }   
                    }
                    
                    function Desactiva($id)
                    {       return $this->DAOMatContenido->Desactiva(intval($id)); 
                    }
                    
                    function BuscarByTipo($prepareDQL)
                    {     //Busqueda modal en materia contenido 
                            return $this->DAOMatContenido->ObtenerByTipo($prepareDQL);
                    }
                 
                    function BuscarIndice($prepareDQL)
                    {     //Consulta princial de la definicion de contenidos
                            $menus = array();
                            $level = 0;
                            $roots = $this->DAOMatContenido->ObtenerIndiceByTipo($prepareDQL);
                            if ($roots[0])
                            {   foreach ($roots[1] as $sheet)
                                {       $descripcion = trim($sheet['DESCRIPCION']);
                                        $child = array();
                                        $child = $this->BuscarIndiceSgte($level+1,$sheet['ID'],$descripcion,$prepareDQL['idmtc'],$prepareDQL['idcht']); 
                                        $menus[] = array('nivel' => $level,
                                                         'id' => $sheet['ID'],
                                                         'idcohorte' => $sheet['IDCOHORTE'],
                                                         'orden' => $sheet['ORDEN'],
                                                         'idtipo' => $sheet['IDTIPOPCION'],
                                                         'descripcion' => utf8_encode($descripcion),
                                                         'resultado' => utf8_encode($sheet['RESULTADO']),
                                                         'metodo' => utf8_encode($sheet['METODO']),
                                                         'objetivo' => utf8_encode($sheet['OBJETIVO']), 
                                                         'cd' => utf8_encode($sheet['CD']),'cp' => utf8_encode($sheet['CP']),'ca' => utf8_encode($sheet['CA']),
                                                         'idpadre' => 0,'txpadre' => ' ','estado' => 1,'hijos' => $child);
                                }
                            }    
                            return json_encode($menus);
                    }

                    private function BuscarIndiceSgte($level,$idpadre,$txpadre,$idmtc,$idcht) 
                    {     //Consulta princial de la definicion de contenidos
                            $subms = array();
                            $roots = $this->DAOMatContenido->ObtenerIndiceByIDPadre($idmtc,$idpadre,$idcht);
                            if ($roots[0])
                            {   foreach ($roots[1] as $sheet)
                                {       $descripcion = trim($sheet['DESCRIPCION']);
                                        $child = array();
                                        if ($sheet['IDTIPOPCION']!=2)
                                        {   $child = $this->BuscarIndiceSgte($level+1,$sheet['ID'],$descripcion,$idmtc,$idcht);
                                        }
                                        $subms[] = array('nivel' => $level,
                                                         'id' => $sheet['ID'],
                                                         'idcohorte' => $sheet['IDCOHORTE'],
                                                         'orden' => $sheet['ORDEN'],
                                                         'idtipo' => $sheet['IDTIPOPCION'],
                                                         'descripcion' => utf8_encode($descripcion),
                                                         'resultado' => utf8_encode($sheet['RESULTADO']),
                                                         'metodo' => utf8_encode($sheet['METODO']),
                                                         'objetivo' => utf8_encode($sheet['OBJETIVO']), 
                                                         'cd' => utf8_encode($sheet['CD']), 'cp' => utf8_encode($sheet['CP']), 'ca' => utf8_encode($sheet['CA']),
                                                         'idpadre' => $idpadre,'txpadre' => utf8_encode($txpadre),'estado' => $sheet['IDESTADO'],'hijos' => $child);                                        
                                }
                            }    
                            return $subms;
                    }
                    
                    
                    function BuscarContenidoByIndice($Params)
                    {       $idpln = intval($Params[0]); 
                            $idmtc = intval($Params[1]);
                            $idniv = intval($Params[2]);
                            $idcht = intval($Params[3]);
                            
                            $menus = array();
                            $roots = $this->DAOMatContenido->ObtenerContenidoByIndice($idmtc,$idcht);
                            if ($roots[0])
                            {   foreach ($roots[1] as $sheet)
                                {       $descripcion = utf8_encode(trim($sheet['DESCRIPCION']));
                                        $padre = $descripcion;
                                        $child = array();
                                        if ($idniv>0)
                                        {   $padre = "<b>".$descripcion."</b>";
                                            $child = $this->BuscarContenidoByIndiceSgte($sheet['ID'],$padre,$idpln,$idmtc,$idniv,$idcht); 
                                        }
                                        $menus[] = array('id' => $sheet['ID'],
                                                         'idtipo' => $sheet['IDTIPOPCION'],
                                                         'descripcion' => $descripcion,
                                                         'ruta' => $padre,
                                                         'orden' => $sheet['ORDEN'],
                                                         'tpniv' => $idniv,
                                                         'hijos' => $child);    
                                }
                            }    
                            return json_encode($menus);
                    }

                    private function BuscarContenidoByIndiceSgte($idpadre,$txpadre,$idpln,$idmtc,$idniv,$idcht) 
                    {       $subms = array();
                            $roots = $this->DAOMatContenido->ObtenerContenidoByIndiceSgte($idpadre,$idpln,$idmtc,$idniv,$idcht);
                            if ($roots[0])
                            {   foreach ($roots[1] as $sheet)
                                {       $descripcion = utf8_encode(trim($sheet['DESCRIPCION']));
                                        $child = array();
                                        if ($sheet['IDTIPOPCION']!=2)
                                        {   $txruta = $txpadre."<br>".($idniv==1 ? $descripcion : "<b>".$descripcion."</b>");
                                            $child = $this->BuscarContenidoByIndiceSgte($sheet['ID'],$txruta,$idpln,$idmtc,$idniv,$idcht);
                                            $subms[] = array('id' => $sheet['ID'],'idtipo' => $sheet['IDTIPOPCION'],'descripcion' => $descripcion,'ruta' => $txruta,   
                                                             'orden' => $sheet['ORDEN'],'hijos' => $child);
                                        }
                                        else
                                        {   $txruta = $txpadre."<br>".$descripcion;
                                            $subms[] = array('id' => $sheet['ID'],'idtipo' => $sheet['IDTIPOPCION'],'descripcion' => $descripcion,'ruta' => $txruta,   
                                                             'orden' => $sheet['ORDEN'],'hijos' => $child);                                        
                                        }
                                }
                            }    
                            return $subms;
                    }                    
}        
?>        
