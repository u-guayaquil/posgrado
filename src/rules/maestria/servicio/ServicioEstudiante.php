<?php       require_once("src/rules/maestria/servicio/ServicioAsistencia.php");
            require_once("src/rules/maestria/dao/DAOEstudiante.php");
            
            class ServicioEstudiante
            {       private $DAOEstudiante;
                    
                    function __construct()
                    {       $this->DAOEstudiante = new DAOEstudiante();
                    }
                    
                    function BuscarMaestria($Params)
                    {       return $this->DAOEstudiante->ConsultarMaestria($Params);                   
                    }
                    
                    function BuscarCohorteByMaestria($Params)
                    {       return $this->DAOEstudiante->ConsultarCohorteByMaestria($Params);                   
                    }
                    
                    function BuscarParalelosByCohorte($IdCohorte)
                    {       return $this->DAOEstudiante->ConsultarParalelosyCohorte($IdCohorte);
                    }
                    
                    function BuscarEstudiantes($Params)
                    {       return $this->DAOEstudiante->ConsultarEstudiantes($Params);
                    }
            }
?>

