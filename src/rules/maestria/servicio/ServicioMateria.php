<?php
            require_once("src/rules/maestria/dao/DAOMateria.php");
            
            class ServicioMateria 
            {       private $DAOMateria;
     
                    function __construct()
                    {        $this->DAOMateria = new DAOMateria();
                    }
           
                    function BuscarMateriaByMaestria($id)
                    {       return $this->DAOMateria->ObtenerMateriaByMaestria(intval($id));
                    }
                    
                    function BuscarMateriaMallaByMaestriaVersion($prepareDQL)
                    {       return $this->DAOMateria->ObtenerMateriaMallaByMaestriaVersion($prepareDQL);
                    }

                    function GuardaDBMateria($Materia)
                    {       if (empty($Materia->id)){
                                return $this->CreaMateria($Materia);
                            }else{
                                return $this->EditaMateria($Materia);
                            }
                    }
                    
                    function CreaMateria($Form)
                    {   $UltimaMateria = $this->RecuperaUltimaMateriaByMaestria($Form->idmaestria);
                        $idMateria = $this->DAOMateria->InsertaMateria($Form);
                        if($UltimaMateria){
                            $DatosMallDetByMateria = $this->RecuperaDatosMallDet($UltimaMateria->ID);
                            $this->InsertaMateriaEnMallaDetalle($DatosMallDetByMateria,$idMateria);
                        }else{
                            $MallaByMaestria = $this->RecuperaMallaByMaestria($Form->idmaestria);
                            $this->InsertaMateriaEnNuevaMallaDetalle($MallaByMaestria,$idMateria);
                        }
                        return $idMateria; 
                    }
           
                    function EditaMateria($Form)
                    {   $ExisteMateriaMalla = $this->DAOMateria->ObtenerMateriaMalla($Form->id);
                        if($ExisteMateriaMalla){
                            return $this->DAOMateria->ActualizaMateria($Form);
                        }    
                        else{
                            $UltimaMateria = $this->RecuperaUltimaMateriaByMaestria($Form->idmaestria);
                            $idMateria = $this->DAOMateria->ActualizaMateria($Form);
                            $DatosMallDetByMateria = $this->RecuperaDatosMallDet($UltimaMateria->ID);
                            $this->InsertaMateriaEnMallaDetalle($DatosMallDetByMateria,$idMateria);
                            return $idMateria;
                        }
                    }
                    
                    function DesactivaMateria($id)
                    {       return $this->DAOMateria->DesactivaMateria(intval($id));
                    }
                    
                    function BuscarMateriaByID($id)
                    {       return $this->DAOMateria->ObtenerMateriaByID(intval($id));
                    }

                    function BuscarMateriaByDescripcion($prepareDQL)
                    {       return $this->DAOMateria->ObtenerMateriaByDescripcion($prepareDQL);
                    }
                    
                    function BuscarMateriaEstadoByID($id)
                    {       return $this->DAOMateria->ObtenerMateriaEstadoByID(intval($id));
                    }
                    
                    function SeleccionarTodos()
                    {       return $this->DAOMateria->ObtenerMaterias();
                    }
                    
                    function BuscarMaestriaByDescripcion($prepareDQL)
                    {       return $this->DAOMateria->ObtenerMaestriaByDescripcion($prepareDQL);
                    }
                    
                    function BuscarMateriaEstadoByMaestria($id)
                    {       return $this->DAOMateria->ObtenerMateriaEstadoByMaestria(intval($id));
                    }
                    
                    function BuscarMateriaByDescripcionByMaestria($prepareDQL)
                    {       return $this->DAOMateria->ObtenerMateriaByDescripcionByMaestria($prepareDQL);
                    }

                    function BuscarMateriaByIDByMaestria($prepareDQL)
                    {       return $this->DAOMateria->ObtenerMateriaByIDByMaestria($prepareDQL);
                    }

                    function RecuperaUltimaMateriaByMaestria($idmaestria)
                    {       return $this->DAOMateria->ObtenerUltimaMateriaByMaestria($idmaestria);
                    }
                    
                    function RecuperaDatosMallDet($id)
                    {       return $this->DAOMateria->ObtenerDatosMallDet($id);
                    }

                    function RecuperaMallaByMaestria($idmaestria)
                    {       return $this->DAOMateria->ObtenerMallaByMaestria($idmaestria);
                    }
                    
                    function InsertaMateriaEnMallaDetalle($DatosMallByMateria,$id)
                    {       return $this->DAOMateria->GuardaMateriaEnMallaDetalle($DatosMallByMateria,$id);
                    }

                    function InsertaMateriaEnNuevaMallaDetalle($DatosMalla,$idMateria)
                    {       return $this->DAOMateria->GuardaMateriaEnNuevaMallaDetalle($DatosMalla,$idMateria);
                    }
            }        
?>        
