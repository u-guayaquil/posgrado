<?php
            require_once("src/rules/maestria/dao/DAOMaestria.php");
            
            class ServicioMaestria 
            {       private $DAOMaestria;
     
                    function __construct()
                    {        $this->DAOMaestria = new DAOMaestria();
                    }
                               
                    function BuscarMaestriaVersionByFacultad($prepareDQL) //ok
                    {       return $this->DAOMaestria->ObtenerMaestriaVersionByFacultad($prepareDQL);
                    }

                    function GuardaDBMaestria($Maestria)
                    {       if (empty($Maestria->id)){
                                return $this->CreaMaestria($Maestria);
                            }else{
                                return $this->EditaMaestria($Maestria);
                            }
                    }
                    
                    function CreaMaestria($Form)
                    {       return $this->DAOMaestria->InsertaMaestria($Form);
                    }
           
                    function EditaMaestria($Form)
                    {       return $this->DAOMaestria->ActualizaMaestria($Form);
                    }
                    
                    function DesactivaMaestria($id)
                    {       return $this->DAOMaestria->DesactivaMaestria(intval($id));
                    }
                    
                    function BuscarMaestriaByID($id)
                    {       return $this->DAOMaestria->ObtenerMaestriaByID(intval($id));
                    }

                    function BuscarMaestriaByDescripcion($prepareDQL)
                    {       return $this->DAOMaestria->ObtenerMaestriaByDescripcion($prepareDQL);
                    }
                    
                    function BuscarMaestriaEstadoByID($id)
                    {       return $this->DAOMaestria->ObtenerMaestriaEstadoByID(intval($id));
                    }
                    
                    function SeleccionarTodos()
                    {       return $this->DAOMaestria->ObtenerMaestria();
                    }
                    
                    function BuscarFacultadByDescripcion($prepareDQL)
                    {       return $this->DAOMaestria->ObtenerFacultadByDescripcion($prepareDQL);
                    }
                    
                    function BuscarMaestriaEstadoByIdFacultad($id)
                    {       return $this->DAOMaestria->ObtenerMaestriaEstadoByFacultad(intval($id));
                    }
                    
                    function BuscarMaestriaByDescripcionByFacultad($prepareDQL)
                    {       return $this->DAOMaestria->ObtenerMaestriaByDescripcionByFacultad($prepareDQL);
                    }
                    
                    function BuscarMaestriaByIDByFacultad($prepareDQL)
                    {       return $this->DAOMaestria->ObtenerMaestriaByIDByFacultad($prepareDQL);
                    }
            }        
?>        
