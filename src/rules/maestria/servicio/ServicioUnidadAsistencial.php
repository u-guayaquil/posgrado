<?php
            require_once("src/rules/maestria/dao/DAOParalelo.php");
            class ServicioParalelo
            {       private $DAOParalelo;
     
                    function __construct()
                    {        $this->DAOParalelo = new DAOParalelo();
                    }
                    
                    function GuardaDBParalelo($Paralelo)
                    {       if (empty($Paralelo->id)){
                                return $this->CreaParalelo($Paralelo);
                            }else{
                                return $this->EditaParalelo($Paralelo);
                            }
                    }
                    
                    function CreaParalelo($Form)
                    {       return $this->DAOParalelo->InsertaParalelo($Form);
                    }
           
                    function EditaParalelo($Form)
                    {       return $this->DAOParalelo->ActualizaParalelo($Form);
                    }
                    
                    function DesactivaParalelo($id)
                    {       return $this->DAOParalelo->DesactivaParalelo(intval($id));
                    }
                    
                    function BuscarParaleloByID($id)
                    {       return $this->DAOParalelo->ObtenerParaleloByID(intval($id));
                    }

                    function BuscarParaleloByDescripcion($prepareDQL)
                    {       return $this->DAOParalelo->ObtenerParaleloByDescripcion($prepareDQL);
                    }
                    
                    function BuscarParaleloEstadoByID($id)
                    {       return $this->DAOParalelo->ObtenerParaleloEstadoByID(intval($id));
                    }
                    
                    function SeleccionarTodos()
                    {       return $this->DAOParalelo->ObtenerParalelos();
                    }
                    
                    function BuscarMaestriaByDescripcion($prepareDQL)
                    {       return $this->DAOParalelo->ObtenerMaestriaByDescripcion($prepareDQL);
                    }
                    
                    function BuscarCohorteByDescripcion($prepareDQL)
                    {       return $this->DAOParalelo->ObtenerCohorteByDescripcion($prepareDQL);
                    }
                    
                    function BuscarCohorteByVmaestriaByDescripcion($prepareDQL)
                    {       return $this->DAOParalelo->ObtenerCohorteByVmaestriaByDescripcion($prepareDQL);
                    }
                    
                    function BuscarParaleloEstadoByCohorte($idCohorte)
                    {       return $this->DAOParalelo->ObtenerParaleloEstadoByCohorte(intval($idCohorte));
                    }
                    
                    function BuscarParaleloEstadoByVMaestria()
                    {       return $this->DAOParalelo->ObtenerParaleloEstadoByVMaestria();
                    }
                    
                    function BuscarParaleloByDescripcionByCohorte($prepareDQL)
                    {       return $this->DAOParalelo->ObtenerParaleloByDescripcionByCohorte($prepareDQL);
                    }

                    function BuscarParaleloByIDByCohorte($prepareDQL)
                    {       return $this->DAOParalelo->ObtenerParaleloByIDByCohorte($prepareDQL);
                    }
            }
?>

