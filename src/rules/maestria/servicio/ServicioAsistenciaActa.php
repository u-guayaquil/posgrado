<?php       require_once("src/rules/maestria/dao/DAOAsistenciaActa.php");
            require_once("src/rules/maestria/dao/DAOHorario.php");
            
            class ServicioAsistenciaActa
            {       private $DAOAsistenciaActa;
                    private $DAOHorario;
                    
                    function __construct()
                    {        $this->DAOAsistenciaActa = new DAOAsistenciaActa();
                             $this->DAOHorario = new DAOHorario();
                    }
                    
                    function ConsultaAsistenciaActa($Params)
                    {       return $this->DAOAsistenciaActa->ObtenerAsistenciaActa($Params);
                    }
                    
                    function ConsultaDetalleAsistenciaActa($Params)
                    {       return $this->DAOAsistenciaActa->ObtenerAsistenciaDetalle($Params);
                        
                    }
                    function ConsultaDetalleAsistenciaActaByUAD($Params,$UAD)
                    {       return $this->DAOAsistenciaActa->ObtenerAsistenciaDetalleByUAD($Params,$UAD);
                        
                    }
            }
?>

