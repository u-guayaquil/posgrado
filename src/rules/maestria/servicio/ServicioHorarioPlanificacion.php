<?php

            require_once("src/rules/maestria/dao/DAOHorarioPlanificacion.php");
            
            class ServicioHorarioPlanificacion
            {       private $DAOHorarioPlanificacion;
                   
            
            
                    function __construct()
                    {        $this->DAOHorarioPlanificacion = new DAOHorarioPlanificacion();
                    }
            
                    function BuscarHorario($idpln)
                    {       return $this->DAOHorarioPlanificacion->ObtenerHorario($idpln);
                        
                    }
                    
                    function GuardaHorarioDB($Params)
                    {       return $this->DAOHorarioPlanificacion->InsertaHorario($Params);
                    }
                    
                    function CalculaHoraClaseByNumhoras($NoHoras)
                    {       $Utils = new Utils();
                            
                            $tmin = ($Utils->GetMinOficial()*$NoHoras)%60;
                            $thor = intval(($Utils->GetMinOficial()*$NoHoras)/60)+($Utils->GetHorOficial()*$NoHoras);
                            return array('hora'=>$thor,'mint'=>$tmin);
                    }

                    function BuscarHorarioMateriaByPlanificacion($Params,$Compte)
                    {       return $this->DAOHorarioPlanificacion->ObtenerHorarioMateriaByPlanificacion($Params,$Compte);   
                    }
            }        
?>