<?php
require_once("src/rules/maestria/dao/DAORotacion.php");
class ServicioRotacion
{	private $DAORotacion;

	function __construct()
	{        $this->DAORotacion = new DAORotacion();
	}
	
	function GuardaDB($Rotacion)
	{   if (empty($Rotacion->id)){
			return $this->CreaRotacion($Rotacion);
		}else{
			return $this->EditaRotacion($Rotacion);
		}
	}
	
	function CreaRotacion($Form)
	{       return $this->DAORotacion->InsertaRotacion($Form);
	}

	function EditaRotacion($Form)
	{       return $this->DAORotacion->ActualizaRotacion($Form);
	}
	
	function DesactivaRotacion($id)
	{       return $this->DAORotacion->DesactivaRotacion(intval($id));
	}

	function BuscarMaestriaByDescripcion($prepareDQL)
	{       return $this->DAORotacion->ObtenerMaestriaByDescripcion($prepareDQL);
	}
	
	function BuscarCohorteByDescripcion($prepareDQL)
	{       return $this->DAORotacion->ObtenerCohorteByDescripcion($prepareDQL);
	}

	function BuscarHospitalByDescripcion($prepareDQL)
	{       return $this->DAORotacion->ObtenerHospitalByDescripcion($prepareDQL);
	}

	function BuscarRotacionByCohorte($idCohorte)
	{       return $this->DAORotacion->ObtenerRotacionByCohorte(intval($idCohorte));
	}
	
	function BuscarRotacionByID($prepareDQL)
	{       return $this->DAORotacion->ObtenerRotacionByID($prepareDQL);
	}

	function BuscarRotacionByDescripcion($prepareDQL)
	{       return $this->DAORotacion->ObtenerRotacionByDescripcion($prepareDQL);
	}
	
	function BuscarRotacionByIDProcesado($id)
	{       return $this->DAORotacion->ObtenerRotacionByIDProcesado(intval($id));
	}
}
?>

