<?php       require_once('src/libs/clases/Mailer.php');
            require_once("src/rules/seguridad/servicio/ServicioLogin.php");
            require_once("src/modules/seguridad/login/render/RenderLogin.php");
            require_once("src/rules/maestria/dao/DAOExterno.php");
            require_once("src/rules/sistema/dao/DAOUsuario.php");
            require_once("src/rules/sistema/dao/DAOUsuarioPerfil.php");
            
            class ServicioExterno 
            {       private $DAOExterno;
                    private $DAOUsuario;
                    private $DAOUsuarioPerfil;
                    private $ServicioLogin;
                    private $Password;
                    
                    function __construct()
                    {        $this->DAOExterno = new DAOExterno();
                             $this->DAOUsuario = new DAOUsuario();
                             $this->DAOUsuarioPerfil = new DAOUsuarioPerfil();
                             $this->ServicioLogin = new ServicioLogin(); 
                    }
           
                    private function PreparaDatos($Form)
                    {       $Utils = new Utils();
                            $Form->nombre = utf8_decode($Utils->ConvertTildesToUpper($Form->nombre));
                            $Form->apellido = utf8_decode($Utils->ConvertTildesToUpper($Form->apellido));
                            $Form->direccion = utf8_decode($Utils->ConvertTildesToUpper($Form->direccion));
                            $Form->email = strtolower($Form->email);
                            $Form->idExterno = strtoupper($Form->idExterno);
                            return $Form;
                    }       
                    
                    function GuardaDB($Op,$JsDatos) //Ok
                    {       $MsGston = "";
                            $JsDatos = $this->PreparaDatos($JsDatos);
                            if ($this->ServicioLogin->EsCorreo($JsDatos->email))
                            {   $MsGston = "Se creo el rol de gestión administrativa revise su correo ".$JsDatos->email;
                                if ($Op==0)
                                {   $Resp = $this->CreaGestion($JsDatos);
                                    if ($Resp[0] && is_numeric($Resp[1])) 
                                        $JsDatos->idroluser = $Resp[1];
                                    else
                                        $MsGston = $Resp[1];
                                    return $this->DAOExterno->Inserta($JsDatos,$MsGston);
                                }
                                else
                                {   $Resp = $this->ActualGestion($JsDatos);
                                    if ($Resp[0] && is_numeric($Resp[1])) 
                                        $JsDatos->idroluser = $Resp[1];
                                    else
                                        $MsGston = $Resp[1];
                                    return $this->DAOExterno->Actualiza($JsDatos,$MsGston);
                                }
                            }
                            else
                            return array(false,"Error en la dirección de correo electrónico.");
                    }
                    
                    function Desactiva($Cedula)//Ok
                    {       return $this->DAOExterno->Inactiva($Cedula); 
                    }
                    
                    function BuscarByCedula($Cedula)//Ok
                    {       return $this->DAOExterno->ObtenerByCedula($Cedula);
                    }

                    function BuscarExterno($prepareDQL) //Ok
                    {       return $this->DAOExterno->ObtenerExterno($prepareDQL);
                    }
                    
                    function BuscarExternoPlanificacion($prepareDQL) //ok
                    {       return $this->DAOExterno->ObtenerExternoPlanificacion($prepareDQL);
                    }
                    
                    function BuscarExternoByTX($prepareDQL) //ok
                    {       return $this->DAOExterno->ObtenerExternoByTX($prepareDQL);   
                        
                    }
                    
                    private function CreaGestion($Externo)
                    {       $this->Password = "";
                            if ($Externo->idgestion!=0)
                            {   $Resp = $this->VerificaUsuarioPassword($Externo->idExterno);
                                if ($Resp[0]==false)
                                {   if ($Resp[1]=="CMD_CREAR_CUENTA")
                                    {   $this->Password = $this->ServicioLogin->GenerarPasswordRandom();
                                        $Resp = $this->DAOUsuario->CreaCuentaByUsuarioPasswordSIUG($Externo->idExterno,$this->Password);
                                        if ($Resp[0])
                                        {   $Resp = $this->DAOExterno->CreaRolGestion($Externo->idExterno,$Externo->idgestion);
                                            if ($Resp[0])
                                            {   $Modulo = json_decode(Session::getValue('Sesion'));
                                                $Login = array('id'=>$Resp[1],'rol'=>$Externo->idgestion,'modulo'=>$Modulo->Idmodulo);
                                                $this->DAOUsuarioPerfil->CreaUsuarioPerfilDefault($Login);
                                                
                                                $this->SendMailConfirmacion($Externo); 
                                            }       
                                            return $Resp; 
                                        }
                                    }
                                    return $Resp; 
                                }
                                else
                                {   $Resp = $this->DAOUsuario->ObtenerUsuarioRol($Externo->idExterno,$Externo->idgestion);
                                    print_r($Resp);
                                    if ($Resp[0]==false)
                                        return $this->DAOExterno->CreaRolGestion($Externo->idExterno,$Externo->idgestion);
                                    else
                                        return array(true,$Resp[1][0]['ID']); //Asocio el ID Administrativo
                                }
                            }    
                            return array(false,"");
                    }        

                    private function SendMailConfirmacion($Externo)
                    {       $Mssg = new RenderLogin();
                            $Mail = new Mailer(); 
                            $Mail->setFromName("Unidad de Posgrado de la Universidad de Guayaquil.");
                            $Mail->setSubject("Cuenta SIUG-POSGRADO.");
                            
                            $Comp = $Externo->nombre.' '.$Externo->apellido;
                            $Data = array('rol'=>3,'usuario'=>$Externo->idExterno,'password'=>$this->Password,'nombrecompleto'=>$Comp);
                            $Mail->setBodyHTML($Mssg->MensajeCorreo("MAIL_CREA_CUENTA_COORD",$Data));
                            $Mail->addTo($Externo->email); //$Externo->email
                            return $Mail->sendMail();
                    }

                    private function ActualGestion($Externo)
                    {       if ($Externo->idroluser!=0)
                            {   if ($Externo->idgestion==0)
                                    return $this->DAOUsuario->CambiaEstadoUsuarioRol(0,$Externo->idroluser);
                                else
                                {   $Resp = $this->VerificaUsuarioPassword($Externo->idExterno);
                                    if ($Resp[0])
                                        return $this->DAOUsuario->CambiaEstadoUsuarioRol(1,$Externo->idroluser);
                                    else
                                        return array(false,$Resp[1]!="CMD_CREAR_CUENTA" ? $Resp[1] : "El externo no tiene cuenta. No fue creada la gestión administrativa"); 
                                }
                            }
                            else return $this->CreaGestion($Externo);
                    }        
                    
                    private function VerificaUsuarioPassword($Usuario)
                    {       $User = $this->DAOUsuario->BuscarCuentaByUsuarioPasswordSIUG($Usuario);
                            if ($User[0])
                            {   $Data = $User[1][0];
                                if ($Data['IDESTADO']==1)
                                {   if ($Data['CONFIRMACION']=='S')
                                    {   $HaCaducado = $this->ServicioLogin->HaCaducadoAcceso($Data['FECADUCIDAD']->format("Y-m-d")); 
                                        if ($HaCaducado)
                                            return $this->DAOUsuario->ConfirmaCuentaSIUG($Usuario,$HaCaducado);
                                        else
                                        return array(true,"");
                                    }   
                                    return array(true,"");
                                }
                                return array(false,"La cuenta del externo se encuentra inactiva. Si desea asignar gestión debe activarla.");
                            }
                            return $User; 
                    }
        }        

?>        
