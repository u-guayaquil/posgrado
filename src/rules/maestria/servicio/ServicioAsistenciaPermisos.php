<?php
    require_once("src/rules/maestria/dao/DAOAsistenciaPermisos.php");

    class ServicioAsistenciaPermisos
    {       private $DAOAsistenciaPermisos; 

            function __construct()
            {   $this->DAOAsistenciaPermisos = new DAOAsistenciaPermisos();    
            }
           
            function BuscarSolicitudes($Params)
            {       return $this->DAOAsistenciaPermisos->ObtenerSolicitudes($Params);
            }
            
            function GuardaAsistenciaPermisos($Form)
            {       return $this->DAOAsistenciaPermisos->ActualizaAsistenciaPermisos($Form);
            }
     }
?>
