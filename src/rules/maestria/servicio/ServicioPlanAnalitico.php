<?php
    require_once("src/rules/maestria/dao/DAOPlanAnalitico.php");

    class ServicioPlanAnalitico
    {       private $DAOPlanAnalitico;

            function __construct()
            {        $this->DAOPlanAnalitico = new DAOPlanAnalitico();
            }

            function GuardaDBCab($Planficacion) //ok
            {       if (empty($Planficacion->id))
                    {   $resp = $this->DAOPlanAnalitico->InsertaPlanificacion($Planficacion);
                        if ($resp[0])
                        {   $Planficacion->id = $resp[1];
                            return array(true,$Planficacion);
                        }
                    }
                    else
                    {   $resp = $this->DAOPlanAnalitico->UpdatePlanificacion($Planficacion);
                        if ($resp[0])
                        {   return array(true,$Planficacion);
                        }
                    }
                    return $resp;
            }

            function EliminaPlanAnalitico($Id)
            {       return $this->DAOPlanAnalitico->EliminaPlanAnalitico($Id);
            }

            function EliminaDetallePlanAnalitico($Id)
            {       return $this->DAOPlanAnalitico->EliminaDetallePlanAnalitico($Id);
            }

            function BuscarPlanAnalitico($Params)
            {       $cab = $this->DAOPlanAnalitico->ObtenerPlanAnalitico($Params);
                    if ($cab[0])
                    {   $PlanAnalitico = $cab[1][0]["ID"];
                        $det = $this->DAOPlanAnalitico->ObtenerPlanAnaliticoDetalle($PlanAnalitico,$Params[3]);

                        if ($det[0])
                            return array(true,$cab[1],$det[1]);
                        else
                            return array(true,$cab[1],array());
                    }
                    return $cab;
            }

            function BuscarPlanAnaliticoDetalle($IdPlan,$IdFiltro=0)
            {        return $this->DAOPlanAnalitico->ObtenerPlanAnaliticoDetalle($IdPlan,$IdFiltro);
            }

            function BuscarMaestriaByDocenteFacultad($prepareDQL)
            {       return $this->DAOPlanAnalitico->ObtenerMaestriaByDocenteFacultad($prepareDQL);
            }

            function BuscarMateriaGrupoByMaestriaCiclo($prepareDQL)
            {       return $this->DAOPlanAnalitico->ObtenerMateriaGrupoByMaestriaCiclo($prepareDQL);
            }

            function EjecutaAsistenciaPlanificacion($Params,$Compte)
            {       if ($Params[6]=="N")
                        return $this->DAOPlanAnalitico->ActualizaAsistenciaPlanificacion($Params,$Compte);
                    else
                        return true;
            }

            function GuardaDB($Planficacion)
            {       if (empty($Planficacion->id))
                    {   $resp = $this->DAOPlanAnalitico->InsertaPlanificacion($Planficacion);
                        if ($resp[0])
                        {   $Planficacion->id = $resp[1];
                            $Indxs = array('id' => $Planficacion->id,'iddetalle' => $Planficacion->iddetalle,'idcontenido' => $Planficacion->idcontenido,'fecha' => $Planficacion->fecha);
                            $resp = $this->DAOPlanAnalitico->ExisteItemPlanificacion($Indxs);
                            if ($resp[0]==false)
                            {   $Indxs = array('idmtc' => $Planficacion->idmatcont,
                                                'idpln' => $Planficacion->id,
                                                'idcnt' => $Planficacion->iddetalle,
                                                'comcd' => $Planficacion->componentecd,
                                                'comcp' => $Planficacion->componentecp,
                                                'comca' => $Planficacion->componenteca);
                                $resp= $this->DAOPlanAnalitico->ExcedeHorasPlanificacion($Indxs);
                                if ($resp[0]==false)
                                {   if (empty($Planficacion->iddetalle))
                                    {   $resp = $this->DAOPlanAnalitico->InsertaItemPlanficacion($Planficacion);
                                        if ($resp[0])
                                        {   $Planficacion->iddetalle = $resp[1];
                                            return array(true,$Planficacion);
                                        }
                                    }
                                    else
                                    {   $resp = $this->DAOPlanAnalitico->UpdateItemPlanificacion($Planficacion);
                                        if ($resp[0])
                                        return array(true,$Planficacion);
                                    }
                                }
                                else
                                return array(false,$resp[1]);
                            }
                            else
                            return array(false,$resp[1]);
                        }
                        return $resp;
                    }
                    else
                    {   $resp = $this->DAOPlanAnalitico->UpdatePlanificacion($Planficacion);
                        if ($resp[0])
                        {   $Indxs = array('id' => $Planficacion->id,'iddetalle' => $Planficacion->iddetalle,'idcontenido' => $Planficacion->idcontenido,'fecha' => $Planficacion->fecha);
                            $resp = $this->DAOPlanAnalitico->ExisteItemPlanificacion($Indxs);
                            if ($resp[0]==false)
                            {   $Indxs = array('idmtc' => $Planficacion->idmatcont,
                                                'idpln' => $Planficacion->id,
                                                'idcnt' => $Planficacion->iddetalle,
                                                'comcd' => $Planficacion->componentecd,
                                                'comcp' => $Planficacion->componentecp,
                                                'comca' => $Planficacion->componenteca);
                                $resp= $this->DAOPlanAnalitico->ExcedeHorasPlanificacion($Indxs);
                                if ($resp[0]==false)
                                {   if (empty($Planficacion->iddetalle))
                                    {   $resp = $this->DAOPlanAnalitico->InsertaItemPlanficacion($Planficacion);
                                        if ($resp[0])
                                        {   $Planficacion->iddetalle = $resp[1];
                                            return array(true,$Planficacion);
                                        }
                                    }
                                    else
                                    {   $resp = $this->DAOPlanAnalitico->UpdateItemPlanificacion($Planficacion);
                                        if ($resp[0])
                                        return array(true,$Planficacion);
                                    }
                                }
                                else
                                return array(false,$resp[1]);
                            }
                            else
                            return array(false,$resp[1]);
                        }
                        return $resp;
                    }
            }

            function ReplicaPlanificacionDB($Planficacion)
            {   $AumentarDias = 7;
                $VecesAReplicar = $Planficacion->nVeces;
                $resp = $this->DAOPlanAnalitico->UpdatePlanificacion($Planficacion);
                if ($resp[0])
                {   $DetallePlanificacion = $this->DAOPlanAnalitico->ObtenerPlanAnaliticoDetalle($resp[1]);
                    for($i=1; $i<=$VecesAReplicar;$i++){
                        foreach($DetallePlanificacion[1] as $Count => $Item){
                            $FechaItem = date("d-m-Y",strtotime($Item['FECHA']->format("Y-m-d")."+ ".$AumentarDias." days"));
                            $Indxs = array('idmtc' => $Planficacion->idmatcont,
                                            'idpln' => $Planficacion->id,
                                            'idcnt' => $Planficacion->iddetalle,
                                            'comcd' => $Item['CD'],
                                            'comcp' => $Item['CP'],
                                            'comca' => $Item['CA']);
                            $response= $this->DAOPlanAnalitico->ExcedeHorasPlanificacion($Indxs);
                            if ($response[0]==false)
                            {   $DataCollection = array("id"=>$Planficacion->id,"semana"=>$i+1,"fecha"=>$FechaItem,"idcontenido"=>$Item['IDCONTENIDO'],
                                                        "sbcontenido"=>$Item['SBCONTENIDO'],"componentecd"=>$Item['CD'],
                                                        "componentecp"=>$Item['CP'],"componenteca"=>$Item['CA'],"horcd"=>$Item['HORCD']);
                                $Datos= (object)$DataCollection;
                                if (empty($Planficacion->iddetalle))
                                {   $respuesta = $this->DAOPlanAnalitico->InsertaItemPlanficacionReplicar($Datos);
                                    if ($respuesta[0])
                                    {   $DetallePlan = $this->DAOPlanAnalitico->ObtenerPlanAnaliticoDetalle($resp[1]);
                                        $ValorCD = 0; $ValorCP=0; $ValorCA=0;
                                        foreach($DetallePlan[1] as $Contador => $Registro){
                                            $ValorCD = $ValorCD+$Registro['CD'];
                                            $ValorCP = $ValorCP+$Registro['CP'];
                                            $ValorCA = $ValorCA+$Registro['CA'];
                                        }
                                        if($ValorCD===$Planficacion->compcd && $ValorCP===$Planficacion->compcp && $ValorCD===$Planficacion->compca)
                                        {   return array(true,$Planficacion);   }
                                    }
                                }
                            }
                            else{
                                $DetallePlan = $this->DAOPlanAnalitico->ObtenerPlanAnaliticoDetalle($resp[1]);
                                $ValorCD = 0; $ValorCP=0; $ValorCA=0;
                                $NuevoCD = $Item['CD']; $NuevoCP = $Item['CP']; $NuevoCA = $Item['CA'];
                                foreach($DetallePlan[1] as $Contador => $Registro){
                                        $ValorCD = $ValorCD+$Registro['CD'];
                                        $ValorCP = $ValorCP+$Registro['CP'];
                                        $ValorCA = $ValorCA+$Registro['CA'];
                                }
                                if($Item['CD']!=0)
                                    $NuevoCD = $Planficacion->compcd-$ValorCD;
                                if($Item['CP']!=0)
                                    $NuevoCP = $Planficacion->compcp-$ValorCP;
                                if($Item['CA']!=0)
                                    $NuevoCA = $Planficacion->compca-$ValorCA;

                                $DataCollection = array("id"=>$Planficacion->id,"semana"=>$i+1,"fecha"=>$FechaItem,"idcontenido"=>$Item['IDCONTENIDO'],
                                                        "sbcontenido"=>$Item['SBCONTENIDO'],"componentecd"=>$NuevoCD,
                                                        "componentecp"=>$NuevoCP,"componenteca"=>$NuevoCA,"horcd"=>$Item['HORCD']);

                                $Datos= (object)$DataCollection;
                                if (empty($Planficacion->iddetalle))
                                {   if($ValorCD==$Planficacion->compcd && $ValorCP==$Planficacion->compcp && $ValorCA==$Planficacion->compca)
                                    {   return array(true,$Planficacion);   }
                                    else{
                                        $respuesta = $this->DAOPlanAnalitico->InsertaItemPlanficacionReplicar($Datos);
                                    }
                                }
                            }
                        }
                        $AumentarDias=$AumentarDias+7;
                    }
                }
                return $resp;
            }
    }
?>

