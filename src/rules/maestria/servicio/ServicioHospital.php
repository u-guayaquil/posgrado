<?php
require_once("src/rules/maestria/dao/DAOHospital.php");

class ServicioHospital 
{       private $DAOHospital;

    function __construct()
    {        $this->DAOHospital = new DAOHospital();
    }

    function GuardaDBHospital($Hospital)
    {       if (empty($Hospital->id)){
                return $this->CreaHospital($Hospital);
            }else{
                return $this->EditaHospital($Hospital);
            }
    }
    
    function CreaHospital($Form)
    {   return $this->DAOHospital->InsertaHospital($Form);
    }

    function EditaHospital($Form)
    {   return $this->DAOHospital->ActualizaHospital($Form);
    }
    
    function DesactivaHospital($id)
    {       return $this->DAOHospital->DesactivaHospital(intval($id));
    }
    
    function BuscarHospitalByID($id)
    {       return $this->DAOHospital->ObtenerHospitalByID(intval($id));
    }

    function BuscarHospitalByIDGuardado($id)
    {       return $this->DAOHospital->ObtenerHospitalByIDGuardado(intval($id));
    }

    function BuscarHospitalByDescripcion($prepareDQL)
    {       return $this->DAOHospital->ObtenerHospitalByDescripcion($prepareDQL);
    }
}        
?>        
