<?php       require_once("src/rules/maestria/servicio/ServicioAsistencia.php");
            require_once("src/rules/maestria/servicio/ServicioMatInformacion.php");
            require_once("src/rules/maestria/dao/DAOAperturarActa.php");
            
            class ServicioAperturarActa
            {       private $DAOAperturarActa;
                    private $DAOPoliticaEvaluacion;
                    
                    function __construct()
                    {       $this->DAOAperturarActa = new DAOAperturarActa();
                            $this->DAOPoliticaEvaluacion = new DAOPoliticaEvaluacion();
                    }
                    
                    function ConsultaCierreActa($Params)
                    {       return $this->DAOAperturarActa->ObtCierreActaCalificacion($Params);
                    }
                    
                    function ConsultarNotas($Params)
                    {       $ServicioMatInforma = new ServicioMatInformacion();
                            $ValorComponente = $ServicioMatInforma->BuscarValorComponente('CD', array($Params[2],$Params[3]));
                            if ($ValorComponente[0])
                            {   $Compte = $ValorComponente[1][0]['COMPTE'];
                                if ($Compte>0)
                                {   $ColsPolitica = $this->DAOPoliticaEvaluacion->ObtenerColumnsPoliticaEvaluacion($Params[5],$Params[6],$Params[7],$Params[3]);
                                    if ($ColsPolitica[0])
                                    {   $AperturarActa = $this->DAOAperturarActa->ObtenerNotasMod($Params,$Compte);
                                        if ($AperturarActa[0])
                                        {   return array(true,$ColsPolitica[1],$AperturarActa[1]);
                                        }   
                                        return $AperturarActa;
                                    }       
                                    return array(false,"No existe estructura de calificación definida para la maestria y materia.");
                                }    
                                return array(false,"El valor del componente CD no puede ser cero.");
                            }
                            return $ValorComponente;
                    }
                    
                    function GuardaDB($Params)
                    {       return $this->DAOAperturarActa->ActualizarActa($Params);
                    }      
            }
?>

