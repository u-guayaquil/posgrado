<?php
            require_once("src/rules/maestria/dao/DAOPoliticaEvaluacion.php");
            
            class ServicioPoliticaEvaluacion 
            {       private $DAOPoliticaEvaluacion;
     
                    function __construct()
                    {        $this->DAOPoliticaEvaluacion = new DAOPoliticaEvaluacion();
                    }
                     
                    function ConsultaParcialesPoliticaEvaluacion($Politica)
                    {       return $this->DAOPoliticaEvaluacion->ObtenerParcialesPoliticaEvaluacion($Politica);
                    }

            }        
?>        
