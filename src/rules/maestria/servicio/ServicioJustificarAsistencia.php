<?php       require_once("src/rules/maestria/dao/DAOJustificarAsistencia.php");
            require_once("src/rules/maestria/dao/DAOHorario.php");
            
            class ServicioJustificarAsistencia  
            {       private $DAOJustificarAsistencia;
                    private $DAOHorario;
                    
                    function __construct()
                    {        $this->DAOJustificarAsistencia = new DAOJustificarAsistencia();
                             $this->DAOHorario = new DAOHorario();
                    }
                    
                    function ConsultaJustificaciones($Params)
                    {       return $this->DAOJustificarAsistencia->ObtenerJustificaciones($Params);   
                    }

                    function GuardaDBJustificar($Justificar,$Observacion)
                    {       if (empty($Justificar->idCabecera)){
                                return $this->CreaCabeceraJustificar($Justificar,$Observacion);
                            }else{
                                return $this->EditaCabeceraJustificar($Justificar);
                            }
                    }
                    
                    function CreaCabeceraJustificar($Form,$Observacion)
                    {   $RegistroExiste = $this->ValidarRegistroExistente($Form);
                        if($RegistroExiste == "ok")
                        {return array(false,"Ya existe una registro de Justificación para esta fecha");}
                        else{
                            $idJustificar = $this->DAOJustificarAsistencia->InsertaJustificarAsistenciaCabecera($Form,$Observacion);
                            return $idJustificar;
                        } 
                    }
           
                    function EditaCabeceraJustificar($Form)
                    {   $RegistroExiste = $this->ValidarRegistroExistente($Form);
                        if($RegistroExiste == "ok")
                        {return array(false,"Ya existe una registro de Justificación para esta fecha");}
                        else{    
                        return $this->DAOJustificarAsistencia->ActualizaJustificarAsistenciaCabecera($Form);
                        }
                    }

                    function ValidarRegistroExistente($Form){
                            $ExisteRegistro = $this->DAOJustificarAsistencia->BuscaJustificadoExistente($Form);
                            if($ExisteRegistro){ return "ok";}
                            else{ return "error";}
                    }
                    
                    function DesactivaJusticarAsistencia($idCabecera){
                        return $this->DAOJustificarAsistencia->DesactivaJustificadoCabecera($idCabecera);
                    }

                    function BuscarEstudiantes($Params)
                    {       return $this->DAOJustificarAsistencia->ConsultarEstudiantes($Params);
                    }

                    function AsignarEstudiantes($CedulaEstudiante,$IdCabecera)
                    {       return $this->DAOJustificarAsistencia->AsignarEstudiantes($CedulaEstudiante,$IdCabecera);
                    }

                    function DesasignarEstudiantes($CedulaEstudiante)
                    {       return $this->DAOJustificarAsistencia->DesasignarEstudiantes($CedulaEstudiante);
                    }
            }
?>

