<?php       require_once('src/libs/clases/Mailer.php');
            require_once("src/rules/seguridad/servicio/ServicioLogin.php");
            require_once("src/modules/seguridad/login/render/RenderLogin.php");
            require_once("src/rules/maestria/dao/DAODocente.php");
            require_once("src/rules/sistema/dao/DAOUsuario.php");
            require_once("src/rules/sistema/dao/DAOUsuarioPerfil.php");
            
            class ServicioDocente 
            {       private $DAODocente;
                    private $DAOUsuario;
                    private $DAOUsuarioPerfil;
                    private $ServicioLogin;
                    private $Password;
                    
                    function __construct()
                    {        $this->DAODocente = new DAODocente();
                             $this->DAOUsuario = new DAOUsuario();
                             $this->DAOUsuarioPerfil = new DAOUsuarioPerfil();
                             $this->ServicioLogin = new ServicioLogin(); 
                    }
           
                    private function PreparaDatos($Form)
                    {       $Utils = new Utils();
                            $Form->nombre = utf8_decode($Utils->ConvertTildesToUpper($Form->nombre));
                            $Form->apellido = utf8_decode($Utils->ConvertTildesToUpper($Form->apellido));
                            $Form->direccion = utf8_decode($Utils->ConvertTildesToUpper($Form->direccion));
                            $Form->email = strtolower($Form->email);
                            $Form->idDocente = strtoupper($Form->idDocente);
                            return $Form;
                    }       
                    
                    function GuardaDB($Op,$JsDatos) //Ok
                    {       $MsGston = "";
                            $JsDatos = $this->PreparaDatos($JsDatos);
                            if ($this->ServicioLogin->EsCorreo($JsDatos->email))
                            {   $MsGston = "Se creo el rol de gestión administrativa revise su correo ".$JsDatos->email;
                                if ($Op==0)
                                {   $Resp = $this->CreaGestion($JsDatos);
                                    if ($Resp[0] && is_numeric($Resp[1])) 
                                        $JsDatos->idroluser = $Resp[1];
                                    else
                                        $MsGston = $Resp[1];
                                    return $this->DAODocente->Inserta($JsDatos,$MsGston);
                                }
                                else
                                {   $Resp = $this->ActualGestion($JsDatos);
                                    if ($Resp[0] && is_numeric($Resp[1])) 
                                        $JsDatos->idroluser = $Resp[1];
                                    else
                                        $MsGston = $Resp[1];
                                    return $this->DAODocente->Actualiza($JsDatos,$MsGston);
                                }
                            }
                            else
                            return array(false,"Error en la dirección de correo electrónico.");
                    }
                    
                    function Desactiva($Cedula)//Ok
                    {       return $this->DAODocente->Inactiva($Cedula); 
                    }
                    
                    function BuscarByCedula($Cedula)//Ok
                    {       return $this->DAODocente->ObtenerByCedula($Cedula);
                    }

                    function BuscarDocente($prepareDQL) //Ok
                    {       return $this->DAODocente->ObtenerDocente($prepareDQL);
                    }
                    
                    function BuscarDocentePlanificacion($prepareDQL) //ok
                    {       return $this->DAODocente->ObtenerDocentePlanificacion($prepareDQL);
                    }
                    
                    function BuscarDocenteByTX($prepareDQL) //ok
                    {       return $this->DAODocente->ObtenerDocenteByTX($prepareDQL);   
                        
                    }
                    
                    function GuardaFacultad($DocID,$FacID) //ok
                    {       return $this->DAODocente->InsertaFacultad($DocID,$FacID);       
                    }
                    
                    private function CreaGestion($Docente)
                    {       $this->Password = "";
                            if ($Docente->idgestion!=0)
                            {   $Resp = $this->VerificaUsuarioPassword($Docente->idDocente);
                                if ($Resp[0]==false)
                                {   if ($Resp[1]=="CMD_CREAR_CUENTA")
                                    {   $this->Password = $this->ServicioLogin->GenerarPasswordRandom();
                                        $Resp = $this->DAOUsuario->CreaCuentaByUsuarioPasswordSIUG($Docente->idDocente,$this->Password);
                                        if ($Resp[0])
                                        {   $Resp = $this->DAODocente->CreaRolGestion($Docente->idDocente,$Docente->idgestion);
                                            if ($Resp[0])
                                            {   $Modulo = json_decode(Session::getValue('Sesion'));
                                                $Login = array('id'=>$Resp[1],'rol'=>$Docente->idgestion,'modulo'=>$Modulo->Idmodulo);
                                                $this->DAOUsuarioPerfil->CreaUsuarioPerfilDefault($Login);
                                                
                                                $this->SendMailConfirmacion($Docente); 
                                            }       
                                            return $Resp; 
                                        }
                                    }
                                    return $Resp; 
                                }
                                else
                                {   $Resp = $this->DAOUsuario->ObtenerUsuarioRol($Docente->idDocente,$Docente->idgestion);
                                    if ($Resp[0]==false)
                                        return $this->DAODocente->CreaRolGestion($Docente->idDocente,$Docente->idgestion);
                                    else
                                        return array(true,$Resp[1][0]['ID']); //Asocio el ID Administrativo
                                }
                            }    
                            return array(false,"");
                    }        

                    private function SendMailConfirmacion($Docente)
                    {       $Mssg = new RenderLogin();
                            $Mail = new Mailer(); 
                            $Mail->setFromName("Unidad de Posgrado de la Universidad de Guayaquil.");
                            $Mail->setSubject("Cuenta SIUG-POSGRADO.");
                            
                            $Comp = $Docente->nombre.' '.$Docente->apellido;
                            $Data = array('rol'=>3,'usuario'=>$Docente->idDocente,'password'=>$this->Password,'nombrecompleto'=>$Comp);
                            $Mail->setBodyHTML($Mssg->MensajeCorreo("MAIL_CREA_CUENTA_COORD",$Data));
                            $Mail->addTo('casalazarv@yahoo.es'); //$Docente->email
                            return $Mail->sendMail();
                    }

                    private function ActualGestion($Docente)
                    {       if ($Docente->idroluser!=0)
                            {   if ($Docente->idgestion==0)
                                    return $this->DAOUsuario->CambiaEstadoUsuarioRol(0,$Docente->idroluser);
                                else
                                {   $Resp = $this->VerificaUsuarioPassword($Docente->idDocente);
                                    if ($Resp[0])
                                        return $this->DAOUsuario->CambiaEstadoUsuarioRol(1,$Docente->idroluser);
                                    else
                                        return array(false,$Resp[1]!="CMD_CREAR_CUENTA" ? $Resp[1] : "El docente no tiene cuenta. No fue creada la gestión administrativa"); 
                                }
                            }
                            else return $this->CreaGestion($Docente);
                    }        
                    
                    private function VerificaUsuarioPassword($Usuario)
                    {       $User = $this->DAOUsuario->BuscarCuentaByUsuarioPasswordSIUG($Usuario);
                            if ($User[0])
                            {   $Data = $User[1][0];
                                if ($Data['IDESTADO']==1)
                                {   if ($Data['CONFIRMACION']=='S')
                                    {   $HaCaducado = $this->ServicioLogin->HaCaducadoAcceso($Data['FECADUCIDAD']->format("Y-m-d")); 
                                        if ($HaCaducado)
                                            return $this->DAOUsuario->ConfirmaCuentaSIUG($Usuario,$HaCaducado);
                                        else
                                        return array(true,"");
                                    }   
                                    return array(true,"");
                                }
                                return array(false,"La cuenta del docente se encuentra inactiva. Si desea asignar gestión debe activarla.");
                            }
                            return $User; 
                    }
        }        

?>        
