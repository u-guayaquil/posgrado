<?php
            require_once("src/rules/maestria/dao/DAOGrupo.php");
            
            class ServicioGrupo 
            {       private $DAOGrupo;
     
                    function __construct()
                    {        $this->DAOGrupo = new DAOGrupo();
                    }
           
                    function BuscarGrupoByCohorte($idCohorte)
                    {       return $this->DAOGrupo->ObtenerGrupoByCohorte($idCohorte);
                    }
                    
                    function BuscarGrupoByCiclo($idCiclo)
                    {       return $this->DAOGrupo->ObtenerGrupoByCiclo($idCiclo);
                    }

            }        
?>        
