<?php
        require_once("src/base/DBDAO.php");

        class DAOAsistenciaActa extends DBDAO
        {   
                private static $entityAsistencia;
                private $Works;

                public function __construct()
                {       self::$entityAsistencia = DBDAO::getConnection();
                        $this->Works = self::$entityAsistencia->MyFunction();
                }

                function ObtenerAsistenciaActa($Params)
                {       $SQL = "SELECT DISTINCT Y.FECHASISTENCIA,Z.HORARIO 
                                FROM POSTGRADO..COL_ESTUDIANTE_MATERIA AS X, POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA AS Y,POSTGRADO..COL_HORARIO_HORAS AS Z
                                WHERE X.IDESTADO=1 AND X.IDCICLO='$Params[1]' AND X.IDMAESTRIA='$Params[2]' AND X.IDMATERIA='$Params[3]' AND X.IDPARALELO='$Params[4]' AND X.ID=Y.IDESTUDIANTEMATERIA AND Y.IDHORHORAS=Z.ID
                                ORDER BY Y.FECHASISTENCIA,Z.HORARIO ";    
                        $resourceID = $this->Works->Query($SQL);               
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen registrada asistencias.");
                }

                function ObtenerAsistenciaDetalle($Params)
                {       $SQL = "SELECT W.CEDULA,W.APELLIDOS+' '+W.NOMBRES AS ALUMNO, Y.EFECTIVA,Y.JUSTIFICADA,Y.FALTA, EC.IDUAD
                                FROM  POSTGRADO..COL_ESTUDIANTE AS W, POSTGRADO..COL_ESTUDIANTE_COHORTE AS EC,
                                       POSTGRADO..COL_ESTUDIANTE_MATERIA AS X, POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA AS Y
                                WHERE X.IDESTADO=1 AND W.ID=X.IDESTUDIANTE AND X.IDCICLO='$Params[1]' 
                                AND X.IDMAESTRIA='$Params[2]' AND X.IDMATERIA='$Params[3]' 
                                AND X.IDPARALELO='$Params[4]' AND X.ID=Y.IDESTUDIANTEMATERIA
                                AND EC.IDESTUDIANTE = W.ID 
                                ORDER BY EC.IDUAD,W.APELLIDOS+' '+W.NOMBRES,Y.FECHASISTENCIA";
                       $resourceID = $this->Works->Query($SQL);               
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen registrada asistencias.");
                }

                function ObtenerAsistenciaDetalleByUAD($Params,$UAD)
                {       $SQL = "SELECT W.CEDULA,W.APELLIDOS+' '+W.NOMBRES AS ALUMNO, Y.EFECTIVA,Y.JUSTIFICADA,Y.FALTA, EC.IDUAD
                                FROM  POSTGRADO..COL_ESTUDIANTE AS W, POSTGRADO..COL_ESTUDIANTE_COHORTE AS EC,
                                       POSTGRADO..COL_ESTUDIANTE_MATERIA AS X, POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA AS Y
                                WHERE X.IDESTADO=1 AND W.ID=X.IDESTUDIANTE AND X.IDCICLO='$Params[1]' 
                                AND X.IDMAESTRIA='$Params[2]' AND X.IDMATERIA='$Params[3]' 
                                AND X.IDPARALELO='$Params[4]' AND X.ID=Y.IDESTUDIANTEMATERIA
                                AND EC.IDESTUDIANTE = W.ID AND EC.IDUAD = $UAD
                                ORDER BY EC.IDUAD,W.APELLIDOS+' '+W.NOMBRES,Y.FECHASISTENCIA";
                       $resourceID = $this->Works->Query($SQL);               
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen registrada asistencias.");
                }
        }    
