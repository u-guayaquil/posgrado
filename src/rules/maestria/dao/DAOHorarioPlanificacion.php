<?php

        require_once("src/base/DBDAO.php");

        class DAOHorarioPlanificacion extends DBDAO
        {   
            private static $entityHorarioPlanificacion;
            private $Works;
            private $Dc;
            private $Ut;
              
            public function __construct()
            {       self::$entityHorarioPlanificacion = DBDAO::getConnection();
                    $this->Works = self::$entityHorarioPlanificacion->MyFunction();
                    $this->Dc = new DateControl();
                    $this->Ut = new Utils();
            }

            
            public function ObtenerHorario($idpln) //OK
            {       $DataCollection = array();
                    
                    $SQL = "SET DATEFIRST 7; ";
                    $SQL.= "SELECT 1 AS COMPTE,X.IDPLANANA,X.HOREXCD AS HOREJE,0 AS NUM,DATEPART(dw,X.FECHA) AS DIASEMANA,X.HORCD AS HORA,SUM(X.CD) AS TOTALHORAS
                            FROM POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS X  
                            WHERE X.CD>0 AND X.IDPLANANA='$idpln' AND X.IDESTADO IN (1,6) 
                            GROUP BY X.IDPLANANA,X.HOREXCD,X.HORCD,DATEPART(dw,X.FECHA) 
                            ORDER BY COMPTE,HOREJE,(CASE DATEPART(dw,X.FECHA) WHEN 1 THEN 8 ELSE DATEPART(dw,X.FECHA) END) ASC,HORA DESC ";
                    $DataCollection = $this->CollectionHorario($SQL,$DataCollection);

                    $SQL = "SET DATEFIRST 7; ";
                    $SQL.= "SELECT 2 AS COMPTE,X.IDPLANANA,X.HOREXCP AS HOREJE,0 AS NUM,DATEPART(dw,X.FECHA) AS DIASEMANA,X.HORCP AS HORA,SUM(X.CP) AS TOTALHORAS
                            FROM POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS X  
                            WHERE X.CP>0 AND X.IDPLANANA='$idpln' AND X.IDESTADO IN (1,6)   
                            GROUP BY X.IDPLANANA,X.HOREXCP,X.HORCP,DATEPART(dw,X.FECHA) 
                            ORDER BY COMPTE,HOREJE,(CASE DATEPART(dw,X.FECHA) WHEN 1 THEN 8 ELSE DATEPART(dw,X.FECHA) END) ASC,HORA DESC ";
                    $DataCollection = $this->CollectionHorario($SQL,$DataCollection);

                    $SQL = "SET DATEFIRST 7; ";
                    $SQL.= "SELECT 3 AS COMPTE,X.IDPLANANA,X.HOREXCA AS HOREJE,0 AS NUM,DATEPART(dw,X.FECHA) AS DIASEMANA,X.HORCA AS HORA,SUM(X.CA) AS TOTALHORAS
                            FROM POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS X  
                            WHERE X.CA>0 AND X.IDPLANANA='$idpln' AND X.IDESTADO IN (1,6) 
                            GROUP BY X.IDPLANANA,X.HOREXCA,X.HORCA,DATEPART(dw,X.FECHA) 
                            ORDER BY COMPTE,HOREJE,(CASE DATEPART(dw,X.FECHA) WHEN 1 THEN 8 ELSE DATEPART(dw,X.FECHA) END) ASC,HORA DESC ";
                    $DataCollection = $this->CollectionHorario($SQL,$DataCollection);
                   
                    return array((count($DataCollection)>0),$DataCollection);
            }
            
            private function CollectionHorario($SQL,$DataCollection)
            {       $Acceso = false;
                    
                    $resourceID = $this->Works->Query($SQL);
                    while($Horas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Acceso = true;
                          $DataCollection[]=$Horas;            
                    }
                    $this->Works->CloseResource($resourceID);
                    return $DataCollection;    
            }            
            
            function InsertaHorario($Params) //OK
            {       $Ho = $Params[0];
                    $Pl = $Params[2];
                    $Dy = $Params[3];
                
                    $Cx = ($Params[1]==1 ? "CD": ($Params[1]==2 ? "CP":"CA"));
                    $Fl = "HOR".$Cx;
                    $Ej = "HOREX".$Cx;
                    
                    $SQL = "SET DATEFIRST 7; ";
                    $SQL.= "UPDATE POSTGRADO..COL_PLAN_ANALITICO_DETALLE SET ";
                    $SQL.= $Fl."='$Params[0]' ";
                    $SQL.= "WHERE ".$Ej."='N' AND IDPLANANA=$Pl AND DATEPART(dw,FECHA)=$Dy AND IDESTADO=1 AND ".$Cx.">0";
                    $resourceID = $this->Works->Query($SQL);
            }            
            
            function ObtenerHorarioMateriaByPlanificacion($Params,$Compte)
            {       $DataCount = 0;
                    $DataCollection = array();
                    $DocenteID=$Params[0]; 
                    $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $GrupoID=$Params[4];
                    
                    $SQL = "SET DATEFIRST 7; 
                            SELECT TOP 24 
                                   Y.IDESTADO, Y.FECHA, Y.HOREX".$Compte." AS HOREJE,
                                   (CASE WHEN Y.FECHA<CONVERT(date,GETDATE(),103) THEN 0 ELSE 1 END) AS PERMISO,
                                   DATEPART(dw,Y.FECHA) AS DIASEMANA, Y.HOR".$Compte." AS INICIO, SUM(Y.".$Compte.") AS NUMHORAS, ISNULL(Z.ID,0) AS AUTH, ISNULL(Z.GRUPO,'') AS GRPEST 
                            FROM   POSTGRADO..COL_PERMISOS_REG_ASISTENCIA AS Z 
                                   RIGHT JOIN (POSTGRADO..COL_PLAN_ANALITICO AS X 
                                   INNER JOIN  POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS Y 
                                   ON X.ID=Y.IDPLANANA AND Y.IDESTADO IN (1,6) AND Y.".$Compte.">0) 
                                   ON Z.IDTIPO='$Compte' AND Z.IDCICLO=X.IDCICLO AND Z.IDMAESTRIA=X.IDMAESTRIA AND Z.IDMATERIA=X.IDMATERIA AND Z.IDGRUPO=X.IDPARALELO AND Z.IDESTADO=4 AND Z.FECHA=Y.FECHA 
			    WHERE  X.IDESTADO=1 AND X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDPARALELO=$GrupoID
                            GROUP BY Y.IDESTADO, Y.FECHA, Y.HOREX".$Compte.", Y.HOR".$Compte.",Z.ID,Z.GRUPO 
                            ORDER BY Y.FECHA DESC"; 
           
                    $resourceID = $this->Works->Query($SQL);
                    while ($filas = $this->Works->NextRecordAssoc($resourceID))
                    {      if ($filas['INICIO']!="00:00")
                           {    $FeHor = strtotime($filas['FECHA']->format('Y-m-d'));
                                $FeHoy = strtotime($this->Dc->getNowDateTime('DATE'));
                                
                                if ($FeHor<=$FeHoy)
                                {   $EsIni = 1;
                                    $filas['DIASEMANA']= strtoupper($this->Dc->getDayName($filas['DIASEMANA']));
                                    if ($filas['HOREJE']=="N")
                                    {   if ($filas['AUTH']!=0) 
                                            $filas['PERMISO']=1;
                                        else if ($filas['PERMISO']==0 && $filas['IDESTADO']==1)
                                             $this->RegInasistenciaDocenteHorPlan($Params,$filas['FECHA']->format('d/m/Y'),$Compte);
                                        
                                        if ($filas['PERMISO']==1 && $filas['IDESTADO']==1)
                                        {   if ($FeHor==$FeHoy)
                                            {   $FeIni = strtotime($filas['FECHA']->format('Y-m-d')." ".$filas['INICIO']);
                                                $HoraF = strtotime($this->Ut->CalculaHoraFinalHorario($filas['INICIO'],$filas['NUMHORAS']));
                                                $TimeF = date("H:i",strtotime('+15 minutes',$HoraF));
                                                $FeFin = strtotime($filas['FECHA']->format('Y-m-d')." ".$TimeF);
                                                $EsIni = $this->CumpleHorarioAcceso($FeIni,$FeFin);
                                            }    
                                        }
                                    }    
                                    else
                                    {   if ($filas['AUTH']!=0 && $filas['GRPEST']=="N")
                                        $filas['PERMISO']=1;
                                    }       
                                        
                                    if ($EsIni>0)
                                    {   //if ($EsIni==2) $filas['PERMISO']=0;
                                        $DataCollection[] = $filas;
                                    }  
                                }    
                                $DataCount++;
                           }
                           else
                           return array(false,"Revise el horario en su planificación (00:00:00).");    
                    }
                    $this->Works->CloseResource($resourceID);
                    
                    if ($DataCount>0)
                    {   if (count($DataCollection)==0)
                            return array(false,"Aún no inicia su horario de clases.");
                        else
                            return array(true,$DataCollection);
                    }
                    return array(false,"No existen horarios para la materia en la planificación actual.");
            }
            
            function CumpleHorarioAcceso($FeIni,$FeFin)
            {       $FeHoy = strtotime($this->Dc->getNowDateTime('DATETIME')); 
                    if ($FeHoy>$FeFin) { return 2; }
                    if ($FeHoy<$FeIni) { return 0; }     
                    return 1;
            }             
            
            function RegInasistenciaDocenteHorPlan($Params,$Fecha,$Compte) 
            {       $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $GrupoID=$Params[4];

                    $SQL = "UPDATE POSTGRADO..COL_PLAN_ANALITICO_DETALLE  
                            SET POSTGRADO..COL_PLAN_ANALITICO_DETALLE.IDESTADO=6 
                            FROM POSTGRADO..COL_PLAN_ANALITICO AS X 
                            WHERE X.IDESTADO=1 AND X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDPARALELO=$GrupoID AND 
                                  POSTGRADO..COL_PLAN_ANALITICO_DETALLE.IDPLANANA=X.ID AND
                                  POSTGRADO..COL_PLAN_ANALITICO_DETALLE.HOREX".$Compte."='N' AND 
                                  POSTGRADO..COL_PLAN_ANALITICO_DETALLE.IDESTADO=1 AND 
                                  POSTGRADO..COL_PLAN_ANALITICO_DETALLE.".$Compte.">0 AND 
                                  POSTGRADO..COL_PLAN_ANALITICO_DETALLE.FECHA='$Fecha'"; 
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,"La inasistencia se registró con exito.");
            }
            
        }
?>