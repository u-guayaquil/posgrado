<?php
        require_once("src/base/DBDAO.php");

        class DAOParalelo extends DBDAO
        {   private static $entityParalelo;
            private $Works;                
            
            public function __construct()
            {      self::$entityParalelo = DBDAO::getConnection();
                   $this->Works = self::$entityParalelo->MyFunction();                            
            }
    
            public function ObtenerParaleloByID($id)
            {          
                    $DSQL = "SELECT TOP 50 CP.ID, CP.PARALELO, CP.CAPACIDAD, CP.IDCHORTE AS IDCOHORTE, MC.COHORTE, CP.FEUSCREA AS FCREACION, CP.IDESTADO
                                FROM POSTGRADO..COL_COHORTE_PARALELO AS CP, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CP.IDCHORTE = MC.ID
                                AND CP.ID = '$id'"; 
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function ObtenerParaleloByidByCohorte($Form)
            {                
                    $id = $Form["id"];
                    $idCohorte = $Form["idCohorte"];

                    $DSQL = "SELECT TOP 50 CP.ID, CP.PARALELO, CP.CAPACIDAD, CP.IDCHORTE AS IDCOHORTE, MC.COHORTE, CP.FEUSCREA AS FCREACION, CP.IDESTADO
                                FROM POSTGRADO..COL_COHORTE_PARALELO AS CP, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CP.IDCHORTE = MC.ID
                                AND CP.ID LIKE '%$id%'
                                AND CP.IDCHORTE = '$idCohorte'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->NextRecordObject($resourceID); 
            }

            public function ObtenerParalelos()
            {       $Registro = array();
                    $resourceID = $this->Works->Query("SELECT CP.ID, CP.PARALELO FROM POSTGRADO..COL_COHORTE_PARALELO CP WHERE CP.IDESTADO = 1");
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }

            public function ObtenerParaleloByDescripcion($PrepareDQL)
            {   $DSQL = "SELECT TOP 50 CP.ID, CP.PARALELO, CP.CAPACIDAD, CP.IDCHORTE AS IDCOHORTE, MC.COHORTE, CP.FEUSCREA AS FCREACION, CP.IDESTADO
                                FROM POSTGRADO..COL_COHORTE_PARALELO AS CP, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CP.IDCHORTE = MC.ID
                                AND CP.PARALELO LIKE '%".$PrepareDQL['texto']."%'";                                                 
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen paralelos definidos."); 
            }
            
            public function ObtenerParaleloByDescripcionByCohorte($Form)
            {                
                    $Text = trim($Form["texto"]);
                    $idCohorte = $Form["id"];

                    $DSQL = "SELECT TOP 50 CP.ID, CP.PARALELO, CP.CAPACIDAD, CP.IDCHORTE AS IDCOHORTE, MC.COHORTE, CP.FEUSCREA AS FCREACION, CP.IDESTADO
                                FROM POSTGRADO..COL_COHORTE_PARALELO AS CP, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CP.IDCHORTE = MC.ID
                                AND CP.PARALELO LIKE '%".$Text."%'
                                AND CP.IDCHORTE = $idCohorte";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen paralelos definidos."); 
            }
            
            public function ObtenerMaestriaByDescripcion($PrepareDQL)
            {                                                  
                    $SQL = "SELECT F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.MAESTRIA, MV.ID AS IDVERSION, MV.VERSION, MV.COHORTES 
                            FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F, POSTGRADO..ADM_MAESTRIA_VERSION MV 
                            WHERE M.IDFACULTAD = F.ID 
                            AND MV.IDMAESTRIA = M.ID
                            AND MV.IDESTADO = 1 
                            AND M.MAESTRIA LIKE '%".$PrepareDQL['texto']."%'
                            ORDER BY F.ID ASC";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Cohortes definidos."); 
            }
            
             public function ObtenerCohorteByDescripcion($PrepareDQL)
            {                                                  
                    $SQL = "SELECT MC.ID, MC.COHORTE, MC.FEINICIO, MC.FEFIN, MC.IDESTADO 
                            FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS MC, POSTGRADO..ADM_MAESTRIA_VERSION AS MV
                            WHERE MC.IDMAESVER = MV.ID 
                            AND MC.COHORTE LIKE '%".$PrepareDQL['texto']."%' 
                            AND MV.ID = ".$PrepareDQL['idvmaestria']."
                            AND MC.IDESTADO = 1
                            ORDER BY MC.ID ASC";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Cohortes definidos."); 
            }
            
            public function ObtenerCohorteByVmaestriaByDescripcion($PrepareDQL)
            {                                                  
                    $SQL = "SELECT MC.ID, MC.COHORTE, MC.FEINICIO, MC.FEFIN, MC.IDESTADO 
                            FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS MC, POSTGRADO..ADM_MAESTRIA_VERSION AS MV
                            WHERE MC.IDMAESVER = MV.ID 
                            AND MC.COHORTE LIKE '%".$PrepareDQL['texto']."%' 
                            AND MV.ID = ".$PrepareDQL['idvmaestria']."
                            AND MC.IDESTADO = 1
                            ORDER BY MC.ID ASC";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Cohortes definidos."); 
            }

            public function ObtenerParaleloEstadoByID($id)
            {          
                    $DSQL = "SELECT TOP 50 CP.ID, CP.PARALELO, CP.CAPACIDAD, CP.IDCHORTE AS IDCOHORTE, MC.COHORTE, CP.FEUSCREA AS FCREACION, CP.IDESTADO
                                FROM POSTGRADO..COL_COHORTE_PARALELO AS CP, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CP.IDCHORTE = MC.ID
                                AND CP.ID = '$id'"; 
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen paralelos definidos."); 
            }
            
            public function ObtenerParaleloEstadoByCohorte($idCohorte)
            {          
                    $DSQL = "SELECT TOP 50 CP.ID, CP.PARALELO, CP.CAPACIDAD, CP.IDCHORTE AS IDCOHORTE, MC.COHORTE, CP.FEUSCREA AS FCREACION, CP.IDESTADO
                                FROM POSTGRADO..COL_COHORTE_PARALELO AS CP, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CP.IDCHORTE = MC.ID
                                AND CP.IDCHORTE = '$idCohorte'";
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen paralelos definidos."); 
            } 
            
            public function ObtenerParaleloEstadoByVMaestria()
            {          
                    $DSQL = "SELECT TOP 50 CP.ID, CP.PARALELO, CP.CAPACIDAD, CP.IDCHORTE AS IDCOHORTE, MC.COHORTE, CP.FEUSCREA AS FCREACION, CP.IDESTADO
                                FROM POSTGRADO..COL_COHORTE_PARALELO AS CP, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CP.IDCHORTE = MC.ID
                                AND CP.IDCHORTE = 0";
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen paralelos definidos."); 
            }
            
            public function InsertaParalelo($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = strtoupper($Form->descripcion);
                    $idCohorte = $Form->idcohorte;
                    $Capacidad = $Form->capacidad;
                    $resourceID = $this->Works->Query("insert into POSTGRADO..COL_COHORTE_PARALELO(IDCHORTE,PARALELO,CAPACIDAD,IDUSCREA,FEUSCREA) OUTPUT INSERTED.ID values('$idCohorte','$Descrp','$Capacidad','".$Modulo->Idusrol."',getDate())");
                    return $this->Works->FieldDataByName($resourceID,'ID');
            }
    
            public function ActualizaParalelo($paralelo)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = utf8_decode(strtoupper($paralelo->descripcion));
                    $IdCohorte = $paralelo->idcohorte;
                    $Capacidad=$paralelo->capacidad;
                    $resourceID = $this->Works->Query("Update POSTGRADO..COL_COHORTE_PARALELO set 
                                                       PARALELO='$Descrp',
                                                       IDCHORTE='$IdCohorte',
                                                       CAPACIDAD='$Capacidad',
                                                       IDUSMODIFICA='".$Modulo->Idusrol."',
                                                       IDESTADO='$paralelo->estado',    
                                                       FEUSMODIFICA=getDate() 
                                                       WHERE ID='$paralelo->id'"); 
                    return intval($paralelo->id);
            }
            
            public function DesactivaParalelo($id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("update POSTGRADO..COL_COHORTE_PARALELO set IDESTADO=0, IDUSMODIFICA='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() where id='$id' ");    
                    return 0;                    
            }

        }
