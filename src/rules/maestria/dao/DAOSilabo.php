<?php
        require_once("src/base/DBDAO.php");
        
        class DAOSilabo extends DBDAO
        {   private static $entitySilabo;
            private $Works;  
            
            public function __construct()
            {      self::$entitySilabo = DBDAO::getConnection();
                   $this->Works = self::$entitySilabo->MyFunction();                
            }

            public function Silabo($Id)
            {       $SQL = "SELECT X.ID,UPPER(Y.DESCRIPCION) AS MAESTRIA,Z.MATERIA,X.REQUISITO,X.ORGCURRICULAR,X.COMPONENTECD,X.COMPONENTECP,X.COMPONENTECA,X.OBGENERAL,X.OBESPECIFICO,X.METODOLOGIA,X.LINEA
                            FROM POSTGRADO..COL_MAT_CONT_GENERAL AS X,POSTGRADO..ADM_MAESTRIA AS Y,POSTGRADO..COL_MATERIA AS Z
                            WHERE X.ID=$Id AND X.IDMATERIA=Z.ID AND Z.IDMAESTRIA=Y.ID"; 
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido Silabo con el ID.");            
            }
            
            public function SilaboLibros($Id,$Tipo)
            {      $SQL = "SELECT * FROM POSTGRADO..COL_MAT_CONT_BIBLIOGRAFIA WHERE IDMATCONT=$Id AND TIPO='$Tipo'"; 
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido Silabo con el ID.");            
            }
            
        }
?>        
