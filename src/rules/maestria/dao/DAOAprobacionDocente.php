<?php
        require_once("src/base/DBDAO.php");        

        class DAOAprobacionDocente extends DBDAO
        {       
                private $Works;
                private static $entityAprobacionDocente;
                
                public function __construct()
                {       self::$entityAprobacionDocente = DBDAO::getConnection();
                        $this->Works = self::$entityAprobacionDocente->MyFunction();                
                }
            
                function ObtenerDocentesSinContratos()
                {   $DSQL = "SELECT D.ID, D.CEDULA, D.NOMBRES, D.APELLIDOS, F.DESCRIPCION AS FACULTAD, D.IDESTADO
                                FROM POSTGRADO..COL_DOCENTE D, POSTGRADO..COL_DOCENTE_FACULTAD DF, POSTGRADO..ADM_FACULTAD F
                                WHERE D.ID = DF.IDDOCENTE
                                AND DF.IDFACULTAD = F.ID
                                AND D.IDESTADO = 2"; 
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen docentes sin contratos por aprobar.");
                }
                
                function ObtenerDocentesSinContratosById($id)
                {   $DSQL = "SELECT D.ID, D.CEDULA, D.NOMBRES, D.APELLIDOS, F.DESCRIPCION AS FACULTAD, D.IDESTADO
                                FROM POSTGRADO..COL_DOCENTE D, POSTGRADO..COL_DOCENTE_FACULTAD DF, POSTGRADO..ADM_FACULTAD F
                                WHERE D.ID = DF.IDDOCENTE
                                AND DF.IDFACULTAD = F.ID
                                AND D.IDESTADO = 2
                                AND D.ID = '$id'"; 
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen docentes sin contratos por aprobar.");
                }
                
                function ActualizaDocenteSinContrato($Form)
                {       $id = $Form['id'];
                        $observacion = $Form['observacion'];
                        $usmodifica = $Form['usmodifica'];
                        $estado = $Form['estado'];
                        $resourceID = $this->Works->Query("UPDATE POSTGRADO..COL_DOCENTE SET OBSERVACION = '$observacion',IDESTADO='$estado',IDUSMODIFICA='$usmodifica',FEUSMODIFICA=getDate() WHERE ID='$id'");
                        return $id;
                }
        }
?>        
