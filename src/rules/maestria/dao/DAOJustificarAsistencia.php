<?php
require_once("src/base/DBDAO.php");

class DAOJustificarAsistencia extends DBDAO
{   
        private static $entityJustificarAsistencia;
        private $Dc;
        private $Works;
        
        public function __construct()
        {       self::$entityJustificarAsistencia = DBDAO::getConnection();
                $this->Works = self::$entityJustificarAsistencia->MyFunction();
                $this->Dc = new DateControl();
        }

        function ObtenerJustificaciones($Params)
        {       $DataCollection = array();
                $DocenteID=$Params[0];    
                $CicloID=$Params[1];
                $MaestriaID=$Params[2];
                $MateriaID=$Params[3];
                $ParaleloID=$Params[4];
                $SQL = "SET DATEFIRST 7; 
                        SELECT DISTINCT Z.ID, DATEPART(dw,Y.FECHA) AS DIASEMANA ,Z.FECHA, Y.HORCD AS INICIO, Z.OBSERVACION, Z.IDESTADO 
                        FROM   POSTGRADO..COL_PERMISOS_REG_ASISTENCIA AS Z,
                                        POSTGRADO..COL_PLAN_ANALITICO AS X, 
                                        POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS Y 
                        WHERE X.ID=Y.IDPLANANA  AND Z.IDCICLO=X.IDCICLO AND Z.IDMAESTRIA=X.IDMAESTRIA 
                        AND Z.IDMATERIA=X.IDMATERIA AND Z.IDGRUPO=X.IDPARALELO AND Z.FECHA=Y.FECHA 
                        AND (Z.IDESTADO = 4) AND (Z.GRUPO = 'N') AND (Z.IDTIPO = 'CD') 
                        AND Z.IDDOCENTE = $DocenteID AND Z.IDMAESTRIA = $MaestriaID AND Z.IDCICLO = $CicloID
                        AND Z.IDMATERIA = $MateriaID AND IDGRUPO = $ParaleloID 
                        AND Y.HORCD != '00:00' AND Y.IDESTADO = 1
                        ORDER BY FECHA DESC";
                $resourceID = $this->Works->Query($SQL);
                while ($filas = $this->Works->NextRecordAssoc($resourceID))
                {       $filas['DIASEMANA']= strtoupper($this->Dc->getDayName($filas['DIASEMANA']));   
                        $DataCollection[] = $filas;
                }
                if (count($DataCollection)==0)
                        return array(false,"No hay existe registros de justificaciones por procesar.");
                else
                        return array(true,$DataCollection);
        }
        
        function BuscaJustificadoExistente($Form)
        {       $idDocente = $Form->iddocente;
                $idMaestria = $Form->idmaestria;
                $idCiclo = $Form->idciclo;
                $idMateria = $Form->idmateria;
                $idGrupo = $Form->idgrupo;
                $Fecha = $Form->Fecha;

                $SQL = "SELECT ID,FECHA,OBSERVACION,IDESTADO FROM POSTGRADO..COL_PERMISOS_REG_ASISTENCIA
                        WHERE (IDESTADO = 4) AND (GRUPO = 'N') AND (IDTIPO = 'CD') 
                        AND IDDOCENTE = $idDocente AND IDMAESTRIA = $idMaestria AND IDCICLO = $idCiclo
                        AND IDMATERIA = $idMateria AND IDGRUPO = $idGrupo AND FECHA = '$Fecha'";

                $resourceID = $this->Works->Query($SQL);
                return $this->Works->NextRecordObject($resourceID); 
        }

        function InsertaJustificarAsistenciaCabecera($Form,$Observacion)
        {       $Modulo = json_decode(Session::getValue('Sesion'));
                $idDocente = $Form->iddocente;
                $idMaestria = $Form->idmaestria;
                $idCiclo = $Form->idciclo;
                $idMateria = $Form->idmateria;
                $idGrupo = $Form->idgrupo;
                $Fecha = $Form->Fecha;
                $SQL = "INSERT INTO POSTGRADO..COL_PERMISOS_REG_ASISTENCIA(
                        IDDOCENTE,IDTIPO,GRUPO,IDCICLO,IDMAESTRIA,IDMATERIA,IDGRUPO,FECHA,OBSERVACION,IDUSCREA,FEUSCREA,IDESTADO) 
                        OUTPUT INSERTED.ID values('$idDocente','CD','N','$idCiclo','$idMaestria','$idMateria',
                        '$idGrupo','$Fecha','$Observacion','".$Modulo->Idusrol."',getDate(),4)";
                $resourceID = $this->Works->Query($SQL);
                $id = $this->Works->FieldDataByName($resourceID,'ID');
                return array(true,$id);
        }

        function ActualizaJustificarAsistenciaCabecera($Form)
        {       $Modulo = json_decode(Session::getValue('Sesion'));
                $idCabecera = $Form->idCabecera;
                $Fecha = $Form->Fecha;
                $Modulo = json_decode(Session::getValue('Sesion'));
                $resourceID = $this->Works->Query("UPDATE POSTGRADO..COL_PERMISOS_REG_ASISTENCIA set 
                                                        Fecha='$Fecha',
                                                        IDUSMODIFICA='".$Modulo->Idusrol."',   
                                                        FEUSMODIFICA=getDate() 
                                                        WHERE ID='$idCabecera'"); 
                return array(true,$idCabecera);
        }

        function DesactivaJustificadoCabecera($idCabecera){
                $Modulo = json_decode(Session::getValue('Sesion'));
                $resourceID = $this->Works->Query("UPDATE POSTGRADO..COL_PERMISOS_REG_ASISTENCIA set 
                                                                IDESTADO=0, 
                                                                IDUSMODIFICA='".$Modulo->Idusrol."', 
                                                                FEUSMODIFICA=getDate() 
                                                        WHERE id='$idCabecera' ");    
                return true;
        }

        function ConsultarEstudiantes($Params)
        {       $MaestriaID=$Params->idmaestria;
                $CohorteID=$Params->idcohorte;
                $Paralelo=$Params->txgrupo;
                $idCabecera=$Params->idCabecera;
                $SQL = "SELECT DISTINCT E.CEDULA, E.APELLIDOS + ' ' + E.NOMBRES AS ESTUDIANTE,
                                (CASE 
                                WHEN EMC.FEUSMODIFICA = CAST('1900-01-01 00:00:00.000' AS	DATETIME)
                                THEN ''
                                ELSE UPPER(CONVERT(VARCHAR, EMC.FEUSMODIFICA, 6))
                                END)
                                + ' ' + EMC.OBSERVACION AS OBSERVACION, EMC.IDESTADO AS ESTADO, $idCabecera as CABECERA,
                                        CASE
                                        WHEN LEN(IDPERMISO) > 0 
                                        THEN 1
                                        ELSE 0
                                        END AS PERMISO
                        FROM    POSTGRADO..COL_ESTUDIANTE E
                                JOIN  POSTGRADO..COL_ESTUDIANTE_MATERIA EM ON EM.IDESTUDIANTE = E.ID
                                JOIN  POSTGRADO..COL_ESTUDIANTE_COHORTE EC ON EC.IDESTUDIANTE = E.ID
                                JOIN  POSTGRADO..ADM_MAESTRIA_COHORTE MC ON MC.ID = EC.IDCOHORTE
                                JOIN  POSTGRADO..COL_COHORTE_PARALELO CP ON EM.IDPARALELO = CP.ID
                                JOIN  POSTGRADO..COL_ESTUDIANTE_COHORTE_MATRICULA EMC ON EC.ID = EMC.IDESTUDCOHORTE
                                LEFT JOIN POSTGRADO..COL_PERMISOS_REG_ASISTENCIA_DT PRAD ON PRAD.CIDESTUDIANTE = E.CEDULA AND PRAD.IDPERMISO = $idCabecera
                        WHERE	EM.IDMAESTRIA = $MaestriaID
                        AND     CP.PARALELO = '$Paralelo'
                        AND     MC.ID = $CohorteID
                        ORDER BY E.APELLIDOS + ' ' + E.NOMBRES ";
                $resourceID = $this->Works->Query($SQL);
                return $this->Works->FillDataRecordAssoc($resourceID,"No existen definido estudiantes.");
        }

        function AsignarEstudiantes($CedulaEstudiante,$IdCabecera)
        {       $SQL = "INSERT INTO POSTGRADO..COL_PERMISOS_REG_ASISTENCIA_DT(IDPERMISO,CIDESTUDIANTE) 
                        values($IdCabecera,'$CedulaEstudiante')";
                $this->Works->Query($SQL);
                return true;
        }

        function DesasignarEstudiantes($CedulaEstudiante)
        {       $SQL = "DELETE FROM POSTGRADO..COL_PERMISOS_REG_ASISTENCIA_DT
                        WHERE CIDESTUDIANTE='$CedulaEstudiante'";
                $this->Works->Query($SQL);
                return true;
        }
}    
