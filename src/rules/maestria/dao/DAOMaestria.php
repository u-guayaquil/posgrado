<?php
        require_once("src/base/DBDAO.php");
        
        class DAOMaestria extends DBDAO
        {   private static $entityMaestria;
            private $Works;  
            
            public function __construct()
            {      self::$entityMaestria = DBDAO::getConnection();
                   $this->Works = self::$entityMaestria->MyFunction();                
            }

            public function ObtenerMaestriaByID($Id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidas ");            
            }

            public function ObtenerMaestriaVersionByFacultad($prepareDQL) //ok1
            {       $CD1 = ""; $CD2 = "";
                    if (key_exists('facultad', $prepareDQL))
                    {   $FID = implode(',',$prepareDQL['facultad']);
                        $CD1 = " Y.ID IN ($FID) AND ";
                    }
                    if (key_exists('texto', $prepareDQL))
                    {   $CD2 = " AND (X.DESCRIPCION LIKE '%".$prepareDQL['texto']."%') ";
                    }
            
                    $SQL = "SELECT TOP 50 X.ID AS IDM,UPPER(X.DESCRIPCION) AS MAESTRIA,ISNULL(W.ID,0) AS IDV,ISNULL(W.VERSION,'') AS TXVERSION,Y.ID AS IDFACULTAD,UPPER(Y.DESCRIPCION) AS FACULTAD 
                            FROM POSTGRADO..ADM_MAESTRIA_VERSION AS W RIGHT JOIN 
                            (POSTGRADO..ADM_MAESTRIA AS X INNER JOIN POSTGRADO..ADM_FACULTAD AS Y ON ".$CD1." Y.ID=X.IDFACULTAD) 
                            ON W.IDMAESTRIA = X.ID AND W.IDESTADO=1
                            WHERE  X.IDESTADO=1 ".$CD2." 
                            ORDER BY Y.DESCRIPCION, X.MAESTRIA ASC ";
                    
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen maestrias con su versión definidas.");
            }

            public function ObtenerMaestriaByidByFacultad($Form)
            {                
                    $id = $Form["id"];
                    $idFacultad = $Form["id_facultad"];

                    $DSQL = "SELECT TOP 50 M.ID, M.MAESTRIA, M.FECREA, M.IDESTADO, F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.DESCRIPCION, M.VALADMISION
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F
                                WHERE M.IDFACULTAD = F.ID
                                AND M.ID LIKE '%$id%'
                                AND M.IDFACULTAD = $idFacultad";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->NextRecordObject($resourceID); 
            }

            public function ObtenerMaestria()
            {       $Registro = array();
                    $resourceID = $this->Works->Query("SELECT M.ID, M.MAESTRIA FROM POSTGRADO..ADM_MAESTRIA AS M WHERE M.IDESTADO = 1");
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }

            public function ObtenerMaestriaByDescripcion($PrepareDQL)
            {                                                  
                    $DSQL = "SELECT TOP 50 M.ID, M.MAESTRIA, M.FECREA, M.IDESTADO, F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.DESCRIPCION, M.VALADMISION
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F
                                WHERE M.IDFACULTAD = F.ID
                                AND M.MAESTRIA LIKE '%".$PrepareDQL['texto']."%'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen maestrias definidas."); 
            }
            
            public function ObtenerMaestriaByDescripcionByFacultad($Form)
            {                
                    $Text = trim($Form["texto"]);
                    $idFacultad = $Form["id"];

                    $DSQL = "SELECT TOP 50 M.ID, M.MAESTRIA, M.FECREA, M.IDESTADO, F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.DESCRIPCION, M.VALADMISION
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F
                                WHERE M.IDFACULTAD = F.ID
                                AND M.MAESTRIA LIKE '%".$Text."%'
                                AND M.IDFACULTAD = $idFacultad";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen maestrias definidas."); 
            }
            
            public function ObtenerFacultadByDescripcion($PrepareDQL)
            {                                        
                    $DSQL = "SELECT F.ID, F.DESCRIPCION AS FACULTAD
                                FROM POSTGRADO..ADM_FACULTAD AS F
                                WHERE F.DESCRIPCION LIKE '%".$PrepareDQL['texto']."%'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen maestrias definidas."); 
            }

            public function ObtenerMaestriaEstadoByID($id)
            {                  
                    $DSQL = "SELECT TOP 50 M.ID, M.MAESTRIA, M.FECREA, M.IDESTADO, F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.DESCRIPCION, M.VALADMISION
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F
                                WHERE M.IDFACULTAD = F.ID
                                AND M.ID = '$id'"; 
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen maestrias definidas."); 
            }
            
            public function ObtenerMaestriaEstadoByFacultad($idFacultad)
            {          
                    $DSQL = "SELECT TOP 50 M.ID, M.MAESTRIA, M.FECREA, M.IDESTADO, F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.DESCRIPCION, M.VALADMISION
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F
                                WHERE M.IDFACULTAD = F.ID
                                AND M.IDFACULTAD = $idFacultad";

                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen maestrias definidas."); 
            }
            
            public function InsertaMaestria($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Maestria = strtoupper($Form->descripcion);
                    $Descrp = strtoupper($Form->maestriadesc);
                    $idFacultad = str_replace('00', '', $Form->idfacultad);
                    $ValAdmision = $Form->valadmision;
                    $SQL = "insert into POSTGRADO..ADM_MAESTRIA(IDFACULTAD,MAESTRIA,DESCRIPCION,VALADMISION,USCREA,FECREA) 
                                OUTPUT INSERTED.ID values('$idFacultad','$Maestria','$Descrp','$ValAdmision',
                                '".$Modulo->Idusrol."',getDate())";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FieldDataByName($resourceID,'ID');
            }
    
            public function ActualizaMaestria($Maestria)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Mtria = utf8_decode(strtoupper($Maestria->descripcion));
                    $Descrp = utf8_decode(strtoupper($Maestria->maestriadesc));
                    $idFacultad = str_replace('00', '', $Maestria->idfacultad);
                    $ValAdmision = $Maestria->valadmision;
                    $resourceID = $this->Works->Query("Update POSTGRADO..ADM_MAESTRIA set 
                                                       MAESTRIA='$Mtria',
                                                       DESCRIPCION='$Descrp',
                                                       IDFACULTAD='$idFacultad',
                                                       VALADMISION='$ValAdmision',
                                                       USMODIFICA='".$Modulo->Idusrol."',
                                                       IDESTADO='$Maestria->estado',    
                                                       FEMODIFICA=getDate() 
                                                       WHERE ID='$Maestria->id'"); 
            return intval($Maestria->id);
            }
            
            public function DesactivaMaestria($id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("update POSTGRADO..ADM_MAESTRIA set IDESTADO=0, USMODIFICA='".$Modulo->Idusrol."', FEMODIFICA=getDate() where id='$id' ");    
                    return 0;                    
            }
            
        }
?>        
