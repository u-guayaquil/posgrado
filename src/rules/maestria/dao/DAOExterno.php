<?php
        require_once("src/base/DBDAO.php");
        
        class DAOExterno extends DBDAO
        {   private static $entityExterno;
            private $Works;  
            
            public function __construct()
            {      self::$entityExterno = DBDAO::getConnection();
                   $this->Works = self::$entityExterno->MyFunction();                
            }
                   
            public function CreaRolGestion($Usuario,$Rol)
            {      $Utils = new Utils(); 
                   $Modulo = json_decode(Session::getValue('Sesion')); 
                   
                   $RIP = $Utils->retornaRemoteIPAddress();
                   $SQL = "Insert Into BdCert..SEG_USUARIO_ROL(IDMODROL,IDUSUARIO,IDUSCREA,FEUSCREA,IPCREACION) ";
                   $SQL.= "OUTPUT Inserted.ID Select X.ID,'$Usuario','$Modulo->Idusrol',getDate(),'$RIP' From BdCert..SEG_MODULO_ROL AS X Where X.IDROL='$Rol' AND X.IDMODULO='$Modulo->Idmodulo' ";
                   print_r($SQL);
                   $resourceID = $this->Works->Query($SQL);
                   if ($resourceID)
                       return array(true,$this->Works->FieldDataByName($resourceID,'ID'));    
                   else
                   return array(false,"No se puedo crear el rol para gestión administrativa.");
            }
            
            public function Inserta($Forma,$MsGston) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    
                    $SQL = "INSERT INTO POSTGRADO..COL_EXTERNO(CEDULA,NOMBRES,APELLIDOS,DIRECCION,EMAIL,TELEFONO,IDUSCREA,FEUSCREA,IDESTADO) 
                            OUTPUT Inserted.ID 
                            VALUES('$Forma->idExterno',upper('$Forma->nombre'),upper('$Forma->apellido'),upper('$Forma->direccion'),
                            '$Forma->email','$Forma->telefono','$Modulo->Idusrol',getDate(),'$Forma->estado')"; 
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$this->Works->FieldDataByName($resourceID,'ID'),$MsGston);
            }
            
            public function Actualiza($Forma,$MsGston) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $SQL = "UPDATE POSTGRADO..COL_EXTERNO
                            SET IDROLUSER='$Forma->idroluser',IDGESTION='$Forma->idgestion',NOMBRES='$Forma->nombre',APELLIDOS='$Forma->apellido',DIRECCION='$Forma->direccion',EMAIL='$Forma->email',TELEFONO='$Forma->telefono',IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate(),IDESTADO='$Forma->estado' 
                            WHERE CEDULA='$Forma->idExterno'"; 
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$Forma->idExterno,$MsGston);
            }

            public function Inactiva($Cedula) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    if ($this->NumeroFacultadByExterno($Cedula)==0)
                    {   $SQL = "UPDATE POSTGRADO..COL_EXTERNO 
                                        SET IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate(),IDESTADO=0 
                                WHERE CEDULA='$Cedula'; ";
                        $resourceID = $this->Works->Query($SQL);
                        return array(true,0);
                    }
                    return array(false,"El Externo tiene asociado facultad(s). No se permite la desactivación.");
            }
            
            public function NumeroFacultadByExterno($Cedula) //Ok
            {      $SQL = "SELECT COUNT(X.IDFACULTAD) as NUM FROM POSTGRADO..COL_EXTERNO_FACULTAD AS X, POSTGRADO..COL_EXTERNO AS Y WHERE Y.CEDULA='$Cedula' AND X.IDEXTERNO=Y.ID"; 
                   
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FieldDataByName($resourceID,'NUM');
            }

            private function TieneExternoFacultad($Externo,$Facultad)
            {      $SQL = "SELECT COUNT(X.IDCICLO) AS NUM FROM POSTGRADO..COL_PLAN_ANALITICO AS X, POSTGRADO..ADM_MAESTRIA AS Y WHERE X.IDEXTERNO=$Externo AND X.IDMAESTRIA=Y.ID AND Y.IDFACULTAD='$Facultad'"; 
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FieldDataByName($resourceID,'NUM'); 
            }
            
            public function ObtenerExterno($prepareDQL) //Ok
            {       $Txd = ""; 
                    $Fac = "";
                    $Doc = "";
                    
                    if (array_key_exists('externo', $prepareDQL))
                    {   $ExternoID = $prepareDQL['externo'];
                        $Doc = " AND X.CEDULA='$ExternoID' ";
                    }
                    if (array_key_exists('facultad', $prepareDQL))
                    {   $Fid = implode(',',$prepareDQL['facultad']);
                        $Fac = " AND Y.IDFACULTAD IN ($Fid) ";
                    } 
                    if (array_key_exists('texto', $prepareDQL))
                    {   $Txd = " AND (X.APELLIDOS+' '+X.NOMBRES LIKE '%".$prepareDQL['texto']."%') ";
                    }                    
                    $SQL = "SELECT TOP 20 X.ID,X.CEDULA,X.NOMBRES,X.APELLIDOS,X.EMAIL
                            FROM   POSTGRADO..COL_EXTERNO AS X,
                                   POSTGRADO..COL_EXTERNO_FACULTAD AS Y
                            WHERE  X.IDESTADO=1 AND X.ID=Y.IDEXTERNO ".$Fac.$Doc.$Txd; 

                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos Externos para la maestria.");
            }
            
            public function ObtenerByCedula($Cedula) 
            {       $SQL = "SELECT ID AS ID,CEDULA,NOMBRES AS NMB,APELLIDOS AS APLL,DIRECCION AS DIR,EMAIL AS EML,
                                        TELEFONO AS FONO,IDESTADO AS ESTADO, IDGESTION AS GESTION
                                FROM POSTGRADO..COL_EXTERNO WHERE CEDULA='$Cedula' and IDESTADO IN (0,1,2) ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos externos con la identificación ingresada."); ;
            }
            
            public function ObtenerPorConfirmarByCedula($Cedula)
            {      $SQL = "SELECT ID AS IDD,CEDULA,NOMBRES AS NMB,APELLIDOS AS APLL,DIRECCION AS DIR,EMAIL AS EML,TELEFONO AS FONO,IDESTADO AS ESTADO, IDGESTION AS GESTION, IDROLUSER AS ROLUSER FROM POSTGRADO..COL_EXTERNO WHERE CEDULA='$Cedula' and IDESTADO=2 ";
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos externos con la identificación ingresada."); 
            }
            
            public function ObtenerExternoPlanificacion($prepareDQL) //OK 
            {       $SQL1=""; $SQL2=""; $SQL3="";

                    if (array_key_exists("externo", $prepareDQL))
                    {   $ExternoID = $prepareDQL['externo'];
                        $SQL3 = " AND Y.CEDULA='$ExternoID' ";
                    }
                    if (array_key_exists("facultad", $prepareDQL))
                    {   if (count($prepareDQL['facultad'])>0)
                        {   $FacultadID = implode(',',$prepareDQL['facultad']);
                            $SQL1 = " AND J.IDFACULTAD IN ($FacultadID) ";
                        }
                    }
                    if (array_key_exists("texto", $prepareDQL))
                    {   $ExternoTX = $prepareDQL['texto'];
                        $SQL2 = " AND (Y.APELLIDOS+' '+Y.NOMBRES LIKE '%".$ExternoTX."%')";
                    }
                    $SQL = "SELECT DISTINCT Y.ID,Y.CEDULA,Y.NOMBRES,Y.APELLIDOS 
                            FROM   POSTGRADO..COL_PLAN_ANALITICO AS X,
                                    POSTGRADO..COL_EXTERNO AS Y,
                                    POSTGRADO..COL_CICLOS AS Z,
				                    POSTGRADO..ADM_MAESTRIA AS J
                            WHERE   X.IDEXTERNO=Y.ID AND X.IDESTADO=1 AND X.IDCICLO=Z.ID AND Z.IDESTADO=1 AND J.ID=X.IDMAESTRIA".$SQL1.$SQL2.$SQL3;        
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Externo definidos para el ciclo vigente.");
            }

            public function ObtenerExternoByTX($prepareDQL) //OK 
            {       $SQL1 = "";
            
                    if (array_key_exists("texto", $prepareDQL))
                    {   $ExternoTX = $prepareDQL['texto'];
                        $SQL1 = " WHERE (X.APELLIDOS+' '+X.NOMBRES LIKE '%".$ExternoTX."%') ";
                    }

                    $SQL = "SELECT TOP 10 X.ID AS ID, X.CEDULA, X.NOMBRES AS NMB, X.APELLIDOS AS APLL, X.EMAIL AS EML, 
                                        X.DIRECCION AS DIR, X.TELEFONO AS FONO, X.IDESTADO AS ESTADO, X.IDGESTION AS GESTION
                                FROM POSTGRADO..COL_EXTERNO AS X ".$SQL1;
                                               
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Externo definidos.");
            }                        
        }
?>        
