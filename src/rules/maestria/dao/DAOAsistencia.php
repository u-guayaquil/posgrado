<?php
        require_once("src/base/DBDAO.php");

        class DAOAsistencia extends DBDAO
        {   
            private static $entityAsistencia;
            private $Works;
              
            public function __construct()
            {       self::$entityAsistencia = DBDAO::getConnection();
                    $this->Works = self::$entityAsistencia->MyFunction();
            }
              
            function ObtenerCurso($Params)
            {       $MDL = json_decode(Session::getValue('Sesion'));
                
                    $DocenteID=$Params[0];
                    $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $ParaleloID=$Params[4]; 

                    $SQL = "SELECT COUNT(IDMATERIA) AS NUMEST FROM COL_ESTUDIANTE_MATERIA WHERE IDCICLO=$CicloID AND IDMAESTRIA=$MaestriaID AND IDMATERIA=$MateriaID AND IDPARALELO=$ParaleloID AND IDESTADO=1 ";    
                    $resourceID = $this->Works->Query($SQL);
                    if ($this->Works->FieldDataByName($resourceID,'NUMEST')==0)
                    {   
                        $SQL="SELECT IDMATERIA FROM COL_ESTUDIANTE_MATERIA WHERE ID IN (SELECT MAX(ID) FROM COL_ESTUDIANTE_MATERIA WHERE IDCICLO=$CicloID AND IDMAESTRIA=$MaestriaID AND IDPARALELO=$ParaleloID AND IDESTADO=1)";
                        $rID= $this->Works->Query($SQL);
                        while($filas = $this->Works->NextRecordAssoc($rID))
                        {     $IDM = $filas['IDMATERIA'];
                              $IMD = $MDL->Idusrol;
                             
                              $SQL = "INSERT INTO COL_ESTUDIANTE_MATERIA (IDMATRICULA, IDCICLO, IDESTUDIANTE, IDMAESTRIA, IDMATERIA, IDPARALELO, IDDOCENTE, NOTA, ASISTENCIA, ESTACADEMICO, IDUSCREA, FEUSCREA,IDESTADO) 
                                      SELECT IDMATRICULA, IDCICLO, IDESTUDIANTE, IDMAESTRIA, '$MateriaID', IDPARALELO, '$DocenteID', NOTA, ASISTENCIA, ESTACADEMICO, '$IMD', GETDATE(), 1 
                                      FROM COL_ESTUDIANTE_MATERIA 
                                      WHERE IDCICLO=$CicloID AND IDMAESTRIA=$MaestriaID AND IDPARALELO=$ParaleloID AND IDESTADO=1 AND IDMATERIA=$IDM ";
                              $resourceID = $this->Works->Query($SQL);
                        }

                    }
            }

            function ObtenerAsistencia($Params,$Horario)
            {       $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $ParaleloID=$Params[4];
                    $FechaAsist=$Params[5];
                    
                    $SQLAD = "";
                    if ($Params[9]=="N")
                    {   $SQLAD = " AND V.CEDULA IN (SELECT CIDESTUDIANTE 
                                  FROM POSTGRADO..COL_PERMISOS_REG_ASISTENCIA_DT AS Y,POSTGRADO..COL_PERMISOS_REG_ASISTENCIA AS X
                                  WHERE X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDGRUPO=$ParaleloID AND X.FECHA='$FechaAsist' AND X.ID=Y.IDPERMISO AND X.GRUPO='N' AND X.IDESTADO=4) ";
                    }

                    $this->ObtenerCurso($Params);

                    $DataCount = 0;
                    $DataCollection = array();
                    $SQL = "SELECT ISNULL(Y.ID,0) AS IDASISTENCIA, X.ID AS IDESTUDMATE, ISNULL(Y.FECHASISTENCIA,'$FechaAsist') AS FECHA, 
                                   V.APELLIDOS+' '+V.NOMBRES AS ESTUDIANTE,ISNULL(Y.ASISTENCIA,'') AS ASISTENCIA,ISNULL(Y.IDHORHORAS,0) AS IDHORARIO
                            FROM   POSTGRADO..COL_ESTUDIANTE AS V
                                   INNER JOIN (POSTGRADO..COL_ESTUDIANTE_COHORTE_MATRICULA AS W 
                                   INNER JOIN (POSTGRADO..COL_ESTUDIANTE_MATERIA AS X 
                                   LEFT JOIN   POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA AS Y 
                                   ON X.ID=Y.IDESTUDIANTEMATERIA AND Y.FECHASISTENCIA='$FechaAsist') 
                                   ON W.IDESTADO<>0 AND W.ID=X.IDMATRICULA) 
                                   ON V.ID=X.IDESTUDIANTE
                            WHERE X.IDESTADO=1 AND X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDPARALELO=$ParaleloID ".$SQLAD." 
                            ORDER BY V.APELLIDOS+' '+V.NOMBRES";                    
                    //echo $SQL;

                    $resourceID = $this->Works->Query($SQL);
                    while($filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $DataCount++;
                          if ($filas['IDHORARIO']==0)
                          {   $filas['IDHORARIO']=$Horario[1];
                          }
                          $DataCollection[] = $filas;
                    }
                    $this->Works->CloseResource($resourceID);
                    return  array($Horario[2],array(($DataCount>0),($DataCount>0 ? $DataCollection:"No han definido estudiantes para la materia y grupo.")));        
            }
            
            function RegistraAsistencia($Form)
            {       $DataCollection = array();
                    $Modulo = json_decode(Session::getValue('Sesion')); 
                    foreach($Form as $Row)    
                    {       if ($Row[1]==0)
                            {   $Cll = str_pad($Row[0],2,'0',STR_PAD_LEFT);
                                $SQL="Insert Into POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA
                                      (IDESTUDIANTEMATERIA,FECHASISTENCIA,IDHORHORAS,ASISTENCIA,EFECTIVA,JUSTIFICADA,FALTA,IDUSCREA,FEUSCREA)
                                      OUTPUT Inserted.ID
                                      Values('$Row[2]','$Row[3]','$Row[4]','$Row[5]','$Row[6]','$Row[7]','$Row[8]','$Modulo->Idusrol',GetDate())";
                                      $resourceID = $this->Works->Query($SQL); 
                                      $DataCollection[] = array("TD_".$Cll."_000",$this->Works->FieldDataByName($resourceID,'ID'));
                            }
                            else
                            {   $SQL="Update POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA 
                                      Set IDHORHORAS='$Row[4]',ASISTENCIA='$Row[5]',EFECTIVA='$Row[6]',JUSTIFICADA='$Row[7]',FALTA='$Row[8]',IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=GetDate() 
                                      Where ID='$Row[1]'";
                                $resourceID = $this->Works->Query($SQL);
                            }
                    }
                    return array(true,$DataCollection);
            }    
            
            function ObtenerPermisoAsistencia($Params,$Compte)
            {       $DocenteID=$Params[0];
                    $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $GrupoID=$Params[4];
                    $FechaID=$Params[5];
                    
                    $SQL = "SELECT ISNULL(X.ID,0) AS ID,ISNULL(X.OBSERVACION,'') AS MOTIVO,ISNULL(X.FEUSCREA,GETDATE()) AS SOLICITUD, 
                           (SELECT W.PARALELO FROM POSTGRADO..COL_COHORTE_PARALELO AS W WHERE W.ID=$GrupoID) AS PARALELO,Y.MATERIA,ISNULL(X.FECHA,'$FechaID') AS FECHA
                            FROM 
                            POSTGRADO..COL_MATERIA AS Y LEFT JOIN
                            POSTGRADO..COL_PERMISOS_REG_ASISTENCIA AS X                            
                            ON X.IDESTADO=1 AND X.IDTIPO='$Compte' AND X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDGRUPO=$GrupoID AND X.FECHA='$FechaID'
                            WHERE Y.ID=$MateriaID ";
                    //echo $SQL;
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen permisos para registro de asistencia.");
            }
            
            function RegistraPermisoAsistencia($Params,$Compte)
            {       $Utils = new Utils();
                    $Modulo = json_decode(Session::getValue('Sesion')); 
                    $DocenteID=$Params[0];
                    $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $GrupoID=$Params[4];
                    $FechaID=$Params[5];
                    $MotivoTX= utf8_decode($Utils->ConvertTildesToUpper(Trim($Params[6])));
                    $SQL = "INSERT INTO POSTGRADO..COL_PERMISOS_REG_ASISTENCIA(IDDOCENTE,IDTIPO,IDCICLO,IDMAESTRIA,IDMATERIA,IDGRUPO,FECHA,OBSERVACION,IDUSCREA,FEUSCREA,IDESTADO) ";
                    $SQL.= "OUTPUT Inserted.ID VALUES ($DocenteID,'$Compte',$CicloID,$MaestriaID,$MateriaID,$GrupoID,'$FechaID','$MotivoTX','$Modulo->Idusrol',getDate(),1)";
                    $resourceID = $this->Works->Query($SQL);
                    if ($resourceID)
                        return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
                    else
                        return array(false,"No se pudo tramitar el permiso.");
            }
            
            function EjecutaPermisoAsistencia($Params,$Compte)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $DocenteID=$Params[0];
                    $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $GrupoID=$Params[4];
                    $FechaID=$Params[5];
                    $SQL = "UPDATE POSTGRADO..COL_PERMISOS_REG_ASISTENCIA SET IDESTADO=9,IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate()  
                            WHERE IDTIPO='$Compte' AND IDCICLO=$CicloID AND IDMAESTRIA=$MaestriaID AND IDMATERIA=$MateriaID AND IDGRUPO=$GrupoID AND FECHA='$FechaID'";
                    $resourceID = $this->Works->Query($SQL);
                    return true;
            }
            
            function ObtenerTotalAsistenciaEjecutada($Compte,$Params)
            {       $CicloID = $Params[1];
                    $MaestriaID = $Params[2];
                    $MateriaID = $Params[3];
                    $ParaleloID = $Params[4];
                    
                    $SQL = "SELECT ISNULL(SUM(Y.".$Compte."),0) TASIST  
                            FROM   POSTGRADO..COL_PLAN_ANALITICO AS X, POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS Y 
                            WHERE  X.ID=Y.IDPLANANA AND X.IDESTADO=1 AND Y.IDESTADO=1 AND Y.HOREX".$Compte."='S' AND 
                                   X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDPARALELO=$ParaleloID";
                    
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FieldDataByName($resourceID,'TASIST');
            }
            
        }    
