<?php

    require_once("src/base/DBDAO.php");

    class DAORptEstudiantes extends DBDAO
    {       private static $entityEstudiante;
            private $Works;

            public function __construct()
            {       self::$entityEstudiante = DBDAO::getConnection();
                    $this->Works = self::$entityEstudiante->MyFunction();
            }

            function ConsultarEstudiantes()
            {       $SQL = "SELECT DISTINCT F.DESCRIPCION AS FACULTAD,M.MAESTRIA, MC.COHORTE,E.CEDULA, E.APELLIDOS, E.NOMBRES,
                    (CASE
                        WHEN EMC.IDESTADO = 1
                        THEN 'ACTIVO'
                        ELSE 'RETIRADO'
                        END) AS ESTADO
                FROM    POSTGRADO..COL_ESTUDIANTE E,
                        POSTGRADO..COL_ESTUDIANTE_MATERIA EM,
                        POSTGRADO..COL_ESTUDIANTE_COHORTE EC,
                        POSTGRADO..ADM_MAESTRIA_COHORTE MC,
                        POSTGRADO..COL_COHORTE_PARALELO CP,
                        POSTGRADO..COL_ESTUDIANTE_COHORTE_MATRICULA EMC,
                                POSTGRADO..ADM_MAESTRIA_VERSION MV,
                                POSTGRADO..ADM_MAESTRIA M,
                                POSTGRADO..ADM_FACULTAD F
                WHERE   EC.IDCOHORTE = MC.ID
                AND     EC.IDESTUDIANTE = E.ID
                AND     EM.IDESTUDIANTE = E.ID
                AND		CP.IDCHORTE = MC.ID
                AND     EM.IDPARALELO = CP.ID
                AND     EMC.IDESTUDCOHORTE = EC.ID
                AND		MC.IDMAESVER = MV.ID
                AND		MV.IDMAESTRIA = M.ID
                AND		M.IDFACULTAD = F.ID
                ORDER BY E.APELLIDOS ASC";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definido estudiantes.");
            }
    }
?>