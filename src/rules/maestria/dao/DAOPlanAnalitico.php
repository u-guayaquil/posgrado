<?php
        require_once("src/base/DBDAO.php");

        class DAOPlanAnalitico extends DBDAO
        {
            private static $entityPlanAnalitico;
            private $Works;

            public function __construct()
            {       self::$entityPlanAnalitico = DBDAO::getConnection();
                    $this->Works = self::$entityPlanAnalitico->MyFunction();
            }

            function ObtenerPlanAnalitico($Params) //OK
            {       $SQL = "SELECT X.ID,X.IDDOCENTE,Y.NOMBRES,Y.APELLIDOS,X.IDESTADO
                            FROM POSTGRADO..COL_PLAN_ANALITICO AS X,POSTGRADO..COL_DOCENTE AS Y
                            WHERE X.IDESTADO=1 AND IDMATCONT='".$Params[0]."' AND IDCICLO='".$Params[1]."' AND IDPARALELO='".$Params[2]."' AND X.IDDOCENTE=Y.ID";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido un plan analitico.");
            }

            function ObtenerPlanAnaliticoDetalle($IdPlan,$IdFiltro=0) //OK
            {       $FQL = ($IdFiltro!=0 ? " AND X.SEMANA=$IdFiltro ":" ");
                    $SQL = "SELECT X.ID,X.SEMANA,X.FECHA,X.IDCONTENIDO,X.SBCONTENIDO,Y.DESCRIPCION,X.CD,X.CP,X.CA,X.HOREXCD,X.IDESTADO,X.HORCD ";
                    $SQL.= "FROM	POSTGRADO..COL_PLAN_ANALITICO AS W, ";
                    $SQL.= "		POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS X, ";
                    $SQL.= "		POSTGRADO..COL_MAT_CONT_TEMATICO AS Y ";
                    $SQL.= "WHERE W.ID='$IdPlan' AND W.IDESTADO=1 AND W.ID=X.IDPLANANA AND X.IDESTADO IN (1,6) AND X.IDCONTENIDO=Y.ID".$FQL;
                    $SQL.= "ORDER BY X.FECHA ASC";

                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definida Planificaciones.");
            }

            public function ExisteItemPlanificacion($Plan)
            {       $Ejecuta = explode("/",$Plan['fecha']);
                    $Content = $Plan['idcontenido'];
                    $Excluir = (empty($Plan['iddetalle']) ? "" : "ID<>".$Plan['iddetalle']." AND ");
                    $SQL = "SELECT COUNT(SEMANA) AS REPETIDO FROM POSTGRADO..COL_PLAN_ANALITICO_DETALLE
                            WHERE ".$Excluir." IDCONTENIDO='$Content' AND IDESTADO=1 AND IDPLANANA=".$Plan['id']." AND (YEAR(FECHA)='$Ejecuta[2]' AND MONTH(FECHA)='$Ejecuta[1]' AND DAY(FECHA)='$Ejecuta[0]')";
                            $resourceID = $this->Works->Query($SQL);
                            $resp = $this->Works->FieldDataByName($resourceID,'REPETIDO');
                    return array(($resp>0 ? true : false),"El item ya se encuentra en la planificación");
            }

            public function ExcedeHorasPlanificacion($Plan)
            {       $Ex = (empty($Plan['idcnt']) ? "" : "Z.ID<>".$Plan['idcnt']." AND ");
                    $Qw = "SELECT COMPONENTECD,COMPONENTECP,COMPONENTECA, ISNULL(SUM(Z.CD),0) AS CD, ISNULL(SUM(Z.CP),0) AS CP, ISNULL(SUM(Z.CA),0) AS CA ";
                    $Qw.= "FROM POSTGRADO..COL_MAT_CONT_GENERAL AS X ";
                    $Qw.= "LEFT JOIN (POSTGRADO..COL_PLAN_ANALITICO AS Y ";
	            $Qw.= "INNER JOIN POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS Z ON ".$Ex." Y.ID=Z.IDPLANANA AND Z.IDESTADO=1) ";
                    $Qw.= "ON X.ID=Y.IDMATCONT AND Y.ID=".$Plan['idpln']." AND Y.IDESTADO=1 ";
                    $Qw.= "WHERE X.ID=".$Plan['idmtc']." AND X.IDESTADO=1 ";
                    $Qw.= "GROUP BY COMPONENTECD,COMPONENTECP,COMPONENTECA ";

                    $resourceID = $this->Works->Query($Qw);
                    while($Horas = $this->Works->NextRecordAssoc($resourceID))
                    {     if ($Horas["COMPONENTECD"]<$Horas["CD"]+intval($Plan['comcd'])) return array(true,"El componente CD excede lo planificado.");
                          if ($Horas["COMPONENTECP"]<$Horas["CP"]+intval($Plan['comcp'])) return array(true,"El componente CPE excede lo planificado.");
                          if ($Horas["COMPONENTECA"]<$Horas["CA"]+intval($Plan['comca'])) return array(true,"El componente CA excede lo planificado.");
                    }
                    $this->Works->CloseResource($resourceID);
                    return array(false,"No excede total de horas planificadas.");
            }

            public function InsertaPlanificacion($Form)
            {       $MOD = json_decode(Session::getValue('Sesion'));
                    $SQL = "INSERT INTO POSTGRADO..COL_PLAN_ANALITICO (IDMATCONT,IDMAESTRIA,IDMATERIA,IDCICLO,IDDOCENTE,IDPARALELO,IDESTADO,IDUSCREA,FEUSCREA)
                            OUTPUT Inserted.ID
                            VALUES('$Form->idmatcont','$Form->idmaestria','$Form->idmateria','$Form->idcohorte','$Form->iddocente','$Form->idgrupo','$Form->estado','$MOD->Idusrol',getDate())";

                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
            }

            public function ValidaIngresoHorario($IdPlan)
            {       $SQL = "SELECT COUNT(Y.CD) AS CTRLHOR FROM POSTGRADO..COL_PLAN_ANALITICO AS X, POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS Y
                            WHERE X.ID=$IdPlan AND X.ID=Y.IDPLANANA AND Y.IDESTADO=1 AND Y.CD<>0 AND Y.HORCD='00:00' ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FieldDataByName($resourceID,'CTRLHOR');
            }

            public function UpdatePlanificacion($Form)
            {       $MOD = json_decode(Session::getValue('Sesion'));
                    if ($this->ValidaIngresoHorario($Form->id)==0)
                    {   $SQL = "UPDATE POSTGRADO..COL_PLAN_ANALITICO
                                SET IDMATCONT='$Form->idmatcont',
                                IDMAESTRIA='$Form->idmaestria',
                                IDMATERIA='$Form->idmateria',
                                IDCICLO='$Form->idcohorte',
                                IDDOCENTE='$Form->iddocente',
                                IDPARALELO='$Form->idgrupo',
                                IDESTADO='$Form->estado',
                                IDUSMODIFICA='$MOD->Idusrol',
                                FEUSMODIFICA=getDate()
                                WHERE ID='$Form->id'";
                        $this->Works->Query($SQL);
                        return array(true,$Form->id);
                    }
                    return array(false,"Debe registrar el horario haciendo click en el emoticon.");
            }

            public function ValidaFechaItemCicloPlanificacion($Plan,$Fecha)
            {       $SQL = "SELECT COUNT(Y.ID) AS VALIDA
                            FROM POSTGRADO..COL_PLAN_ANALITICO AS X,POSTGRADO..COL_CICLOS AS Y
                            WHERE X.ID=$Plan AND X.IDCICLO=Y.ID AND DATEDIFF(DAY,Y.FEINICIO,'$Fecha')>=0 AND DATEDIFF(DAY,'$Fecha',Y.FEFIN)>=0";
                    $resourceID = $this->Works->Query($SQL);
                    $Valida = $this->Works->FieldDataByName($resourceID,'VALIDA');
                    return array(($Valida>0),($Valida>0 ? "Fecha en el rango correcto":"La fecha está fuera del ciclo."));
            }

            public function InsertaItemPlanficacion($Plan)
            {       $MOD = json_decode(Session::getValue('Sesion'));
                    $Rsp = $this->ValidaFechaItemCicloPlanificacion($Plan->id,$Plan->fecha);
                    if ($Rsp[0])
                    {   $SQL = "INSERT INTO POSTGRADO..COL_PLAN_ANALITICO_DETALLE(IDPLANANA,SEMANA,FECHA,IDCONTENIDO,SBCONTENIDO,CD,CP,CA,IDUSCREA,FEUSCREA)
                                OUTPUT Inserted.ID 
                                VALUES('$Plan->id','$Plan->semana','$Plan->fecha','$Plan->idcontenido','".utf8_decode($Plan->sbcontenido)."','$Plan->componentecd','$Plan->componentecp','$Plan->componenteca','$MOD->Idusrol',getDate())";
                        $resourceID = $this->Works->Query($SQL);
                        return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
                    }
                    return $Rsp;
            }

            public function InsertaItemPlanficacionReplicar($Plan)
            {       $MOD = json_decode(Session::getValue('Sesion'));
                    $Rsp = $this->ValidaFechaItemCicloPlanificacion($Plan->id,$Plan->fecha);
                    if ($Rsp[0])
                    {   $SQL = "INSERT INTO POSTGRADO..COL_PLAN_ANALITICO_DETALLE(IDPLANANA,SEMANA,FECHA,IDCONTENIDO,SBCONTENIDO,CD,CP,CA,HORCD,IDUSCREA,FEUSCREA)
                                OUTPUT Inserted.ID 
                                VALUES('$Plan->id','$Plan->semana','$Plan->fecha','$Plan->idcontenido','".utf8_decode($Plan->sbcontenido)."','$Plan->componentecd','$Plan->componentecp','$Plan->componenteca','$Plan->horcd','$MOD->Idusrol',getDate())";
                        $resourceID = $this->Works->Query($SQL);
                        return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
                    }
                    return $Rsp;
            }

            public function UpdateItemPlanificacion($Plan)
            {       $MOD = json_decode(Session::getValue('Sesion'));
                    $Rsp = $this->ValidaFechaItemCicloPlanificacion($Plan->id,$Plan->fecha);
                    if ($Rsp[0])
                    {   $SQL = "UPDATE POSTGRADO..COL_PLAN_ANALITICO_DETALLE 
                                SET    SEMANA='$Plan->semana',
                                       FECHA='$Plan->fecha',
                                       IDCONTENIDO='$Plan->idcontenido',
                                       SBCONTENIDO='".utf8_decode($Plan->sbcontenido)."',
                                       CD='$Plan->componentecd',
                                       CP='$Plan->componentecp',
                                       CA='$Plan->componenteca',
                                       IDUSMODIFICA='$MOD->Idusrol',
                                       FEUSMODIFICA=getDate()
                                WHERE  ID='$Plan->iddetalle'";
                        $resourceID = $this->Works->Query($SQL);
                        return array(true,$Plan->iddetalle);
                    }
                    return $Rsp;
            }

            public function EliminaDetallePlanAnalitico($id)
            {       $MOD = json_decode(Session::getValue('Sesion'));
                    $SQL = "UPDATE POSTGRADO..COL_PLAN_ANALITICO_DETALLE SET IDESTADO=0, IDUSMODIFICA='$MOD->Idusrol', FEUSMODIFICA=getDate()
                            WHERE ID='$id'";
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$id);
            }

            //validar que no este vinculado con mallas asistencias y notas
            public function EliminaPlanAnalitico($id)
            {       $MOD = json_decode(Session::getValue('Sesion'));
                    $SQL = "UPDATE POSTGRADO..COL_PLAN_ANALITICO SET IDESTADO=0, IDUSMODIFICA='$MOD->Idusrol', FEUSMODIFICA=getDate()
                            WHERE ID='$id'";
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$id);
            }

            function ObtenerMaestriaByDocenteFacultad($prepareDQL)
            {       $SQL1="";
                    $SQL2="";
                    if (array_key_exists("facultad", $prepareDQL))
                    {   if (count($prepareDQL['facultad'])>0)
                        {   $FacultadID = implode(',',$prepareDQL['facultad']);
                            $SQL1 = " AND J.IDFACULTAD IN ($FacultadID) ";
                        }
                    }
                    if (array_key_exists("texto", $prepareDQL))
                    {   $MaestriaTX = $prepareDQL['texto'];
                        $SQL2 = " AND (J.DESCRIPCION LIKE '%".$MaestriaTX."%')";
                    }
                    $DocenteID = $prepareDQL['docente'];
                    $SQL = "SELECT DISTINCT J.ID AS IDMAESTRIA,UPPER(J.DESCRIPCION) AS MAESTRIA,
                                Z.ID,Z.CICLO+' ('+Y.COHORTE+')' AS CICLO, Y.IDPOLEVAL, Y.IDMAESVER, 
                                Y.ID AS IDCOHORTE
                            FROM
                                POSTGRADO..COL_PLAN_ANALITICO AS X,
                                POSTGRADO..ADM_MAESTRIA_COHORTE AS Y,
                                POSTGRADO..COL_CICLOS AS Z,
                                POSTGRADO..ADM_MAESTRIA AS J
                            WHERE X.IDDOCENTE='$DocenteID' AND X.IDESTADO=1 AND X.IDCICLO=Z.ID AND Z.IDESTADO=1 AND Z.IDCOHORTE=Y.ID AND J.ID=X.IDMAESTRIA".$SQL1.$SQL2;

		    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen maestrias planificadas para el docente en el presente ciclo.");
            }

            function ObtenerMateriaGrupoByMaestriaCiclo($prepareDQL)
            {       $SQL1="";
                    if (array_key_exists("texto", $prepareDQL))
                    {   $MateriaTX = $prepareDQL['texto'];
                        $SQL1 = " AND (Y.MATERIA LIKE '%".$MateriaTX."%')";
                    }
                    $DocenteID = $prepareDQL['docente'];
                    $CicloID = $prepareDQL['ciclo'];
                    $MaestriaID = $prepareDQL['maestria'];
                    //." GROUP BY Y.ID, Y.MATERIA,Z.ID, Z.PARALELO ORDER BY IDMATERIA"
                    $SQL = "SELECT Y.ID AS IDMATERIA,Y.MATERIA,Z.ID AS IDGRUPO,Z.PARALELO AS GRUPO
                            FROM
                                POSTGRADO..COL_PLAN_ANALITICO AS X,
                                POSTGRADO..COL_MATERIA AS Y,
                                POSTGRADO..COL_COHORTE_PARALELO AS Z
                            WHERE X.IDDOCENTE='$DocenteID' AND X.IDESTADO=1 AND X.IDCICLO='$CicloID' AND X.IDMAESTRIA='$MaestriaID' AND X.IDMATERIA=Y.ID AND X.IDPARALELO=Z.ID".$SQL1." GROUP BY Y.ID, Y.MATERIA,Z.ID, Z.PARALELO ORDER BY IDMATERIA";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias planificadas para el docente en el presente ciclo.");
            }

            function ActualizaAsistenciaPlanificacion($Params,$Compte)
            {        $CicloID=$Params[1];
                     $MaestriaID=$Params[2];
                     $MateriaID=$Params[3];
                     $ParaleloID=$Params[4];
                     $FechaAsist=$Params[5];

                     $SQL = "UPDATE POSTGRADO..COL_PLAN_ANALITICO_DETALLE ";
                     $SQL.= "SET HOREX".$Compte."='S', POSTGRADO..COL_PLAN_ANALITICO_DETALLE.IDESTADO=1 ";
                     $SQL.= "FROM POSTGRADO..COL_PLAN_ANALITICO AS X ";
                     $SQL.= "WHERE X.IDESTADO=1 AND X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDPARALELO=$ParaleloID AND ";
                     $SQL.= "POSTGRADO..COL_PLAN_ANALITICO_DETALLE.IDPLANANA=X.ID AND ";
                     $SQL.= "POSTGRADO..COL_PLAN_ANALITICO_DETALLE.IDESTADO IN (1,6) AND ";
                     $SQL.= "POSTGRADO..COL_PLAN_ANALITICO_DETALLE.".$Compte.">0 AND ";
                     $SQL.= "POSTGRADO..COL_PLAN_ANALITICO_DETALLE.FECHA='$FechaAsist' ";
                     $resourceID = $this->Works->Query($SQL);
                     return true;
            }

            function ObtenerDistributivo($Params)
            {     //Params = [IDMTC,ORDEN,DOCTE,EMAIL,FCLTD,IDCLO,IDMAE,IDMAT,IDDOC,IDGRP];
                    $SQL = "SELECT SUBSTRING(LTRIM(STR(Y.ORDEN)),1,1) AS UNIDAD,
                                   SUM(X.CD) AS CD,SUM(X.CP) AS CPE,SUM(X.CA) AS CA
                            FROM
                                  POSTGRADO..COL_PLAN_ANALITICO AS W,
                                  POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS X,
                                  POSTGRADO..COL_MAT_CONT_TEMATICO AS Y
                            WHERE W.IDESTADO=1 AND W.IDCICLO='$Params[5]' AND W.IDMAESTRIA='$Params[6]' AND W.IDMATERIA='$Params[7]' AND W.IDPARALELO='$Params[9]' AND W.IDDOCENTE='$Params[8]' AND 
                                  W.ID=X.IDPLANANA AND X.IDCONTENIDO=Y.ID AND X.IDESTADO IN (1,6) AND Y.IDMATCONT='$Params[0]' AND SUBSTRING(LTRIM(STR(Y.ORDEN)),1,1)='$Params[1]'  
                                  GROUP BY SUBSTRING(LTRIM(STR(Y.ORDEN)),1,1)";

                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No distributivo por unidad en planificación.");
            }

            function ObtenerPlanClases($Params)
            {        $SQL = "SELECT X.SEMANA,X.FECHA,Y.DESCRIPCION,X.CD,X.CP,X.CA 
                             FROM POSTGRADO..COL_PLAN_ANALITICO AS W, POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS X, POSTGRADO..COL_MAT_CONT_TEMATICO AS Y 
                             WHERE W.IDESTADO=1 AND W.IDCICLO='$Params[5]' AND W.IDMAESTRIA='$Params[6]' AND W.IDMATERIA='$Params[7]' AND W.IDPARALELO='$Params[9]' AND 
                                   W.IDDOCENTE='$Params[8]' AND W.ID=X.IDPLANANA AND X.IDCONTENIDO=Y.ID AND X.IDESTADO=1 AND Y.IDMATCONT='$Params[0]' 
                             ORDER BY X.SEMANA,X.FECHA";
                      $resourceID = $this->Works->Query($SQL);
                      return $this->Works->FillDataRecordAssoc($resourceID,"No distributivo por unidad en planificación.");
            }

            function ObtenerPlanClasesObjetivos($idmtc)
            {       $SQL="SELECT Y.ORDEN,Y.METODO,Y.OBJETIVO,Y.CD,Y.CP,Y.CA FROM POSTGRADO..COL_MAT_CONT_TEMATICO AS Y WHERE Y.IDMATCONT='$idmtc' AND Y.IDTIPOPCION=0 AND Y.IDESTADO=1";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"");
            }
        }
