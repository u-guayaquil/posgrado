<?php
        require_once("src/base/DBDAO.php");
        
        class DAOHorario extends DBDAO
        {   private static $entityHorario;
            private $Works;  
            
            public function __construct()
            {      self::$entityHorario = DBDAO::getConnection();
                   $this->Works = self::$entityHorario->MyFunction();                
            }

            function CrearHorarioByHoras($AsisHoho,$AsisNuho)
            {       if ($AsisNuho!=0)
                    {   $Utils = new Utils();
                        $Config = $Utils->EstructuraHorario($AsisHoho,$AsisNuho);
                        $resourceID = $this->Works->Query("INSERT INTO POSTGRADO..COL_HORARIO_HORAS(HORARIO,INICIO,HORAS) OUTPUT Inserted.ID VALUES('".$Config['HTabs']."','$AsisHoho',$AsisNuho)");
                        return array(true,$this->Works->FieldDataByName($resourceID,'ID'),$Config['HHors']); 
                    }
                    else
                    return array(false,"No se puede crear horario con cero numero de horas.");    
            }
            
            function ObtenerHorarioByHoras($AsisHoho,$AsisNuho)
            {       $DataCollection = array();
                    $HorarioID = 0;
                    $resourceID = $this->Works->Query("SELECT ID,HORARIO FROM POSTGRADO..COL_HORARIO_HORAS WHERE INICIO='$AsisHoho' AND HORAS=$AsisNuho");
                    while($filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $HorarioID = $filas['ID'];
                          $HorarioTX = explode("|",$filas['HORARIO']);
                          foreach($HorarioTX as $HoraTx)
                          {       $DataCollection[] = $HoraTx;                 
                          }
                    }
                    $this->Works->CloseResource($resourceID);
                    return array(($HorarioID>0),$HorarioID,$DataCollection);        
            }
         
            
        }
?>        
