<?php
require_once("src/base/DBDAO.php");

class DAOHospital extends DBDAO
{   private static $entityHospital;
    private $Works;  
    
    public function __construct()
    {      self::$entityHospital = DBDAO::getConnection();
            $this->Works = self::$entityHospital->MyFunction();                
    }

    public function ObtenerHospitalByID($id)
    {   $DSQL = "SELECT CM.ID, CM.UAD, CM.SIGLAS, CM.FECREACION, CM.IDESTADO
                        FROM POSTGRADO..COL_HOSPITAL AS CM
                        WHERE CM.ID = '$id'"; 
        $resourceID = $this->Works->Query($DSQL);
        return $this->Works->NextRecordObject($resourceID); 
    }

    public function ObtenerHospitalByIDGuardado($id)
    {   $DSQL = "SELECT CM.ID, CM.UAD, CM.SIGLAS, CM.FECREACION, CM.IDESTADO
                        FROM POSTGRADO..COL_HOSPITAL AS CM
                        WHERE CM.ID = '$id'"; 
        $resourceID = $this->Works->Query($DSQL);
        return $this->Works->FillDataRecordAssoc($resourceID,"No existen un hospital definido."); 
    }

    public function ObtenerHospitalByDescripcion($PrepareDQL)
    {   $DSQL = "SELECT CH.ID, CH.UAD, CH.SIGLAS, CH.FECREACION,CH.IDESTADO
                        FROM  POSTGRADO..COL_HOSPITAL AS CH
                        WHERE ID > 0
                        AND CH.UAD LIKE '%".$PrepareDQL['texto']."%'";                                            
            $resourceID = $this->Works->Query($DSQL);                  
            return $this->Works->FillDataRecordAssoc($resourceID,"No existen hospitales definidos."); 
    }
    
    public function InsertaHospital($Form)
    {       $Modulo = json_decode(Session::getValue('Sesion'));
            $Descrp = strtoupper($Form->descripcion);
            $Siglas = strtoupper($Form->siglas);
            $QUERY = "INSERT INTO POSTGRADO..COL_HOSPITAL(UAD,SIGLAS,IDUSCREA,FECREACION) 
                        OUTPUT INSERTED.ID 
                        VALUES('$Descrp','$Siglas','".$Modulo->Idusrol."',getDate())";
            $resourceID = $this->Works->Query($QUERY);
            return $this->Works->FieldDataByName($resourceID,'ID');
    }

    public function ActualizaHospital($Form)
    {       $Modulo = json_decode(Session::getValue('Sesion'));
            $Descrp = utf8_decode(strtoupper($Form->descripcion));
            $Siglas = strtoupper($Form->siglas);
            $QUERY = "UPDATE POSTGRADO..COL_HOSPITAL SET 
                        UAD='$Descrp',
                        SIGLAS='$Siglas',
                        IDUSMODFICA='".$Modulo->Idusrol."',
                        IDESTADO='$Form->estado',    
                        FEMODIFICA=getDate() 
                        WHERE ID='$Form->id'";
            $resourceID = $this->Works->Query($QUERY); 
            return intval($Form->id);
    }
    
    public function DesactivaHospital($id)
    {       $Modulo = json_decode(Session::getValue('Sesion'));
            $resourceID = $this->Works->Query("UPDATE POSTGRADO..COL_HOSPITAL SET IDESTADO=0, IDUSMODFICA='".$Modulo->Idusrol."', FEMODIFICA=getDate() WHERE id='$id' ");    
            return 0;                    
    }
}
?>        
