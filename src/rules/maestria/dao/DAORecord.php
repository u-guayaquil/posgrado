<?php
require_once("src/base/DBDAO.php");

class DAORecord extends DBDAO
{   private static $entityRecord;
	private $Works;

	public function __construct()
	{       self::$entityRecord = DBDAO::getConnection();
			$this->Works = self::$entityRecord->MyFunction();
	}

	function ConsultarRecords($Params)
	{	$MaestriaID=$Params[0];
		$CohorteID=$Params[1];
		$EstudianteCI=$Params[2];
		$SQL = "SELECT ISNULL(Y.ID,0) AS IDNOTAS, X.ID AS IDESTUDMATE, ISNULL(Y.NOTA,'') AS NOTAS, 
					ISNULL(SUM(H.EFECTIVA),0)+ISNULL(SUM(H.JUSTIFICADA),0) AS ASIST,
					CM.MATERIA, CP.PARALELO, MCG.COMPONENTECD AS TCD
				FROM   POSTGRADO..COL_ESTUDIANTE AS V
						INNER JOIN (POSTGRADO..COL_ESTUDIANTE_COHORTE AS EC
						INNER JOIN (POSTGRADO..COL_ESTUDIANTE_COHORTE_MATRICULA AS W
						INNER JOIN (POSTGRADO..COL_ESTUDIANTE_MATERIA AS X 
						LEFT JOIN POSTGRADO..COL_MATERIA AS CM ON X.IDMATERIA=CM.ID 
						LEFT JOIN  POSTGRADO..COL_MAT_CONT_GENERAL AS MCG ON CM.ID=MCG.IDMATERIA AND MCG.IDESTADO=1 
						LEFT JOIN POSTGRADO..COL_COHORTE_PARALELO AS CP ON X.IDPARALELO=CP.ID
						LEFT JOIN (POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA AS H
						LEFT JOIN   POSTGRADO..COL_ESTUDIANTE_MATERIA_NOTAS AS Y
								ON H.IDESTUDIANTEMATERIA=Y.IDESTUDIANTEMATERIA)
								ON X.ID=H.IDESTUDIANTEMATERIA)
								ON W.IDESTADO<>0 AND W.ID=X.IDMATRICULA)
								ON EC.ID=W.IDESTUDCOHORTE)
								ON V.ID=X.IDESTUDIANTE
				WHERE X.IDESTADO=1 AND V.CEDULA = '".$EstudianteCI."' AND EC.IDCOHORTE = $CohorteID AND X.IDMAESTRIA = $MaestriaID
				GROUP by ISNULL(Y.ID,0), X.ID, ISNULL(Y.NOTA,''), EC.IDUAD, CM.MATERIA, MCG.COMPONENTECD, CP.PARALELO
				ORDER BY CM.MATERIA";
		$resourceID = $this->Works->Query($SQL);
		return $this->Works->FillDataRecordAssoc($resourceID,"No existen definido datos del estudiante seleccionado.");
	}

	function ConsultarMaestria($Params)
	{   $txt = $Params['txt'];

		$SQL = "SELECT M.ID AS IDMAESTRIA, UPPER(M.DESCRIPCION) AS MAESTRIA, M.IDESTADO, F.ID AS IDFACULTAD
				FROM	POSTGRADO..ADM_MAESTRIA AS M,
						POSTGRADO..ADM_MAESTRIA_VERSION MV,
						POSTGRADO..ADM_FACULTAD F
				WHERE	M.IDFACULTAD = F.ID
				AND	M.ID = MV.IDMAESTRIA
				AND	M.IDESTADO = 1 AND M.MAESTRIA LIKE '%$txt%'";
		$resourceID = $this->Works->Query($SQL);
		return $this->Works->FillDataRecordAssoc($resourceID,"No existe definida maestrias.");
	}

	function ConsultarCohorteByMaestria($Params)
	{   $texto = $Params['txt'];
		$maestria = $Params['mtr'];
		$SQL = "SELECT MC.ID, MC.COHORTE, MV.VERSION AS VERSION_MAESTRIA, MC.IDESTADO, CC.ID AS IDCICLO
				FROM	POSTGRADO..ADM_MAESTRIA M,
						POSTGRADO..ADM_MAESTRIA_VERSION MV,
						POSTGRADO..ADM_MAESTRIA_COHORTE MC,
						POSTGRADO..COL_CICLOS CC
				WHERE	M.ID = MV.IDMAESTRIA
				AND	MV.ID = MC.IDMAESVER
				AND	CC.IDCOHORTE = MC.ID
				AND     MC.IDESTADO = 1
				AND	MC.COHORTE LIKE '%$texto%'
				AND     M.ID = $maestria";

		$resourceID = $this->Works->Query($SQL);
		return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido cohortes.");
	}

	function ConsultarEstudiantesByCohorte($Params)
	{	$texto = $Params['txt'];
		$maestria = $Params['mtr'];
		$cohorte = $Params['cht'];
		$SQL = "SELECT DISTINCT E.ID,E.CEDULA,E.NOMBRES,E.APELLIDOS
				FROM POSTGRADO..COL_ESTUDIANTE E
				LEFT JOIN POSTGRADO..COL_ESTUDIANTE_MATERIA EM ON E.ID = EM.IDESTUDIANTE
				LEFT JOIN POSTGRADO..COL_ESTUDIANTE_COHORTE EC ON E.ID = EC.IDESTUDIANTE
				WHERE EM.IDMAESTRIA = $maestria AND EC.IDCOHORTE = $cohorte
				ORDER BY E.APELLIDOS";
		$resourceID = $this->Works->Query($SQL);
		return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido estudiantes.");
	}

	function ConsultarEstudiantesByTxt($Params)
	{	$texto = $Params['txt'];
		$maestria = $Params['mtr'];
		$cohorte = $Params['cht'];
		$DSQL = "";
		if(is_numeric($texto))
			$DSQL=" AND E.CEDULA LIKE '%$texto%' ";
		else
			$DSQL=" AND E.NOMBRES LIKE '%".$texto."%' OR E.APELLIDOS LIKE '%".$texto."%' ";
		$SQL = "SELECT DISTINCT E.ID,E.CEDULA,E.NOMBRES,E.APELLIDOS
				FROM POSTGRADO..COL_ESTUDIANTE E
				LEFT JOIN POSTGRADO..COL_ESTUDIANTE_MATERIA EM ON E.ID = EM.IDESTUDIANTE
				LEFT JOIN POSTGRADO..COL_ESTUDIANTE_COHORTE EC ON E.ID = EC.IDESTUDIANTE
				WHERE EM.IDMAESTRIA = $maestria AND EC.IDCOHORTE = $cohorte".$DSQL."
				ORDER BY E.APELLIDOS";
		$resourceID = $this->Works->Query($SQL);
		return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido estudiantes.");
	}

	function ObtenerDatosCabeceraRecordEstudiante($Params)
	{       //var Params = [MAEST,COHORTE,CEDULA];  
			$MaestriaID = $Params[0];   $CohorteID = $Params[1];
			$SQL = "SELECT DISTINCT A.DESCRIPCION AS FACULTAD,B.DESCRIPCION AS MAESTRIA,D.COHORTE,
							J.DESCRIPCION AS TIPOMALLA, MIN(N.FECHA) AS DESDE, MAX(N.FECHA) AS HASTA
						FROM 
							POSTGRADO..ADM_FACULTAD AS A,		POSTGRADO..ADM_MAESTRIA AS B,		POSTGRADO..ADM_MAESTRIA_VERSION AS C,
							POSTGRADO..ADM_MAESTRIA_COHORTE AS D,	POSTGRADO..COL_COHORTE_PARALELO AS E,   POSTGRADO..COL_CICLOS AS F, 
							POSTGRADO..COL_MALLA_DETALLE AS G,	POSTGRADO..COL_MALLA AS I,		POSTGRADO..COL_MALLA_TIPO J,			
							POSTGRADO..COL_PLAN_ANALITICO AS M, 
							POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS N
						WHERE	
								A.ID=B.IDFACULTAD		AND B.ID=$MaestriaID AND D.ID=$CohorteID	
								AND B.ID=C.IDMAESTRIA	AND C.IDESTADO IN (1,11) 
								AND C.ID=D.IDMAESVER	AND D.IDESTADO<>0	AND D.ID=E.IDCHORTE
								AND F.IDCOHORTE=D.ID	AND G.IDMALLA=D.IDMALLA AND G.NIVEL=F.NIVEL	AND I.ID=G.IDMALLA		
								AND I.IDTIPOMALLA=J.ID	AND M.IDESTADO=1	AND M.IDCICLO=F.ID	AND M.IDMAESTRIA=B.ID	
								AND M.IDMATERIA=G.IDMATERIA AND M.IDPARALELO=E.ID 	AND M.ID=N.IDPLANANA	AND N.IDESTADO=1
						GROUP BY A.DESCRIPCION,B.DESCRIPCION,D.COHORTE,E.PARALELO,F.CICLO,J.DESCRIPCION";
			$resourceID = $this->Works->Query($SQL);
			return $this->Works->FillDataRecordAssoc($resourceID,"No existen datos para el resporte.");            
	}
}
