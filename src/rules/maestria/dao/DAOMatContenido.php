<?php   require_once("src/base/DBDAO.php");
        
        class DAOMatContenido extends DBDAO
        {   private static $entityMatContenido;
            private $Works;  
            
            public function __construct()
            {      self::$entityMatContenido = DBDAO::getConnection();
                   $this->Works = self::$entityMatContenido->MyFunction();                
            }

          //Consulta princial de la definicion de contenidos            
            function ObtenerIndiceByTipo($prepareDQL) //OK
            {   if($prepareDQL){
                        $idMtc = $prepareDQL['idmtc'];
                        $idCht = $prepareDQL['idcht'];
                }
                else{$idMtc="";$idCht ="";}
                /*print_r("SELECT o.ID,o.IDCOHORTE,o.DESCRIPCION,o.ORDEN,o.IDTIPOPCION,o.RESULTADO,o.METODO,o.OBJETIVO,o.CD,o.CP,o.CA
                    FROM POSTGRADO..COL_MAT_CONT_TEMATICO o 
                    WHERE o.IDMATCONT='$idMtc' and o.IDCOHORTE='$idCht' and o.IDTIPOPCION=0 and o.IDESTADO=1 
                    ORDER BY o.ORDEN");*/
                $resourceID = $this->Works->Query("SELECT o.ID,o.IDCOHORTE,o.DESCRIPCION,o.ORDEN,o.IDTIPOPCION,o.RESULTADO,o.METODO,o.OBJETIVO,o.CD,o.CP,o.CA
                                                FROM POSTGRADO..COL_MAT_CONT_TEMATICO o 
                                                WHERE o.IDMATCONT='$idMtc' and o.IDCOHORTE='$idCht' and o.IDTIPOPCION=0 and o.IDESTADO=1 
                                                ORDER BY o.ORDEN");
                return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidas unidades en la estructura");
            }   
            public function ObtenerIndiceByIDPadre($IdMtc,$Id,$IdCht)
            {       $SQL = "SELECT o.ID, o.IDCOHORTE, o.DESCRIPCION, o.IDTIPOPCION, o.ORDEN, o.IDPADRE, o.IDESTADO, o.RESULTADO,o.METODO,o.OBJETIVO,o.CD,o.CP,o.CA ";
                    $SQL.= "FROM POSTGRADO..COL_MAT_CONT_TEMATICO o ";
                    $SQL.= "WHERE o.IDPADRE='$Id' AND o.IDMATCONT='$IdMtc' AND o.IDCOHORTE='$IdCht' AND o.IDESTADO IN (1,0)";
                    $SQL.= "ORDER BY o.ORDEN ASC ";
                    //print_r($SQL);
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos temas y subtemas.");
            }
          //Consulta princial de la definicion de contenidos
          
          //Modal de definicion de Matetria Contenido
            public function ObtenerByTipo($prepareDQL)
            {       $Rgt = array();
                    $Ing = false;
                    $idMtc = $prepareDQL['idmtc'];
                    $idCht = $prepareDQL['idcht'];
                    $SQL = "SELECT o.ID, o.IDTIPOPCION, o.DESCRIPCION, o.ORDEN 
                            FROM POSTGRADO..COL_MAT_CONT_TEMATICO o 
                            WHERE o.IDMATCONT='$idMtc' and o.IDCOHORTE='$idCht' and o.IDTIPOPCION = 0 and o.IDESTADO=1 
                            ORDER BY o.ORDEN ASC ";
                    $resourceID = $this->Works->Query($SQL);
                    while ($fila = $this->Works->NextRecordAssoc($resourceID))
                    {      $Ing = true;
                           $Rgt[] = array('ID'=>$fila['ID'],'IDTIPOPCION'=>$fila['IDTIPOPCION'],'DESCRIPCION'=>utf8_encode($fila['DESCRIPCION']),'ORDEN'=>$fila['ORDEN']);        
                           $Rgt = $this->ObtenerByTipoSgte($idMtc,$fila['ID'],$Rgt);
                    }
                    $this->Works->CloseResource($resourceID);
                    return array($Ing,$Rgt);
            }
            
            private function ObtenerByTipoSgte($idMtc,$idSgt,$Rgt)
            {       $SQL = "SELECT o.ID, o.IDTIPOPCION, o.DESCRIPCION, o.ORDEN 
                            FROM POSTGRADO..COL_MAT_CONT_TEMATICO o 
                            WHERE o.IDPADRE='$idSgt' and o.IDMATCONT='$idMtc' and o.IDTIPOPCION = 1 and o.IDESTADO=1 
                            ORDER BY o.ID, o.ORDEN ASC ";
                    $resourceID = $this->Works->Query($SQL);
                    while ($fila = $this->Works->NextRecordAssoc($resourceID))
                    {      $Rgt[] = array('ID'=>$fila['ID'],'IDTIPOPCION'=>$fila['IDTIPOPCION'],'DESCRIPCION'=>utf8_encode($fila['DESCRIPCION']),'ORDEN'=>$fila['ORDEN']);        
                           $Rgt = $this->ObtenerByTipoSgte($idMtc,$fila['ID'],$Rgt);
                    }
                    $this->Works->CloseResource($resourceID);
                    return $Rgt;
            }
          //Fin de Modal de definicion de Matetria Contenido
          
           public function Inserta($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Matcon = $Form->idmatcont;
                    $Cohorte = $Form->idcohorte;
                    $Ordens = $Form->orden;
                    $Idtipo = $Form->idtipo;
                    $Descrp = $Form->descripcion;
                    $IPadre = $Form->idpadre;
                    $result = $Form->resultado;
                    $metodo = $Form->metodos;
                    $objetv = $Form->objaprende;
                    $Cd = $Form->cd;
                    $Cp = $Form->cp;
                    $Ca = $Form->ca;
                    $resourceID = $this->Works->Query("INSERT INTO POSTGRADO..COL_MAT_CONT_TEMATICO (IDMATCONT,IDCOHORTE,DESCRIPCION,ORDEN,IDPADRE,IDESTADO,IDUSCREA,FEUSCREA,IDTIPOPCION,RESULTADO,METODO,OBJETIVO,CD,CP,CA) OUTPUT Inserted.ID VALUES('$Matcon','$Cohorte','$Descrp','$Ordens','$IPadre','$Form->estado','".$Modulo->Idusrol."',getDate(),'$Idtipo','$result','$metodo','$objetv','$Cd','$Cp','$Ca')");    
                    return array(true,$this->Works->FieldDataByName($resourceID,'ID')); 
            }

            private function TieneHijos($id)
            {       $resourceID = $this->Works->Query("select count(X.ID) AS EXISTE From POSTGRADO..COL_MAT_CONT_TEMATICO as X where X.IDPADRE='$id'");    
                    return $this->Works->FieldDataByName($resourceID,'EXISTE');
            }
            
            public function Actualiza($Form)
            {       $id = intval($Form->id);
                    if ($this->TieneHijos($id)>0 && $Form->idtipo==2)
                        return array(false,"El item tiene subordinados no puede cambiar su estructura.");
                    else{
                        $HayCambio = $this->HayCambioEstructural($id,$Form->idtipo,$Form->idpadre);
                        $HayPlanan = $this->TienePlanificacion($id);
                        
                        if ($HayCambio && $HayPlanan)
                            return array(false,"El item registra planificación. No puede cambiar su estructura y subordinación.");
                        else
                        {   $Modulo = json_decode(Session::getValue('Sesion'));
                            $SQL = "Update POSTGRADO..COL_MAT_CONT_TEMATICO
                                    Set   descripcion='".$Form->descripcion."',
                                          orden='".$Form->orden."',
                                          idpadre='".$Form->idpadre."',
                                          idestado='$Form->estado',
                                          idusmodifica='$Modulo->Idusrol',
                                          feusmodifica=getDate(),
                                          idtipopcion='$Form->idtipo',
                                          resultado='$Form->resultado',
                                          metodo='$Form->metodos',
                                          objetivo='$Form->objaprende',
                                          cd='$Form->cd',
                                          cp='$Form->cp',
                                          ca='$Form->ca'
                                    where id='$id'";
               
                            $resourceID = $this->Works->Query($SQL);    
                            return array(true,$id);
                        }    
                    }    
            }

            private function HayCambioEstructural($idCont,$idTipo,$idPdr)
            {       $Rsp = false;
                    $SQL ="SELECT IDTIPOPCION,IDPADRE FROM POSTGRADO..COL_MAT_CONT_TEMATICO WHERE ID='$idCont' and IDESTADO=1";
                    $resourceID = $this->Works->Query($SQL);
                    while ($fila = $this->Works->NextRecordAssoc($resourceID))
                    {      if ($fila['IDTIPOPCION']!=$idTipo || $fila['IDPADRE']!=$idPdr)       
                           $Rsp = true;
                    }
                    $this->Works->CloseResource($resourceID);
                    return $Rsp;
            }
            
            private function TienePlanificacion($idContenido)
            {       $SQL = "Select count(X.IDPLANANA) as EXISTE 
                            From POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS X, POSTGRADO..COL_PLAN_ANALITICO AS Y 
                            Where X.IDESTADO=1 and X.IDCONTENIDO='$idContenido' and Y.ID=X.IDPLANANA and Y.IDESTADO=1 ";
                    
                    $resourceID = $this->Works->Query($SQL);
                    return ($this->Works->FieldDataByName($resourceID,'EXISTE')==0 ? false: true);
            }        
            
            public function Desactiva($id)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    if (!$this->TienePlanificacion($id))
                    {   $resourceID = $this->Works->Query("Update POSTGRADO..COL_MAT_CONT_TEMATICO Set IDESTADO=0,IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate() Where id='$id'");    
                        return array(true,"El item se desactivo con exito.");
                    }
                    return array(false,"No puede desactivar este item. Se encuentra por lo menos en una planificación!!!");                                        
            }
            
            
          //Esta es la consulta modal de todo el contenido para la definicion de la planificacion
            function ObtenerContenidoByIndice($Idmtc,$Idcht) 
            {       $SQL = "SELECT o.ID,o.DESCRIPCION,o.IDTIPOPCION, o.ORDEN
                            FROM   POSTGRADO..COL_MAT_CONT_GENERAL  n,
                                   POSTGRADO..COL_MAT_CONT_TEMATICO o 
                            WHERE n.ID='$Idmtc' and o.IDCOHORTE='$Idcht' and n.ID=o.IDMATCONT and o.IDTIPOPCION=0 and o.IDESTADO=1 
                            ORDER BY o.ORDEN";

                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidas unidades en la estructura del contenido de la materia.");
            }   
            
          //Esta es la consulta modal de todo el contenido para la definicion de la planificacion 
            public function ObtenerContenidoByIndiceSgte($idpadre,$idpln,$idmtc,$idniv,$idcht)
            {       $SQA = ""; 
                    /*
                    if ($idpln!=0) //Solo controla el contenido de los capitulos
                    {   $SQA = "AND o.ID NOT IN (SELECT Y.IDCONTENIDO 
                                FROM POSTGRADO..COL_PLAN_ANALITICO AS X, POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS Y  
                                WHERE X.ID='$idpln' AND X.IDESTADO=1 AND X.ID=Y.IDPLANANA AND Y.IDESTADO=1) ";
                    }*/
            
                    $SQL = "SELECT o.ID,o.DESCRIPCION,o.IDTIPOPCION, o.ORDEN ";
                    $SQL.= "FROM   POSTGRADO..COL_MAT_CONT_GENERAL  n, POSTGRADO..COL_MAT_CONT_TEMATICO o ";
                    $SQL.= "WHERE  n.ID='$idmtc' and o.IDCOHORTE='$idcht' and n.ID=o.IDMATCONT and o.IDPADRE='$idpadre' and o.IDTIPOPCION<=$idniv and o.IDESTADO=1 ".$SQA;
                    $SQL.= "ORDER BY o.ORDEN";
                    
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos temas y subtemas.");
            }          
            
            
        }
?>        
