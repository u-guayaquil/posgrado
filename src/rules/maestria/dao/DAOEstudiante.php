<?php
    
    require_once("src/base/DBDAO.php");

    class DAOEstudiante extends DBDAO
    {       private static $entityEstudiante;
            private $Works;
              
            public function __construct()
            {       self::$entityEstudiante = DBDAO::getConnection();
                    $this->Works = self::$entityEstudiante->MyFunction();
            }
                       
            function ConsultarEstudiantes($Params)
            {       $MaestriaID=$Params[0];
                    $CohorteID=$Params[1];
                    $Paralelo=$Params[2];
                    
                    $SQL = "SELECT DISTINCT E.CEDULA, E.APELLIDOS + ' ' + E.NOMBRES AS ESTUDIANTE, 
                                (CASE 
                                WHEN EMC.FEUSMODIFICA = CAST('1900-01-01 00:00:00.000' AS	DATETIME)
                                THEN ''
                                ELSE UPPER(CONVERT(VARCHAR, EMC.FEUSMODIFICA, 6))
                                END)
                                + ' ' + EMC.OBSERVACION AS OBSERVACION, EMC.IDESTADO AS ESTADO
                            FROM    POSTGRADO..COL_ESTUDIANTE E,
                                    POSTGRADO..COL_ESTUDIANTE_MATERIA EM,
                                    POSTGRADO..COL_ESTUDIANTE_COHORTE EC,
                                    POSTGRADO..ADM_MAESTRIA_COHORTE MC,
                                    POSTGRADO..COL_COHORTE_PARALELO CP,
                                    POSTGRADO..COL_ESTUDIANTE_COHORTE_MATRICULA EMC
                            WHERE   MC.ID = EC.IDCOHORTE
                            AND     EC.IDESTUDIANTE = E.ID
                            AND     EM.IDESTUDIANTE = E.ID
                            AND     EM.IDPARALELO = CP.ID
                            AND     EC.ID = EMC.IDESTUDCOHORTE
                            AND     EM.IDMAESTRIA = $MaestriaID
                            AND     CP.PARALELO = '$Paralelo'
                            AND     MC.ID = $CohorteID
                            ORDER BY E.APELLIDOS + ' ' + E.NOMBRES ASC";
                    
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definido estudiantes.");
            }
            
            function ConsultarMaestria($Params)
            {   $txt = $Params['txt'];
                
                $SQL = "SELECT M.ID AS IDMAESTRIA, UPPER(M.DESCRIPCION) AS MAESTRIA, M.IDESTADO, F.ID AS IDFACULTAD
                        FROM	POSTGRADO..ADM_MAESTRIA AS M,
                                POSTGRADO..ADM_MAESTRIA_VERSION MV,
                                POSTGRADO..ADM_FACULTAD F
                        WHERE	M.IDFACULTAD = F.ID	
                        AND	M.ID = MV.IDMAESTRIA 
                        AND	M.IDESTADO = 1 AND M.MAESTRIA LIKE '%$txt%'";
                
                $resourceID = $this->Works->Query($SQL);
                return $this->Works->FillDataRecordAssoc($resourceID,"No existe definida maestrias.");          
            }
            
            function ConsultarCohorteByMaestria($Params)
            {   $texto = $Params['txt'];
                $maestria = $Params['mtr'];
                $SQL = "SELECT MC.ID, MC.COHORTE, MV.VERSION AS VERSION_MAESTRIA, MC.IDESTADO, CC.ID AS IDCICLO
                        FROM	POSTGRADO..ADM_MAESTRIA M,
                                POSTGRADO..ADM_MAESTRIA_VERSION MV,
                                POSTGRADO..ADM_MAESTRIA_COHORTE MC,
                                POSTGRADO..COL_CICLOS CC
                        WHERE	M.ID = MV.IDMAESTRIA
                        AND	MV.ID = MC.IDMAESVER
                        AND	CC.IDCOHORTE = MC.ID
                        AND     MC.IDESTADO = 1
                        AND	MC.COHORTE LIKE '%$texto%'
                        AND     M.ID = $maestria";
                
                $resourceID = $this->Works->Query($SQL);
                return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido cohortes.");
            }
            
            function ConsultarParalelosyCohorte($IdCohorte)
            {
                $DataCollection = array();
                    $Counter = 0;
                    $SQL = "SELECT  CP.PARALELO
                            FROM    POSTGRADO..ADM_MAESTRIA_COHORTE MC,
                                    POSTGRADO..COL_COHORTE_PARALELO CP
                            WHERE   CP.IDCHORTE = MC.ID
                            AND     CP.IDESTADO = 1
                            AND     MC.ID = $IdCohorte"; 
                    
                    $DataCollection[] = array(0,"SELECCIONE...");
                    $resourceID = $this->Works->Query($SQL);
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Counter++;
                          $DataCollection[] = array($Counter,$Filas['PARALELO']);                 
                    }
                    $this->Works->CloseResource($resourceID);
                    return array(($Counter>0),$DataCollection);
            }
    }    
