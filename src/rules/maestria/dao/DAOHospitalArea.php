<?php
require_once("src/base/DBDAO.php");

class DAOHospitalArea extends DBDAO
{   private static $entityArea;
    private $Works;  
    
    public function __construct()
    {      self::$entityArea = DBDAO::getConnection();
            $this->Works = self::$entityArea->MyFunction();                
    }

    public function ObtenerAreaByID($id)
    {   $DSQL = "SELECT CM.ID, CM.AREA, CM.SIGLAS, CM.FECREACION, CM.IDESTADO
                        FROM POSTGRADO..COL_HOSPITAL_AREA AS CM
                        WHERE CM.ID = '$id'"; 
        $resourceID = $this->Works->Query($DSQL);
        return $this->Works->NextRecordObject($resourceID); 
    }

    public function ObtenerAreaByIDGuardado($id)
    {   $DSQL = "SELECT CM.ID, CM.AREA, CM.SIGLAS, CM.FECREACION, CM.IDESTADO
                        FROM POSTGRADO..COL_HOSPITAL_AREA AS CM
                        WHERE CM.ID = '$id'"; 
        $resourceID = $this->Works->Query($DSQL);
        return $this->Works->FillDataRecordAssoc($resourceID,"No existen un Area definido."); 
    }

    public function ObtenerAreaByDescripcion($PrepareDQL)
    {   $DSQL = "SELECT CH.ID, CH.AREA, CH.SIGLAS, CH.FECREACION,CH.IDESTADO
					FROM  POSTGRADO..COL_HOSPITAL_AREA AS CH
					WHERE ID > 0
					AND CH.AREA LIKE '%".$PrepareDQL['texto']."%'";                                            
		$resourceID = $this->Works->Query($DSQL);                  
		return $this->Works->FillDataRecordAssoc($resourceID,"No existen Areas definidos."); 
	}
    
    public function InsertaArea($Form)
    {   $Modulo = json_decode(Session::getValue('Sesion'));
		$Descrp = strtoupper($Form->descripcion);
		$Siglas = strtoupper($Form->siglas);
		$QUERY = "INSERT INTO POSTGRADO..COL_HOSPITAL_AREA(AREA,SIGLAS,IDUSCREA,FECREACION) 
					OUTPUT INSERTED.ID 
					VALUES('$Descrp','$Siglas','".$Modulo->Idusrol."',getDate())";
		$resourceID = $this->Works->Query($QUERY);
		return $this->Works->FieldDataByName($resourceID,'ID');
    }

    public function ActualizaArea($Form)
    {   $Modulo = json_decode(Session::getValue('Sesion'));
		$Descrp = utf8_decode(strtoupper($Form->descripcion));
		$Siglas = strtoupper($Form->siglas);
		$QUERY = "UPDATE POSTGRADO..COL_HOSPITAL_AREA SET 
					AREA='$Descrp',
					SIGLAS='$Siglas',
					IDUSMODIFICA='".$Modulo->Idusrol."',
					IDESTADO='$Form->estado',    
					FEMODIFICA=getDate() 
					WHERE ID='$Form->id'";
		$resourceID = $this->Works->Query($QUERY); 
		return intval($Form->id);
    }
    
	public function DesactivaArea($id)
	{  	$Modulo = json_decode(Session::getValue('Sesion'));
		$SQL = "UPDATE POSTGRADO..COL_HOSPITAL_AREA 
			SET IDESTADO=0, IDUSMODIFICA='".$Modulo->Idusrol."', FEMODIFICA=getDate() WHERE id='$id' ";
		$resourceID = $this->Works->Query($SQL);    
		return 0;                    
	}
}
?>        
