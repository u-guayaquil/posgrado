<?php
        require_once("src/base/DBDAO.php");
        
        class DAOReporte extends DBDAO
        {   private static $entitySilabo;
            private $Works;  
            
            public function __construct()
            {      self::$entitySilabo = DBDAO::getConnection();
                   $this->Works = self::$entitySilabo->MyFunction();                
            }

            function ObtenerDatosCabeceraActas($Params)
            {   $SQL = "SELECT A.DESCRIPCION AS FACULTAD,B.DESCRIPCION AS MAESTRIA,D.COHORTE,E.PARALELO,F.CICLO,H.MATERIA,J.DESCRIPCION AS TIPOMALLA, L.APELLIDOS+' '+L.NOMBRES AS DOCENTE,MIN(N.FECHA) AS DESDE, MAX(N.FECHA) AS HASTA
                            FROM 
                                POSTGRADO..ADM_FACULTAD AS A, POSTGRADO..ADM_MAESTRIA AS B, POSTGRADO..ADM_MAESTRIA_VERSION AS C,
                                POSTGRADO..ADM_MAESTRIA_COHORTE AS D, POSTGRADO..COL_COHORTE_PARALELO AS E,
                                POSTGRADO..COL_CICLOS AS F, POSTGRADO..COL_MALLA_DETALLE AS G,
                                POSTGRADO..COL_MATERIA AS H, POSTGRADO..COL_MALLA AS I, POSTGRADO..COL_MALLA_TIPO J,
                                POSTGRADO..COL_DOCENTE AS L, POSTGRADO..COL_PLAN_ANALITICO AS M, POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS N
                            WHERE 
                                A.ID=B.IDFACULTAD AND B.ID=$Params[2] AND B.ID=C.IDMAESTRIA AND C.IDESTADO IN (1,11) AND 
                                C.ID=D.IDMAESVER  AND D.IDESTADO<>0 AND D.ID=E.IDCHORTE AND E.ID=$Params[4] AND 
                                F.ID=$Params[1] AND F.IDCOHORTE=D.ID AND G.IDMALLA=D.IDMALLA AND G.NIVEL=F.NIVEL AND G.IDMATERIA=$Params[3] AND 
                                H.ID=G.IDMATERIA AND I.ID=G.IDMALLA AND I.IDTIPOMALLA=J.ID AND
                                M.IDESTADO=1 AND M.IDCICLO=F.ID AND M.IDMAESTRIA=B.ID AND M.IDMATERIA=G.IDMATERIA AND M.IDPARALELO=E.ID AND	  
                                M.ID=N.IDPLANANA AND N.IDESTADO=1 AND L.ID=M.IDDOCENTE
                            GROUP BY A.DESCRIPCION,B.DESCRIPCION,D.COHORTE,E.PARALELO,F.CICLO,H.MATERIA,J.DESCRIPCION,L.APELLIDOS+' '+L.NOMBRES";
                    $resourceID = $this->Works->Query($SQL);

                        
                    $DataCount = 0;
                    $DataCollection = array();
                    while($filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $DataCount++;
                                
                          $filas["DOCENTE"]=utf8_encode($filas["DOCENTE"]);      
                          $DataCollection[] = $filas;
                    }
                    
                    $this->Works->CloseResource($resourceID);
                    $Result = ($DataCount>0);
                    return array($Result,($Result ? $DataCollection : "No existen datos para el resporte."));            
            }
            
            function ObtenerDatosCabeceraEstudiante($Params)
            {       //var Params = [MAEST,COHORTE,PARALELO,CICLO,FACULTAD];  
                    $IDMaestria = $Params[0];   $IDParalelo = $Params[2];    $IDCiclo = $Params[3];
                    $SQL = "SELECT A.DESCRIPCION AS FACULTAD,B.DESCRIPCION AS MAESTRIA,D.COHORTE,E.PARALELO,F.CICLO,
                                    J.DESCRIPCION AS TIPOMALLA, MIN(N.FECHA) AS DESDE, MAX(N.FECHA) AS HASTA
                            FROM 
                                POSTGRADO..ADM_FACULTAD AS A,		POSTGRADO..ADM_MAESTRIA AS B,		POSTGRADO..ADM_MAESTRIA_VERSION AS C,
                                POSTGRADO..ADM_MAESTRIA_COHORTE AS D,	POSTGRADO..COL_COHORTE_PARALELO AS E,   POSTGRADO..COL_CICLOS AS F, 
                                POSTGRADO..COL_MALLA_DETALLE AS G,	POSTGRADO..COL_MALLA AS I,		POSTGRADO..COL_MALLA_TIPO J,			
                                POSTGRADO..COL_PLAN_ANALITICO AS M, 
                                POSTGRADO..COL_PLAN_ANALITICO_DETALLE AS N
                            WHERE	
                                    A.ID=B.IDFACULTAD		AND B.ID='$IDMaestria'	AND B.ID=C.IDMAESTRIA	AND C.IDESTADO IN (1,11) 
                                    AND C.ID=D.IDMAESVER	AND D.IDESTADO<>0	AND D.ID=E.IDCHORTE	AND E.PARALELO LIKE'$IDParalelo'    AND F.ID='$IDCiclo'
                                    AND F.IDCOHORTE=D.ID	AND G.IDMALLA=D.IDMALLA AND G.NIVEL=F.NIVEL	AND I.ID=G.IDMALLA		
                                    AND I.IDTIPOMALLA=J.ID	AND M.IDESTADO=1	AND M.IDCICLO=F.ID	AND M.IDMAESTRIA=B.ID	
                                    AND M.IDMATERIA=G.IDMATERIA AND M.IDPARALELO=E.ID 	AND M.ID=N.IDPLANANA	AND N.IDESTADO=1
                            GROUP BY A.DESCRIPCION,B.DESCRIPCION,D.COHORTE,E.PARALELO,F.CICLO,J.DESCRIPCION";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen datos para el resporte.");            
            }
        }
?>        
