<?php
        require_once("src/base/DBDAO.php");        

        class DAOAsistenciaPermisos extends DBDAO
        {       
                private $Works;
                private static $entityAsistenciaPermisos;
                
                public function __construct()
                {       self::$entityAsistenciaPermisos = DBDAO::getConnection();
                        $this->Works = self::$entityAsistenciaPermisos->MyFunction();                
                }
            
                function ObtenerSolicitudes($Params)
                {       $SQLFacultd = "";
                
                        if (array_key_exists("facultad", $Params))
                        {   if (count($Params['facultad'])>0)
                            {   $FacultadID = implode(',',$Params['facultad']);
                                $SQLFacultd = " AND F.ID IN ($FacultadID) ";
                            }
                        }
                    
                        $SQL = "SET DATEFIRST 7; SELECT X.ID, Y.APELLIDOS+' '+Y.NOMBRES AS DOCENTE,T.COHORTE,F.ID AS IDFAC,F.DESCRIPCION AS FACULTAD,U.CICLO, Z.DESCRIPCION, W.MATERIA, V.PARALELO, X.OBSERVACION, X.FECHA,X.FEUSCREA,X.IDESTADO, DATEPART(dw,X.FECHA) AS DIASEMANA 
                                FROM   POSTGRADO..COL_PERMISOS_REG_ASISTENCIA AS X, POSTGRADO..COL_DOCENTE Y, POSTGRADO..ADM_MAESTRIA Z, POSTGRADO..COL_MATERIA W,
                                       POSTGRADO..COL_COHORTE_PARALELO AS V, POSTGRADO..COL_CICLOS AS U, POSTGRADO..ADM_MAESTRIA_COHORTE AS T,POSTGRADO..ADM_FACULTAD AS F
                                WHERE  X.IDDOCENTE=Y.ID AND X.IDMAESTRIA=Z.ID AND X.IDMATERIA=W.ID AND X.IDGRUPO=V.ID AND X.IDCICLO=U.ID AND Z.IDFACULTAD=F.ID AND
                                       U.IDCOHORTE=T.ID AND X.IDESTADO=1 AND X.IDTIPO='CD' ". $SQLFacultd." 
                                ORDER BY Z.IDFACULTAD,X.FECHA,X.FEUSCREA";  
                               
                        $resourceID = $this->Works->Query($SQL);                  
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen solicitudes para registro de asistencias.");
                }
                
                function ActualizaAsistenciaPermisos($Form)
                {       $Modulo = json_decode(Session::getValue('Sesion'));

                        $Utils = new Utils();
                        $Form[2] = utf8_decode($Utils->ConvertTildesToUpper($Form[2]));
                
                        $SQL = "UPDATE POSTGRADO..COL_PERMISOS_REG_ASISTENCIA SET RESPUESTA='$Form[2]', IDESTADO='$Form[1]', IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate() WHERE ID='$Form[0]'";
                       
                        $resourceID = $this->Works->Query($SQL);
                        if ($resourceID)
                            return array(true,"Se actualizo la información correctamente.");
                        else
                            return array(false,"No se pudo actualizar la información.");
                }
        }
?>        
