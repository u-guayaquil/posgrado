<?php
        require_once("src/base/DBDAO.php");
        
        class DAOGrupo extends DBDAO
        {   private static $entityGrupo;
            private $Works;  
            
            public function __construct()
            {      self::$entityGrupo = DBDAO::getConnection();
                   $this->Works = self::$entityGrupo->MyFunction();                
            }

            public function ObtenerGrupoByCohorte($idCohorte)
            {       $SQL = "SELECT X.ID,X.PARALELO FROM POSTGRADO..COL_COHORTE_PARALELO AS X WHERE X.IDCHORTE='$idCohorte' AND X.IDESTADO=1";        
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido grupos para la cohorte.");            
            }
            
            public function ObtenerGrupoByCiclo($idCiclo)
            {       $SQL = "SELECT X.ID,X.PARALELO "; 
                    $SQL.= "FROM POSTGRADO..COL_COHORTE_PARALELO AS X, ";
                    $SQL.= "POSTGRADO..COL_CICLOS AS Y ";
                    $SQL.= "WHERE Y.ID='$idCiclo' AND Y.IDESTADO=1 AND Y.IDCOHORTE=X.IDCHORTE AND X.IDESTADO=1 ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido grupos para la ciclo-cohorte.");            
            }
        }
?>        
