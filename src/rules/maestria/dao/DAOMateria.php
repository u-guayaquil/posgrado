<?php
        require_once("src/base/DBDAO.php");
        
        class DAOMateria extends DBDAO
        {   private static $entityMateria;
            private $Works;  
            
            public function __construct()
            {      self::$entityMateria = DBDAO::getConnection();
                   $this->Works = self::$entityMateria->MyFunction();                
            }

            public function ObtenerMateriaByMaestria($id) //ok
            {      $resourceID = $this->Works->Query("SELECT X.ID,X.MATERIA,X.IDESTADO FROM POSTGRADO..COL_MATERIA AS X WHERE X.IDMAESTRIA='$id' ORDER BY X.MATERIA");
                   return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias para la maestria seleccionada.");
            }
            
            public function ObtenerMateriaMallaByMaestriaVersion($prepareDQL)
            {       $txMst = (key_exists('texto', $prepareDQL) ? " AND Z.MATERIA LIKE '%".$prepareDQL['texto']."%' " : " ");     
                    $idMvr = (key_exists('maesver', $prepareDQL) ? $prepareDQL['maesver'] : 0);
                        
                    $SQL = "SELECT Z.ID,Z.MATERIA,Z.IDESTADO,Q.DESCRIPCION AS ESTADO, Y.NIVEL, Y.NIVELORDEN
                            FROM POSTGRADO..COL_MALLA AS X,
                                 POSTGRADO..COL_MALLA_DETALLE AS Y,
                                 POSTGRADO..COL_MATERIA AS Z,
                                 BdCert..GEN_ESTADO AS Q
                            WHERE X.IDMAESVER=$idMvr AND X.IDESTADO=1 AND X.ID=Y.IDMALLA AND Y.IDMATERIA=Z.ID AND Q.ID=Z.IDESTADO".$txMst." 
			    ORDER BY Y.NIVEL,Y.NIVELORDEN ";       
                          
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias en la malla para la maestria seleccionada.");       
            }

            public function ObtenerMateriaByID($id)
            {          
                    $DSQL = "SELECT TOP 50 CM.ID, CM.MATERIA, CM.FEUSCREA, CM.IDESTADO, M.MAESTRIA, M.ID AS IDMAESTRIA
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..COL_MATERIA AS CM
                                WHERE CM.IDMAESTRIA = M.ID
                                AND CM.ID = '$id'"; 
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function ObtenerMateriaByidByMaestria($Form)
            {                
                    $id = $Form["id"];
                    $idMaestria = $Form["id_maestria"];

                    $DSQL = "SELECT TOP 50 CM.ID, CM.MATERIA, CM.FEUSCREA, CM.IDESTADO, M.MAESTRIA, M.ID AS IDMAESTRIA
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..COL_MATERIA AS CM
                                WHERE CM.IDMAESTRIA = M.ID
                                AND CM.ID LIKE '%$id%'
                                AND CM.IDMAESTRIA = '$idMaestria'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->NextRecordObject($resourceID); 
            }

            public function ObtenerMaterias()
            {       $Registro = array();
                    $resourceID = $this->Works->Query("SELECT CM.ID, CM.MATERIA FROM POSTGRADO..COL_MATERIA CM WHERE CM.IDESTADO = 1");
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }

            public function ObtenerMateriaByDescripcion($PrepareDQL)
            {                                                     
                    $DSQL = "SELECT TOP 50 CM.ID, CM.MATERIA, CM.FEUSCREA, CM.IDESTADO, M.MAESTRIA, M.ID AS IDMAESTRIA
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..COL_MATERIA AS CM
                                WHERE CM.IDMAESTRIA = M.ID
                                AND CM.MATERIA LIKE '%".$PrepareDQL['texto']."%'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias definidas."); 
            }
            
            public function ObtenerMateriaByDescripcionByMaestria($Form)
            {                
                    $Text = trim($Form["texto"]);
                    $idMaestria = $Form["id"];

                    $DSQL = "SELECT TOP 50 CM.ID, CM.MATERIA, CM.FEUSCREA, CM.IDESTADO, M.MAESTRIA, M.ID AS IDMAESTRIA
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..COL_MATERIA AS CM
                                WHERE CM.IDMAESTRIA = M.ID
                                AND CM.MATERIA LIKE '%".$Text."%'
                                AND CM.IDMAESTRIA = $idMaestria";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias definidas."); 
            }
            
            public function ObtenerMaestriaByDescripcion($PrepareDQL)
            {                                                  
                    $SQL = "SELECT M.ID AS IDMAESTRIA, M.MAESTRIA, F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD ";
                    $SQL.= "FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F ";
                    $SQL.= "WHERE M.IDFACULTAD = F.ID ";
                    $SQL.= "AND M.DESCRIPCION LIKE '%".$PrepareDQL['texto']."%' ";
                    $SQL.= "ORDER BY M.ID ASC ";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen maestrias definidas."); 
            }

            public function ObtenerMateriaEstadoByID($id)
            {          
                    $DSQL = "SELECT TOP 50 CM.ID, CM.MATERIA, CM.FEUSCREA, CM.IDESTADO, M.MAESTRIA, M.ID AS IDMAESTRIA
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..COL_MATERIA AS CM
                                WHERE CM.IDMAESTRIA = M.ID
                                AND CM.ID = '$id'"; 
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias definidas."); 
            }
            
            public function ObtenerMateriaEstadoByMaestria($idMaestria)
            {          
                    $DSQL = "SELECT TOP 50 CM.ID, CM.MATERIA, CM.FEUSCREA, CM.IDESTADO, M.MAESTRIA, M.ID AS IDMAESTRIA
                                FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..COL_MATERIA AS CM
                                WHERE CM.IDMAESTRIA = M.ID
                                AND M.ID = '$idMaestria'";
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias definidas por maestria."); 
            }
            
            public function InsertaMateria($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = strtoupper($Form->descripcion);
                    $idMaestria = $Form->idmaestria;
                    $resourceID = $this->Works->Query("insert into POSTGRADO..COL_MATERIA(IDMAESTRIA,MATERIA,IDUSCREA,FEUSCREA) OUTPUT INSERTED.ID values('$idMaestria','$Descrp','".$Modulo->Idusrol."',getDate())");
                    return $this->Works->FieldDataByName($resourceID,'ID');
            }
    
            public function ActualizaMateria($materia)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = utf8_decode(strtoupper($materia->descripcion));
                    $IdMaestria = $materia->idmaestria;
                    $resourceID = $this->Works->Query("Update POSTGRADO..COL_MATERIA set 
                                                       MATERIA='$Descrp',
                                                       IDMAESTRIA='$IdMaestria',
                                                       IDUSMODIFICA='".$Modulo->Idusrol."',
                                                       IDESTADO='$materia->estado',    
                                                       FEUSMODIFICA=getDate() 
                                                       WHERE ID='$materia->id'"); 
                    return intval($materia->id);
            }
            
            public function DesactivaMateria($id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("update POSTGRADO..COL_MATERIA set IDESTADO=0, IDUSMODIFICA='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() where id='$id' ");    
                    return 0;                    
            }
            
            public function ObtenerUltimaMateriaByMaestria($idmaestria)
            {      $DSQL = "SELECT TOP 1 ID FROM POSTGRADO..COL_MATERIA WHERE IDMAESTRIA = $idmaestria ORDER BY ID DESC";
                   $resourceID = $this->Works->Query($DSQL);                  
                   return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function ObtenerDatosMallDet($id)
            {      $DSQL = "SELECT * FROM POSTGRADO..COL_MALLA_DETALLE WHERE IDMATERIA = $id";
                   $resourceID = $this->Works->Query($DSQL);                  
                   return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function GuardaMateriaEnMallaDetalle($DatosMallDetByMateria,$id)
            {       $SQL = "INSERT INTO POSTGRADO..COL_MALLA_DETALLE (IDMALLA, IDMATERIA, IDTPMATERIA, NIVEL, NIVELORDEN)
                                VALUES ($DatosMallDetByMateria->IDMALLA,$id,$DatosMallDetByMateria->IDTPMATERIA,$DatosMallDetByMateria->NIVEL,$DatosMallDetByMateria->NIVELORDEN+1)";
                    $this->Works->Query($SQL);
                    return 0;
            }

            public function GuardaMateriaEnNuevaMallaDetalle($DatosMalla,$idMaestria)
            {       $SQL = "INSERT INTO POSTGRADO..COL_MALLA_DETALLE (IDMALLA, IDMATERIA, IDTPMATERIA, NIVEL, NIVELORDEN)
                                VALUES ($DatosMalla->ID,$idMaestria,'1','1','1')";
                    $this->Works->Query($SQL);
                    return 0;
            }

            public function ObtenerMateriaMalla($idMateria)
            {   $DSQL = "SELECT * FROM POSTGRADO..COL_MALLA_DETALLE WHERE IDMATERIA = $idMateria";
                $resourceID = $this->Works->Query($DSQL);                  
                return $this->Works->NextRecordObject($resourceID); 
            }

            public function ObtenerMallaByMaestria($idMaestria)
            {      $DSQL = "SELECT ML.ID,ML.DESCRIPCION FROM POSTGRADO..COL_MALLA ML
                                LEFT JOIN POSTGRADO..ADM_MAESTRIA_VERSION MV ON MV.ID = ML.IDMAESVER 
                                LEFT JOIN POSTGRADO..ADM_MAESTRIA M ON M.ID = MV.IDMAESTRIA
                                WHERE M.ID = $idMaestria";
                   $resourceID = $this->Works->Query($DSQL);                  
                   return $this->Works->NextRecordObject($resourceID); 
            }
        }
?>        
