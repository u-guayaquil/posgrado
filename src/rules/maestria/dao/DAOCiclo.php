<?php
        require_once("src/base/DBDAO.php");
        
        class DAOCiclo extends DBDAO
        {   private static $entityCiclo;
            private $Works;  
            
            public function __construct()
            {      self::$entityCiclo = DBDAO::getConnection();
                   $this->Works = self::$entityCiclo->MyFunction();                
            }

            public function ObtenerCicloByMaestriaVersion($idMaver)
            {       $SQL = "SELECT X.ID AS IDCICLO,ISNULL(Z.ID,0) AS IDMALLA,ISNULL(Z.DESCRIPCION,'MALLA INACTIVA') AS MALLA,X.CICLO+' ('+Y.COHORTE+')' AS CICLO,X.IDCOHORTE
                            FROM 
                            POSTGRADO..COL_CICLOS AS X 
                            INNER JOIN (POSTGRADO..ADM_MAESTRIA_COHORTE AS Y 
                            LEFT JOIN   POSTGRADO..COL_MALLA AS Z 
                            ON Y.IDMALLA=Z.ID)
                            ON X.IDCOHORTE=Y.ID AND Y.IDESTADO=1 AND Y.IDMAESVER='$idMaver' AND X.IDESTADO=1 ";      
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido ciclo para la maestria.");            
            }
            
            public function ObtenerCicloByID($id)
            {          
                    $DSQL = "SELECT TOP 50 CC.ID, CC.CICLO, CC.NIVEL, CC.IDCOHORTE AS IDCOHORTE, MC.COHORTE, CC.FEINICIO AS INICIO, CC.FEFIN AS FIN, CC.IDESTADO
                                FROM POSTGRADO..COL_CICLOS AS CC, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CC.IDCOHORTE = MC.ID
                                AND CC.ID = '$id'"; 
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function ObtenerCicloByidByCohorte($Form)
            {                
                    $id = $Form["id"];
                    $idCohorte = $Form["idcohorte"];

                    $DSQL = "SELECT TOP 50 CC.ID, CC.CICLO, CC.NIVEL, CC.IDCOHORTE AS IDCOHORTE, MC.COHORTE, CC.FEINICIO AS INICIO, CC.FEFIN AS FIN, CC.IDESTADO
                                FROM POSTGRADO..COL_CICLOS AS CC, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CC.IDCOHORTE = MC.ID
                                AND CC.ID LIKE '%$id%'
                                AND CC.IDCOHORTE = '$idCohorte'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->NextRecordObject($resourceID); 
            }

            public function ObtenerCiclos()
            {       $Registro = array();
                    $resourceID = $this->Works->Query("SELECT CC.ID, CC.CICLO, CC.NIVEL, C.IDCOHORTE, C.FEINICIO, C.FEFIN FROM POSTGRADO..COL_CICLOS AS CC WHERE C.IDESTADO = 1");
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }

            public function ObtenerCicloByDescripcion($PrepareDQL)
            {       $DSQL = "SELECT TOP 50 CC.ID, CC.CICLO, CC.NIVEL, CC.IDCOHORTE AS IDCOHORTE, MC.COHORTE, CC.FEINICIO AS INICIO, CC.FEFIN AS FIN, CC.IDESTADO
                                FROM POSTGRADO..COL_CICLOS AS CC, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CC.IDCOHORTE = MC.ID
                                AND CC.PERIODO LIKE '%".$PrepareDQL['texto']."%'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen ciclos definidas."); 
            }
            
            public function ObtenerCicloByDescripcionByCohorte($Form)
            {                
                    $Text = trim($Form["texto"]);
                    $idCohorte = $Form["idcohorte"];

                    $DSQL = "SELECT TOP 50 CC.ID, CC.CICLO, CC.NIVEL, CC.IDCOHORTE AS IDCOHORTE, MC.COHORTE, CC.FEINICIO AS INICIO, CC.FEFIN AS FIN, CC.IDESTADO
                                FROM POSTGRADO..COL_CICLOS AS CC, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CC.IDCOHORTE = MC.ID
                                AND CC.CICLO LIKE '%".$Text."%'
                                AND CC.IDCOHORTE = $idCohorte";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen ciclos definidos."); 
            }
            
            public function ObtenerCohorteByDescripcion($PrepareDQL)
            {                                                  
                    $SQL = "SELECT MC.ID, M.ID AS IDMAESTRIA, M.MAESTRIA, MC.IDMAESVER, MV.VERSION, MC.COHORTE, MC.FEINICIO, MC.FEFIN, MC.IDESTADO ";
                    $SQL.= "FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS MC, POSTGRADO..ADM_MAESTRIA_VERSION AS MV, POSTGRADO..ADM_MAESTRIA AS M ";
                    $SQL.= "WHERE MC.IDMAESVER = MV.ID ";
                    $SQL.= "AND MV.IDMAESTRIA = M.ID ";
                    $SQL.= "AND M.MAESTRIA LIKE '%".$PrepareDQL['texto']."%' ";
                    $SQL.= "ORDER BY MC.ID ASC  ";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen cohortes definidos definidos."); 
            }

            public function ObtenerCicloEstadoByID($id)
            {          
                    $DSQL = "SELECT TOP 50 CC.ID, CC.CICLO, CC.NIVEL, CC.IDCOHORTE AS IDCOHORTE, MC.COHORTE, CC.FEINICIO AS INICIO, CC.FEFIN AS FIN, CC.IDESTADO
                                FROM POSTGRADO..COL_CICLOS AS CC, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CC.IDCOHORTE = MC.ID
                                AND CC.ID = '$id'"; 
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen ciclos definidos."); 
            }
            
            public function ObtenerCicloEstadoByCohorte($idCohorte)
            {                
                    $DSQL = "SELECT TOP 50 CC.ID, CC.CICLO, CC.NIVEL, CC.IDCOHORTE AS IDCOHORTE, MC.COHORTE, CC.FEINICIO AS INICIO, CC.FEFIN AS FIN, CC.IDESTADO
                                FROM POSTGRADO..COL_CICLOS AS CC, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                                WHERE CC.IDCOHORTE = MC.ID
                                AND CC.IDCOHORTE = '$idCohorte'";
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen ciclos definidos por cohorte."); 
            }
            
            public function InsertaCiclo($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
            
                    $Descrp = strtoupper($Form->descripcion);
                    $Nivel = $Form->nivel;
                    $idCohorte = $Form->idcohorte;
                    $FeInicio = $Form->feinicio;
                    $FeFin = $Form->fefin;
                    
                    $resourceID = $this->Works->Query("insert into POSTGRADO..COL_CICLOS(IDCOHORTE,CICLO,NIVEL,FEINICIO,FEFIN,IDUSCREA,FEUSCREA) OUTPUT INSERTED.ID values('$idCohorte','$Descrp','$Nivel','$FeInicio','$FeFin','".$Modulo->Idusrol."',getDate())");
                    return $this->Works->FieldDataByName($resourceID,'ID');
            }
    
            public function ActualizaCiclo($ciclo)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = utf8_decode(strtoupper($ciclo->descripcion));
                    $IdCohorte = $ciclo->idcohorte;
                    $Nivel = $ciclo->nivel;
                    $FeInicio = $ciclo->feinicio;
                    $FeFin = $ciclo->fefin;
                    
                    $resourceID = $this->Works->Query("Update POSTGRADO..COL_CICLOS set 
                                                       CICLO='$Descrp',
                                                       NIVEL='$Nivel',
                                                       IDCOHORTE='$IdCohorte',
                                                       FEINICIO='$FeInicio',
                                                       FEFIN='$FeFin',
                                                       IDUSMODIFICA='".$Modulo->Idusrol."',
                                                       IDESTADO='$ciclo->estado',    
                                                       FEUSMODIFICA=getDate() 
                                                       WHERE ID='$ciclo->id'"); 
                    return intval($ciclo->id);
            }
            
            public function DesactivaCiclo($id)
            {   $Modulo = json_decode(Session::getValue('Sesion'));
                $resourceID = $this->Works->Query("update POSTGRADO..COL_CICLOS set IDESTADO=0, IDUSMODIFICA='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() where id='$id' ");    
                return 0;                    
            }
            
            public function ObtenerFechaFinUltimoCiclo($idCohorte)
            {
                $DSQL = "SELECT TOP 1 CC.FEFIN
                            FROM POSTGRADO..COL_CICLOS AS CC, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                            WHERE CC.IDCOHORTE = MC.ID
                            AND CC.IDCOHORTE = '$idCohorte'
                            ORDER BY CC.ID DESC"; 
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function ObtenerRangoCicloByCohorte($idCohorte)
            {
                $DSQL = "SELECT TOP 1 MC.FEINICIO, MC.FEFIN
                FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                WHERE MC.ID = '$idCohorte'
                AND MC.IDESTADO = 1
                ORDER BY MC.ID DESC";

                /*$DSQL = "SELECT TOP 1 MC.FEINICIO, MC.FEFIN
                    FROM POSTGRADO..COL_CICLOS AS CC, POSTGRADO..ADM_MAESTRIA_COHORTE AS MC
                    WHERE CC.IDCOHORTE = MC.ID
                    AND CC.IDCOHORTE = '$idCohorte' AND MC.IDESTADO = 1
                    ORDER BY CC.ID DESC"; */
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }
        }
?>        
