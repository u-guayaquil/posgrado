<?php
        require_once("src/base/DBDAO.php");
        
        class DAOMatInformacion extends DBDAO
        {   private static $entityMatInformacion;
            private $Works;  
            
            public function __construct()
            {      self::$entityMatInformacion = DBDAO::getConnection();
                   $this->Works = self::$entityMatInformacion->MyFunction();                
            }

            public function ObtenerMaestriaVersionByFacultadSinCohorte($prepareDQL) //Ok
            {       $CD1 = ""; $CD2 = "";
                    if (key_exists('facultad', $prepareDQL))
                    {   $FID = implode(',',$prepareDQL['facultad']);
                        $CD1 = " W.ID IN ($FID) AND ";
                    }
                    if (key_exists('texto', $prepareDQL))
                    {   $CD2 = " AND (Z.DESCRIPCION LIKE '%".$prepareDQL['texto']."%') ";
                    }
                
                    $SQL = "SELECT DISTINCT Z.ID AS IDM, UPPER(Z.DESCRIPCION) AS MAESTRIA, Y.ID AS IDV, 
                                Y.VERSION AS TXVERSION, UPPER(W.DESCRIPCION) AS FACULTAD, 
                                W.ID AS IDFACULTAD
                           FROM POSTGRADO..COL_MAT_CONT_GENERAL AS X, POSTGRADO..ADM_MAESTRIA_VERSION AS Y, 
                                POSTGRADO..ADM_MAESTRIA AS Z, POSTGRADO..ADM_FACULTAD AS W
                           WHERE ".$CD1." X.IDESTADO=1 
                                AND X.IDMAESVER=Y.ID AND Y.IDMAESTRIA=Z.ID AND Z.IDFACULTAD=W.ID
                                AND Y.IDESTADO=1".$CD2."
                           ORDER BY MAESTRIA, FACULTAD ";
   
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe maestrias para listas para definir su contenido.");
            }

            public function ObtenerMaestriaVersionByFacultad($prepareDQL) //Ok
            {       $CD1 = ""; $CD2 = "";
                    if (key_exists('facultad', $prepareDQL))
                    {   $FID = implode(',',$prepareDQL['facultad']);
                        $CD1 = " W.ID IN ($FID) AND ";
                    }
                    if (key_exists('texto', $prepareDQL))
                    {   $CD2 = " AND (Z.DESCRIPCION LIKE '%".$prepareDQL['texto']."%') ";
                    }
                
                    $SQL = "SELECT DISTINCT Z.ID AS IDM, UPPER(Z.DESCRIPCION) AS MAESTRIA, Y.ID AS IDV, 
                                Y.VERSION AS TXVERSION, UPPER(W.DESCRIPCION) AS FACULTAD, 
                                W.ID AS IDFACULTAD, CM.ID AS IDC, CM.COHORTE
                           FROM POSTGRADO..COL_MAT_CONT_GENERAL AS X, POSTGRADO..ADM_MAESTRIA_VERSION AS Y, 
                                POSTGRADO..ADM_MAESTRIA AS Z, POSTGRADO..ADM_FACULTAD AS W, 
                                POSTGRADO..ADM_MAESTRIA_COHORTE AS CM
                           WHERE ".$CD1." X.IDESTADO=1 
                                AND X.IDMAESVER=Y.ID AND Y.IDMAESTRIA=Z.ID AND Z.IDFACULTAD=W.ID
                                AND Y.ID = CM.IDMAESVER
                                AND Y.IDESTADO=1".$CD2."
                           ORDER BY MAESTRIA, FACULTAD ";
   
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe maestrias para listas para definir su contenido.");
            }
            
            public function ObtenerMateriaByMaestriaVersion($prepareDQL)
            {       $Mvr = 0; $Mat = "";$Cht = $prepareDQL['cohorte'];
                    if (key_exists('maestver', $prepareDQL))
                        $Mvr = $prepareDQL['maestver'];
                    if (key_exists('texto', $prepareDQL))
                        $Mat = " AND (Y.MATERIA LIKE '%".$prepareDQL['texto']."%') ";
                
                    $SQL = "SELECT X.ID,Y.ID AS MID,Y.MATERIA,W.NIVEL,W.NIVELORDEN,X.NIVCONT          
                            FROM POSTGRADO..COL_MALLA AS V, POSTGRADO..COL_MALLA_DETALLE AS W, POSTGRADO..COL_MAT_CONT_GENERAL AS X,
                                 POSTGRADO..COL_MATERIA AS Y, POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA_COHORTE MC
                            WHERE V.IDESTADO=1 AND V.ID=W.IDMALLA AND W.IDMATERIA=X.IDMATERIA AND X.IDESTADO=1 AND  
                                  X.IDMAESVER=$Mvr AND X.IDMATERIA=Y.ID AND V.IDMAESVER=MV.ID AND 
                                  X.IDMAESVER=MV.ID AND MV.ID=MC.IDMAESVER AND MC.ID = $Cht
                                  AND V.IDMAESVER=X.IDMAESVER".$Mat."
                            ORDER BY W.NIVEL,W.NIVELORDEN ASC ";
                    $resourceID = $this->Works->Query($SQL);    
                    return $this->Works->FillDataRecordAssoc($resourceID,"No se han definido materias para la maestria seleccionada.");
            }
            
            public function ObtenerObjetivosByMateriaVersion($idVersion,$IdMateria) //Ok
            {       $SQL = "SELECT 
                           upper(X.REQUISITO) AS REQUISITO, upper(X.ORGCURRICULAR) AS ORGCURRICULAR,
                           X.COMPONENTECD, X.COMPONENTECP, X.COMPONENTECA, 
                           UPPER(X.OBGENERAL) AS OBGENERAL, UPPER(X.OBESPECIFICO) AS OBESPECIFICO, UPPER(X.METODOLOGIA) AS METODOLOGIA, 
                           UPPER(X.LINEA) AS LINEA,X.NIVCONT,X.NIVPLAN
                           FROM  POSTGRADO..COL_MAT_CONT_GENERAL AS X
                           WHERE X.IDESTADO=1 AND X.IDMAESVER='$idVersion' AND  X.IDMATERIA='$IdMateria'"; // 
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definida información para la materia seleccionada.");            
            }

            public function ObtenerBibliografiaByMateriaVersion($idVersion,$IdMateria) //Ok
            {       $SQL = "SELECT Y.TIPO,Y.LIBRO,Y.EDICION,Y.EXISTENBIBLIOTECA,Y.EJEMPLARES
                            FROM   POSTGRADO..COL_MAT_CONT_GENERAL AS X, POSTGRADO..COL_MAT_CONT_BIBLIOGRAFIA AS Y
                            WHERE  X.IDESTADO=1  AND X.IDMAESVER='$idVersion' AND  X.IDMATERIA='$IdMateria' AND X.ID=Y.IDMATCONT"; //
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido Bibliografia para la Materia.");            
            }            
            
            public function Inserta($Form,$Libros)
            {       $Modulo = json_decode(Session::getValue('Sesion')); //,,
                    $SQL="INSERT INTO POSTGRADO..COL_MAT_CONT_GENERAL
                          (IDMAESVER,
                           IDMATERIA,
                           REQUISITO,
                           ORGCURRICULAR,
                           COMPONENTECD,
                           COMPONENTECP,
                           COMPONENTECA,
                           OBGENERAL,
                           NIVCONT,
                           NIVPLAN,
                           OBESPECIFICO,
                           METODOLOGIA,
                           LINEA,
                           IDUSCREA,
                           FEUSCREA)
                           OUTPUT Inserted.ID
                           VALUES('$Form->idversion',
                           '$Form->idmateria',
                           '$Form->requisito',
                           '$Form->orgcurricular',
                           '$Form->componentecd',
                           '$Form->componentecp',
                           '$Form->componenteca',
                           '$Form->obgeneral',
                           '$Form->nivcon',
                           '$Form->nivpln',
                           '$Form->obespecifico',
                           '$Form->metodologia',
                           '$Form->linea',
                           '$Modulo->Idusrol',
                            getDate())";
                    $resourceID = $this->Works->Query($SQL);    
                    $ID = $this->Works->FieldDataByName($resourceID,'ID');
                    return $this->InsertaBibliografia($ID,$Form,$Libros); 
            }

            public function Actualiza($Form,$Libros)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $SQL="UPDATE POSTGRADO..COL_MAT_CONT_GENERAL SET
                           REQUISITO='$Form->requisito',
                           ORGCURRICULAR='$Form->orgcurricular',
                           COMPONENTECD='$Form->componentecd',
                           COMPONENTECP='$Form->componentecp',
                           COMPONENTECA='$Form->componenteca',
                           OBGENERAL='$Form->obgeneral',
                           NIVCONT='$Form->nivcon',
                           NIVPLAN='$Form->nivpln',    
                           OBESPECIFICO='$Form->obespecifico',
                           METODOLOGIA='$Form->metodologia',
                           LINEA='$Form->linea',
                           IDUSMODIFICA='$Modulo->Idusrol',
                           FEUSMODIFICA=getDate(),
                           IDESTADO=1
                           OUTPUT INSERTED.ID
                           WHERE
                           IDESTADO=1 AND IDMAESVER='$Form->idversion' AND IDMATERIA='$Form->idmateria'"; // 
                    $resourceID=$this->Works->Query($SQL);    
                    $ID = $this->Works->FieldDataByName($resourceID,'ID');
                    return $this->InsertaBibliografia($ID,$Form,$Libros);
            }

            private function InsertaBibliografia($ID,$Form,$Libros)
            {       if (intval($ID)>0)
                    {   $this->Works->Query("DELETE FROM POSTGRADO..COL_MAT_CONT_BIBLIOGRAFIA WHERE IDMATCONT='$ID'");
                        for ($u=1;$u<=$Libros;$u++)
                        {   $Basname = "Basname".$u;
                            if (trim($Form->{$Basname})!="")
                            {   $Basanio = "Basanio".$u;
                                $Basnume = "Basnume".$u;     
                                $this->Works->Query("INSERT INTO POSTGRADO..COL_MAT_CONT_BIBLIOGRAFIA(IDMATCONT,TIPO,LIBRO,EDICION,EJEMPLARES) VALUES('$ID','BAS','".$Form->{$Basname}."','".$Form->{$Basanio}."','".$Form->{$Basnume}."') ");    
                            }
 
                            $Comname = "Comname".$u;
                            if (trim($Form->{$Comname})!="")
                            {   $Comanio = "Comanio".$u;
                                $Comnume = "Comnume".$u; 
                                $this->Works->Query("INSERT INTO POSTGRADO..COL_MAT_CONT_BIBLIOGRAFIA(IDMATCONT,TIPO,LIBRO,EDICION,EJEMPLARES) VALUES('$ID','COM','".$Form->{$Comname}."','".$Form->{$Comanio}."','".$Form->{$Comnume}."') ");    
                            }

                            $Lnkname = "Lnkname".$u;
                            if (trim($Form->{$Lnkname})!="")
                            {   $Lnkanio = "Lnkanio".$u; 
                                $this->Works->Query("INSERT INTO POSTGRADO..COL_MAT_CONT_BIBLIOGRAFIA(IDMATCONT,TIPO,LIBRO,EDICION) VALUES('$ID','LNK','".$Form->{$Lnkname}."','".$Form->{$Lnkanio}."')");    
                            }
                        }    
                        return array(true,$ID);   
                    }    
            }
            
            public function Desactiva($idVer,$idMat)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $resourceID = $this->Works->Query("Update POSTGRADO..COL_MAT_CONT_GENERAL Set IDESTADO=0,IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate() WHERE IDMAESVER='$idVer' AND IDMATERIA='$idMat' ");  //   
                    return true;                                        
            }

            public function ObtenerNumeroComponentes($IdMsv,$IdMat)
            {      $SQL = "SELECT (CASE COMPONENTECD WHEN 0 THEN 0 ELSE 1 END)+   
                                  (CASE COMPONENTECP WHEN 0 THEN 0 ELSE 1 END)+
                                  (CASE COMPONENTECA WHEN 0 THEN 0 ELSE 1 END) AS NUMCOMPTE
                           FROM   POSTGRADO..COL_MAT_CONT_GENERAL WHERE IDESTADO=1 AND IDMAESVER='$IdMsv' AND IDMATERIA='$IdMat' ";
                   $resourceID = $this->Works->Query($SQL);
                   while($Filas = $this->Works->NextRecordAssoc($resourceID))
                   {     return  $Filas['NUMCOMPTE'];                 
                   }
                   return 1;     
            }
            
            public function ObtenerMateriaContenidoByMalla($prepareDQL)
            {       $Clo = 0; $Mvr = 0; $Mat = "";
                    if (key_exists('maesver', $prepareDQL))
                        $Mvr = $prepareDQL['maesver'];
                    if (key_exists('ciclo', $prepareDQL))
                        $Clo = $prepareDQL['ciclo'];
                    if (key_exists('texto', $prepareDQL))
                        $Mat = " AND (Z.MATERIA LIKE '%".$prepareDQL['texto']."%') ";
                
                    $SQL = "SELECT Y.ID AS IDMATCONT, Z.ID AS IDMATERIA, Z.MATERIA, X.NIVEL, X.NIVELORDEN,Y.NIVPLAN, ";
                    $SQL.= "       ISNULL(Y.COMPONENTECD,0) CD, ISNULL(Y.COMPONENTECP,0) CP, ISNULL(Y.COMPONENTECA,0) CA,
                                   (ISNULL(Y.COMPONENTECD,0)+ISNULL(Y.COMPONENTECP,0)+ISNULL(Y.COMPONENTECA,0)) AS TOTAL ";
                    $SQL.= "FROM   POSTGRADO..COL_CICLOS AS U, POSTGRADO..ADM_MAESTRIA_COHORTE AS V, POSTGRADO..COL_MALLA AS W,
                                   POSTGRADO..COL_MALLA_DETALLE AS X, POSTGRADO..COL_MAT_CONT_GENERAL AS Y,POSTGRADO..COL_MATERIA AS Z ";
                    $SQL.= "WHERE  (U.ID=$Clo AND U.IDCOHORTE=V.ID) AND (V.IDMALLA=W.ID) AND 
                                   (W.ID=X.IDMALLA) AND (X.IDMATERIA=Y.IDMATERIA AND Y.IDMAESVER=$Mvr) AND 
                                   (Y.IDMATERIA=Z.ID) AND (U.NIVEL=X.NIVEL) ".$Mat." 
                                   AND Z.IDESTADO = 1 
                            ORDER  BY X.NIVEL, X.NIVELORDEN ";
        
                    $resourceID = $this->Works->Query($SQL);    
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias con definicion de contenido para el ciclo y maestria.");
            }
            
            function ObtenerValorComponente($Compte,$Params)
            {       $MaestriaID = $Params[0];
                    $MateriaID  = $Params[1];
                    $SQL = "SELECT A.COMPONENTE".$Compte." AS COMPTE 
                            FROM POSTGRADO..COL_MAT_CONT_GENERAL AS A, POSTGRADO..ADM_MAESTRIA_VERSION AS B 
                            WHERE A.IDESTADO=1 AND A.IDMATERIA=$MateriaID AND B.IDMAESTRIA=$MaestriaID AND
                                  B.ID=A.IDMAESVER AND B.IDESTADO IN (1,11) ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definicion de contenido para la maestria y materia seleccionada.");
            }
        }      
?>        
