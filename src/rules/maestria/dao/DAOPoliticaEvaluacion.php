<?php
        require_once("src/base/DBDAO.php");
        
        class DAOPoliticaEvaluacion extends DBDAO
        {   private static $entityHorario;
            private $Works;  
            
            public function __construct()
            {      self::$entityHorario = DBDAO::getConnection();
                   $this->Works = self::$entityHorario->MyFunction();                
            }

            function ObtenerColumnsPoliticaEvaluacion($IdPol,$IdEtp,$IdMsv,$IdMat)
            {       $DataCollection = array();
                    $Counter = 0;
                    $SQL = "SELECT X.TIPO,X.VARIABLE,X.COLUMNA,X.ANCHO,W.ID as POLEVAL,W.DECIMALES,W.RGDESDE,W.RGHASTA,X.EDITABLE,X.TIPODATO,X.INFORMATIVO FROM POSTGRADO..COL_POLITICA_EVALUACION AS W, POSTGRADO..COL_POLITICA_EVALUACION_DETALLE AS X
                            WHERE W.ID=$IdPol AND X.ETAPA='$IdEtp' AND W.IDESTADO=1 AND W.ID=X.IDPOLEVAL  
                            ORDER BY X.ORDEN"; 
                    $resourceID = $this->Works->Query($SQL);
                    while ($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {      $Counter++;
                           if ($Filas['TIPO']=='COMPONENTE')
                           {   $Compt = $Filas['VARIABLE']=='CPE' ? 'CP':$Filas['VARIABLE'];
                              
                               $SQL = "SELECT (CASE ".$Filas['TIPO'].$Compt." WHEN 0 THEN 'N' ELSE 'S' END) AS EDITABLE FROM POSTGRADO..COL_MAT_CONT_GENERAL WHERE IDESTADO=1 AND IDMAESVER='$IdMsv' AND IDMATERIA='$IdMat' ";             
                               $rID = $this->Works->Query($SQL);
                               while ($Edita = $this->Works->NextRecordAssoc($rID))
                               {      $Filas['EDITABLE'] = $Edita['EDITABLE'];   
                               }         
                           }
                           $DataCollection[] = array($Filas['POLEVAL'],$Filas['TIPO'],$Filas['VARIABLE'],$Filas['COLUMNA'],$Filas['ANCHO'],$Filas['DECIMALES'],$Filas['RGDESDE'],$Filas['RGHASTA'],$Filas['EDITABLE'],$Filas['TIPODATO'],$Filas['INFORMATIVO']);                 
                    }
                    $this->Works->CloseResource($resourceID);
                    return array(($Counter>0),$DataCollection);                                
            }
         
            function ObtenerVariablesPoliticaEvaluacion($Param)
            {       $SQL = "SELECT X.TIPO,X.VARIABLE,X.PORCENTAJE,W.DECIMALES,W.MINNOTA,W.MINASIS,X.COLUMNA,X.ANCHO,X.TIPODATO
                            FROM POSTGRADO..COL_POLITICA_EVALUACION AS W, POSTGRADO..COL_POLITICA_EVALUACION_DETALLE AS X
                            WHERE W.ID=$Param[0] AND X.ETAPA='$Param[1]' AND W.IDESTADO=1 AND W.ID=X.IDPOLEVAL  
                            ORDER BY X.ORDEN"; 
            
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen politicas de evaluación.");        
            }            
            
            function ObtenerParcialesPoliticaEvaluacion($Politica)
            {       $DataCollection = array();
                    $Counter = 0;
                    $SQL = "SELECT DISTINCT X.ETAPA AS ESTRUCTURA 
                            FROM POSTGRADO..COL_POLITICA_EVALUACION AS W, POSTGRADO..COL_POLITICA_EVALUACION_DETALLE AS X 
                            WHERE W.ID=$Politica AND W.IDESTADO=1 AND W.ID=X.IDPOLEVAL"; 
                    
                    $DataCollection[] = array(0,"SELECCIONE...");
                    $resourceID = $this->Works->Query($SQL);
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Counter++;
                          $DataCollection[] = array($Counter,$Filas['ESTRUCTURA']);                 
                    }
                    $this->Works->CloseResource($resourceID);
                    return array(($Counter>0),$DataCollection);
            }
            
            function ObtenerPoliticasEvaluacion($Politica,$TipoNota)
            {       $SQL = "SELECT X.PORCENTAJE,W.DECIMALES,W.RGDESDE,W.RGHASTA
                            FROM POSTGRADO..COL_POLITICA_EVALUACION AS W, POSTGRADO..COL_POLITICA_EVALUACION_DETALLE AS X
                            WHERE W.ID=$Politica AND W.IDESTADO=1 AND X.ID=$TipoNota AND W.ID=X.IDPOLIEVAL ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen politicas de evaluación.");        
            }
            
        }
?>        
