<?php
        require_once("src/base/DBDAO.php");

        class DAOCiclosMateria extends DBDAO
        {   
            private static $entityCiclosMateria;
            private $Works;
              
            public function __construct()
            {       self::$entityCiclosMateria = DBDAO::getConnection();
                    $this->Works = self::$entityCiclosMateria->MyFunction();
            }
              
            function ObtenerMaterias($Params)
            {   $MallaID=$Params[1];
                $NivelID=$Params[5];
                $CohorteID=$Params[0];

                $SQL = "SELECT ISNULL(CM.ID,0) AS IDCICLOSMAT, M.ID AS IDMATERIA, MC.ID AS IDCOHORTE, M.MATERIA,ISNULL(CM.IDDOCENTE,0) AS IDDOCENTE,
                                ISNULL(CM.IDGRUPO,0) AS IDGRUPO, ISNULL(CP.PARALELO,'') AS PARALELO, ISNULL(CM.FEINICIO,'1/1/1999') AS FEINICIO, ISNULL(CM.FEFIN,'1/1/1999') AS FEFIN,
                                        MD.ID AS IDMALLADET, ISNULL(CM.ID,0) AS ACTIVO, ML.IDTIPOMALLA, ISNULL(CM.IDESTADO,0) AS IDESTADO
                        FROM POSTGRADO..COL_MATERIA M
                        LEFT JOIN POSTGRADO..COL_MALLA_DETALLE MD ON  M.ID = MD.IDMATERIA
                        LEFT JOIN POSTGRADO..COL_MALLA ML ON ML.ID = MD.IDMALLA AND ML.ID = $MallaID
                        LEFT JOIN POSTGRADO..ADM_MAESTRIA_COHORTE MC ON ML.ID = MC.IDMALLA AND MC.ID = $CohorteID
                        LEFT JOIN POSTGRADO..COL_COHORTE_PARALELO CP ON MC.ID = CP.IDCHORTE AND CP.IDESTADO = 1
                        LEFT JOIN POSTGRADO..COL_CICLOS_MATERIA AS CM ON  MD.ID = CM.IDMALLADT AND CM.IDGRUPO = CP.ID
                        WHERE MD.NIVEL = $NivelID
                        AND M.IDESTADO = 1
                        AND MC.IDESTADO = 1
                        AND (CM.IDESTADO = 1 OR CM.IDESTADO = 2 OR CM.IDESTADO IS NULL)
                        ORDER BY M.MATERIA";

                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias en la malla para la maestria seleccionada.");
            }
            
            function ObtenerMaestria()
            {   $SQL = "SELECT F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.ID AS IDMAESTRIA, M.MAESTRIA, MV.ID AS IDVERSION, MV.VERSION
                        FROM POSTGRADO..ADM_MAESTRIA M, POSTGRADO..ADM_FACULTAD F, POSTGRADO..ADM_MAESTRIA_VERSION MV
                        WHERE 
                                M.IDFACULTAD = F.ID
                                AND M.ID = MV.IDMAESTRIA	AND M.IDESTADO = 1
                        ORDER BY F.ID ASC";
                           
                $resourceID = $this->Works->Query($SQL);               
                return $this->Works->FillDataRecordAssoc($resourceID,"No existen Maestria definidas.");
            }
            
            function ObtenerCohorteByMaestria($Params)
            {   $maestria = $Params['mst'];
                $version = $Params['vmt'];
                $SQL = "SELECT  MC.ID AS IDCOHORTE, MC.COHORTE, MC.IDPOLEVAL AS IDPOLITICA, MC.IDMALLA
                        FROM	POSTGRADO..ADM_MAESTRIA_COHORTE MC, POSTGRADO..ADM_MAESTRIA_VERSION MV, 
                                POSTGRADO..ADM_MAESTRIA M
                        WHERE 
                                    MC.IDMAESVER = MV.ID	AND MV.IDMAESTRIA = M.ID
                                AND MC.IDESTADO = 1		AND M.ID = $maestria 
                                AND MV.ID =  $version ";
                           
                $resourceID = $this->Works->Query($SQL);               
                return $this->Works->FillDataRecordAssoc($resourceID,"No existen Cohortes definidos para la Maestria Seleccionada.");
            }
            
            function ObtenerCicloByCohorte($Params)
            {   $Maestria = $Params['mst'];
                $Version = $Params['vmt'];
                $Cohorte = $Params['cht'];
                $SQL = "SELECT C.ID, C.CICLO, C.NIVEL, C.FEINICIO, C.FEFIN
                        FROM    POSTGRADO..COL_CICLOS C, POSTGRADO..ADM_MAESTRIA_COHORTE MC, 
                                POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA M 
                        WHERE 
                                M.ID = MV.IDMAESTRIA	AND MV.ID = MC.IDMAESVER	AND MC.ID = C.IDCOHORTE
                            AND MC.ID = $Cohorte	AND M.ID = $Maestria		AND MV.ID = $Version
                            AND C.IDESTADO = 1";
                           
                $resourceID = $this->Works->Query($SQL);               
                return $this->Works->FillDataRecordAssoc($resourceID,"No existen Ciclos definido para el Cohorte seleccionado.");
            }
            
            function ObtenerDocenteByFacultad($Params)
            {       $FacultadID=$Params;
                    $SQL = "SELECT '0'ID, 'SELECCIONE'DOCENTE
                            UNION
                            SELECT D.ID, D.APELLIDOS + ' ' + D.NOMBRES AS DOCENTE
                            FROM POSTGRADO..COL_DOCENTE D,
                                    POSTGRADO..COL_DOCENTE_FACULTAD DF,
                                    POSTGRADO..ADM_FACULTAD F
                            WHERE D.ID = DF.IDDOCENTE
                            AND DF.IDFACULTAD = F.ID
                            AND F.ID = $FacultadID
                            AND D.IDESTADO = 1"; 
                    $resourceID = $this->Works->Query($SQL);
                    $Registro = array();
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }
            
            function ObtenerParaleloByCohorte($Params)
            {       $CohorteID=$Params;
                    $SQL = "SELECT '0'ID, 'SELECCIONE'PARALELO
                            UNION       
                            SELECT CP.ID, CP.PARALELO 
                            FROM POSTGRADO..COL_COHORTE_PARALELO CP,
                                 POSTGRADO..ADM_MAESTRIA_COHORTE MC
                            WHERE CP.IDCHORTE = MC.ID
                            AND MC.ID = $CohorteID
                            AND CP.IDESTADO = 1"; 
                    $resourceID = $this->Works->Query($SQL);
                    $Registro = array();
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }
         
            function ObtenerMaestriaByTx($Params)
            {   $texto = $Params['txt'];
                $SQL = "SELECT F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.ID AS IDMAESTRIA, M.MAESTRIA, MV.ID AS IDVERSION, MV.VERSION
                        FROM POSTGRADO..ADM_MAESTRIA M, POSTGRADO..ADM_FACULTAD F, POSTGRADO..ADM_MAESTRIA_VERSION MV
                        WHERE 
                                M.IDFACULTAD = F.ID     AND M.ID = MV.IDMAESTRIA	
                                AND M.IDESTADO = 1      AND M.MAESTRIA LIKE '%$texto%'
                        ORDER BY F.ID ASC";
                           
                $resourceID = $this->Works->Query($SQL);               
                return $this->Works->FillDataRecordAssoc($resourceID,"No existen Maestria definidas.");
            }
            
            function ObtenerCohorteByMaestriaByTx($Params)
            {   $texto = $Params['txt'];
                $maestria = $Params['mst'];
                $version = $Params['vmt'];
                $SQL = "SELECT  MC.ID AS IDCOHORTE, MC.COHORTE, MC.IDPOLEVAL AS IDPOLITICA, MC.IDMALLA
                        FROM	POSTGRADO..ADM_MAESTRIA_COHORTE MC, POSTGRADO..ADM_MAESTRIA_VERSION MV, 
                                POSTGRADO..ADM_MAESTRIA M
                        WHERE 
                                    MC.IDMAESVER = MV.ID	AND MV.IDMAESTRIA = M.ID
                                AND MC.IDESTADO = 1		AND M.ID = $maestria 
                                AND MV.ID =  $version           AND MC.COHORTE LIKE '%$texto%'";
                           
                $resourceID = $this->Works->Query($SQL);               
                return $this->Works->FillDataRecordAssoc($resourceID,"No existen Cohortes definidos para la Maestria Seleccionada.");
            }
            
            function ObtenerCicloByCohorteByTx($Params)
            {   $texto = $Params['txt'];
                $Maestria = $Params['mst'];
                $Version = $Params['vmt'];
                $Cohorte = $Params['cht'];
                $SQL = "SELECT C.ID, C.CICLO, C.NIVEL, C.FEINICIO, C.FEFIN
                        FROM    POSTGRADO..COL_CICLOS C, POSTGRADO..ADM_MAESTRIA_COHORTE MC, 
                                POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA M 
                        WHERE 
                                M.ID = MV.IDMAESTRIA	AND MV.ID = MC.IDMAESVER	AND MC.ID = C.IDCOHORTE
                            AND MC.ID = $Cohorte	AND M.ID = $Maestria		AND MV.ID = $Version
                            AND C.IDESTADO = 1          AND C.CICLO LIKE '%$texto%'";
                           
                $resourceID = $this->Works->Query($SQL);               
                return $this->Works->FillDataRecordAssoc($resourceID,"No existen Ciclos definido para el Cohorte seleccionado.");
            }

            function GuardaHistorico($Datos,$Texto)
            {   $Modulo = json_decode(Session::getValue('Sesion'));
                $IdCicloMat = $Datos[0];
                $resourceID = $this->Works->Query("UPDATE POSTGRADO..COL_CICLOS_MATERIA SET OBSERVACION = '$Texto', IDESTADO=11, IDUSMODIFICA='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() WHERE ID = '$IdCicloMat' ");    
                return 1;   
            }
            
            function GuardaNuevoRegistro($Params,$Datos)
            {   $Modulo = json_decode(Session::getValue('Sesion'));
                $IdCiclo = $Params[4];
                $IdMallaDetalle = $Datos[4];
                $DcntID = $Datos[5];
                $GrpoID = $Datos[6];
                $FeInicio = $Datos[7];
                $FeFin = $Datos[8];
                $SQL="INSERT INTO POSTGRADO..COL_CICLOS_MATERIA
                      (IDCICLO, IDMALLADT, IDDOCENTE, IDGRUPO,FEINICIO,FEFIN,IDUSCREA,FEUSCREA,IDESTADO)
                      OUTPUT INSERTED.ID VALUES ('$IdCiclo','$IdMallaDetalle','$DcntID','$GrpoID','$FeInicio','$FeFin','$Modulo->Idusrol',getDate(),2)";
                $resourceID = $this->Works->Query($SQL);
                return $this->Works->FieldDataByName($resourceID,'ID');
            }
            
            function GuardarDB($Params,$Datos)
            {   $Modulo = json_decode(Session::getValue('Sesion'));
                $IdCiclo = $Params[4];
                $IdMallaDetalle = $Datos[4];
                $DcntID = $Datos[5];
                $GrpoID = $Datos[6];
                $FeInicio = $Datos[7];
                $FeFin = $Datos[8];
                $SQL="INSERT INTO POSTGRADO..COL_CICLOS_MATERIA
                      (IDCICLO, IDMALLADT, IDDOCENTE, IDGRUPO,FEINICIO,FEFIN,IDUSCREA,FEUSCREA)
                      OUTPUT INSERTED.ID VALUES ('$IdCiclo','$IdMallaDetalle','$DcntID','$GrpoID','$FeInicio','$FeFin','$Modulo->Idusrol',getDate())";
                $resourceID = $this->Works->Query($SQL);
                return $this->Works->FieldDataByName($resourceID,'ID');
            }
            
            function ObtenerExisteGrupoMateria($Datos)
            {   $MateriaID = $Datos[2];
                $GrupoID   = $Datos[6];
                $SQL = "SELECT COUNT (*) AS VALIDAR
                        FROM POSTGRADO..COL_CICLOS_MATERIA CM,
                                POSTGRADO..COL_MALLA_DETALLE MD,
                                POSTGRADO..COL_MATERIA M
                        WHERE	M.ID = MD.IDMATERIA
                        AND		MD.ID = CM.IDMALLADT    AND CM.IDESTADO = 1
                        AND		M.ID = '$MateriaID'	AND CM.IDGRUPO = '$GrupoID'";
                           
                $resourceID = $this->Works->Query($SQL);               
                return $this->Works->FieldDataByName($resourceID,'VALIDAR');
            }
            
            function ObtenerExisteCicloMateria($Datos,$IdCicloMateria)
            {   $MateriaID = $Datos[2];
                $GrupoID   = $Datos[6];
                $SQL = "SELECT COUNT (*) AS VALIDAR
                        FROM POSTGRADO..COL_CICLOS_MATERIA CM,
                                POSTGRADO..COL_MALLA_DETALLE MD,
                                POSTGRADO..COL_MATERIA M
                        WHERE	M.ID = MD.IDMATERIA
                        AND		MD.ID = CM.IDMALLADT        AND CM.IDESTADO = 2
                        AND		CM.ID = '$IdCicloMateria'   AND M.ID = '$MateriaID'
                        AND CM.IDGRUPO = '$GrupoID'";
                           
                $resourceID = $this->Works->Query($SQL);               
                return $this->Works->FieldDataByName($resourceID,'VALIDAR');
            }
            
            function ActualizarDB($Params,$Datos,$IdCicloMateria)
            {   $Modulo = json_decode(Session::getValue('Sesion'));
                $IdCiclo = $Params[4];
                $IdMallaDetalle = $Datos[4];
                $DcntID = $Datos[5];
                $GrpoID = $Datos[6];
                $FeInicio = $Datos[7];
                $FeFin = $Datos[8];
                $SQL="UPDATE POSTGRADO..COL_CICLOS_MATERIA
                      SET IDCICLO = '$IdCiclo',  IDMALLADT = '$IdMallaDetalle', IDDOCENTE = '$DcntID', IDGRUPO = '$GrpoID',
                      FEINICIO = '$FeInicio', FEFIN = '$FeFin',IDUSCREA = '$Modulo->Idusrol', FEUSCREA = getDate(),IDESTADO = 1
                      WHERE ID = '$IdCicloMateria' AND IDESTADO = 2";

                $resourceID = $this->Works->Query($SQL);
                return 1; 
            }

            public function CambiarEstado($IdCicloMateria)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("update POSTGRADO..COL_CICLOS_MATERIA set IDESTADO=1, IDUSMODIFICA='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() where id='$IdCicloMateria' ");    
                    return 1;                    
            }

            public function CambiarEstadoTemporal($IdTemportalCM)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("update POSTGRADO..COL_CICLOS_MATERIA set IDESTADO=11, IDUSMODIFICA='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() where id='$IdTemportalCM' ");    
                    return 1;                    
            }
        }    
