<?php
        require_once("src/base/DBDAO.php");

        class DAOVMaestria extends DBDAO
        {   private static $entityVMaestria;
            private $Works;                
            
            public function __construct()
            {      self::$entityVMaestria = DBDAO::getConnection();
                   $this->Works = self::$entityVMaestria->MyFunction();                            
            }
    
            public function ObtenerVMaestriaByID($id)
            {          
                    $DSQL = "SELECT TOP 50 MV.ID AS ID, MV.VERSION, M.ID AS IDMAESTRIA, M.MAESTRIA AS MAESTRIA, MV.COHORTES, MV.MATERIAS, MV.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA M
                                WHERE M.ID = MV.IDMAESTRIA
                                AND MV.ID = '$id'"; 
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function ObtenerVMaestriaByidByMaestria($Form)
            {                
                    $id = $Form["id"];
                    $idMaestria = $Form["id_maestria"];

                    $DSQL = "SELECT TOP 50 MV.ID AS ID, MV.VERSION, M.ID AS IDMAESTRIA, M.MAESTRIA AS MAESTRIA, MV.COHORTES, MV.MATERIAS, MV.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA M
                                WHERE M.ID = MV.IDMAESTRIA
                                AND MV.ID LIKE '%$id%'
                                AND MV.IDMAESTRIA = '$idMaestria'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->NextRecordObject($resourceID); 
            }

            public function ObtenerVMaestria()
            {       $Registro = array();
                    $resourceID = $this->Works->Query("SELECT MV.ID, MV.VERSION FROM POSTGRADO..ADM_MAESTRIA_VERSION MV WHERE MV.IDESTADO = 1");
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }

            public function ObtenerVMaestriaByDescripcion($PrepareDQL)
            {                                                     
                    $DSQL = "SELECT TOP 50 MV.ID AS ID, MV.VERSION, M.ID AS IDMAESTRIA, M.MAESTRIA AS MAESTRIA, MV.COHORTES, MV.MATERIAS, MV.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA M
                                WHERE M.ID = MV.IDMAESTRIA
                                AND MV.VERSION LIKE '%".$PrepareDQL['texto']."%'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen versiones definidas."); 
            }
            
            public function ObtenerVMaestriaByDescripcionByMateria($Form)
            {                
                    $Text = trim($Form["texto"]);
                    $idMaestria = $Form["id"];

                    $DSQL = "SELECT TOP 50 MV.ID AS ID, MV.VERSION, M.ID AS IDMAESTRIA, M.MAESTRIA AS MAESTRIA, MV.COHORTES, MV.MATERIAS, MV.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA M
                                WHERE M.ID = MV.IDMAESTRIA
                                AND MV.VERSION LIKE '%".$Text."%'
                                AND MV.IDMAESTRIA = $idMaestria";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen versiones definidas."); 
            }
            
            public function ObtenerMaestriaByDescripcion($PrepareDQL)
            {                                                  
                    $SQL = "SELECT M.ID AS IDMAESTRIA, M.MAESTRIA, F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD ";
                    $SQL.= "FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F ";
                    $SQL.= "WHERE M.IDFACULTAD = F.ID ";
                    $SQL.= "AND M.DESCRIPCION LIKE '%".$PrepareDQL['texto']."%' ";
                    $SQL.= "ORDER BY M.ID ASC ";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen versiones definidas."); 
            }

            public function ObtenerVMaestriaEstadoByID($id)
            {          
                    $DSQL = "SELECT TOP 50 MV.ID AS ID, MV.VERSION, M.ID AS IDMAESTRIA, M.MAESTRIA AS MAESTRIA, MV.COHORTES, MV.MATERIAS, MV.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA M
                                WHERE M.ID = MV.IDMAESTRIA
                                AND MV.ID = '$id'"; 
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen versiones definidas."); 
            }
            
            public function ObtenerVMaestriaEstadoByMaestria($idMaestria)
            {          
                    $DSQL = "SELECT TOP 50 MV.ID AS ID, MV.VERSION, M.ID AS IDMAESTRIA, M.MAESTRIA AS MAESTRIA, MV.COHORTES, MV.MATERIAS, MV.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_VERSION MV, POSTGRADO..ADM_MAESTRIA M
                                WHERE M.ID = MV.IDMAESTRIA
                                AND M.ID = '$idMaestria'";
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen versiones definidas por maestria."); 
            }
            
            public function InsertaVMaestriaSinVersion($Form)
            {           $Modulo = json_decode(Session::getValue('Sesion'));
                        $QUERY = "INSERT INTO POSTGRADO..ADM_MAESTRIA_VERSION(IDMAESTRIA,VERSION,COHORTES,MATERIAS,IDUSCREA,FEUSCREA) OUTPUT INSERTED.ID values('$Form->idmaestria','V.1','$Form->cohortes','$Form->materias','".$Modulo->Idusrol."',getDate())";
                        $resourceID = $this->Works->Query($QUERY);
                        return $this->Works->FieldDataByName($resourceID,'ID');
            }
            
            public function InsertaVMaestriaConVersion($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("INSERT INTO POSTGRADO..ADM_MAESTRIA_VERSION(IDMAESTRIA,VERSION,COHORTES,MATERIAS,IDUSCREA,FEUSCREA) OUTPUT INSERTED.ID values('$Form->idmaestria',(SELECT TOP 1 SUBSTRING(VERSION,1,2) + CAST(CAST(SUBSTRING(VERSION,3,3) AS INT)+1 AS varchar) AS V FROM POSTGRADO..ADM_MAESTRIA_VERSION MV WHERE MV.IDMAESTRIA = '$Form->idmaestria' ORDER BY MV.ID DESC),'$Form->cohortes','$Form->materias','".$Modulo->Idusrol."',getDate())");
                    return $this->Works->FieldDataByName($resourceID,'ID');
            }
    
            public function ActualizaVMaestria($vmaestria)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("Update POSTGRADO..ADM_MAESTRIA_VERSION set 
                                                       COHORTES='$vmaestria->cohortes',
                                                       MATERIAS='$vmaestria->materias',
                                                       IDMAESTRIA='$vmaestria->idmaestria',
                                                       IDUSMODIFICA='".$Modulo->Idusrol."',
                                                       IDESTADO='$vmaestria->estado',    
                                                       FEUSMODIFICA=getDate() 
                                                       WHERE ID='$vmaestria->id'"); 
                    return intval($vmaestria->id);
            }
            
            public function DesactivaVMaestria($id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("update POSTGRADO..ADM_MAESTRIA_VERSION set IDESTADO=0, IDUSMODIFICA ='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() where id='$id'");
                    return 0;                    
            }

            public function ValidarVMaestriaByIdMaestria($Form)
            {
                    $DSQL = "SELECT * FROM POSTGRADO..ADM_MAESTRIA_VERSION MV
                                WHERE MV.IDMAESTRIA  = '$Form->idmaestria'";

                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID);   
            }
            
            public function  DesactivaVMaestriaVersionAnterior($idmaestria)
            {
                    $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("UPDATE POSTGRADO..ADM_MAESTRIA_VERSION set IDESTADO=0, IDUSMODIFICA ='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() WHERE IDESTADO='1' AND IDMAESTRIA='$idmaestria'");
                    return 0;    
            }
        }
