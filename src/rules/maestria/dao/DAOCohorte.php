<?php
        require_once("src/base/DBDAO.php");
        
        class DAOCohorte extends DBDAO
        {   private static $entityCohorte;
            private $Works;  
            
            public function __construct()
            {      self::$entityCohorte = DBDAO::getConnection();
                   $this->Works = self::$entityCohorte->MyFunction();                
            }

            public function ObtenerCohorteCicloByMaestriaVersion($idMaver)
            {       $SQL = "SELECT X.ID AS IDCOHORTE,ISNULL(Y.ID,0) AS IDMALLA,ISNULL(Y.DESCRIPCION,'MALLA INACTIVA') AS MALLA,X.COHORTE,X.FEINICIO,X.FEFIN 
                            FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS X LEFT JOIN POSTGRADO..COL_MALLA AS Y ON X.IDMALLA=Y.ID AND Y.IDESTADO=1 
                            WHERE X.IDMAESVER='$idMaver' AND X.IDESTADO=1 ";        
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definido cohorte para la maestria.");            
            }
            
            public function ObtenerCohorteByID($id)
            {          
                    $DSQL = "SELECT TOP 50 C.ID, C.IDMAESVER AS IDVERSION, V.IDMAESTRIA, ME.MAESTRIA AS MAESTRIA, V.VERSION, C.IDMALLA, M.DESCRIPCION AS MALLA, C.IDMODALIDA, C.COHORTE, C.FEINICIO AS INICIO, C.FEFIN AS FIN, C.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS C, POSTGRADO..ADM_MAESTRIA_VERSION AS V, POSTGRADO..COL_MALLA AS M, POSTGRADO..ADM_MAESTRIA AS ME
                                WHERE C.IDMAESVER = V.ID
                                AND V.IDMAESTRIA = ME.ID
                                AND C.IDMALLA = M.ID
                                AND C.ID = '$id'"; 
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }
            
            public function ObtenerCohorteByidByVersion($Form)
            {                
                    $id = $Form["id"];
                    $idVersion = $Form["idVersion"];

                    $DSQL = "SELECT TOP 50 C.ID, C.IDMAESVER AS IDVERSION, V.IDMAESTRIA, ME.MAESTRIA AS MAESTRIA, V.VERSION, C.IDMALLA, M.DESCRIPCION AS MALLA, C.IDMODALIDA, C.COHORTE, C.FEINICIO AS INICIO, C.FEFIN AS FIN, C.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS C, POSTGRADO..ADM_MAESTRIA_VERSION AS V, POSTGRADO..COL_MALLA AS M, POSTGRADO..ADM_MAESTRIA AS ME
                                WHERE C.IDMAESVER = V.ID
                                AND V.IDMAESTRIA = ME.ID
                                AND C.IDMALLA = M.ID
                                AND C.ID LIKE '%$id%'
                                AND C.IDMAESVER = '$idVersion'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->NextRecordObject($resourceID); 
            }

            public function ObtenerCohortes()
            {       $Registro = array();
                    $resourceID = $this->Works->Query("SELECT C.ID, C.COHORTE FROM POSTGRADO..ADM_MAESTRIA_COHORTE C WHERE C.IDESTADO = 1");
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                    }
                    return $Registro; 
            }
            
            public function ObtenerCohortesByIdVersion($idversion)
            {       $Registro = array();
                    $count = 0;
                    $resourceID = $this->Works->Query("SELECT C.ID, C.COHORTE FROM POSTGRADO..ADM_MAESTRIA_COHORTE C WHERE C.IDMAESVER = '$idversion'");
                    while($Filas = $this->Works->NextRecordAssoc($resourceID))
                    {     $Registro[] = $Filas;  
                            $count = $count +1;
                    }
                    return $count; 
            }
            
            public function ObtenerCohortesDeVersionByIdVersion($idversion)
            {       
                    $resourceID = $this->Works->Query("SELECT MV.COHORTES FROM POSTGRADO..ADM_MAESTRIA_VERSION MV WHERE MV.ID = '$idversion'");
                    return $this->Works->NextRecordObject($resourceID);
            }

            public function ObtenerCohorteByDescripcion($PrepareDQL)
            {                                                     
                    $DSQL = "SELECT TOP 50 C.ID, C.IDMAESVER AS IDVERSION, V.IDMAESTRIA, ME.MAESTRIA AS MAESTRIA, V.VERSION, C.IDMALLA, M.DESCRIPCION AS MALLA, C.IDMODALIDA, C.COHORTE, C.FEINICIO AS INICIO, C.FEFIN AS FIN, C.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS C, POSTGRADO..ADM_MAESTRIA_VERSION AS V, POSTGRADO..COL_MALLA AS M, POSTGRADO..ADM_MAESTRIA AS ME
                                WHERE C.IDMAESVER = V.ID
                                AND V.IDMAESTRIA = ME.ID
                                AND C.IDMALLA = M.ID
                                AND C.COHORTE LIKE '%".$PrepareDQL['texto']."%'";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen cohortes definidos."); 
            }
            
            public function ObtenerCohorteByDescripcionByVersion($Form)
            {                
                    $Text = trim($Form["texto"]);
                    $idVersion = $Form["idversion"];

                    $DSQL = "SELECT TOP 50 C.ID, C.IDMAESVER AS IDVERSION, V.IDMAESTRIA, ME.MAESTRIA AS MAESTRIA, V.VERSION, C.IDMALLA, M.DESCRIPCION AS MALLA, C.IDMODALIDA, C.COHORTE, C.FEINICIO AS INICIO, C.FEFIN AS FIN, C.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS C, POSTGRADO..ADM_MAESTRIA_VERSION AS V, POSTGRADO..COL_MALLA AS M, POSTGRADO..ADM_MAESTRIA AS ME
                                WHERE C.IDMAESVER = V.ID
                                AND V.IDMAESTRIA = ME.ID
                                AND C.IDMALLA = M.ID
                                AND C.COHORTE LIKE '%".$Text."%'
                                AND C.IDMAESVER = $idVersion";                                                 
                  
                    $resourceID = $this->Works->Query($DSQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen cohortes definidos."); 
            }
            
            public function ObtenerVersionByDescripcion($PrepareDQL)
            {                                                  
                    $SQL = "SELECT M.ID AS IDMAESTRIA, M.MAESTRIA, MV.ID AS IDVERSION, MV.VERSION, MV.COHORTES, F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD ";
                    $SQL.= "FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F, POSTGRADO..ADM_MAESTRIA_VERSION MV ";
                    $SQL.= "WHERE M.IDFACULTAD = F.ID ";
                    $SQL.= "AND MV.IDMAESTRIA = M.ID ";
                    $SQL.= "AND MV.IDESTADO = 1 ";
                    $SQL.= "AND M.MAESTRIA LIKE '%".$PrepareDQL['texto']."%' ";
                    $SQL.= "ORDER BY MV.ID ASC ";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Versiones definidas."); 
            }
            
            public function ObtenerMallaByDescripcion($PrepareDQL)
            {                       
                    $IdVersion = $PrepareDQL['idversion'];
                    
                    $SQL = "SELECT TOP 50 M.ID, M.DESCRIPCION AS MALLA, M.IDTIPOMALLA, MT.DESCRIPCION AS TIPOMALLA, M.CICLOS, M.FECHA, M.IDESTADO ";
                    $SQL.= "FROM POSTGRADO..COL_MALLA AS M, POSTGRADO..COL_MALLA_TIPO MT ";
                    $SQL.= "WHERE M.IDTIPOMALLA = MT.ID  ";
                    $SQL.= "AND M.IDESTADO = 1 ";
                    $SQL.= "AND M.IDMAESVER = '$IdVersion' ";
                    $SQL.= "AND M.DESCRIPCION LIKE '%".$PrepareDQL['texto']."%' ";
                    $SQL.= "ORDER BY M.ID ASC ";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Mallas definidas."); 
            }
            
            public function ObtenerMallaByVersionByDescripcion($Params)
            {       $Tx = $Params['txt'];
                    $IdVersion = $Params['idv'];
                    
                    $SQL = "SELECT TOP 50 M.ID, M.DESCRIPCION AS MALLA, M.IDTIPOMALLA, MT.DESCRIPCION AS TIPOMALLA, M.CICLOS, M.FECHA, M.IDESTADO ";
                    $SQL.= "FROM POSTGRADO..COL_MALLA AS M, POSTGRADO..COL_MALLA_TIPO MT ";
                    $SQL.= "WHERE M.IDTIPOMALLA = MT.ID  ";
                    $SQL.= "AND M.IDESTADO = 1 ";
                    $SQL.= "AND M.IDMAESVER = '$IdVersion' ";
                    $SQL.= "AND M.DESCRIPCION LIKE '%$Tx%' ";
                    $SQL.= "ORDER BY M.ID ASC ";
                  
                    $resourceID = $this->Works->Query($SQL);                  
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Mallas definidas."); 
            }

            public function ObtenerCohorteEstadoByID($id)
            {          
                    $DSQL = "SELECT TOP 50 C.ID, C.IDMAESVER AS IDVERSION, V.IDMAESTRIA, ME.MAESTRIA AS MAESTRIA, V.VERSION, C.IDMALLA, M.DESCRIPCION AS MALLA, C.IDMODALIDA, C.COHORTE, C.FEINICIO AS INICIO, C.FEFIN AS FIN, C.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS C, POSTGRADO..ADM_MAESTRIA_VERSION AS V, POSTGRADO..COL_MALLA AS M, POSTGRADO..ADM_MAESTRIA AS ME
                                WHERE C.IDMAESVER = V.ID
                                AND V.IDMAESTRIA = ME.ID
                                AND C.IDMALLA = M.ID
                                AND C.ID = '$id'"; 
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen materias definidas."); 
            }
            
            public function ObtenerCohorteEstadoByVersion($idVersion)
            {               
                    $DSQL = "SELECT TOP 50 C.ID, C.IDMAESVER AS IDVERSION, V.IDMAESTRIA, ME.MAESTRIA AS MAESTRIA, V.VERSION, C.IDMALLA, M.DESCRIPCION AS MALLA, C.IDMODALIDA, C.COHORTE, C.FEINICIO AS INICIO, C.FEFIN AS FIN, C.IDESTADO
                                FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS C, POSTGRADO..ADM_MAESTRIA_VERSION AS V, POSTGRADO..COL_MALLA AS M, POSTGRADO..ADM_MAESTRIA AS ME
                                WHERE C.IDMAESVER = V.ID
                                AND V.IDMAESTRIA = ME.ID
                                AND C.IDMALLA = M.ID
                                AND C.IDMAESVER = '$idVersion'";
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen cohortes definidos para esta versión de maestria."); 
            }
            
            public function InsertaCohorte($Form)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = strtoupper($Form->descripcion);
                    $idVersion = $Form->idversion;
                    $idMalla = $Form->idmalla;
                    $idModalidad = $Form->idmodalidad;
                    $FeInicio = $Form ->feinicio;
                    $FeFin = $Form->fefin;
                    
                    $resourceID = $this->Works->Query("insert into POSTGRADO..ADM_MAESTRIA_COHORTE(IDMAESVER,IDMALLA,IDMODALIDA,COHORTE,FEINICIO,FEFIN,IDUSCREA,FEUSCREA) OUTPUT INSERTED.ID values('$idVersion','$idMalla','$idModalidad','$Descrp','$FeInicio','$FeFin','".$Modulo->Idusrol."',getDate())");
                    return $this->Works->FieldDataByName($resourceID,'ID');
            }
    
            public function ActualizaCohorte($cohorte)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $Descrp = utf8_decode(strtoupper($cohorte->descripcion));
                    $idVersion = $cohorte->idversion;
                    $idMalla = $cohorte->idmalla;
                    $idModalidad = $cohorte->idmodalidad;
                    $FeInicio = $cohorte ->feinicio;
                    $FeFin = $cohorte->fefin;
                    $Estado = $cohorte->estado;
                    
                    $resourceID = $this->Works->Query("Update POSTGRADO..ADM_MAESTRIA_COHORTE set 
                                                       IDMAESVER='$idVersion',
                                                       IDMALLA='$idMalla',
                                                       IDMODALIDA='$idModalidad',
                                                       COHORTE='$Descrp',
                                                       FEINICIO='$FeInicio',
                                                       FEFIN='$FeFin',
                                                       IDUSMODIFICA='".$Modulo->Idusrol."',
                                                       IDESTADO='$Estado',    
                                                       FEUSMODIFICA=getDate() 
                                                       WHERE ID='$cohorte->id'"); 
                    return intval($cohorte->id);
            }
            
            public function DesactivaCohorte($id)
            {       $Modulo = json_decode(Session::getValue('Sesion'));
                    $resourceID = $this->Works->Query("update POSTGRADO..ADM_MAESTRIA_COHORTE  set IDESTADO=0, IDUSMODIFICA='".$Modulo->Idusrol."', FEUSMODIFICA=getDate() where id='$id' ");    
                    return 0;                    
            }

            public function ObtenerFechaFinUltimoCohorte($idVersion)
            {
                $DSQL = "SELECT TOP 1 YEAR(MC.FEINICIO) AS FEINICIO
                            FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS MC, POSTGRADO..ADM_MAESTRIA_VERSION AS MV
                            WHERE MC.IDMAESVER = MV.ID
                            AND MC.IDMAESVER = '$idVersion'
                            ORDER BY MC.ID DESC"; 
            
                    $resourceID = $this->Works->Query($DSQL);
                    return $this->Works->NextRecordObject($resourceID); 
            }
        }
?>        
