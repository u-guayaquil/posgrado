<?php
    
    require_once("src/base/DBDAO.php");

    class DAOAperturarActa extends DBDAO
    {       private static $entityAperturarActa;
            private $Works;
              
            public function __construct()
            {       self::$entityAperturarActa = DBDAO::getConnection();
                    $this->Works = self::$entityAperturarActa->MyFunction();
            }
              
            function ObtenerNotasMod($Params,$Compte)
            {       $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $ParaleloID=$Params[4];
                    
                    $SQL = "SELECT ISNULL(Y.ID,0) AS IDNOTAS, X.ID AS IDESTUDMATE, V.CEDULA AS CEDULA, V.APELLIDOS+' '+V.NOMBRES AS ESTUDIANTE, ISNULL(Y.NOTA,'') AS NOTAS, ISNULL(SUM(H.EFECTIVA),0)+ISNULL(SUM(H.JUSTIFICADA),0) AS ASIST, $Compte AS TCD
                            FROM   POSTGRADO..COL_ESTUDIANTE AS V
                                   INNER JOIN (POSTGRADO..COL_ESTUDIANTE_COHORTE_MATRICULA AS W 
                                   INNER JOIN (POSTGRADO..COL_ESTUDIANTE_MATERIA AS X 
                                   LEFT JOIN (POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA AS H
                                   LEFT JOIN   POSTGRADO..COL_ESTUDIANTE_MATERIA_NOTAS AS Y 
                                               ON H.IDESTUDIANTEMATERIA=Y.IDESTUDIANTEMATERIA)
                                               ON X.ID=H.IDESTUDIANTEMATERIA)      
                                               ON W.IDESTADO<>0 AND W.ID=X.IDMATRICULA) 
                                               ON V.ID=X.IDESTUDIANTE
                                   WHERE X.IDESTADO=1 AND X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDPARALELO=$ParaleloID
                                   GROUP by ISNULL(Y.ID,0), X.ID, V.CEDULA, V.APELLIDOS+' '+V.NOMBRES, ISNULL(Y.NOTA,'')
                                   ORDER BY V.APELLIDOS+' '+V.NOMBRES ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definida la materia registro de notas.");
            }   
            
            function ObtCierreActaCalificacion($Params)
            {       $SQL = "SELECT COUNT(ID) AS EXISTE FROM POSTGRADO..COL_CICLOS_MATERIA_CIERRE WHERE IDCICLO='$Params[1]' AND IDMAESTRIA='$Params[2]' AND IDMATERIA='$Params[3]' AND IDGRUPO='$Params[4]' AND ETAPA='$Params[6]' AND IDESTADO=1 ";
                $resourceID = $this->Works->Query($SQL);
                return $this->Works->FieldDataByName($resourceID,'EXISTE');
            }
            
            function ActualizarActa($Params)
            {   //$Params = [DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP,MAESV];
                $IDDocente = $Params[0];
                $IDCiclo = $Params[1];
                $IDMaestria = $Params[2];
                $IDMateria = $Params[3];
                $IDGrupo = $Params[4];
                $Modulo = json_decode(Session::getValue('Sesion')); 

                $SQL = "UPDATE POSTGRADO..COL_CICLOS_MATERIA_CIERRE 
                        SET IDESTADO = 11,  IDUSMODIFICA='$Modulo->Idusrol', FEUSMODIFICA=getDate()
                        WHERE IDDOCENTE = $IDDocente AND IDCICLO = $IDCiclo
                        AND IDMAESTRIA = $IDMaestria AND IDMATERIA = $IDMateria
                        AND IDGRUPO = $IDGrupo AND IDESTADO = 1";
                $resourceID = $this->Works->Query($SQL);
                if ($resourceID)
                        return "OK";
                else
                        return "Error";
            }
    }    
