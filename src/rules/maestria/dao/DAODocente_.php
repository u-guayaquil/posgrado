<?php
        require_once("src/base/DBDAO.php");
        
        class DAODocente extends DBDAO
        {   private static $entityDocente;
            private $Works;  
            
            public function __construct()
            {      self::$entityDocente = DBDAO::getConnection();
                   $this->Works = self::$entityDocente->MyFunction();                
            }
                   
            public function Inserta($Forma) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $SQL = "INSERT INTO POSTGRADO..COL_DOCENTE(CEDULA,NOMBRES,APELLIDOS,DIRECCION,EMAIL,TELEFONO,IDUSCREA,FEUSCREA,IDESTADO) 
                            OUTPUT Inserted.ID VALUES('$Forma->idDocente',upper('$Forma->nombre'),upper('$Forma->apellido'),upper('$Forma->direccion'),lower('$Forma->email'),'$Forma->telefono','$Modulo->Idusrol',getDate(),'$Forma->estado')"; 
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
            }
            
            public function InsertaFacultad($DocID,$FacID) //ok
            {       $resourceID = $this->Works->Query("SELECT COUNT(IDDOCENTE) AS DC FROM POSTGRADO..COL_DOCENTE_FACULTAD WHERE IDDOCENTE=$DocID AND IDFACULTAD='$FacID'");
                    if ($this->Works->FieldDataByName($resourceID,'DC')==0)
                        $SQL = "INSERT INTO POSTGRADO..COL_DOCENTE_FACULTAD (IDDOCENTE,IDFACULTAD) VALUES($DocID,'$FacID')"; 
                    else
                    {   if ($this->TieneDocenteFacultad($DocID,$FacID)==0)    
                            $SQL = "DELETE FROM POSTGRADO..COL_DOCENTE_FACULTAD WHERE IDDOCENTE=$DocID AND IDFACULTAD='$FacID'";
                        else
                            return array(false,"No puede quitar esta facultad. Tiene información relacionda.");
                    }    
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,"Los datos se guardaron correctamente.");
            }
            
            public function Actualiza($Forma) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $SQL = "UPDATE POSTGRADO..COL_DOCENTE
                            SET NOMBRES='$Forma->nombre',APELLIDOS='$Forma->apellido',DIRECCION='$Forma->direccion',EMAIL='$Forma->email',TELEFONO='$Forma->telefono',IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate(),IDESTADO='$Forma->estado' 
                            WHERE CEDULA='$Forma->idDocente'"; 
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$Forma->idDocente);
            }

            public function Inactiva($Cedula) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    if ($this->NumeroFacultadByDocente($Cedula)==0)
                    {   $SQL = "UPDATE POSTGRADO..COL_DOCENTE SET IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate(),IDESTADO=0 WHERE CEDULA='$Cedula'";
                        $resourceID = $this->Works->Query($SQL);
                        return array(true,0);
                    }
                    return array(false,"El Docente tiene asociado facultad(s). No se permite la desactivación.");
            }
            
            public function NumeroFacultadByDocente($Cedula) //Ok
            {      $SQL = "SELECT COUNT(X.IDFACULTAD) as NUM FROM POSTGRADO..COL_DOCENTE_FACULTAD AS X, POSTGRADO..COL_DOCENTE AS Y WHERE Y.CEDULA='$Cedula' AND X.IDDOCENTE=Y.ID"; 
                   
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FieldDataByName($resourceID,'NUM');
            }

            private function TieneDocenteFacultad($Docente,$Facultad)
            {      $SQL = "SELECT COUNT(X.IDCICLO) AS NUM FROM POSTGRADO..COL_PLAN_ANALITICO AS X, POSTGRADO..ADM_MAESTRIA AS Y WHERE X.IDDOCENTE=$Docente AND X.IDMAESTRIA=Y.ID AND Y.IDFACULTAD='$Facultad'"; 
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FieldDataByName($resourceID,'NUM'); 
            }
            
            public function ObtenerDocente($prepareDQL) //Ok
            {       $Txd = ""; 
                    $Fac = "";
                    $Doc = "";
                    
                    if (array_key_exists('docente', $prepareDQL))
                    {   $DocenteID = $prepareDQL['docente'];
                        $Doc = " AND X.CEDULA='$DocenteID' ";
                    }
                    if (array_key_exists('facultad', $prepareDQL))
                    {   $Fid = implode(',',$prepareDQL['facultad']);
                        $Fac = " AND Y.IDFACULTAD IN ($Fid) ";
                    } 
                    if (array_key_exists('texto', $prepareDQL))
                    {   $Txd = " AND (X.APELLIDOS+' '+X.NOMBRES LIKE '%".$prepareDQL['texto']."%') ";
                    }                    
                    $SQL = "SELECT TOP 20 X.ID,X.CEDULA,X.NOMBRES,X.APELLIDOS,X.EMAIL
                            FROM   POSTGRADO..COL_DOCENTE AS X,
                                   POSTGRADO..COL_DOCENTE_FACULTAD AS Y
                            WHERE  X.IDESTADO=1 AND X.ID=Y.IDDOCENTE ".$Fac.$Doc.$Txd; 

                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos Docentes para la maestria.");
            }
            
            public function ObtenerByCedula($Cedula) //OK
            {       $SQL = "SELECT W.CEDULA, V.NOMBRES, V.APELLIDOS, V.DIRECCION, V.TELEF_DMCL, V.EMAIL, ISNULL(X.NOMBRES,'') AS NMB, ISNULL(X.APELLIDOS,'') AS APLL, ISNULL(X.EMAIL,'') AS EML, ISNULL(X.IDESTADO,1) AS ESTADO,ISNULL(X.DIRECCION,1) AS DIR,ISNULL(X.TELEFONO,1) AS FONO,ISNULL(X.ID,0) AS IDD ";
                    $SQL.= "FROM "; 
                    $SQL.= "[10.87.117.112].DB_RRHH.dbo.DATOS_PERSONAL AS V INNER JOIN "; 
                    $SQL.= "([10.87.117.112].DB_RRHH.dbo.ADMISION AS W LEFT JOIN POSTGRADO..COL_DOCENTE AS X ON X.CEDULA=W.CEDULA COLLATE Modern_Spanish_CI_AS) ON V.CEDULA=W.CEDULA ";
                    $SQL.= "WHERE W.CEDULA='$Cedula' AND UPPER(W.DESCR_CAT_CARGO)='DOCENTE' AND W.ESTADOREG=0 "; 
                 
                    $resourceID = $this->Works->Query($SQL);
                    $Rspta = $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos docentes con la identificación ingresada.");
                    if ($Rspta[0]==false)
                    {   $Rspta = $this->ObtenerPorConfirmarByCedula($Cedula);
                    }
                    return $Rspta; 
            }
            
            public function ObtenerPorConfirmarByCedula($Cedula)
            {      $SQL = "SELECT ID AS IDD,CEDULA,NOMBRES AS NMB,APELLIDOS AS APLL,DIRECCION AS DIR,EMAIL AS EML,TELEFONO AS FONO,IDESTADO AS ESTADO FROM POSTGRADO..COL_DOCENTE WHERE CEDULA='$Cedula' and IDESTADO=2 ";
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos docentes con la identificación ingresada."); 
            }
            
            public function ObtenerDocentePlanificacion($prepareDQL) //OK 
            {       $SQL1=""; $SQL2=""; $SQL3="";

                    if (array_key_exists("docente", $prepareDQL))
                    {   $DocenteID = $prepareDQL['docente'];
                        $SQL3 = " AND Y.CEDULA='$DocenteID' ";
                    }
                    if (array_key_exists("facultad", $prepareDQL))
                    {   if (count($prepareDQL['facultad'])>0)
                        {   $FacultadID = implode(',',$prepareDQL['facultad']);
                            $SQL1 = " AND J.IDFACULTAD IN ($FacultadID) ";
                        }
                    }
                    if (array_key_exists("texto", $prepareDQL))
                    {   $DocenteTX = $prepareDQL['texto'];
                        $SQL2 = " AND (Y.APELLIDOS+' '+Y.NOMBRES LIKE '%".$DocenteTX."%')";
                    }
                    $SQL = "SELECT DISTINCT Y.ID,Y.CEDULA,Y.NOMBRES,Y.APELLIDOS 
                            FROM
                                    POSTGRADO..COL_PLAN_ANALITICO AS X,
                                    POSTGRADO..COL_DOCENTE AS Y,
                                    POSTGRADO..COL_CICLOS AS Z,
				    POSTGRADO..ADM_MAESTRIA AS J
                            WHERE   X.IDDOCENTE=Y.ID AND X.IDESTADO=1 AND X.IDCICLO=Z.ID AND Z.IDESTADO=1 AND J.ID=X.IDMAESTRIA".$SQL1.$SQL2.$SQL3;        
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Docente definidos para el ciclo vigente.");
            }

            public function ObtenerDocenteByTX($prepareDQL) //OK 
            {       
                    $SQL2="";
                    if (array_key_exists("texto", $prepareDQL))
                    {   $DocenteTX = $prepareDQL['texto'];
                        $SQL2 = " AND (V.APELLIDOS+' '+V.NOMBRES LIKE '%".$DocenteTX."%')";
                    }

                    $SQL = "SELECT TOP 20 W.CEDULA, V.NOMBRES, V.APELLIDOS, V.DIRECCION, V.TELEF_DMCL, V.EMAIL, ISNULL(X.NOMBRES,'') AS NMB, ISNULL(X.APELLIDOS,'') AS APLL, ISNULL(X.EMAIL,'') AS EML, ISNULL(X.IDESTADO,1) AS ESTADO,ISNULL(X.DIRECCION,'') AS DIR,ISNULL(X.TELEFONO,'') AS FONO, ISNULL(X.ID,0) AS IDD ";
                    $SQL.= "FROM "; 
                    $SQL.= "[10.87.117.112].DB_RRHH.dbo.DATOS_PERSONAL AS V INNER JOIN "; 
                    $SQL.= "([10.87.117.112].DB_RRHH.dbo.ADMISION AS W LEFT JOIN POSTGRADO..COL_DOCENTE AS X ON X.CEDULA=W.CEDULA COLLATE Modern_Spanish_CI_AS) ON V.CEDULA=W.CEDULA ";
                    $SQL.= "WHERE UPPER(W.DESCR_CAT_CARGO)='DOCENTE' AND W.ESTADOREG=0 ".$SQL2; 

                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Docente definidos.");
            }            

            /*
            public function ObtenerDocenteMaestriaByDescripcion($PrepareDQL) //Pendiente
            {       $Id = $PrepareDQL['maestria'];
                    $Tx = $PrepareDQL['texto'];
                    $SQL = "SELECT DISTINCT Top 50 Z.CEDULA,Z.NOMBRES,Z.APELLIDOS,Z.EMAIL
                            FROM   BdAcademico..TB_DOCENTE_DACADEMICO AS X,
                                   POSTGRADO..ADM_MAESTRIA AS Y, 
                                   DB_RRHH..DATOS_PERSONAL AS Z,
                                   DB_RRHH..ADMISION AS W  
                            WHERE  X.ESTADO='A' AND Y.ID='$Id' AND Y.IDFACULTAD=SUBSTRING(X.COD_CARRERA,1,2)  COLLATE Modern_Spanish_CI_AS AND
                                   X.COD_DOCENTE=Z.CEDULA AND W.CEDULA=X.COD_DOCENTE AND UPPER(W.DESCR_CAT_CARGO)='DOCENTE' AND W.ESTADOREG=0
                                   AND RTRIM(Z.APELLIDOS)+' '+RTRIM(Z.NOMBRES) LIKE '%".$Tx."%'";   
           
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen docentes.");
            }*/

            /*
            public function ObtenerDocenteByMaestria($ID) //Pendiente
            {       $SQL = "SELECT DISTINCT Top 50 Z.CEDULA,Z.NOMBRES,Z.APELLIDOS,Z.EMAIL
                            FROM   BdAcademico..TB_DOCENTE_DACADEMICO AS X,
                                   POSTGRADO..ADM_MAESTRIA AS Y, 
                                   DB_RRHH..DATOS_PERSONAL AS Z,
                                   DB_RRHH..ADMISION AS W  
                            WHERE  X.ESTADO='A' AND Y.ID='$ID' AND Y.IDFACULTAD=SUBSTRING(X.COD_CARRERA,1,2)  COLLATE Modern_Spanish_CI_AS AND
                                   X.COD_DOCENTE=Z.CEDULA AND W.CEDULA=X.COD_DOCENTE AND UPPER(W.DESCR_CAT_CARGO)='DOCENTE' AND W.ESTADOREG=0";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos Docentes para la maestria seleccionada.");
            }*/
            
        }
?>        
