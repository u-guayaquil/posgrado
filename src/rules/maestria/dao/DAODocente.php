<?php
        require_once("src/base/DBDAO.php");
        
        class DAODocente extends DBDAO
        {   private static $entityDocente;
            private $Works;  
            
            public function __construct()
            {      self::$entityDocente = DBDAO::getConnection();
                   $this->Works = self::$entityDocente->MyFunction();                
            }
                   
            public function CreaRolGestion($Usuario,$Rol)
            {      $Utils = new Utils(); 
                   $Modulo = json_decode(Session::getValue('Sesion')); 
                   
                   $RIP = $Utils->retornaRemoteIPAddress();
                   $SQL = "Insert Into BdCert..SEG_USUARIO_ROL(IDMODROL,IDUSUARIO,IDUSCREA,FEUSCREA,IPCREACION) ";
                   $SQL.= "OUTPUT Inserted.ID Select X.ID,'$Usuario','$Modulo->Idusrol',getDate(),'$RIP' From BdCert..SEG_MODULO_ROL AS X Where X.IDROL='$Rol' AND X.IDMODULO='$Modulo->Idmodulo' ";
                   
                   $resourceID = $this->Works->Query($SQL);
                   if ($resourceID)
                       return array(true,$this->Works->FieldDataByName($resourceID,'ID'));    
                   else
                   return array(false,"No se puedo crear el rol para gestión administrativa.");
            }
            
            public function Inserta($Forma,$MsGston) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    
                    $SQL = "INSERT INTO POSTGRADO..COL_DOCENTE(CEDULA,NOMBRES,APELLIDOS,DIRECCION,EMAIL,TELEFONO,IDUSCREA,FEUSCREA,IDESTADO,IDGESTION,IDROLUSER) 
                            OUTPUT Inserted.ID VALUES('$Forma->idDocente',upper('$Forma->nombre'),upper('$Forma->apellido'),upper('$Forma->direccion'),'$Forma->email','$Forma->telefono','$Modulo->Idusrol',getDate(),'$Forma->estado','$Forma->idgestion','$Forma->idroluser')"; 
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$this->Works->FieldDataByName($resourceID,'ID'),$MsGston);
            }
            
            public function InsertaFacultad($DocID,$FacID) //ok
            {       $resourceID = $this->Works->Query("SELECT COUNT(IDDOCENTE) AS DC FROM POSTGRADO..COL_DOCENTE_FACULTAD WHERE IDDOCENTE=$DocID AND IDFACULTAD='$FacID'");
                    if ($this->Works->FieldDataByName($resourceID,'DC')==0)
                        $SQL = "INSERT INTO POSTGRADO..COL_DOCENTE_FACULTAD (IDDOCENTE,IDFACULTAD) VALUES($DocID,'$FacID')"; 
                    else
                    {   if ($this->TieneDocenteFacultad($DocID,$FacID)==0)    
                            $SQL = "DELETE FROM POSTGRADO..COL_DOCENTE_FACULTAD WHERE IDDOCENTE=$DocID AND IDFACULTAD='$FacID'";
                        else
                            return array(false,"No puede quitar esta facultad. Tiene información relacionda.");
                    }    
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,"Los datos se guardaron correctamente.");
            }
            
            public function Actualiza($Forma,$MsGston) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $SQL = "UPDATE POSTGRADO..COL_DOCENTE
                            SET IDROLUSER='$Forma->idroluser',IDGESTION='$Forma->idgestion',NOMBRES='$Forma->nombre',APELLIDOS='$Forma->apellido',DIRECCION='$Forma->direccion',EMAIL='$Forma->email',TELEFONO='$Forma->telefono',IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate(),IDESTADO='$Forma->estado' 
                            WHERE CEDULA='$Forma->idDocente'"; 
                    $resourceID = $this->Works->Query($SQL);
                    return array(true,$Forma->idDocente,$MsGston);
            }

            public function Inactiva($Cedula) //Ok
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    if ($this->NumeroFacultadByDocente($Cedula)==0)
                    {   $SQL = "UPDATE POSTGRADO..COL_DOCENTE SET IDGESTION=0,IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate(),IDESTADO=0 WHERE CEDULA='$Cedula'; ";
                        $resourceID = $this->Works->Query($SQL);
                        return array(true,0);
                    }
                    return array(false,"El Docente tiene asociado facultad(s). No se permite la desactivación.");
            }
            
            public function NumeroFacultadByDocente($Cedula) //Ok
            {      $SQL = "SELECT COUNT(X.IDFACULTAD) as NUM FROM POSTGRADO..COL_DOCENTE_FACULTAD AS X, POSTGRADO..COL_DOCENTE AS Y WHERE Y.CEDULA='$Cedula' AND X.IDDOCENTE=Y.ID"; 
                   
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FieldDataByName($resourceID,'NUM');
            }

            private function TieneDocenteFacultad($Docente,$Facultad)
            {      $SQL = "SELECT COUNT(X.IDCICLO) AS NUM FROM POSTGRADO..COL_PLAN_ANALITICO AS X, POSTGRADO..ADM_MAESTRIA AS Y WHERE X.IDDOCENTE=$Docente AND X.IDMAESTRIA=Y.ID AND Y.IDFACULTAD='$Facultad'"; 
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FieldDataByName($resourceID,'NUM'); 
            }
            
            public function ObtenerDocente($prepareDQL) //Ok
            {       $Txd = ""; 
                    $Fac = "";
                    $Doc = "";
                    
                    if (array_key_exists('docente', $prepareDQL))
                    {   $DocenteID = $prepareDQL['docente'];
                        $Doc = " AND X.CEDULA='$DocenteID' ";
                    }
                    if (array_key_exists('facultad', $prepareDQL))
                    {   $Fid = implode(',',$prepareDQL['facultad']);
                        $Fac = " AND Y.IDFACULTAD IN ($Fid) ";
                    } 
                    if (array_key_exists('texto', $prepareDQL))
                    {   $Txd = " AND (X.APELLIDOS+' '+X.NOMBRES LIKE '%".$prepareDQL['texto']."%') ";
                    }                    
                    $SQL = "SELECT TOP 20 X.ID,X.CEDULA,X.NOMBRES,X.APELLIDOS,X.EMAIL
                            FROM   POSTGRADO..COL_DOCENTE AS X,
                                   POSTGRADO..COL_DOCENTE_FACULTAD AS Y
                            WHERE  X.IDESTADO=1 AND X.ID=Y.IDDOCENTE ".$Fac.$Doc.$Txd; 

                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos Docentes para la maestria.");
            }
            
            public function ObtenerByCedula($Cedula) 
            {       $SQL = "SELECT ID AS IDD,CEDULA,NOMBRES AS NMB,APELLIDOS AS APLL,DIRECCION AS DIR,EMAIL AS EML,TELEFONO AS FONO,IDESTADO AS ESTADO, IDGESTION AS GESTION,IDROLUSER AS ROLUSER FROM POSTGRADO..COL_DOCENTE WHERE CEDULA='$Cedula' and IDESTADO IN (0,1,2) ";
                    
                    $resourceID = $this->Works->Query($SQL);
                    $Rspta = $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos docentes con la identificación ingresada.");
                    
                    if ($Rspta[0]==false)
                    {   $SQL = "SELECT 0 AS IDD,W.CEDULA,V.NOMBRES AS NMB,V.APELLIDOS AS APLL,V.DIRECCION AS DIR,V.TELEF_DMCL AS FONO,V.EMAIL AS EML,(CASE W.ESTADOREG WHEN 0 THEN 1 ELSE 0 END) AS ESTADO, 0 AS GESTION, 0 AS ROLUSER "; 
                        $SQL.= "FROM ".LINKSVR."DATOS_PERSONAL AS V ";
                        $SQL.= "INNER JOIN ".LINKSVR."ADMISION AS W ON V.CEDULA=W.CEDULA ";
                        $SQL.= "WHERE W.CEDULA='$Cedula' AND UPPER(W.DESCR_CAT_CARGO)='DOCENTE' AND W.ESTADOREG=0 ";
                    
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos docentes con la identificación ingresada."); 
                    }
                    return $Rspta;
            }
            
            public function ObtenerPorConfirmarByCedula($Cedula)
            {      $SQL = "SELECT ID AS IDD,CEDULA,NOMBRES AS NMB,APELLIDOS AS APLL,DIRECCION AS DIR,EMAIL AS EML,TELEFONO AS FONO,IDESTADO AS ESTADO, IDGESTION AS GESTION, IDROLUSER AS ROLUSER FROM POSTGRADO..COL_DOCENTE WHERE CEDULA='$Cedula' and IDESTADO=2 ";
                   $resourceID = $this->Works->Query($SQL);
                   return $this->Works->FillDataRecordAssoc($resourceID,"No existen definidos docentes con la identificación ingresada."); 
            }
            
            public function ObtenerDocentePlanificacion($prepareDQL) //OK 
            {       $SQL1=""; $SQL2=""; $SQL3="";

                    if (array_key_exists("docente", $prepareDQL))
                    {   $DocenteID = $prepareDQL['docente'];
                        $SQL3 = " AND Y.CEDULA='$DocenteID' ";
                    }
                    if (array_key_exists("facultad", $prepareDQL))
                    {   if (count($prepareDQL['facultad'])>0)
                        {   $FacultadID = implode(',',$prepareDQL['facultad']);
                            $SQL1 = " AND J.IDFACULTAD IN ($FacultadID) ";
                        }
                    }
                    if (array_key_exists("texto", $prepareDQL))
                    {   $DocenteTX = $prepareDQL['texto'];
                        $SQL2 = " AND (Y.APELLIDOS+' '+Y.NOMBRES LIKE '%".$DocenteTX."%')";
                    }
                    $SQL = "SELECT DISTINCT Y.ID,Y.CEDULA,Y.NOMBRES,Y.APELLIDOS 
                            FROM   POSTGRADO..COL_PLAN_ANALITICO AS X,
                                    POSTGRADO..COL_DOCENTE AS Y,
                                    POSTGRADO..COL_CICLOS AS Z,
				                    POSTGRADO..ADM_MAESTRIA AS J
                            WHERE   X.IDDOCENTE=Y.ID AND X.IDESTADO=1 AND X.IDCICLO=Z.ID AND Z.IDESTADO=1 AND J.ID=X.IDMAESTRIA".$SQL1.$SQL2.$SQL3;        
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Docente definidos para el ciclo vigente.");
            }

            public function ObtenerDocenteByTX($prepareDQL) //OK 
            {       $SQL1 = "";
                    $SQL2 = "";
            
                    if (array_key_exists("texto", $prepareDQL))
                    {   $DocenteTX = $prepareDQL['texto'];
                        $SQL1 = " AND V.CEDULA COLLATE Modern_Spanish_CI_AS NOT IN (SELECT X.CEDULA FROM POSTGRADO..COL_DOCENTE AS X WHERE X.APELLIDOS+' '+X.NOMBRES LIKE '%".$DocenteTX."%') ";
                        $SQL1.= " AND (V.APELLIDOS+' '+V.NOMBRES LIKE '%".$DocenteTX."%') ";
                        $SQL2 = " WHERE (X.APELLIDOS+' '+X.NOMBRES LIKE '%".$DocenteTX."%') ";
                    }

                    $SQL = "SELECT TOP 10 0 AS IDD, V.CEDULA AS CEDULA, V.NOMBRES AS NMB, V.APELLIDOS AS APLL, V.EMAIL AS EML, 
                                   V.DIRECCION AS DIR, V.TELEF_DMCL AS FONO, 1 AS ESTADO, 0 AS GESTION, 0 AS ROLUSER 
                            FROM  
                                   ".LINKSVR."DATOS_PERSONAL AS V INNER JOIN  
                                   ".LINKSVR."ADMISION AS W ON V.CEDULA=W.CEDULA AND UPPER(W.DESCR_CAT_CARGO)='DOCENTE' AND W.ESTADOREG=0 ".$SQL1." 
                            UNION
                            SELECT TOP 10 X.ID AS IDD,
                                X.CEDULA COLLATE Modern_Spanish_CI_AS AS CEDULA, X.NOMBRES COLLATE Modern_Spanish_CI_AS AS NMB,
                                X.APELLIDOS COLLATE Modern_Spanish_CI_AS AS APLL, X.EMAIL COLLATE Modern_Spanish_CI_AS AS EML, 
                                X.DIRECCION COLLATE Modern_Spanish_CI_AS AS DIR, X.TELEFONO COLLATE Modern_Spanish_CI_AS AS FONO,
                                X.IDESTADO AS ESTADO, X.IDGESTION AS GESTION, X.IDROLUSER AS ROLUSER
                            FROM POSTGRADO..COL_DOCENTE AS X ".$SQL2;
                                               
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen Docente definidos.");
            }                        
        }
?>        
