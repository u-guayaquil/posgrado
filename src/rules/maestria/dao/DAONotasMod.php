<?php
    
    require_once("src/base/DBDAO.php");

    class DAONotasMod extends DBDAO
    {       private static $entityNotasMod;
            private $Works;
              
            public function __construct()
            {       self::$entityNotasMod = DBDAO::getConnection();
                    $this->Works = self::$entityNotasMod->MyFunction();
            }
              
            function ObtenerNotasMod($Params,$Compte)
            {       $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $ParaleloID=$Params[4];
                    
                    $SQL = "SELECT ISNULL(Y.ID,0) AS IDNOTAS, X.ID AS IDESTUDMATE, V.CEDULA AS CEDULA, 
                                V.APELLIDOS+' '+V.NOMBRES AS ESTUDIANTE, ISNULL(Y.NOTA,'') AS NOTAS, 
                                ISNULL(SUM(H.EFECTIVA),0)+ISNULL(SUM(H.JUSTIFICADA),0) AS ASIST, 
                                $Compte AS TCD, EC.IDUAD AS UAD
                                FROM   POSTGRADO..COL_ESTUDIANTE AS V
                                        INNER JOIN (POSTGRADO..COL_ESTUDIANTE_COHORTE AS EC 
                                        INNER JOIN (POSTGRADO..COL_ESTUDIANTE_COHORTE_MATRICULA AS W 
                                        INNER JOIN (POSTGRADO..COL_ESTUDIANTE_MATERIA AS X 
                                        LEFT JOIN (POSTGRADO..COL_ESTUDIANTE_MATERIA_ASISTENCIA AS H 
                                        LEFT JOIN   POSTGRADO..COL_ESTUDIANTE_MATERIA_NOTAS AS Y 
                                               ON H.IDESTUDIANTEMATERIA=Y.IDESTUDIANTEMATERIA)
                                               ON X.ID=H.IDESTUDIANTEMATERIA)      
                                               ON W.IDESTADO<>0 AND W.ID=X.IDMATRICULA)
                                               ON EC.ID=W.IDESTUDCOHORTE)
                                               ON V.ID=X.IDESTUDIANTE
                                WHERE X.IDESTADO=1 AND X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDPARALELO=$ParaleloID
                                GROUP by ISNULL(Y.ID,0), X.ID, V.CEDULA, V.APELLIDOS+' '+V.NOMBRES, ISNULL(Y.NOTA,''), EC.IDUAD
                                ORDER BY V.APELLIDOS+' '+V.NOMBRES ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existe definida la materia registro de notas.");
            }   
            
            function RegistraNotasMod($Form)
            {       $DataCollection = array();
                    $Modulo = json_decode(Session::getValue('Sesion')); 
                    foreach($Form as $Row)    
                    {       $Cll = str_pad($Row->NO,2,'0',STR_PAD_LEFT);
                            if ($Row->ID==0)      
                            {   $SQL = "Insert Into POSTGRADO..COL_ESTUDIANTE_MATERIA_NOTAS (IDESTUDIANTEMATERIA,FECHAEVAL,NOTA,IDUSCREA,FEUSCREA) OUTPUT Inserted.ID ";
                                $SQL.= "Values('$Row->ESMA',getDate(),'$Row->JSON','$Modulo->Idusrol',getDate()) ";
                                $resourceID = $this->Works->Query($SQL);
                                $DataCollection[] = array("TD_".$Cll."_000",$this->Works->FieldDataByName($resourceID,'ID'));
                            }
                            else 
                            {   $SQL = "Update POSTGRADO..COL_ESTUDIANTE_MATERIA_NOTAS Set NOTA='$Row->JSON', IDUSMODIFICA='$Modulo->Idusrol', FEUSMODIFICA=getDate() ";
                                $SQL.= "WHERE ID='$Row->ID'";
                                $resourceID = $this->Works->Query($SQL);
                                $DataCollection[] = array("TD_".$Cll."_000",$Row->ID);
                            }
                    }
                    return array(true,$DataCollection);
            }    
            
            function ActualizaNotasCierre($IdEstm,$VaNota,$VaAsis,$Estad)
            {       $Estad = substr($Estad,0,1);
                    $SQL = "Update POSTGRADO..COL_ESTUDIANTE_MATERIA Set NOTA=$VaNota, ASISTENCIA=$VaAsis, ESTACADEMICO='$Estad' Where ID=$IdEstm";
                    $resourceID = $this->Works->Query($SQL);
                    if ($resourceID)
                        return true;
                    else
                        return false;
            }
            
            function RegCierreActaCalificacion($Params)
            {     //[DOCTE,CICLO,MAEST,MATER,GRUPO,POLEV,NTETAP] 
                    if ($this->ObtCierreActaCalificacion($Params)==0)
                    {   $Modulo = json_decode(Session::getValue('Sesion')); 
                        $SQL = "Insert Into POSTGRADO..COL_CICLOS_MATERIA_CIERRE(IDDOCENTE,IDCICLO,IDMAESTRIA,IDMATERIA,IDGRUPO,ETAPA,FECIERRE,IDUSCREA,FEUSCREA) OUTPUT Inserted.ID "; 
                        $SQL.= "Values('$Params[0]','$Params[1]','$Params[2]','$Params[3]','$Params[4]','$Params[6]',getDate(),'$Modulo->Idusrol',getDate())";
                        $resourceID = $this->Works->Query($SQL);
                        return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
                    }    
                    return array(false,"El acta no se encuentra cerrado.");
            }
            
            function ObtCierreActaCalificacion($Params)
            {       $SQL = "SELECT COUNT(ID) AS EXISTE FROM POSTGRADO..COL_CICLOS_MATERIA_CIERRE WHERE IDCICLO='$Params[1]' AND IDMAESTRIA='$Params[2]' AND IDMATERIA='$Params[3]' AND IDGRUPO='$Params[4]' AND ETAPA='$Params[6]' AND IDESTADO=1 ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FieldDataByName($resourceID,'EXISTE');
            }
            
        /*
            function ObtenerPermisoNotasMod($Params,$Compte)
            {       $DocenteID=$Params[0];
                    $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $GrupoID=$Params[4];
                    $FechaID=$Params[5];
                    
                    $SQL = "SELECT ISNULL(X.ID,0) AS ID,ISNULL(X.OBSERVACION,'') AS MOTIVO,ISNULL(X.FEUSCREA,GETDATE()) AS SOLICITUD, 
                           (SELECT W.PARALELO FROM POSTGRADO..COL_COHORTE_PARALELO AS W WHERE W.ID=$GrupoID) AS PARALELO,Y.MATERIA,ISNULL(X.FECHA,'$FechaID') AS FECHA
                            FROM 
                            POSTGRADO..COL_MATERIA AS Y LEFT JOIN
                            POSTGRADO..COL_PERMISOS_REG_ASISTENCIA AS X                            
                            ON X.IDESTADO=1 AND X.IDTIPO='$Compte' AND X.IDCICLO=$CicloID AND X.IDMAESTRIA=$MaestriaID AND X.IDMATERIA=$MateriaID AND X.IDGRUPO=$GrupoID AND X.FECHA='$FechaID'
                            WHERE Y.ID=$MateriaID ";
                    $resourceID = $this->Works->Query($SQL);
                    return $this->Works->FillDataRecordAssoc($resourceID,"No existen permisos para registro de asistencia.");
            }
            
            function RegistraPermisoNotasMod($Params,$Compte)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $DocenteID=$Params[0];
                    $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $GrupoID=$Params[4];
                    $FechaID=$Params[5];
                    $MotivoTX= strtoupper(Trim($Params[6]));
                    $SQL = "INSERT INTO POSTGRADO..COL_PERMISOS_REG_ASISTENCIA(IDDOCENTE,IDTIPO,IDCICLO,IDMAESTRIA,IDMATERIA,IDGRUPO,FECHA,OBSERVACION,IDUSCREA,FEUSCREA) ";
                    $SQL.= "OUTPUT Inserted.ID VALUES ($DocenteID,'$Compte',$CicloID,$MaestriaID,$MateriaID,$GrupoID,'$FechaID','$MotivoTX','$Modulo->Idusrol',getDate())";
                    $resourceID = $this->Works->Query($SQL);
                    if ($resourceID)
                        return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
                    else
                        return array(false,"No se pudo tramitar el permiso.");
            }
            
            function ActualizaEstadoPermisoNotasMod($Params,$Compte,$Status)
            {       $Modulo = json_decode(Session::getValue('Sesion')); 
                    $DocenteID=$Params[0];
                    $CicloID=$Params[1];
                    $MaestriaID=$Params[2];
                    $MateriaID=$Params[3];
                    $GrupoID=$Params[4];
                    $FechaID=$Params[5];
                    $SQL = "UPDATE POSTGRADO..COL_PERMISOS_REG_ASISTENCIA SET IDESTADO=$Status,IDUSMODIFICA='$Modulo->Idusrol',FEUSMODIFICA=getDate()  
                            WHERE IDTIPO='$Compte' AND IDCICLO=$CicloID AND IDMAESTRIA=$MaestriaID AND IDMATERIA=$MateriaID AND IDGRUPO=$GrupoID AND FECHA='$FechaID'";
                    $resourceID = $this->Works->Query($SQL);
                    return true;
            }
        */
        function ActualizaNotasEstadoMod($idNotas,$Notas){      
                $SQL = "Update POSTGRADO..COL_ESTUDIANTE_MATERIA_NOTAS Set NOTA='".$Notas."' Where ID=$idNotas";
                //print_r($SQL);
                $resourceID = $this->Works->Query($SQL);
                if ($resourceID)
                    return "OK";
                else
                    return "Error";
        }
    }    
