<?php
require_once("src/base/DBDAO.php");

class DAOResidencia extends DBDAO
{   private static $entityResidencia;
	private $Works;                
	
	public function __construct()
	{      self::$entityResidencia = DBDAO::getConnection();
			$this->Works = self::$entityResidencia->MyFunction();                            
	}

	public function ObtenerResidenciaByCohorte($idCohorte)
	{   $DSQL = "SELECT HP.ID,H.ID AS IDHOSPITAL,H.UAD AS HOSPITAL,HP.RESIDENCIA,HP.FEINICIO AS INICIO,HP.FEFIN AS FIN,HP.IDESTADO
					FROM POSTGRADO..COL_HOSPITAL_RESIDENCIA AS HP, POSTGRADO..COL_HOSPITAL H
					WHERE H.ID = HP.IDHOSPITAL
					AND HP.IDCOHORTE = '$idCohorte'";
		$resourceID = $this->Works->Query($DSQL);
		return $this->Works->FillDataRecordAssoc($resourceID,"No existen residencias definidas."); 
	} 
	
	public function ObtenerResidenciaByID($Form)
	{   $id = $Form["id"];

		$DSQL = "SELECT HP.ID,H.ID AS IDHOSPITAL,H.UAD AS HOSPITAL,HP.RESIDENCIA,HP.FEINICIO AS INICIO,HP.FEFIN AS FIN,HP.IDESTADO
					FROM POSTGRADO..COL_HOSPITAL_RESIDENCIA AS HP, POSTGRADO..COL_HOSPITAL H
					WHERE H.ID = HP.IDHOSPITAL
					AND HP.ID = '$id' AND HP.IDCOHORTE = '".$Form["idCohorte"]."'";                                                 
		
		$resourceID = $this->Works->Query($DSQL);                  
		return $this->Works->NextRecordObject($resourceID); 
	}

	public function ObtenerResidenciaByIDProcesado($id)
	{   $DSQL = "SELECT HP.ID,H.ID AS IDHOSPITAL,H.UAD AS HOSPITAL,HP.RESIDENCIA,HP.FEINICIO AS INICIO,HP.FEFIN AS FIN,HP.IDESTADO
					FROM POSTGRADO..COL_HOSPITAL_RESIDENCIA AS HP, POSTGRADO..COL_HOSPITAL H
					WHERE H.ID = HP.IDHOSPITAL
					AND HP.ID = '$id'";                                              
		
		$resourceID = $this->Works->Query($DSQL);                  
		return $this->Works->FillDataRecordAssoc($resourceID,"No existen residencias definidas."); 
	}
	
	public function ObtenerResidenciaByDescripcion($Form)
	{   $Text = trim($Form["txt"]);
		$idCohorte = $Form["idCohorte"];

		$DSQL = "SELECT HP.ID,H.ID AS IDHOSPITAL,H.UAD AS HOSPITAL,HP.RESIDENCIA,HP.FEINICIO AS INICIO,HP.FEFIN AS FIN,HP.IDESTADO
					FROM POSTGRADO..COL_HOSPITAL_RESIDENCIA AS HP, POSTGRADO..COL_HOSPITAL H
					WHERE H.ID = HP.IDHOSPITAL
					AND HP.IDCOHORTE = '$idCohorte' AND HP.RESIDENCIA LIKE '%".$Text."%'";                                                
		
		$resourceID = $this->Works->Query($DSQL);                  
		return $this->Works->FillDataRecordAssoc($resourceID,"No existen residencias definidas."); 
	}
	
	public function ObtenerMaestriaByDescripcion($PrepareDQL)
	{	$SQL = "SELECT F.ID AS IDFACULTAD, F.DESCRIPCION AS FACULTAD, M.MAESTRIA, MV.ID AS IDVERSION, MV.VERSION, MV.COHORTES 
				FROM POSTGRADO..ADM_MAESTRIA AS M, POSTGRADO..ADM_FACULTAD AS F, POSTGRADO..ADM_MAESTRIA_VERSION MV 
				WHERE M.IDFACULTAD = F.ID 
				AND MV.IDMAESTRIA = M.ID
				AND MV.IDESTADO = 1 
				AND M.MAESTRIA LIKE '%".$PrepareDQL['txt']."%'
				ORDER BY F.ID ASC";
		
		$resourceID = $this->Works->Query($SQL);                  
		return $this->Works->FillDataRecordAssoc($resourceID,"No existen Maestrías definidos."); 
	}
	
	public function ObtenerCohorteByDescripcion($PrepareDQL)
	{   $SQL = "SELECT MC.ID, MC.COHORTE, MC.FEINICIO, MC.FEFIN, MC.IDESTADO 
				FROM POSTGRADO..ADM_MAESTRIA_COHORTE AS MC, POSTGRADO..ADM_MAESTRIA_VERSION AS MV
				WHERE MC.IDMAESVER = MV.ID 
				AND MC.COHORTE LIKE '%".$PrepareDQL['txt']."%' 
				AND MV.ID = ".$PrepareDQL['mtr']."
				AND MC.IDESTADO = 1
				ORDER BY MC.ID ASC";
		
		$resourceID = $this->Works->Query($SQL);                  
		return $this->Works->FillDataRecordAssoc($resourceID,"No existen Cohortes definidos."); 
	}

	public function ObtenerHospitalByDescripcion($PrepareDQL)
	{	$SQL = "SELECT ID, UAD AS HOSPITAL, SIGLAS, FECREACION, IDESTADO
				FROM POSTGRADO..COL_HOSPITAL
				WHERE IDESTADO = 1 AND ID > 0
				AND UAD LIKE '%".$PrepareDQL['txt']."%'
				ORDER BY ID ASC";
		
		$resourceID = $this->Works->Query($SQL);                  
		return $this->Works->FillDataRecordAssoc($resourceID,"No existen Hospitales definidos."); 
	}

	public function InsertaResidencia($Form)
	{   $Modulo = json_decode(Session::getValue('Sesion'));
		$Residencia = strtoupper($Form->descripcion);
		$idCohorte = $Form->idcohorte; $idHospital = $Form->idhospital;
		$Inicio = $Form->feinicio; $Fin = $Form->fefin;

		$SQL = "INSERT INTO POSTGRADO..COL_HOSPITAL_RESIDENCIA(IDCOHORTE,IDHOSPITAL,RESIDENCIA,FEINICIO,FEFIN,IDUSCREA,FECREACION) 
					OUTPUT INSERTED.ID 
				VALUES('$idCohorte','$idHospital','$Residencia','$Inicio','$Fin','".$Modulo->Idusrol."',getDate())";
		$resourceID = $this->Works->Query($SQL);
		return $this->Works->FieldDataByName($resourceID,'ID');
	}

	public function ActualizaResidencia($Form)
	{       $Modulo = json_decode(Session::getValue('Sesion'));
			$Residencia = strtoupper($Form->descripcion);
			$idCohorte = $Form->idcohorte; $idHospital = $Form->idhospital;
			$Inicio = $Form->feinicio; $Fin = $Form->fefin; $Estado = $Form->estado;
			$SQL = "UPDATE POSTGRADO..COL_HOSPITAL_RESIDENCIA 
					SET	IDCOHORTE='$idCohorte',
						IDHOSPITAL='$idHospital',
						RESIDENCIA='$Residencia',
						FEINICIO='$Inicio',
						FEFIN='$Fin',
						IDESTADO='$Estado',
						IDUSMODIFICA='$Modulo->Idusrol',
						FEMODIFICA=getDate() 
					WHERE ID='$Form->id'";
			$resourceID = $this->Works->Query($SQL); 
			return intval($Form->id);
	}
	
	public function DesactivaResidencia($id)
	{       $Modulo = json_decode(Session::getValue('Sesion'));
			$SQL = "UPDATE POSTGRADO..COL_HOSPITAL_RESIDENCIA 
					SET IDESTADO=0, IDUSMODIFICA='".$Modulo->Idusrol."', FEMODIFICA=getDate() 
					WHERE id='$id' ";
			$resourceID = $this->Works->Query($SQL);    
			return 0;                    
	}
}
