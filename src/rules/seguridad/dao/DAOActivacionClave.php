<?php
        require_once("src/base/DBDAO.php");
        
        class DAOActivacionClave extends DBDAO
		{   private static $entityPoliticas;
			private $Works;
	
			public function __construct()
			{      self::$entityPoliticas = DBDAO::getConnection();
					$this->Works = self::$entityPoliticas->MyFunction();
			}
   
            function ExisteToken($Token)
			{   include "ConexionRecovery.php";
				$SQL = "SELECT ID,IDUSUARIO,CLAVE,FECADUCIDAD,IDESTADO FROM BdCert..SEG_RECUPERACION_CLAVE WHERE TOKEN = '$Token'";
				$resourceID = sqlsrv_query($con,$SQL);
				return $this->FillDataRecordAssoc($resourceID,"El enlace solicitado no existe.");
            }

			public function FillDataRecordAssoc($resourceID,$DefaultMsg = "")
                {       if ($resourceID>=0)
                        {   $DataCount = 0;
                            $DataCollection = array();
                            while($filas = $this->NextRecordAssoc($resourceID))
                            {     $DataCount++;
                                  $DataCollection[] = $filas;
                            }
                            $this->CloseResource($resourceID);
                            $Result = ($DataCount>0);
                            return array($Result,($Result ? $DataCollection : $DefaultMsg));
                        }
                        return array(false,"Error en ejecución de consulta.");
                }

			public function NextRecordAssoc($resourceID)
			{       if (is_resource($resourceID))
							return sqlsrv_fetch_array($resourceID, SQLSRV_FETCH_ASSOC);
					else
					return false;
			}

			public function CloseResource($resourceID)
                {       if (is_resource($resourceID)) sqlsrv_free_stmt($resourceID);
                }
            
            function TokenEstadoProcesado($ID)
            {       include "ConexionRecovery.php";
					$SQL = "UPDATE BdCert..SEG_RECUPERACION_CLAVE SET IDESTADO = 9 WHERE ID = $ID";
                    $resourceID = sqlsrv_query($con,$SQL);
                    return $this->FillDataRecordAssoc($resourceID,"Error en la base de datos.");
            }
            
            function TokenEstadoCaducado($ID)
            {   include "ConexionRecovery.php";
				$SQL = "UPDATE BdCert..SEG_RECUPERACION_CLAVE SET IDESTADO = 8 WHERE ID = $ID";
                $resourceID = sqlsrv_query($con,$SQL);
                return $this->FillDataRecordAssoc($resourceID,"Error en la base de datos.");
            }
            
            function UpdateClaveUsuario($IDUSUARIO,$CLAVE)
            {       include "ConexionRecovery.php";
					$CLAVE = md5($CLAVE);
                    $SQL = "UPDATE BdCert..SEG_USUARIO SET PASSWORD = '$CLAVE' WHERE IDUSUARIO = '$IDUSUARIO'";
                    $resourceID = sqlsrv_query($con,$SQL);
                    return $this->FillDataRecordAssoc($resourceID,"Error en la base de datos.");
            }
        }    
