<?php
        require_once("src/base/DBDAO.php");
        
        class DAORecovery extends DBDAO
        {       private static $entityPoliticas;
                private $Works;
        
                public function __construct()
                {      self::$entityPoliticas = DBDAO::getConnection();
                       $this->Works = self::$entityPoliticas->MyFunction();
                }
                
                public function BuscarUsuario($id)
                {   $SQL = "SELECT IDUSUARIO FROM BDCERT..SEG_USUARIO WHERE IDUSUARIO='$id' AND IDESTADO=1 AND CONFIRMACION='N'";
                    $resourceID = $this->Works->Query($SQL); 
                    return $this->Works->FillDataRecordAssoc($resourceID,"El usuario ingresado no existe.");
                }

                public function BuscarTokenActivoByID($id)
                {   $SQL = "SELECT ID,FECADUCIDAD FROM BdCert..SEG_RECUPERACION_CLAVE WHERE IDUSUARIO = '$id' AND IDESTADO = 1";
                    $resourceID = $this->Works->Query($SQL); 
                    return $this->Works->FillDataRecordAssoc($resourceID,"No hay token activo para este usuario.");
                }


                public function BuscarRolUsuarioSIUG($id,$rolid)
                {       $DbModulo = self::$entityPoliticas->DbModulo();
                        switch ($rolid) 
                        {   case 1: 
                                    if ($DbModulo=="POSGRADO")
                                    {   $SQL = "SELECT CEDULA,NOMBRES AS NOMBRE,APELLIDOS AS APELLIDO,EMAIL FROM POSTGRADO..COL_ESTUDIANTE WHERE CEDULA='$id' AND IDESTADO=1"; 
                                    }
                            break;
                            case 2: 
                                    if ($DbModulo=="POSGRADO")
                                    {   $SQL = "SELECT CEDULA,NOMBRES AS NOMBRE,APELLIDOS AS APELLIDO,EMAIL FROM POSTGRADO..COL_DOCENTE WHERE CEDULA='$id' AND IDESTADO=1 ";   
                                    }
                            break;
                            case 3: $SQL = "SELECT X.CEDULA,Y.NOMBRES AS NOMBRE,Y.APELLIDOS AS APELLIDO,Y.EMAIL  
                                            FROM [10.87.117.112].DB_RRHH.dbo.ADMISION AS X, 
                                            [10.87.117.112].DB_RRHH.dbo.DATOS_PERSONAL AS Y
                                            WHERE X.CEDULA='$id' AND (UPPER(DESCR_CAT_CARGO)='ADMINISTRATIVO') AND 
                                                  X.CEDULA = Y.CEDULA ";
                                            $resourceID = $this->Works->Query($SQL); 
                                            $Resp = $this->Works->FillDataRecordAssoc($resourceID,"Usted no tiene asignado el rol seleccionado.");                
                                            if ($Resp[0]==false)
                                            {   if ($DbModulo=="POSGRADO")
                                                {   $SQL = "SELECT CEDULA,NOMBRES AS NOMBRE,APELLIDOS AS APELLIDO,EMAIL FROM POSTGRADO..COL_DOCENTE WHERE IDGESTION=3 AND IDROLUSER<>0 AND CEDULA='$id' AND IDESTADO=1 ";   
                                                    $resourceID = $this->Works->Query($SQL); 
                                                    return $this->Works->FillDataRecordAssoc($resourceID,"Usted no tiene asignado el rol seleccionado.");
                                                }
                                            }
                                            return $Resp;
                            break;
                            case 4:
                            break;
                        }  
                        $resourceID = $this->Works->Query($SQL); 
                        return $this->Works->FillDataRecordAssoc($resourceID,"Usted no tiene asignado el rol seleccionado.");
                }                
                
                public function ObtenerPoliticas($Sistema)
                {      $SDQL = "SELECT SisVariable,SisValor FROM BdCert..SEG_POLITICA_CLAVE WHERE SisName='$Sistema' AND SisClase='STRPASS' ";
                       $resourceID = $this->Works->Query($SDQL); 
                       return $this->Works->FillDataRecordAssoc($resourceID);
                }                 
                
                function ObtenerRolesModulo()
                {       $DbModulo = self::$entityPoliticas->DbModulo();
                        
                        $SQLD = "SELECT '['+LTRIM(STR(X.ID))+','+LTRIM(STR(Y.ID))+','+LTRIM(STR(Z.ID))+']' AS ID, Z.DESCRIPCION 
                                FROM BdCert..SEG_MODULO as X, BdCert..SEG_MODULO_ROL as Y, BdCert..SEG_ROL AS Z    
                                WHERE X.DESCRIPCION ='$DbModulo' AND X.ID=Y.IDMODULO AND Z.ID=Y.IDROL AND Y.ESTADO=1 ";
                                $resourceID = $this->Works->Query($SQLD);
                                return $this->Works->FillDataRecordAssoc($resourceID,"Roles no definidos para el Modulo ".$DbModulo);
                }

                function TokenEstadoCaducado($ID)
                {       $SQL = "UPDATE BdCert..SEG_RECUPERACION_CLAVE SET IDESTADO = 8 WHERE ID = $ID";
                        $resourceID = $this->Works->Query($SQL);
                        return $this->Works->FillDataRecordAssoc($resourceID,"Error en la base de datos.");
                }
        }    
