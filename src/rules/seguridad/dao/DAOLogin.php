<?php
        require_once("src/base/DBDAO.php");
        
        class DAOLogin extends DBDAO
        {       private static $entityPoliticas;
                private $Works;
        
                public function __construct()
                {      self::$entityPoliticas = DBDAO::getConnection();
                       $this->Works = self::$entityPoliticas->MyFunction();
                }
                
                public function BuscarRolByID($Id)
                {       $resourceID = $this->Works->Query("SELECT * FROM BdCert..SEG_ROL WHERE ID='$Id'");
                        return $this->Works->FillDataRecordAssoc($resourceID);
                }
                
                public function BuscarLoginRolUsuarioSIUG($id,$rolid)
                {       $DbModulo = self::$entityPoliticas->DbModulo();
                        switch ($rolid) 
                        {   case 1: 
                                    if ($DbModulo=="POSGRADO")
                                    {   $SQL = "SELECT CEDULA,NOMBRES AS NOMBRE,APELLIDOS AS APELLIDO,EMAIL FROM POSTGRADO..COL_ESTUDIANTE WHERE CEDULA='$id' AND IDESTADO=1"; 
                                    }
                            break;
                            case 2: 
                                    if ($DbModulo=="POSGRADO")
                                    {   $SQL = "SELECT CEDULA,NOMBRES AS NOMBRE,APELLIDOS AS APELLIDO,EMAIL FROM POSTGRADO..COL_DOCENTE WHERE CEDULA='$id' AND IDESTADO=1 ";   
                                    }
                            break;
                            case 3: $SQL = "SELECT X.CEDULA,Y.NOMBRES AS NOMBRE,Y.APELLIDOS AS APELLIDO,Y.EMAIL  
                                            FROM ".LINKSVR."ADMISION AS X, 
                                                 ".LINKSVR."DATOS_PERSONAL AS Y
                                            WHERE X.CEDULA='$id' AND (UPPER(DESCR_CAT_CARGO)='ADMINISTRATIVO') AND 
                                                  X.CEDULA = Y.CEDULA ";
                                                  
                                     /*$SQL = "SELECT X.CEDULA,Y.NOMBRES AS NOMBRE,Y.APELLIDOS AS APELLIDO,Y.EMAIL  
                                             FROM ".LINKSVR."ADMISION AS X, 
                                                  ".LINKSVR."DATOS_PERSONAL AS Y
                                             WHERE X.CEDULA='$id' AND X.ESTADOREG=0 AND (UPPER(DESCR_CAT_CARGO)='ADMINISTRATIVO') AND 
                                                   X.CEDULA = Y.CEDULA ";*/

                                            $resourceID = $this->Works->Query($SQL); 
                                            $Resp = $this->Works->FillDataRecordAssoc($resourceID,"Usted no tiene asignado el rol seleccionado.");                
                                            if ($Resp[0]==false)
                                            {   if ($DbModulo=="POSGRADO")
                                                {   $SQL = "SELECT CEDULA,NOMBRES AS NOMBRE,APELLIDOS AS APELLIDO,EMAIL FROM POSTGRADO..COL_DOCENTE WHERE IDGESTION=3 AND IDROLUSER<>0 AND CEDULA='$id' AND IDESTADO=1 ";   
                                                    $resourceID = $this->Works->Query($SQL); 
                                                    return $this->Works->FillDataRecordAssoc($resourceID,"Usted no tiene asignado el rol seleccionado.");
                                                }
                                            }
                                            return $Resp;
                            break;
                            case 4:
								if ($DbModulo=="POSGRADO")
								{   $SQL = "SELECT CEDULA,NOMBRES AS NOMBRE,APELLIDOS AS APELLIDO,EMAIL FROM POSTGRADO..COL_EXTERNO WHERE CEDULA='$id' AND IDESTADO=1 ";   
								}
                            break;
                            case 5:
                            break;
                        }  
                        $resourceID = $this->Works->Query($SQL); 
                        return $this->Works->FillDataRecordAssoc($resourceID,"Usted no tiene asignado el rol seleccionado.");
                }                
                
                public function ObtenerPoliticas($Sistema)
                {      $SDQL = "SELECT SisVariable,SisValor FROM BdCert..SEG_POLITICA_CLAVE WHERE SisName='$Sistema' AND SisClase='STRPASS' ";
                       $resourceID = $this->Works->Query($SDQL); 
                       return $this->Works->FillDataRecordAssoc($resourceID);
                }                 
                
                public function InsertaLoginLog($Loginlog,$Msg="")
                {      $idrolusr = $Loginlog['id'];
                       $emcuenta = $Loginlog['email'];
                       $ipacceso = $Loginlog['ipaccess'];
                       $resourceID = $this->Works->Query("INSERT INTO BdCert..SEG_LOGINLOG(IDUSROL,CUENTA,IPACCESO,FEACCESO,OBSERVACION) OUTPUT INSERTED.ID VALUES('$idrolusr','$emcuenta','$ipacceso',getDate(),'$Msg') ");
                       return array(true,$this->Works->FieldDataByName($resourceID,'ID'));
                }
                
                function ObtenerRolesModulo()
                {       $DbModulo = self::$entityPoliticas->DbModulo();
                        
                        $SQLD = "SELECT '['+LTRIM(STR(X.ID))+','+LTRIM(STR(Y.ID))+','+LTRIM(STR(Z.ID))+']' AS ID, Z.DESCRIPCION 
                                FROM BdCert..SEG_MODULO as X, BdCert..SEG_MODULO_ROL as Y, BdCert..SEG_ROL AS Z    
                                WHERE X.DESCRIPCION ='$DbModulo' AND X.ID=Y.IDMODULO AND Z.ID=Y.IDROL AND Y.ESTADO=1 ";
                                $resourceID = $this->Works->Query($SQLD);
                                return $this->Works->FillDataRecordAssoc($resourceID,"Roles no definidos para el Modulo ".$DbModulo);
                }
                
      
        }    
