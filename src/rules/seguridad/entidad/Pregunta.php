<?php
    
    /**
     *
     * @Entity
     * @Table(name="pregunta", schema="sistema")
     */
    class Pregunta
    {   /**
          * @Id
          * @Column(name="id", type="integer", nullable=false)
          * @GeneratedValue
          */
        private $id;

        /**
          * @Column(name="descripcion", type="string", length=40, nullable=false)
          */
        private $descripcion;

        public function getId()
        {      return $this->id;
        }

        public function setDescripcion($descripcion)
        {      $this->descripcion = $descripcion;
        }

        public function getDescripcion()
        {      return $this->descripcion;
        }
    }

