<?php
    
     /**
      * @Entity
      * @Table(name="loginlog", schema = "sistema")
      */
      class Loginlog
      {
           /**
            *
            * @Id
            * @Column(name="id", type="integer", nullable=false)
            * @GeneratedValue
            */
            private $id;

           /**
            *
            * @Column(name="cuenta", type="string", length=80, nullable=false)
            */
            private $cuenta;

           /**
            *
            * @Column(name="ipacceso", type="string", length=15, nullable=false)
            */
            private $ipacceso;

           /**
            *
            * @Column(name="feacceso", type="datetime", nullable=false)
            */
            private $feacceso;

           /**
            *
            * @Column(name="fesalida", type="datetime", nullable=true)
            */
            private $fesalida;

            public function getId()
            {      return $this->id;
            }

            public function setCuenta($cuenta)
            {      $this->cuenta = $cuenta;
            }

            public function getCuenta()
            {      return $this->cuenta;
            }

            public function setIpacceso($ipacceso)
            {      $this->ipacceso = $ipacceso;
            }

            public function getIpacceso()
            {      return $this->ipacceso;
            }

            public function setFeacceso($feacceso)
            {      $this->feacceso = $feacceso;
            }

            public function getFeacceso()
            {      return $this->feacceso;
            }

            public function setFesalida($fesalida)
            {      $this->fesalida = $fesalida;
            }

            public function getFesalida()
            {      return $this->fesalida;
            }
      }

