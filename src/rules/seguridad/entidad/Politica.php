<?php
    
    /**
     *
     * @Entity
     * @Table(name="politica", schema="sistema")
     */
    class Politica
    {       /**
              * @Id
              * @Column(name="id", type="integer")
              * @GeneratedValue
              */
            private $id;

            /**
              * @Column(name="clase", type="string", length=15)
              */
            private $clase;

            /**
              * @Column(name="numero", type="integer")
              */
            private $numero = 0;

            public function getId()
            {      return $this->id;
            }

            public function setClase($clase)
            {      $this->clase = $clase;
            }

            public function getClase()
            {      return $this->clase;
            }

            public function setNumero($numero)
            {      $this->numero = $numero;
            }

            public function getNumero()
            {      return $this->numero;
            }
    }        
?>
