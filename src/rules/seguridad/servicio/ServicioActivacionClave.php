<?php   require_once("src/rules/seguridad/dao/DAOActivacionClave.php");   

        class ServicioActivacionClave
        {       private $DAOActivacionClave;

                function __construct()
                {       $this->DAOActivacionClave = new DAOActivacionClave();
                }

                function ExisteToken($Token)
                {       return $this->DAOActivacionClave->ExisteToken($Token);
                }
                
                function TokenEstadoProcesado($ID)
                {       return $this->DAOActivacionClave->TokenEstadoProcesado($ID);
                }
                
                function TokenEstadoCaducado($ID)
                {       return $this->DAOActivacionClave->TokenEstadoCaducado($ID);
                }
                
                function UpdateClaveUsuario($IDUSUARIO,$CLAVE)
                {       return $this->DAOActivacionClave->UpdateClaveUsuario($IDUSUARIO,$CLAVE);
                }
       }
       
       
?>