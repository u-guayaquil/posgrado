<?php   require_once('src/libs/clases/Mailer.php');
       
        require_once("src/rules/seguridad/dao/DAORecovery.php");    
        require_once("src/rules/sistema/dao/DAOUsuario.php");    
        require_once("src/rules/sistema/dao/DAOUsuarioPerfil.php");

        class ServicioRecovery
        {       private $DAORecovery;
                private $DAOUsuario;
                private $DAOUsuarioPerfil;
                private $Utils;
                private $DateControl;
                private $Recovery;
             
                function __construct()
                {       $this->DAORecovery = new DAORecovery();
                        $this->DAOUsuario = new DAOUsuario();
                        $this->DAOUsuarioPerfil = new DAOUsuarioPerfil();
                        $this->Utils = New Utils();                                
                        $this->DateControl = new DateControl();
                }
                
                function CargaRolesModulo()
                {       return $this->DAORecovery->ObtenerRolesModulo();
                }

                private function PreparaDatosAutenticacion($Form)
                {       $this->Recovery['usuario']   = str_replace(' ','',$Form['usuario']);
                        $this->Recovery['modulo']    = json_decode($Form['rol'])[0]; 
                        $this->Recovery['rolmodulo'] = json_decode($Form['rol'])[1];
                        $this->Recovery['rol']       = json_decode($Form['rol'])[2];
                        $this->Recovery['ipaccess']  = $this->Utils->retornaRemoteIPAddress();
                }
                
                function AutenticaUsuarioSistema($Form)
                {       $this->PreparaDatosAutenticacion($Form);
                        if ($this->EsUsuario($this->Recovery['usuario']))
                        {   $Respuesta = $this->VerificaUsuario();
                            if($Respuesta == true)
                            {   $Respuesta = $this->VerificaRolUsuarioSIUG();
                                if ($Respuesta[0])
                                {   $Respuesta = $this->VerificaTokenActivo($this->Recovery['usuario']);
                                    if($Respuesta[0])
                                    {    $Respuesta = $this->EnviarNuevoPasswordSIUG();
                                    }
                                    return $Respuesta;
                                }
                                return $Respuesta;
                            }
                            else
                            {    return array(false,"El usuario ingresado no existe."); }
                        }    
                        return array(false,"Debe ingresar un usuario válido.");
                } 

                private function VerificaTokenActivo($IDUSUARIO)
                {       $ExisteTokenActivo = $this->DAORecovery->BuscarTokenActivoByID($IDUSUARIO);
                        if ($ExisteTokenActivo[0])
                        {   $Datos = $ExisteTokenActivo[1];
                            $caducidad = $Datos[0]['FECADUCIDAD']->format('Y-m-d H:i:s');
                            $HaCaducado = $this->HaCaducadoToken($caducidad);
                            if($HaCaducado)
                            {   return array(false,"Ya existe una solicitud de clave activa");
                            }
                            else
                            {   $ID = $Datos[0]['ID'];
                                $this->DAORecovery->TokenEstadoCaducado($ID);
                                return array(true,"No hay solicitud activa.");
                            }
                        }
                        else
                        {       return array(true,"No hay solicitud activa.");
                        }        
                }

                function HaCaducadoToken($caducidad)
                {       if ($caducidad != "1900-01-01")
                        {   return (strtotime($this->DateControl->getNowDateTime('DATETIME')) < strtotime($caducidad));
                        }
                        return false;
                }
                
                private function VerificaUsuario()
                {   $RecoveryUsuario = $this->DAORecovery->BuscarUsuario($this->Recovery["usuario"]);
                    if ($RecoveryUsuario[0])
                    {       return true;    }
                    else{   return false;   }
                }
                
                private function VerificaRolUsuarioSIUG()
                {       $RecoveryRolSIUG = $this->DAORecovery->BuscarRolUsuarioSIUG($this->Recovery["usuario"],$this->Recovery["rol"]);
                        if ($RecoveryRolSIUG[0])
                        {   $RecoveryRolUser = $RecoveryRolSIUG[1][0]; 
                            if ($RecoveryRolUser["CEDULA"]!="0000000000")
                            {   $this->Recovery['email'] = $RecoveryRolUser["EMAIL"];
                                $this->Recovery['nombre'] = trim($RecoveryRolUser["APELLIDO"])." ".trim($RecoveryRolUser["NOMBRE"]); 
                                return array(true,"Rol correcto para recuperación de clave.");
                            }
                            else
                                return array(false,"El usuario no se encuentra activo en TH.");
                        }    
                        return $RecoveryRolSIUG;    
                }
    
                private function EnviarNuevoPasswordSIUG()
                {       if ($this->EsCorreo($this->Recovery['email']))
                        {   $this->Recovery['password'] = $this->GenerarPasswordRandom();
                            $Rspt = $this->DAOUsuario->CrearNuevoPasswordSIUG($this->Recovery["usuario"],$this->Recovery['password'],$this->Recovery['ipaccess'],$this->Recovery['email']);
                            $this->Recovery["token"] = $Rspt[2];
                            
                            if ($Rspt[0]) 
                            {   $Rspt = $this->SendMail();
                                if ($Rspt[0])
                                {   $Mesg = "Se ha enviado un mensaje a la cuenta de correo: ".$this->Recovery['email']."<br>Favor siga las instrucciones.";
                                    return array(false,$Mesg); 
                                }
                            }
                            return $Rspt;
                        }
                        return array(false,"No tiene registrada una cuenta de correo valida para completar la solicitud.");
                }
                
                private function SendMail()
                {       $Mssg = new RenderRecovery();
                        $Mail = new Mailer(); 
                        $Mail->setFromName("Unidad de Posgrado de la Universidad de Guayaquil.");
                        $Mail->setSubject("Recuperar Clave SIUG-POSGRADO.");
                        $Mail->setBodyHTML($Mssg->MensajeCorreo("MAIL_RECOVERY_CLAVE",$this->Recovery));
                        $Mail->addTo($this->Recovery['email']);
                        return $Mail->sendMail();
                }
                
                function EsUsuario($Usuario)
                {      // $Rspt = $this->Utils->validaIndentificacion(2,$Usuario);
                       // return (filter_var($Usuario, FILTER_VALIDATE_EMAIL) || $Rspt[0]);
                       return true;
                }

                function EsCorreo($Email)
                {       return (filter_var($Email, FILTER_VALIDATE_EMAIL));
                }
                
                function GenerarPasswordRandom()
                {       $Categoria = array();
                        $Politicas = $this->DAORecovery->ObtenerPoliticas("SIUG");
                      
                        foreach ($Politicas[1] as $politica)
                        {       $cantidad = $politica['SisValor'];
                                switch (trim($politica['SisVariable']))
                                {   case "ESPECIAL":    
                                        $Categoria[] = $this->Utils->retornaRandCaracteresEspeciales($cantidad);
                                        break;
                                    case "NUMERO":        
                                        $Categoria[] = $this->Utils->retornaRandNumeros($cantidad);    
                                        break;
                                    case "MAYUSCULA":           
                                        $Categoria[] = $this->Utils->retornaRandCaracteres(range('A','Z'),$cantidad);
                                        break;
                                    case "MINUSCULA":                
                                        $Categoria[] = $this->Utils->retornaRandCaracteres(range('a','z'),$cantidad);    
                                        break;
                                }        
                        }
                        return $this->RetornaPasswordRandom($Categoria);
                }
                
                private function RetornaPasswordRandom($Password)
                {       $ordchar = rand(0,3);
                        if ($ordchar==0) return $Password[0].$Password[1].$Password[2].$Password[3];
                        if ($ordchar==1) return $Password[1].$Password[2].$Password[3].$Password[0];
                        if ($ordchar==2) return $Password[2].$Password[3].$Password[0].$Password[1];
                        if ($ordchar==3) return $Password[3].$Password[0].$Password[1].$Password[2];
                }         
       }
       
       
?>