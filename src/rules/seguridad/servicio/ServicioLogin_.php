<?php   require_once('src/libs/clases/Mailer.php');
       
        require_once("src/rules/seguridad/dao/DAOLogin.php");    
        require_once("src/rules/sistema/dao/DAOUsuario.php");    
        require_once("src/rules/sistema/dao/DAOUsuarioPerfil.php");

        class ServicioLogin 
        {       private $DAOLogin;
                private $DAOUsuario;
                private $DAOUsuarioPerfil;
                private $Utils;
                private $DateControl;
                private $Login;
             
                function __construct()
                {       $this->DAOLogin = new DAOLogin();
                        $this->DAOUsuario = new DAOUsuario();
                        $this->DAOUsuarioPerfil = new DAOUsuarioPerfil();
                        $this->Utils = New Utils();                                
                        $this->DateControl = new DateControl();
                }
                
                function CargaRolesModulo()
                {       return $this->DAOLogin->ObtenerRolesModulo();
                }
                
                private function PreparaDatosActualizacion($Form)
                {       $Sesion = json_decode(Session::getValue("Sesion"));
                        
                        $this->Login['usuario']  = $Sesion->Cedula;
                        $this->Login['password'] = str_replace(' ','',$Form['password']); 
                        $this->Login['clave']    = strtoupper(str_replace(' ','',$Form['clave'])); 
                        $this->Login['npassword']= str_replace(' ','',$Form['npassword']);
                        $this->Login['cpassword']= str_replace(' ','',$Form['cpassword']);
                        $this->Login['idusrol']  = $Sesion->Idusrol;
                        $this->Login['email'] = $Sesion->Correo;
                }
                
                function CambiaUsuarioSistema($Form)
                {       $this->PreparaDatosActualizacion($Form);
                
                        if ($this->Login['npassword']==$this->Login['cpassword'])
                        {   if ($this->PasswordCumplePoliticas($this->Login["npassword"]))
                            {   $User = $this->DAOUsuario->BuscarCuentaByUsuarioPasswordSIUG($this->Login['usuario']);
                                if ($User[0])
                                {   $Data = $User[1][0];
                                    if ($Data['PASSWORD']==md5($this->Login["password"]))
                                    {   $Rspt = $this->DAOUsuario->ActualizaCuentaSIUG($this->Login);
                                        if ($Rspt[0])
                                        {   $Mail = $this->SendMailActualizacion();
                                            if ($Mail[0]) 
                                            {   $Mesg = "Se ha enviado un mensaje a la cuenta de correo: ".$this->Login['email']."<br>Favor siga las instrucciones.";
                                                return array(true,$Mesg);
                                            }
                                        }
                                        return $Rspt;
                                    }
                                    return array(false,"La contraseña actual es incorrecta.");
                                }
                                return array(false,"Error fatal, el usuario no existe.");
                            }
                            return array(false,"La nueva contraseña debe cumplir con politicas de seguridad.");
                        }
                        return array(false,"El nuevo password y la confirmación son diferentes.");
                }     

                private function SendMailActualizacion()
                {       $Mssg = new RenderLogin();
                        $Mail = new Mailer(); 
                        $Mail->setFromName("Unidad de Posgrado de la Universidad de Guayaquil.");
                        $Mail->setSubject("Cambio de Clave SIUG-POSGRADO.");
                        $Mail->setBodyHTML($Mssg->MensajeCorreo("MAIL_MODF_CUENTA",$this->Login));
                        //$Mail->addTo('casalazarv@yahoo.es'); //$this->Login['email']
                        $Mail->addTo($this->Login['email']); //$this->Login['email']
                        return $Mail->sendMail();
                }

                private function PreparaDatosAutenticacion($Form)
                {       $this->Login['usuario']   = str_replace(' ','',$Form['usuario']);
                        $this->Login['password']  = str_replace(' ','',$Form['password']); 
                        $this->Login['modulo']    = json_decode($Form['rol'])[0]; 
                        $this->Login['rolmodulo'] = json_decode($Form['rol'])[1];
                        $this->Login['rol']       = json_decode($Form['rol'])[2];
                        $this->Login['ipaccess']  = $this->Utils->retornaRemoteIPAddress();
                }
                
                function AutenticaUsuarioSistema($Form)
                {       $this->PreparaDatosAutenticacion($Form);
                        if ($this->EsUsuario($this->Login['usuario']))
                        {   $Respuesta = $this->VerificaLoginRolUsuarioSIUG();
                            if ($Respuesta[0])
                            {   $Respuesta = $this->VerificaUsuarioPasswordSIUG();   
                                if ($Respuesta[0])
                                    return $this->VerificaEstadoUsuario();
                                else 
                                {   if ($this->Login['usuario']==$this->Login['password']) //Artificio de Autogeneracion de Password
                                    {   if ($Respuesta[1]=="CMD_CREAR_CUENTA")
                                        $Respuesta = $this->CreaUsuarioPasswordSIUG();
                                    }
                                    else
                                    return array(false,"La cuenta no existe.");
                                }
                            }
                            return $Respuesta;
                        }    
                        return array(false,"Debe ingresar un usuario válido.");
                }     
                
                private function VerificaLoginRolUsuarioSIUG()
                {       $LoginRolSIUG = $this->DAOLogin->BuscarLoginRolUsuarioSIUG($this->Login["usuario"],$this->Login["rol"]);
                        if ($LoginRolSIUG[0])
                        {   $LoginRolUser = $LoginRolSIUG[1][0]; 
                            if ($LoginRolUser["CEDULA"]!="0000000000")
                            {   $this->Login['email'] = $LoginRolUser["EMAIL"];
                                $this->Login['nombre'] = trim($LoginRolUser["APELLIDO"])." ".trim($LoginRolUser["NOMBRE"]); 
                                return array(true,"Rol correcto para login.");
                            }
                            else
                                return array(false,"El usuario no se encuentra activo en TH.");
                        }    
                        return $LoginRolSIUG;    
                }
                
                private function VerificaUsuarioPasswordSIUG()
                {       $User = $this->DAOUsuario->BuscarCuentaByUsuarioPasswordSIUG($this->Login["usuario"]);
                        if ($User[0])
                        {   $Data = $User[1][0];
                            if ($Data['PASSWORD']==md5($this->Login["password"]))
                            {   if ($Data['IDESTADO']==1)
                                {   if ($Data['CONFIRMACION']=='S')
                                    {   $HaCaducado = $this->HaCaducadoAcceso($Data['FECADUCIDAD']->format("Y-m-d")); 
                                        return $this->DAOUsuario->ConfirmaCuentaSIUG($this->Login["usuario"],$HaCaducado);      
                                    }   
                                    return array(true,"");
                                }
                                return array(false,"Su cuenta se encuentra inactiva.");
                            }
                            return array(false,"La cuenta no es correcta.");
                        }
                        else
                        return $User; 
                }
                
                private function CreaUsuarioPasswordSIUG()
                {       if ($this->EsCorreo($this->Login['email']))
                        {   $this->Login['password'] = $this->GenerarPasswordRandom();
                            $Rspt = $this->DAOUsuario->CreaCuentaByUsuarioPasswordSIUG($this->Login["usuario"],$this->Login['password']);
                            if ($Rspt[0]) 
                            {   $Rspt = $this->SendMailConfirmacion();
                                if ($Rspt[0])
                                {   $DatosUsuario = $this->DAOUsuario->CrearUsuarioRolByLogin($this->Login);
                                    $this->Login["id"] = $DatosUsuario[1];
                                    $Perfiles = $this->DAOUsuarioPerfil->CreaUsuarioPerfilDefault($this->Login);
                                    if ($Perfiles[0])
                                    {   $this->GuardaLoginLog("SOLICITUD DE CREACION DE CUENTA");
                                    }    
                                    $Mesg = "Se ha enviado un mensaje a la cuenta de correo: ".$this->Login['email']."<br>Favor siga las instrucciones.";
                                    return array(false,$Mesg); //Es falso porque no deseo que acceda por el momento de ahi. primero debe validar lo del correo
                                }
                            }
                            return $Rspt;
                        }
                        return array(false,"No tiene registrada una cuenta de correo valida para completar la solicitud.");
                }
                
                private function SendMailConfirmacion()
                {       $Mssg = new RenderLogin();
                        $Mail = new Mailer(); 
                        $Mail->setFromName("Unidad de Posgrado de la Universidad de Guayaquil.");
                        $Mail->setSubject("Solicitud de Cuenta SIUG-POSGRADO.");
                        $Mail->setBodyHTML($Mssg->MensajeCorreo("MAIL_CREA_CUENTA",$this->Login));
                        //$Mail->addTo('casalazarv@yahoo.es'); //$this->Login['email']
                        $Mail->addTo($this->Login['email']); //$this->Login['email']
                        return $Mail->sendMail();
                }
                
                private function VerificaEstadoUsuario()
                {       $UsuarioByLogin = $this->DAOUsuario->ObtenerUsuarioRolByLogin($this->Login);
                        if ($UsuarioByLogin[0])
                        {   $DatosUsuario = $UsuarioByLogin[1][0]; 
                            if ($DatosUsuario["IDESTADO"]==1)
                            {   $this->Login["id"] = $DatosUsuario["ID"];
                                return $this->ControlUsuarioEstadoActivo($DatosUsuario);
                            }
                            else
                            return array(false,"La cuenta para el rol seleccionado está inactiva.");
                        }
                        else
                        {   if ($UsuarioByLogin[1]=="CMD_CREAR_USUARIO_LOGIN")    
                            {   $DatosUsuario = $this->DAOUsuario->CrearUsuarioRolByLogin($this->Login);
                                $this->Login["id"] = $DatosUsuario[1];
                                $Perfiles = $this->DAOUsuarioPerfil->CreaUsuarioPerfilDefault($this->Login);
                                if ($Perfiles[0])
                                {   $this->GuardaLoginLog("ACCESO CON ACTUALIZACION DE PERFIL");
                                    $this->GuardaSessionUsuario($Perfiles[1]);
                                    return array(true,"CMD_MAIN");
                                }
                                return $Perfiles;
                            }
                        }    
                        return $UsuarioByLogin;
                }
                
                private function ControlUsuarioEstadoActivo($UserData)
                {       $Caducidad = $UserData['FECADUCIDAD']->format("Y-m-d");
                        if (!$this->HaCaducadoAcceso($Caducidad))
                        {   if ($this->PasswordCumplePoliticas($this->Login["password"]))
                            {   if ($this->CumpleHorarioAcceso($UserData['HORAINI'],$UserData['HORAFIN']))
                                {   $ExistenPerfiles = $this->ObtenerPerfilesVigentesUsuario($UserData['ID']);
                                    if (!$ExistenPerfiles[0])
                                    {   $DefaultPerfiles = $this->DAOUsuarioPerfil->CreaUsuarioPerfilDefault($this->Login);
                                        if ($DefaultPerfiles[0])
                                        $ExistenPerfiles = $DefaultPerfiles;    
                                    }
                                    else
                                    $this->DAOUsuarioPerfil->CreaUsuarioPerfilDefault($this->Login); //Verifica si existen nuevos perfiles creados por defecto y asignados
                                    
                                    if ($ExistenPerfiles[0])
                                    {   $this->GuardaLoginLog("ACCESO AL SISTEMA");
                                        $this->GuardaSessionUsuario($ExistenPerfiles[1]);
                                        return array(true,"CMD_MAIN");
                                    }
                                    return $ExistenPerfiles;                                  
                                }
                                return array(false,"Se encuentra fuera del horario permitido.");
                            }
                            else
                            {   return array(false,"Su contraseña debe cumplir con politicas de seguridad.");
                            }
                        }
                        return array(false,"Su rol de acceso al sistema ha caducado.");
                }        
                
                function ObtenerRolByID($Id)
                {       $Respuesta = $this->DAOLogin->BuscarRolByID($Id);
                        if ($Respuesta[0])
                        {   if (count($Respuesta[1])==0)
                                return array(false,"No existe definido el Rol.");
                            else
                                return array(true,$Respuesta[1][0]);
                        }
                        return $Respuesta;
                }
                
                private function GuardaSessionUsuario($Perfiles)
                {       $sesion = array("Idusrol"=>$this->Login['id'],
                                        "Cedula"=>$this->Login['usuario'],
                                        "Nombre"=>utf8_decode($this->Utils->ConvertTildesToUpper($this->Login['nombre'])),
                                        "Idmodulo"=>$this->Login['modulo'],
                                        "Idrol"=>$this->Login['rol'],
                                        "Idmodrol"=>$this->Login['rolmodulo'],
                                        "Correo" =>$this->Login['email']);
                        Session::setValue("Sesion", json_encode($sesion));
                        Session::setValue("Perfil", $Perfiles);
                        return true;
                }

                function CumpleHorarioAcceso($horadesde,$horahasta)
                {       $horadesde = trim($horadesde);
                        $horahasta = trim($horahasta);  
                        $fechadhoy = strtotime($this->DateControl->getNowDateTime('DATETIME')); 
                        if ($horadesde != "" && $horahasta != "")
                        {   $hoy = $this->DateControl->getNowDateTime('DATE'); 
                            $compare_desde = strtotime($horadesde);
                            $compare_hasta = strtotime($horahasta);
                            $fecha_desde = ($hoy.' '.$horadesde);
                            if ($compare_hasta < $compare_desde)
                            {   $hoy = date($this->DateControl->getNowDateTime('DATE'),strtotime('+1 day'));
                            }
                            $fecha_hasta = ($hoy.' '.$horahasta);
                            if ((strtotime($fecha_desde) > $fechadhoy) || (strtotime($fecha_hasta) < $fechadhoy))
                               { return false;
                               }
                        }
                        return true;
                }        
                
                function HaCaducadoAcceso($caducidad)
                {       if ($caducidad != "1900-01-01")
                        {   return (strtotime($this->DateControl->getNowDateTime('DATE')) >= strtotime($caducidad));
                        }
                        return false;
                }
                
                function PasswordCumplePoliticas($password)
                {       $Carateres = $this->DecodificaPassword($password);
                        $Politicas = $this->DAOLogin->ObtenerPoliticas("SIUG");
                        foreach ($Politicas[1] as $politica)
                        {       $Key = trim($politica['SisVariable']);
                                if ($Carateres[$Key] < $politica['SisValor'])
                                return false;
                        }
                        return true;
                }
                
                private function DecodificaPassword($password)
                {       $tamanio = strlen(trim($password));
                        $categoria = array('NUMERO' => 0,'ESPECIAL' => 0,'MAYUSCULA' => 0,'MINUSCULA' => 0);
                        if ($tamanio > 0)
                        {   for ($i = 0; $i < $tamanio; $i++)
                                {   $letra = substr(trim($password),$i,1);
                                    if(is_numeric($letra))   
                                    {  $categoria['NUMERO'] = $categoria['NUMERO']+1;
                                    }
                                    if(!ctype_alnum($letra)) 
                                    {  $categoria['ESPECIAL'] = $categoria['ESPECIAL']+1;
                                    }
                                    if(ctype_upper($letra))  
                                    {  $categoria['MAYUSCULA'] = $categoria['MAYUSCULA']+1;
                                    }
                                    if(ctype_lower($letra))  
                                    {  $categoria['MINUSCULA'] = $categoria['MINUSCULA']+1;
                                    }
                                }
                        }  
                        return $categoria;    
                }                    

                function EsUsuario($Usuario)
                {      // $Rspt = $this->Utils->validaIndentificacion(2,$Usuario);
                       // return (filter_var($Usuario, FILTER_VALIDATE_EMAIL) || $Rspt[0]);
                       return true;
                }

                function EsCorreo($Email)
                {       return (filter_var($Email, FILTER_VALIDATE_EMAIL));
                }
                
                function GenerarPasswordRandom()
                {       $Categoria = array();
                        $Politicas = $this->DAOLogin->ObtenerPoliticas("SIUG");
                      
                        foreach ($Politicas[1] as $politica)
                        {       $cantidad = $politica['SisValor'];
                                switch (trim($politica['SisVariable']))
                                {   case "ESPECIAL":    
                                        $Categoria[] = $this->Utils->retornaRandCaracteresEspeciales($cantidad);
                                        break;
                                    case "NUMERO":        
                                        $Categoria[] = $this->Utils->retornaRandNumeros($cantidad);    
                                        break;
                                    case "MAYUSCULA":           
                                        $Categoria[] = $this->Utils->retornaRandCaracteres(range('A','Z'),$cantidad);
                                        break;
                                    case "MINUSCULA":                
                                        $Categoria[] = $this->Utils->retornaRandCaracteres(range('a','z'),$cantidad);    
                                        break;
                                }        
                        }
                        return $this->RetornaPasswordRandom($Categoria);
                }
                
                private function RetornaPasswordRandom($Password)
                {       $ordchar = rand(0,3);
                        if ($ordchar==0) return $Password[0].$Password[1].$Password[2].$Password[3];
                        if ($ordchar==1) return $Password[1].$Password[2].$Password[3].$Password[0];
                        if ($ordchar==2) return $Password[2].$Password[3].$Password[0].$Password[1];
                        if ($ordchar==3) return $Password[3].$Password[0].$Password[1].$Password[2];
                }       
                
                function GuardaLoginLog($Msg="")
                {       return $this->DAOLogin->InsertaLoginLog($this->Login,$Msg);
                }
                
                function ObtenerPerfilesVigentesUsuario($id)
                {       $UsuarioPerfil = $this->DAOUsuarioPerfil->ObtenerUsuarioPerfilByUserModuloID($id,$this->Login['modulo']);
                        if ($UsuarioPerfil[0])
                        {   $Perfiles = array();
                            $Contador = 0;
                            $actual = $this->DateControl->getNowDateTime('DATE');
                            foreach($UsuarioPerfil[1] as $Perfil)
                            {       $desde = $Perfil['FEDESDE']->format("Y-m-d");
                                    $tempo = $Perfil['FEHASTA']->format("Y-m-d");;
                                    $hasta = ($tempo == "1900-01-01" ? $actual : $tempo);
                                    if (strtotime($actual) >= strtotime($desde) && strtotime($actual)<= strtotime($hasta))
                                    {   $Perfiles[] = $Perfil["ID"];
                                        $Contador++;
                                    }
                            }
                            $Evaluar = ($Contador>0);
                            return array($Evaluar,($Evaluar ? $Perfiles: "No tiene un perfile vigente."));
                        }    
                        return $UsuarioPerfil;    
                }
                
                function ObtenerPoliticasContrasena()
                {       $Txt = "";
                        $Politicas = $this->DAOLogin->ObtenerPoliticas("SIUG");
                        foreach ($Politicas[1] as $politica)
                        {       $Key = trim($politica['SisVariable']);
                                $Vlu = $politica['SisValor'];
                                $Txt = $Txt.$Vlu." ". ucwords(strtolower($Key))."<br>"; 
                        }
                        return $Txt;
                }
       }
       
       
?>