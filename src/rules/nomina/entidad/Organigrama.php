<?php
    require_once("src/base/EntidadGlobal.php");

    /**
     *
     * @Entity
     * @Table(name="organigrama", schema="nomina")
     */
    class Organigrama extends EntidadGlobal
    {
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
    * @Column(name="fedesde", type="datetime")
    */
    private $fedesde;

    /**
    * @Column(name="fehasta", type="datetime")
    */
    private $fehasta;

    /**
      * @Column(name="version", type="string", length=10)
    */
    private $version;

    /**
      * @Column(name="comentario", type="string", length=512)
    */
    private $comentario;
    
    public function getId()
    {
        return $this->id;
    }

    public function setFedesde($fedesde)
    {
        $this->fedesde = $fedesde;
    }

    public function getFedesde()
    {
        return $this->fedesde;
    }

    public function setFehasta($fehasta)
    {
        $this->fehasta = $fehasta;
    }

    public function getFehasta()
    {
        return $this->fehasta;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    }

    public function getComentario()
    {
        return $this->comentario;
    }
    
}

