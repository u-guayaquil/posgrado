<?php
    
    require_once ('src/rules/nomina/entidad/Departamento.php');
    require_once ('src/rules/nomina/entidad/Organigrama.php');
    /**
     *
     * @Entity
     * @Table(name="org_departamento", schema="nomina")
     */
    class OrgDepartamento
    {
    
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;
    /**
      * @Column(name="id_departamento", type="integer", length=10)
    */
    private $idDepartamento;
    
    /**
      * @Column(name="id_organigrama", type="integer", length=10)
    */
    private $idOrganigrama;

    /**
     * @Column(name="id_departamento_padre", type="integer")
     */
    private $idDepartamentoPadre = '';
    
    
    /**
     * @ManyToOne(targetEntity="Organigrama")
     * @JoinColumn(name="id_organigrama", referencedColumnName="id")
     */
    private $Organigramas;
    
    /**
     * @ManyToOne(targetEntity="Departamento")
     * @JoinColumn(name="id_departamento", referencedColumnName="id")
     */
    private $Departamentos; 
    
    

    public function getId()
    {      return $this->id;
    }

    public function setIdDepartamentoPadre($idDepartamentoPadre)
    {      $this->idDepartamentoPadre = $idDepartamentoPadre;
    }

    public function getIdDepartamentoPadre()
    {      return $this->idDepartamentoPadre;
    }

    public function setDepartamentos(Departamento $departamentos)
    {      $this->Departamentos = $departamentos;
    }
    
    public function getDepartamentos()
    {      return $this->Departamentos;
    }
    
    public function setOrganigramas(Organigrama $organigramas)
    {      $this->Organigramas = $organigramas;
    }
    
    public function getOrganigramas()
    {      return $this->Organigramas;
    }
    
    public function setIdDepartamento($idDepartamento)
    {      $this->idDepartamento = $idDepartamento;
    }
    
    public function getIdDepartamento()
    {      return $this->idDepartamento;
    }

    public function setIdOrganiograma($idOrganiograma)
    {      $this->idOrganiograma = $idOrganiograma;
    }
    
    public function getIdOrganiograma()
    {      return $this->idOrganiograma;
    }
    }

