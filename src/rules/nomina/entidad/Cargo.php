<?php
require_once("src/base/EntidadGlobal.php");

    /**
     *
     * @Entity
     * @Table(name="cargo", schema="nomina")
     */
class Cargo extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;
    /**
      * @Column(name="descripcion", type="string", length=50)
    */
    private $descripcion = '';

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

}

