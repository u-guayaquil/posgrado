<?php
    
    require_once ('src/rules/nomina/entidad/Cargo.php');
    require_once ('src/rules/nomina/entidad/OrgDepartamento.php');
    /**
     *
     * @Entity
     * @Table(name="org_departamento_cargo", schema="nomina")
     */
    class OrgDepCargo
    {
    
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;    
    
    /**
     * @ManyToOne(targetEntity="OrgDepartamento")
     * @JoinColumn(name="id_orgdept", referencedColumnName="id")
     */
    private $OrgDepartamento;
    
    /**
     * @ManyToOne(targetEntity="Cargo")
     * @JoinColumn(name="id_cargo", referencedColumnName="id")
     */
    private $Cargos; 
    
    /**
     * @ManyToOne(targetEntity="Cargo")
     * @JoinColumn(name="id_cargo_padre", referencedColumnName="id")
     */
    private $CargosPadres;

    public function getId()
    {      return $this->id;
    }

    function getCargos() {
        return $this->Cargos;
    }

    function getCargosPadres() {
        return $this->CargosPadres;
    }

    function setCargos(Cargo $Cargos) {
        $this->Cargos = $Cargos;
    }

    function setCargosPadres($CargosPadres) {
        $this->CargosPadres = $CargosPadres;
    }
    
    function getOrgDepartamento() {
        return $this->OrgDepartamento;
    }

    function setOrgDepartamento(OrgDepartamento $OrgDepartamento) {
        $this->OrgDepartamento = $OrgDepartamento;
    }


}

