<?php
            require_once("src/rules/nomina/dao/DAORubrosRol.php");
            
            class ServicioRubrosRol
            {     private $DAORubrosRol;
     
                  function __construct()
                  {        $this->DAORubrosRol = new DAORubrosRol();
                  }
                  function GuardaDBRubrosRol($Form)
                  {       if (empty($Form->id)){
                            return $this->DAORubrosRol->InsertaRubrosRol($Form);
                          }else{
                            return $this->DAORubrosRol->ActualizaRubrosRol($Form);
                          }
                  }
                  
                  function DesactivaRubrosRol($Id)
                  {       return $this->DAORubrosRol->DesactivaRubrosRol($Id);
                  }
           
                  function BuscarRubrosRol($prepareDQL)
                  {        return $this->DAORubrosRol->BuscarRubrosRol($prepareDQL);
                  }
            }
?>

