<?php
            require_once("src/rules/nomina/dao/DAORolPagos.php");
            
            class ServicioRolPagos
            {       private $DAORolPagos;
     
                    function __construct()
                    {        $this->DAORolPagos = new DAORolPagos();
                    }

                    function BuscarRolPagos($prepareDQL)
                    {        return $this->DAORolPagos->ObtenerRolPagos($prepareDQL);
                    }

                    function BuscarRolPagosDetalle($id)
                    {        return $this->DAORolPagos->ObtenerRolPagosDetalle(intval($id));
                    }
                    
                    function BuscarRolPagosDetalleResumen($id)
                    {        return $this->DAORolPagos->ObtenerRolPagosDetalleResumen(intval($id));
                    }
                    
                    function ProcesoCalculaRolPagos($idRol,$Datos,$Methods)
                    {       $this->DAORolPagos->EliminaRolPreProceso($idRol);
                            $Resumen = array();
                            foreach($Datos as $Dato) 
                            {   $ingreso = 0;
                                $egreso = 0;
                                $provision = 0;
                                if ($Methods->InicializaMethodsRol($Dato['idempleado']))
                                {   foreach($Dato['formulas'] as $Formula)
                                    {       $monto = $Methods->EjecutaMethodsRol($Formula['txformula'], $Formula['decoders']);
                                            if ($Formula['movmnto']==1) $ingreso = $ingreso + $monto;
                                            if ($Formula['movmnto']==2) $egreso = $egreso + $monto; 
                                            if ($Formula['movmnto']==3) $provision = $provision + $monto;  
                                            
                                            $detalle = array('idrolpago'=>$idRol,'idempleado'=>$Dato['idempleado'],'idrubro'=>$Formula['idrubro'],'idmovmnto'=>$Formula['movmnto'],'valor'=>$monto,'idsucursal'=>$Dato['idsucursal'],'idplantilla'=>$Dato['idplantilla']);
                                            $this->DAORolPagos->InsertaRolPreProceso($detalle);    
                                    }
                                    $Resumen[] = array('idempleado'=>$Dato['idempleado'],'idplantilla'=>$Dato['idplantilla'],'empleado'=>$Dato['empleado'],'provision'=>$provision,'ingreso'=>$ingreso,'egreso'=>$egreso);    
                                }    
                            }        
                            return $Resumen;
                    }
                    
                    function GuardaDB($Form)
                    {       if (empty($Form->idrol)){
                                return $this->DAORolPagos->Inserta($Form); 
                            }else{
                                return $this->DAORolPagos->Actualiza($Form); 
                            }
                    }
                  
                    function DesactivaRolPagos($Id)
                    {       return $this->DAORolPagos->DesactivaRolPagos($Id);
                    }
                    
                    
            }        
?>                    