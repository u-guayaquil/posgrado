<?php
            
            require_once("src/rules/nomina/dao/DAOFormula.php");
            
class ServicioFormula
{       private $DAOFormula;
        private $Utils;

        function __construct()
        {       $this->DAOFormula = new DAOFormula();
                $this->Utils = new Utils();
        }
           
        function AnalizaFormula($Cadena)
        {       $Pharse = str_replace(" ","", $Cadena);
                if (strlen($Pharse)>0)
                {   if (is_numeric($Pharse))
                    {   $fnData[] = array("Numerico",$this->Utils->numberToOperation($Pharse)); 
                        return array(true,$Pharse,$fnData);    
                    }
                    else
                    {   list($fnStatu,$fnData) = $this->Utils->DecoderFormula($Pharse);
                        if ($fnStatu)
                        {   foreach($fnData as $fnPart)
                            {   if (method_exists('ServicioRolMetodos',$fnPart[0]))
                                {   if (!$this->ExisteParametro($fnPart[1]))
                                    return array(false,$Pharse,"No existe el parámetro ".$fnPart[1]." para la función ".$fnPart[0]."()");
                                }
                                else
                                return array(false,$Pharse,"No existe la función ".$fnPart[0]."()");
                            }
                            return array(true,$Pharse,$fnData);
                        }
                        else
                        return array(false,$Pharse,$fnData);
                    }
                }    
                else
                return array(false,$Pharse,"Fórmula no puede ser vacía.");    
        }
                    
        private function ExisteParametro($key)
        {       if (is_numeric($key))
                    return true;
                else
                    return $this->DAOFormula->ExisteVariableRubro($key);
        }
                    
        function ArrayConstanteGeneral()
        {       $Constante = array();
                $arreglo = $this->DAOFormula->ObtenerVariablesConstante('C');
                foreach($arreglo as $elemento)
                {       $Constante[trim($elemento['descripcion'])] = $elemento['valor'];
                }
                return $Constante;
        }
                    
        function ArrayVariablesGeneral()
        {       $Variables = array();
                $arreglo = $this->DAOFormula->ObtenerVariablesConstante('V');
                foreach($arreglo as $elemento)
                {       $Variables[] = trim($elemento['descripcion']);
                } 
                return $Variables;
        }
                    
        function ArrayVariablesEmpleados($idEmp)
        {       $Variables = array();
                $arreglo = $this->DAOFormula->ObtenerVariablesEmpleado($idEmp);
                foreach($arreglo as $elemento)
                {       $key = trim($elemento['txvariable']);
                        $Variables[$key] = $elemento['valor'];
                }
                return $Variables;
        }
                    
        function ArrayRolMethods($type="F")
        {       $type = strtoupper($type);
        
                $Methods = array();
                $metodos_clase = get_class_methods('ServicioRolMetodos');
                foreach ($metodos_clase as $nombre_metodo) 
                {       $iniciales = strtoupper(substr($nombre_metodo,0,3));
                        if ($type=="C" && $iniciales=="CNS") $Methods[] = $nombre_metodo;
                        if ($type=="F" && $iniciales=="VAL") $Methods[] = $nombre_metodo;
                        if ($type=="P" && $iniciales=="PRO") $Methods[] = $nombre_metodo;                   
                }
                return $Methods;
                
        }
                
                    
}


?>

