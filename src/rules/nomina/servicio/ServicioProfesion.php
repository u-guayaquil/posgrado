<?php
            require_once("src/rules/nomina/dao/DAOProfesion.php");
            
            class ServicioProfesion
            {     private $DAOProfesion;
     
                  function __construct()
                  {        $this->DAOProfesion = new DAOProfesion();
                  }
                  function GuardaDBProfesion($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOProfesion->InsertaProfesion($Form);
                          }else{
                            return $this->DAOProfesion->ActualizaProfesion($Form);
                          }
                  }
                  
                  function DesactivaProfesion($Id)
                  {       return $this->DAOProfesion->DesactivaProfesion($Id);
                  }
           
                  function BuscarProfesionByID($id)
                  {        return $this->DAOProfesion->BuscarProfesionByDescripcion($id);
                  }

                  function BuscarProfesionByDescripcion($prepareDQL)
                  {     return $this->DAOProfesion->BuscarProfesionByDescripcion($prepareDQL);                                                
                  }
                  
                  function BuscarProfesionByArrayID($idArray)
                  {       return $this->DAOProfesion->ObtenerProfesionCombo($idArray);
                  }
                  
                  function BuscarProfesionsIndependientes($ArrayOrg)
                  {       return $this->DAOProfesion->ObtenerProfesionssinOrgDepartamento($ArrayOrg);
                  }
            }
?>

