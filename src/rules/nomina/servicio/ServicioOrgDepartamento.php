<?php
            require_once("src/rules/nomina/dao/DAOOrgDepartamento.php");
            require_once("src/rules/nomina/entidad/OrgDepartamento.php");
            require_once("src/rules/nomina/servicio/ServicioDepartamento.php");
            require_once("src/rules/nomina/servicio/ServicioOrganigrama.php");
            
            class ServicioOrgDepartamento
            {     private $DAOOrgDepartamento;
                  private $ServicioOrganigrama;
                  private $ServicioDepartamento;
                  private $padre = array();
     
                  function __construct()
                  {        $this->DAOOrgDepartamento = new DAOOrgDepartamento();
                           $this->ServicioOrganigrama = new ServicioOrganigrama();
                           $this->ServicioDepartamento = new ServicioDepartamento();
                  }
           
                  function CreaOrgDepartamento($Form)
                  {       $DepartamentoPadre=0;
                          $Organigrama=0;
                          $Id=0;
                          
                            foreach ($Form as $datos=>$informacion){ 
                                switch (substr($datos, 0,3)){
                                    case 'chk':  
                                            $OrgDepartamento = new OrgDepartamento();                                            
                                            $OrgDepartamento->setIdDepartamentoPadre($DepartamentoPadre);
                                            $OrgDepartamento->setOrganigramas($Organigrama);
                                            $Departamento = $this->ServicioDepartamento->BuscarDepartamentoByID($informacion);
                                            $OrgDepartamento->setDepartamentos($Departamento);
                                            $this->DAOOrgDepartamento->InsertaOrgDepartamento($OrgDepartamento); 
                                            $Id = 0;
                                        break;
                                    case 'Dep': 
                                            $DepartamentoPadre = $informacion; 
                                            $this->EliminaRelaciones($DepartamentoPadre);
                                        break;
                                    case 'Org':                                        
                                            $Organigrama = $this->ServicioOrganigrama->BuscarOrganigramaByID($informacion);                                            
                                        break;
                                    case 'Id': 
                                            $Id = $informacion;
                                        break;
                                }
                            } 
                            return array(true, 'Datos Guardados exitosamente');
                  }
                  
                  function EditaOrgDepartamento($Form)
                  {        return $this->DAOOrgDepartamento->CreaOrgDepartamento($Form);
                  }
                  
                  function EliminaRelaciones($DepartamentoPadre)
                  {        $ObjetoOrgDepartamento = $this->DAOOrgDepartamento->ObtenerObjetoporDepartamento($DepartamentoPadre);                      
                           foreach ($ObjetoOrgDepartamento as $datos){
                               $Objeto = $this->DAOOrgDepartamento->ObtenerOrgDepartamentoByID($datos['id']);
                               $this->DAOOrgDepartamento->EliminaOrgDepartamento($Objeto);
                           }
                           return true;
                  }
                  
           
                  function BuscarOrgDepartamentoByID($id)
                  {        return $this->DAOOrgDepartamento->ObtenerOrgDepartamentoByID($id);
                  }

                  function BuscarOrgDepartamentoByDescripcion($prepareDQL)
                  {        return $this->DAOOrgDepartamento->ObtenerOrgDepartamentoGeneral($prepareDQL);                                                    
                  } 
                  
                  function BuscarOrgDepartamentoByArrayID($idArray)
                  {        return $this->DAOOrgDepartamento->ObtenerOrgDepartamentoCombo($idArray);
                  }
                  
                  function BuscarDepartamentosPadres()
                  {        return $this->DAOOrgDepartamento->ObtenerDepartamentosPadres();
                  }
                  
                  function BuscarOrgDepartamentosGeneral($id=0,$Organigrama=0)
                  {         $Datos = $this->DAOOrgDepartamento->TraerDatosDepartamentoGeneral($id,$Organigrama);                    
                               if(count($Datos)>0){
                                  foreach ($Datos as $orgdepart){
                                      array_push($this->padre,$orgdepart);
                                      $this->BuscarOrgDepartamentosGeneral($orgdepart['idDepartamento'],$orgdepart['idOrganigrama']);                                                                                
                                  }  
                                } 
                            return $this->padre;
                  }
                  
                  function BuscarDepartamentosRelacionados($id,$Organigrama)
                  {        return $this->DAOOrgDepartamento->TraerDatosDepartamentoGeneral($id,$Organigrama);
                  }

            }
?>

