<?php
            require_once("src/rules/nomina/dao/DAODepartamento.php");
            
            
            class ServicioDepartamento
            {     private $DAODepartamento;
     
                  function __construct()
                  {        $this->DAODepartamento = new DAODepartamento();
                  }
                  
                  function GuardaDBDepartamento($Form)
                  {       if (empty($Form->id)){
                            return $this->DAODepartamento->InsertaDepartamento($Form);
                          }else{
                            return $this->DAODepartamento->ActualizaDepartamento($Form);
                          }
                  }
                  
                  function DesactivaDepartamento($Id)
                  {        return $this->DAODepartamento->DesactivaDepartamento($Id);
                  }
           
                  function BuscarDepartamentoByID($id)
                  {        return $this->DAODepartamento->BuscarDepartamentoByDescripcion($id);
                  }

                  function BuscarDepartamentoByDescripcion($prepareDQL)
                  {        return $this->DAODepartamento->BuscarDepartamentoByDescripcion($prepareDQL);                        
                  }
                  
                  function BuscarDepartamentoByArrayID($idArray)
                  {       return $this->DAODepartamento->ObtenerDepartamentoCombo($idArray);
                  }
                  
                  function BuscarDepartamentosIndependientes($ArrayOrg)
                  {       return $this->DAODepartamento->ObtenerDepartamentossinOrganigramas($ArrayOrg);
                  }

            }
?>

