<?php
            require_once("src/rules/nomina/dao/DAOCargo.php");
            require_once("src/rules/nomina/entidad/Cargo.php");
            
            class ServicioCargo
            {     private $DAOCargo;
     
                  function __construct()
                  {        $this->DAOCargo = new DAOCargo();
                  }
                  function GuardaDBCargo($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOCargo->InsertaCargo($Form);
                          }else{
                            return $this->DAOCargo->ActualizaCargo($Form);
                          }
                  }
                  
                  function DesactivaCargo($Id)
                  {       return $this->DAOCargo->DesactivaCargo($Id);
                  }
           
                  function BuscarCargoByID($id)
                  {        return $this->DAOCargo->BuscarCargoByDescripcion($id);
                  }

                  function BuscarCargoByDescripcion($prepareDQL)
                  {     return $this->DAOCargo->BuscarCargoByDescripcion($prepareDQL);                                                
                  }
                  
                  function BuscarCargoByArrayID($idArray)
                  {       return $this->DAOCargo->ObtenerCargoCombo($idArray);
                  }
                  
                  function BuscarCargosIndependientes($ArrayOrg)
                  {       return $this->DAOCargo->ObtenerCargossinOrgDepartamento($ArrayOrg);
                  }
            }
?>

