<?php
            require_once("src/rules/nomina/dao/DAOEmpleadoRubro.php");
            
            class ServicioEmpleadoRubro
            {     private $DAOEmpleadoRubro;
     
                function __construct()
                {        $this->DAOEmpleadoRubro = new DAOEmpleadoRubro();
                }
                  
                function Guarda($Form)
                {        return $this->DAOEmpleadoRubro->Inserta($Form);
                }

                function Edita($Form)
                {        return $this->DAOEmpleadoRubro->Actualiza($Form);
                }
                    
                function DesactivaEmpleadoRubro($Id)
                {       return $this->DAOEmpleadoRubro->DesactivaEmpleadoRubro($Id);
                }
           
                function BuscarEmpleadoRubro($prepareDQL)
                {        return $this->DAOEmpleadoRubro->BuscarEmpleadoRubro($prepareDQL);
                }
            }
?>

