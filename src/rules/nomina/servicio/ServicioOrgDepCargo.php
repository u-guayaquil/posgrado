<?php
            require_once("src/rules/nomina/dao/DAOOrgDepCargo.php");
            require_once("src/rules/nomina/entidad/OrgDepCargo.php");
            require_once("src/rules/nomina/servicio/ServicioOrgDepartamento.php");
            require_once("src/rules/nomina/servicio/ServicioCargo.php");
            
            class ServicioOrgDepCargo
            {     private $DAOOrgDepCargo;
                  private $ServicioOrgDepartamento;
                  private $ServicioCargo;
                  private $padre = array();
     
                  function __construct()
                  {        $this->DAOOrgDepCargo = new DAOOrgDepCargo();
                           $this->ServicioOrgDepartamento = new ServicioOrgDepartamento();
                           $this->ServicioCargo = new ServicioCargo();
                  }
           
                  function CreaOrgDepCargo($Form)
                  {       $CargoPadre=0;
                          $OrgDepartamento=0;
                          $Id=0;                          
                            foreach ($Form as $datos=>$informacion){ 
                                switch (substr($datos, 0,3)){
                                    case 'chk':  
                                            $Cargo = $this->ServicioCargo->BuscarCargoByID($informacion);
                                            $CargoPadre = $this->ServicioCargo->BuscarCargoByID($CargoPadre);
                                            $OrgDepCargo = new OrgDepCargo();
                                            $OrgDepCargo->setCargosPadres($CargoPadre);
                                            $OrgDepCargo->setOrgDepartamento($OrgDepartamento);                                            
                                            $OrgDepCargo->setCargos($Cargo);
                                            $this->DAOOrgDepCargo->InsertaOrgDepCargo($OrgDepCargo); 
                                            $Id = 0;
                                        break;
                                    case 'Car': 
                                            $CargoPadre = $informacion; 
                                            $this->EliminaRelaciones($CargoPadre);                                          
                                        break;
                                    case 'Dep':                                        
                                            $OrgDepartamento = $this->ServicioOrgDepartamento->BuscarOrgDepartamentoByID($informacion); 
                                        break;
                                    case 'Id': 
                                            $Id = $informacion;
                                        break;
                                }
                            } 
                            return array(true, 'Datos Guardados exitosamente');
                  }
                  
                  function EditaOrgDepCargo($Form)
                  {        return $this->DAOOrgDepCargo->CreaOrgDepartamento($Form);
                  }
                  
                  function EliminaRelaciones($DepartamentoPadre)
                  {        $ObjetoOrgDepartamento = $this->DAOOrgDepCargo->ObtenerObjetoporDepartamento($DepartamentoPadre);                      
                           foreach ($ObjetoOrgDepartamento as $datos){
                               $Objeto = $this->DAOOrgDepCargo->ObtenerOrgDepartamentoByID($datos['id']);
                               $this->DAOOrgDepCargo->EliminaOrgDepartamento($Objeto);
                           }
                           return true;
                  }
                  
           
                  function BuscarOrgDepCargoByID($id)
                  {        return $this->DAOOrgDepCargo->ObtenerOrgDepartamentoByID($id);
                  }

                  function BuscarOrgDepCargoByDescripcion($prepareDQL)
                  {        return $this->DAOOrgDepCargo->ObtenerOrgDepartamentoGeneral($prepareDQL);                                                    
                  } 
                  
                  function BuscarOrgDepCargoByArrayID($idArray)
                  {        return $this->DAOOrgDepCargo->ObtenerOrgDepartamentoCombo($idArray);
                  }
                  
                  function BuscarCargosPadres()
                  {        return $this->DAOOrgDepCargo->ObtenerCargosPadres();
                  }
                  
                  function BuscarOrgDepCargoGeneral($id=0,$Organigrama=0)
                  {         $Datos = $this->DAOOrgDepCargo->TraerDatosDepCargoGeneral($id,$Organigrama);                    
                               if(count($Datos)>0){
                                  foreach ($Datos as $orgdepart){
                                      array_push($this->padre,$orgdepart);
                                      $this->BuscarOrgDepCargoGeneral($orgdepart['idDepartamento'],$orgdepart['OrgDepartamento']);                                                                                
                                  }  
                                } 
                            return $this->padre;
                  }
                  
                  function BuscarDepCargoRelacionados($id,$Organigrama)
                  {        return $this->DAOOrgDepCargo->TraerDatosDepCargoGeneral($id,$Organigrama);
                  }

            }
?>

