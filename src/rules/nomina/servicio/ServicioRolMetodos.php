<?php
        require_once("src/rules/nomina/dao/DAORolMetodos.php");

class ServicioRolMetodos
{       private $DAORolMetodos;
        private $Empleado;
        private $ServicioFormula;
        
        private $Variables = array();
        private $Constante = array();
                
        function __construct()
        {       $this->ServicioFormula = new ServicioFormula();
                $this->DAORolMetodos = new DAORolMetodos();
                $this->Constante = $this->ServicioFormula->ArrayConstanteGeneral();
                $this->Empleado = 0;
        }
            
        function InicializaMethodsRol($IdEmp = 0)
        {       if ($IdEmp!=0)
                {   $this->Variables = $this->ServicioFormula->ArrayVariablesEmpleados($IdEmp);
                    $this->Empleado = $IdEmp;
                    return true;
                }    
                return false;
        }
        
        function EjecutaMethodsRol($formula,$datos)
        {       foreach($datos as $parte)
                {       $fnName = trim($parte['funcion']);
                        $fnPrms = trim($parte['params']);
                        if ($fnName!="Numerico")
                        {   $semilla = "{".$fnName."(".$fnPrms.")"."}";
                            $reempla = $this->{$fnName}($fnPrms);
                            $formula = str_replace($semilla, $reempla, $formula);
                        }
                        else
                        $formula = $fnPrms;
                }
                eval('$resultado = '.$formula.';');
                return doubleval($resultado);
        }       
        
        function ConstVal($key) 
        {       if (array_key_exists($key, $this->Constante))
                {   return $this->Constante[$key]; 
                }
                return -1;
        }
                
        /*************************** PERSONALES ******************************/

        function ValNormal($key)
        {       if ($this->Empleado!=0){
                    if (array_key_exists($key, $this->Variables)){ 
                        return $this->Variables[$key];
                    }    
                    return 0;
                }    
                else
                return $this->Empleado;
        }

        function ValDiario($key)
        {       if ($this->Empleado!=0){
                    if ($this->Constante['_DIAS_30']!=0){
                        if (array_key_exists($key, $this->Variables)){  
                            return $this->Variables[$key]/$this->Constante['_DIAS_30'];
                        }    
                        return 0;
                    }
                    else{
                        echo "No existe valor para la constante {_DIAS_30}";
                        return 0;
                    }
                }
                return $this->Empleado;
        }

        function ValHora($key)
        {       if  ($this->Constante['_HORAS_JC']!=0){
                    if ($this->ValDiario($key)>0){ 
                        return $this->ValDiario($key)/$this->Constante['_HORAS_JC'];
                    }    
                    return 0;
                }else{
                    echo "No existe valor para la constante {_HORAS_JC}.";
                    return 0;
                }
        }

        /*************************** PROCESOS EXTERNOS ************************/
        
        function ProDiasTrabajados()
        {       if ($this->Empleado!=0) 
                    return 25;   
                else
                    return $this->Empleado;   
        }

        function ProHorasNormalesTrab()
        {       if ($this->Empleado!=0)  
                    return 40;   
                else
                    return $this->Empleado;   
        }

        function ProHorasCincuentaTrab()
        {       if ($this->Empleado!=0)   
                    return 4;   
                else
                    return $this->Empleado;   
        }

        function ProHorasCienTrab()
        {       if ($this->Empleado!=0)    
                    return 12;   
                else
                    return $this->Empleado;   
        }
                
        function ProLiquidacion()
        {
                return 100;
        }
                
}


?>