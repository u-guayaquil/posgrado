<?php
            require_once("src/rules/nomina/dao/DAOEmpleadoPermisos.php");
            
            class ServicioEmpleadoPermisos
            {     private $DAOEmpleadoPermisos;
     
                  function __construct()
                  {        $this->DAOEmpleadoPermisos = new DAOEmpleadoPermisos();
                  }
                  function GuardaDBEmpleadoPermisos($Form)
                  {       $fechai = explode('/', $Form->fedesde); 
                          $fechaf = explode('/', $Form->fehasta);
                          $Form->fedesde = $fechai[2].'-'.$fechai[1].'-'.$fechai[0].' '.$Form->hdesde.':00';
                          $Form->fehasta = $fechaf[2].'-'.$fechaf[1].'-'.$fechaf[0].' '.$Form->hhasta.':00';
                          $resultado = $this->ValidaFechas($Form);
                                if(is_numeric($resultado)){
                                    if (empty($Form->id)){
                                        return $this->DAOEmpleadoPermisos->InsertaEmpleadoPermisos($Form);
                                    }else{
                                        return $this->DAOEmpleadoPermisos->ActualizaEmpleadoPermisos($Form);
                                    }
                                }else{
                                    return $resultado;
                                }
                  }
                  
                  function DesactivaEmpleadoPermisos($Id)
                  {        return $this->DAOEmpleadoPermisos->DesactivaEmpleadoPermisos($Id);
                  }
           
                  function BuscarEmpleadoPermisos($PrepareDQL)
                  {        return $this->DAOEmpleadoPermisos->BuscarEmpleadoPermisos($PrepareDQL);
                  }
                  
                  function BuscarEmpleadoPermisosById($id)
                  {        return $this->DAOEmpleadoPermisos->BuscarEmpleadoPermisosById($id);
                  }
                  
                  function BuscarEmpleadoPermisosByEmpleado($id)
                  {        return $this->DAOEmpleadoPermisos->BuscarEmpleadoPermisosByEmpleado($id);
                  }
                  
                  function BuscarPermisosByEmpleadoyTexto($PrepareDQL)
                  {        return $this->DAOEmpleadoPermisos->BuscarPermisosByEmpleadoyTexto($PrepareDQL);
                  }
                  
                  function BuscarPermisosByEmpleadoyFechas($PrepareDQL)
                  {        return $this->DAOEmpleadoPermisos->BuscarPermisosByEmpleadoyFechas($PrepareDQL);
                  }
           
                  function BuscarEmpleadoPermisosByUsuario($PrepareDQL)
                  {        return $this->DAOEmpleadoPermisos->BuscarEmpleadoPermisosByUsuario($PrepareDQL);
                  }
                  
                  function BuscarEmpleadoPermisosImprime($PrepareDQL)
                  {        return $this->DAOEmpleadoPermisos->BuscarEmpleadoPermisosImprime($PrepareDQL);
                  }
                  
                  function ValidaFechas($Form){
                          $valida = 1;
                          $fechai = explode('/', $Form->fedesde); 
                          $fechaf = explode('/', $Form->fehasta);
                          $hdesde = explode(':', $Form->hdesde); 
                          $hhasta = explode(':', $Form->hhasta);

                          $fechaInicial = $fechai[2].$fechai[1].$fechai[0];
                          $fechaFinal = $fechaf[2].$fechaf[1].$fechaf[0];  
                          $hifechaInicial = $fechai[2].$fechai[1].$fechai[0].$hdesde[0].$hdesde[1];
                          $hffechaFinal = $fechaf[2].$fechaf[1].$fechaf[0].$hhasta[0].$hhasta[1];
                          
                          $compare_ingreso = strtotime($fechaInicial);
                          $compare_final = strtotime($fechaFinal);                          
                          $hicompare_ingreso = strtotime($hifechaInicial);
                          $hfcompare_final = strtotime($hffechaFinal);
                          
                          if($Form->fedesde != '' && $Form->fehasta != ''){
                            if ($compare_ingreso <= $compare_final){   
                                if($hicompare_ingreso <= $hfcompare_final){
                                    $valida = 1;
                                }else{
                                    $valida = 'La Hora Inicial debe ser menor a la Hora Final...';
                                }
                            } else {
                                 $valida = 'La Fecha Inicial debe ser menor a la Fecha Final...';
                            }                      
                          }else{
                              $valida = 'Debe Ingresar el rango de fechas completo...';
                          }
                          return $valida;
                  }
            }
?>

