<?php
            require_once("src/rules/nomina/dao/DAOPlantillaRol.php");
            
            class ServicioPlantillaRol
            {       private $DAOPlantillaRol;
     
                    function __construct()
                    {        $this->DAOPlantillaRol = new DAOPlantillaRol();
                    }
                  
                    function GuardaDB($Form,$Grid)
                    {       if (empty($Form->idplantilla)){
                                return $this->DAOPlantillaRol->Inserta($Form,$Grid);
                            }else{
                                return $this->DAOPlantillaRol->Actualiza($Form,$Grid);
                            }
                    }
                  
                    function DesactivaPlantillaRol($Id)
                    {       return $this->DAOPlantillaRol->DesactivaPlantillaRol($Id);
                    }
           
                    function BuscarPlantillaRol($prepareDQL)
                    {        return $this->DAOPlantillaRol->ObtenerPlantillaRol($prepareDQL);
                    }
                  
                    function BuscarPlantillaRolDetalle($prepareDQL)
                    {        return $this->DAOPlantillaRol->ObtenerPlantillaRolDetalle($prepareDQL);
                    }

                    function BuscarPlantillaRolByDescripcion($prepareDQL)
                    {        return $this->DAOPlantillaRol->ObtenerPlantillaRol($prepareDQL);
                    }
                    
                    function BuscarPlantillaRolEmpleado($prepareDQL)
                    {       $rolEmpleado = array();
                            $Plantillas = $this->DAOPlantillaRol->ObtenerPlantillaRolEmpleado($prepareDQL);
                            foreach($Plantillas as $Plantilla)
                            {       $rolFormula = array();
                                    $Formulas = $this->DAOPlantillaRol->ObtenerPlantillaRolFormula($Plantilla['idplantilla']);
                                    foreach($Formulas as $Formula)
                                    {       $rolDecoder = array();
                                            $Decoders = $this->DAOPlantillaRol->ObtenerPlantillaRolDecoder($Formula['id']);
                                            foreach($Decoders as $Decoder) 
                                            {       $rolDecoder[] = array('funcion'=>trim($Decoder['fnname']),'params'=>trim($Decoder['fnparam']));    
                                            }
                                            $rolFormula[] = array('idrubro'=>$Formula['idrubro'],'txrubro'=>trim($Formula['descripcion']),'movmnto'=>$Formula['idmovimiento'],'txformula'=>trim($Formula['txformula']),'decoders'=>$rolDecoder);
                                    }
                                    $Empleado = trim($Plantilla['apellido'])." ".trim($Plantilla['nombre']);
                                    $rolEmpleado[] = array('idempleado'=>$Plantilla['id'], 'idsucursal'=>trim($Plantilla['idsucursal']), 'idplantilla'=>$Plantilla['idplantilla'], 'plantilla'=>$Plantilla['plantilla'], 'empleado'=>$Empleado, 'formulas'=>$rolFormula);
                            }     
                            return $rolEmpleado;
                    }
                    
                    
                    
                    
                    
                    
            }
?>

