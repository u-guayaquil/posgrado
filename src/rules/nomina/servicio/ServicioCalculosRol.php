<?php
            require_once("src/rules/nomina/dao/DAOCalculosRol.php");
            require_once("src/rules/nomina/servicio/DAORubrosFijos.php");
            
            class ServicioCalculosRol
            {     private $DAOCalculosRol;
     
                  function __construct()
                  {        $this->DAOCalculosRol = new DAOCalculosRol();
                  }
                  function GuardaDBCalculosRol($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOCalculosRol->InsertaCalculosRol($Form);
                          }else{
                            return $this->DAOCalculosRol->ActualizaCalculosRol($Form);
                          }
                  }
                  
                  function DesactivaCalculosRol($Id)
                  {       return $this->DAOCalculosRol->DesactivaCalculosRol($Id);
                  }
           
                  function BuscarCalculosRolByID($id)
                  {        return $this->DAOCalculosRol->BuscarCalculosRolByDescripcion($id);
                  }

                  function BuscarCalculosRolByDescripcion($prepareDQL)
                  {     return $this->DAOCalculosRol->BuscarCalculosRolByDescripcion($prepareDQL);                                                
                  }
                  
                  function BuscarCalculosRolByArrayID($idArray)
                  {       return $this->DAOCalculosRol->ObtenerCalculosRolCombo($idArray);
                  }
                  
                  function BuscarCalculosRolsIndependientes($ArrayOrg)
                  {       return $this->DAOCalculosRol->ObtenerCalculosRolssinOrgDepartamento($ArrayOrg);
                  }
            }
?>