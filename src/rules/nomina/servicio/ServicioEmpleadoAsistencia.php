<?php
            require_once("src/rules/nomina/dao/DAOEmpleadoAsistencia.php");
            
            class ServicioEmpleadoAsistencia
            {     private $DAOEmpleadoAsistencia;
     
                  function __construct()
                  {        $this->DAOEmpleadoAsistencia = new DAOEmpleadoAsistencia();
                  }
                  function GuardaDBEmpleadoAsistencia($Form)
                  {       $Forma = $this->verificaDatos($Form);
                          //$resultado = $this->ValidaFechas($Form);
                               // if(is_numeric($resultado)){
                                    if (empty($Forma->id)){
                                        return $this->DAOEmpleadoAsistencia->InsertaEmpleadoAsistencia($Forma);
                                    }else{
                                        return $this->DAOEmpleadoAsistencia->ActualizaEmpleadoAsistencia($Forma);
                                    }
                               /* }else{
                                    return $resultado;
                                }*/
                  }
                  
                  function DesactivaEmpleadoAsistencia($Id)
                  {       return $this->DAOEmpleadoAsistencia->DesactivaEmpleadoAsistencia($Id);
                  }
           
                  function BuscarEmpleadoAsistencia($id)
                  {        return $this->DAOEmpleadoAsistencia->BuscarEmpleadoAsistencia($id);
                  }
                  
                  function ValidaFechas($Form){
                          $valida = 1;
                          $fechai = explode('/', $Form->fedesde); 
                          $fechaf = explode('/', $Form->fehasta);
                          $hdesde = explode(':', $Form->hdesde); 
                          $hhasta = explode(':', $Form->hhasta);

                          $fechaInicial = $fechai[2].$fechai[1].$fechai[0];
                          $fechaFinal = $fechaf[2].$fechaf[1].$fechaf[0];  
                          $hifechaInicial = $fechai[2].$fechai[1].$fechai[0].$hdesde[0].$hdesde[1];
                          $hffechaFinal = $fechaf[2].$fechaf[1].$fechaf[0].$hhasta[0].$hhasta[1];
                          
                          $compare_ingreso = strtotime($fechaInicial);
                          $compare_final = strtotime($fechaFinal);                          
                          $hicompare_ingreso = strtotime($hifechaInicial);
                          $hfcompare_final = strtotime($hffechaFinal);
                          
                          if($Form->fedesde != '' && $Form->fehasta != ''){
                            if ($compare_ingreso <= $compare_final){   
                                if($hicompare_ingreso <= $hfcompare_final){
                                    $valida = 1;
                                }else{
                                    $valida = 'La Hora Inicial debe ser menor a la Hora Final...';
                                }
                            } else {
                                 $valida = 'La Fecha Inicial debe ser menor a la Fecha Final...';
                            }                      
                          }else{
                              $valida = 'Debe Ingresar el rango de fechas completo...';
                          }
                          return $valida;
                  }
                  
                  
                  function verificaDatos($Form){
                          $feentrada = $Form->feentrada;            $fesalida = $Form->fesalida;    
                          $Alsalida = $Form->Alsalida;              $Alentrada = $Form->Alentrada;
                          $hentrada = $Form->hentrada;              $hsalida = $Form->hsalida;
                          $halsalida = $Form->halsalida;            $halentrada = $Form->halentrada;
                          $permiso =  $Form->idpermiso;

                            if($feentrada == ''){
                                $feentrada = '01/01/1900';
                            }
                            if($fesalida == ''){
                                $fesalida = '01/01/1900';
                            }
                            if($Alsalida == ''){
                                $Alsalida = '01/01/1900';
                            }
                            if($Alentrada == ''){
                                $Alentrada = '01/01/1900';
                            }
                            if($hentrada == ''){
                                $Form->hentrada = '00:00';
                            }
                            if($hsalida == ''){
                                $Form->hsalida = '00:00';
                            }
                            if($halsalida == ''){
                                $Form->halsalida = '00:00';
                            }
                            if($halentrada == ''){
                                $Form->halentrada = '00:00';
                            }
                            if($permiso == ''){
                                $Form->idpermiso = '0';
                            }
                        
                          $fechai = explode('/', $feentrada); 
                          $fechaf = explode('/', $fesalida);
                          $afechae = explode('/', $Alsalida); 
                          $afechas = explode('/', $Alentrada);
                          $Form->feentrada = $fechai[2].'-'.$fechai[1].'-'.$fechai[0].' '.$Form->hentrada.':00';
                          $Form->fesalida = $fechaf[2].'-'.$fechaf[1].'-'.$fechaf[0].' '.$Form->hsalida.':00';
                          $Form->Alentrada = $afechae[2].'-'.$afechae[1].'-'.$afechae[0].' '.$Form->halentrada.':00';
                          $Form->Alsalida = $afechas[2].'-'.$afechas[1].'-'.$afechas[0].' '.$Form->halsalida.':00';
                          return $Form;
                }
}
?>

