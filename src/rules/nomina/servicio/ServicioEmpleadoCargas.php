<?php
            require_once("src/rules/nomina/dao/DAOEmpleadoCargas.php");
            
            class ServicioEmpleadoCargas
            {     private $DAOEmpleadoCargas;
     
                    function __construct()
                    {        $this->DAOEmpleadoCargas = new DAOEmpleadoCargas();
                    }
                  
                    function Guarda($Form)
                    {       return $this->DAOEmpleadoCargas->Inserta($Form);
                    }

                    function Edita($Form)
                    {        return $this->DAOEmpleadoCargas->Actualiza($Form);
                    }
                    
                    function DesactivaEmpleadoCargas($Id)
                    {        return $this->DAOEmpleadoCargas->DesactivaEmpleadoCargas($Id);
                    }

                    function BuscarEmpleadoCargas($prepareDQL)
                    {        return $this->DAOEmpleadoCargas->ObtenerEmpleadoCargas($prepareDQL);
                    }
            }
?>

