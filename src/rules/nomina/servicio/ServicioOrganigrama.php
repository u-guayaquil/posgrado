<?php
            require_once("src/rules/nomina/dao/DAOOrganigrama.php");
            require_once("src/rules/nomina/entidad/Organigrama.php");
            
            class ServicioOrganigrama
            {     private $DAOOrganigrama;
     
                  function __construct()
                  {        $this->DAOOrganigrama = new DAOOrganigrama();
                  }
                  
                  function GuardaDBOrganigrama($Form)
                  {       if (empty($Form->id)){
                            return $this->CreaOrganigrama($Form);
                          }else{
                            return $this->EditaOrganigrama($Form);
                          }
                  }
           
                  function CreaOrganigrama($Form)
                  {   $fechai = explode('/', $Form->fedesde);
                      $fechaf = explode('/', $Form->fehasta); 
                      $ValidaFecha = $Form->fehasta;
                      $Form->fedesde = $fechai[2].'-'.$fechai[1].'-'.$fechai[0];
                      $Form->fehasta = $fechaf[2].'-'.$fechaf[1].'-'.$fechaf[0];
                      $fechaInicial = $fechai[2].$fechai[1].$fechai[0];
                      $fechaFinal = $fechaf[2].$fechaf[1].$fechaf[0];  
                      $compare_ingreso = strtotime($fechaInicial);
                      $compare_final = strtotime($fechaFinal);
                      if($ValidaFecha != ''){
                          if ($compare_ingreso < $compare_final){                       
                                $Resultado = $this->ValidaActivo($compare_ingreso);
                                if($Resultado != 1){
                                    return $Resultado;
                                }else{
                                    return $this->DAOOrganigrama->InsertaOrganigrama($Form);
                                }
                            } else {
                                return 'La Fecha Inicial debe ser menor a la Fecha Final...';
                            }
                      }else{
                          $Resultado = $this->ValidaActivo($compare_ingreso);
                          if($Resultado != 1){
                             return $Resultado;
                          }else{
                             return $this->DAOOrganigrama->InsertaOrganigrama($Form);
                          }
                      }
                  }
                  
                  function ValidaActivo($compare_ingreso){
                              $fechaValidada = $this->BuscaOrganigramaByFechas();                          
                              $fechaV = explode(' ', $fechaValidada[0]['fehasta']);
                              $fechaMayor = explode('-', $fechaV[0]);
                              $fechaMayorBase = $fechaMayor[0].$fechaMayor[1].$fechaMayor[2];
                              $compare_base = strtotime($fechaMayorBase);

                              if ($compare_base < $compare_ingreso){                            
                                  return 1;
                              } else {
                                  return 'Ya existe un Organigrama Activo...';
                              }
                  }
           
                  function BuscaOrganigramaByFechas($id=0)
                  {        return $this->DAOOrganigrama->ObtenerFechaOrganigramaVigente($id);
                  }
                  
                  function EditaOrganigrama($Form)
                  {     $fechai = explode('/', $Form->fedesde);
                        $fechaf = explode('/', $Form->fehasta);                         
                        $ValidaFecha = $Form->fehasta;
                        $Form->fedesde = $fechai[2].'-'.$fechai[1].'-'.$fechai[0];
                        $Form->fehasta = $fechaf[2].'-'.$fechaf[1].'-'.$fechaf[0];
                        $fechaInicial = $fechai[2].$fechai[1].$fechai[0];
                        $fechaFinal = $fechaf[2].$fechaf[1].$fechaf[0]; 
                        $compare_ingreso = strtotime($fechaInicial);
                        $compare_final = strtotime($fechaFinal);
                        if($ValidaFecha != ''){
                                if ($compare_ingreso < $compare_final){    
                                    $Resultado = $this->ValidaActivo($compare_ingreso);
                                    if($Resultado != 1){
                                        return $Resultado;
                                    }else{
                                        return $this->DAOOrganigrama->ActualizaOrganigrama($Form);
                                    }
                                } else {
                                    return 'La Fecha Inicial debe ser menor a la Fecha Final...';
                                }
                        }else{
                          $Resultado = $this->ValidaActivo($compare_ingreso);
                          if($Resultado != 1){
                             return $Resultado;
                          }else{
                             return $this->DAOOrganigrama->ActualizaOrganigrama($Form);
                          }
                      }
                  }
                  
                  function DesactivaOrganigrama($Id)
                  {        return $this->DAOOrganigrama->DesactivaOrganigrama($Id);
                  }
           
                  function BuscarOrganigramaByID($id)
                  {        return $this->DAOOrganigrama->BuscarOrganigramaByDescripcion($id);
                  }

                  function BuscarOrganigramaByDescripcion($id)
                  {     return $this->DAOOrganigrama->BuscarOrganigramaByDescripcion($id);                        
                  } 
                  
                  function BuscarOrganigramaByArrayID($id)
                  {       return $this->DAOOrganigrama->ObtenerOrganigramaVigente($id);
                  }
                  
                  

            }
?>

