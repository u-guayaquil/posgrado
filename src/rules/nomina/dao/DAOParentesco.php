<?php
        require_once("src/base/DBDAO.php");

        class DAOParentesco extends DBDAO
        {     private static $entityParentesco;
              private $Funciones;
              
              public function __construct()
              {      self::$entityParentesco = DBDAO::getConnection();
                     $this->Funciones = self::$entityParentesco->MyFunction();
              }
              
              function BuscarParentesco()
              {      $DSQL = "SELECT id, descripcion "
                           . "FROM general.parentesco "
                           . "ORDER BY descripcion";           
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
        }
