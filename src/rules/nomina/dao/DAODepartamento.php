<?php
        require_once("src/base/DBDAO.php");

        class DAODepartamento extends DBDAO
        {     private static $entityDepartamento;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityDepartamento = DBDAO::getConnection();
                     $this->Funciones = self::$entityDepartamento->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarDepartamentoByDescripcion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and d.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(d.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and d.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT d.id, d.descripcion,d.fecreacion, e.id as idestado"
                           . ", e.descripcion as txtestado "
                           . "FROM nomina.departamento d, general.Estado e "
                           . "WHERE d.idestado=e.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY d.descripcion".$LIM;                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaDepartamento($datos)
              {    try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');
                        $Query = "INSERT INTO nomina.departamento (descripcion,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);                           
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Departamento..';
                    }                   
              }
    
              public function ActualizaDepartamento($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.departamento "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.""
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaDepartamento($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.departamento SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Departamento..';
                        }
              }
              
              public function ObtenerDepartamentoCombo($IdArray)
              {      $QueryBld = $this->entityDepartamento->createQueryBuilder('d');
                     $WhereBld = $QueryBld->expr()->in('d.idestado', ':Arreglo');
                     $QueryDQL = $QueryBld->where($WhereBld)->orderBy('d.descripcion', 'ASC')
                                       ->setParameter('Arreglo',$IdArray)
                                       ->getQuery();
                     return $QueryDQL->getArrayResult();
             }
                    
             public function ObtenerDepartamentossinOrganigramas($ArrayOrgDepartamento)
             {      $QueryBld = $this->entityDepartamento->createQueryBuilder('d');
                    $QueryBld->add('select','d.id,d.descripcion');                           
                    $WhereBld = $QueryBld->expr()->notIn('d.id', ':Arreglo');
                    $QueryDQL = $QueryBld->where($WhereBld)
                                         ->setParameter('Arreglo',$ArrayOrgDepartamento)
                                         ->orderBy('d.descripcion','ASC')
                                         ->getQuery();
                    return $QueryDQL->getArrayResult(); 
             }
                    
        }
