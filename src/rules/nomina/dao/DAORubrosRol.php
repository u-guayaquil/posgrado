<?php
        require_once("src/base/DBDAO.php");

        class DAORubrosRol extends DBDAO
        {   private static $entityRubrosRol;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {   self::$entityRubrosRol = DBDAO::getConnection();
                $this->Funciones = self::$entityRubrosRol->MyFunction();
                $this->Fecha  = new DateControl();
            }
              
            function BuscarRubrosRol($PrepareDQL)
            {   $JOIN = array_key_exists("id", $PrepareDQL) ?       " and r.id =".$PrepareDQL['id'] : "";
                $JOIN.= array_key_exists("variable", $PrepareDQL) ? " and r.variable <>".$PrepareDQL['variable'] : "";
                $JOIN.= array_key_exists("movimnto", $PrepareDQL) ? " and (m.id in (".$PrepareDQL['movimnto'].")) ": "";
                $JOIN.= array_key_exists("estado", $PrepareDQL) ?   " and (r.idestado in (".$PrepareDQL['estado'].")) ": "";
                $JOIN.= array_key_exists("texto", $PrepareDQL) ?    " and (upper(r.descripcion) LIKE '%".$PrepareDQL['texto']."%') ": "";
                $ORDN = array_key_exists("orden", $PrepareDQL) ? $PrepareDQL['orden'] : " r.descripcion ";
                $LIMT = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";

                $DSQL = "SELECT 
                                r.id, 
                                r.descripcion,
                                r.variable, g.descripcion as txvariable,
                                r.idmovimiento, m.descripcion as movimiento, 
                                r.fecreacion, 
                                e.id as idestado, e.descripcion as txtestado,
                                r.ordenrol
                        FROM 
                                general.estado e, (nomina.rubros r 
                                INNER JOIN nomina.variable_rubros as g ON g.id=r.variable   
                                INNER JOIN general.movimiento m ON r.idmovimiento=m.id) 
                        WHERE 
                                r.idestado=e.id ".$JOIN."         
                        ORDER BY 
                                ".$ORDN.$LIMT; 
                    
                return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            public function InsertaRubrosRol($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');
                    $descripcion = strtoupper($datos->descripcion);
                    $Query = "INSERT INTO nomina.rubros (ordenrol,variable,descripcion,idmovimiento,idestado,iduscreacion,fecreacion) ";
                    $Query.= "VALUES ('$datos->orden','$datos->variablerubros','$descripcion','$datos->movimiento', '$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id ";
                    return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
            }
    
            public function ActualizaRubrosRol($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $descripcion = strtoupper(trim($datos->descripcion));
                    
                    $Query = "UPDATE nomina.rubros ";
                    $Query.= "SET ";
                    $Query.= "      ordenrol='$datos->orden',";
                    $Query.= "      variable='$datos->variablerubros',";
                    $Query.= "      descripcion='$descripcion',";
                    $Query.= "      idmovimiento='$datos->movimiento',";
                    $Query.= "      idestado='$datos->estado',";
                    $Query.= "      idusmodifica=".$_SESSION['IDUSER'].",";
                    $Query.= "      femodificacion='$fecha' ";
                    $Query.= "WHERE id= ".$datos->id;
                    
                    $this->Funciones->Query($Query);
                    return $datos->id;
            } 
    
            public function DesactivaRubrosRol($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE nomina.rubros SET idestado=0, idusmodifica=".$_SESSION['IDUSER'].", femodificacion='$fecha' WHERE id='$id' ";
                    $this->Funciones->Query($Query);
                    return intval($id);
            }
            
        }
