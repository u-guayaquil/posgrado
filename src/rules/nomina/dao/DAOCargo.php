<?php
        require_once("src/base/DBDAO.php");

        class DAOCargo extends DBDAO
        {       private static $entityCargo;
                private $Funciones;
                private $Fecha;
              
                public function __construct()
                {       self::$entityCargo = DBDAO::getConnection();
                        $this->Funciones = self::$entityCargo->MyFunction();
                        $this->Fecha  = new DateControl();
                }
              
                function BuscarCargoByDescripcion($PrepareDQL)
                {       $JOIN = array_key_exists("estado", $PrepareDQL) ? " and c.idestado in (".$PrepareDQL['estado'].")" : "";
                        $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(c.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                        $IDD  = array_key_exists("id", $PrepareDQL) ? " and c.id =".$PrepareDQL['id']: "";
                        $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                        $DEP  = array_key_exists("departamento", $PrepareDQL) ? " and c.iddepartamento =".$PrepareDQL['departamento']: "";
                        
                        $DSQL = "SELECT "
                              . "c.id, c.descripcion, c.fecreacion, "
                              . "e.id as idestado, e.descripcion as txtestado, "
                              . "c.iddepartamento,d.descripcion as departamento "
                              . "FROM nomina.cargo c, general.estado e,nomina.departamento d "
                              . "WHERE c.idestado=e.id and d.id=c.iddepartamento  ".$JOIN.$LIKE.$IDD.$DEP
                              . "ORDER BY c.descripcion".$LIM;                  
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
              
                public function InsertaCargo($datos)
                {   try {
                            $estado = $datos->estado;
                            $descripcion = strtoupper($datos->descripcion);
                            $departamento = $datos->departamento;
                            $fechai = $this->Fecha->getNowDateTime('DATETIME');
                            $Query = "INSERT INTO nomina.cargo (descripcion,iddepartamento,idestado,iduscreacion,fecreacion)"
                                   . "VALUES ('".$descripcion."', ".$departamento.", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                            $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                            return intval($id);
                        } 
                        catch (Exception $ex) 
                        {   $this->manejadorExcepciones($ex);
                            return 'No se pudo crear el cargo.';
                        }                     
                }
    
              public function ActualizaCargo($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $departamento = $datos->departamento;
                        $descripcion = strtoupper($datos->descripcion);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.cargo "
                               . "SET descripcion= '".$descripcion."',iddepartamento=".$departamento.", idestado= ".$estado.""
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaCargo($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.cargo SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Cargo..';
                        }
              }
        }
