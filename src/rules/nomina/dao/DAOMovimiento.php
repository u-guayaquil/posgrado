<?php
        require_once("src/base/DBDAO.php");

        class DAOMovimiento extends DBDAO
        {     private static $entityMovimiento;
              private $Funciones;
              
              public function __construct()
              {      self::$entityMovimiento = DBDAO::getConnection();
                     $this->Funciones = self::$entityMovimiento->MyFunction();
              }
              
              function BuscarMovimiento()
              {      $DSQL = "SELECT id, descripcion "
                           . "FROM general.movimiento "
                           . "ORDER BY descripcion";           
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
        }
