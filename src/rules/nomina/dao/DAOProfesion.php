<?php
        require_once("src/base/DBDAO.php");

        class DAOProfesion extends DBDAO
        {     private static $entityProfesion;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityProfesion = DBDAO::getConnection();
                     $this->Funciones = self::$entityProfesion->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarProfesionByDescripcion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and c.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(c.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and c.id =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT c.id,c.codigo, c.descripcion, c.fecreacion, e.id as idestado"
                           . ", e.descripcion as txtestado "
                           . "FROM nomina.profesion c, general.Estado e "
                           . "WHERE c.idestado=e.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY c.descripcion"
                           . " limit ".$PrepareDQL['limite'];                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaProfesion($datos)
              {    try{
                        $estado = $datos->estado;
                        $codigo = strtoupper($datos->codigo);
                        $descripcion = strtoupper($datos->descripcion);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');
                        $Query = "INSERT INTO nomina.profesion (codigo,descripcion,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$codigo."','".$descripcion."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Profesion..';
                    }                     
              }
    
              public function ActualizaProfesion($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);                        
                        $codigo = strtoupper($datos->codigo);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.profesion "
                               . "SET codigo = '".$codigo."',descripcion= '".$descripcion."', idestado= ".$estado.""
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaProfesion($id)
              {      try{
                            $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                            $Query = "UPDATE nomina.profesion SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                            $this->Funciones->Query($Query);
                            return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar la Profesion..';
                        }
              }
        }
