<?php
        require_once("src/base/DBDAO.php");

        class DAOFormula extends DBDAO
        {       private static $entityFormula;
                private $Funciones;
              
                public function __construct()
                {       self::$entityFormula = DBDAO::getConnection();
                        $this->Funciones = self::$entityFormula->MyFunction();
                }
                
                function ExisteVariableRubro($key)
                {       $DSQL = "select count(descripcion) from nomina.variable_rubros where descripcion='$key'";
                        $RESP = $this->Funciones->FieldData($this->Funciones->Query($DSQL),0);
                        if ($RESP==1) return true; else return false;
                }
                
                function ObtenerVariablesEmpleado($idEmp)
                {       $DSQL = "select vr.descripcion as txvariable,er.valor ";
                        $DSQL.= "from nomina.empleado_rubro as er, nomina.rubros as r, nomina.variable_rubros as vr ";
                        $DSQL.= "where er.idempleado='$idEmp' and r.id=er.idrubrorol and er.idestado=1 and r.variable=vr.id and vr.valor=0 ";
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
                
                function ObtenerVariablesConstante($Opcion)
                {       if ($Opcion=='C')
                            $DSQL = "select descripcion,valor from nomina.variable_rubros where valor<>0 ";
                        else
                        {   $DSQL = "Select b.descripcion, b.valor From nomina.rubros as a, nomina.variable_rubros as b ";
                            $DSQL.= "Where a.variable = b.id and a.variable<>0 and a.idmovimiento<>4 ";
                        }    
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
        }
        
?>
