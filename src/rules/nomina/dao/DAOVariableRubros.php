<?php
        require_once("src/base/DBDAO.php");

        class DAOVariableRubros extends DBDAO
        {     private static $entityVariableRubros;
              private $Funciones;
              
              public function __construct()
              {      self::$entityVariableRubros = DBDAO::getConnection();
                     $this->Funciones = self::$entityVariableRubros->MyFunction();
              }
              
              function BuscarVariableRubros()
              {      $DSQL = "SELECT id, descripcion,valor "
                           . "FROM nomina.variable_rubros where valor='0'"
                           . "ORDER BY descripcion";           
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
        }
