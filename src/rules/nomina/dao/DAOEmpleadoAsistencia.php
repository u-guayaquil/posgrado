<?php
        require_once("src/base/DBDAO.php");

        class DAOEmpleadoAsistencia extends DBDAO
        {     private static $entityEmpleadoAsistencia;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityEmpleadoAsistencia = DBDAO::getConnection();
                     $this->Funciones = self::$entityEmpleadoAsistencia->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarEmpleadoAsistencia($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " WHERE a.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(m.nombre) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE a.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT a.id, a.idempleado,a.hdesde,a.hhasta,a.halmuerzo,a.feentrada,a.fesalida"
                           . ",a.salidaalmuerzo,a.entradaalmuerzo, a.idpermiso, e.id as idestado, e.descripcion as txtestado"
                           . ",p.observaciones as permiso,m.apellido||' '||m.nombre as empleado, t.descripcion as motivo,p.idmotivo "
                           . "FROM nomina.empleado_asistencia a "
                           . "INNER JOIN general.Estado e ON a.idestado=e.id "
                           . "INNER JOIN general.empleado m ON a.idempleado=m.id "
                           . "LEFT JOIN nomina.empleado_permisos p ON a.idpermiso=p.id "
                           . "LEFT JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY a.id ".$LIM;                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaEmpleadoAsistencia($datos)
              {    try{
                        $estado = $datos->estado;
                        $empleado = ($datos->idempleado);
                        $feentrada = ($datos->feentrada);
                        $fesalida = ($datos->fesalida);
                        $Alsalida = ($datos->Alsalida);
                        $Alentrada = ($datos->Alentrada);
                        $permiso = ($datos->permiso);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');
                        $Query = "INSERT INTO nomina.empleado_asistencia (idempleado,hdesde,hhasta,halmuerzo,feentrada,fesalida"
                               . ",salidaalmuerzo,entradaalmuerzo,idpermiso,idestado,iduscreacion,fecreacion)"
                               . "VALUES (".$empleado.",'".$feentrada."','".$fesalida."','".$feentrada."'"
                               . ",'".$feentrada."','".$fesalida."','".$Alsalida."','".$Alentrada."',".$permiso.""
                               . ", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Permiso ..';
                    }                     
              }
    
              public function ActualizaEmpleadoAsistencia($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $empleado = ($datos->idempleado);
                        $feentrada = ($datos->feentrada);
                        $fesalida = ($datos->fesalida);
                        $Alsalida = ($datos->Alsalida);
                        $Alentrada = ($datos->Alentrada);
                        $permiso = ($datos->permiso);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');   

                        $Query = "UPDATE nomina.empleado_asistencia "
                               . "SET idempleado= ".$empleado.",hdesde = '".$feentrada."',hhasta= '".$fesalida."', halmuerzo='".$feentrada."'"
                               . ",feentrada = '".$feentrada."',fesalida = '".$fesalida."',salidaalmuerzo = '".$Alsalida."' "
                               . ",entradaalmuerzo= '".$Alentrada."', idpermiso=".$permiso.",idestado= ".$estado.", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaEmpleadoAsistencia($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.empleado_asistencia SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Permiso ..';
                        }
              }
        }
