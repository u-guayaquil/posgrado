<?php
        require_once("src/base/DBDAO.php");

        class DAOEmpleadoPermisos extends DBDAO
        {     private static $entityEmpleadoPermisos;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityEmpleadoPermisos = DBDAO::getConnection();
                     $this->Funciones = self::$entityEmpleadoPermisos->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarEmpleadoPermisos($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " WHERE p.idestado in (".$PrepareDQL['estado'].")" : "";
                     $MODA = array_key_exists("estadomodal", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estadomodal'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(m.nombre) LIKE '%".$PrepareDQL['texto']."%' ".$MODA: "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE p.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT p.id, p.idempleado,p.idtipo,p.idmotivo,p.detalle,p.stremunerado,p.stestado"
                           . ",p.fedesde,p.fehasta,m.apellido||' '||m.nombre as empleado,p.idreemplazo,r.apellido||' '||r.nombre as reemplazo"
                           . ",p.observaciones, p.fecreacion, e.id as idestado, e.descripcion as txtestado"
                           . ",t.descripcion as motivo,s.descripcion as estadopermiso "
                           . "FROM nomina.empleado_permisos p "
                           . "INNER JOIN general.Estado e ON p.idestado=e.id "
                           . "INNER JOIN general.empleado m ON p.idempleado=m.id "
                           . "LEFT JOIN general.empleado r ON p.idreemplazo=r.id "
                           . "INNER JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id "
                           . "LEFT JOIN general.estado s ON p.stestado=s.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY p.id ".$LIM;  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarEmpleadoPermisosById($id)
              {      $DSQL = "SELECT p.id, p.idempleado,p.idtipo,p.idmotivo,p.detalle,p.stremunerado,p.stestado"
                           . ",p.fedesde,p.fehasta,m.apellido||' '||m.nombre as empleado,p.idreemplazo,r.apellido||' '||r.nombre as reemplazo"
                           . ",p.observaciones, p.fecreacion, e.id as idestado, e.descripcion as txtestado"
                           . ",t.descripcion as motivo,s.descripcion as estadopermiso "
                           . "FROM nomina.empleado_permisos p "
                           . "INNER JOIN general.Estado e ON p.idestado=e.id "
                           . "INNER JOIN general.empleado m ON p.idempleado=m.id "
                           . "LEFT JOIN general.empleado r ON p.idreemplazo=r.id "
                           . "INNER JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id "
                           . "LEFT JOIN general.estado s ON p.stestado=s.id "
                           . " WHERE p.id= ".$id
                           . " ORDER BY p.id "; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarEmpleadoPermisosByEmpleado($id)
              {      $DSQL = "SELECT p.id, p.idempleado,p.idtipo,p.idmotivo,p.detalle,p.stremunerado,p.stestado"
                           . ",p.fedesde,p.fehasta,m.apellido||' '||m.nombre as empleado,p.idreemplazo,r.apellido||' '||r.nombre as reemplazo"
                           . ",p.observaciones, p.fecreacion, e.id as idestado, e.descripcion as txtestado"
                           . ",t.descripcion as motivo,s.descripcion as estadopermiso "
                           . "FROM nomina.empleado_permisos p "
                           . "INNER JOIN general.Estado e ON p.idestado=e.id "
                           . "INNER JOIN general.empleado m ON p.idempleado=m.id "
                           . "LEFT JOIN general.empleado r ON p.idreemplazo=r.id "
                           . "INNER JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id "
                           . "LEFT JOIN general.estado s ON p.stestado=s.id "
                           . " WHERE p.idempleado= ".$id
                           . " ORDER BY p.id ";  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarPermisosByEmpleadoyTexto($PrepareDQL)
              {      $DSQL = "SELECT p.id, p.idempleado,p.idtipo,p.idmotivo,p.detalle,p.stremunerado,p.stestado"
                           . ",p.fedesde,p.fehasta,m.apellido||' '||m.nombre as empleado,p.idreemplazo,r.apellido||' '||r.nombre as reemplazo"
                           . ",p.observaciones, p.fecreacion, e.id as idestado, e.descripcion as txtestado"
                           . ",t.descripcion as motivo,s.descripcion as estadopermiso "
                           . "FROM nomina.empleado_permisos p "
                           . "INNER JOIN general.Estado e ON p.idestado=e.id "
                           . "INNER JOIN general.empleado m ON p.idempleado=m.id "
                           . "LEFT JOIN general.empleado r ON p.idreemplazo=r.id "
                           . "INNER JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id "
                           . "LEFT JOIN general.estado s ON p.stestado=s.id "
                           . " WHERE p.idempleado= ".$PrepareDQL['empleado']." and t.descripcion like '%".$PrepareDQL['texto']."%'"
                           . " ORDER BY p.id ";
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarEmpleadoPermisosByUsuario($PrepareDQL)
              {      $DSQL = "SELECT id, idempleado,idtipo,idmotivo,detalle,stremunerado,stestado,fedesde,fehasta
                              ,empleado,idreemplazo,reemplazo,observaciones, fecreacion,idestado,txtestado,motivo,estadopermiso FROM ("
                           . "SELECT p.id, p.idempleado,p.idtipo,p.idmotivo,p.detalle,p.stremunerado,p.stestado"
                           . ",p.fedesde,p.fehasta,m.apellido||' '||m.nombre as empleado,p.idreemplazo,r.apellido||' '||r.nombre as reemplazo"
                           . ",p.observaciones, p.fecreacion, e.id as idestado, e.descripcion as txtestado"
                           . ",t.descripcion as motivo,s.descripcion as estadopermiso "
                           . "FROM nomina.empleado_permisos p "
                           . "INNER JOIN general.Estado e ON p.idestado=e.id "
                           . "INNER JOIN general.empleado m ON p.idempleado=m.id "
                           . "LEFT JOIN general.empleado r ON p.idreemplazo=r.id "
                           . "INNER JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id "
                           . "LEFT JOIN general.estado s ON p.stestado=s.id "
                           . "INNER JOIN sistema.usuario u ON p.idempleado = u.idempleado 
                              WHERE u.id = ".$PrepareDQL['usuario']." and m.apellido||' '||m.nombre like '%".$PrepareDQL['texto']."%'
                               
                              UNION 

                              SELECT p.id, p.idempleado,p.idtipo,p.idmotivo,p.detalle,p.stremunerado,p.stestado
                              ,p.fedesde,p.fehasta,m.apellido||' '||m.nombre as empleado,p.idreemplazo,r.apellido||' '||r.nombre as reemplazo
                              ,p.observaciones, p.fecreacion, e.id as idestado, e.descripcion as txtestado
                              ,t.descripcion as motivo,s.descripcion as estadopermiso 
                              FROM nomina.empleado_permisos p 
                              INNER JOIN general.Estado e ON p.idestado=e.id 
                              INNER JOIN general.empleado m ON p.idempleado=m.id 
                              LEFT JOIN general.empleado r ON p.idreemplazo=r.id 
                              INNER JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id 
                              LEFT JOIN general.estado s ON p.stestado=s.id
                              INNER JOIN sistema.usuario u ON u.id = 1 
                              WHERE u.idempleado = m.idjefe  and m.apellido||' '||m.nombre like '%".$PrepareDQL['texto']."%'
                              ) as d"
                           . " ORDER BY d.id "; 
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarPermisosByEmpleadoyFechas($PrepareDQL)
              {      $IDD  = array_key_exists("permiso", $PrepareDQL) ? " and p.id =".$PrepareDQL['permiso']: "";
                     $DSQL = "SELECT p.id as idmotivo,t.descripcion as motivo "
                           . "FROM nomina.empleado_permisos p "
                           . "INNER JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id "
                           . " WHERE p.idempleado= ".$PrepareDQL['id'].$IDD
                           . "and p.fedesde::DATE >= NOW()::DATE AND NOW()::DATE <= p.fehasta::DATE"
                           . " ORDER BY p.id ";  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              function BuscarEmpleadoPermisosImprime($PrepareDQL)
              {      $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE p.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT p.id, p.idempleado,p.idtipo,p.idmotivo,p.detalle,p.stremunerado,p.stestado"
                           . ",p.fedesde,p.fehasta,m.apellido||' '||m.nombre as empleado,p.idreemplazo,r.apellido||' '||r.nombre as reemplazo"
                           . ",p.observaciones, p.fecreacion, e.id as idestado, e.descripcion as txtestado"
                           . ",t.descripcion as motivo,s.descripcion as estadopermiso,m.numero,d.descripcion as departamento"
                           . ",j.apellido||' '||j.nombre as jefe,p.fecreacion,m.firma  "
                           . "FROM nomina.empleado_permisos p "
                           . "INNER JOIN general.Estado e ON p.idestado=e.id "
                           . "INNER JOIN general.empleado m ON p.idempleado=m.id "
                           . "LEFT JOIN general.empleado r ON p.idreemplazo=r.id "
                           . "INNER JOIN nomina.departamento d ON m.iddepartamento=d.id "
                           . "LEFT JOIN general.empleado j ON m.idjefe=j.id "
                           . "INNER JOIN nomina.empleado_motivo_permisos t ON p.idmotivo=t.id "
                           . "LEFT JOIN general.estado s ON p.stestado=s.id ".$IDD
                           . " ORDER BY p.id ".$LIM;     
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaEmpleadoPermisos($datos)
              {    try{
                        $estado = $datos->estado;
                        $empleado = ($datos->idempleado);
                        $tipo = ($datos->tipopermiso);
                        $motivo = ($datos->motivo);
                        $fedesde = ($datos->fedesde);
                        $fehasta = ($datos->fehasta);
                        $detalle = strtoupper($datos->detalle);
                        $remunerado = strtoupper($datos->remunerado);
                        $estadosol = ($datos->estadopermiso);
                        if($datos->idreemplazo == ''){
                            $reemplazo = 0;
                        }else{
                            $reemplazo = ($datos->idreemplazo);
                        }
                        $observaciones = strtoupper($datos->observaciones);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');
                        $Query = "INSERT INTO nomina.empleado_permisos (idtipo,idempleado,idmotivo,fedesde,fehasta,detalle,stremunerado"
                               . ",stestado,idreemplazo,observaciones,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$tipo."',".$empleado.",".$motivo.",'".$fedesde."','".$fehasta."'"
                               . ",'".$detalle."','".$remunerado."',".$estadosol.",".$reemplazo.",'".$observaciones."'"
                               . ", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id"; 
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Permiso ..';
                    }                     
              }
    
              public function ActualizaEmpleadoPermisos($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $empleado = ($datos->idempleado);
                        $tipo = ($datos->tipopermiso);
                        $motivo = ($datos->motivo);
                        $fedesde = ($datos->fedesde);
                        $fehasta = ($datos->fehasta);
                        $detalle = strtoupper($datos->detalle);
                        $remunerado = strtoupper($datos->remunerado);
                        $estadosol = ($datos->estadopermiso);
                        if($datos->idreemplazo == ''){
                            $reemplazo = 0;
                        }else{
                            $reemplazo = ($datos->idreemplazo);
                        }
                        $observaciones = strtoupper($datos->observaciones);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');   

                        $Query = "UPDATE nomina.empleado_permisos "
                               . "SET idtipo= '".$tipo."',idempleado= ".$empleado.",idmotivo = '".$motivo."',stestado= ".$estadosol.", stremunerado='".$remunerado."'"
                               . ",detalle = '".$detalle."',fedesde = '".$fedesde."',fehasta = '".$fehasta."' "
                               . ",idreemplazo= ".$reemplazo.", observaciones='".$observaciones."',idestado= ".$estado.", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaEmpleadoPermisos($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.empleado_permisos SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Permiso ..';
                        }
              }
        }
