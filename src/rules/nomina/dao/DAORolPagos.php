<?php

        require_once("src/base/DBDAO.php");

        class DAORolPagos extends DBDAO
        {       private static $entityRolPagos;
                private $Funciones;
                private $Fecha;
              
                public function __construct()
                {       self::$entityRolPagos = DBDAO::getConnection();
                        $this->Funciones = self::$entityRolPagos->MyFunction();
                        $this->Fecha  = new DateControl();
                }

                function ObtenerRolPagos($PrepareDQL)
                {       $JOIN = array_key_exists("id", $PrepareDQL) ?     " and x.id=".$PrepareDQL['id'] : "";
                        $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and x.idestado in (".$PrepareDQL['estado'].")" : "";

                        $DSQL = "Select x.id,x.descripcion,x.fecharol,";
                        $DSQL.= "       x.idfrecuencia, f.descripcion as frecuencia,";
                        $DSQL.= "       x.idplantilla, pr.descripcion as plantilla,";
                        $DSQL.= "       x.idasiento,";	
                        $DSQL.= "       x.idsucursal, s.descripcion as sucursal,";
                        $DSQL.= "       x.idmediopago, m.descripcion||' '||ms.descripcion||' '||mp.descripcion as mediopago,";
                        $DSQL.= "       x.idestado, e.descripcion as txestado "; 
                        $DSQL.= "From   caja.medios as m,";
                        $DSQL.= "       caja.medios_pagos as mp,";
                        $DSQL.= "       caja.medios_subtipo as ms, ";
                        $DSQL.= "       general.frecuencia as f,";
                        $DSQL.= "       general.estado as e,"; 
                        $DSQL.= "       nomina.rolpagos as x ";
                        $DSQL.= "       LEFT JOIN nomina.plantilla_rol as pr ON x.idplantilla = pr.id ";
                        $DSQL.= "       LEFT JOIN general.sucursal as s ON x.idsucursal = s.id ";
                        $DSQL.= "Where  ms.id=mp.idsubtipo and m.id=mp.idcajamed and x.idestado=e.id and f.id=x.idfrecuencia and mp.id=x.idmediopago ".$JOIN;
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                function ObtenerRolPagosDetalleResumen($Id)
                {       $DSQL = "Select x.idempleado,y.apellido||' '||y.nombre as empleado,";
                        $DSQL.= "       sum(case x.idmovmnto when 3 then x.valor else 0 end) as provision,";
                        $DSQL.= "       sum(case x.idmovmnto when 1 then x.valor else 0 end) as ingreso,";
                        $DSQL.= "       sum(case x.idmovmnto when 2 then x.valor else 0 end) as egreso ";
                        $DSQL.= "From ";
                        $DSQL.= "       nomina.rolpagos_preproceso as x,general.empleado as y ";
                        $DSQL.= "Where ";
                        $DSQL.= "       x.idempleado=y.id and idusuario=".$_SESSION['IDUSER']." and x.idrol='$Id' ";
                        $DSQL.= "Group by ";
                        $DSQL.= "       x.idempleado,y.nombre,y.apellido ";               
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                function ObtenerRolPagosDetalle($Id)
                {       $DSQL = "Select w.descripcion as txrol,w.fecharol,y.id as idempleado,y.apellido||' '||y.nombre as empleado,";
                        $DSQL.= "       z.descripcion as txrubro,x.idmovmnto,x.valor,f.descripcion as frecuencia ";
                        $DSQL.= "From ";
                        $DSQL.= "       nomina.rolpagos as w,nomina.rolpagos_preproceso as x,";
                        $DSQL.= "       general.empleado as y, nomina.rubros as z, general.frecuencia as f ";
                        $DSQL.= "Where ";
                        $DSQL.= "       x.idempleado=y.id and z.id=x.idrubro and w.idfrecuencia=f.id and ";
                        $DSQL.= "       w.id=x.idrol and x.valor<>0 and x.idrol='$Id' ";
                        $DSQL.= "Order by ";
                        $DSQL.= "       x.idempleado, x.idmovmnto, z.ordenrol ";
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
                
                public function Inserta($datos) 
                {       $idplantilla = (empty($datos->idplantilla) ? 0: $datos->idplantilla);
                        $fecha = $this->Fecha->getNowDateTime('DATETIME');
                        $descripcion = strtoupper(trim($datos->descripcion));

                        self::$entityRolPagos->MyTransaction('BEGIN');
                        $Query = "INSERT INTO nomina.rolpagos (descripcion,fecharol,idfrecuencia,idplantilla,idsucursal,idmediopago,iddocumento,idsecdocum,idestado,iduscreacion,fecreacion) ";
                        $Query.= "VALUES ('$descripcion','$datos->fecha','$datos->frecuencia','$idplantilla','$datos->sucursal','$datos->idmpago',0,0,'$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id ";
                        $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                        $this->ActualizaRolPreProceso($id);
                        self::$entityRolPagos->MyTransaction('COMMIT');
                        return $id;
                }
                
                public function Actualiza($datos)
                {       $idplantilla = (empty($datos->idplantilla) ? 0: $datos->idplantilla);
                        $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                        $descripcion = strtoupper(trim($datos->descripcion));
                        
                        self::$entityRolPagos->MyTransaction('BEGIN');    
                        $Query = "UPDATE nomina.rolpagos Set ";
                        $Query.= "descripcion='$descripcion', ";
                        $Query.= "fecharol='$datos->fecha', ";
                        $Query.= "idplantilla='$idplantilla', ";
                        $Query.= "idsucursal='$datos->sucursal', ";
                        $Query.= "idmediopago='$datos->idmpago', ";
                        $Query.= "iddocumento=0, ";
                        $Query.= "idsecdocum=0, ";
                        $Query.= "idestado='$datos->estado', ";
                        $Query.= "idusmodifica=".$_SESSION['IDUSER'].", ";
                        $Query.= "femodificacion='$fecha' ";
                        $Query.= "WHERE id='$datos->idrol'";
                        $this->Funciones->Query($Query);
                        
                        self::$entityRolPagos->MyTransaction('COMMIT');
                        return $datos->idrol;
                }

                public function DesactivaRolPagos($id)
                {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                        self::$entityRolPagos->MyTransaction('BEGIN');    
                        $Query = "UPDATE nomina.rolpagos SET idestado=0,idusmodifica=".$_SESSION['IDUSER'].",femodificacion= '".$fecha."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        self::$entityRolPagos->MyTransaction('COMMIT');
                        return $id;
                }

                public function EliminaRolPreProceso($IdRol=0)
                {      $idusuario = $_SESSION['IDUSER']; 
                       $this->Funciones->Query("Delete From nomina.rolpagos_preproceso Where idrol='$IdRol' and idusuario='$idusuario'");
                       
                }

                public function InsertaRolPreProceso($detalle)
                {      $idrolpago = $detalle['idrolpago'];
                       $idusuario = $_SESSION['IDUSER'];
                       $idempleado = $detalle['idempleado'];
                       $idrubro = $detalle['idrubro'];
                       $idmovmnto = $detalle['idmovmnto'];
                       $valor = $detalle['valor'];
                       $idsucursal = $detalle['idsucursal'];
                       $idplantilla = $detalle['idplantilla'];
                       
                       $Query = "INSERT INTO nomina.rolpagos_preproceso(idrol,idusuario,idempleado,idrubro,idmovmnto,valor,idsucursal,idplantilla) "; 
                       $Query.= "VALUES ('$idrolpago','$idusuario','$idempleado','$idrubro','$idmovmnto','$valor','$idsucursal','$idplantilla') ";
                       $this->Funciones->Query($Query);
                }

                public function ActualizaRolPreProceso($idRol)
                {      $idusuario = $_SESSION['IDUSER']; 
                       $this->Funciones->Query("Update nomina.rolpagos_preproceso Set idrol='$idRol' Where idrol=0 and idusuario='$idusuario'");
                }
               
        }          
?>                
