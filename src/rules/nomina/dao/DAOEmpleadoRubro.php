<?php
        require_once("src/base/DBDAO.php");

        class DAOEmpleadoRubro extends DBDAO
        {       private static $entityEmpleadoRubro;
                private $Funciones;
                private $Fecha;
              
                public function __construct()
                {       self::$entityEmpleadoRubro = DBDAO::getConnection();
                        $this->Funciones = self::$entityEmpleadoRubro->MyFunction();
                        $this->Fecha  = new DateControl();
                }
              
                function BuscarEmpleadoRubro($PrepareDQL)
                {       $JOIN = array_key_exists("id", $PrepareDQL) ?       " and p.id =".$PrepareDQL['id'] : "";
                        $JOIN.= array_key_exists("empleado", $PrepareDQL) ? " and p.idempleado =".$PrepareDQL['empleado'] : "";        
                        $JOIN.= array_key_exists("estado", $PrepareDQL) ?   " and (p.idestado in (".$PrepareDQL['estado'].")) " : "";
                        $JOIN.= array_key_exists("texto", $PrepareDQL) ?    " and (upper(m.nombre) LIKE '%".$PrepareDQL['texto']."%') " : "";
                        
                        $LIMT  = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
                        $DSQL = "SELECT 
                                    p.id, 
                                    p.idempleado, m.apellido||' '||m.nombre as empleado,
                                    p.idrubrorol, r.descripcion as rubro,
                                    p.valor, 
                                    p.fecreacion, 
                                    e.id as idestado, e.descripcion as txtestado,
                                    p.aportaiess
                                FROM 
                                    general.estado e, (nomina.empleado_rubro p 
                                    INNER JOIN general.empleado m ON p.idempleado=m.id 
                                    INNER JOIN nomina.rubros r ON p.idrubrorol=r.id) 
                                WHERE 
                                    p.idestado=e.id ".$JOIN. "
                                ORDER BY 
                                    p.id ".$LIMT;    
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
              
                public function Inserta($datos)
                {   try {
                            $estado   = $datos->estado;
                            $empleado = intval($datos->idempleado);
                            $rubro    = intval($datos->idrubrorol);
                            $valor    = floatval($datos->valor);
                            $aporta   = intval($datos->aportaIESS);
                            $fechai   = $this->Fecha->getNowDateTime('DATETIME');
                            $Query = "INSERT INTO nomina.empleado_rubro (idempleado,idrubrorol,valor,aportaiess,idestado,iduscreacion,fecreacion)"
                                   . "VALUES (".$empleado.",".$rubro.",".$valor.",'$aporta',".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        
                            $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                            return intval($id);
                        } 
                        catch (Exception $ex) 
                        {   $this->manejadorExcepciones($ex);
                            return 'No se pudo crear el rubro al empleado.';
                        }                     
                }
    
                public function Actualiza($datos)
                {   try { 
                            $id = $datos->id;
                            $estado   = $datos->estado;
                            $empleado = intval($datos->idempleado);
                            $rubro    = intval($datos->idrubrorol);
                            $valor    = floatval($datos->valor);
                            $aporta   = intval($datos->aportaIESS);
                            $fecham   = $this->Fecha->getNowDateTime('DATETIME');                        
                            
                            $Query = "UPDATE nomina.empleado_rubro "
                                   . "SET aportaiess=".$aporta.", idempleado= ".$empleado.",idrubrorol= ".$rubro.", valor=".$valor
                                   . ", idestado= ".$estado.", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                                   . "WHERE id= ".$id;
                            $this->Funciones->Query($Query);
                            return intval($datos->id);
                        }
                        catch (Exception $ex) 
                        {   $this->manejadorExcepciones($ex);
                            return 'No se pudo actualizar los datos.';
                        }
                }
    
                public function DesactivaEmpleadoRubro($id)
                {   try {
                            $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                            $Query = "UPDATE nomina.empleado_rubro SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                            $this->Funciones->Query($Query);
                            return intval($id);
                        } 
                        catch (Exception $ex) 
                        {   $this->manejadorExcepciones($ex);
                            return 'No se pudo eliminar el rubro.';
                        }
                }
        }
