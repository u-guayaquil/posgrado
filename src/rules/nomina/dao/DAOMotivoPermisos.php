<?php
        require_once("src/base/DBDAO.php");

        class DAOMotivoPermisos extends DBDAO
        {     private static $entityMotivoPermisos;
              private $Funciones;
              
              public function __construct()
              {      self::$entityMotivoPermisos = DBDAO::getConnection();
                     $this->Funciones = self::$entityMotivoPermisos->MyFunction();
              }
              
              function BuscarMotivoPermisos()
              {      $DSQL = "SELECT id, descripcion "
                           . "FROM nomina.empleado_motivo_permisos "
                           . "ORDER BY descripcion";           
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
        }
