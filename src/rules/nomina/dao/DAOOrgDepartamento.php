<?php
        require_once("src/base/DAO.php");
        use Doctrine\ORM\Query\Expr\Join;

        class DAOOrgDepartamento extends DAO
        {     private $entityOrgDepartamento;
              private $padre = array();
              
              public function __construct()
              {      $this->entityOrgDepartamento = $this->getEntityManager()->getRepository('OrgDepartamento');
              }
    
              public function ObtenerOrgDepartamentoByID($id)
              {      return $this->entityOrgDepartamento->findOneById($id);
              }

              public function ObtenerOrgDepartamentoByDescripcion($PrepareDQL)
              {      $DSQL = "SELECT t.id, t.id_organigrama, t.id_departamento, t.id_departamento_padre"
                          . ", o.version as organigrama "
                          . "FROM OrgDepartamento t, Organigrama o"
                          . "WHERE t.id_organigrama=o.id and upper(t.descripcion) "
                          . "LIKE :Texto ORDER BY t.id";
                  
                     $Query = $this->getEntityManager()->createQuery($DSQL);
                     $Query->setParameter('Texto','%'.$PrepareDQL['texto'].'%');
                     $Query->setMaxResults($PrepareDQL['limite']); 
                     return $Query->getResult();
              }
              
              

              public function ObtenerOrgDepartamentoByRango()
              {         $OrgDepartamento = $this->getEntityManager()->getRepository(OrgDepartamento::class);
                        $QBuild = $OrgDepartamento->createQueryBuilder("t");
                        $Query  = $QBuild->select("t.id, t.id_organigrama, a.idestado"
                                                . ",o.version as organigrama")
                                         ->innerJoin(Organigrama::class,'o',Join::WITH,'t.id_organigrama = o.id')
                                         ->orderBy("t.id", "ASC")   
                                         ->getQuery();
                        return $Query->getResult();                     
              }
              
              public function ObtenerObjetoporDepartamento($DepartamentoPadre)
              {         $OrgDepartamento = $this->getEntityManager()->getRepository(OrgDepartamento::class);
                        $QBuild = $OrgDepartamento->createQueryBuilder("t");
                        $Query  = $QBuild->where('t.idDepartamentoPadre= :Departamento')
                                         ->setParameter('Departamento',$DepartamentoPadre)
                                         ->getQuery();
                        return $Query->getArrayResult();                     
              }
              
              public function InsertaOrgDepartamento(OrgDepartamento $organigrama)
              {     try{
                        $Inserta = $this->getEntityManager();
                        $Inserta->persist($organigrama);
                        $Inserta->flush(); 
                        return array(true, $organigrama->getId());
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo crear la relacion..');
                    }                     
              }
              
              public function EliminaOrgDepartamento(OrgDepartamento $organigrama)
                {      
                        try{
                         $Elimina = $this->getEntityManager();
                         $Elimina->remove($organigrama);
                         $Elimina->flush();
                         return array(true, $organigrama->getId());
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return array(false, 'No se pudo eliminar la Relacion..');
                        }
                }
              
              public function ObtenerOrgDepartamentoCombo($IdArray)
              {      $QueryBld = $this->entityOrgDepartamento->createQueryBuilder('c');
                     $WhereBld = $QueryBld->expr()->in('c.idestado', ':Arreglo');
                     $QueryDQL = $QueryBld->where($WhereBld)->orderBy('c.id', 'ASC')
                                       ->setParameter('Arreglo',$IdArray)
                                       ->getQuery();
                     return $QueryDQL->getArrayResult();
              }
                    
              public function ObtenerDepartamentosPadres()
              {      $QueryBld = $this->entityOrgDepartamento->createQueryBuilder('o');
                     $QueryBld->add('select','d.id,d.descripcion');
                     $QueryBld->innerJoin('o.Departamentos', 'd');
                     $QueryDQL = $QueryBld->getQuery();
                     return $QueryDQL->getArrayResult();
              }    
                    
              public function BuscarOrgDepartamentoPorRango()
              {                         
                     $QueryBld = $this->entityOrgDepartamento->createQueryBuilder('o');
                     $QueryBld->add('select','max(o.fehasta)');
                     $QueryDQL = $QueryBld->getQuery();
                     return $QueryDQL->getArrayResult();
              }
              
              public function TraerDatosDepartamentoGeneral($id,$Organigrama)
              {      
                     $QueryBld = $this->entityOrgDepartamento->createQueryBuilder('o');
                     $QueryBld->add('select','o.idDepartamento,d.descripcion as departamento'
                                  . ' ,r.version as organigrama, o.idDepartamentoPadre, o.idOrganigrama'
                                  . ' ,t.descripcion as departamentopadre,o.id,t.id as iddepartamentopadre');
                     $QueryBld->innerJoin('o.Departamentos', 'd');
                     $QueryBld->innerJoin('o.Organigramas', 'r')
                              ->leftJoin(Departamento::class,'t',Join::WITH,'o.idDepartamentoPadre = t.id');
                     if($Organigrama!==0){
                        $QueryBld->where('o.idDepartamentoPadre = :Padre and r.id = :Organigrama')
                                 ->setParameter('Organigrama',$Organigrama)
                                 ->setParameter('Padre',$id); 
                     }else{
                        $QueryBld->where('o.idDepartamentoPadre = :Padre')
                                 ->setParameter('Padre',$id);
                     }        
                     $QueryBld->orderBy("o.idDepartamentoPadre");
                     $QueryDQL = $QueryBld->getQuery();
                     return $QueryDQL->getArrayResult();
              }
              
              function ObtenerOrgDepartamentoGeneral()
                  {     
                     $DSQL = "SELECT t.id, t.id_organigrama, t.id_departamento, t.id_departamento_padre"
                          . ", o.version as organigrama "
                          . "FROM OrgDepartamento t, Departamento d"
                          . "WHERE t.id_departamento=d.id";
                  
                     $Query = $this->getEntityManager()->createQuery($DSQL);
                     return $Query->getResult();
                  }
           
              function BuscarOrgDepartamentoByDescripcion($prepareDQL)
              {     $LIKE = ($prepareDQL['texto'] == "" ? "" : "and upper(a.descripcion) LIKE :Texto ");        
                    $DAOF = ($LIKE != "" ? "ObtenerOrgDepartamentoByDescripcion" : "ObtenerOrgDepartamentoByRango");
                    return $this->{$DAOF}($prepareDQL);
              }
        }
