<?php
        require_once("src/base/DBDAO.php");

        class DAOFrecuencia extends DBDAO
        {   private static $entityFrecuencia;
            private $Funciones;
              
            public function __construct()
            {      self::$entityFrecuencia = DBDAO::getConnection();
                     $this->Funciones = self::$entityFrecuencia->MyFunction();
            }
              
            function BuscarFrecuencia()
            {       $DSQL = "SELECT id, descripcion FROM general.frecuencia ORDER BY id";           
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
        }
