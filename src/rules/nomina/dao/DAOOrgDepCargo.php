<?php
        require_once("src/base/DAO.php");
        use Doctrine\ORM\Query\Expr\Join;

        class DAOOrgDepCargo extends DAO
        {     private $entityOrgDepCargo;
              
              public function __construct()
              {      $this->entityOrgDepCargo = $this->getEntityManager()->getRepository('OrgDepCargo');
              }
    
              public function ObtenerOrgDepCargoByID($id)
              {      return $this->entityOrgDepCargo->findOneById($id);
              }

              public function ObtenerOrgDepCargoByDescripcion($PrepareDQL)
              {      $DSQL = "SELECT t.id, t.id_organigrama, t.id_departamento, t.id_departamento_padre"
                          . ", o.version as organigrama "
                          . "FROM OrgDepCargo t, Organigrama o"
                          . "WHERE t.id_organigrama=o.id and upper(t.descripcion) "
                          . "LIKE :Texto ORDER BY t.id";
                  
                     $Query = $this->getEntityManager()->createQuery($DSQL);
                     $Query->setParameter('Texto','%'.$PrepareDQL['texto'].'%');
                     $Query->setMaxResults($PrepareDQL['limite']); 
                     return $Query->getResult();
              }
              
              

              public function ObtenerOrgDepCargoByRango()
              {         $OrgDepCargo = $this->getEntityManager()->getRepository(OrgDepCargo::class);
                        $QBuild = $OrgDepCargo->createQueryBuilder("t");
                        $Query  = $QBuild->select("t.id, t.id_organigrama, a.idestado"
                                                . ",o.version as organigrama")
                                         ->innerJoin(OrgDepartamento::class,'o',Join::WITH,'t.id_organigrama = o.id')
                                         ->orderBy("t.id", "ASC")   
                                         ->getQuery();
                        return $Query->getResult();                     
              }
              
              public function ObtenerObjetoporDepartamento($DepartamentoPadre)
              {         $OrgDepCargo = $this->getEntityManager()->getRepository(OrgDepCargo::class);
                        $QBuild = $OrgDepCargo->createQueryBuilder("t");
                        $Query  = $QBuild->where('t.idDepartamentoPadre= :Departamento')
                                         ->setParameter('Departamento',$DepartamentoPadre)
                                         ->getQuery();
                        return $Query->getArrayResult();                     
              }
              
              public function InsertaOrgDepCargo(OrgDepCargo $organigrama)
              {     try{
                        $Inserta = $this->getEntityManager();
                        $Inserta->persist($organigrama);
                        $Inserta->flush(); 
                        return array(true, $organigrama->getId());
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo crear la relacion..');
                    }                     
              }
              
              public function EliminaOrgDepCargo(OrgDepCargo $organigrama)
                {      
                        try{
                         $Elimina = $this->getEntityManager();
                         $Elimina->remove($organigrama);
                         $Elimina->flush();
                         return array(true, $organigrama->getId());
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return array(false, 'No se pudo eliminar la Relacion..');
                        }
                }
              
              public function ObtenerOrgDepCargoCombo($IdArray)
              {      $QueryBld = $this->entityOrgDepCargo->createQueryBuilder('c');
                     $WhereBld = $QueryBld->expr()->in('c.idestado', ':Arreglo');
                     $QueryDQL = $QueryBld->where($WhereBld)->orderBy('c.id', 'ASC')
                                       ->setParameter('Arreglo',$IdArray)
                                       ->getQuery();
                     return $QueryDQL->getArrayResult();
              }
                    
              public function ObtenerCargosPadres()
              {      $QueryBld = $this->entityOrgDepCargo->createQueryBuilder('o');
                     $QueryBld->add('select','c.id,c.descripcion');
                     $QueryBld->innerJoin('o.Cargos', 'c');
                     $QueryDQL = $QueryBld->getQuery();
                     return $QueryDQL->getArrayResult();
              }    
                    
              public function BuscarOrgDepCargoPorRango()
              {                         
                     $QueryBld = $this->entityOrgDepCargo->createQueryBuilder('o');
                     $QueryBld->add('select','max(o.fehasta)');
                     $QueryDQL = $QueryBld->getQuery();
                     return $QueryDQL->getArrayResult();
              }
              
              public function TraerDatosDepCargoGeneral($id,$Organigrama)
              {      
                     $QueryBld = $this->entityOrgDepCargo->createQueryBuilder('o');
                     $QueryBld->add('select','g.id as idCargo,g.descripcion as Cargos ,r.id as OrgDepartamento'
                                  . ' ,c.id as idCargoPadre,c.descripcion as CargoPadre,o.id');
                     $QueryBld->innerJoin('o.Cargos', 'g')
                              ->innerJoin('o.OrgDepartamento', 'r')
                              ->leftJoin('o.CargosPadres','c');
                     if($Organigrama!==0){
                        $QueryBld->where('o.CargosPadre = :Padre and o.OrgDepartamento = :OrgDep')
                                 ->setParameter('OrgDep',$Organigrama)
                                 ->setParameter('Padre',$id); 
                     }else{
                        $QueryBld->where('o.CargosPadre = :Padre')
                                 ->setParameter('Padre',$id);
                     }        
                     $QueryBld->orderBy("o.CargosPadre");
                     $QueryDQL = $QueryBld->getQuery();
                     return $QueryDQL->getArrayResult();
              }
              
              function ObtenerOrgDepCargoGeneral()
                  {     
                     $DSQL = "SELECT t.id, t.id_organigrama, t.id_departamento, t.id_departamento_padre"
                          . ", o.version as organigrama "
                          . "FROM OrgDepCargo t, Departamento d"
                          . "WHERE t.id_departamento=d.id";
                  
                     $Query = $this->getEntityManager()->createQuery($DSQL);
                     return $Query->getResult();
                  }
           
              function BuscarOrgDepCargoByDescripcion($prepareDQL)
              {     $LIKE = ($prepareDQL['texto'] == "" ? "" : "and upper(a.descripcion) LIKE :Texto ");        
                    $DAOF = ($LIKE != "" ? "ObtenerOrgDepCargoByDescripcion" : "ObtenerOrgDepCargoByRango");
                    return $this->{$DAOF}($prepareDQL);
              }
        }
