<?php
        require_once("src/base/DBDAO.php");

        class DAOEmpleadoCargas extends DBDAO
        {     private static $entityEmpleadoCargas;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityEmpleadoCargas = DBDAO::getConnection();
                     $this->Funciones = self::$entityEmpleadoCargas->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
            function ObtenerEmpleadoCargas($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ?       " and c.id =".$PrepareDQL['id']: "";
                    $JOIN.= array_key_exists("empleado", $PrepareDQL) ? " and c.idempleado =".$PrepareDQL['empleado']: "";
                    $JOIN.= array_key_exists("estado", $PrepareDQL) ?   " and c.idestado in (".$PrepareDQL['estado'].")" : "";
                    $LIKE = array_key_exists("texto", $PrepareDQL) ?    " and upper(c.nombre) LIKE '%".$PrepareDQL['texto']."%'": "";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ?  " limit ".$PrepareDQL['limite']: "";
                    
                    $DSQL = "SELECT 
                                    c.id,
                                    c.idempleado,
                                    c.idparentesco,
                                    c.nombre,
                                    c.sexo,
                                    c.fenacimiento,
                                    m.apellido||' '||m.nombre as empleado, 
                                    c.fecreacion, 
                                    e.id as idestado, e.descripcion as txtestado,
                                    p.descripcion as parentesco,
                                    c.documentos
                            FROM    general.estado e, (nomina.empleado_cargas c 
                                    INNER JOIN general.empleado m ON c.idempleado=m.id 
                                    INNER JOIN general.parentesco p ON c.idparentesco=p.id) 
                            WHERE 
                                    c.idestado=e.id $JOIN.$LIKE  
                            ORDER BY c.id ".$LIMT;
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            public function Inserta($datos)
            {   try {   
                        $FileManager = new File();             
                        $FileFinal = "";
                        $FileParts = $FileManager->getFileParts(trim($datos->txdocumento));
                        if ($FileParts[0])
                        {   $Empleados = str_pad(intval($datos->idempleado),4,'0',STR_PAD_LEFT);
                            $Nacimento = str_replace("/", "", $datos->fenacimiento); 
                            $FileFinal = "src/upload/empleados/documentos/".$Empleados."_".$Nacimento.".".$FileParts[3];
                            $FileManager->MoveFile(trim($datos->txdocumento), $FileFinal);
                        }
                        
                        $empleado    = intval($datos->idempleado);
                        $nombre      = strtoupper(trim($datos->nombrepariente));
                        $parentesco  = intval($datos->parentesco);
                        $sexo        = trim($datos->sexo);
                        $fenacimiento= $datos->fenacimiento;
                        $estado      = $datos->estado;
                        $fecha       = $this->Fecha->getNowDateTime('DATETIME');
                        $documento   = $FileFinal; 
                        $usuario     = $_SESSION['IDUSER'];
                        
                        $Query = "INSERT INTO nomina.empleado_cargas (idempleado,nombre,sexo,fenacimiento,idparentesco,documentos,idestado,iduscreacion,fecreacion) "
                               . "VALUES ('$empleado','$nombre','$sexo','$fenacimiento','$parentesco','$documento','$estado','$usuario','$fecha') returning id";

                        $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                        return intval($id);
                    } 
                    catch (Exception $ex) 
                    {   $this->manejadorExcepciones($ex);
                        return 'No se pudo crear la carga familiar.';
                    }                     
            }
    
            public function Actualiza($datos)
            {   try {
                        $FileManag = new File();             
                        $FileFinal = trim($datos->txdocumento);
                        $FileParts = $FileManag->getFileParts($FileFinal);
                        if ($FileParts[0])
                        {   if (file_exists($FileFinal))
                            {   $Empleados = str_pad(intval($datos->idempleado),4,'0',STR_PAD_LEFT);
                                $Nacimento = str_replace("/", "", $datos->fenacimiento); 
                                $FileFinal = "src/upload/empleados/documentos/".$Empleados."_".$Nacimento.".".$FileParts[3];
                                $FileManag->MoveFile(trim($datos->txdocumento), $FileFinal);
                            }    
                        }
                
                        $id          = $datos->id;
                        $nombre      = strtoupper($datos->nombrepariente);
                        $empleado    = intval($datos->idempleado);
                        $parentesco  = intval($datos->parentesco);
                        $sexo        = $datos->sexo;
                        $fenacimiento= $datos->fenacimiento;
                        $documento   = $FileFinal;
                        $estado      = $datos->estado;
                        $fecha       = $this->Fecha->getNowDateTime('DATETIME');                        
                        $usuario     = $_SESSION['IDUSER'];
                                                
                        $Query = "UPDATE nomina.empleado_cargas 
                                  SET 
                                  idempleado  = '$empleado',
                                  idparentesco= '$parentesco', 
                                  nombre = '$nombre',
                                  sexo   = '$sexo',
                                  fenacimiento = '$fenacimiento',
                                  documentos = '$documento',    
                                  idestado = '$estado', 
                                  idusmodifica = '$usuario',
                                  femodificacion= '$fecha' 
                                  WHERE id= '$id'";
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
                    } 
                    catch (Exception $ex) 
                    {   $this->manejadorExcepciones($ex);
                        return 'No se pudo actualizar los Datos.';
                    }
            }
    
            public function DesactivaEmpleadoCargas($id)
            {   try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.empleado_cargas SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } 
                    catch (Exception $ex) 
                    {   $this->manejadorExcepciones($ex);
                        return 'No se pudo eliminar la carga familiar.';
                    }
            }
        }
