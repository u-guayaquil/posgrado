<?php
        require_once("src/base/DBDAO.php");

        class DAOOrganigrama extends DBDAO
        {     private static $entityOrganigrama;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityOrganigrama = DBDAO::getConnection();
                     $this->Funciones = self::$entityOrganigrama->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarOrganigramaByDescripcion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and o.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(o.version) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and o.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT o.id, o.version, o.fecreacion,to_char(o.fedesde, 'DD/MM/YYYY') as fedesde"
                           . ",to_char(o.fehasta, 'DD/MM/YYYY') as fehasta, e.id as idestado"
                           . ", e.descripcion as txtestado, o.comentario "
                           . " FROM nomina.organigrama o, general.Estado e "
                           . " WHERE o.idestado=e.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY o.version".$LIM;            
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaOrganigrama($datos)
              {    try{
                        $estado = $datos->estado;
                        $fedesde = ($datos->fedesde);
                        if($datos->fehasta != '--'){  $fehasta ="to_timestamp('".$fehasta."', 'YYYY-MM-DD')";}else{ $fehasta ='1900-01-01 00:00:00'; }
                        $version = strtoupper($datos->version);
                        $comentario = strtoupper($datos->comentario);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO nomina.Organigrama (fedesde,fehasta,version,comentario,idestado,iduscreacion,fecreacion)"
                               . "VALUES (to_timestamp('".$fedesde."','YYYY-MM-DD'), '".$fehasta."'"
                               . ",'".$version."','".$comentario."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id"; 
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo crear el Organigrama..');
                    }                     
              }
    
              public function ActualizaOrganigrama($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $fedesde = ($datos->fedesde);
                        if($datos->fehasta != '--'){  $fehasta ="to_timestamp('".$fehasta."', 'YYYY-MM-DD')";  }else{  $fehasta ='1900-01-01 00:00:00';  } 
                        $version = strtoupper($datos->version);
                        $comentario = strtoupper($datos->comentario);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.Organigrama "
                               . "SET fedesde = to_timestamp('".$fedesde."','YYYY-MM-DD'), fehasta= '".$fehasta."'"
                                . ", version= '".$version."', comentario= '".$comentario."', idestado= ".$estado.""
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Actualizar los Datos..');
                    }
              }
    
              public function DesactivaOrganigrama($id)
              {      try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE nomina.organigrama SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                        } catch (Exception $ex) {
                            $this->manejadorExcepciones($ex);
                            return 'No se pudo Eliminar el Cargo..';
                        }
              }
              
              public function ObtenerOrganigramaVigente()
              {      $QueryBld = "select id,version from nomina.organigrama where idestado = 1 order by id desc limit 1";     
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($QueryBld));
              }
                    
              public function ObtenerFechaOrganigramaVigente($id)
              {     if($id==0){
                       $Cond = "";                       
                    }else{
                       $Cond = " and id<>".$id."";
                    } 
                    $QueryBld = "select fehasta from nomina.organigrama where idestado = 1 ".$Cond." order by id desc limit 1";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($QueryBld));
              }


        }
