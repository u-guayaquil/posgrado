<?php
        require_once("src/base/DBDAO.php");

        class DAOPlantillaRol extends DBDAO
        {   
            private static $entityPlantillaRol;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {       self::$entityPlantillaRol = DBDAO::getConnection();
                    $this->Funciones = self::$entityPlantillaRol->MyFunction();
                    $this->Fecha  = new DateControl();
            }
              
            function ObtenerPlantillaRol($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ?     " and p.id=".$PrepareDQL['id'] : "";
                    $JOIN = array_key_exists("frecuencia", $PrepareDQL) ? " and p.idfrecuencia=".$PrepareDQL['frecuencia'] : "";
                    $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ?  " and upper(p.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " limit ".$PrepareDQL['limite']: "";
            
                    $DSQL = "SELECT ";
                    $DSQL.= "p.id,p.descripcion, ";
                    $DSQL.= "e.id as idestado,e.descripcion as txtestado, ";
                    $DSQL.= "p.idfrecuencia, f.descripcion as frecuencia, ";
                    $DSQL.= "p.fecreacion ";
                    $DSQL.= "FROM general.frecuencia f, general.Estado e, nomina.plantilla_rol p ";                             
                    $DSQL.= "WHERE p.idfrecuencia=f.id and p.idestado=e.id ".$JOIN;
                    $DSQL.= "ORDER BY p.id ".$LIMT;                
                    
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            function ObtenerPlantillaRolDetalle($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ? " and p.idplantilla=".$PrepareDQL['id'] : "";
                    $DSQL = "SELECT ";
                    $DSQL.= "p.id,p.idplantilla, ";
                    $DSQL.= "p.idrubrorol,r.descripcion as rubro, ";
                    $DSQL.= "p.idcuentadebe,d.descripcion as cuentadebe, ";
                    $DSQL.= "p.idcuentahaber,h.descripcion as cuentahaber, ";
                    $DSQL.= "p.txformula as formula ";
                    $DSQL.= "FROM nomina.plantilla_rol_dt p, nomina.rubros r, general.planctas d, general.planctas h ";
                    $DSQL.= "WHERE p.idcuentadebe=d.id and p.idcuentahaber=h.id and r.id=p.idrubrorol ".$JOIN;
                    $DSQL.= "ORDER BY p.id ";       
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
              
            public function Inserta($datos,$detalle)
            {       $ServicioFormula = new ServicioFormula();   
                
                    $fecha = $this->Fecha->getNowDateTime('DATETIME');
                    $descripcion = strtoupper(trim($datos->descripcion));
                    
                    self::$entityPlantillaRol->MyTransaction('BEGIN');
                    $Query = "INSERT INTO nomina.plantilla_rol (descripcion,idfrecuencia,idestado,iduscreacion,fecreacion) ";
                    $Query.= "VALUES ('$descripcion','$datos->frecuencia','$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id ";
 
                    $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                    if (is_numeric($id))
                    {   foreach ($detalle as $campos)
                        {       $Query = "INSERT INTO nomina.plantilla_rol_dt (idplantilla,idrubrorol,idcuentadebe,idcuentahaber,txformula) ";
                                $Query.= "VALUES ('$id','$campos->IdRubro','$campos->IdDebe','$campos->IdHaber','$campos->TxFormula') returning id "; 
                                $iddetalle = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                            
                                $Formula = $ServicioFormula->AnalizaFormula($campos->TxFormula);
                                if ($Formula[0]){
                                    $this->InsertaDetalleFormula($iddetalle,$Formula[2]);
                                }else{
                                    self::$entityPlantillaRol->MyTransaction('ROLLBACK');
                                    return "No se guardo la informacion: ".$Formula[2];
                                }
                        }   
                        self::$entityPlantillaRol->MyTransaction('COMMIT');
                        return intval($id);
                    }
                    else
                    {   self::$entityPlantillaRol->MyTransaction('ROLLBACK');
                        return "No se guardo la informacion.";
                    }
            }
              
            private function InsertaDetalleFormula($id,$datos)
            {       foreach($datos as $parte)
                    {       $fnName = trim($parte[0]);
                            $fnPrms = trim($parte[1]);
                            $Query = "Insert Into nomina.plantilla_rol_dt_formula(idplantillaroldt,fnname,fnparam) ";
                            $Query.= "Values('$id','$fnName','$fnPrms')";
                            $this->Funciones->Query($Query);
                    }
            }       

            public function Actualiza($datos,$detalle)
            {       $ServicioFormula = new ServicioFormula();   
            
                    $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $descripcion = strtoupper(trim($datos->descripcion));
                    
                    self::$entityPlantillaRol->MyTransaction('BEGIN');    
                    $Query = "UPDATE nomina.plantilla_rol Set ";
                    $Query.= "descripcion='$descripcion', ";
                    $Query.= "idfrecuencia='$datos->frecuencia', ";
                    $Query.= "idestado='$datos->estado', ";
                    $Query.= "idusmodifica=".$_SESSION['IDUSER'].", ";
                    $Query.= "femodificacion='$fecha' ";
                    $Query.= "WHERE id='$datos->idplantilla'";
                    $this->Funciones->Query($Query);
                    
                    $Query = "DELETE FROM nomina.plantilla_rol_dt WHERE idplantilla='$datos->idplantilla'";
                    $this->Funciones->Query($Query);

                    foreach ($detalle as $campos)
                    {       $Query = "INSERT INTO nomina.plantilla_rol_dt (idplantilla,idrubrorol,idcuentadebe,idcuentahaber,txformula) ";
                            $Query.= "VALUES ('$datos->idplantilla','$campos->IdRubro','$campos->IdDebe','$campos->IdHaber','$campos->TxFormula') returning id "; 
                            $iddetalle = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                            
                            $Formula = $ServicioFormula->AnalizaFormula($campos->TxFormula);
                            if ($Formula[0]){
                                $this->InsertaDetalleFormula($iddetalle,$Formula[2]);
                            }else{
                                self::$entityPlantillaRol->MyTransaction('ROLLBACK');
                                return "No se guardo la informacion: ".$Formula[2];
                            }
                    }   
                    self::$entityPlantillaRol->MyTransaction('COMMIT');
                    return $datos->idplantilla;
            }
    
            public function DesactivaPlantillaRol($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
            
                    self::$entityPlantillaRol->MyTransaction('BEGIN');    
                    $Query = "UPDATE nomina.plantilla_rol SET idestado=0,idusmodifica=".$_SESSION['IDUSER'].",femodificacion= '".$fecha."' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    self::$entityPlantillaRol->MyTransaction('COMMIT');
                    return $id;
            }

            public function ObtenerPlantillaRolEmpleado($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ? "           and w.id=".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("sucursal", $PrepareDQL) ? "     and w.idsucursal =".$PrepareDQL['sucursal'] : "";        
                    $JOIN.= array_key_exists("plantilla", $PrepareDQL) ? "    and w.idplantilla=".$PrepareDQL['plantilla'] : "";
                    $JOIN.= array_key_exists("frecuencia", $PrepareDQL) ? "   and x.idfrecuencia=".$PrepareDQL['frecuencia'] : "";
                    $JOIN.= array_key_exists("departamento", $PrepareDQL) ? " and w.iddepartamento=".$PrepareDQL['departamento'] : "";
                                        
                    $Query = "Select ";
                    $Query.= "w.id, w.idsucursal, w.idplantilla, x.descripcion as plantilla, w.apellido, w.nombre ";
                    $Query.= "From general.empleado as w, nomina.plantilla_rol as x ";
                    $Query.= "Where "; 
                    $Query.= "w.idplantilla=x.id and x.idestado=1 ".$JOIN;
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
            }
            
            public function ObtenerPlantillaRolFormula($idPlantilla)
            {       $Query = "Select x.id, y.id as idrubro, y.descripcion, y.idmovimiento, x.txformula ";
                    $Query.= "From ";
                    $Query.= "nomina.plantilla_rol_dt as x, nomina.rubros as y ";
                    $Query.= "Where x.idrubrorol = y.id and x.idplantilla='$idPlantilla' and y.idestado=1 ";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
            }
            
            public function ObtenerPlantillaRolDecoder($idFormula)
            {       $Query = "Select x.fnname,x.fnparam ";
                    $Query.= "From ";
                    $Query.= "nomina.plantilla_rol_dt_formula as x ";
                    $Query.= "Where x.idplantillaroldt = '$idFormula'"; 
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($Query));
            }
            
            
            
        }    
