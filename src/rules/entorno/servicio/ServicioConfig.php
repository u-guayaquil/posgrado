<?php
    
    require_once("src/rules/entorno/dao/DAOConfig.php");    

    class ServicioConfig 
    {       private $DAOConfig;
            private $EstadoConfig;
            
            function __construct($Modulo="",$Entorno="") 
            {       $this->DAOConfig = new DAOConfig();
                    
                    if ($Modulo!="" && $Entorno!="")
                        $this->ObtenerConfiguracion($Modulo,$Entorno);
                    else
                        $this->EstadoConfig = array(false,"Falta información para inicio de servicio.");
            }
                       
            private function PrepararDatos($Datos)
            {       $Datos['Db']      = str_replace(' ','',$Datos['Db']); 
                    $Datos['Usuario'] = str_replace(' ','',$Datos['Usuario']); 
                    $Datos['Password']= str_replace(' ','',$Datos['Password']);
                    $Datos['Host']    = strtolower($Datos['Host']);
                    $Datos['Port']    = str_replace(' ','',$Datos['Port']);
                    $Datos['Driver']  = str_replace(' ','',$Datos['Driver']);
                    return $Datos;
            }
            
            function CrearConfiguracion($Form)
            {       $Configuracion = $this->PrepararDatos($Form);
                
                    if (empty($Configuracion['Entorno']) || empty($Configuracion['Usuario']) || empty($Configuracion['Password']) || empty($Configuracion['Db']) || empty($Configuracion['Host']) || empty($Configuracion['Port']) || empty($Configuracion['Driver']))
                        $this->EstadoConfig = array(false,"Debe ingresar todos los datos.");
                    else      
                    {   if (!$this->DAOConfig->Existe($Configuracion))
                        {   $this->EstadoConfig = array(true,"La configuración se guardó correctamente.");
                            $this->DAOConfig->Guardar($Configuracion);
                        }    
                        else 
                        $this->EstadoConfig = array(false,"La configuración ya existe."); 
                    }        
            }
            
            function ObtenerConfiguraciones()
            {       return $this->DAOConfig->Consultar();   
            }
            
            private function ObtenerConfiguracion($Modulo,$Entorno)
            {       $Conexion = $this->DAOConfig->Buscar($Modulo,$Entorno);
                    if ($Conexion[0])
                    {   Session::setValue("Conexion", json_encode($Conexion[1])); 
                        $this->EstadoConfig = array(true,"Configuración Disponible");
                    }
                    else
                    $this->EstadoConfig = array(false,$Conexion[1]);
            }
            
            function EstadoServicio()
            {       return $this->EstadoConfig;
            }
            
    }
?>