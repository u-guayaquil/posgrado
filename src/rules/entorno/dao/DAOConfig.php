<?php
        require_once ("src/libs/clases/Security.php");
        
        class DAOConfig
        {       
                private $Security;
                
                public function __construct()
                {      $this->Security = new Security();  
                }
                
                function Consultar()
                {       $Recordset = array();
                        if ($this->Security->SecurityFileExist())
                        {   $this->Security->setFileName($this->Security->SecurityFileName());
                            $this->Security->openFile("r");
                            while(!$this->Security->endOfFile())
                            {       $fields = explode("|", $this->Security->getDataFile());
                                    if (count($fields)>1)
                                    {   $Recordset[]= array($fields[0],
                                                            $fields[1],
                                                            $this->Security->dencrypt($fields[2]),
                                                            $this->Security->dencrypt($fields[3]),
                                                            $this->Security->dencrypt($fields[4]),
                                                            $this->Security->dencrypt($fields[5]),
                                                            $this->Security->dencrypt($fields[6]),
                                                            $this->Security->dencrypt($fields[7]));
                                    }
                            }
                            $this->Security->closFile();
                            return $Recordset;
                        }
                        return array();
                }
 
                function Existe($Configuracion)
                {       if ($this->Security->SecurityFileExist())
                        {   $this->Security->setFileName($this->Security->SecurityFileName());
                            $this->Security->openFile("r");
                            while(!$this->Security->endOfFile())
                            {       $fields = explode("|", $this->Security->getDataFile());
                                    if (count($fields)>1)
                                    {   if ($Configuracion['Entorno']==$fields[0] &&
                                            $Configuracion['Modulo']==$fields[1] &&
                                            $Configuracion['Driver']==$this->Security->dencrypt($fields[7]))
                                            return true;
                                    }
                            }
                            $this->Security->closFile();
                        }
                        return false;
                }                                
                
                function Guardar($Configuracion)
                {       $salto = chr(10);
                        $tocks = "|";
                        $this->Security->setFileName($this->Security->SecurityFileName());
                        $txcifrado = $this->Security->encrypt($Configuracion['Usuario']).$tocks;
                        $txcifrado = $txcifrado.$this->Security->encrypt($Configuracion['Password']).$tocks;
                        $txcifrado = $txcifrado.$this->Security->encrypt($Configuracion['Db']).$tocks;
                        $txcifrado = $txcifrado.$this->Security->encrypt($Configuracion['Host']).$tocks;
                        $txcifrado = $txcifrado.$this->Security->encrypt($Configuracion['Port']).$tocks;
                        $txcifrado = $txcifrado.$this->Security->encrypt($Configuracion['Driver']).$salto;
                        $this->Security->setContenido($Configuracion['Entorno'].$tocks.$Configuracion['Modulo'].$tocks.$txcifrado);
                        return $this->Security->createFile();
                }               

                function Buscar($Modulo,$Entorno)
                {       if ($this->Security->SecurityFileExist())
                        {   $this->Security->setFileName($this->Security->SecurityFileName());
                            $this->Security->openFile("r");
                            while(!$this->Security->endOfFile())
                            {       $fields = explode("|", $this->Security->getDataFile());
                                    if (count($fields)>1)
                                    {   if ($fields[0]==$Entorno && $fields[1]==$Modulo)
                                            return array(1,array("Env" => $Entorno,
                                                                 "Mod" => $Modulo, 
                                                                 "Usr" => $fields[2],
                                                                 "Pss" => $fields[3],
                                                                 "Dbs" => $fields[4],
                                                                 "Hst" => $fields[5],
                                                                 "Prt" => $fields[6],
                                                                 "Drv" => $fields[7]));
                                    }
                            }
                            $this->Security->closFile();
                            return array(0,"Configuración de acceso no disponible en certificado."); 
                        }
                        return array(0,"Certificado de acceso no disponible."); 
                }  
                
        }

?>
