<?php

        /**
         *
         * @Table(name="configuracion_mail", schema="sistema")
         * @Entity
         */
        class ConfiguracionMail
        {
    
            /**
             * @Column(name="operacion", type="string", length=20, nullable=false)
             * @Id
             * @GeneratedValue
             */
            private $operacion;

            /**
             * @Column(name="usuario", type="string", length=50, nullable=true)
             */
            private $usuario;

            /**
             * @Column(name="password", type="string", length=50, nullable=true)
             */
            private $password;

            /**
             * @Column(name="smtp", type="string", length=50, nullable=true)
             */
            private $smtp;

            /**
             * @Column(name="puerto", type="string", length=4, nullable=true)
             */
            private $puerto;

            /**
             * @Column(name="correo", type="string", length=50, nullable=true)
             */
            private $correo;

            public function getOperacion()
            {      return $this->operacion;
            }

            public function setUsuario($usuario)
            {      $this->usuario = $usuario;
            }

            public function getUsuario()
            {      return $this->usuario;
            }

            public function setPassword($password)
            {      $this->password = $password;
            }

            public function getPassword()
            {      return $this->password;
            }

            public function setSmtp($smtp)
            {      $this->smtp = $smtp;
            }

            public function getSmtp()
            {      return $this->smtp;
            }

            public function setPuerto($puerto)
            {      $this->puerto = $puerto;
            }

            public function getPuerto()
            {      return $this->puerto;
            }

            public function setCorreo($correo)
            {      $this->correo = $correo;
            }

            public function getCorreo()
            {      return $this->correo;
            }
        }
?>
