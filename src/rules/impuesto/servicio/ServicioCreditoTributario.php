<?php
            require_once("src/rules/impuesto/dao/DAOCreditoTributario.php");
            require_once("src/rules/impuesto/entidad/CreditoTributario.php");
            
            class ServicioCreditoTributario
            {     private $DAOCreditoTributario;
     
                  function __construct()
                  {        $this->DAOCreditoTributario = new DAOCreditoTributario();
                  }
                  
                  function GuardaDBCreditoTributario($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOCreditoTributario->InsertaCreditoTributario($Form);
                          }else{
                            return $this->DAOCreditoTributario->ActualizaCreditoTributario($Form);
                          }
                  }
                  
                  function DesactivaCreditoTributario($Id)
                  {       return $this->DAOCreditoTributario->DesactivaCreditoTributario($Id);
                  }
           
                  function BuscarCreditoTributarioByID($id)
                  {        return $this->DAOCreditoTributario->BuscarCreditoTributarioByDescripcion($id);
                  }

                  function BuscarCreditoTributarioByDescripcion($prepareDQL)
                  {     return $this->DAOCreditoTributario->BuscarCreditoTributarioByDescripcion($prepareDQL);                        
                  }
                  
                  function BuscarCreditoTributarioByArrayID($idArray)
                  {       return $this->DAOCreditoTributario->ObtenerCreditoTributarioCombo($idArray);
                  }
                  
                  

            }
?>

