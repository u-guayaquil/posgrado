<?php
            require_once("src/rules/impuesto/dao/DAOTipoEmision.php");
            require_once("src/rules/impuesto/entidad/TipoEmision.php");

            
            class ServicioTipoEmision
            {     private $DAOTipoEmision;
     
                  function __construct()
                  {        $this->DAOTipoEmision = new DAOTipoEmision();
                  }
           
                  function BuscarTipoEmisionByID($id)
                  {        return $this->DAOTipoEmision->ObtenerTipoEmision(array('id'=>$id));
                  }
                  
                  function BuscarTipoEmisionCombo()
                  {        return $this->DAOTipoEmision->ObtenerTipoEmision(array('limite'=>50));
                  }
                  
                  

            }
?>
