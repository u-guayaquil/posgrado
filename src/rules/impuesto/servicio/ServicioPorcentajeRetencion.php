<?php
            require_once("src/rules/impuesto/dao/DAOPorcentajeRetencion.php");
            require_once("src/rules/impuesto/entidad/PorcentajeRetencion.php");
            
            class ServicioPorcentajeRetencion
            {     private $DAOPorcentajeRetencion;
     
                  function __construct()
                  {        $this->DAOPorcentajeRetencion = new DAOPorcentajeRetencion();
                  }
                  
                  function GuardaDBPorcentajeRetencion($Form)
                  {       $Form->codigo = str_pad($Form->codigo, 10, "0", STR_PAD_LEFT);
                          if (empty($Form->id)){
                            return $this->DAOPorcentajeRetencion->InsertaPorcentajeRetencion($Form);
                          }else{
                            return $this->DAOPorcentajeRetencion->ActualizaPorcentajeRetencion($Form);
                          }
                  }
                  
                  function DesactivaPorcentajeRetencion($Id)
                  {       return $this->DAOPorcentajeRetencion->DesactivaPorcentajeRetencion($Id);
                  }
           
                  function BuscarPorcentajeRetencionByID($id)
                  {        return $this->DAOPorcentajeRetencion->ObtenerPorcentajeRetencion($id);
                  }

                  function BuscarPorcentajeRetencionByDescripcion($prepareDQL)
                  {        return $this->DAOPorcentajeRetencion->ObtenerPorcentajeRetencion($prepareDQL);                        
                  }
                  
                  function BuscarPorcentajeRetencionByArrayID($idArray)
                  {        return $this->DAOPorcentajeRetencion->ObtenerPorcentajeRetencionCombo($idArray);
                  }
                  
                  

            }
?>

