<?php
            require_once("src/rules/impuesto/dao/DAOIvaCategoria.php");
            require_once("src/rules/impuesto/entidad/IvaCategoria.php");

            
            class ServicioIvaCategoria
            {     private $DAOIvaCategoria;
     
                  function __construct()
                  {        $this->DAOIvaCategoria = new DAOIvaCategoria();
                  }
           
                  function BuscarIvaCategoriaByID($id)
                  {        return $this->DAOIvaCategoria->ObtenerIvaCategoria($id);
                  }
                  
                  function BuscarIvaCategoriaCombo($estado)
                  {        return $this->DAOIvaCategoria->ObtenerIvaCategoria($estado);
                  }

            }
?>

