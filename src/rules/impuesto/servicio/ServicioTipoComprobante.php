<?php
            require_once("src/rules/impuesto/dao/DAOTipoComprobante.php");
            require_once("src/rules/impuesto/entidad/TipoComprobante.php");
            
            class ServicioTipoComprobante
            {     private $DAOTipoComprobante;
     
                  function __construct()
                  {        $this->DAOTipoComprobante = new DAOTipoComprobante();
                  }
                  
                  function GuardaDBTipoComprobante($Form)
                  {       if (empty($Form->id)){
                            return $this->DAOTipoComprobante->InsertaTipoComprobante($Form);
                          }else{
                            return $this->DAOTipoComprobante->ActualizaTipoComprobante($Form);
                          }
                  }
                  
                  function DesactivaTipoComprobante($Id)
                  {       return $this->DAOTipoComprobante->DesactivaTipoComprobante($Id);
                  }
           
                  function BuscarTipoComprobanteByID($id)
                  {        return $this->DAOTipoComprobante->BuscarTipoComprobanteByDescripcion($id);
                  }
                                 
                  function BuscarTipoComprobanteByDescripcion($prepareDQL)
                  {     return $this->DAOTipoComprobante->BuscarTipoComprobanteByDescripcion($prepareDQL);                        
                  }
                  
            }
?>

