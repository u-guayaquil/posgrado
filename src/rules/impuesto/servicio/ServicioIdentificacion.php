<?php
        require_once("src/rules/impuesto/dao/DAOIdentificacion.php");
        
        class ServicioIdentificacion 
        {       private $DAOIdentificacion;

                function __construct()
                {        $this->DAOIdentificacion = new DAOIdentificacion();
                }

                function BuscarByArrayID($idArray)
                {        return $this->DAOIdentificacion->ObtenerByArrayID($idArray);
                }
                
                function BuscarIdentificacionByID($id)
                {        return $this->DAOIdentificacion->ObtenerObjetoById($id);
                }
                
                function BuscarIdentificacionByDescription($id)
                {        return $this->DAOIdentificacion->ObtenerByDescriptionID($id);
                }
               
                function SeleccionarTodos()
                {        return $this->DAOIdentificacion->ObtenerTodos();
                }
                
                function SeleccionaIdentificacionCombo()
                {        return $this->DAOIdentificacion->ObtenerIdentificacionCombo();
                }
        }
?>
