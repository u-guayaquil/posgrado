<?php
            require_once("src/rules/impuesto/dao/DAOTipoImpuesto.php");
            require_once("src/rules/impuesto/entidad/TipoImpuesto.php");

            
            class ServicioTipoImpuesto
            {     private $DAOTipoImpuesto;
     
                  function __construct()
                  {        $this->DAOTipoImpuesto = new DAOTipoImpuesto();
                  }
           
                  function BuscarTipoImpuestoByID($id)
                  {        return $this->DAOTipoImpuesto->ObtenerTipoImpuesto(array('id'=>$id));
                  }
                  
                  function BuscarTipoImpuestoCombo($tipo)
                  {        return $this->DAOTipoImpuesto->ObtenerTipoImpuesto(array('tipo'=>$tipo));
                  }
                  
                  function BuscarTipoImpuestoByPadre($padre)
                  {        return $this->DAOTipoImpuesto->ObtenerTipoImpuestoByPadre(array('padre'=>$padre));
                  }

            }
?>

