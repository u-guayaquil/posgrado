<?php
            require_once("src/rules/impuesto/dao/DAOIvaPorcentaje.php");
            require_once("src/rules/impuesto/entidad/IvaPorcentaje.php");
            
            class ServicioIvaPorcentaje
            {     private $DAOIvaPorcentaje;
     
                  function __construct()
                  {        $this->DAOIvaPorcentaje = new DAOIvaPorcentaje();
                  }
           
                  function GuardaDBIvaPorcentaje($Form)
                  {      if (empty($Form->id)){
                              return $this->DAOIvaPorcentaje->InsertaIvaPorcentaje($Form);
                          }else{
                              return $this->DAOIvaPorcentaje->ActualizaIvaPorcentaje($Form);
                          }
                  }
                  
                  function DesactivaIvaPorcentaje($Id)
                  {       return $this->DAOIvaPorcentaje->DesactivaIvaPorcentaje($Id);
                  }
           
                  function BuscarIvaPorcentajeByID($id)
                  {        return $this->DAOIvaPorcentaje->BuscarIvaPorcentajeByDescripcion($id);
                  }

                  function BuscarIvaPorcentajeByDescripcion($prepareDQL)
                  {        return $this->DAOIvaPorcentaje->BuscarIvaPorcentajeByDescripcion($prepareDQL);                        
                  }
                  
                  function BuscarIvaPorcentajeByArrayID($idArray)
                  {        return $this->DAOIvaPorcentaje->BuscarIvaPorcentajeByDescripcion($idArray);
                  }
                  
                  

            }
?>

