<?php
            require_once("src/rules/impuesto/dao/DAOTipoPagoSri.php");
            require_once("src/rules/impuesto/entidad/TipoPagoSri.php");
            require_once("src/rules/caja/servicio/ServicioTipo.php");

            
            class ServicioTipoPagoSri
            {     private $DAOTipoPagoSri;
                  private $ServicioTipo;
     
                  function __construct()
                  {        $this->DAOTipoPagoSri = new DAOTipoPagoSri();
                  }
           
                  function GuardaDBTipoPagoSri($Form)
                    {       if (empty($Form->id)){
                              return $this->DAOTipoPagoSri->InsertaTipoPagoSri($Form);
                            }else{
                              return $this->DAOTipoPagoSri->ActualizaTipoPagoSri($Form);
                            }
                    }
                  
                  function DesactivaTipoPagoSri($Id)
                  {       return $this->DAOTipoPagoSri->DesactivaTipoPagoSri($Id);
                  }
           
                  function BuscarTipoPagoSriByID($id)
                  {        return $this->DAOTipoPagoSri->BuscarTipoPagoSri($id);
                  }

                  function BuscarTipoPagoSriByDescripcion($prepareDQL)
                  {        return $this->DAOTipoPagoSri->BuscarTipoPagoSri($prepareDQL);                        
                  }
                  
                  function BuscarTipoPagoSriByArrayID($idArray)
                  {        return $this->DAOTipoPagoSri->ObtenerTipoPagoSriCombo($idArray);
                  }
                  
                  

            }
?>

