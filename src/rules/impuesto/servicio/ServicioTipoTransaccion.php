<?php
            require_once("src/rules/impuesto/dao/DAOTipoTransaccion.php");
            require_once("src/rules/impuesto/entidad/TipoTransaccion.php");
            
            class ServicioTipoTransaccion
            {     private $DAOTipoTransaccion;
     
                  function __construct()
                  {        $this->DAOTipoTransaccion = new DAOTipoTransaccion();
                  }
                  
                  function GuardaDBTipoTransaccion($Form)
                  {       $Form->codigo = str_pad($Form->codigo, 10, "0", STR_PAD_LEFT);
                          if (empty($Form->id)){
                            return $this->DAOTipoTransaccion->InsertaTipoTransaccion($Form);
                          }else{
                            return $this->DAOTipoTransaccion->ActualizaTipoTransaccion($Form);
                          }
                  }
                  
                  function DesactivaTipoTransaccion($Id)
                  {       return $this->DAOTipoTransaccion->DesactivaTipoTransaccion($Id);
                  }
           
                  function BuscarTipoTransaccionByID($id)
                  {        return $this->DAOTipoTransaccion->BuscarTipoTransaccion($id);
                  }

                  function BuscarTipoTransaccionByDescripcion($prepareDQL)
                  {     return $this->DAOTipoTransaccion->BuscarTipoTransaccion($prepareDQL);                       
                  }
                  
                  function BuscarTipoTransaccionByArrayID($idArray)
                  {       return $this->DAOTipoTransaccion->ObtenerTipoTransaccionCombo($idArray);
                  }
                  
                  

            }
?>

