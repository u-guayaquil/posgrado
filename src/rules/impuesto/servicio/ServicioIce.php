<?php

            require_once("src/rules/impuesto/dao/DAOIce.php");
            require_once("src/rules/impuesto/entidad/Ice.php");
            
            class ServicioIce 
            {       private $DAOIce;
     
                    function __construct()
                    {        $this->DAOIce = new DAOIce();
                    }
                    
                    function GuardaDBIce($Form)
                    {       $Form->codigo = str_pad($Form->codigo, 10, "0", STR_PAD_LEFT);
                            if (empty($Form->id)){
                              return $this->DAOIce->InsertaIce($Form);
                            }else{
                              return $this->DAOIce->ActualizaIce($Form);
                            }
                    }
                    
                    function DesactivaIce($id)
                    {       return $this->DAOIce->DesactivaIce(intval($id)); 
                    }
                    
                    function BuscarIceByID($id)
                    {       return $this->DAOIce->ObtenerDatosGeneral(($id));
                    }

                    function BuscarIceByDescripcion($prepareDQL)
                    {       return $this->DAOIce->ObtenerDatosGeneral($prepareDQL);
                    }
                    
                    function BuscarIceEstadoByID($id)
                    {       return $this->DAOIce->ObtenerIceEstadoByID(intval($id));
                    }

}        
?>        
