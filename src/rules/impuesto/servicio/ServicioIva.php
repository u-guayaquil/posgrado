<?php

            require_once("src/rules/impuesto/dao/DAOIva.php");
            
            class ServicioIva 
            {       private $DAOIva;
     
                    function __construct()
                    {        $this->DAOIva = new DAOIva();
                    }
                    
                    function GuardaDBIva($Form)
                    {       if (empty($Form->id)){
                              return $this->DAOIva->InsertaIva($Form);
                            }else{
                              return $this->DAOIva->ActualizaIva($Form);
                            }
                    }
                    
                    function DesactivaIva($id)
                    {       return $this->DAOIva->DesactivaIva(intval($id)); 
                    }
                    
                    function BuscarIvaByID($PrepareDQL)
                    {       return $this->DAOIva->ObtenerDatosGeneral($PrepareDQL);
                    }

                    function BuscarIvaByDescripcion($PrepareDQL)
                    {       return $this->DAOIva->ObtenerDatosGeneral($PrepareDQL);
                    }
                    
                    function BuscarIvaByArrayImpuestoID($PrepareDQL)
                    {       return $this->DAOIva->ObtenerIvaCombo($PrepareDQL);
                    }

}        
?>        
