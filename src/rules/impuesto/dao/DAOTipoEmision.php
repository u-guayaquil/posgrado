<?php
        require_once("src/base/DBDAO.php");

        class DAOTipoEmision extends DBDAO
        {     private static $entityTipoEmision;
              private $Funciones;
              
              public function __construct()
              {      self::$entityTipoEmision = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityTipoEmision->MyFunction();
              }
    
              public function ObtenerTipoEmision($PrepareDQL)
              {      $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE t.id =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT t.id,t.descripcion"
                           . " FROM impuesto.tipo_emision t ".$IDD
                           . " ORDER BY t.id";                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));      
              }
        }