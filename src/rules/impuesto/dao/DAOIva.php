<?php
        require_once("src/base/DBDAO.php");
        
        class DAOIva extends DBDAO
        {   private static $entityIva;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {      self::$entityIva = DBDAO::getConnection('PgProvider');
                   $this->Funciones = self::$entityIva->MyFunction();
                   $this->Fecha  = new DateControl();
            }
            
            public function ObtenerDatosGeneral($PrepareDQL)
            {       $JOIN = array_key_exists("estado", $PrepareDQL) ? " and i.idestado in (".$PrepareDQL['estado'].")" : "";
                    $IDD  = array_key_exists("id", $PrepareDQL) ? " and i.id =".$PrepareDQL['id']: "";
                    $TIP  = array_key_exists("impuesto", $PrepareDQL) ? " and i.idimpuesto in(".$PrepareDQL['impuesto'].")": "";
                    $LIM  = array_key_exists("limite", $PrepareDQL) ? "  limit ".$PrepareDQL['limite']: "";
                    $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(n.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                 
                    $DSQL = "SELECT i.id,i.idporcentaje,i.idimpuesto,i.idctacontable, i.fecreacion, i.idestado,e.descripcion as txtestado"
                           . ", l.descripcion as cuentacontable, n.descripcion as porcentaje,t.descripcion as impuesto"
                           . " FROM impuesto.iva i"
                           . " INNER JOIN general.Estado e ON i.idestado = e.id"
                           . " INNER JOIN general.planctas l ON i.idctacontable = l.id"
                           . " INNER JOIN impuesto.iva_porcentaje n ON i.idporcentaje = n.id"
                           . " INNER JOIN impuesto.tipo_impuesto t ON i.idimpuesto = t.id".$JOIN.$IDD.$TIP.$TXT
                           . " ORDER BY n.descripcion".$LIM;             
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                                     
            }
            
            
            public function ObtenerIvaCombo($PrepareDQL)
            {        $DSQL = "SELECT i.id,CONCAT(t.descripcion,n.descripcion) as descripcion"
                           . " FROM impuesto.iva i"
                           . " INNER JOIN impuesto.iva_porcentaje n ON i.idporcentaje = n.id"
                           . " INNER JOIN impuesto.tipo_impuesto t ON i.idimpuesto = t.id"
                           . " WHERE i.idimpuesto in(".$PrepareDQL['impuesto'].")"
                           . " ORDER BY i.id";             
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                                               
            }

            public function InsertaIva($datos)
            {       try{
                        $estado = $datos->estado;
                        $idporcentaje = ($datos->porcentaje);
                        $idimpuesto = ($datos->impuesto);
                        $idctacontable = ($datos->idCuentaIva);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO impuesto.iva (idporcentaje,idimpuesto,idctacontable,idestado,iduscreacion,fecreacion)"
                               . "VALUES (".$idporcentaje.", ".$idimpuesto.", ".$idctacontable.", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Iva..';
                    }
            }

            public function ActualizaIva($datos)
            {       try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $idporcentaje = ($datos->porcentaje);
                        $idimpuesto = ($datos->impuesto);
                        $idctacontable = ($datos->idCuentaIva);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.iva "
                               . "SET idporcentaje= ".$idporcentaje.",idimpuesto = ".$idimpuesto.", idestado= ".$estado.", idctacontable= ".$idctacontable
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }  
            }

            public function DesactivaIva($id)
            {     try{
                    $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE impuesto.iva SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Eliminar el Porcentaje..';
                  }
            }            
              
        }
?>        
