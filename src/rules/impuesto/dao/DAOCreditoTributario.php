<?php
        require_once("src/base/DBDAO.php");

        class DAOCreditoTributario extends DBDAO
        {     private static $entityCreditoTributario;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityCreditoTributario = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityCreditoTributario->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarCreditoTributarioByDescripcion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and c.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(c.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and c.id =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT c.id, c.descripcion,c.codigo, c.fecreacion, e.id as idestado"
                           . ", e.descripcion as txtestado "
                           . " FROM impuesto.cre_tributario c, general.Estado e "
                           . " WHERE c.idestado=e.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY c.id"
                           . " limit ".$PrepareDQL['limite'];
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaCreditoTributario($datos)
              {    try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = ($datos->codigo);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO impuesto.cre_tributario (descripcion,codigo,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', '".$codigo."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Credito Tributario..';
                    }                     
              }
    
              public function ActualizaCreditoTributario($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = ($datos->codigo);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.cre_tributario "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.", codigo= '".$codigo."'"
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaCreditoTributario($id)
              {   try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.cre_tributario SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Eliminar el Credito..';
                    }
              }
        }
