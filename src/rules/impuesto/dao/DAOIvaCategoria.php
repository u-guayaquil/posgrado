<?php
        require_once("src/base/DBDAO.php");

        class DAOIvaCategoria extends DBDAO
        {     private static $entityIvaCategoria;
              private $Funciones;
              
              public function __construct()
              {      self::$entityIvaCategoria = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityIvaCategoria->MyFunction();
              }

              public function ObtenerIvaCategoria($PrepareDQL)
              {      
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " where i.id =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT i.id,i.descripcion"
                           . " FROM impuesto.iva_categoria i ".$IDD
                           . " ORDER BY i.id";
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
        }
