<?php
        require_once("src/base/DBDAO.php");

        class DAOIvaPorcentaje extends DBDAO
        {     private static $entityIvaPorcentaje;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityIvaPorcentaje = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityIvaPorcentaje->MyFunction();
                     $this->Fecha  = new DateControl();
              }
    
              function BuscarIvaPorcentajeByDescripcion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and i.idestado in (".$PrepareDQL['estado'].")" : "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and i.id =".$PrepareDQL['id']: "";
                     $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(i.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $DSQL = "SELECT i.id,i.descripcion,i.valor,i.codigo,i.idcativa,i.fecreacion,i.idestado,e.descripcion as txtestado"
                           . " FROM impuesto.iva_porcentaje i"
                           . " INNER JOIN general.Estado e ON i.idestado = e.id".$JOIN.$IDD.$TXT
                           . " ORDER BY i.id"
                           . " limit ".$PrepareDQL['limite'];
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
                         
              public function InsertaIvaPorcentaje($datos)
              {    try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $valor = ($datos->valor);
                        $idcativa = ($datos->categoria);
                        $codigo = ($datos->codigo);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO impuesto.iva_porcentaje (descripcion,valor,codigo,idcativa,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', ".$valor.", ".$codigo.", ".$idcativa.", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Porcentaje de IVA..';
                    }                     
              }
    
              public function ActualizaIvaPorcentaje($datos)
              {     try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $valor = ($datos->valor);
                        $codigo = ($datos->codigo);
                        $idcativa = ($datos->categoria);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.iva_porcentaje "
                               . "SET descripcion= '".$descripcion."',valor = ".$valor.", idestado= ".$estado.", idcativa= ".$idcativa
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."', codigo= ".$codigo
                               . "WHERE id= ".$id; 
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) { 
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    } 
              }
    
              public function DesactivaIvaPorcentaje($id)
              {   try{
                    $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE impuesto.iva_porcentaje SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Eliminar el Porcentaje..');
                    }
              }
        }
