<?php
        require_once("src/base/DBDAO.php");

        class DAOTipoPagoSri extends DBDAO
        {     private static $entityTipoPagoSri;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityTipoPagoSri = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityTipoPagoSri->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarTipoPagoSri($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estado'].")" : "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and p.id =".$PrepareDQL['id']: "";
                     $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(p.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT p.id,p.descripcion,p.codigo,p.idcajatipo, p.fecreacion, p.idestado,e.descripcion as txtestado"
                           . ", t.descripcion as tipo"
                           . " FROM impuesto.tipo_pagosri p"
                           . " INNER JOIN general.Estado e ON p.idestado = e.id"
                           . " INNER JOIN caja.tipo t ON p.idcajatipo = t.id".$JOIN.$IDD.$TXT
                           . " ORDER BY p.id".$LIM;
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
                         
              public function InsertaTipoPagoSri($datos)
              {    try{
                        $estado = $datos->estado;
                        $codigo = ($datos->codigo);
                        $descripcion = strtoupper($datos->descripcion);
                        $idcajatipo = ($datos->tipo);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO impuesto.tipo_pagosri (codigo,descripcion,idcajatipo,idestado,iduscreacion,fecreacion)"
                               . "VALUES (".$codigo.", '".$descripcion."', ".$idcajatipo.", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Tipo de Pago..';
                    }                     
              }
    
              public function ActualizaTipoPagoSri($datos)
              {     try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $codigo = ($datos->codigo);
                        $descripcion = strtoupper($datos->descripcion);
                        $idcajatipo = ($datos->tipo);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.tipo_pagosri "
                               . "SET codigo= ".$codigo.",descripcion = '".$descripcion."', idestado= ".$estado.", idcajatipo= ".$idcajatipo
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    } 
              }
    
              public function DesactivaTipoPagoSri($id)
              {   try{
                    $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE impuesto.tipo_pagosri SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Eliminar el Tipo..';
                    }
              }
        }
