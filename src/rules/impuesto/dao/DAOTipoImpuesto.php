<?php
        require_once("src/base/DBDAO.php");

        class DAOTipoImpuesto extends DBDAO
        {     private static $entityTipoImpuesto;
              private $Funciones;
              
              public function __construct()
              {      self::$entityTipoImpuesto = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityTipoImpuesto->MyFunction();
              }
    
            public function ObtenerTipoImpuesto($PrepareDQL)
            {       $IDD  = array_key_exists("id", $PrepareDQL) ? " WHERE t.id =".$PrepareDQL['id']: "";
                    $TIPO  = array_key_exists("tipo", $PrepareDQL) ? " WHERE t.idpadre =".$PrepareDQL['tipo']: "";
                    $DSQL = "SELECT t.id,t.descripcion,t.idpadre,t.codigo"
                           . " FROM impuesto.tipo_impuesto t ".$IDD.$TIPO
                           . " ORDER BY t.id";                  
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));  
            }
            
            public function ObtenerTipoImpuestoByPadre($PrepareDQL)
            {       $FATH  = array_key_exists("padre", $PrepareDQL) ? " WHERE t.idpadre in (".$PrepareDQL['padre'].")": "";
                    $DSQL = "SELECT t.id,t.codigo,t.descripcion,t.idpadre FROM impuesto.tipo_impuesto t ".$FATH." ORDER BY t.id ";
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));  
            }
            
            
              
        }
