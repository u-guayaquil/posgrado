<?php
        require_once("src/base/DBDAO.php");

        class DAOTipoComprobante extends DBDAO
        {     private static $entityTipoComprobante;
              
              public function __construct()
              {      self::$entityTipoComprobante = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityTipoComprobante->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarTipoComprobanteByDescripcion($PrepareDQL)
              {     $JOIN = array_key_exists("estado", $PrepareDQL) ? " and t.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(t.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and t.id =".$PrepareDQL['id']: "";
                     $DSQL = "SELECT t.id, t.descripcion,t.codigo, t.fecreacion, e.id as idestado"
                           . ", e.descripcion as txtestado "
                           . " FROM impuesto.tipo_comprobante t, general.Estado e "
                           . " WHERE t.idestado=e.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY t.descripcion"
                           . " limit ".$PrepareDQL['limite'];
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaTipoComprobante($datos)
              {    try{        
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = str_pad($datos->codigo,10,'0',STR_PAD_LEFT);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO impuesto.tipo_comprobante (descripcion,codigo,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', '".$codigo."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo crear el Tipo de Comprobante..');
                    }                     
              }
    
              public function ActualizaTipoComprobante($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = str_pad($datos->codigo,10,'0',STR_PAD_LEFT);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.tipo_comprobante "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.", codigo= '".$codigo."'"
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Actualizar los Datos..');
                    }
              }
    
              public function DesactivaTipoComprobante($id)
              {   try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.tipo_comprobante SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    return array(true, intval($id));
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return array(false, 'No se pudo Eliminar el Tipo de Comprobante..');
                    }
              }
        }
