<?php

        require_once("src/base/DBDAO.php");

        class DAOIdentificacion extends DBDAO
        {       private static $entityIdentificacion;
                private $Funciones;

                public function __construct()
                {      self::$entityIdentificacion = DBDAO::getConnection('PgProvider');
                       $this->Funciones = self::$entityIdentificacion->MyFunction();
                }

                public function ObtenerByArrayID($IdArray)
                {      $DSQL = "SELECT i.id, i.identificacion,i.codigo"                           
                          . " FROM impuesto.identificacion i WHERE i.id in(".$IdArray['tipo'].")"
                          . " ORDER BY i.id"; 
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
                
                public function ObtenerByDescriptionID($Descripcion)
                {      $DSQL = "SELECT i.id, i.identificacion,i.codigo"                           
                          . " FROM impuesto.identificacion i WHERE RTRIM(LTRIM(i.identificacion)) like '%".$Descripcion."%'"
                          . " ORDER BY i.id"; 
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));  
                }
              
                public function ObtenerTodos()
                {      $DSQL = "SELECT i.id, i.identificacion,i.codigo"                           
                          . " FROM impuesto.identificacion i"
                          . " ORDER BY i.id"; 
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
                
                public function ObtenerObjetoById($id)
                {      $DSQL = "SELECT i.id, i.identificacion,i.codigo"                           
                          . " FROM impuesto.identificacion i WHERE i.id =".$id.""
                          . " ORDER BY i.id"; 
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
                
                public function ObtenerIdentificacionCombo()
                {      $DSQL = "SELECT i.id, i.identificacion,i.codigo"                           
                          . " FROM impuesto.identificacion i"
                          . " ORDER BY i.id"; 
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

        }

?>