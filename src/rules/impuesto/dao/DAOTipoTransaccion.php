<?php
        require_once("src/base/DBDAO.php");

        class DAOTipoTransaccion extends DBDAO
        {     private static $entityTipoTransaccion;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityTipoTransaccion = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityTipoTransaccion->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function BuscarTipoTransaccion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and t.idestado in (".$PrepareDQL['estado'].")" : "";
                     $LIKE = array_key_exists("texto", $PrepareDQL) ? " and upper(t.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and t.id =".$PrepareDQL['id']: "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT t.id, t.descripcion,t.codigo, t.fecreacion, e.id as idestado"
                           . ", e.descripcion as txtestado "
                           . " FROM impuesto.tipo_transaccion t, general.Estado e "
                           . " WHERE t.idestado=e.id ".$JOIN.$LIKE.$IDD
                           . " ORDER BY t.descripcion".$LIM;
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
              
              public function InsertaTipoTransaccion($datos)
              {    try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO impuesto.tipo_transaccion (descripcion,codigo,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', '".$codigo."', ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Tipo Transaccion..';
                    }                     
              }
    
              public function ActualizaTipoTransaccion($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.tipo_transaccion "
                               . "SET descripcion= '".$descripcion."', idestado= ".$estado.", codigo= '".$codigo."'"
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
    
              public function DesactivaTipoTransaccion($id)
              {   try{
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.tipo_transaccion SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Eliminar el Tipo de Transaccion..';
                    }
              }
              
              public function ObtenerTipoTransaccionCombo($IdArray)
              {      $QueryBld = $this->entityTipoTransaccion->createQueryBuilder('t');
                     $WhereBld = $QueryBld->expr()->in('t.idestado', ':Arreglo');
                     $QueryDQL = $QueryBld->where($WhereBld)->orderBy('t.descripcion', 'ASC')
                                       ->setParameter('Arreglo',$IdArray)
                                       ->getQuery();
                     return $QueryDQL->getArrayResult();
             }
        }
