<?php
        require_once("src/base/DBDAO.php");

        class DAOPorcentajeRetencion extends DBDAO
        {     private static $entityPorcentajeRetencion;
              private $Funciones;
              private $Fecha;
              
              public function __construct()
              {      self::$entityPorcentajeRetencion = DBDAO::getConnection('PgProvider');
                     $this->Funciones = self::$entityPorcentajeRetencion->MyFunction();
                     $this->Fecha  = new DateControl();
              }
              
              function ObtenerPorcentajeRetencion($PrepareDQL)
              {      $JOIN = array_key_exists("estado", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estado'].")" : "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and p.id =".$PrepareDQL['id']: "";
                     $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(p.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT p.id, p.descripcion,p.codigo,p.valor, p.fecreacion, p.idestado,p.idimpuesto"
                           . ", e.descripcion as txtestado,p.idctaacreedora,p.idctadeudora,l.descripcion as acreedora,n.descripcion as deudora "
                           . " FROM impuesto.por_retencion p"
                           . " INNER JOIN general.Estado e ON p.idestado = e.id"
                           . " INNER JOIN general.planctas l ON p.idctaacreedora = l.id"
                           . " INNER JOIN general.planctas n ON p.idctadeudora = n.id".$JOIN.$IDD.$TXT
                           . " ORDER BY p.descripcion".$LIM;
                  
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
              }
                         
              public function InsertaPorcentajeRetencion($datos)
              {    try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $valor = strtoupper($datos->valor);
                        $acreedora = strtoupper($datos->idacreedora);
                        $deudora = strtoupper($datos->iddeudora);
                        $tipoimpuesto = strtoupper($datos->tipoimpuesto);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO impuesto.por_retencion (descripcion,codigo,valor,idimpuesto,idctaacreedora,idctadeudora,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', '".$codigo."', ".$valor.", ".$tipoimpuesto.", ".$acreedora.", ".$deudora.", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Porcentaje de Retencion..';
                    }                     
              }    
              
              public function ActualizaPorcentajeRetencion($datos)
              {      try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $valor = strtoupper($datos->valor);
                        $acreedora = strtoupper($datos->idacreedora);
                        $deudora = strtoupper($datos->iddeudora);
                        $tipoimpuesto = strtoupper($datos->tipoimpuesto);
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "UPDATE impuesto.por_retencion "
                               . "SET descripcion= '".$descripcion."',valor = ".$valor.", idestado= ".$estado.", codigo= '".$codigo."'"
                               . ",idctaacreedora = ".$acreedora." , idctadeudora = ".$deudora.", idimpuesto=".$tipoimpuesto
                               . ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }
              }
              
              public function DesactivaPorcentajeRetencion($id)
              {   try{
                    $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE impuesto.por_retencion SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
                  } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Eliminar el Porcentaje..';
                    }
              }

        }
