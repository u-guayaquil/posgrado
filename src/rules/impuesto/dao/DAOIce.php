<?php
        require_once("src/base/DBDAO.php");
        
        class DAOIce extends DBDAO
        {   private static $entityIce;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {      self::$entityIce = DBDAO::getConnection('PgProvider');
                   $this->Funciones = self::$entityIce->MyFunction();
                   $this->Fecha  = new DateControl();
            }
            
            public function ObtenerDatosGeneral($PrepareDQL)
            {        $JOIN = array_key_exists("estado", $PrepareDQL) ? " and i.idestado in (".$PrepareDQL['estado'].")" : "";
                     $IDD  = array_key_exists("id", $PrepareDQL) ? " and i.id =".$PrepareDQL['id']: "";
                     $TXT  = array_key_exists("texto", $PrepareDQL) ? " WHERE upper(i.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                     $LIM  = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                     $DSQL = "SELECT i.id, i.descripcion,i.codigo,i.valor, i.fecreacion, i.idestado, e.descripcion as txtestado"
                           . ",i.idctaiceventa,l.descripcion as iceventa,i.idctaicecompra, n.descripcion as icecompra "
                           . ",i.idctaivaventa,c.descripcion as ivaventa,i.idctaivacompra, t.descripcion as ivacompra "
                           . " FROM impuesto.ice i"
                           . " INNER JOIN general.Estado e ON i.idestado = e.id"
                           . " INNER JOIN general.planctas l ON i.idctaiceventa = l.id"
                           . " INNER JOIN general.planctas n ON i.idctaicecompra = n.id"
                           . " INNER JOIN general.planctas c ON i.idctaivaventa = c.id"
                           . " INNER JOIN general.planctas t ON i.idctaivacompra = t.id".$JOIN.$IDD.$TXT
                           . " ORDER BY i.descripcion".$LIM;
                     return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                                   
            }
            
            public function InsertaIce($datos)
            {      try{
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $valor = strtoupper($datos->valor);
                        $icecompra = strtoupper($datos->idIceCompra);
                        $iceventa = strtoupper($datos->idIceVenta);
                        $ivacompra = strtoupper($datos->idIvaCompra);
                        $ivaventa = strtoupper($datos->idIvaVenta);
                        $fechai = $this->Fecha->getNowDateTime('DATETIME');                        
                        $Query = "INSERT INTO impuesto.ice (descripcion,codigo,valor,idctaiceventa,idctaicecompra,idctaivaventa"
                               . ",idctaivacompra,idestado,iduscreacion,fecreacion)"
                               . "VALUES ('".$descripcion."', '".$codigo."', ".$valor.", ".$iceventa.", ".$icecompra.""
                               . ", ".$ivaventa.", ".$ivacompra.", ".$estado.",".$_SESSION['IDUSER'].",'".$fechai."') returning id";
                        $Rsid = $this->Funciones->Query($Query);
                        $id = $this->Funciones->FieldDataByName($Rsid,"id");
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo crear el Ice..';
                    }
            }

            public function ActualizaIce($datos)
            {       try{
                        $id = $datos->id;
                        $estado = $datos->estado;
                        $descripcion = strtoupper($datos->descripcion);
                        $codigo = strtoupper($datos->codigo);
                        $valor = strtoupper($datos->valor);
                        $icecompra = strtoupper($datos->idIceCompra);
                        $iceventa = strtoupper($datos->idIceVenta);
                        $ivacompra = strtoupper($datos->idIvaCompra);
                        $ivaventa = strtoupper($datos->idIvaVenta);  
                        $fecham = $this->Fecha->getNowDateTime('DATETIME');
                        $Query = "UPDATE impuesto.ice "
                               . "SET descripcion= '".$descripcion."',valor = ".$valor.", idestado= ".$estado.", codigo= '".$codigo."'"
                               . ",idctaiceventa = ".$iceventa." , idctaicecompra = ".$icecompra.", idctaivaventa=".$ivaventa." "
                               . ", idctaivacompra = ".$ivacompra. ", idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' "
                               . "WHERE id= ".$id;
                        $this->Funciones->Query($Query);
                        return intval($id);
                    } catch (Exception $ex) {
                        $this->manejadorExcepciones($ex);
                        return 'No se pudo Actualizar los Datos..';
                    }  
            }

            public function DesactivaIce($id)
            {       $fecham = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE impuesto.ice SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecham."' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }   
            
        }
?>        
