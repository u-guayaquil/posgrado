<?php
require_once("src/rules/impuesto/entidad/Iva.php");
    /**
     *
     * @Entity
     * @Table(name="tipo_impuesto", schema="impuesto")
     */
class TipoImpuesto
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;
    /**
      * @Column(name="codigo", type="string", length=10)
    */
    private $codigo;
    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion = '';
    /**
      * @Column(name="idpadre", type="integer")
    */
    private $idPadre= '';
    
    /**
     * @OneToMany(targetEntity="Iva",
     *            mappedBy="Impuestos")
     */
    private $TipoImpuestos;

    public function getId()
    {
        return $this->id;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function setIdPadre($idPadre)
    {
        $this->idPadre = $idPadre;
    }

    public function getIdPadre()
    {
        return $this->idPadre;
    }
    
    public function getTipoImpuestos(){
	return $this->TipoImpuestos;
    }
	
    public function setTipoImpuestos(Iva $TipoImpuestos){
           $this->TipoImpuestos = $TipoImpuestos;
    }

}

