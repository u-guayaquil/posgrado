<?php

    /**
     * @Table(name="identificacion", schema="impuesto")
     * @Entity
     */
    
    class Identificacion
    {
    /**
     * @Id
     * @Column(name="id", type="integer", nullable=false)
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(name="identificacion", type="string", length=20, nullable=false)
     */
    private $identificacion = '';

    /**
     * @Column(name="codigo", type="string", length=10, nullable=false)
     */
    private $codigo;

    public function getId()
    {      return $this->id;
    }

    public function setIdentificacion($identificacion)
    {      $this->identificacion = $identificacion;
    }

    public function getIdentificacion()
    {      return $this->identificacion;
    }

    public function setCodigo($codigo)
    {      $this->codigo = $codigo;
    }

    public function getCodigo()
    {      return $this->codigo;
    }
}

