<?php
require_once("src/base/EntidadGlobal.php");
require_once("src/rules/impuesto/entidad/Iva.php");

    /**
     * @Entity
     * @Table(name="iva_porcentaje", schema="impuesto")
     */
class IvaPorcentaje extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=10)
    */
    private $descripcion = '';

    /**
     * @Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor = '0';

    /**
      * @Column(name="codigo", type="string", length=10)
    */
    private $codigo = '';

    /**
      * @Column(name="idcativa", type="integer", length=10)
    */
    private $idcativa;
    
    /**
     * @ManyToOne(targetEntity="IvaCategoria")
     * @JoinColumn(name="idcativa", referencedColumnName="id", nullable=false)
     */
    private $IvaCategorias;
    
    /**
     * @OneToMany(targetEntity="Iva",
     *            mappedBy="Porcentajes")
     */
    private $Porcentaje;

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    public function getValor()
    {
        return $this->valor;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }
    
    public function setIdCatIva($idcativa)
    {      $this->idcativa = $idcativa;
    }

    public function getIdCatIva()
    {      return $this->idcativa;
    }

    public function setIdIvaCategorias(IvaCategoria $IvaCategorias)
    {
        $this->IvaCategorias = $IvaCategorias;
    }

    public function getIdIvaCategorias()
    {
        return $this->IvaCategorias;
    }
    
    public function getPorcentaje(){
	return $this->Porcentaje;
    }
	
    public function setPorcentaje(IvaPorcentaje $Porcentaje){
           $this->Porcentaje = $Porcentaje;
    }
}

