<?php
require_once("src/base/EntidadGlobal.php");
require_once("src/rules/caja/entidad/Tipo.php");

    /**
     * @Entity
     * @Table(name="tipo_pagosri", schema="impuesto")
     */
class TipoPagoSri extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=10)
    */
    private $descripcion = '';

    /**
      * @Column(name="codigo", type="string", length=10)
    */
    private $codigo = '';

    /**
      * @Column(name="idcajatipo", type="integer", length=10)
    */
    private $idcajatipo;
    
    /**
     * @ManyToOne(targetEntity="Tipo")
     * @JoinColumn(name="idcajatipo", referencedColumnName="id", nullable=false)
     */
    private $TipoCajas;
    

    function getId() {
        return $this->id;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getIdcajatipo() {
        return $this->idcajatipo;
    }

    function getTipoCajas() {
        return $this->TipoCajas;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setIdcajatipo($idcajatipo) {
        $this->idcajatipo = $idcajatipo;
    }

    function setTipoCajas(Tipo $TipoCajas) {
        $this->TipoCajas = $TipoCajas;
    }


}

