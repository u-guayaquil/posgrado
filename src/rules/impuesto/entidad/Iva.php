<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/impuesto/entidad/IvaPorcentaje.php");
    require_once("src/rules/impuesto/entidad/TipoImpuesto.php");
    require_once("src/rules/general/entidad/PlanCuentas.php");

    /**
     * @Entity
     * @Table(name="iva",schema ="impuesto")
     */
    class Iva extends EntidadGlobal 
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="idporcentaje", type="integer", nullable=false)
         */
        private $idPorcentaje;

        /**
         * @Column(name="idimpuesto", type="integer", nullable=false)
         */
        private $idImpuesto;

        /**
         * @Column(name="idctacontable", type="integer", nullable=false)
         */
        private $idCtaContable;
        
        /**
        * @ManyToOne(targetEntity="IvaPorcentaje")
        * @JoinColumn(name="idporcentaje", referencedColumnName="id", nullable=false)
        */        
        private $Porcentajes;
        
        /**
        * @ManyToOne(targetEntity="TipoImpuesto")
        * @JoinColumn(name="idimpuesto", referencedColumnName="id", nullable=false)
        */        
        private $Impuestos;
        
        /**
        * @ManyToOne(targetEntity="PlanCuentas")
        * @JoinColumn(name="idctacontable", referencedColumnName="id", nullable=false)
        */        
        private $CuentasContables;

        public function getId()
        {      return $this->id;
        }

        public function setIdPorcentaje($idPorcentaje)
        {      $this->idPorcentaje = $idPorcentaje;
        }

        public function getIdPorcentaje()
        {      return $this->idPorcentaje;
        }

        public function setIdImpuesto($idImpuesto)
        {      $this->idImpuesto = $idImpuesto;
        }

        public function getIdImpuesto()
        {      return $this->idImpuesto;
        }

        public function setIdCtaContable($idCtaContable)
        {      $this->idCtaContable = $idCtaContable;
        }

        public function getIdCtaContable()
        {      return $this->idCtaContable;
        }
        
        public function setPorcentajes(IvaPorcentaje $Porcentajes)
        {
            $this->Porcentajes = $Porcentajes;
        }

        public function getPorcentajes()
        {
            return $this->Porcentajes;
        }
        
        public function setImpuestos(TipoImpuesto $Impuestos)
        {
            $this->Impuestos = $Impuestos;
        }

        public function getImpuestos()
        {
            return $this->Impuestos;
        }
        
        public function setCuentasContables(PlanCuentas $CuentasContables)
        {
            $this->CuentasContables = $CuentasContables;
        }

        public function getCuentasContables()
        {
            return $this->CuentasContables;
        }

    }
