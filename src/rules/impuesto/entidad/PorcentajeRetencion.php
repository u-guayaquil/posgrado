<?php
require_once("src/base/EntidadGlobal.php");
require_once("src/rules/general/entidad/PlanCuentas.php");
require_once("src/rules/impuesto/entidad/TipoImpuesto.php");

    /**
     * @Entity
     * @Table(name="por_retencion", schema="impuesto")
     */
class PorcentajeRetencion extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion = '';

    /**
     * @Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor = '0';

    /**
      * @Column(name="codigo", type="string", length=10)
    */
    private $codigo = '';
    
    /**
     * @ManyToOne(targetEntity="PlanCuentas")
     * @JoinColumn(name="idctaacreedora", referencedColumnName="id", nullable=false)
     */
    private $CuentaAcreedora;
    
    /**
     * @ManyToOne(targetEntity="PlanCuentas")
     * @JoinColumn(name="idctadeudora", referencedColumnName="id", nullable=false)
     */
    private $CuentaDeudora;

    /**
      * @ManyToOne(targetEntity="TipoImpuesto")
      * @JoinColumn(name="idimpuesto", referencedColumnName="id", nullable=false)
      */        
    private $Impuestos;

    public function getId()
    {
        return $this->id;
    }
    
    function getDescripcion() {
        return $this->descripcion;
    }

    function getValor() {
        return $this->valor;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getCuentaAcreedora() {
        return $this->CuentaAcreedora;
    }

    function getCuentaDeudora() {
        return $this->CuentaDeudora;
    }

    function getImpuestos() {
        return $this->Impuestos;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setValor($valor) {
        $this->valor = $valor;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setCuentaAcreedora(PlanCuentas $CuentaAcreedora) {
        $this->CuentaAcreedora = $CuentaAcreedora;
    }

    function setCuentaDeudora(PlanCuentas $CuentaDeudora) {
        $this->CuentaDeudora = $CuentaDeudora;
    }

    function setImpuestos(TipoImpuesto $Impuestos) {
        $this->Impuestos = $Impuestos;
    }


}

