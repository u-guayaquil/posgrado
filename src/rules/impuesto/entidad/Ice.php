<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/general/entidad/PlanCuentas.php");

    /**
     * @Entity
     * @Table(name="ice",schema ="impuesto")
     */
    class Ice extends EntidadGlobal 
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;
        
        /**
         * @Column(name="descripcion", type="string", length=10)
        */
        private $Descripcion = '';
        
        /**
         * @Column(name="codigo", type="string", length=10)
        */
        private $Codigo = '';
        /**
         * @Column(name="valor", type="float", precision=10, scale=0, nullable=false)
        */
        private $Valor = '0';        
               
        /**
        * @ManyToOne(targetEntity="PlanCuentas")
        * @JoinColumn(name="idctaiceventa", referencedColumnName="id", nullable=false)
        */        
        private $IceVenta;
        
        /**
        * @ManyToOne(targetEntity="PlanCuentas")
        * @JoinColumn(name="idctaicecompra", referencedColumnName="id", nullable=false)
        */        
        private $IceCompra;
        
        /**
        * @ManyToOne(targetEntity="PlanCuentas")
        * @JoinColumn(name="idctaivaventa", referencedColumnName="id", nullable=false)
        */        
        private $IvaVenta;
        
        /**
        * @ManyToOne(targetEntity="PlanCuentas")
        * @JoinColumn(name="idctaivacompra", referencedColumnName="id", nullable=false)
        */        
        private $IvaCompra;

        public function getId()
        {      return $this->id;
        }

        function getDescripcion() {
            return $this->Descripcion;
        }

        function getCodigo() {
            return $this->Codigo;
        }

        function getValor() {
            return $this->Valor;
        }

        function getIceVenta() {
            return $this->IceVenta;
        }

        function getIceCompra() {
            return $this->IceCompra;
        }

        function getIvaVenta() {
            return $this->IvaVenta;
        }

        function getIvaCompra() {
            return $this->IvaCompra;
        }

        function setDescripcion($descripcion) {
            $this->Descripcion = $descripcion;
        }

        function setCodigo($codigo) {
            $this->Codigo = $codigo;
        }

        function setValor($valor) {
            $this->Valor = $valor;
        }

        function setIceVenta(PlanCuentas $IceVenta) {
            $this->IceVenta = $IceVenta;
        }

        function setIceCompra(PlanCuentas $IceCompra) {
            $this->IceCompra = $IceCompra;
        }

        function setIvaVenta(PlanCuentas $IvaVenta) {
            $this->IvaVenta = $IvaVenta;
        }

        function setIvaCompra(PlanCuentas $IvaCompra) {
            $this->IvaCompra = $IvaCompra;
        }


    }
