<?php
/**
     * @Entity
     * @Table(name="iva_categoria", schema="impuesto")
     */
class IvaCategoria
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion;
    
     /**
     * @OneToMany(targetEntity="IvaPorcentaje",
     *            mappedBy="IvaCategorias")
     */
    private $Categorias;


    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function getCategorias(){
	return $this->Categorias;
    }
	
    public function setCategorias(IvaPorcentaje $Categorias){
           $this->Categorias = $Categorias;
    }
}

