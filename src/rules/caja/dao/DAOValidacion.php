<?php
        require_once("src/base/DBDAO.php");

        class DAOValidacion extends DBDAO
        {   private static $entityValidacion;
            private $Funciones;
         
              
            public function __construct()
            {      self::$entityValidacion = DBDAO::getConnection();
                   $this->Funciones = self::$entityValidacion->MyFunction();
            }
    
            public function ObtenerValidacionByID($id)
            {      $DSQL = "SELECT id,descripcion FROM caja.documentos_validacion WHERE id = '$id' ORDER BY id ";
                   return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }

            public function ObtenerValidacion($PrepareDQL)
            {      $ORDN = array_key_exists("orden", $PrepareDQL) ? $PrepareDQL['orden'] : "id";
                   $DSQL = "SELECT id,descripcion FROM caja.documentos_validacion ORDER BY ".$ORDN;                  
                   return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
        }
