<?php
        require_once("src/base/DBDAO.php");
        
        class DAOMediosPago extends DBDAO
        {   private static $entityPago;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {      self::$entityPago = DBDAO::getConnection();
                   $this->Funciones = self::$entityPago->MyFunction();
                   $this->Fecha  = new DateControl();
            }
            
            public function ObtenerDatosGeneral($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ? " and p.id =".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("sucursal", $PrepareDQL) ? " and p.idsucursal in (".$PrepareDQL['sucursal'].")" : "";
                    $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and p.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ? " and upper(m.descripcion||' '||p.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite'] : "";
                    
                    $DSQL = "SELECT ";
                    $DSQL.= "m.idcajatipo,"; 
                    $DSQL.= "p.idcajamed, m.descripcion as medio,";
                    $DSQL.= "p.id, p.descripcion,";
                    $DSQL.= "p.idsubtipo,  t.descripcion as subtipo,";
                    $DSQL.= "p.idsucursal, s.descripcion as sucursal,";
                    $DSQL.= "p.idctacontable, n.descripcion as cuentacontable,";
                    $DSQL.= "p.idestado, e.descripcion as txtestado,";
                    $DSQL.= "p.fecreacion ";
                    $DSQL.= "FROM caja.medios_pagos p, general.estado e, caja.medios m, caja.medios_subtipo t, general.planctas n, general.sucursal s ";
                    $DSQL.= "WHERE p.idestado=e.id and p.idcajamed=m.id and p.idsubtipo=t.id and p.idctacontable=n.id and p.idsucursal=s.id ".$JOIN;
                    $DSQL.= " ORDER BY p.id ".$LIMT;                  
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                                     
            }

            public function InsertaPago($datos)
            {           $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                        $descripcion = strtoupper($datos->descripcion);
                        
                        $Query = "INSERT INTO caja.pago (descripcion,idcajamed,idctacontable,idsucursal,idsubtipo,idestado,iduscreacion,fecreacion) "
                               . "VALUES ('$descripcion','$datos->medio','$datos->idCuentaPago','$datos->idSucursal','$datos->subtipo','$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id ";

                        $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                        return intval($id);
            }

            public function ActualizaPago($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');  
                    $descripcion = strtoupper($datos->descripcion);
                    
                    $Query = "UPDATE caja.medios_pagos SET "
                               . "descripcion='$descripcion', "
                               . "idcajamed='$datos->medio', "
                               . "idestado='$datos->estado', "
                               . "idctacontable='$datos->idCuentaPago', "
                               . "idsucursal='$datos->idSucursal', "
                               . "idsubtipo='$datos->subtipo', "
                               . "idusmodifica=".$_SESSION['IDUSER'].", "
                               . "femodificacion='$fecha' "
                               . "WHERE id= ".$datos->id;
                    
                    $this->Funciones->Query($Query);
                    return intval($datos->id);
            }

            public function DesactivaPago($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE caja.medios_pagos SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion='$fecha' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }   
            
        }
?>        
