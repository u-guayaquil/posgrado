<?php
        require_once("src/base/DBDAO.php");

        class DAODocumento extends DBDAO
        {   private static $entityDocumento;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {       self::$entityDocumento = DBDAO::getConnection();
                    $this->Funciones = self::$entityDocumento->MyFunction();
                    $this->Fecha  = new DateControl();
            }
              
            function BuscarDocumento($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ? " and d.id =".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("tipo", $PrepareDQL) ? " and d.idcajatipo=".$PrepareDQL['tipo'] : "";
                    $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and d.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ? " and upper(d.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                    $ORDE = array_key_exists("orden", $PrepareDQL) ? $PrepareDQL['orden'] : " d.id ";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                    
                    $DSQL = "SELECT d.id, d.idcajaval, d.idcajatipo, d.fecreacion, d.idestado, e.descripcion as txtestado, v.descripcion as validacion, t.descripcion as tipo, d.descripcion "
                          . "FROM caja.documentos d, general.estado e, caja.documentos_validacion v, caja.medios_tipo t "
                          . "WHERE d.id>0 and d.idestado=e.id and d.idcajaval=v.id and d.idcajatipo=t.id ".$JOIN
                          . "ORDER BY ".$ORDE.$LIMT;                  
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));  
            }
                         
            public function InsertaDocumento($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $descripcion = strtoupper($datos->descripcion);
                    
                    $Query = "INSERT INTO caja.documentos (descripcion,idcajaval,idcajatipo,idestado,iduscreacion,fecreacion) "
                           . "VALUES ('$descripcion', '$datos->validacion', '$datos->tipo', '$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id ";
                        
                    $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                    return intval($id);
            }
    
            public function ActualizaDocumento($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');  
                    $descripcion = strtoupper($datos->descripcion);
                        
                    $Query = "UPDATE caja.documentos SET "
                           . "descripcion='$descripcion', idcajaval='$datos->validacion', idestado='$datos->estado', idcajatipo='$datos->tipo', idusmodifica=".$_SESSION['IDUSER'].", femodificacion='$fecha' "
                           . "WHERE id= ".$datos->id;
                    $this->Funciones->Query($Query);
                    return intval($datos->id);
            }
    
            public function DesactivaDocumento($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE caja.documentos SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion='$fecha' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }
              
        }
