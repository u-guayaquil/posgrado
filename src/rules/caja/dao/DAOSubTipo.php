<?php
        require_once("src/base/DBDAO.php");

        class DAOSubTipo extends DBDAO
        {       private static $entitySubTipo;
                private $Funciones;
              
                public function __construct()
                {      self::$entitySubTipo = DBDAO::getConnection();
                       $this->Funciones = self::$entitySubTipo->MyFunction();
                }
    
                public function ObtenerSubTipoByID($id)
                {      $DSQL = "SELECT id, descripcion FROM caja.medios_subtipo WHERE id='$id' ORDER BY id";                  
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

              
                public function ObtenerSubTipo($PrepareDQL)
                {      $TIPO = array_key_exists("tipo", $PrepareDQL) ? " WHERE a.idcajatipo in (".$PrepareDQL['tipo'].")" : "";
                       $DSQL = "SELECT a.id, a.descripcion FROM caja.medios_subtipo as a ".$TIPO." ORDER BY a.id ";                  
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                public function ObtenerSubTipoMedios($IdMedios)
                {       $DSQL = "Select b.id,b.descripcion from caja.medios as a,caja.medios_subtipo as b Where a.id='$IdMedios' and a.idcajatipo=b.idcajatipo Order by b.id ";
                        return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

        }
