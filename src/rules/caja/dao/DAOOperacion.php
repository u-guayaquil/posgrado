<?php
        require_once("src/base/DBDAO.php");

        class DAOOperacion extends DBDAO
        {       private static $entityOperacion;
                private $Funciones;
              
                public function __construct()
                {      self::$entityOperacion = DBDAO::getConnection();
                       $this->Funciones = self::$entityOperacion->MyFunction();
                }
    
                public function ObtenerOperacionByID($id)
                {      $DSQL = "SELECT id,descripcion FROM caja.medios_operacion WHERE id='$id' ORDER BY id ";                  
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                public function ObtenerOperacion($prepareDQL)
                {      $ORDN = array_key_exists("orden", $prepareDQL) ? $prepareDQL['orden'] : "id";
                       $DSQL = "SELECT id,descripcion FROM caja.medios_operacion ORDER BY ".$ORDN;                  
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
        }
