<?php
        require_once("src/base/DBDAO.php");

        class DAOTipo extends DBDAO
        {       private static $entityTipo;
                private $Funciones;
              
                public function __construct()
                {      self::$entityTipo = DBDAO::getConnection();
                       $this->Funciones = self::$entityTipo->MyFunction();
                }
    
                public function ObtenerTipoByID($id)
                {      $DSQL = "SELECT id, descripcion FROM caja.medios_tipo WHERE id='$id' ORDER BY id";                  
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }

                public function ObtenerTipo($PrepareDQL)
                {      $ORDN = array_key_exists("orden", $PrepareDQL) ? $PrepareDQL['orden'] : "id";
                       $DSQL = "SELECT id, descripcion FROM caja.medios_tipo ORDER BY ".$ORDN;                  
                       return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
                }
        }
?>
