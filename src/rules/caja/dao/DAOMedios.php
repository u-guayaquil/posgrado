<?php
        require_once("src/base/DBDAO.php");

        class DAOMedios extends DBDAO
        {   private static $entityMedio;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {       self::$entityMedio = DBDAO::getConnection();
                    $this->Funciones = self::$entityMedio->MyFunction();
                    $this->Fecha  = new DateControl();
            }
              
            function ObtenerMedio($PrepareDQL)
            {       $JOIN = "";
                    $JOIN.= array_key_exists("id",$PrepareDQL)  ? " and m.id =".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("tipo",$PrepareDQL) ? " and m.idcajatipo in (".$PrepareDQL['tipo'].")" : "";
                    $JOIN.= array_key_exists("oper",$PrepareDQL)  ? " and m.idcajaoper in (".$PrepareDQL['oper'].")" : "";
                    $JOIN.= array_key_exists("estado",$PrepareDQL) ? " and m.idestado   in (".$PrepareDQL['estado'].")" : "";
                    $TEXT = array_key_exists("texto",$PrepareDQL) ? " and upper(m.descripcion) like '%".$PrepareDQL['texto']."%'" : "";
                    $ORDR = array_key_exists("order",$PrepareDQL) ? $PrepareDQL['order'] : "m.id"; 
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite'] : "";
                    
                    $DSQL = "SELECT m.id,m.descripcion, "
                           ."m.idcajatipo,m.idcajaoper,m.fecreacion, "
                           ."m.idestado,e.descripcion as txtestado, "
                           ."o.descripcion as operacion,t.descripcion as tipo "
                           ."FROM caja.medios m, general.estado e, caja.medios_operacion o, caja.medios_tipo t "
                           ."WHERE m.idestado = e.id and "
                           ."      m.idcajaoper = o.id and "
                           ."      m.idcajatipo = t.id ".$JOIN.$TEXT
                           ."ORDER BY ".$ORDR." ".$LIMT;
                  
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));
            }
                         
            public function InsertaMedio($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $descripcion = strtoupper($datos->descripcion);
                    $Query = "INSERT INTO caja.medios (descripcion,idcajatipo,idcajaoper,idestado,iduscreacion,fecreacion) "
                           . "VALUES ('".$descripcion."', ".$datos->tipo.", ".$datos->operacion.", ".$datos->estado.",".$_SESSION['IDUSER'].",'".$fecha."') returning id";

                    $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                    return intval($id);
            }
    
            public function ActualizaMedio($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                            
                    $descripcion = strtoupper($datos->descripcion);
                    $Query = "UPDATE caja.medios "
                           . "SET descripcion='$descripcion', idcajatipo='$datos->tipo', idestado='$datos->estado', idcajaoper='$datos->operacion', idusmodifica=".$_SESSION['IDUSER'].", femodificacion='$fecha' "
                           . "WHERE id='$datos->id'";
                    
                    $this->Funciones->Query($Query);
                    return intval($datos->id);
            }
    
            public function DesactivaMedio($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE caja.medios SET idestado= 0, idusmodifica= ".$_SESSION['IDUSER'].", femodificacion= '".$fecha."' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }
              
        }
