<?php
        require_once("src/base/DBDAO.php");
        
        class DAOCajaGeneral extends DBDAO
        {     private static $entityCajaGeneral;
              private $Funciones;
              private $Fecha;
              
            public function __construct()
            {      self::$entityCajaGeneral = DBDAO::getConnection();
                   $this->Funciones = self::$entityCajaGeneral->MyFunction();
                   $this->Fecha  = new DateControl();
            }
            
            public function ObtenerDatosGeneral($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ? "     and g.id = ".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("estado", $PrepareDQL) ? " and g.idestado in (".$PrepareDQL['estado'].")" : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ? "  and upper(g.descripcion) LIKE '%".$PrepareDQL['texto']."%'" : "";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite']: "";
                    
                    $DSQL = "SELECT g.id,g.idctacontable,g.idsucursal, g.fecreacion,g.idestado,e.descripcion as txtestado, p.descripcion as cuentacontable, s.descripcion as sucursal,g.descripcion "
                           . "FROM caja.medios_cajageneral g, general.estado e, general.planctas p, general.sucursal s "
                           . "WHERE g.idestado=e.id and g.idctacontable=p.id and g.idsucursal=s.id ".$JOIN
                           . "ORDER BY g.id ".$LIMT;                  
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                             
            }

            public function InsertaCajaGeneral($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $descripcion = strtoupper($datos->descripcion);
                    
                    $Query = "INSERT INTO caja.medios_cajageneral (descripcion,idctacontable,idsucursal,idestado,iduscreacion,fecreacion) "
                           . "VALUES ('$descripcion','$datos->idCuentaCajaGeneral','$datos->idSucursal','$datos->estado',".$_SESSION['IDUSER'].",'$fecha') returning id ";
                    return $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
            }

            public function ActualizaCajaGeneral($datos)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');  
                    $descripcion = strtoupper($datos->descripcion);
                    
                    $Query = "UPDATE caja.medios_cajageneral "
                            ."SET descripcion='$descripcion',idctacontable='$datos->idCuentaCajaGeneral',idestado='$datos->estado',idsucursal='$datos->idSucursal',idusmodifica=".$_SESSION['IDUSER'].",femodificacion='$fecha' "
                            ."WHERE id= ".$datos->id;
                    $this->Funciones->Query($Query);
                    return intval($datos->id);
            }

            public function DesactivaCajaGeneral($id)
            {       $fecha = $this->Fecha->getNowDateTime('DATETIME');                        
                    $Query = "UPDATE caja.medios_cajageneral SET idestado= 0,idusmodifica= ".$_SESSION['IDUSER'].",femodificacion='$fecha' WHERE id= ".$id;
                    $this->Funciones->Query($Query);
                    return intval($id);
            }    
        }
?>        
