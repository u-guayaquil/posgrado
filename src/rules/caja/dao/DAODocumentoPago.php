<?php
        require_once("src/base/DBDAO.php");
        
        class DAODocumentoPago extends DBDAO
        {   private static $entityDocumentoPago;
            private $Funciones;
            private $Fecha;
              
            public function __construct()
            {      self::$entityDocumentoPago = DBDAO::getConnection();
                   $this->Funciones = self::$entityDocumentoPago->MyFunction();
                   $this->Fecha  = new DateControl();
            }
            
            public function ObtenerDatosGeneral($PrepareDQL)
            {       $JOIN = array_key_exists("id", $PrepareDQL) ? "    and d.id =".$PrepareDQL['id'] : "";
                    $JOIN.= array_key_exists("texto", $PrepareDQL) ? " and upper(t.descripcion) LIKE '%".$PrepareDQL['texto']."%'": "";
                    $ORDN = array_key_exists("orden", $PrepareDQL) ? $PrepareDQL['orden'] : "d.id";
                    $LIMT = array_key_exists("limite", $PrepareDQL) ? " LIMIT ".$PrepareDQL['limite'] : "";
                    
                    $DSQL = "SELECT d.id, ";
                    $DSQL.= "m.descripcion, ";
                    $DSQL.= "d.idcajapag, p.descripcion as mpago, ";
                    $DSQL.= "p.idsubtipo, s.descripcion as subtipo, ";
                    $DSQL.= "d.idcajadoc, t.descripcion as documento, ";
                    $DSQL.= "d.ctrlsecudoc, d.iniciosecdoc, ";
                    $DSQL.= "d.formatodoc, t.idcajatipo ";
                    $DSQL.= "FROM caja.documentos_pagos d, caja.medios_pagos p, caja.medios m, caja.documentos t, caja.medios_subtipo s ";
                    $DSQL.= "WHERE d.idcajapag=p.id and p.idcajamed=m.id and d.idcajadoc=t.id and p.idsubtipo=s.id ".$JOIN;
                    $DSQL.= "ORDER BY ".$ORDN.$LIMT;
                            
                    return $this->Funciones->RecorreDatos($this->Funciones->Query($DSQL));                                      
            }
            
            public function InsertaDocumentoPago($datos)
            {           $formato = strtolower(trim($datos->formato));
                        $secuens = intval($datos->secuencial);
                        $Query = "INSERT INTO caja.documentos_pagos (idcajapag,idcajadoc,formatodoc,ctrlsecudoc,iniciosecdoc) "
                               . "VALUES ('$datos->idmpago','$datos->documento','$formato','$datos->CTRL','$secuens') returning id ";
                        $id = $this->Funciones->FieldDataByName($this->Funciones->Query($Query),"id");
                        return intval($id);
            }

            public function ActualizaDocumentoPago($datos)
            {           $formato = strtolower(trim($datos->formato));
                        $secuens = intval($datos->secuencial);
                        $Query = "UPDATE caja.documentos_pagos "
                               . "SET "
                               . "idcajapag = '$datos->idmpago', "
                               . "idcajadoc = '$datos->documento', " 
                               . "formatodoc= '$formato', " 
                               . "ctrlsecudoc= '$datos->CTRL', "
                               . "iniciosecdoc= '$secuens' "
                               . "WHERE id= '$datos->id'";
                        $this->Funciones->Query($Query);
                        return intval($datos->id);
            }

            public function DesactivaDocumentoPago($id)
            {       $this->Funciones->Query("Delete From caja.documentos_pagos Where id='$id'");
                    return $id;
            }            
            
        }
?>        
