<?php
require_once("src/base/EntidadGlobal.php");
require_once("src/rules/caja/entidad/Operacion.php");
require_once("src/rules/caja/entidad/Tipo.php");

    /**
     * @Entity
     * @Table(name="medio", schema="caja")
     */
class Medio extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=10)
    */
    private $descripcion = '';

    /**
     * @Column(name="idcajatipo", type="integer")
     */
    private $idcajatipo;

    /**
      * @Column(name="idcajaoper", type="integer")
    */
    private $idcajaoper;
    
    /**
     * @ManyToOne(targetEntity="Operacion")
     * @JoinColumn(name="idcajaoper", referencedColumnName="id", nullable=false)
     */
    private $Operaciones;
    
    /**
     * @ManyToOne(targetEntity="Tipo")
     * @JoinColumn(name="idcajatipo", referencedColumnName="id", nullable=false)
     */
    private $TiposMedios;

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function setIdCajaTipo($idcajatipo)
    {      $this->idcajatipo = $idcajatipo;
    }

    public function getIdCajaTipo()
    {      return $this->idcajatipo;
    }
    
    public function setIdCajaOper($idcajaoper)
    {      $this->idcajaoper = $idcajaoper;
    }

    public function getIdCajaOper()
    {      return $this->idcajaoper;
    }    

    public function setOperaciones(Operacion $Operaciones)
    {
        $this->Operaciones = $Operaciones;
    }

    public function getOperaciones()
    {
        return $this->Operaciones;
    }
    
    public function setTiposMedios(Tipo $TiposMedios)
    {
        $this->TiposMedios = $TiposMedios;
    }

    public function getTiposMedios()
    {
        return $this->TiposMedios;
    }
}

