<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/general/entidad/Sucursal.php");
    require_once("src/rules/general/entidad/PlanCuentas.php");

    /**
     * @Entity
     * @Table(name="general",schema ="caja")
     */
    class CajaGeneral extends EntidadGlobal 
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="descripcion", type="string")
         */
        private $descripcion;

        
        /**
         * @Column(name="idctacontable", type="integer", nullable=false)
         */
        private $idCtaContable;
        
        /**
         * @Column(name="idsucursal", type="integer", nullable=false)
         */
        private $idSucursal;
        
        /**
        * @ManyToOne(targetEntity="Sucursal")
        * @JoinColumn(name="idsucursal", referencedColumnName="id", nullable=false)
        */        
        private $Sucursales;
        
        /**
        * @ManyToOne(targetEntity="PlanCuentas")
        * @JoinColumn(name="idctacontable", referencedColumnName="id", nullable=false)
        */        
        private $CuentasContables;

        public function getId()
        {      return $this->id;
        }

        function getDescripcion() {
            return $this->descripcion;
        }

        function getIdCtaContable() {
            return $this->idCtaContable;
        }

        function getIdSucursal() {
            return $this->idSucursal;
        }

        function getSucursales() {
            return $this->Sucursales;
        }

        function getCuentasContables() {
            return $this->CuentasContables;
        }

        function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }

        function setIdCtaContable($idCtaContable) {
            $this->idCtaContable = $idCtaContable;
        }

        function setIdSucursal($idSucursal) {
            $this->idSucursal = $idSucursal;
        }

        function setSucursales(Sucursal $Sucursales) {
            $this->Sucursales = $Sucursales;
        }

        function setCuentasContables(PlanCuentas $CuentasContables) {
            $this->CuentasContables = $CuentasContables;
        }


    }
