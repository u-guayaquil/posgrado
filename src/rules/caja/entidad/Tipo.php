<?php 
      require_once("src/rules/caja/entidad/Documento.php");
      require_once("src/rules/caja/entidad/Medio.php");
     /**
     * @Entity
     * @Table(name="tipo", schema="caja")
     */
class Tipo
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion;
    
     /**
     * @OneToMany(targetEntity="Documento",
     *            mappedBy="Tipos")
     */
    private $Tipos;
    
    /**
     * @OneToMany(targetEntity="Medio",
     *            mappedBy="TiposMedios")
     */
    private $TiposMedios;


    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function getTipos(){
	return $this->Tipos;
    }
	
    public function setTipos(Documento $Tipos){
           $this->Tipos = $Tipos;
    }    
    
    public function getTiposMedios(){
	return $this->TiposMedios;
    }
	
    public function setTiposMedios(Medio $TiposMedios){
           $this->TiposMedios = $TiposMedios;
    }
}

