<?php
/**
     * @Entity
     * @Table(name="validacion", schema="caja")
     */
class Validacion
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion;
    
     /**
     * @OneToMany(targetEntity="Documento",
     *            mappedBy="Documentos")
     */
    private $Validaciones;


    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function getValidaciones(){
	return $this->Validaciones;
    }
	
    public function setValidaciones(Documento $Validaciones){
           $this->Validaciones = $Validaciones;
    }
}

