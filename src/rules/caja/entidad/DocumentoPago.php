<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/caja/entidad/Documento.php");
    require_once("src/rules/caja/entidad/Pago.php");
    

    /**
     * @Entity
     * @Table(name="documento_pago",schema ="caja")
     */
    class DocumentoPago
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="formatodoc", type="string")
         */
        private $FormatoDoc;
        
        /**
         * @Column(name="ctrlsecudoc", type="integer", nullable=false)
         */
        private $CtrlSecuDoc;
        
        /**
         * @Column(name="iniciosecdoc", type="integer", nullable=false)
         */
        private $InicioSecDoc;
        
        /**
         * @Column(name="idcajapag", type="integer", nullable=false)
         */
        private $idCajaPago;
        
        /**
         * @Column(name="idcajadoc", type="integer", nullable=false)
         */
        private $idCajaDoc;
        
        /**
        * @ManyToOne(targetEntity="Pago")
        * @JoinColumn(name="idcajapag", referencedColumnName="id", nullable=false)
        */        
        private $Pagos;
        
        /**
        * @ManyToOne(targetEntity="Documento")
        * @JoinColumn(name="idcajadoc", referencedColumnName="id", nullable=false)
        */        
        private $Documentos;

        public function getId()
        {      return $this->id;
        }
        
        function getFormatodoc() {
            return $this->FormatoDoc;
        }

        function getCtrlSecuDoc() {
            return $this->CtrlSecuDoc;
        }

        function getInicioSecDoc() {
            return $this->InicioSecDoc;
        }

        function getIdCajaPago() {
            return $this->idCajaPago;
        }

        function getIdCajaDoc() {
            return $this->idCajaDoc;
        }

        function getPagos() {
            return $this->Pagos;
        }

        function getDocumentos() {
            return $this->Documentos;
        }

        function setFormatodoc($formatodoc) {
            $this->FormatoDoc = $formatodoc;
        }

        function setCtrlSecuDoc($CtrlSecuDoc) {
            $this->CtrlSecuDoc = $CtrlSecuDoc;
        }

        function setInicioSecDoc($InicioSecDoc) {
            $this->InicioSecDoc = $InicioSecDoc;
        }

        function setIdCajaPago($idCajaPago) {
            $this->idCajaPago = $idCajaPago;
        }

        function setIdCajaDoc($idCajaDoc) {
            $this->idCajaDoc = $idCajaDoc;
        }

        function setPagos(Pago $Pagos) {
            $this->Pagos = $Pagos;
        }

        function setDocumentos(Documento $Documentos) {
            $this->Documentos = $Documentos;
        }


    }
