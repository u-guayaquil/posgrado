<?php
require_once("src/base/EntidadGlobal.php");
require_once("src/rules/caja/entidad/Validacion.php");
require_once("src/rules/caja/entidad/Tipo.php");

    /**
     * @Entity
     * @Table(name="documento", schema="caja")
     */
class Documento extends EntidadGlobal
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=10)
    */
    private $descripcion = '';

    /**
     * @Column(name="idcajatipo", type="integer")
     */
    private $idcajatipo;

    /**
      * @Column(name="idcajaval", type="integer")
    */
    private $idcajaval;
    
    /**
     * @ManyToOne(targetEntity="Validacion")
     * @JoinColumn(name="idcajaval", referencedColumnName="id", nullable=false)
     */
    private $Validaciones;
    
    /**
     * @ManyToOne(targetEntity="Tipo")
     * @JoinColumn(name="idcajatipo", referencedColumnName="id", nullable=false)
     */
    private $Tipos;

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function setIdCajaTipo($idcajatipo)
    {      $this->idcajatipo = $idcajatipo;
    }

    public function getIdCajaTipo()
    {      return $this->idcajatipo;
    }
    
    public function setIdCajaVal($idcajaval)
    {      $this->idcajaval = $idcajaval;
    }

    public function getIdCajaVal()
    {      return $this->idcajaval;
    }
    

    public function setValidaciones(Validacion $Validaciones)
    {
        $this->Validaciones = $Validaciones;
    }

    public function getValidaciones()
    {
        return $this->Validaciones;
    }
    
    public function setTipos(Tipo $Tipos)
    {
        $this->Tipos = $Tipos;
    }

    public function getTipos()
    {
        return $this->Tipos;
    }
}

