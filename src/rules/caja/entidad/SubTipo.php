<?php
require_once("src/rules/caja/entidad/Tipo.php");
/**
  * @Entity
  * @Table(name="subtipo", schema="caja")
*/
class SubTipo
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion;
    
    /**
      * @ManyToOne(targetEntity="Tipo")
      * @JoinColumn(name="idtipo", referencedColumnName="id", nullable=false)
    */        
    private $Tipos;

    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    function getTipos() {
        return $this->Tipos;
    }

    function setTipos(Tipo $Tipos) {
        $this->Tipos = $Tipos;
    }


}

