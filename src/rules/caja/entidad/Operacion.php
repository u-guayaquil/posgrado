<?php
/**
     * @Entity
     * @Table(name="operacion", schema="caja")
     */
class Operacion
{
    /**
      * @Id
      * @Column(name="id", type="integer")
      * @GeneratedValue
    */
    private $id;

    /**
      * @Column(name="descripcion", type="string", length=20)
    */
    private $descripcion;
    
     /**
     * @OneToMany(targetEntity="Medio",
     *            mappedBy="Operaciones")
     */
    private $Operacion;
    


    public function getId()
    {
        return $this->id;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function getOperacion(){
	return $this->Operacion;
    }
	
    public function setOperacion(Medio $Operacion){
           $this->Operacion = $Operacion;
    }
}

