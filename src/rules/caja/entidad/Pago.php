<?php

    require_once("src/base/EntidadGlobal.php");
    require_once("src/rules/caja/entidad/Medio.php");
    require_once("src/rules/caja/entidad/SubTipo.php");
    require_once("src/rules/general/entidad/PlanCuentas.php");
    require_once("src/rules/general/entidad/Sucursal.php");

    /**
     * @Entity
     * @Table(name="pago",schema ="caja")
     */
    class Pago extends EntidadGlobal 
    {
        /**
         * @Id
         * @Column(name="id", type="integer", nullable=false)
         * @GeneratedValue
         */
        private $id;

        /**
         * @Column(name="descripcion", type="string")
         */
        private $descripcion;
        
        /**
        * @ManyToOne(targetEntity="Sucursal")
        * @JoinColumn(name="idsucursal", referencedColumnName="id", nullable=false)
        */        
        private $Sucursales;
        
        /**
        * @ManyToOne(targetEntity="PlanCuentas")
        * @JoinColumn(name="idctacontable", referencedColumnName="id", nullable=false)
        */        
        private $CuentasContables;
        
        /**
        * @ManyToOne(targetEntity="Medio")
        * @JoinColumn(name="idcajamed", referencedColumnName="id", nullable=false)
        */        
        private $Medios;
        
        /**
        * @ManyToOne(targetEntity="SubTipo")
        * @JoinColumn(name="idsubtipo", referencedColumnName="id", nullable=false)
        */        
        private $SubTipo;

        public function getId()
        {      return $this->id;
        }

        function getDescripcion() {
            return $this->descripcion;
        }

        function getIdCtaContable() {
            return $this->idCtaContable;
        }

        function getIdSucursal() {
            return $this->idSucursal;
        }

        function getSucursales() {
            return $this->Sucursales;
        }

        function getCuentasContables() {
            return $this->CuentasContables;
        }

        function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }

        function setIdCtaContable($idCtaContable) {
            $this->idCtaContable = $idCtaContable;
        }

        function setIdSucursal($idSucursal) {
            $this->idSucursal = $idSucursal;
        }

        function setSucursales(Sucursal $Sucursales) {
            $this->Sucursales = $Sucursales;
        }

        function setCuentasContables(PlanCuentas $CuentasContables) {
            $this->CuentasContables = $CuentasContables;
        }

        function getIdCajaMedio() {
            return $this->idCajaMedio;
        }

        function getMedios() {
            return $this->Medios;
        }

        function setIdCajaMedio($idCajaMedio) {
            $this->idCajaMedio = $idCajaMedio;
        }

        function setMedios(Medio $Medios) {
            $this->Medios = $Medios;
        }
        
        function getIdSubTipo() {
            return $this->idSubTipo;
        }

        function setIdSubTipo($idSubTipo) {
            $this->idSubTipo = $idSubTipo;
        }

        function getSubTipo() {
            return $this->SubTipo;
        }

        function setSubTipo(SubTipo $SubTipo) {
            $this->SubTipo = $SubTipo;
        }



    }
