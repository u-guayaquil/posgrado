<?php
            require_once("src/rules/caja/dao/DAOValidacion.php");

            class ServicioValidacion
            {   private $DAOValidacion;
     
                function __construct()
                {        $this->DAOValidacion = new DAOValidacion();
                }
           
                function BuscarValidacionByID($id)
                {        return $this->DAOValidacion->ObtenerValidacionByID(intval($id));
                }

                function BuscarValidacionTodos($PrepareDQL=array())
                {        return $this->DAOValidacion->ObtenerValidacion($PrepareDQL);
                }
                
            }
?>

