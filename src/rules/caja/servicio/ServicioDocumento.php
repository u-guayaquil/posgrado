<?php
            require_once("src/rules/caja/dao/DAODocumento.php");
            
            class ServicioDocumento
            {     private $DAODocumento;
     
                  function __construct()
                  {        $this->DAODocumento = new DAODocumento();
                  }
                  
                  function GuardaDBDocumento($Form)
                  {       if (empty($Form->id)){
                              return $this->DAODocumento->InsertaDocumento($Form);
                            }else{
                              return $this->DAODocumento->ActualizaDocumento($Form);
                            }
                  }
                  
                  function DesactivaDocumento($Id)
                  {        return $this->DAODocumento->DesactivaDocumento(intval($Id)); 
                  }
           
                  function BuscarDocumentoByID($prepareDQL)
                  {        return $this->DAODocumento->BuscarDocumento($prepareDQL);
                  }

                  function BuscarDocumentoByDescripcion($prepareDQL)
                  {        return $this->DAODocumento->BuscarDocumento($prepareDQL);                        
                  }
                  
            }
?>

