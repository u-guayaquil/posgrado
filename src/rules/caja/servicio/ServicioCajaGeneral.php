<?php

            require_once("src/rules/caja/dao/DAOCajaGeneral.php");
            require_once("src/rules/caja/entidad/CajaGeneral.php");
            
            class ServicioCajaGeneral 
            {       private $DAOCajaGeneral;
     
                    function __construct()
                    {        $this->DAOCajaGeneral = new DAOCajaGeneral();
                    }
                    
                    function GuardaDBCajaGeneral($Form)
                  {       if (empty($Form->id)){
                              return $this->DAOCajaGeneral->InsertaCajaGeneral($Form);
                            }else{
                              return $this->DAOCajaGeneral->ActualizaCajaGeneral($Form);
                            }
                  }
                    
                    function DesactivaCajaGeneral($id)
                    {       return $this->DAOCajaGeneral->DesactivaCajaGeneral(intval($id)); 
                    }
                    
                    function BuscarCajaGeneralByID($PrepareDQL)
                    {       return $this->DAOCajaGeneral->ObtenerDatosGeneral($PrepareDQL);
                    }

                    function BuscarCajaGeneralByDescripcion($PrepareDQL)
                    {       return $this->DAOCajaGeneral->ObtenerDatosGeneral($PrepareDQL);
                    }
                    
                    function BuscarCajaGeneralEstadoByID($id)
                    {       return $this->DAOCajaGeneral->ObtenerDatosGeneral($id);
                    }

}        
?>        
