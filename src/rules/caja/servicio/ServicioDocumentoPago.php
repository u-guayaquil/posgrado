<?php
            require_once("src/rules/caja/dao/DAODocumentoPago.php");
            
            class ServicioDocumentoPago 
            {       private $DAODocumentoPago;
     
                    function __construct()
                    {        $this->DAODocumentoPago = new DAODocumentoPago();
                    }
                    
                    function GuardaDBDocumentoPago($Form)
                    {       if (intval($Form->CTRL)==1 && trim($Form->secuencial)=="")
                                return "Debe inicializar la secuencia del documento.";
                            else{
                                if (empty($Form->id))
                                return $this->DAODocumentoPago->InsertaDocumentoPago($Form);
                                else
                                return $this->DAODocumentoPago->ActualizaDocumentoPago($Form);
                            }    
                    }
                    
                    function DesactivaDocumentoPago($id)
                    {       return $this->DAODocumentoPago->DesactivaDocumentoPago(intval($id)); 
                    }
                    
                    function BuscarDocumentoPagoByID($PrepareDQL)
                    {       return $this->DAODocumentoPago->ObtenerDatosGeneral($PrepareDQL);
                    }

                    function BuscarDocumentoPagoByDescripcion($PrepareDQL)
                    {       return $this->DAODocumentoPago->ObtenerDatosGeneral($PrepareDQL);
                    }
                    
}        
?>        
