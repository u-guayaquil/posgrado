<?php
            require_once("src/rules/caja/dao/DAOSubTipo.php");
            
            class ServicioSubTipo
            {       private $DAOSubTipo;
     
                    function __construct()
                    {        $this->DAOSubTipo = new DAOSubTipo();
                    }
           
                    function BuscarSubTipoByID($id)
                    {        return $this->DAOSubTipo->ObtenerSubTipoByID(intval($id));
                    }
                  
                    function BuscarSubTipoTodos($PrepareDQL)
                    {        return $this->DAOSubTipo->ObtenerSubTipo($PrepareDQL);
                    }
                
                    function BuscarSubTipoMedios($IdMedio)
                    {        return $this->DAOSubTipo->ObtenerSubTipoMedios(intval($IdMedio));
                    }
                
            }
?>

