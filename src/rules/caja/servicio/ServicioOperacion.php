<?php
            require_once("src/rules/caja/dao/DAOOperacion.php");
            
            class ServicioOperacion
            {       private $DAOOperacion;
     
                    function __construct()
                    {        $this->DAOOperacion = new DAOOperacion();
                    }
           
                    function BuscarOperacionByID($id)
                    {        return $this->DAOOperacion->ObtenerOperacionByID($id);
                    }

                    function BuscarOperacionTodos($prepareDQL=array())
                    {        return $this->DAOOperacion->ObtenerOperacion($prepareDQL);
                    }
            }
?>

