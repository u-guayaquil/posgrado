<?php
            require_once("src/rules/caja/dao/DAOMediosPago.php");
            
            class ServicioMediosPago 
            {       private $DAOMediosPago;
     
                    function __construct()
                    {        $this->DAOMediosPago = new DAOMediosPago();
                    }
                  
                    function GuardaDBPago($Form)
                    {       if (empty($Form->id)){
                              return $this->DAOMediosPago->InsertaPago($Form);
                            }else{
                              return $this->DAOMediosPago->ActualizaPago($Form);
                            }
                    }
                    
                    function DesactivaPago($id)
                    {       return $this->DAOMediosPago->DesactivaPago(intval($id)); 
                    }
                    
                    function BuscarPagoByID($prepareDQL)
                    {       return $this->DAOMediosPago->ObtenerDatosGeneral($prepareDQL);
                    }

                    function BuscarPagoByDescripcion($prepareDQL)
                    {       return $this->DAOMediosPago->ObtenerDatosGeneral($prepareDQL);
                    }
}        
?>        
