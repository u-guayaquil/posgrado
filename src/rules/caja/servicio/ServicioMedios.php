<?php
            require_once("src/rules/caja/dao/DAOMedios.php");

            
            class ServicioMedios
            {       private $DAOMedios;
     
                    function __construct()
                    {        $this->DAOMedios = new DAOMedios();
                    }
           
                    function GuardaDBMedio($Form)
                    {       if (empty($Form->id)){
                              return $this->DAOMedios->InsertaMedio($Form);
                            }else{
                              return $this->DAOMedios->ActualizaMedio($Form);
                            }
                    }
                  
                    function DesactivaMedio($Id)
                    {        return $this->DAOMedios->DesactivaMedio($Id);
                    }
           
                    function BuscarMedioByID($prepareDQL)
                    {        return $this->DAOMedios->ObtenerMedio($prepareDQL);
                    }

                    function BuscarMedioByDescripcion($prepareDQL)
                    {        return $this->DAOMedios->ObtenerMedio($prepareDQL);                        
                    }
                  
                    function BuscarMedioByArray($prepareDQL)
                    {        return $this->DAOMedios->ObtenerMedio($prepareDQL);
                    }
            }
?>

