<?php
            require_once("src/rules/caja/dao/DAOTipo.php");

            
            class ServicioTipo
            {       private $DAOTipo;
     
                    function __construct()
                    {        $this->DAOTipo = new DAOTipo();
                    }
           
                    function BuscarTipoByID($id)
                    {        return $this->DAOTipo->ObtenerTipoByID($id);
                    }

                    function BuscarTipoTodos($PrepareDQL=array())
                    {        return $this->DAOTipo->ObtenerTipo($PrepareDQL);
                    }
            }
?>

