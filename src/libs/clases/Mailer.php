<?php
        require_once("src/libs/plugins/mailer/src/PHPMailer.php");
        require_once("src/libs/plugins/mailer/src/SMTP.php"); 
        require_once("src/libs/clases/File.php");
    
        class  Mailer //extends DAO
        {       private $ObjFile;
                private $ObjMail;
                private $FrmName;
                private $Subject;
                private $Destiny;
                private $BodyHTM;
                private $EmbImag = array();
                private $CopyTo;
                private $Utils;
                private $Cuenta = "webmaster.posgrado@ug.edu.ec";
                private $Tokens = "714588567665658686231217224280175";
                                  

                function __construct()
                {       $this->ObjFile = new File;
                        $this->Utils = new Utils();
                }

                function setFromName ($frmn) {  $this->FrmName   = $frmn;  }
                function setSubject  ($subj) {  $this->Subject   = $subj;  }
                function setBodyHTML ($html) {  $this->BodyHTM   = $html;  }
                function addTo       ($toem) {  $this->Destiny   = $toem;  }
                function addCopyTo   ($tocp) {  $this->CopyTo    = $tocp;  }

                function addEmbImag($pathImg,$imgName)
                {       list($cidm,$type) = $this->ObjFile->typeImg($imgName);
                        if (trim($cidm)!=="" && trim($type)!=="")
                        $this->EmbImag[] = array($pathImg.$imgName,$cidm,$type);
                }

                protected function EmailDecoder($cuentas)
                {       $vcorreos = array();
                        $elements = explode(';',$cuentas);
                        for ($m=0; $m<count($elements); $m++)
                        {   if (filter_var($elements[$m], FILTER_VALIDATE_EMAIL))
                            {   $vcorreos [] = trim($elements[$m]);
                            }
                        }
                        return $vcorreos;
                }

                function sendMail()
                {       $ecorreos = $this->EmailDecoder($this->Destiny);
                        if (count($ecorreos)>0)
                        {   $this->ObjMail = new PHPMailer();
                            $this->ObjMail->SetLanguage("en", 'src/libs/plugins/mailer/language/'); 
                            $this->ObjMail->IsSMTP();
                            
                            $this->ObjMail->Host = 'smtp.office365.com';   
                            $this->ObjMail->Port = 587;                    
                            $this->ObjMail->SMTPSecure = 'tls';
                            $this->ObjMail->SMTPAuth = true;
                            $this->ObjMail->Username = $this->Cuenta; 
                            $this->ObjMail->Password = $this->Utils->Desencriptar($this->Tokens);
                            $this->ObjMail->From = $this->Cuenta; 
                            $this->ObjMail->FromName = $this->FrmName;
                            $this->ObjMail->Timeout = 30;

                            for ($m=0; $m<count($ecorreos); $m++)
                            {    $this->ObjMail->AddAddress($ecorreos[$m]);
                            }

                            $ccorreos = $this->EmailDecoder($this->CopyTo);
                            for ($m=0; $m<count($ccorreos); $m++)
                            {    $this->ObjMail->AddCC($ccorreos[$m]);
                            }

                            $this->ObjMail->Subject = $this->Subject;
                            $this->ObjMail->IsHTML(true);
/*
                            for ($k=0; $k<count($this->EmbImag); $k++)
                            {   $file = trim($this->EmbImag[$k][0]);
                                $cidm = trim($this->EmbImag[$k][1]);
                                $type = trim($this->EmbImag[$k][2]);
                                $this->ObjMail->AddEmbeddedImage($file,$cidm,'','base64','image/'.$type);
                            }
*/
                            $this->ObjMail->Body = trim($this->BodyHTM);
                            return ($this->ObjMail->Send() ? array(true,"CMD_MAIL_ENVIADO"): array(false,$this->ObjMail->ErrorInfo)); 
                        }
                        return array(false,"No existen destinatarios del correo.");
                }
        }
?>


