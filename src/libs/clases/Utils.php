<?php
        
        class Utils
        {   private $HorOficial = 1;
            private $MinOficial = 0;
           
            function GetHorOficial()
            {
                    return $this->HorOficial; 
            }

            function GetMinOficial()
            {
                    return $this->MinOficial; 
            }

            
            function ConvertTildesToUpper($Pharse)
            {        $Pharse = strtoupper(trim($Pharse));
                     $minesp = array('é','á','í','ó','ú','ñ','ü','ä');
                     $mayesp = array('É','Á','Í','Ó','Ú','Ñ','Ü','Ä');
                     return str_replace($minesp,$mayesp, $Pharse);
            }
            
            function ConvertTildesToLower($Pharse)
            {        $minesp = array('é','á','í','ó','ú','ñ','ü','ä');
                     $mayesp = array('É','Á','Í','Ó','Ú','Ñ','Ü','Ä');
                     return str_replace($mayesp,$minesp, $Pharse);
            }

            
            function numberToOperation($number=0)
            {        return doubleval(str_replace(",","",$number));
            }
            
            function numberToFormat($number=0,$decimal=2)
            {        return number_format($number,$decimal,".",",");
            }
            
            function retornaRandCaracteresEspeciales($Cantidad)
            {       $espchar = "";
                    $element = array('@','#','!','*','&','%','$','-','?','_','+','¡','.');
                    for ($i = 1; $i <= $Cantidad; $i++)
                    {    $espchar = $espchar.$element[rand(0,12)];
                    }
                    return $espchar;
            }
            
            function retornaRandNumeros($Cantidad)
            {       $numchar = "";
                    for ($i = 1; $i <= $Cantidad; $i++)
                    {   $numchar = $numchar.rand(0,9);
                    }
                    return $numchar;
            }
            
            function retornaRandCaracteres($Rango,$Cantidad)
            {       $maychar = "";
                    if ($Cantidad>0)
                    {   $element = array_flip(array_merge($Rango));
                        if ($Cantidad == 1)
                            $maychar = array_rand($element,1);
                        else
                            $maychar = implode("",array_rand($element, $Cantidad));
                    }    
                    
                    if($maychar=='Â')
                        $this->retornaRandCaracteres($Rango,$Cantidad);

                    return $maychar;
            }
             
            function retornaRemoteIPAddress()
            {       if (!empty($_SERVER['HTTP_CLIENT_IP']))
   	                return $_SERVER['HTTP_CLIENT_IP'];
             	    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		        return $_SERVER['HTTP_X_FORWARDED_FOR'];
                    else
                        return $_SERVER['REMOTE_ADDR'];
            }
            
            function validaIndentificacion($Tipo,$Numero)
            {       $longitud = strlen($Numero);
                    switch ($Tipo)
                    {       case 1:
                                if ($longitud == 13) 
                                    return $this->AlgoritmoIdentificacion($Numero);    
                                else
                                    return array(false,'Debe ingresar un RUC correcto.');            
                                break;
                            case 2:
                                if (!is_numeric($Numero))
                                {   if ($longitud >=6)
                                    return array(true,'La cédula es de extrangero.');
                                }  
                                else if($Numero == '0961856374')
                                    return array(true,'La cédula es de extrangero.'); 
                                else if ($longitud == 10) 
                                     return $this->AlgoritmoIdentificacion($Numero); 
                                else if ($longitud == 9) 
                                    return array(true,'La cédula es de extrangero.');    
                                
                                return array(false,'Debe ingresar una cédula correcta.');
                                break;
                            case 3:
                                if ($longitud == 10) 
                                    return $this->AlgoritmoIdentificacion($Numero);    
                                else
                                    return array(false,'Debe ingresar una pasaporte correcto.');            
                                break;
                            default:  
                                return array(true,'No hay existe algoritmo de validación.');            
                                break;
                    }            
            }
                        
            private function AlgoritmoIdentificacion($documento)
            {       $Suma = 0;
                    $Residuo = 0;

                    $Provincia = 24;
                    $documento = str_replace(" ","",$documento);
                    $documento = str_replace("-","",$documento);
                    $documento = str_replace(".","",$documento);
                    $documento = str_replace("E","",$documento);
                    $documento = str_replace("+","",$documento);
                    $documento = str_replace(" ","",$documento);

                    if (strlen($documento) >= 10)
                    {   $c1  = substr($documento, 0, 1);
                        $c2  = substr($documento, 1, 1);
                        $c3  = substr($documento, 2, 1);
                        $c4  = substr($documento, 3, 1);
                        $c5  = substr($documento, 4, 1);
                        $c6  = substr($documento, 5, 1);
                        $c7  = substr($documento, 6, 1);
                        $c8  = substr($documento, 7, 1);
                        $c9  = substr($documento, 8, 1);
                        $c10 = substr($documento, 9, 1);

                        if ($c3 <= 5)
                        {   if (intval(substr($documento, 0, 2)) > 0 && intval(substr($documento, 0, 2)) <= $Provincia)
                            {   $p1  = $c1 * 2;
                                $p2  = $c2 * 1;
                                $p3  = $c3 * 2;
                                $p4  = $c4 * 1;
                                $p5  = $c5 * 2;
                                $p6  = $c6 * 1;
                                $p7  = $c7 * 2;
                                $p8  = $c8 * 1;
                                $p9  = $c9 * 2;
                                $p10 = $c10 * 1;
                                if ($p1 >= 10)  $p1 = $p1 - 9;
                                if ($p2 >= 10)  $p2 = $p2 - 9;
                                if ($p3 >= 10)  $p3 = $p3 - 9;
                                if ($p4 >= 10)  $p4 = $p4 - 9;
                                if ($p5 >= 10)  $p5 = $p5 - 9;
                                if ($p6 >= 10)  $p6 = $p6 - 9;
                                if ($p7 >= 10)  $p7 = $p7 - 9;
                                if ($p8 >= 10)  $p8 = $p8 - 9;
                                if ($p9 >= 10)  $p9 = $p9 - 9;
                                if ($p10>= 10)  $p10 = $p10 - 9;

                                $Suma = $p1 + $p2 + $p3 + $p4 + $p5 + $p6 + $p7 + $p8 + $p9 + $p10;
                                $Residuo = $Suma % 10;

                                if (strlen($documento) == 10)
                                {   if ($Residuo == 0)
                                        return array(true,"La cédula es válida.");
                                    else
                                        return array(false,"La cédula no es válida.");
                                }
                                elseif (strlen($documento) > 10)
                                {       if ($Residuo == 0)
                                        {   if (intval(substr($documento, 10, 3)) == 1 && strlen(substr($documento, 10, 3)) == 3)
                                            {   if (strlen(substr($documento, 10, 3)) == strlen(substr($documento, 10, 3)))
                                                    return array(true,"El RUC es válido.");
                                                else
                                                    return array(false,"Número de RUC correspondiente a Persona Natural es incorrecto.");
                                            }
                                            else
                                            return array(false,"Número de RUC correspondiente a Persona Natural es incorrecto.");
                                        }
                                        else
                                        {   if ($Residuo == 0)
                                            {   if (strlen(substr($documento, 10, 3)) == strlen(substr($documento, 10, 3)))
                                                    return array(true,"El RUC es válido.");
                                                else
                                                    return array(false,"Número de RUC correspondiente a Persona Natural es incorrecto.");
                                            }
                                            else
                                            return array(false,"Número de RUC correspondiente a Persona Natural es incorrecto.");
                                        }
                                }
                            }
                            else
                            return array(false,"RUC de Persona Natural es incorrecto, Verificar los digitos de la Provincia.");
                        }

                        if ($c3 == 9)
                        {   if (intval(substr($documento, 0, 2)) > 0 && intval(substr($documento, 0, 2)) <= $Provincia)
                            {   if (strlen($documento) == 13)
                                {   $p1 = $c1 * 4;
                                    $p2 = $c2 * 3;
                                    $p3 = $c3 * 2;
                                    $p4 = $c4 * 7;
                                    $p5 = $c5 * 6;
                                    $p6 = $c6 * 5;
                                    $p7 = $c7 * 4;
                                    $p8 = $c8 * 3;
                                    $p9 = $c9 * 2;
                                    $Suma = $p1 + $p2 + $p3 + $p4 + $p5 + $p6 + $p7 + $p8 + $p9;

                                    $valDivision = int($Suma / 11);
                                    $valMultiplicacion = $valDivision * 11;
                                    $valResta = $Suma - $valMultiplicacion;

                                    if ($valResta == 0)
                                        $digitoVerificador = 0;
                                    else
                                        $digitoVerificador = 11 - $valResta;

                                    if (intval($digitoVerificador) == intval($c10) && intval(substr($documento, 10, 3)) == 1)
                                        return array(true,"Numero de Ruc correspondiente a Persona Jurídica es correcto.");
                                    else
                                        return array(false,"Número de RUC correspondiente a Persona Jurídica es incorrecta.");
                                }
                                else
                                return array(false,"Número de RUC correspondiente a Persona Jurídica es incorrecta.");
                            }
                            else
                            return array(false,"RUC de Persona Jurídica incorrecto, verificar los digitos de la Provincia.");
                        }

                        if ($c3 == 6)
                        {   if (intval(substr($documento, 0, 2)) > 0 && intval(substr($documento, 0, 2)) <= $Provincia)
                            {   if (strlen($documento) == 13)
                                {   $p1 = $c1 * 3;
                                    $p2 = $c2 * 2;
                                    $p3 = $c3 * 7;
                                    $p4 = $c4 * 6;
                                    $p5 = $c5 * 5;
                                    $p6 = $c6 * 4;
                                    $p7 = $c7 * 3;
                                    $p8 = $c8 * 2;
                                    $Suma = $p1 + $p2 + $p3 + $p4 + $p5 + $p6 + $p7 + $p8;
                                    $valDivision = intval($Suma / 11);
                                    $valMultiplicacion = $valDivision * 11;
                                    $valResta = $Suma - $valMultiplicacion;
                                    $digitoVerificador = 11 - $valResta;

                                    if (intval($c9) == intval($digitoVerificador) && intval(substr($documento, 9, 4)) > 0 && intval(substr($documento, 10, 3)) == 1 || $valResta == 0)
                                        return array(true,"Número de RUC correspondiente a Empresas del Sector Público es correcto.");
                                    else
                                        return array(false,"Número de RUC correspondiente a Empresas del Sector Público es incorrecta.");
                                }
                            }
                            else
                            return array(false,"RUC de Empresas del Sector Público incorrecto, verificar los digitos de la Provincia.");
                        }
                    }
                    else
                    return array(false,"Número de C.I. o R.U.C no válido.");
            }

            function Encriptar($pharse)
  	    {       $encriptado="";
                    for($i = 0;$i<=strlen($pharse)-1;$i++)
		    {   $encriptado = $encriptado.str_pad((ord(substr($pharse, $i, 1)) - 17) * 7,3,"0", STR_PAD_LEFT);
        	    }
        	    return $encriptado;
  	    }

            function Desencriptar($pharse)
   	    {       $desencripta="";
                    if (is_numeric($pharse))
                    {   $hasta = strlen($pharse)/3;
                        $k=0;
                        for($i=0;$i<=$hasta-1;$i++)
                        {   $desencripta = $desencripta.chr((intval(substr($pharse,$k,3))/7)+17);
                            $k=$k+3;
                        }
                    }    
        	    return $desencripta;
  	    }


            function DecoderFormula($Formula)
            {       $Sintax1 = $this->SintaxisParentesis($Formula);
                    if (is_bool($Sintax1))
                    {   $Sintax2 = $this->SintaxisOperador($Formula);
                        if (is_bool($Sintax2))
                        {   $fnInit = 0;
                            $fnName = "";
                            $prInit = 0;
                            $prName = "";
                            $fnForm = array();

                            for ($i=0;$i<strlen($Formula);$i++)
                            {   $char = $Formula[$i];
                                if ($prInit>0)
                                {   list($statu,$prInit,$prName) = $this->NombreParametro($prInit,$char,$prName);
                                    if ($statu)
                                    {   if ($prInit==0)
                                        {   $fnForm [] = array($fnName,$prName);
                                            $fnName='';
                                            $prName='';
                                        }    
                                    }
                                    else
                                    return array($statu,$prName);
                                }   
                                if ($fnInit>0)
                                {   list($statu,$fnInit,$fnName,$prInit) = $this->NombreFuncion($fnInit,$char,$fnName);
                                    if (!$statu) return array($statu,$fnName);
                                }
                                if ($char=="{") $fnInit = 1;
                            }
                            if (count($fnForm)>0)
                                return array(true,$fnForm); 
                            else
                                return array(false,"Error en fórmula o en valor constante."); 
                        }    
                        return array(false,$Sintax2);    
                    }    
                    return array(false,$Sintax1);    
            }
                
            private function SintaxisParentesis($Formula)
            {       $charln = strlen($Formula);
                    $parent = 0;
                    $llaves = 0;
                    for ($i=0; $i<$charln; $i++)
                    {   $char = $Formula[$i]; 
                        if ($char=="(") $parent = $parent+1;
                        if ($char==")") $parent = $parent-1;   
                        if ($char=="{") $llaves = $llaves+1;   
                        if ($char=="}") $llaves = $llaves-1;      
                    }
                    if ($parent>0) return "Falta un ')' en la función.";
                    if ($parent<0) return "Falta un '(' en la función.";
                    if ($llaves>0) return "Falta un '}' en la función.";
                    if ($llaves<0) return "Falta un '{' en la función.";
                    return true;
            }        

            private function SintaxisOperador($Formula)
            {       $charlen = strlen($Formula);
                    for ($i=0; $i<$charlen; $i++)
                    {   $siguien = false;        
                        $charpto = $Formula[$i];
                         
                        if (in_array($charpto, array("+", "-", "*", "/"))) 
                        {   if ($i==0)
                            {   if ($charpto=="-")  
                                {   if ($i<$charlen-1)
                                    {   $charnxt = $Formula[$i+1];                
                                        if (ctype_alpha($charnxt) || $charnxt=="(" || $charnxt=="{" || is_numeric($charnxt))
                                            $siguien = true;        
                                    }
                                }
                            }
                            else
                            {   if ($i<$charlen-1)
                                {   $charprv = $Formula[$i-1];                
                                    $charnxt = $Formula[$i+1];
                                    $isNegative = $charpto=="-" ? ($charprv=="(") : false;
                                    if ($isNegative || $charprv==")" || $charprv=="}" || is_numeric($charprv))
                                    {   if ($charnxt=="(" || $charnxt=="{" || is_numeric($charnxt) || ctype_alpha($charnxt))
                                        $siguien = true;
                                    }            
                                }
                            }
                            if (!$siguien) return "Error en la sintaxis del operador '".$charpto."'.";
                        }
                            
                        if ($i<$charlen-1)
                        {   $siguien = $this->SintaxisFaltaOperador($i,$Formula);
                            if (!is_bool($siguien)) return $siguien;
                        }    
                            
                    }
                    return true;
            }    
                
            private function SintaxisFaltaOperador($i,$Formula)
            {       $acts = $Formula[$i];
                    $next = $Formula[$i+1];
                    if ($acts=="}" && ($next=="{" || $next=="(" || is_numeric($next) || ctype_alpha($next))) 
                        return "Falta operador entre ".$acts." ?? ".substr($Formula,$i+1);
                    if ($acts==")" && (is_numeric($next) || ctype_alpha($next))) 
                        return "Falta operador entre ".$acts." ?? ".substr($Formula,$i+1);
                    return true;
            }        

            private function NombreParametro($nchar,$char,$pharse)
            {       if ($char==")")
                        return array(true, 0 , $pharse);
                    else
                    {   if (ctype_alpha($char) || $char=="_" || is_numeric($char))
                        return array(true, $nchar+1, $pharse.$char);
                        else
                        return array(false,0,"Error en parametros.");
                    }    
            }

            private function NombreFuncion($nchar,$char,$pharse)
            {       if ($nchar!=1 || ctype_alpha($char))
                    {   if (ctype_alpha($char))  
                            return array(true, $nchar+1, $pharse.$char,0);
                        else
                        {   if ($char=="(") 
                            return array(true, 0, $pharse,1);
                        }    
                    }
                    return array(false,0,"Error en formula.",0);
            }

            public function EstructuraHorario($Inicio,$NoHoras)
            {       $Columns = array();
                    $Colindx = "";
                    for ($o=0;$o<$NoHoras;$o++)
                    {   $raiz = explode(":",$Inicio);
                        $vhor = $raiz[0];
                        $vmin = $raiz[1];
                        $tmin = $vmin+$this->MinOficial;
                        if ($tmin>60)
                        {   $tmin=$tmin-60;
                            $vhor=$vhor+1;
                        }    
                        $thor = $vhor+$this->HorOficial;
                                
                        $Final = str_pad($thor,2,'0',STR_PAD_LEFT).":".str_pad($tmin,2,'0',STR_PAD_LEFT);
                        
                        $Colindx = $Colindx.($o==0 ? "":"|").$Inicio."<br>".$Final; 
                        $Columns[] = $Inicio."<br>".$Final;
                        $Inicio=$Final;
                    }
                    return array('HHors'=>$Columns,'HTabs'=>$Colindx); 
            }

            public function CalculaHoraFinalHorario($Inicio,$NoHoras)
            {       for ($o=0;$o<$NoHoras;$o++)
                    {   $raiz = explode(":",$Inicio);
                        $vhor = $raiz[0];
                        $vmin = $raiz[1];
                        $tmin = $vmin+$this->MinOficial;
                        if ($tmin>60)
                        {   $tmin=$tmin-60;
                            $vhor=$vhor+1;
                        }    
                        $thor = $vhor+$this->HorOficial;
                        $Inicio = str_pad($thor,2,'0',STR_PAD_LEFT).":".str_pad($tmin,2,'0',STR_PAD_LEFT);
                        
                    }
                    return $Inicio;
            }
            
        }
?>
