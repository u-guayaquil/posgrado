<?php
        class SearchGrid
        {     
                
                private $Config = array();  
                private $cells = array();
                private $rows= array();
                private $ancho;
                private $Margen;
              
                private $FindTextBx;
                private $FindButton;
                private $FindBClick;
                private $DataGrilla;
                private $DataBClick;
                private $DataCClick;
                
                private function Define($Objeto)
                {       $this->FindTextBx = "Find".$Objeto."TextBx";
                        $this->FindButton = "Find".$Objeto."Button";
                        $this->FindBClick = "SearchByElement";        
                        $this->DataGrilla = "SchDet".$Objeto;
                        $this->DataBClick = "Search".$Objeto."GetData";
                        $this->DataCClick = "Search".$Objeto."GetCell";
                }
                
                function __construct($Operacion = "") 
                {       
                        $Objeto = trim(ucwords(str_replace("bt","",$Operacion))); 
                        $this->Define($Objeto);
                }  
              
                function CapaDataGrilla()
                {        return $this->DataGrilla;
                }
                
                function ConfiguraSearchGrid($Config)
                {        $this->Config = $Config;
                }
              
                
                private function CreaSeccionSchTxt()
                {       $crea = true;
                        if (array_key_exists('FindTxt',$this->Config)){
                            $crea = $this->Config['FindTxt'];
                        }
                        $gridCab = '';
                        if ($crea)
                        {   $gridCab.= '<div class="SchTxt">';
                            $gridCab.=      '<p class="SchTxtLab">Buscar:</p>';
                            $gridCab.=      '<p class="SchTxtFnd">';
                            $gridCab.=          '<input type="text"  id="'.$this->FindTextBx.'" class="txt-upper t06" value="" maxlength="50"/>';    
                            $gridCab.=      '</p>';                            
                            $gridCab.=      '<p class="SchTxtBtn">';
                            $gridCab.=      '   <input type="button" id="'.$this->FindButton.'" class="special_button addSch" value="" onClick="return '.$this->FindBClick.'('.$this->FindTextBx.');">';
                            $gridCab.=      '</p>';                            
                            $gridCab.= '</div>';
                        }    
                        return $gridCab; 
                }
                        
                function CreaSearchGrid($HTML = "")
                {       if (count($this->Config)>0 && is_array($this->Config))
                        {   $gridCab = $this->CreaSeccionSchTxt();
                            $gridCab.= '<div class="SchCab">';
                            $gridCab.= '     <table class="SchGrdCab">';   
                            $gridCab.= '     <tr height="'.$this->Config['AltoCab'].'">';
                                                 $gridCab.= $this->FormateaColumna($this->Config['DatoCab']);
                            $gridCab.= '     </tr>';
                            $gridCab.= '     </table>';
                            $gridCab.= '</div>';
                           
                            $gridCab.= '<div id="'.$this->DataGrilla.'" class="SchDet" style="height:'.$this->Config['AltoDet'].'">';
                            $gridCab.= $HTML;
                            $gridCab.= '</div>';        
                            return $gridCab;
                        }
                       return 'Falta configurar la consulta.';
                }  

              private function FormateaColumna($Datos)
              {         $Forcab = '';
                        $this->ancho=0;
                        foreach(array_keys($Datos) as $Columna)
                        {       $Tamano = $Datos[$Columna][0];
                                $Oculta = strtolower(trim($Datos[$Columna][2]));
                                $Respta = ($Oculta != "none" ? "" : " display: ".$Oculta);
                                $Forcab.= '<td class="SchGrdCabCell" style="width: '.$Tamano.';'.$Respta.'">'.$Columna.'</td>';
                                if ($Oculta != "none")
                                $this->ancho = $this->ancho+ intval(str_replace("px","",$Datos[$Columna][0]));
                                     
                        }
                        return $Forcab;
              }
              
              function SearchTableWidth()
              {        return $this->ancho+32;
              }
              
              function CreaSearchTableDetalle($opcion=0,$grdmsg="")
              {        if (count($this->Config)>0 && is_array($this->Config)) 
                       {   $rows = count($this->rows);
                           if ($rows > 0)
                              {      $GridTable = '<table id="DetSch" class="SchGrdDet">';   
                                     for ($rw = 0; $rw < $rows; $rw++)
                                     {    $GridTable.= $this->rows[$rw];
                                     }
                                     $GridTable.= "</table>";
                                     $this->rows = array();
                                     return $GridTable; 
                              }
                           elseif($opcion===0){
                                    return '<p class="SchGrdMessage">'.$grdmsg.'</p>';
                              }else{
                                  $GridTable = '<table id="DetSch" class="SchGrdDet"></table>';
                                  return $GridTable;
                              }
                       }
                       return 'Falta configurar la consulta.';
              }

              function CreaSearchRowsDetalle($Idx,$Color = "",$Click=1)
              {        if (count($this->Config)>0 && is_array($this->Config)) 
                       {   $IDROW = 'TR_'.$Idx;
                           $COLOR = (trim($Color) == "" ? "" : " color:".$Color."; ");
                           $STYLE = (trim($COLOR) == "" ? "" : " style = '".$COLOR."' ");
                           
                           $EVENT = '';
                           if ($Click==1){
                               $EVENT = 'onClick="return '.$this->DataBClick.'(this);"';
                           }
                           
                           $cells = count($this->cells);
                           if ($cells > 0)
                              {     $GridRows = '<tr id="'.$IDROW.'" height="'.$this->Config['AltoRow'].'" '.$STYLE.' '.$EVENT.'>';
                                    for ($cel = 0; $cel < $cells; $cel++)
                                    {    $GridRows.= $this->cells[$cel];
                                    }
                                    $GridRows.= "</tr>";
                                    $this->rows[] = $GridRows;
                              }       
                       }
                       $this->cells = array();
              }

                function CreaSearchCellsDetalle($Idx,$Margen,$HTML,$Cspan = 0,$Click = 0)
                {       if (count($this->Config)>0 && is_array($this->Config))
                        {   $this->Margen = $Margen;
                            $IDCEL = 'TD_'.$Idx.'_'.str_pad(count($this->cells),3,"0",STR_PAD_LEFT);
                            $CSPAN = ($Cspan == 0 ? "" : " colspan = '$Cspan' ");
                            $STYLE = $this->FormateaDetalle($this->Config['DatoCab']); 
                            $EVENT = '';
                            if ($Click==1){
                                $EVENT = 'onClick="return '.$this->DataCClick.'(this);"'; 
                            }
                            $GridCell = '<td id="'.$IDCEL.'" '.$CSPAN.' '.$EVENT.' class="SchGrdDetCell" '.$STYLE.'>'.$HTML.'</td>';
                            $this->cells[] = $GridCell;
                        }    
                }
              
                private function FormateaDetalle($Datos,$MargenLeft="")
                {       $idx = 0;
                        $itm = count($this->cells);
                        foreach(array_keys($Datos) as $Columna)
                        {       if ($itm == $idx)
                                {   $Tamano = trim($Datos[$Columna][0]);
                                    $Alinea = trim($Datos[$Columna][1]);
                                    $Oculta = strtolower(trim($Datos[$Columna][2])); 
                                    
                                    $Estilo = ($Tamano == "" ? "" : "width: ".$Tamano."; ");
                                    $Estilo.= ($Alinea == "" ? "" : "text-align: ".$Alinea."; ");
                                    $Estilo.= ($Oculta != "none" ? "" : "display: ".$Oculta."; ");
                                    //$Estilo.= ($this->Margen=="" ? "": "margin-left: ".$this->Margen."px; ");
                                    return 'style="'.$Estilo.'"';
                                }
                                $idx++;        
                        }
                }
                
                function GetRow($RowId,$estado)
                {       
                        $Style = "color: ".($estado==0 ? "red" : "#000");
                        $RowOb = "TR_".$RowId;
                        return array('Id' => $RowId,'Name' => $RowOb,'Color' => $Style);
                }

                function GetCell($RowId,$Cell)
                {       return "TD_".$RowId."_".str_pad(intval($Cell),3,'0',STR_PAD_LEFT);
                }
                
        }

?>
