<?php

        class SearchInput
        {      
                private $Objeto;
                private $Values;
                private $Estado;
                private $Lenght;
                private $Codigo;
                private $Writer;
                private $TypeBt;
                private $KeyType;
                private $KeyDeps; 
                
                function TextSch($nombre,$codigo = "",$valor = "")
                {       $this->Objeto = $nombre;
                        $this->Codigo = $codigo;
                        $this->Values = $valor;
                        $this->Lenght = '';
                        $this->Estado = 'disabled';
                        $this->Writer = array('readonly','');
                        $this->TypeBt = 'search';
                        return $this;
                }

                function MaxLength($numero = 0)
                {       $this->Lenght = ($numero == 0 ? '': 'maxlength="'.$numero.'"');
                        return $this;
                }

                function Enabled($bool = false)
                {       $this->Estado = ($bool == false ? 'disabled' : '');
                        return $this;
                }

                function ReadOnly($bool = true, $element = "id")
                {       $this->Writer = array(($bool == true ? 'readonly' : ''), (trim($element)=="" ? "id" : trim($element)));
                        return $this;
                }

                function TypeBtn($TypeBt = 'search')
                {       $this->TypeBt = $TypeBt;
                        return $this;
                }

                function Keys($Deps,$Type = 'NUM')
                {       $this->KeyType = $Type;
                        $this->KeyDeps = $Deps;
                        return $this;
                }

                
                function Create($style = "s04")
                {       if (!empty($this->Objeto))
                        {   $idHid = 'id'.$this->Objeto;
                            $idTxt = 'tx'.$this->Objeto;
                            $idBtn = 'bt'.$this->Objeto;
                            
                            $evKey = ' onKeyDown = "return Dependencia(event,\''.$this->KeyDeps.'\',\''.$this->KeyType.'\'); " ';
                            $evBlu = ' onBlur =  "return SearchByElement(this); " ';

                            $classbtn = "txt-boton-search";     
                            if ($this->TypeBt == 'success')   { $classbtn = "txt-boton-success";  }
                            if ($this->TypeBt == 'uploads')   { $classbtn = "txt-boton-carga";    }
                            if ($this->TypeBt == 'downloads') { $classbtn = "txt-boton-descarga"; }

                            $InSch = '<div class="txt-search">';
                            if ($this->Writer[0]=='readonly')
                            {   $InSch.= '<input type="hidden" id="'.$idHid.'" name="'.$idHid.'" value="'.trim($this->Codigo).'"/>';
                                $InSch.= '<input type="text"   id="'.$idTxt.'" name="'.$idTxt.'" class="txt-input-search '.$style.'"  value="'.trim($this->Values).'" '.$this->Lenght.' '.$this->Estado.' '.$this->Writer[0].'/>';
                                $InSch.= '<input type="button" id="'.$idBtn.'" class="'.$classbtn.'" value="" '.$this->Estado.' onclick=" return ButtonClick(\'bt'.$this->Objeto.'\'); ">';
                            }
                            else{
                                $TXT = strtoupper($this->Writer[1]);
                                if ($TXT=='TEXT' || $TXT=='TXT' || $TXT=='TX' || $TXT=='TEXTO'){
                                    $InSch.= '<input type="hidden" id="'.$idTxt.'" name="'.$idTxt.'" class="txt-input-search '.$style.'"  value="'.trim($this->Values).'" '.$this->Lenght.' '.$this->Estado.' '.$evKey.' '.$evBlu.' '.$this->Writer[0].'/>';    
                                }else{
                                    $InSch.= '<input type="text"   id="'.$idHid.'" name="'.$idHid.'" class="txt-input-search '.$style.'"  value="'.trim($this->Values).'" '.$this->Lenght.' '.$this->Estado.' '.$evKey.' '.$evBlu.' '.$this->Writer[0].'/>';
                                }    
                                $InSch.= '<input type="button" id="'.$idBtn.'" class="'.$classbtn.'" value="" '.$this->Estado.' onclick=" return ButtonClick(\'bt'.$this->Objeto.'\'); ">';
                            }
                            $InSch.= '</div>';
                            
                            $this->Lenght = '';
                            $this->Objeto = '';
                            $this->Codigo = '';
                            $this->Values = '';
                            $this->Estado = 'disabled';
                            $this->Writer = array('readonly','');
                            $this->TypeBt = 'search';
                            return $InSch;
                        }
                        else
                        return "Use método TextSch";
                }
        }

?>