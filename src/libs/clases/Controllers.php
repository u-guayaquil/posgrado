<?php

        class Controllers
        {       private $url;
                private $cmd;
            
                function __construct($url) 
                {       $this->url = $url;
                        $this->cmd = strtoupper(substr($url,0,3));
                }  
 
                function View()
                {       if ($this->cmd=="CMD")  
                            return $this->Commands();
                        else
                            return $this->Routes();
                }
                
                private function Commands()
                {       if ($this->url == "CMD_LOGIN") 
                            return array(true,'src/modules/seguridad/login/vista/Login.php');
                        
                        if ($this->url == "CMD_RECOVERY") 
                            return array(true,'src/modules/seguridad/recovery/vista/Recovery.php');
                        
                        if ($this->url == "CMD_ACTIVACION_PWD") 
                            return array(true,'src/utils/Recovery_clave.php');

                        /*if ($this->url == "CMD_CONFIG") 
                            return array(true, 'src/modules/entorno/config/vista/Config.php');*/

                        /*if ($this->url == "CMD_ERROR_SESSION") 
                            return array(false,'src/utils/reload.php'); */  
                
                        if (Session::exist())
                        {   if (Session::existVariable("Sesion"))
                            {   if ($this->url == "CMD_MAIN")
                                return array(true, 'src/modules/entorno/main/vista/Main.php');
                                if ($this->url == "CMD_CONFIRM")
                                return array(true, "ConfirmaPassword");
                                if ($this->url == "CMD_CHGPASS")
                                return array(true, "CambiaPassword");
                            }
                            if ($this->url == "CMD_CLSESION")
                            Session::destroy();
                        }
                        return array(false,"src/utils/error_sesion.html");
                }
                
                private function Routes()
                {       if (Session::exist())
                        {   if (Session::existVariable("Sesion"))
                            {   $Utils = new Utils();  
                                $Pharse = $Utils->Desencriptar($this->url);
                                if ($Pharse!="")
                                {   if (file_exists($Pharse))
                                    return array(true,$Pharse);
                                }
                                Session::destroy();
                            }
                        }
                        return array(false,"src/utils/reload.php");
                }        
                        
        }

?>