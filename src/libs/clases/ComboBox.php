<?php

    class ComboBox
        {       private $Values;
                private $Objeto;
                private $Select;
                private $Opcion;
                private $Codigo;
                private $Textos;
                private $Estado;
                private $Evento;
                

                function __construct($Opcion = "") 
                {       $this->Opcion = $Opcion;
                        $this->DefaultValues(); 
                }
                
                private function DefaultValues()
                {       $this->Select = '';
                        $this->Objeto = '';
                        $this->Values = '';
                        $this->Codigo = '';
                        $this->Textos = '';                            
                        $this->Estado = 'disabled';    
                }
                        
                function Combo($nombre,$datos)
                {       $this->Objeto = $nombre;
                        $this->Values = $datos;
                        return $this;
                }
                
                function Selected($value="")
                {       $this->Select = $value;   
                        return $this;
                }
                
                function Fields($id,$texto)
                {       $this->Codigo = $id;
                        $this->Textos = $texto;
                        return $this;
                }
                
                function Enabled($bool = false)
                {       $this->Estado = ($bool == false ? 'disabled' : '');
                        return $this;
                }
                
                function Evento()
                {       $this->Evento = 'onchange=" return Control'.ucwords($this->Objeto).$this->Opcion.'(this); "';
                        return $this;
                }
                
                function Create($Style = "",$Default = "")
                {       if (!empty($this->Objeto)) 
                        {   if (strlen($this->Codigo)>0 && strlen($this->Textos)>0)
                            {   
                                $combo = '<select class="sel-input '.$Style.'" id="'.$this->Objeto.$this->Opcion.'" name="'.$this->Objeto.$this->Opcion.'" '.$this->Evento.' '.$this->Estado.'>';
                                $combo.=  $this->DefaultOption($Default); 
                                
                                foreach($this->Values as $valor)
                                {       $marca = '';
                                        if (is_numeric($this->Select)){
                                            $marca = ($valor[$this->Codigo] == $this->Select ? "selected" : "");
                                        }
                                        elseif (trim($this->Select)!=""){
                                            $marca = ($valor[$this->Codigo] == $valor[$this->Select] ? "selected" : "");
                                        }
                                        $combo.= '<option value="'.$valor[$this->Codigo].'" '.$marca.'>'.$valor[$this->Textos].'</option>';
                                }
                                $combo.= '</select>';
                                $this->DefaultValues();
                                return $combo;
                            }
                            return "Use método Fields.";
                        }
                        return "Use método Combo.";
                }
                
                private function DefaultOption($Default)
                {       if($Default=="TUPE") return '<option value="@T00">TODOS</option>'; 
                        if($Default=="SUPE") return '<option value="@S00">SELECCIONE</option>';                                    
                        if($Default=="NUPE") return '<option value="@N00">NINGUNO</option>';                                    
                        if($Default=="TLOW") return '<option value="@T00">Todos</option>'; 
                        if($Default=="SLOW") return '<option value="@S00">Seleccione</option>';                                    
                        if($Default=="NLOW") return '<option value="@N00">Ninguno</option>';
                        return "";
                }        
              
        }
?>
