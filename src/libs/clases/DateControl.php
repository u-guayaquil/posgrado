<?php
    
//int getDayName(dia{Variant})
//Retorna el nombre del dia. Por defecto retorna el dia actual. Recibe como parametros no obligatorio valores del (0 al 6). donde cero es
//domingo y seis es sabado. Cualquier otro valor ingresado como parametro devolvera el dia actual.

//int getNowDay()     Retorna el dia de la fecha actual, no recibe parametros.
//int getNowMonth()   Retorna el mes de la fecha actual, no recibe parametros.
//int getNowYear()    Retorna el anio de la fecha actual, no recibe parametros.
//string getNowTime() Retorna la hora actual, no recibe parametros.

//string getNowDateTime(fecha{String})
//Retorna la fecha actual en dos formatos (dd/mm/yyyy o yyyy-mm-dd) incluida la hora cuando es requerido.
//Recibe como parametros no obligatorios constantes: FECHAHORA,HOY,DATETIME,NOW,FECHA,DATE. Por defecto retorna la fecha actual en formato
//(dd/mm/yyyy 00:00:00)

//array(estado[Bool],resultado[String]) chkDateDMY(fecha[String])
//Verifica si cumple con el formato de fecha establecido por la funcion. Recibe un parametro fecha y retona un arreglo con el estado (true o false)
//y un mensaje o fecha como resultado como segundo elemento del arreglo.

//array(estado[Bool],resultado[String]) chkDateYMD(fecha[String])
//Verifica si cumple con el formato de fecha establecido por la funcion. Recibe un parametro fecha y retona un arreglo con el estado (true o false)
//y un mensaje o fecha como resultado como segundo elemento del arreglo.

//array(estado[bool],resultado[string]) chkDate(fecha[String])
//Valida que una fecha sea correcta sin importar el formato. Recibe un parametro como fecha y retona un arreglo con el estado (true o false)
//y un mensaje o fecha como resultado como segundo elemento del arreglo.

//string getDayName(dia{Variant})
//Retorna el nombre actual del dia de la semana segun el parametro no obligatorio que reciba. De (0 a 6) donde (0 es domingo y 6 es sabado).
//Por defecto retorna el nombre del dia actual.

//array(estado[bool],resultado[string]) getDayNameByDate(fecha[String])
//Retorna el nombre del dia de la semana por medio de una fecha. Por defecto retorna el dia actual.

//array(status[bool],resultado[string]) changeFormatDate(fecha[string], fmtDateOut[string])
//Cambia una fechas a dos posibles formatos de salida (DMY o YMD). Recibe dos parametro fecha y formato de salida, y retona un arreglo con
//el estado (true o false) y un mensaje o fecha como resultado como segundo elemento del arreglo.

//Variant return getMonthName(mes{Variant})
//Retorna el nombre del mes o el numero del mes. Recibe como parametros el nombre del mes en el cual retornara su numero y si recibe un numero
//Retornara el nombre.

//array(status[bool],resultado[string]) getDMY(fecha[String],Parte[String])
//Retorna un arreglo con el estado(true o false) y el mes, dia o anio de una fecha. Recibe como parametros la fecha y la parte de la fecha
//que debe ser retornada.

       class DateControl
       {     private $mesStr = array(1=>'Enero',2=>'Febrero',3=>'Marzo',4=>'Abril',5=>'Mayo',6=>'Junio',7=>'Julio',8=>'Agosto',9=>'Septiembre',10=>'Octubre',11=>'Noviembre',12=>'Diciembre');
             private $diaStr = array(1=>'Domingo',2=>'Lunes',3=>'Martes',4=>'Miercoles',5=>'Jueves',6=>'Viernes',7=>'Sabado');

             function getNowDay()
             {        date_default_timezone_set('America/Bogota');
                      $sysDate = mktime(0,0,0,date("m"),date("d"),date("Y"));
                      return date("d",$sysDate);
             }
             function getNowMonth()
             {        date_default_timezone_set('America/Bogota');
                      $sysDate = mktime(0,0,0,date("m"),date("d"),date("Y"));
                      return date("m",$sysDate);
             }
             function getNowYear()
             {        date_default_timezone_set('America/Bogota');
                      $sysDate = mktime(0,0,0,date("m"),date("d"),date("Y"));
                      return date("Y",$sysDate);
             }
             function getNowTime()
             {        date_default_timezone_set('America/Bogota');
                      return date("H:i:s");
             }
             
            function getNowDateTime($opcion="")
            {       $opcion = strtoupper($opcion);
                    if ($opcion=="FECHAHORA" || $opcion=="HOY" || $opcion==""){
                        return $this->getNowDay()."/".$this->getNowMonth()."/".$this->getNowYear()." ".$this->getNowTime();
                    }elseif ($opcion=="DATETIME" || $opcion=="NOW"){
                        return $this->getNowYear()."-".$this->getNowMonth()."-".$this->getNowDay()." ".$this->getNowTime();
                    }elseif ($opcion=="FECHA"){
                        return $this->getNowDay()."/".$this->getNowMonth()."/".$this->getNowYear();
                    }elseif ($opcion=="DATE"){
                        return $this->getNowYear()."-".$this->getNowMonth()."-".$this->getNowDay();
                    }else{
                        return $this->getNowDay()."/".$this->getNowMonth()."/".$this->getNowYear()." ".$this->getNowTime();
                    }    
            }
             
            protected function decoderDate($fecha)
            {       $soltim = "00:00:00";
                    $solfec = explode(" ",$fecha);
                    if (count($solfec)==2)  $soltim = $solfec[1];
                    return array($solfec[0],$soltim);
            }
             
            function chkDateDMY($fecha="")
            {       if ($fecha==""){
                        return array(false,'Debe ingresar un parametro fecha','');
                    }else{ 
                        list($solfec,$soltim)=$this->decoderDate($fecha);
                        $sector = explode("/",$solfec);
                        if (count($sector)!=3){
                            return array(false,'Formato debe ser dd/mm/yyyy','');
                        }else{ 
                            if (is_numeric($sector[0]) && is_numeric($sector[1]) && is_numeric($sector[2]))
                            {   if (checkdate($sector[1],$sector[0],$sector[2]))
                                {   $formatDMY = str_pad($sector[0],2,"0",STR_PAD_LEFT)."/".str_pad($sector[1],2,"0",STR_PAD_LEFT)."/".str_pad($sector[2],4,"0",STR_PAD_LEFT);
                                    return array(true,$formatDMY,$soltim);
                                }else{
                                    return array(false,'Valores para dd, mm, yyyy incorrectos','');
                                }    
                            }else{
                                return array(false,'La fecha debe contener valores numericos para dd, mm, yyyy','');
                            }    
                        }
                    }
            }
            
            function chkDateYMD($fecha="")
            {       if ($fecha==""){
                        return array(false,'Debe ingresar un parametro fecha','');
                    }else{
                        list($solfec,$soltim)=$this->decoderDate($fecha);
                        $sector = explode("-",$solfec);
                        if (count($sector)!=3){
                            return array(false,'Formato debe ser yyyy-mm-dd','');
                        }else{
                            if (is_numeric($sector[0]) && is_numeric($sector[1]) && is_numeric($sector[2])){
                                if (checkdate($sector[1],$sector[2],$sector[0])){
                                    $formatYMD = str_pad($sector[0],4,"0",STR_PAD_LEFT)."-".str_pad($sector[1],2,"0",STR_PAD_LEFT)."-".str_pad($sector[2],2,"0",STR_PAD_LEFT);
                                    return array(true,$formatYMD,$soltim);
                                }else{
                                    return array(false,'Fecha incorrecta','');
                                }    
                            }else{
                                return array(false,'La fecha debe contener valores numericos para yyyy, mm, dd','');
                            }
                        }
                    }
            }
             
            function chkDate($fecha="")
            {       if ($fecha==""){
                        return array(false,'Debe ingresar un parametro fecha','');
                    }else{
                        list($st1,$dt1,$tt1)=$this->chkDateDMY($fecha);
                        if ($st1){
                            return array($st1,$dt1,$tt1);
                        }else{   
                            list($st2,$dt2,$tt2)=$this->chkDateYMD($fecha);
                            if ($st2){
                                return array($st2,$dt2,$tt2);
                            }else{
                                return array($st2,'Fecha incorrecta','');
                            }    
                        }
                    }
            }
            
             function changeFormatDate($fecha="",$fmtDateOut='DMY')
             {        $fmtDateOut=strtoupper($fmtDateOut);
                      list($st,$rt,$tt)=$this->chkDateYMD($fecha);
                      if($st)
                      {  if ($fmtDateOut=='DMY' || $fmtDateOut=='DMA' || $fmtDateOut=='DD/MM/AAAA' || $fmtDateOut=='DD/MM/YYYY')
                         {   $sector = explode("-",$rt);
                             return array($st,str_pad($sector[2],2,"0",STR_PAD_LEFT)."/".str_pad($sector[1],2,"0",STR_PAD_LEFT)."/".str_pad($sector[0],4,"0",STR_PAD_LEFT),$tt);
                         }
                         elseif($fmtDateOut=='YMD' || $fmtDateOut=='AMD' || $fmtDateOut=='AAAA-MM-DD' || $fmtDateOut=='YYYY-MM-DD')
                             return array($st,$rt,$tt);
                         else
                             return array(false,'Formato de salida no es valido','');
                      }
                      else
                      {  list($st,$rt,$tt)=$this->chkDateDMY($fecha);
                         if($st)
                         {  if ($fmtDateOut=='DMY' || $fmtDateOut=='DMA' || $fmtDateOut=='DD/MM/AAAA' || $fmtDateOut=='DD/MM/YYYY')
                                return array($st,$rt,$tt);
                            elseif($fmtDateOut=='YMD' || $fmtDateOut=='AMD' || $fmtDateOut=='AAAA-MM-DD' || $fmtDateOut=='YYYY-MM-DD')
                            {   $sector = explode("/",$rt);
                                return array($st,str_pad($sector[2],4,"0",STR_PAD_LEFT)."-".str_pad($sector[1],2,"0",STR_PAD_LEFT)."-".str_pad($sector[0],2,"0",STR_PAD_LEFT),$tt);
                            }
                            else
                                return array(false,'Formato de salida no es valido','');
                         }
                         else
                         return array(false,'Error en la fecha','');
                      }
             }
             function getMonthName($month="")
             {        if (is_numeric($month))
                      {   if (intval($month)<1 || intval($month)>12) $month=intval($this->getNowMonth());
                          return $this->mesStr[intval($month)]; //Retorna Nombre del mes
                      }
                      else
                      {   for($i=1;$i<=12;$i++)
                          {   $comp=strtoupper($this->mesStr[$i]);
                              if ($comp==strtoupper($month) || substr($comp,0,3)==strtoupper($month))
                              {   return str_pad($i,2,"0",STR_PAD_LEFT); //Retorna id del mes
                                  break;
                              }
                          }
                          $month=intval($this->getNowMonth());
                          return $this->mesStr[$month]; //Retorna Nombre del mes
                      }
             }
            
            function getDayNameByDate($fecha="")
            {       list($st,$rt,$tt)=$this->changeFormatDate($fecha,'AMD');
                    if ($st){
                        return array($st,$this->diaStr[date('w', strtotime($rt))],$tt);
                    }else{
                    return array($st,$rt,$tt);
                    }
            }
            
            function getDayNumberByDate($fecha="")
            {       list($st,$rt,$tt)=$this->changeFormatDate($fecha,'AMD');
                    if ($st){
                        return array($st,date('w', strtotime($rt)),$tt);
                    }else{
                    return array($st,$rt,$tt);
                    }
            }
             
            function getDayName($day="")
            {       date_default_timezone_set('America/Bogota');
                    if (is_numeric($day))
                    {   
                        if (intval($day)>=1 && intval($day)<=7){
                            return $this->diaStr[intval($day)];
                        }else{
                            return "Ingrese un rango de [1=Dom a 7=Sab]";
                        }      
                    }
                    else{
                        return $this->diaStr[date("w")];
                    }
            }
             
            function getDMY($fecha="",$part="D")
            {       list($st,$rt,$tt) = $this->changeFormatDate($fecha,"YMD");
                    if($st)
                    {   $part = strtoupper($part);
                        if ($part=="Y") { $lt = substr($rt,0,4); }
                        if ($part=="M") { $lt = substr($rt,5,2); }
                        if ($part=="D") { $lt = substr($rt,8,2); }
                        return array($st,$lt);
                    }else{
                        return array($st,$rt);
                    }
            }
             
            function AddDayToDate($fecha,$dias,$formato)
            {       $resultado = $this->changeFormatDate($fecha, 'YMD');
                    if ($resultado[0])    
                    {   $objfecha= new DateTime($resultado[1]);
                        $objfecha->add(new DateInterval('P'.$dias.'D'));
                        return $objfecha->format($formato);  
                    }else{
                        return $resultado[2];
                    }    
            }
            
       }
 ?>

