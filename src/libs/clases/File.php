<?php
    
    class File
    {       private $ptoFile;
            private $archivo;
            private $content;

            function setFileName($archivo)
            {        $this->archivo = $archivo;
            }

            function setContenido($texto)
            {        $this->content = $texto;
            }

            function getPtoFile()
            {        return $this->ptoFile;
            }

            function createFile($modo="a+")
            {        if (empty($this->content))
                         return false;
                     else
                     {   if ($this->openFile($modo))
                         {   $this->setDataFile();
                             $this->closFile();
                             return true;
                         }
                         return false;
                     }
            }

            function openFile($Modo)
            {        if (empty($this->archivo))
                         return false;
                     else
                     {   $this->ptoFile = fopen($this->archivo, $Modo) or die("Problemas con manejador de archivo.");
                         return true;
                     }

            }

            function setDataFile()
            {        fputs($this->ptoFile,$this->content);
            }

            function getDataFile()
            {        return fgets($this->ptoFile);
            }

            function endOfFile()
            {        return feof($this->getPtoFile());
            }

            function closFile()
            {        fclose($this->ptoFile);
            }
           
            function typeImg($file)
            {        $types = array('png','jpg','gif','bmp','jpeg');
                     $parts = explode('.',$file);
                     if (count($parts)==2)
                     {   if (in_array($parts[1],$types)) return array($parts[0],$parts[1]);
                     }
                     return array("","");
            }
           
            function DeleteFile($ruta)
            {       if (trim($ruta)!=""){ 
                        unlink($ruta);
                    }
            }

            function MoveFile($Origen,$Destino)
            {        if (trim($Origen)!="" && trim($Destino)!="")
                        return rename ($Origen, trim($Destino));
                     else
                        return false;
            }

            function getFileParts($PathFile)
            {       $ruta = "";
                    $name = "";
                    $extn = "";
                    
                    $PathFile = trim($PathFile);
                    if ($PathFile!="")
                    {   $FilePart = explode("/",$PathFile);
                        if (count($FilePart)==1)
                        {   $Nodo = explode(".", $FilePart[0]);
                            if (count($Nodo)==1){
                                $ruta = $Nodo[0];
                            }else{   
                                $name = $Nodo[0];
                                $extn = $Nodo[1];
                            }    
                        }else{   
                            $Nodo = explode(".", end($FilePart));
                            if (count($Nodo)==1){
                                $ruta = $PathFile;
                            }else{   
                                $ruta = explode($Nodo[0], $PathFile)[0];
                                $name = trim($Nodo[0]);
                                $extn = trim($Nodo[1]);
                            }    
                        }   
                        return array(true,$ruta,$name,$extn);
                    }
                    else{
                        return array(false,$ruta,$name,$extn);    
                    }
                    
            }

            function UploadFiles($Files,$Extensions,$UploadDirectory)
            {       $InputFileName = "archivo";  //nombre del Input HTML de Origen (ejemplo name="archivo[]" --tomar solo--> archivo
                    $FileLocations = array();

                    if (file_exists($UploadDirectory) && is_writable($UploadDirectory))
                    {   if (isset($Files[$InputFileName]["error"]))
                        {   foreach ($Files[$InputFileName]["error"] as $key => $error)
                            {       if ($error==0)
                                    {   $Fileparts = explode(".", $Files[$InputFileName]["name"][$key]);
                                        $Extension = strtolower(end($Fileparts));
                                        $ValidFile = false;
                                        foreach ($Extensions as $type)
                                        {        if (trim($type) == $Extension) $ValidFile = true;
                                        }

                                        if ($ValidFile)
                                        {   $FileName = $Fileparts[0].".".$Extension;
                                            $Location = $Files[$InputFileName]["tmp_name"][$key];
                                            if (!move_uploaded_file($Location, "$UploadDirectory/$FileName")) 
                                                return array(false,'No se pudo mover el archivo.');
                                            else
                                                $FileLocations[] = $FileName;
                                        }
                                        else
                                            return array(false,'Extensión de archivo no válida.');
                                    }
                                    else
                                    {   if ($error!= 0 and $error!= 4)
                                        {   $errorMessage = $this->getErrorMessage($Files[$InputFileName]["error"][$key]);
                                            return array(false, $errorMessage." Intente nuevamente.");
                                        }
                                    }
                            }
                            return array(true,$FileLocations);
                        }
                        else{
                            return array(false,"Los archivos sobrepasan la capacidad establecida por el servidor.");
                        }
                    }
                    else{
                        return array(false,"No existe la carpeta para subir archivos o no tiene los permisos suficientes.");
                    }
            }

            private function getErrorMessage($error_code)
            {       switch ($error_code)
                    {       case UPLOAD_ERR_INI_SIZE:
                                return 'El archivo excede el límite permitido por la directiva de PHP';
                            case UPLOAD_ERR_FORM_SIZE:
                                return 'El archivo excede el límite permitido por la directiva de HTML';
                            case UPLOAD_ERR_PARTIAL:
                                return 'El archivo sólo se subió parcialmente, intentelo de nuevo';
                            case UPLOAD_ERR_NO_FILE:
                                return 'No hay archivo que subir';
                            case UPLOAD_ERR_NO_TMP_DIR:
                                return 'El folder temporal no existe';
                            case UPLOAD_ERR_CANT_WRITE:
                                return 'No tiene permisos para grabar archivos en el disco';
                            case UPLOAD_ERR_EXTENSION:
                                return 'El archivo tiene una extensión NO permitida';
                            default:
                                return 'Error desconocido al subir el archivo';
                    }
            }
           
           
     }
?>

