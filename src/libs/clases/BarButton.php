<?php

      class BarButton
      {     private $Sufijo;
            
            function __construct($Sufijo="")
            {       $this->Sufijo = $Sufijo;
            } 

            function getSufijo()
            {        return $this->Sufijo;          
            }
            
            function CreaBarButton($Buttons)
            {        $barra = '<p class="barra_spaces">';
                     foreach ($Buttons as $button)
                     {        if ($button['addnew'] > 0)
                              {   $id = 'addNew';
                                  $Evento= 'onclick=" return ButtonClick(\'addNew\');"';
                                  $barra.= '<input class="barra_button addNew" id="'.$id.'" type="button" '.$Evento.'/>';
                              }
                              if ($button['addmod'] > 0)
                              {   $id = 'addMod';
                                  $Evento= 'onclick=" return ButtonClick(\'addMod\');"';
                                  $barra.= '<input class="barra_button addMod" id="'.$id.'" type="button" '.$Evento.'/>';
                              }
                              if ($button['addcon'] > 0)
                              {   $id = 'addDel';
                                  $Evento= 'onclick=" return ButtonClick(\'addDel\');"';
                                  $barra.= '<input class="barra_button addDel" id="'.$id.'" type="button" '.$Evento.'/>';
                              }
                              if ($button['addnew'] > 0 || $button['addmod'] > 0)
                              {   $id = 'addSav';
                                  $Evento= 'onclick=" return ButtonClick(\'addSav\');"';
                                  $barra.= '<input class="barra_button addSav" id="'.$id.'" type="button" '.$Evento.'/>';

                                  $id = 'addCan';
                                  $Evento= 'onclick=" return ButtonClick(\'addCan\');"';
                                  $barra.= '<input class="barra_button addCan" id="'.$id.'" type="button" '.$Evento.'/>';
                              }
                              if ($button['addimp'] > 0)
                              {   $id = 'addImp';
                                  $Evento= 'onclick=" return ButtonClick(\'addImp\');"';
                                  $barra.= '<input class="barra_button addImp" id="'.$id.'" type="button" '.$Evento.'/>';
                              }
                              if ($button['addexp'] > 0)
                              {   $id = 'addExp';
                                  $Evento= 'onclick=" return ButtonClick(\'addExp\');"';
                                  $barra.= '<input class="barra_button addExp" id="'.$id.'" type="button" '.$Evento.'/>';
                              }
                              if ($button['addpdf'] > 0)
                              {   $id = 'addPdf';
                                  $Evento= 'onclick=" return ButtonClick(\'addPdf\');"';
                                  $barra.= '<input class="barra_button addPdf" id="'.$id.'" type="button" '.$Evento.'/>';
                              }
                     }
                     return $barra.'</p>';
            }
      }
?>

