                 String.prototype.ElementTrim = function()
                 {        return this.replace(/^\s*(\b.*\b|)\s*$/, "$1");
                 }

                 function TrimElement(Cadena)
                 {        return Cadena.ElementTrim();
                 }

                 function ElementStatus(Enabled,Disabled)
                 {        ElementStatusEnabled(Enabled);
                          ElementStatusDisabled(Disabled);
			  return false;
                 }

                 function ValidateStatusEnabled(Enabled)
                 {      if (Enabled.length != 0)
                          {   var myArreglo = Enabled.split(":");
                    		  for (i=0; i<(myArreglo.length); i++)
         		                {    if (!document.getElementById(myArreglo[i]))
                                       {   //alert("El elemento "+myArreglo[i]+" no existe.");
                                       }
                                       else
                                       {   myVarTemp = document.getElementById(myArreglo[i]);
                                           if(myVarTemp.disabled == false)
                                           {    return true;
                                           }
                                        }
			                    }
                          }
                        return false;
                 }

                 function ElementStatusEnabled(Enabled)
                 {        if (Enabled.length != 0)
                          {   var myArreglo = Enabled.split(":");
                    		  for (i=0; i<(myArreglo.length); i++)
         		          {    if (!document.getElementById(myArreglo[i]))
                                       {   //alert("El elemento "+myArreglo[i]+" no existe.");
                                       }
                                       else
                                       {   myVarTemp = document.getElementById(myArreglo[i]);
                                           myVarTemp.disabled = false;
                                       }
			          }
                          }
                          return false;
                 }
                 function ElementStatusDisabled(Disabled)
                 {      if (Disabled.length != 0)
                        {   var myArreglo = Disabled.split(":");
      		            for (i=0; i<(myArreglo.length); i++)
                                {   if (!document.getElementById(myArreglo[i]))
                                    {   //alert("El elemento "+myArreglo[i]+" no existe.");
                                    }
                                    else
                                    {   myVarTemp = document.getElementById(myArreglo[i]);
                                        myVarTemp.disabled = true;
                                    }
			        }
			}
                 }
                 
                 function ElementClear(Elements)
                 {      if (Elements.length != 0)
                        {   var myArreglo = Elements.split(":");
           		    for (i=0; i<(myArreglo.length); i++)
			    {   myVarTemp = document.getElementById(myArreglo[i]);
                                if (myVarTemp.tagName != "SELECT")
                                {   if (myVarTemp.tagName != "DIV" && myVarTemp.tagName != "TD")
	    			    {   if (myVarTemp.type == "checkbox")
                                            myVarTemp.checked = false;
                                        else
                                            myVarTemp.value = "";
				    }
				    else
    				        myVarTemp.innerHTML = "";
                                }
                                else
                                myVarTemp.value = 0;
                            }
  			}
    			return false;
                 }

                 function ElementValidateBeforeSave(Required)
                 {        Required = TrimElement(Required);
                          if (Required.length != 0)
                          {   var myArreglo = Required.split(":");
                              for( i=0; i<(myArreglo.length); i++)
			      {    myRequired = document.getElementById(myArreglo[i]);
    			           myRequired.value = TrimElement(myRequired.value);
                                   if (myRequired.value.length == 0)
                                   {   //alert("Debe ingresar el campo: ");  //getTextLabel(myRequired.id));
                                       return false;
                                   }
                              }
	                  }
	                  return true;
                 }

                 function getTextLabel(ElementID)
                 {        var labels = document.getElementsByTagName ("label");
                          for (var i = 0; i < labels.length; i++)
                          {    if (labels[i].htmlFor == ElementID)
                                   return labels[i].innerHTML;
                          }
                          return "";
                 }

                function IsDisabled(Element) 
                {       return document.getElementById(Element).disabled;
                }

                function ElementGetValue(Element)
                {       //return document.getElementById(Element).value;
                    
                        myVarTemp = document.getElementById(Element);
                        if (myVarTemp.tagName != "SELECT")
                        {   if (myVarTemp.tagName != "DIV" && myVarTemp.tagName != "TD")
    			    {   if (myVarTemp.type == "checkbox")
                                    return myVarTemp.checked;
                                else
                                    return myVarTemp.value;
            		    }
			    else
    			    return myVarTemp.innerHTML;
                        }
                        return myVarTemp.value;
                }

                function ElementGetText(Element)
                {       myVarTemp = document.getElementById(Element);
                        if (myVarTemp.tagName != "SELECT")
                        {   if (myVarTemp.tagName != "DIV" && myVarTemp.tagName != "TD")
    			    {   if (myVarTemp.type == "checkbox")
                                    return myVarTemp.checked;
                                else
                                    return myVarTemp.value;
            		    }
			    else
    			    return myVarTemp.innerHTML;
                        }
                        return myVarTemp.options[myVarTemp.selectedIndex].text;
                }
                 
                function ElementSetValue(Element,Valor)
                {       //document.getElementById(Element).value = Valor;
                        myVarTemp = document.getElementById(Element);
                        if (myVarTemp.tagName != "SELECT")
                        {   if (myVarTemp.tagName != "DIV" && myVarTemp.tagName != "TD")
    			    {   if (myVarTemp.type == "checkbox")
                                    myVarTemp.checked = Valor;
                                else
                                    myVarTemp.value = Valor;
            		    }
			    else
    			    myVarTemp.innerHTML = Valor;
                        }
                        myVarTemp.value = Valor;
                }
                 
                function SetDiv(DivElement,DivText)
		{  	var myVarTemp = document.getElementById(DivElement);
			myVarTemp.innerHTML = DivText;
   	        }
                 
                function PrepareElements(Elements) 
                {       if (Elements.length != 0)
                        {   var Argumento = new Object();
                            var arreglo = Elements.split(":");
           		    
                            for (i=0; i<arreglo.length; i++)
			    {   myTemp = document.getElementById(arreglo[i]);
                                if (myTemp.type == "checkbox")
                                    valor = ((myTemp.checked) ? 1:0);    
                                else if (myTemp.tagName == "DIV")
                                    valor = myTemp.innerHTML;     
                                else    
                                    valor = myTemp.value;
                                Argumento[arreglo[i]] = valor;
                            }
                            return Argumento;    
  			}
                }
                
                function PrepareFreeElements(Elements) 
                {       if (Elements.length != 0)
                        {   var Argumento = new Object();
                            var arreglo = Elements.split("|");
                            for (i=0; i<arreglo.length; i++)
			    {   var objeto = arreglo[i].split(":");
                                Argumento[objeto[0]] = objeto[1];
                            }
                            return Argumento;    
  			}
                }
                
                function PrepareFreeElementsNotas(Elements) 
                {       if (Elements.length != 0)
                        {   var Argumento = new Object();
                            var arreglo = Elements.split("|");
                            for (i=0; i<arreglo.length; i++)
			    {   var objeto = arreglo[i].split("=");
                                Argumento[objeto[0]] = objeto[1];
                            }
                            return Argumento;    
  			}
                }
                
                function DescargarArchivo(Datos, nombreArchivo) 
                {       contenidoEnBlob = new Blob(Datos, {type: 'application/xml'});
                
                        var reader = new FileReader();
                        reader.onload = function (event) {
                        var save = document.createElement('a');
                        save.href = event.target.result;
                        save.target = '_blank';
                        save.download = nombreArchivo || 'archivo.dat';
                        var clicEvent = new MouseEvent('click', {
                            'view': window,
                            'bubbles': true,
                            'cancelable': true
                        });
                        save.dispatchEvent(clicEvent);
                        (window.URL || window.webkitURL).revokeObjectURL(save.href);
                        };
                        reader.readAsDataURL(contenidoEnBlob);
                }
                
                
                
                
                
                /*
//Función de ayuda: reúne los datos a exportar en un solo objeto
function obtenerDatos() {
    return {
        nombre: document.getElementById('textNombre').value,
        telefono: document.getElementById('textTelefono').value,
        fecha: (new Date()).toLocaleDateString()
    };
};

//Función de ayuda: "escapa" las entidades XML necesarias
//para los valores (y atributos) del archivo XML
function escaparXML(cadena) {
    if (typeof cadena !== 'string') {
        return '';
    };
    cadena = cadena.replace('&', '&amp;')
        .replace('<', '&lt;')
        .replace('>', '&gt;')
        .replace('"', '&quot;');
    return cadena;
};

//Genera un objeto Blob con los datos en un archivo TXT
function generarTexto(datos) {
    var texto = [];
    texto.push('Datos Personales:\n');
    texto.push('Nombre: ');
    texto.push(datos.nombre);
    texto.push('\n');
    texto.push('Teléfono: ');
    texto.push(datos.telefono);
    texto.push('\n');
    texto.push('Fecha: ');
    texto.push(datos.fecha);
    texto.push('\n');
    //El contructor de Blob requiere un Array en el primer parámetro
    //así que no es necesario usar toString. el segundo parámetro
    //es el tipo MIME del archivo
    return new Blob(texto, {
        type: 'text/plain'
    });
};


//Genera un objeto Blob con los datos en un archivo XML
function generarXml(datos) {
    var texto = [];
    texto.push('<?xml version="1.0" encoding="UTF-8" ?>\n');
    texto.push('<datos>\n');
    texto.push('\t<nombre>');
    texto.push(escaparXML(datos.nombre));
    texto.push('</nombre>\n');
    texto.push('\t<telefono>');
    texto.push(escaparXML(datos.telefono));
    texto.push('</telefono>\n');
    texto.push('\t<fecha>');
    texto.push(escaparXML(datos.fecha));
    texto.push('</fecha>\n');
    texto.push('</datos>');
    //No olvidemos especificar el tipo MIME correcto :)
    return new Blob(texto, {
        type: 'application/xml'
    });
};

document.getElementById('boton-xml').addEventListener('click', function () {
    var datos = obtenerDatos();
    descargarArchivo(generarXml(datos), 'archivo.xml');
}, false);

document.getElementById('boton-txt').addEventListener('click', function () {
    var datos = obtenerDatos();
    descargarArchivo(generarTexto(datos), 'archivo.txt');
}, false);

*/