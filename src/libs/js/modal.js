        function CreaModal(ModalJS)
        {       MdJSON = JSON.parse(ModalJS);
                
                var modalBase = document.createElement("div");
                modalBase.id = "myModal";
                modalBase.setAttribute("class","modalBase");
                document.body.appendChild(modalBase);

                var xheight = "";
                if (MdJSON.hasOwnProperty('Alto'))
                    xheight = "; height: "+MdJSON.Alto+"px";

                 var modalDocu = document.createElement("div");
                 modalDocu.setAttribute("class","modalDocu");
                 modalDocu.setAttribute("style","width: "+MdJSON.Ancho+"px"+xheight);
                 modalBase.appendChild(modalDocu);

                 var modalCapt = document.createElement("div");
                 modalCapt.setAttribute("class","modalCapt");
                 modalDocu.appendChild(modalCapt);

                 var modalClos = document.createElement("div");
                 modalClos.setAttribute("class","modalClos");
                 modalClos.setAttribute("onclick","cerrar();");
                 modalClos.innerHTML="×";
                 modalCapt.appendChild(modalClos);

                 var modalTitl = document.createElement("p");
                 modalTitl.setAttribute("class","modalTitl");
                 modalTitl.innerHTML=MdJSON.Title;
                 modalCapt.appendChild(modalTitl);

                 var modalData = document.createElement("div");
                 modalData.id = MdJSON.Modal;
                 modalData.innerHTML = MdJSON.Carga;
                 modalDocu.appendChild(modalData);
               //ModalAjaxJSON(MdJSON.Carga,'POST','',MdJSON);
        }

        function CreaModalP(ModalJS)
        {       MdJSON = JSON.parse(ModalJS);
                
                var modalBase = document.createElement("div");
                modalBase.id = "myModal";
                modalBase.setAttribute("class","modalBase");
                document.body.appendChild(modalBase);

                 var modalDocu = document.createElement("div");
                 modalDocu.setAttribute("class","modalDocu");
                 modalDocu.setAttribute("style","width: "+MdJSON.Ancho+"px");
                 modalBase.appendChild(modalDocu);

                 var modalCapt = document.createElement("div");
                 modalCapt.setAttribute("class","modalCapt");
                 modalDocu.appendChild(modalCapt);

                 var modalClos = document.createElement("div");
                 modalClos.setAttribute("class","modalClos");
                 modalClos.setAttribute("onclick","cerrar(); cerrarmodal();");
                 modalClos.innerHTML="×";
                 modalCapt.appendChild(modalClos);

                 var modalTitl = document.createElement("p");
                 modalTitl.setAttribute("class","modalTitl");
                 modalTitl.innerHTML=MdJSON.Title;
                 modalCapt.appendChild(modalTitl);

                 var modalData = document.createElement("div");
                 modalData.id = MdJSON.Modal;
                 modalData.innerHTML = MdJSON.Carga;
                 modalDocu.appendChild(modalData);
               //ModalAjaxJSON(MdJSON.Carga,'POST','',MdJSON);
        }

        function cerrar()
        {        var modalBase=document.getElementById("myModal");
      
                 document.body.removeChild(modalBase);
        }


        function ModalAjaxJSON(Url,Metodo,ParamPost,ParamJSON)
        {       var MyHTTP = CreateAjaxObject();
                if(MyHTTP)
                {   MyHTTP.open(Metodo, Url, true);
                    MyHTTP.onreadystatechange = function(){  ModalAgenteJSON(MyHTTP,ParamJSON);   };
                        
                    if(Metodo==="POST")
                    {   MyHTTP.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        MyHTTP.send(EncodeParam(ParamPost));
                    }
                    else
                    MyHTTP.send(null); 
                }
        }

        function ModalAgenteJSON(MyHTTP,ParamJSON)
        {       MyObj = document.getElementById(ParamJSON.Modal);
                
                if (MyHTTP.readyState==READY_STATE_LOADING)
    	            MyObj.innerHTML= "CARGANDO...";
                else 
                {   if(MyHTTP.readyState==READY_STATE_COMPLETE)
                    {   if(MyHTTP.status==200)
                            MyObj.innerHTML = MyHTTP.responseText;
                        else if (MyHTTP.status==404)
			    MyObj.innerHTML= "No se encuentra el objeto.";
   	                else
		            MyObj.innerHTML=MyHTTP.status;
                    }    
                }
        }