                 function soloNumeros(Evt)
                 {        Evt = (Evt) ? Evt : window.event
                          var charCode = (Evt.which) ? Evt.which : Evt.keyCode
                          
                          if ((charCode>95 && charCode<106) || (charCode>47 && charCode<58) || charCode==35 || charCode==36 || charCode==39 || charCode==37 || charCode==13 || charCode==9 || charCode==8 || charCode==46)
                              return true;
                          else
                              return false;
                 }

                 function soloLetras(Evt)
                 {        Evt = (Evt) ? Evt : window.event
                          var charCode = (Evt.which) ? Evt.which : Evt.keyCode
                          if ((charCode>64 && charCode<91) || charCode==35 || charCode==36 || charCode==39 || charCode==37 || charCode==13 || charCode==9 || charCode==8 || charCode==46)
                              return true;
                          else
                              return false;
                 }

                function Dependencia(Evt,Parameters,TipoValidacion)
                {       Evt = (Evt) ? Evt : window.event
                        var chrCode = (Evt.which) ? Evt.which : Evt.keyCode
                        if (chrCode == 8 || chrCode == 46 || chrCode == 32 || (chrCode>47 && chrCode<58) || (chrCode>95 && chrCode<106) || (chrCode>64 && chrCode<91))
                        {   resultado = ElementClear(Parameters);
                        }
                        if (TipoValidacion == 'LET' || TipoValidacion == 'LETRAS')    return soloLetras(Evt);
                        if (TipoValidacion == 'NUMLET' || TipoValidacion == 'LETNUM') return soloNumerosLetras(Evt);
                        if (TipoValidacion == 'NUM' || TipoValidacion == 'NUMEROS')   return soloNumeros(Evt);
                }
                 
                function Dependencia_II(Evt,Validacion)
                {       resultado = false;
                        Evt = (Evt) ? Evt : window.event
                        var chrCode = (Evt.which) ? Evt.which : Evt.keyCode
                        if (chrCode == 8 || chrCode == 46 || chrCode == 32 || (chrCode>47 && chrCode<58) || (chrCode>95 && chrCode<106) || (chrCode>64 && chrCode<91))
                        {   resultado = true; 
                        }
                        if (Validacion == 'LET'    || Validacion == 'LETRAS')  return Array(resultado,chrCode,soloLetras(Evt));
                        if (Validacion == 'NUMLET' || Validacion == 'LETNUM')  return Array(resultado,chrCode,soloNumerosLetras(Evt));
                        if (Validacion == 'NUM'    || Validacion == 'NUMEROS') return Array(resultado,chrCode,soloNumeros(Evt));
                }


                function soloFloatDep(Evt,MyObj,decimales,Parameters)
                {       Evt = (Evt) ? Evt : window.event
                        var chrCode = (Evt.which) ? Evt.which : Evt.keyCode
                        if (chrCode == 8 || chrCode == 46 || chrCode == 32 || (chrCode>47 && chrCode<58) || (chrCode>95 && chrCode<106) || (chrCode>64 && chrCode<91))
                        {   resultado = ElementClear(Parameters);
                        }
                        return soloFloat(Evt,MyObj,decimales);
                }


                function soloFloat(evt,MyObj,decimales)
                {       evt = (evt) ? evt : window.event
                        var charCode = (evt.which) ? evt.which : evt.keyCode;
                            
                        if (charCode==35 || charCode==36 || charCode==39 || charCode==37 || charCode==13 || charCode==9 || charCode==8 || charCode==46)
                            return true
                        else
                        {   if (charCode==190 || charCode==110 || (charCode>47 && charCode<58) || (charCode>95 && charCode<106))
                            {   if (charCode==190 || charCode==110)
                                    valor=MyObj.value+".";
                                else
                                valor=MyObj.value+String.fromCharCode(charCode);
                                return EsDoble(valor,decimales);
                            }
                            else
                            return false;
                        }
                }

                function EsDoble(valor,decimales)
                {       var puntos=0;
                        var numero=valor;
                        var conteo=false;
                        var decima=0;
                        if (numero.length==0)
                            return false;
                        else
                        {   for(i=0;i<numero.length;i++)
                            {   if (conteo)
                                {  if (numero.substring(i,i+1)!=".")
                                       decima=decima+1;
                                }
                                if (numero.substring(i,i+1)==".")
                                {   puntos=puntos+1;
                                    conteo=true;
                                }
                            }
                            if (puntos>=2 || decima>decimales) return false;
                            if (puntos<=1 || decima<=decimales) return true;
                        }
                }

                function soloNumerosLetras(evt)
                {
                        evt = (evt) ? evt : window.event
                        var charCode = (evt.which) ? evt.which : evt.keyCode
                        if ((charCode>95 && charCode<106) || charCode==35 || charCode==36 || charCode==39 || charCode==37 || charCode==13 || charCode==9 || charCode==8 || charCode==46 || (charCode>47 && charCode<58) || (charCode>64 && charCode<91))
                            return true;
                        else
                            return false;
                }
                
                function SoloFormula(evt)
                {       return true;
                }

                function StrPadCeroNumber(format,numero)
                {       var fila=format+numero;
                        if (fila.length>format.length)
                        {   b = fila.length - format.length;
                            return fila.substring(b, fila.length);
                        }
                }

                function soloCuenta(evt,MyObj)
                {       evt = (evt) ? evt : window.event
                        var charCode = (evt.which) ? evt.which : evt.keyCode;

                        if (charCode==35 || charCode==36 || charCode==39 || charCode==37 || charCode==13 || charCode==9 || charCode==8 || charCode==46)
                            return true
                        else
                        {   if (charCode==190 || (charCode>47 && charCode<58) || (charCode>95 && charCode<106))
                            {   if (charCode==190)
                                    valor=MyObj.value+".";
                                else
                                valor=MyObj.value+String.fromCharCode(charCode);
                                return true;
                            }
                            else
                            return false;
                        }
                }
