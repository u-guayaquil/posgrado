/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


                function FillOptionCbx(nameCbx,Datos)
                {       var ObjCbx = ClearOptionCbx(nameCbx);
                        var Items = Datos.length;
                        for (var b=0; b < Items; b++)
                        {   var option = document.createElement("option");
                            option.value = Datos[b][0];
                            option.text = Datos[b][1];
                            option.selected = true;
                            ObjCbx.add(option);
                        }
                        return false;
                }
                
                function ClearOptionCbx(nameCbx)
                {       var ObjCbx = document.getElementById(nameCbx);
                        var items = ObjCbx.length;
                        for (var b=0; b < items; b++)
                        {    ObjCbx.remove(0);
                        }
                        return ObjCbx;
                }

                function ExistOptionCbx(nameCbx,ValId)
                {       var ObjCbx = document.getElementById(nameCbx);
                        Existe = false;
                        for (var b=0; b < ObjCbx.length; b++)
                        {    if (ValId == ObjCbx[b].value)
                             Existe = true;
                        }
                        return Existe;
                }

                function FindOptionCbx(nameCbx,ValId)
                {       var ObjCbx = document.getElementById(nameCbx);
                        for (var b=0; b < ObjCbx.length; b++)
                        {    if (ValId == ObjCbx[b].value)
                             return b;
                        }
                        return -1;
                }

                function RemoveOptionCbx(nameCbx,ValId)
                {       var ObjCbx = document.getElementById(nameCbx);
                        if (ValId!='' && ValId!=0)
                        {   var Idx = FindOptionCbx(nameCbx,ValId);
                            if (Idx>=0)
                            ObjCbx.remove(Idx);
                        }
                        else
                        {   var IdxCurrent = ObjCbx.selectedIndex;
                            ObjCbx.remove(IdxCurrent);
                          //ObjCbx.selectedIndex = IdxCurrent - 1;
                        }
                        return false;
                }

                function RetornaOptions(IdLevel)
                {       var Rows = [];
                        for (var h=0; h<=IdLevel; h++)
                        {   var Cols = [h,(h==0 ? "UNIDAD": (h==1 ? "TEMA":"SUBTEMA"))];
                            Rows.push(Cols);    
                        }
                        return Rows;    
                }

