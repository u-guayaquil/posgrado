var WorkGrid = new Array();
        
function CreaWorkGrid(ConfWPG) 
{       var DataWPG = JSON.parse(ConfWPG);
        if (!document.getElementById(DataWPG.Name+DataWPG.Sufy))    
        {   var CapaWGP = document.getElementById(DataWPG.Capa);
                var DivcWGP = document.createElement("div");
                DivcWGP.setAttribute("class","SchCab");
                CapaWGP.appendChild(DivcWGP);
            
            var TabcWGP = document.createElement("TABLE");
                TabcWGP.setAttribute("class","SchGrdCab");
                DivcWGP.appendChild(TabcWGP);

            tope = (DataWPG.Hcob==0 ? 1 : 2);
            for (k=0; k<tope; k++)
            {   var RwWSP = TabcWGP.insertRow(TabcWGP.rows.length);
                    RwWSP.setAttribute("style","height: " + (k==0 ? DataWPG.Hcab : DataWPG.Hcob) + "px; ");
            
                    celda = DataWPG.Cell.length;
                    for (i=0; i<celda; i++)
                    {   var ClWSP = RwWSP.insertCell(i); 
                            ClWSP.setAttribute("class", (k==0 ? "SchGrdCabCell" : "SchGrdCobCell") );
                            var Tamano = "width: " + DataWPG.Cell[i][0] + "px; ";
                            var Alinea = "text-align: center; ";
                            var Oculta = (DataWPG.Cell[i][0]==0 ? "display: none" : "");
                            ClWSP.setAttribute("style", Tamano + Alinea + Oculta);
                            ClWSP.innerHTML = DataWPG.Cell[i][k+2];
                    }
                    
                    /*if (tope==2 && DataWPG.Erow==0 && (DataWPG.btEd!=0 || DataWPG.btDl!=0))
                    {   var ClWSP = RwWSP.insertCell(i); 
                            ClWSP.setAttribute("class", (k==0 ? "SchGrdCabCell" : "SchGrdCobCell") );
                            var Tamano = (DataWPG.btEd!=0 && DataWPG.btDl!=0) ? "width: 63px; " : "width: 30px; ";
                            var Alinea = "text-align: center; ";
                            if (k!=0)
                            {   var fnGrid = "WorkGridAdds" + DataWPG.Name + DataWPG.Sufy + "('" + DataWPG.Sufy + "')";
                                ClWSP.setAttribute("onclick",fnGrid);                
                                ClWSP.innerHTML = "<img src='src/public/img/insert.png' border='0'/>" ;
                            }    
                            ClWSP.setAttribute("style", Tamano + Alinea);
                    }  */  
                    if (tope==2 && DataWPG.Erow==0 && (DataWPG.btAsig!=0 || DataWPG.btEd!=0 || DataWPG.btSv!=0 || DataWPG.btDc!=0 ||DataWPG.btDl!=0))
                    {
                        var ClWSP = RwWSP.insertCell(i); 
                            ClWSP.setAttribute("class", (k==0 ? "SchGrdCabCell" : "SchGrdCobCell") );
                            var Tamano = (DataWPG.btAsig!=0 || DataWPG.btEd!=0 && DataWPG.btDc!=0 && DataWPG.btSv!=0 && DataWPG.btDl!=0) ? "width: 93px;" : "width: 30px;";
                            var Alinea = "text-align: center; ";
                            if (k!=0)
                            {   var fnGrid = "WorkGridAdds" + DataWPG.Name + DataWPG.Sufy + "('" + DataWPG.Sufy + "')";
                                ClWSP.setAttribute("onclick",fnGrid);                
                                ClWSP.innerHTML = "<img src='src/public/img/insert.png' border='0'/>" ;
                            }    
                            ClWSP.setAttribute("style", Tamano + Alinea);
                    }    
                    
            }
            
            var csstWGP = (DataWPG.Erow==0 ? "WrkGrdDet" : "SchGrdDet");
            
            var DivdWGP = document.createElement("div");
                DivdWGP.id = "WorkDet" + DataWPG.Name + DataWPG.Sufy;
                DivdWGP.setAttribute("class","SchDet");
                DivdWGP.setAttribute("style","height: " + DataWPG.Hdet + "px; ");
                CapaWGP.appendChild(DivdWGP);

            var TabdWGP = document.createElement("TABLE");
                TabdWGP.id = DataWPG.Name + DataWPG.Sufy;
                TabdWGP.setAttribute("class",csstWGP); 
                DivdWGP.appendChild(TabdWGP);
            
          //Registro de los objetos WorkGrid 
            var RobjWGP = new Object();               
                RobjWGP.ob  = 0;                           //Maximo numero de Filas       
                RobjWGP.sf  = DataWPG.Sufy;                //Sufijo    
                RobjWGP.id  = DataWPG.Name+DataWPG.Sufy;   //Id
                RobjWGP.Hr  = DataWPG.Hrow;                //Altura de las filas
                RobjWGP.Er  = DataWPG.Erow;                //Envento Click por fila
                RobjWGP.Cl  = DataWPG.Cell;                //Formato arreglo de cada celda
                RobjWGP.Ed  = DataWPG.btEd;                //Edita la fila
                RobjWGP.Dl  = DataWPG.btDl;                //Elimina la fila
                RobjWGP.Sv  = DataWPG.btSv;                //Guarda la fila
                RobjWGP.Dc  = DataWPG.btDc;                //Inhabilita la fila
                RobjWGP.Asg = DataWPG.btAsg;               //Asigna estudiante
                WorkGrid[DataWPG.Name] = RobjWGP;
        }
        else
        alert("Ya existe un objeto WorkGrid con el nombre: " + DataWPG.Name + DataWPG.Sufy);
}

function CreaWorkGridFila(IdxTb,Datos)
{       var Celdas = WorkGrid[IdxTb].Cl.length;
        
        if (Celdas>0)
        {   var TabsWGP = document.getElementById(WorkGrid[IdxTb].id);
            if (!TabsWGP)
                alert("Indice incorrecto del objeto WorkGrid.");
            else
            {   var Index = TabsWGP.rows.length;
                if (Index==0) WorkGrid[IdxTb].ob=0;
                    
                var FilaWSP = TabsWGP.insertRow(Index);
                    FilaWSP.id = "R" + IdxTb + "_" + WorkGrid[IdxTb].ob;
                    
                    var Views = 1;
                    var Color = "";
                    var Texto = "";
                    var Colum = -1;    
                    if (Array.isArray(Datos[Celdas-1]))            
                    {   Views = Datos[Celdas-1][0];
                        Color = "color: "+Datos[Celdas-1][1]+";";
                        Texto = Datos[Celdas-1][2];
                        Colum = Celdas-1;
                    }
                    
                    FilaWSP.setAttribute("style","height: "+WorkGrid[IdxTb].Hr + "px; "+Color);

                    var csstCeldas = "WrkGrdDetCell";
                    var EventoFila = WorkGrid[IdxTb].Er; //Si hay evento click por fila
                    if (EventoFila!=0)
                    {   var fnWorkGrid = "WorkGrid" + WorkGrid[IdxTb].id + "(this)";
                        FilaWSP.setAttribute("onclick",fnWorkGrid);
                        csstCeldas = "SchGrdDetCell";
                    }    
                
                    for (i=0; i<Celdas; i++)
                    {   var CellWSP = FilaWSP.insertCell(i);
                            CellWSP.id   = "C" + IdxTb + "_" + WorkGrid[IdxTb].ob + "_" + i;
                            CellWSP.name = CellWSP.id;
                            CellWSP.setAttribute("class",csstCeldas);
                            var Tamano = "width: " + WorkGrid[IdxTb].Cl[i][0] + "px; ";
                            var Alinea = "text-align: " + WorkGrid[IdxTb].Cl[i][1] + "; ";
                            var Oculta = (WorkGrid[IdxTb].Cl[i][0]==0 ? "display: none" : "");
                            CellWSP.setAttribute("style", Tamano + Alinea + Oculta);
                            CellWSP.innerHTML = (Colum==i ? Texto:Datos[i]);
                    }
                
                    if (EventoFila==0) 
                    {   if (WorkGrid[IdxTb].Asg==1) 
                        {   CreaWorkGridCeldaAsignar(IdxTb,FilaWSP.insertCell(i),i,Views);
                            i++;
                        } 
                        if (WorkGrid[IdxTb].Sv==1) 
                        {   CreaWorkGridCeldaSave(IdxTb,FilaWSP.insertCell(i),i,Views);
                            i++;
                        } 
                        if (WorkGrid[IdxTb].Ed==1) 
                        {   CreaWorkGridCeldaEdit(IdxTb,FilaWSP.insertCell(i),i,Views);
                            i++;
                        }    
                        if (WorkGrid[IdxTb].Dc==1) 
                        {   CreaWorkGridCeldaDecline(IdxTb,FilaWSP.insertCell(i),i,Views);
                            i++;
                        }  
                        if (WorkGrid[IdxTb].Dl==1) 
                        {   CreaWorkGridCeldaDelt(IdxTb,FilaWSP.insertCell(i),i,Views);
                        }
                    }    
                    WorkGrid[IdxTb].ob = WorkGrid[IdxTb].ob + 1; //Aumenta el numero de objetos creados
            } 
        }
        else
        alert("No ha configurado celdas para la tabla.");
}       

function CreaWorkGridCeldaAsignar(IdxTb,AsigCell,Idx,View)
{       var FilaPadre = "R" + IdxTb + "_" + WorkGrid[IdxTb].ob;
    
        AsigCell.id   = "C" + IdxTb + "_" + WorkGrid[IdxTb].ob + "_" + Idx;
        AsigCell.name = AsigCell.id;
        AsigCell.setAttribute("class","WrkGrdDetCellOP");
        
        var Asignar = "";
        var Tamano = "width: 30px;";
        var Alinea = "text-align: center;";
        
        if (View==1)
        {   Asignar = "background: url('src/public/img/asignar.png') no-repeat center;";
            var fnGrid = "WorkGridAsig" + WorkGrid[IdxTb].id + "(" + FilaPadre + ",this)";
            AsigCell.setAttribute("onclick",fnGrid);
        }    
        AsigCell.setAttribute("style", Tamano + Alinea + Asignar);
}

function CreaWorkGridCeldaSave(IdxTb,EditCell,Idx,View)
{       FilaPadre = "R" + IdxTb + "_" + WorkGrid[IdxTb].ob;
        
        EditCell.id   = "C" + IdxTb + "_" + WorkGrid[IdxTb].ob + "_" + Idx;
        EditCell.name = EditCell.id;
        EditCell.setAttribute("class","WrkGrdDetCellOP");
        
        var Editar = "";    
        var Tamano = "width: 30px;";
        var Alinea = "text-align: center;";
        if (View==1)
        {   Editar = "background: url('src/public/img/guarda_2.png') no-repeat center;";
            var fnGrid = "WorkGridSave" + WorkGrid[IdxTb].id + "(" + FilaPadre + ",this)";
            EditCell.setAttribute("onclick",fnGrid);
        }    
        EditCell.setAttribute("style", Tamano + Alinea + Editar);
}

function CreaWorkGridCeldaDecline(IdxTb,DeclCell,Idx,View)
{       var FilaPadre = "R" + IdxTb + "_" + WorkGrid[IdxTb].ob;
    
        DeclCell.id   = "C" + IdxTb + "_" + WorkGrid[IdxTb].ob + "_" + Idx;
        DeclCell.name = DeclCell.id;
        DeclCell.setAttribute("class","WrkGrdDetCellOP");
        
        var Delete = "";
        var Tamano = "width: 30px;";
        var Alinea = "text-align: center;";
        
        if (View==1)
        {   Delete = "background: url('src/public/img/cancela_mini.png') no-repeat center;";
            var fnGrid = "WorkGridDecline" + WorkGrid[IdxTb].id + "(" + FilaPadre + ",this)";
            DeclCell.setAttribute("onclick",fnGrid);
        }    
        DeclCell.setAttribute("style", Tamano + Alinea + Delete);
} 
          
function CreaWorkGridCeldaEdit(IdxTb,EditCell,Idx,View)
{       FilaPadre = "R" + IdxTb + "_" + WorkGrid[IdxTb].ob;
        
        EditCell.id   = "C" + IdxTb + "_" + WorkGrid[IdxTb].ob + "_" + Idx;
        EditCell.name = EditCell.id;
        EditCell.setAttribute("class","WrkGrdDetCellOP");
        
        var Editar = "";    
        var Tamano = "width: 30px;";
        var Alinea = "text-align: center;";
        if (View==1)
        {   Editar = "background: url('src/public/img/edita.png') no-repeat center;";
            var fnGrid = "WorkGridEdit" + WorkGrid[IdxTb].id + "(" + FilaPadre + ",this)";
            EditCell.setAttribute("onclick",fnGrid);
        }    
        EditCell.setAttribute("style", Tamano + Alinea + Editar);
}

function CreaWorkGridCeldaDelt(IdxTb,DeltCell,Idx,View)
{       var FilaPadre = "R" + IdxTb + "_" + WorkGrid[IdxTb].ob;
    
        DeltCell.id   = "C" + IdxTb + "_" + WorkGrid[IdxTb].ob + "_" + Idx;
        DeltCell.name = DeltCell.id;
        DeltCell.setAttribute("class","WrkGrdDetCellOP");
        
        var Delete = "";
        var Tamano = "width: 30px;";
        var Alinea = "text-align: center;";
        
        if (View==1)
        {   Delete = "background: url('src/public/img/delete.gif') no-repeat center;";
            var fnGrid = "WorkGridDelt" + WorkGrid[IdxTb].id + "(" + FilaPadre + ",this)";
            DeltCell.setAttribute("onclick",fnGrid);
        }    
        DeltCell.setAttribute("style", Tamano + Alinea + Delete);
}

function EditWorkGridFila(ActiveRow,Datos)
{       Celdas = Datos.length;
        for (i=0; i<Celdas; i++)
        {    ActiveRow.cells[i].childNodes[0].nodeValue = Datos[i];   
        }
}

function DeltWorkGridFila(Index,ActRow)
{       var MyTbl = document.getElementById(WorkGrid[Index].id);
        MyTbl.deleteRow(ActRow.rowIndex);
        if (MyTbl.rows.length==0) { WorkGrid[Index].ob=0; }
        return true;
}

function WriteWorkGridFila(Tarea,Objeto,Texto)
{       if (Tarea=="F")
            Texto.value = Texto.value+"{"+Objeto.value+"()}";
        else if (Tarea=="P")
            Texto.value = Texto.value+"{"+Objeto.value+"(0)}";
        else if (Tarea=="V")
            Texto.value = Texto.value+Objeto.value;    
        else if (Tarea=="C")
            Texto.value = Texto.value+"{ConstVal("+Objeto.value+")}";                                    
        return false;   
}

function ReadWorkGrid(Idx,Campos)
{       var MyTabla = new Array();
        var MyCelda = Campos.length;
                
        var MyTbl = document.getElementById(WorkGrid[Idx].id);  
        var MyRow = MyTbl.getElementsByTagName('tr'); 
        for (i=0; i<MyRow.length; i++)      
        {   var MyFilas = new Object();
            for (j=0;j<MyCelda;j++)      
            {   var Ix = Campos[j]; 
                MyFilas[WorkGrid[Idx].Cl[Ix][2]] = MyRow[i].cells[Ix].childNodes[0].nodeValue;
            }    
            MyTabla[i]=MyFilas;
        }
        return MyTabla;
}

function DeleteWorkGrid(Idx)
{       var MyTbl = document.getElementById(WorkGrid[Idx].id);  
        while (MyTbl.rows.length>0)
        {      MyTbl.deleteRow(0); 
        }   
        WorkGrid[Idx].ob=0; 
}    

function ExistWorkGridRows(Idx)
{       var MyTbl = document.getElementById(WorkGrid[Idx].id);  
        var MyRow = MyTbl.getElementsByTagName('tr'); 
        return (MyRow.length>0 ? true : false);      
}        


