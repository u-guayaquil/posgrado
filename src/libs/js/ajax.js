        var READY_STATE_UNINITIALIZED=0;
        var READY_STATE_LOADING=1;
        var READY_STATE_LOADED=2;
        var READY_STATE_INTERACTIVE=3;
        var READY_STATE_COMPLETE=4;

    	String.prototype.AjaxTrim = function()
        {        return this.replace(/^\s*(\b.*\b|)\s*$/, "$1");
        }

        function TrimAjax(Cadena)
        {        return Cadena.AjaxTrim();
        }
        
        function EncodeParam(_POST)
        {       var POST_STR = "";
                if (_POST.length > 0)
                {   var arreglo = _POST.split(":");
      		    for(i=0; i<(arreglo.length); i++)
		    {   POST_STR = POST_STR + (i>0 ? "&": "");
                        OBJT_TMP = document.getElementById(arreglo[i]);
                        POST_STR = POST_STR + arreglo[i] + "=" + retornaValorObj(OBJT_TMP);
		    }
                }
		return POST_STR;
        }

        function retornaValorObj(OBJT_TMP)
        {       if (OBJT_TMP.tagName == "DIV" || OBJT_TMP.tagName == "TD" || OBJT_TMP.tagName == "TH")
                    return TrimAjax(OBJT_TMP.innerHTML);
                else
                {   if (OBJT_TMP.type == "checkbox")
                    {   return OBJT_TMP.checked;
                    }    
                    return TrimAjax(OBJT_TMP.value);
  	        }
  	}

        function CreateAjaxObject()
     	{       if (window.XMLHttpRequest)
              	    return new XMLHttpRequest();
             	else
                {   if (window.ActiveXObject)
                    {   try
          	        {   return new ActiveXObject("Msxml2.XMLHTTP");    }
             		catch(e)
  			{   return new ActiveXObject("Microsoft.XMLHTTP"); }
   		    }
                }
        }
        


       /**************************** AJAX MODO TRADICIONAL UNO SOLO A LA VEZ ***********************************/
        
        /*
        var MyBasicAjax = false;
        function BasicAjax(Url,Metodo,Funcion,ParamsPost)
        {        MyBasicAjax = CreateAjaxObject();
                 if(MyBasicAjax)
                 {  MyBasicAjax.open(Metodo, Url, true);
                    MyBasicAjax.onreadystatechange = Funcion;
                    if (Metodo == "POST")
                    {   MyBasicAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        MyBasicAjax.send(EncoderParam(ParamsPost));
                    }
                    else
                    MyBasicAjax.send(null);
                 }
        }
        */



      /*AJAX MODO DEDICADO SOLO OPCIONES
        function LoadOptionAjax(Url,Method,Container,Parameters)
        {        var MyLoadAjax = false;

                 MyLoadAjax = CreateAjaxObject();
                 if(MyLoadAjax)
                 {  MyLoadAjax.open(Method, Url, true);

                    MyLoadAjax.onreadystatechange = function(){  LoadOption(MyLoadAjax,Container);   };
                    if (Method == "POST")
                    {   MyLoadAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        MyLoadAjax.send(EncoderParam(Parameters));
                    }
                    else
                    MyLoadAjax.send(null);

                 }
        }

        function LoadOption(MyAjaxRequest,DivObject)
        {        MyDivObj = document.getElementById(DivObject);
                 if (MyAjaxRequest.readyState == READY_STATE_LOADING)
    	             MyDivObj.innerHTML= "CARGANDO...";
                 else if(MyAjaxRequest.readyState == READY_STATE_COMPLETE)
                 {    if(MyAjaxRequest.status == 200)
                         MyDivObj.innerHTML = MyAjaxRequest.responseText;
                      else if (MyAjaxRequest.status == 404)
			               MyDivObj.innerHTML = "No se encuentra el objeto.";
   	                  else
		                   MyDivObj.innerHTML = MyAjaxRequest.status;
                 }
        }
        */
