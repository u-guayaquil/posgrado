              function BarButtonState(Evento)
              {        
                        if (Evento=="addNew" || Evento=="addMod")
                       {   BarButtonStateEnabled("addSav:addCan");
                           BarButtonStateDisabled("addNew:addMod:addDel:addImp:addExp:addPdf");
                           return true;
                       }
                       else if (Evento=="addDel" || Evento=="addCan" || Evento=="Default") 
                       {        BarButtonStateEnabled("addNew");
                                BarButtonStateDisabled("addSav:addCan:addMod:addDel:addImp:addExp:addPdf");
                                return true;
                       }
                       else if (Evento=="addSav" || Evento=="Active")
                       {        BarButtonStateEnabled("addNew:addMod:addDel:addImp:addExp:addPdf");
                                BarButtonStateDisabled("addSav:addCan");
                                return true;
                       }
                       else if (Evento=="Update")
                       {        BarButtonStateEnabled("addMod:addDel:addImp:addExp:addPdf");
                                BarButtonStateDisabled("addNew:addSav:addCan");
                                return true;
                       }
                       else if (Evento=="addImp")
                       {        return true;
                       }
                       else if (Evento=="Inactive")
                       {        BarButtonStateDisabled("addNew:addSav:addCan:addMod:addDel:addImp:addExp:addPdf");
                                return true;
                       }
                       return false;
              }

              function BarButtonStateDisabled(Disabled)
              {        if (Disabled.length!=0)
                       {   var myBoton=Disabled.split(":");
                           for (i=0; i<(myBoton.length); i++)
                           {    if (!document.getElementById(myBoton[i]))
                                {}
                                else
                                {   Boton=document.getElementById(myBoton[i]);
                                    Boton.disabled=true;
                                    Boton.style.backgroundColor= "#324257";
                                    Boton.style.cursor="auto";
                                }
                           }
                       }
              }

              function BarButtonStateEnabled(Enabled)
              {         
                        if (Enabled.length!=0)
                       {   var myBoton=Enabled.split(":");
                           for (i=0; i<(myBoton.length); i++)
                           {    if (!document.getElementById(myBoton[i]))
                                {}
                                else
                                {   Boton=document.getElementById(myBoton[i]);
                                   
                                    Boton.disabled = false;
                                    Boton.style.backgroundColor= "#4C6483";
                                    Boton.style.cursor="pointer";
                                }
                           }
                       }
              }

