<?php
        require_once ('src/base/DbProvider.php');

        class PgProvider extends DbProvider
        {       
                public function Connect($Login)
                {       $this->ConnID = pg_connect($Login);
                        if ($this->IsConnected())
                            return $this->ConnID;
                        else
                        return false;
                }

                public function IsConnected()
                {       $status = pg_connection_status($this->ConnID);
                        if ($status === PGSQL_CONNECTION_OK)
                            return true;
                        else
                        return false;
                }

                public function Query($SQL)
                {       return pg_query($this->ConnID,$SQL);
                }

                public function RecordAffected($resourceID)
                {       if (is_resource($resourceID))
                            return pg_affected_rows($resourceID);
                        else
                        return -1;
                }

                public function NumRecords($resourceID)
                {       if (is_resource($resourceID))
                            return pg_num_rows($resourceID);
                        else
                        return -1;
                }

                public function NumFields($resourceID)
                {       if (is_resource($resourceID))
                            return pg_num_fields($resourceID);
                        else
                        return -1;
                }
                
                public function FieldData($resourceID,$idx)
                {       if (is_resource($resourceID))
                            return pg_fetch_result($resourceID, $idx);
                        else
                            return -1;
                }

                public function NextRecord($resourceID)
                {       if (is_resource($resourceID))
                            return pg_fetch_row($resourceID);
                        else
                        return false;
                }

                public function NextRecordObject($resourceID)
                {       if (is_resource($resourceID))
                            return pg_fetch_object($resourceID);
                        else
                        return false;
                }

                public function NextRecordAssoc($resourceID)
                {       if (is_resource($resourceID))
                            return pg_fetch_assoc($resourceID);
                        else
                        return false;
                }

                public function NextRecordArray($resourceID)
                {       if (is_resource($resourceID))
                            return pg_fetch_array($resourceID);
                        else
                        return false;
                }

                public function FieldDataByName($resourceID,$FieldName)
                {       if (is_resource($resourceID))
                            return (trim($FieldName)!="" ? pg_fetch_result($resourceID,$FieldName): "");
                        else
                        return "";
                }

                public function FieldNameByIndex($resourceID,$FieldIdx)
                {       if (is_resource($resourceID))
                            return ($FieldIdx>=0 ? pg_field_name($resourceID,$FieldIdx) : "");
                        else
                        return "";
                }

                public function FieldIndexByName($resourceID,$FieldName)
                {       if (is_resource($resourceID))
                            return pg_field_num($resourceID,$FieldName);
                        else
                        return -1;
                }

                public function CloseResource($resourceID)
                {       if (is_resource($resourceID)) pg_free_result($resourceID);
                }

                public function Disconnect()
                {       pg_close($this->ConnID);
                }
                
                public function FillDataRecordAssoc($resourceID){
                        $DataCollection = array();
                        while($filas = $this->NextRecordAssoc($resourceID))
                        {     $DataCollection[] = $filas;
                        }
                        $this->CloseResource($resourceID);
                        return $DataCollection;
                }

        }
?>

