<?php
        abstract class DbProvider
        {       protected $ConnID;

                public abstract function Connect($Login);
                public abstract function IsConnected();
                public abstract function Query($SQL);
                public abstract function RecordAffected($resourceID);
                public abstract function NumRecords($resourceID);
                public abstract function NumFields($resourceID);
                public abstract function FieldData($resourceID,$idx);

                public abstract function NextRecord($resourceID);
                public abstract function NextRecordObject($resourceID);
                public abstract function NextRecordArray($resourceID);
                public abstract function NextRecordAssoc($resourceID);

                public abstract function FieldDataByName($resourceID,$FieldName);
                public abstract function FieldNameByIndex($resourceID,$FieldIdx);
                public abstract function FieldIndexByName($resourceID,$FieldName);

                public abstract function CloseResource($resourceID);
                public abstract function Disconnect();
		public abstract function FillDataRecordAssoc($resourceID);
        }

?>
