<?php
        require_once ('src/base/DbProvider.php');

        class SqlProvider extends DbProvider
        {       
                public function Connect($Login)
                {       $this->ConnID = sqlsrv_connect($Login['host'], array("Database"=>$Login['dbname'], "UID"=>$Login['user'], "PWD"=>$Login['password']));   
                        //print_r($Login); 
                        return $this->ConnID;
                }

                public function IsConnected()
                {       if ($this->ConnID)
                            return true;
                        else
                            return false;
                }

                public function Query($SQL)
                {      if ($this->IsConnected()) 
                           return sqlsrv_query($this->ConnID,$SQL);
                       else
                           return -1; 
                }

                public function RecordAffected($resourceID)
                {       if (is_resource($resourceID))
                            return sqlsrv_rows_affected($resourceID);
                        else
                        return -1;
                }

                public function NumRecords($resourceID)
                {       if (is_resource($resourceID))
                            return sqlsrv_num_rows($resourceID);
                        else
                        return -1;
                }

                public function NumFields($resourceID)
                {       if (is_resource($resourceID))
                            return sqlsrv_num_fields($resourceID);
                        else
                        return -1;
                }
                
                public function FieldData($resourceID,$idx)
                {       if (is_resource($resourceID))
                            return sqlsrv_get_field($resourceID, $idx);
                        else
                            return -1;
                }

                public function NextRecord($resourceID)
                {       if (is_resource($resourceID))
                            return sqlsrv_fetch($resourceID); //pg_fetch_row
                        else
                        return false;
                }

                public function NextRecordObject($resourceID)
                {       if (is_resource($resourceID))
                            return sqlsrv_fetch_object($resourceID);
                        else
                        return false;
                }

                public function NextRecordAssoc($resourceID)
                {       if (is_resource($resourceID))
                            return sqlsrv_fetch_array($resourceID, SQLSRV_FETCH_ASSOC);
                        else
                        return false;
                }

                public function NextRecordArray($resourceID)
                {       if (is_resource($resourceID))
                            return sqlsrv_fetch_array($resourceID,SQLSRV_FETCH_NUMERIC);
                        else
                        return false;
                }
                        
                public function FieldDataByName($resourceID,$FieldName)
                {       if (is_resource($resourceID))
                        {   $GetFieldDataByName = sqlsrv_fetch_array($resourceID,SQLSRV_FETCH_ASSOC);
                            return (trim($FieldName)!="" ? $GetFieldDataByName[$FieldName]: "");
                        }    
                        else
                        return "";
                }

                public function FieldNameByIndex($resourceID,$FieldIdx)
                {       if (is_resource($resourceID))
                            return ($FieldIdx>=0 ? pg_field_name($resourceID,$FieldIdx) : "");
                        else
                        return "";
                }

                public function FieldIndexByName($resourceID,$FieldName)
                {       if (is_resource($resourceID))
                            return pg_field_num($resourceID,$FieldName);
                        else
                        return -1;
                }

                public function CloseResource($resourceID)
                {       if (is_resource($resourceID)) sqlsrv_free_stmt($resourceID);
                }

                public function Disconnect()
                {       sqlsrv_close($this->ConnID);
                }
                
                public function FillDataRecordAssoc($resourceID,$DefaultMsg = "")
                {       if ($resourceID>=0)
                        {   $DataCount = 0;
                            $DataCollection = array();
                            while($filas = $this->NextRecordAssoc($resourceID))
                            {     $DataCount++;
                                  $DataCollection[] = $filas;
                            }
                            $this->CloseResource($resourceID);
                            
                            $Result = ($DataCount>0);
                            return array($Result,($Result ? $DataCollection : $DefaultMsg));
                        }
                        return array(false,"Error en ejecución de consulta.");
                }

        }
?>

