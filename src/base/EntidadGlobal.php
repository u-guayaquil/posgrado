<?php


/** @MappedSuperclass */
class EntidadGlobal {

    /**
     * @Column(name="iduscreacion", type="integer")
     */
    private $iduscreacion;

    /**
     * @Column(name="idusmodifica", type="integer")
     */
    private $idusmodifica = 0;

    /**
     * @Column(name="fecreacion", type="datetime")
     */
    private $fecreacion;

    /**
     * @Column(name="femodificacion", type="datetime")
     */
    private $femodificacion;
    
    /**
     * @Column(name="idestado", type="integer")
     */
    private $idestado;
 
    public function getIduscreacion(){
        return $this->iduscreacion;
    }

    public function getIdusmodifica(){
        return $this->idusmodifica;
    }

    public function getFecreacion(){
        return $this->fecreacion;
    }

    public function getFemodificacion(){
        return $this->femodificacion;
    }


    public function setIduscreacion($iduscreacion){
        $this->iduscreacion = $iduscreacion;
    }

    public function setIdusmodifica($idusmodifica){
        $this->idusmodifica = $idusmodifica;
    }

    public function setFecreacion($fecreacion){
        $this->fecreacion = $fecreacion;
    }

    public function setFemodificacion($femodificacion){
        $this->femodificacion = $femodificacion;
    }
    
    function getIdestado() {
        return $this->idestado;
    }

    function setIdestado($idestado) {
        $this->idestado = $idestado;
    }


}
