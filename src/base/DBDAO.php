<?php
        require_once ("src/base/SqlProvider.php");
        require_once ("src/base/PgProvider.php");
        require_once ("src/libs/clases/Security.php");

        class DBDAO extends Security
        {
            private $provider;
            private static $ConDB;
            private static $ConID;
            private $EstadoDAO = array(true,"Ok");

            function __construct()
            {       if ($this->SecurityFileExist())        
                    {   $provider = $this->DbProvider();
                        if (!class_exists($provider))
                            $this->EstadoDAO= array(false,"El provedor DB no existe o no está implementado.");
                        else
                        {   $this->provider = new $provider; 
                            self::$ConID = $this->provider->Connect($this->DbLogin());
                            if (!$this->provider->isConnected())
                            $this->EstadoDAO= array(false,"No fue posible conectarse a la base de datos.");
                        }
                    }
            }
            
            function DbEstadoServicio()
            {       return $this->EstadoDAO;    
            }

            private function DbProvider()
            {       $Conexion = json_decode(session::getValue("Conexion"));
                    return $this->dencrypt($Conexion->Drv);
            }
            
            function DbModulo()
            {       $Conexion = json_decode(session::getValue("Conexion"));
                    return $Conexion->Mod;
            }
            
            private function DbLogin()
            {       $Conexion = json_decode(session::getValue("Conexion"));
                    $CnDriver = $this->dencrypt($Conexion->Drv);
                    if ($CnDriver=="PgProvider")
                        return $this->DbLoginPostgres($Conexion);
                    if ($CnDriver=="SqlProvider")
                        return $this->DbLoginSqlServer($Conexion); 
            }
            
            private function DbLoginSqlServer($Conexion)
            {       $host = $this->dencrypt($Conexion->Hst);
                    $port = $this->dencrypt($Conexion->Prt);
                    $user = $this->dencrypt($Conexion->Usr);
                    $pass = $this->dencrypt($Conexion->Pss);
                    $dbnm = $this->dencrypt($Conexion->Dbs);
                    return array('user' => $user,'password' => $pass,'port' => $port,'host' => $host,'dbname' => $dbnm);                       
            }

            private function DbLoginPostgres($Conexion)
            {       $host = "host=".$this->dencrypt($Conexion->Hst);;
                    $port = "port=".$this->dencrypt($Conexion->Prt);;
                    $user = "user=".$this->dencrypt($Conexion->Usr);;
                    $pass = "password=".$this->dencrypt($Conexion->Pss);;
                    $dbnm = "dbname=".$this->dencrypt($Conexion->Dbs);;
                    return $host." ".$port." ".$user." ".$pass." ".$dbnm;
            }

            static function getConnection() //  PATRON SINGLENTON
            {       if (self::$ConDB)          
                    {   return self::$ConDB;
                    }
                    else
                    {   $class = __CLASS__; 
                        self::$ConDB = new $class();
                        return self::$ConDB;
                    }   
            }

            static function MyConnectionID()
            {       return self::$ConID;
            }

            function MyFunction()
            {       return $this->provider;
            }

            function MyTransaction($TRANS)
            {       $TRANS = strtoupper($TRANS);
                    if ($TRANS=='BEGIN' || $TRANS=='ROLLBACK' || $TRANS=='COMMIT')
                    {   $this->provider->Query($TRANS);
                        return true;
                    }
                    return false;
            }
            
            public function manejadorExcepciones($excepcion)
            {       $archivoLog = fopen("src/utils/FileExceptionLoger.txt", "a");    
                    fwrite($archivoLog , date("d/m/Y H:i:s") . PHP_EOL);
                    fwrite($archivoLog , $excepcion . PHP_EOL . PHP_EOL);    
                    fclose($archivoLog);  
            }
             
        }
?>


