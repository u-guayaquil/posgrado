<?php
        require_once ("vendor/autoload.php");
        use Doctrine\ORM\Tools\Setup;
        use Doctrine\ORM\EntityManager;
        
        if (Session::exist())
        {   require_once ("src/libs/clases/Security.php");
            $Security = new Security();
            {   if ($Security->SecurityFile("src/utils/empresa/conexion/".Session::getValue("EMPRESA")))        
                {   $Configuracion = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"), true);
                    $Conexion = $Security->LoginAsArray();
                    $_REQUEST['EntityManager'] = EntityManager::create($Conexion, $Configuracion);
                }
            }    
        }        
?>

