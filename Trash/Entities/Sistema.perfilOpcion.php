<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Sistema.perfilOpcion
 *
 * @ORM\Table(name="sistema.perfil_opcion", uniqueConstraints={@ORM\UniqueConstraint(name="idx_idperfil_idopcion_perfilopcion", columns={"idperfil", "idopcion"})}, indexes={@ORM\Index(name="idx_idestado_perfilopcion", columns={"idestado"}), @ORM\Index(name="idx_iduscreacion_perfilopcion", columns={"iduscreacion"}), @ORM\Index(name="IDX_626CAE63F3F5C23A", columns={"idopcion"}), @ORM\Index(name="IDX_626CAE63F2D8DBEB", columns={"idperfil"})})
 * @ORM\Entity
 */
class Sistema.perfilOpcion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="sistema.perfil_opcion_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="opnuevo", type="integer", nullable=true)
     */
    private $opnuevo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="opedita", type="integer", nullable=true)
     */
    private $opedita = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="opinactiva", type="integer", nullable=true)
     */
    private $opinactiva = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="opimprime", type="integer", nullable=true)
     */
    private $opimprime = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="opexporta", type="integer", nullable=true)
     */
    private $opexporta = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="oppdf", type="integer", nullable=true)
     */
    private $oppdf = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="iduscreacion", type="integer", nullable=false)
     */
    private $iduscreacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idusmodifica", type="integer", nullable=true)
     */
    private $idusmodifica = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Sistema.opcion
     *
     * @ORM\ManyToOne(targetEntity="Sistema.opcion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idopcion", referencedColumnName="id")
     * })
     */
    private $idopcion;

    /**
     * @var \Sistema.perfil
     *
     * @ORM\ManyToOne(targetEntity="Sistema.perfil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idperfil", referencedColumnName="id")
     * })
     */
    private $idperfil;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set opnuevo
     *
     * @param integer $opnuevo
     *
     * @return Sistema.perfilOpcion
     */
    public function setOpnuevo($opnuevo)
    {
        $this->opnuevo = $opnuevo;

        return $this;
    }

    /**
     * Get opnuevo
     *
     * @return integer
     */
    public function getOpnuevo()
    {
        return $this->opnuevo;
    }

    /**
     * Set opedita
     *
     * @param integer $opedita
     *
     * @return Sistema.perfilOpcion
     */
    public function setOpedita($opedita)
    {
        $this->opedita = $opedita;

        return $this;
    }

    /**
     * Get opedita
     *
     * @return integer
     */
    public function getOpedita()
    {
        return $this->opedita;
    }

    /**
     * Set opinactiva
     *
     * @param integer $opinactiva
     *
     * @return Sistema.perfilOpcion
     */
    public function setOpinactiva($opinactiva)
    {
        $this->opinactiva = $opinactiva;

        return $this;
    }

    /**
     * Get opinactiva
     *
     * @return integer
     */
    public function getOpinactiva()
    {
        return $this->opinactiva;
    }

    /**
     * Set opimprime
     *
     * @param integer $opimprime
     *
     * @return Sistema.perfilOpcion
     */
    public function setOpimprime($opimprime)
    {
        $this->opimprime = $opimprime;

        return $this;
    }

    /**
     * Get opimprime
     *
     * @return integer
     */
    public function getOpimprime()
    {
        return $this->opimprime;
    }

    /**
     * Set opexporta
     *
     * @param integer $opexporta
     *
     * @return Sistema.perfilOpcion
     */
    public function setOpexporta($opexporta)
    {
        $this->opexporta = $opexporta;

        return $this;
    }

    /**
     * Get opexporta
     *
     * @return integer
     */
    public function getOpexporta()
    {
        return $this->opexporta;
    }

    /**
     * Set oppdf
     *
     * @param integer $oppdf
     *
     * @return Sistema.perfilOpcion
     */
    public function setOppdf($oppdf)
    {
        $this->oppdf = $oppdf;

        return $this;
    }

    /**
     * Get oppdf
     *
     * @return integer
     */
    public function getOppdf()
    {
        return $this->oppdf;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Sistema.perfilOpcion
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param integer $iduscreacion
     *
     * @return Sistema.perfilOpcion
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return integer
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param integer $idusmodifica
     *
     * @return Sistema.perfilOpcion
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return integer
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Sistema.perfilOpcion
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Sistema.perfilOpcion
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idopcion
     *
     * @param \Sistema.opcion $idopcion
     *
     * @return Sistema.perfilOpcion
     */
    public function setIdopcion(\Sistema.opcion $idopcion = null)
    {
        $this->idopcion = $idopcion;

        return $this;
    }

    /**
     * Get idopcion
     *
     * @return \Sistema.opcion
     */
    public function getIdopcion()
    {
        return $this->idopcion;
    }

    /**
     * Set idperfil
     *
     * @param \Sistema.perfil $idperfil
     *
     * @return Sistema.perfilOpcion
     */
    public function setIdperfil(\Sistema.perfil $idperfil = null)
    {
        $this->idperfil = $idperfil;

        return $this;
    }

    /**
     * Get idperfil
     *
     * @return \Sistema.perfil
     */
    public function getIdperfil()
    {
        return $this->idperfil;
    }
}

