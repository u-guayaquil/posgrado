<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * General.sucursalUsuario
 *
 * @ORM\Table(name="general.sucursal_usuario", uniqueConstraints={@ORM\UniqueConstraint(name="idx_idusuario_idsucursal_sucursalusuario", columns={"idusuario", "idsucursal"})}, indexes={@ORM\Index(name="fki_idsucursal_sucursalusuario", columns={"idsucursal"}), @ORM\Index(name="IDX_10B3779EFD61E233", columns={"idusuario"})})
 * @ORM\Entity
 */
class General.sucursalUsuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="general.sucursal_usuario_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \General.sucursal
     *
     * @ORM\ManyToOne(targetEntity="General.sucursal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idsucursal", referencedColumnName="id")
     * })
     */
    private $idsucursal;

    /**
     * @var \Sistema.usuario
     *
     * @ORM\ManyToOne(targetEntity="Sistema.usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuario", referencedColumnName="id")
     * })
     */
    private $idusuario;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idsucursal
     *
     * @param \General.sucursal $idsucursal
     *
     * @return General.sucursalUsuario
     */
    public function setIdsucursal(\General.sucursal $idsucursal = null)
    {
        $this->idsucursal = $idsucursal;

        return $this;
    }

    /**
     * Get idsucursal
     *
     * @return \General.sucursal
     */
    public function getIdsucursal()
    {
        return $this->idsucursal;
    }

    /**
     * Set idusuario
     *
     * @param \Sistema.usuario $idusuario
     *
     * @return General.sucursalUsuario
     */
    public function setIdusuario(\Sistema.usuario $idusuario = null)
    {
        $this->idusuario = $idusuario;

        return $this;
    }

    /**
     * Get idusuario
     *
     * @return \Sistema.usuario
     */
    public function getIdusuario()
    {
        return $this->idusuario;
    }
}

