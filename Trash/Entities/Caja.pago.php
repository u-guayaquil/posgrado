<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Caja.pago
 *
 * @ORM\Table(name="caja.pago", indexes={@ORM\Index(name="idx_idestado_cajapago", columns={"idestado"}), @ORM\Index(name="fki_idsubtipo_cajapago", columns={"idsubtipo"}), @ORM\Index(name="fki_idcajamed_cajapago", columns={"idcajamed"}), @ORM\Index(name="idx_iduscreacion_cajapago", columns={"iduscreacion"})})
 * @ORM\Entity
 */
class Caja.pago
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=3, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="caja.pago_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=20, nullable=false)
     */
    private $descripcion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="idctacontable", type="string", length=5, nullable=false)
     */
    private $idctacontable;

    /**
     * @var string
     *
     * @ORM\Column(name="idsucursal", type="string", length=2, nullable=false)
     */
    private $idsucursal;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Caja.subtipo
     *
     * @ORM\ManyToOne(targetEntity="Caja.subtipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idsubtipo", referencedColumnName="id")
     * })
     */
    private $idsubtipo;

    /**
     * @var \Caja.medio
     *
     * @ORM\ManyToOne(targetEntity="Caja.medio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcajamed", referencedColumnName="id")
     * })
     */
    private $idcajamed;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Caja.pago
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idctacontable
     *
     * @param string $idctacontable
     *
     * @return Caja.pago
     */
    public function setIdctacontable($idctacontable)
    {
        $this->idctacontable = $idctacontable;

        return $this;
    }

    /**
     * Get idctacontable
     *
     * @return string
     */
    public function getIdctacontable()
    {
        return $this->idctacontable;
    }

    /**
     * Set idsucursal
     *
     * @param string $idsucursal
     *
     * @return Caja.pago
     */
    public function setIdsucursal($idsucursal)
    {
        $this->idsucursal = $idsucursal;

        return $this;
    }

    /**
     * Get idsucursal
     *
     * @return string
     */
    public function getIdsucursal()
    {
        return $this->idsucursal;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Caja.pago
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return Caja.pago
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return Caja.pago
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Caja.pago
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Caja.pago
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idsubtipo
     *
     * @param \Caja.subtipo $idsubtipo
     *
     * @return Caja.pago
     */
    public function setIdsubtipo(\Caja.subtipo $idsubtipo = null)
    {
        $this->idsubtipo = $idsubtipo;

        return $this;
    }

    /**
     * Get idsubtipo
     *
     * @return \Caja.subtipo
     */
    public function getIdsubtipo()
    {
        return $this->idsubtipo;
    }

    /**
     * Set idcajamed
     *
     * @param \Caja.medio $idcajamed
     *
     * @return Caja.pago
     */
    public function setIdcajamed(\Caja.medio $idcajamed = null)
    {
        $this->idcajamed = $idcajamed;

        return $this;
    }

    /**
     * Get idcajamed
     *
     * @return \Caja.medio
     */
    public function getIdcajamed()
    {
        return $this->idcajamed;
    }
}

