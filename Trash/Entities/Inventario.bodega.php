<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Inventario.bodega
 *
 * @ORM\Table(name="inventario.bodega", uniqueConstraints={@ORM\UniqueConstraint(name="idx_descripcion_bodega", columns={"descripcion"}), @ORM\UniqueConstraint(name="idx_codigo_bodega", columns={"codigo"})}, indexes={@ORM\Index(name="fki_pk_idtipo_bodega", columns={"idtipobodega"}), @ORM\Index(name="fki_idtipobodega_bodega", columns={"idtipobodega"}), @ORM\Index(name="fki_idsucursal_bodega", columns={"idsucursal"}), @ORM\Index(name="idx_iduscreacion_bodega", columns={"iduscreacion"}), @ORM\Index(name="idx_idestado_bodega", columns={"idestado"})})
 * @ORM\Entity
 */
class Inventario.bodega
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="inventario.bodega_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=20, nullable=false)
     */
    private $descripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="iduscreacion", type="integer", nullable=false)
     */
    private $iduscreacion = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idusmodifica", type="integer", nullable=true)
     */
    private $idusmodifica = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \General.sucursal
     *
     * @ORM\ManyToOne(targetEntity="General.sucursal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idsucursal", referencedColumnName="id")
     * })
     */
    private $idsucursal;

    /**
     * @var \Inventario.tipoBodega
     *
     * @ORM\ManyToOne(targetEntity="Inventario.tipoBodega")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtipobodega", referencedColumnName="id")
     * })
     */
    private $idtipobodega;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Inventario.bodega
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Inventario.bodega
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Inventario.bodega
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param integer $iduscreacion
     *
     * @return Inventario.bodega
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return integer
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param integer $idusmodifica
     *
     * @return Inventario.bodega
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return integer
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Inventario.bodega
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Inventario.bodega
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idsucursal
     *
     * @param \General.sucursal $idsucursal
     *
     * @return Inventario.bodega
     */
    public function setIdsucursal(\General.sucursal $idsucursal = null)
    {
        $this->idsucursal = $idsucursal;

        return $this;
    }

    /**
     * Get idsucursal
     *
     * @return \General.sucursal
     */
    public function getIdsucursal()
    {
        return $this->idsucursal;
    }

    /**
     * Set idtipobodega
     *
     * @param \Inventario.tipoBodega $idtipobodega
     *
     * @return Inventario.bodega
     */
    public function setIdtipobodega(\Inventario.tipoBodega $idtipobodega = null)
    {
        $this->idtipobodega = $idtipobodega;

        return $this;
    }

    /**
     * Get idtipobodega
     *
     * @return \Inventario.tipoBodega
     */
    public function getIdtipobodega()
    {
        return $this->idtipobodega;
    }
}

