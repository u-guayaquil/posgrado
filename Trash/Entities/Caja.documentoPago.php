<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Caja.documentoPago
 *
 * @ORM\Table(name="caja.documento_pago", indexes={@ORM\Index(name="fki_cajadoc_cajadocpago", columns={"idcajadoc"}), @ORM\Index(name="IDX_BC873EDC90473E50", columns={"idcajapag"})})
 * @ORM\Entity
 */
class Caja.documentoPago
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=2, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="caja.documento_pago_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="formatodoc", type="string", length=255, nullable=false)
     */
    private $formatodoc = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ctrlsecudoc", type="integer", nullable=false)
     */
    private $ctrlsecudoc = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="iniciosecdoc", type="integer", nullable=false)
     */
    private $iniciosecdoc = '0';

    /**
     * @var \Caja.pago
     *
     * @ORM\ManyToOne(targetEntity="Caja.pago")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcajapag", referencedColumnName="id")
     * })
     */
    private $idcajapag;

    /**
     * @var \Caja.documento
     *
     * @ORM\ManyToOne(targetEntity="Caja.documento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcajadoc", referencedColumnName="id")
     * })
     */
    private $idcajadoc;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set formatodoc
     *
     * @param string $formatodoc
     *
     * @return Caja.documentoPago
     */
    public function setFormatodoc($formatodoc)
    {
        $this->formatodoc = $formatodoc;

        return $this;
    }

    /**
     * Get formatodoc
     *
     * @return string
     */
    public function getFormatodoc()
    {
        return $this->formatodoc;
    }

    /**
     * Set ctrlsecudoc
     *
     * @param integer $ctrlsecudoc
     *
     * @return Caja.documentoPago
     */
    public function setCtrlsecudoc($ctrlsecudoc)
    {
        $this->ctrlsecudoc = $ctrlsecudoc;

        return $this;
    }

    /**
     * Get ctrlsecudoc
     *
     * @return integer
     */
    public function getCtrlsecudoc()
    {
        return $this->ctrlsecudoc;
    }

    /**
     * Set iniciosecdoc
     *
     * @param integer $iniciosecdoc
     *
     * @return Caja.documentoPago
     */
    public function setIniciosecdoc($iniciosecdoc)
    {
        $this->iniciosecdoc = $iniciosecdoc;

        return $this;
    }

    /**
     * Get iniciosecdoc
     *
     * @return integer
     */
    public function getIniciosecdoc()
    {
        return $this->iniciosecdoc;
    }

    /**
     * Set idcajapag
     *
     * @param \Caja.pago $idcajapag
     *
     * @return Caja.documentoPago
     */
    public function setIdcajapag(\Caja.pago $idcajapag = null)
    {
        $this->idcajapag = $idcajapag;

        return $this;
    }

    /**
     * Get idcajapag
     *
     * @return \Caja.pago
     */
    public function getIdcajapag()
    {
        return $this->idcajapag;
    }

    /**
     * Set idcajadoc
     *
     * @param \Caja.documento $idcajadoc
     *
     * @return Caja.documentoPago
     */
    public function setIdcajadoc(\Caja.documento $idcajadoc = null)
    {
        $this->idcajadoc = $idcajadoc;

        return $this;
    }

    /**
     * Get idcajadoc
     *
     * @return \Caja.documento
     */
    public function getIdcajadoc()
    {
        return $this->idcajadoc;
    }
}

