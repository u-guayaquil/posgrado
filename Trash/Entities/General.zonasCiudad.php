<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * General.zonasCiudad
 *
 * @ORM\Table(name="general.zonas_ciudad", indexes={@ORM\Index(name="fki_idzona_zonasciudad", columns={"idzona"}), @ORM\Index(name="fki_idciudad_zonasciudad", columns={"idciudad"})})
 * @ORM\Entity
 */
class General.zonasCiudad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="general.zonas_ciudad_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \General.ciudad
     *
     * @ORM\ManyToOne(targetEntity="General.ciudad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idciudad", referencedColumnName="id")
     * })
     */
    private $idciudad;

    /**
     * @var \General.zonas
     *
     * @ORM\ManyToOne(targetEntity="General.zonas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idzona", referencedColumnName="id")
     * })
     */
    private $idzona;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idciudad
     *
     * @param \General.ciudad $idciudad
     *
     * @return General.zonasCiudad
     */
    public function setIdciudad(\General.ciudad $idciudad = null)
    {
        $this->idciudad = $idciudad;

        return $this;
    }

    /**
     * Get idciudad
     *
     * @return \General.ciudad
     */
    public function getIdciudad()
    {
        return $this->idciudad;
    }

    /**
     * Set idzona
     *
     * @param \General.zonas $idzona
     *
     * @return General.zonasCiudad
     */
    public function setIdzona(\General.zonas $idzona = null)
    {
        $this->idzona = $idzona;

        return $this;
    }

    /**
     * Get idzona
     *
     * @return \General.zonas
     */
    public function getIdzona()
    {
        return $this->idzona;
    }
}

