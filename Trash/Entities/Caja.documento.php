<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Caja.documento
 *
 * @ORM\Table(name="caja.documento", indexes={@ORM\Index(name="idx_iduscreacion_cajadoc", columns={"iduscreacion"}), @ORM\Index(name="idx_idestado_cajadoc", columns={"idestado"}), @ORM\Index(name="IDX_A0711475E006C7F7", columns={"idcajatipo"}), @ORM\Index(name="IDX_A07114753189B6A", columns={"idcajaval"})})
 * @ORM\Entity
 */
class Caja.documento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="caja.documento_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=20, nullable=false)
     */
    private $descripcion = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Caja.tipo
     *
     * @ORM\ManyToOne(targetEntity="Caja.tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcajatipo", referencedColumnName="id")
     * })
     */
    private $idcajatipo;

    /**
     * @var \Caja.validacion
     *
     * @ORM\ManyToOne(targetEntity="Caja.validacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcajaval", referencedColumnName="id")
     * })
     */
    private $idcajaval;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Caja.documento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Caja.documento
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return Caja.documento
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return Caja.documento
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Caja.documento
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Caja.documento
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idcajatipo
     *
     * @param \Caja.tipo $idcajatipo
     *
     * @return Caja.documento
     */
    public function setIdcajatipo(\Caja.tipo $idcajatipo = null)
    {
        $this->idcajatipo = $idcajatipo;

        return $this;
    }

    /**
     * Get idcajatipo
     *
     * @return \Caja.tipo
     */
    public function getIdcajatipo()
    {
        return $this->idcajatipo;
    }

    /**
     * Set idcajaval
     *
     * @param \Caja.validacion $idcajaval
     *
     * @return Caja.documento
     */
    public function setIdcajaval(\Caja.validacion $idcajaval = null)
    {
        $this->idcajaval = $idcajaval;

        return $this;
    }

    /**
     * Get idcajaval
     *
     * @return \Caja.validacion
     */
    public function getIdcajaval()
    {
        return $this->idcajaval;
    }
}

