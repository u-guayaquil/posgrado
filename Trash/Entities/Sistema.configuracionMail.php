<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Sistema.configuracionMail
 *
 * @ORM\Table(name="sistema.configuracion_mail")
 * @ORM\Entity
 */
class Sistema.configuracionMail
{
    /**
     * @var string
     *
     * @ORM\Column(name="operacion", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="sistema.configuracion_mail_operacion_seq", allocationSize=1, initialValue=1)
     */
    private $operacion;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=50, nullable=true)
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=50, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp", type="string", length=50, nullable=true)
     */
    private $smtp;

    /**
     * @var string
     *
     * @ORM\Column(name="puerto", type="string", length=4, nullable=true)
     */
    private $puerto;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=50, nullable=true)
     */
    private $correo;


    /**
     * Get operacion
     *
     * @return string
     */
    public function getOperacion()
    {
        return $this->operacion;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return Sistema.configuracionMail
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Sistema.configuracionMail
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set smtp
     *
     * @param string $smtp
     *
     * @return Sistema.configuracionMail
     */
    public function setSmtp($smtp)
    {
        $this->smtp = $smtp;

        return $this;
    }

    /**
     * Get smtp
     *
     * @return string
     */
    public function getSmtp()
    {
        return $this->smtp;
    }

    /**
     * Set puerto
     *
     * @param string $puerto
     *
     * @return Sistema.configuracionMail
     */
    public function setPuerto($puerto)
    {
        $this->puerto = $puerto;

        return $this;
    }

    /**
     * Get puerto
     *
     * @return string
     */
    public function getPuerto()
    {
        return $this->puerto;
    }

    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Sistema.configuracionMail
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }
}

