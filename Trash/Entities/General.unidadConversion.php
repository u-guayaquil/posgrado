<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * General.unidadConversion
 *
 * @ORM\Table(name="general.unidad_conversion")
 * @ORM\Entity
 */
class General.unidadConversion
{
    /**
     * @var string
     *
     * @ORM\Column(name="und_origen", type="string", length=2, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $undOrigen;

    /**
     * @var string
     *
     * @ORM\Column(name="und_destino", type="string", length=2, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $undDestino;

    /**
     * @var float
     *
     * @ORM\Column(name="und_factor", type="float", precision=10, scale=0, nullable=false)
     */
    private $undFactor = '0';


    /**
     * Set undOrigen
     *
     * @param string $undOrigen
     *
     * @return General.unidadConversion
     */
    public function setUndOrigen($undOrigen)
    {
        $this->undOrigen = $undOrigen;

        return $this;
    }

    /**
     * Get undOrigen
     *
     * @return string
     */
    public function getUndOrigen()
    {
        return $this->undOrigen;
    }

    /**
     * Set undDestino
     *
     * @param string $undDestino
     *
     * @return General.unidadConversion
     */
    public function setUndDestino($undDestino)
    {
        $this->undDestino = $undDestino;

        return $this;
    }

    /**
     * Get undDestino
     *
     * @return string
     */
    public function getUndDestino()
    {
        return $this->undDestino;
    }

    /**
     * Set undFactor
     *
     * @param float $undFactor
     *
     * @return General.unidadConversion
     */
    public function setUndFactor($undFactor)
    {
        $this->undFactor = $undFactor;

        return $this;
    }

    /**
     * Get undFactor
     *
     * @return float
     */
    public function getUndFactor()
    {
        return $this->undFactor;
    }
}

