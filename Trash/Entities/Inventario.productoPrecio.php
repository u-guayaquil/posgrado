<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Inventario.productoPrecio
 *
 * @ORM\Table(name="inventario.producto_precio")
 * @ORM\Entity
 */
class Inventario.productoPrecio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="inventario.producto_precio_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idproducto", type="string", length=10, nullable=false)
     */
    private $idproducto;

    /**
     * @var string
     *
     * @ORM\Column(name="idtipoprecio", type="string", length=2, nullable=false)
     */
    private $idtipoprecio;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idproducto
     *
     * @param string $idproducto
     *
     * @return Inventario.productoPrecio
     */
    public function setIdproducto($idproducto)
    {
        $this->idproducto = $idproducto;

        return $this;
    }

    /**
     * Get idproducto
     *
     * @return string
     */
    public function getIdproducto()
    {
        return $this->idproducto;
    }

    /**
     * Set idtipoprecio
     *
     * @param string $idtipoprecio
     *
     * @return Inventario.productoPrecio
     */
    public function setIdtipoprecio($idtipoprecio)
    {
        $this->idtipoprecio = $idtipoprecio;

        return $this;
    }

    /**
     * Get idtipoprecio
     *
     * @return string
     */
    public function getIdtipoprecio()
    {
        return $this->idtipoprecio;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Inventario.productoPrecio
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Inventario.productoPrecio
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }
}

