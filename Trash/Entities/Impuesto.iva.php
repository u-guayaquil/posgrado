<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Impuesto.iva
 *
 * @ORM\Table(name="impuesto.iva", indexes={@ORM\Index(name="idx_idestado_impuestoiva", columns={"idestado"}), @ORM\Index(name="fki_idporcentaje_impuestoiva", columns={"idporcentaje"}), @ORM\Index(name="idx_iduscreacion_impuestoiva", columns={"iduscreacion"}), @ORM\Index(name="idx_idctacontable_impuestoiva", columns={"idctacontable"}), @ORM\Index(name="IDX_937224F51C3372AC", columns={"idimpuesto"})})
 * @ORM\Entity
 */
class Impuesto.iva
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="impuesto.iva_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idctacontable", type="integer", nullable=true)
     */
    private $idctacontable = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="iduscreacion", type="integer", nullable=false)
     */
    private $iduscreacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idusmodifica", type="integer", nullable=true)
     */
    private $idusmodifica = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Impuesto.tipoImpuesto
     *
     * @ORM\ManyToOne(targetEntity="Impuesto.tipoImpuesto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idimpuesto", referencedColumnName="id")
     * })
     */
    private $idimpuesto;

    /**
     * @var \Impuesto.ivaPorcentaje
     *
     * @ORM\ManyToOne(targetEntity="Impuesto.ivaPorcentaje")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idporcentaje", referencedColumnName="id")
     * })
     */
    private $idporcentaje;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idctacontable
     *
     * @param integer $idctacontable
     *
     * @return Impuesto.iva
     */
    public function setIdctacontable($idctacontable)
    {
        $this->idctacontable = $idctacontable;

        return $this;
    }

    /**
     * Get idctacontable
     *
     * @return integer
     */
    public function getIdctacontable()
    {
        return $this->idctacontable;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Impuesto.iva
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param integer $iduscreacion
     *
     * @return Impuesto.iva
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return integer
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param integer $idusmodifica
     *
     * @return Impuesto.iva
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return integer
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Impuesto.iva
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Impuesto.iva
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idimpuesto
     *
     * @param \Impuesto.tipoImpuesto $idimpuesto
     *
     * @return Impuesto.iva
     */
    public function setIdimpuesto(\Impuesto.tipoImpuesto $idimpuesto = null)
    {
        $this->idimpuesto = $idimpuesto;

        return $this;
    }

    /**
     * Get idimpuesto
     *
     * @return \Impuesto.tipoImpuesto
     */
    public function getIdimpuesto()
    {
        return $this->idimpuesto;
    }

    /**
     * Set idporcentaje
     *
     * @param \Impuesto.ivaPorcentaje $idporcentaje
     *
     * @return Impuesto.iva
     */
    public function setIdporcentaje(\Impuesto.ivaPorcentaje $idporcentaje = null)
    {
        $this->idporcentaje = $idporcentaje;

        return $this;
    }

    /**
     * Get idporcentaje
     *
     * @return \Impuesto.ivaPorcentaje
     */
    public function getIdporcentaje()
    {
        return $this->idporcentaje;
    }
}

