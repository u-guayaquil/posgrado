<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Contable.plantillaAsientoDt
 *
 * @ORM\Table(name="contable.plantilla_asiento_dt", indexes={@ORM\Index(name="fki_idplantilla_plantilla_dt", columns={"idplantilla"})})
 * @ORM\Entity
 */
class Contable.plantillaAsientoDt
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=4, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contable.plantilla_asiento_dt_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idcuenta", type="string", length=5, nullable=false)
     */
    private $idcuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="idmovnto", type="string", length=1, nullable=false)
     */
    private $idmovnto = 'D';

    /**
     * @var \Contable.plantillaAsiento
     *
     * @ORM\ManyToOne(targetEntity="Contable.plantillaAsiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idplantilla", referencedColumnName="id")
     * })
     */
    private $idplantilla;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idcuenta
     *
     * @param string $idcuenta
     *
     * @return Contable.plantillaAsientoDt
     */
    public function setIdcuenta($idcuenta)
    {
        $this->idcuenta = $idcuenta;

        return $this;
    }

    /**
     * Get idcuenta
     *
     * @return string
     */
    public function getIdcuenta()
    {
        return $this->idcuenta;
    }

    /**
     * Set idmovnto
     *
     * @param string $idmovnto
     *
     * @return Contable.plantillaAsientoDt
     */
    public function setIdmovnto($idmovnto)
    {
        $this->idmovnto = $idmovnto;

        return $this;
    }

    /**
     * Get idmovnto
     *
     * @return string
     */
    public function getIdmovnto()
    {
        return $this->idmovnto;
    }

    /**
     * Set idplantilla
     *
     * @param \Contable.plantillaAsiento $idplantilla
     *
     * @return Contable.plantillaAsientoDt
     */
    public function setIdplantilla(\Contable.plantillaAsiento $idplantilla = null)
    {
        $this->idplantilla = $idplantilla;

        return $this;
    }

    /**
     * Get idplantilla
     *
     * @return \Contable.plantillaAsiento
     */
    public function getIdplantilla()
    {
        return $this->idplantilla;
    }
}

