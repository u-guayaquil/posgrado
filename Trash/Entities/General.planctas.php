<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * General.planctas
 *
 * @ORM\Table(name="general.planctas", indexes={@ORM\Index(name="idx_iduscreacion_planctas", columns={"iduscreacion"}), @ORM\Index(name="idx_idestado_planctas", columns={"idestado"}), @ORM\Index(name="idx_codigo_planctas", columns={"codigo"}), @ORM\Index(name="fki_clase_planctas", columns={"clase"})})
 * @ORM\Entity
 */
class General.planctas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="general.planctas_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=false)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=1, nullable=true)
     */
    private $tipo = 'G';

    /**
     * @var integer
     *
     * @ORM\Column(name="idpadre", type="integer", nullable=true)
     */
    private $idpadre;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado;

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=true)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \General.clasectas
     *
     * @ORM\ManyToOne(targetEntity="General.clasectas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clase", referencedColumnName="id")
     * })
     */
    private $clase;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return General.planctas
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return General.planctas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return General.planctas
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set idpadre
     *
     * @param integer $idpadre
     *
     * @return General.planctas
     */
    public function setIdpadre($idpadre)
    {
        $this->idpadre = $idpadre;

        return $this;
    }

    /**
     * Get idpadre
     *
     * @return integer
     */
    public function getIdpadre()
    {
        return $this->idpadre;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return General.planctas
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return General.planctas
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return General.planctas
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return General.planctas
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return General.planctas
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set clase
     *
     * @param \General.clasectas $clase
     *
     * @return General.planctas
     */
    public function setClase(\General.clasectas $clase = null)
    {
        $this->clase = $clase;

        return $this;
    }

    /**
     * Get clase
     *
     * @return \General.clasectas
     */
    public function getClase()
    {
        return $this->clase;
    }
}

