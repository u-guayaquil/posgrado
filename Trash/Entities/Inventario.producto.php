<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Inventario.producto
 *
 * @ORM\Table(name="inventario.producto")
 * @ORM\Entity
 */
class Inventario.producto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="inventario.producto_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=15, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="iditem", type="string", length=2, nullable=false)
     */
    private $iditem;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=60, nullable=false)
     */
    private $descripcion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ctamercaderia", type="string", length=5, nullable=false)
     */
    private $ctamercaderia = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ctaventas", type="string", length=5, nullable=false)
     */
    private $ctaventas;

    /**
     * @var string
     *
     * @ORM\Column(name="ctacostos", type="string", length=5, nullable=false)
     */
    private $ctacostos = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ctadevolucion", type="string", length=5, nullable=false)
     */
    private $ctadevolucion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="idunidad", type="string", length=2, nullable=false)
     */
    private $idunidad;

    /**
     * @var string
     *
     * @ORM\Column(name="ivacompras", type="string", length=2, nullable=false)
     */
    private $ivacompras = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ivaventas", type="string", length=2, nullable=false)
     */
    private $ivaventas = '';

    /**
     * @var string
     *
     * @ORM\Column(name="iceventas", type="string", length=2, nullable=false)
     */
    private $iceventas = '';

    /**
     * @var string
     *
     * @ORM\Column(name="serventas", type="string", length=2, nullable=false)
     */
    private $serventas = '';

    /**
     * @var float
     *
     * @ORM\Column(name="costo", type="float", precision=10, scale=0, nullable=false)
     */
    private $costo = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="costoultcompra", type="float", precision=10, scale=0, nullable=false)
     */
    private $costoultcompra = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="stockmin", type="float", precision=10, scale=0, nullable=false)
     */
    private $stockmin = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ultcompra", type="datetime", nullable=true)
     */
    private $ultcompra = '1900-01-01 00:00:00';

    /**
     * @var float
     *
     * @ORM\Column(name="dsctomax", type="float", precision=10, scale=0, nullable=false)
     */
    private $dsctomax = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="idtipoprecio", type="string", length=2, nullable=false)
     */
    private $idtipoprecio;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="decimales", type="integer", nullable=false)
     */
    private $decimales = '2';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Inventario.producto
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set iditem
     *
     * @param string $iditem
     *
     * @return Inventario.producto
     */
    public function setIditem($iditem)
    {
        $this->iditem = $iditem;

        return $this;
    }

    /**
     * Get iditem
     *
     * @return string
     */
    public function getIditem()
    {
        return $this->iditem;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Inventario.producto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set ctamercaderia
     *
     * @param string $ctamercaderia
     *
     * @return Inventario.producto
     */
    public function setCtamercaderia($ctamercaderia)
    {
        $this->ctamercaderia = $ctamercaderia;

        return $this;
    }

    /**
     * Get ctamercaderia
     *
     * @return string
     */
    public function getCtamercaderia()
    {
        return $this->ctamercaderia;
    }

    /**
     * Set ctaventas
     *
     * @param string $ctaventas
     *
     * @return Inventario.producto
     */
    public function setCtaventas($ctaventas)
    {
        $this->ctaventas = $ctaventas;

        return $this;
    }

    /**
     * Get ctaventas
     *
     * @return string
     */
    public function getCtaventas()
    {
        return $this->ctaventas;
    }

    /**
     * Set ctacostos
     *
     * @param string $ctacostos
     *
     * @return Inventario.producto
     */
    public function setCtacostos($ctacostos)
    {
        $this->ctacostos = $ctacostos;

        return $this;
    }

    /**
     * Get ctacostos
     *
     * @return string
     */
    public function getCtacostos()
    {
        return $this->ctacostos;
    }

    /**
     * Set ctadevolucion
     *
     * @param string $ctadevolucion
     *
     * @return Inventario.producto
     */
    public function setCtadevolucion($ctadevolucion)
    {
        $this->ctadevolucion = $ctadevolucion;

        return $this;
    }

    /**
     * Get ctadevolucion
     *
     * @return string
     */
    public function getCtadevolucion()
    {
        return $this->ctadevolucion;
    }

    /**
     * Set idunidad
     *
     * @param string $idunidad
     *
     * @return Inventario.producto
     */
    public function setIdunidad($idunidad)
    {
        $this->idunidad = $idunidad;

        return $this;
    }

    /**
     * Get idunidad
     *
     * @return string
     */
    public function getIdunidad()
    {
        return $this->idunidad;
    }

    /**
     * Set ivacompras
     *
     * @param string $ivacompras
     *
     * @return Inventario.producto
     */
    public function setIvacompras($ivacompras)
    {
        $this->ivacompras = $ivacompras;

        return $this;
    }

    /**
     * Get ivacompras
     *
     * @return string
     */
    public function getIvacompras()
    {
        return $this->ivacompras;
    }

    /**
     * Set ivaventas
     *
     * @param string $ivaventas
     *
     * @return Inventario.producto
     */
    public function setIvaventas($ivaventas)
    {
        $this->ivaventas = $ivaventas;

        return $this;
    }

    /**
     * Get ivaventas
     *
     * @return string
     */
    public function getIvaventas()
    {
        return $this->ivaventas;
    }

    /**
     * Set iceventas
     *
     * @param string $iceventas
     *
     * @return Inventario.producto
     */
    public function setIceventas($iceventas)
    {
        $this->iceventas = $iceventas;

        return $this;
    }

    /**
     * Get iceventas
     *
     * @return string
     */
    public function getIceventas()
    {
        return $this->iceventas;
    }

    /**
     * Set serventas
     *
     * @param string $serventas
     *
     * @return Inventario.producto
     */
    public function setServentas($serventas)
    {
        $this->serventas = $serventas;

        return $this;
    }

    /**
     * Get serventas
     *
     * @return string
     */
    public function getServentas()
    {
        return $this->serventas;
    }

    /**
     * Set costo
     *
     * @param float $costo
     *
     * @return Inventario.producto
     */
    public function setCosto($costo)
    {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return float
     */
    public function getCosto()
    {
        return $this->costo;
    }

    /**
     * Set costoultcompra
     *
     * @param float $costoultcompra
     *
     * @return Inventario.producto
     */
    public function setCostoultcompra($costoultcompra)
    {
        $this->costoultcompra = $costoultcompra;

        return $this;
    }

    /**
     * Get costoultcompra
     *
     * @return float
     */
    public function getCostoultcompra()
    {
        return $this->costoultcompra;
    }

    /**
     * Set stockmin
     *
     * @param float $stockmin
     *
     * @return Inventario.producto
     */
    public function setStockmin($stockmin)
    {
        $this->stockmin = $stockmin;

        return $this;
    }

    /**
     * Get stockmin
     *
     * @return float
     */
    public function getStockmin()
    {
        return $this->stockmin;
    }

    /**
     * Set ultcompra
     *
     * @param \DateTime $ultcompra
     *
     * @return Inventario.producto
     */
    public function setUltcompra($ultcompra)
    {
        $this->ultcompra = $ultcompra;

        return $this;
    }

    /**
     * Get ultcompra
     *
     * @return \DateTime
     */
    public function getUltcompra()
    {
        return $this->ultcompra;
    }

    /**
     * Set dsctomax
     *
     * @param float $dsctomax
     *
     * @return Inventario.producto
     */
    public function setDsctomax($dsctomax)
    {
        $this->dsctomax = $dsctomax;

        return $this;
    }

    /**
     * Get dsctomax
     *
     * @return float
     */
    public function getDsctomax()
    {
        return $this->dsctomax;
    }

    /**
     * Set idtipoprecio
     *
     * @param string $idtipoprecio
     *
     * @return Inventario.producto
     */
    public function setIdtipoprecio($idtipoprecio)
    {
        $this->idtipoprecio = $idtipoprecio;

        return $this;
    }

    /**
     * Get idtipoprecio
     *
     * @return string
     */
    public function getIdtipoprecio()
    {
        return $this->idtipoprecio;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Inventario.producto
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return Inventario.producto
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return Inventario.producto
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Inventario.producto
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Inventario.producto
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set decimales
     *
     * @param integer $decimales
     *
     * @return Inventario.producto
     */
    public function setDecimales($decimales)
    {
        $this->decimales = $decimales;

        return $this;
    }

    /**
     * Get decimales
     *
     * @return integer
     */
    public function getDecimales()
    {
        return $this->decimales;
    }
}

