<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Importacion.pedidoDt
 *
 * @ORM\Table(name="importacion.pedido_dt", indexes={@ORM\Index(name="IDX_5E5E3B08E2DBA323", columns={"id_pedido"}), @ORM\Index(name="IDX_5E5E3B08F760EA80", columns={"id_producto"})})
 * @ORM\Entity
 */
class Importacion.pedidoDt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="importacion.pedido_dt_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=15, nullable=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=50, nullable=true)
     */
    private $descripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad_pendiente", type="integer", nullable=true)
     */
    private $cantidadPendiente;

    /**
     * @var integer
     *
     * @ORM\Column(name="cajas", type="integer", nullable=true)
     */
    private $cajas;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=true)
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $total;

    /**
     * @var \Importacion.pedido
     *
     * @ORM\ManyToOne(targetEntity="Importacion.pedido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pedido", referencedColumnName="id")
     * })
     */
    private $idPedido;

    /**
     * @var \Inventario.producto
     *
     * @ORM\ManyToOne(targetEntity="Inventario.producto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_producto", referencedColumnName="id")
     * })
     */
    private $idProducto;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Importacion.pedidoDt
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Importacion.pedidoDt
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cantidadPendiente
     *
     * @param integer $cantidadPendiente
     *
     * @return Importacion.pedidoDt
     */
    public function setCantidadPendiente($cantidadPendiente)
    {
        $this->cantidadPendiente = $cantidadPendiente;

        return $this;
    }

    /**
     * Get cantidadPendiente
     *
     * @return integer
     */
    public function getCantidadPendiente()
    {
        return $this->cantidadPendiente;
    }

    /**
     * Set cajas
     *
     * @param integer $cajas
     *
     * @return Importacion.pedidoDt
     */
    public function setCajas($cajas)
    {
        $this->cajas = $cajas;

        return $this;
    }

    /**
     * Get cajas
     *
     * @return integer
     */
    public function getCajas()
    {
        return $this->cajas;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Importacion.pedidoDt
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param string $precio
     *
     * @return Importacion.pedidoDt
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return Importacion.pedidoDt
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set idPedido
     *
     * @param \Importacion.pedido $idPedido
     *
     * @return Importacion.pedidoDt
     */
    public function setIdPedido(\Importacion.pedido $idPedido = null)
    {
        $this->idPedido = $idPedido;

        return $this;
    }

    /**
     * Get idPedido
     *
     * @return \Importacion.pedido
     */
    public function getIdPedido()
    {
        return $this->idPedido;
    }

    /**
     * Set idProducto
     *
     * @param \Inventario.producto $idProducto
     *
     * @return Importacion.pedidoDt
     */
    public function setIdProducto(\Inventario.producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \Inventario.producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }
}

