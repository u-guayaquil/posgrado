<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Caja.subtipo
 *
 * @ORM\Table(name="caja.subtipo", indexes={@ORM\Index(name="fki_idtipo_subtipo", columns={"idtipo"})})
 * @ORM\Entity
 */
class Caja.subtipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="caja.subtipo_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=20, nullable=false)
     */
    private $descripcion;

    /**
     * @var \Caja.tipo
     *
     * @ORM\ManyToOne(targetEntity="Caja.tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtipo", referencedColumnName="id")
     * })
     */
    private $idtipo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Caja.subtipo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idtipo
     *
     * @param \Caja.tipo $idtipo
     *
     * @return Caja.subtipo
     */
    public function setIdtipo(\Caja.tipo $idtipo = null)
    {
        $this->idtipo = $idtipo;

        return $this;
    }

    /**
     * Get idtipo
     *
     * @return \Caja.tipo
     */
    public function getIdtipo()
    {
        return $this->idtipo;
    }
}

