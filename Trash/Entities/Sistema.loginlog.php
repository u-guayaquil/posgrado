<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Sistema.loginlog
 *
 * @ORM\Table(name="sistema.loginlog")
 * @ORM\Entity
 */
class Sistema.loginlog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="sistema.loginlog_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cuenta", type="string", length=80, nullable=true)
     */
    private $cuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="ipacceso", type="string", length=15, nullable=true)
     */
    private $ipacceso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="feacceso", type="datetime", nullable=true)
     */
    private $feacceso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fesalida", type="datetime", nullable=true)
     */
    private $fesalida = '1900-01-01 00:00:00';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cuenta
     *
     * @param string $cuenta
     *
     * @return Sistema.loginlog
     */
    public function setCuenta($cuenta)
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    /**
     * Get cuenta
     *
     * @return string
     */
    public function getCuenta()
    {
        return $this->cuenta;
    }

    /**
     * Set ipacceso
     *
     * @param string $ipacceso
     *
     * @return Sistema.loginlog
     */
    public function setIpacceso($ipacceso)
    {
        $this->ipacceso = $ipacceso;

        return $this;
    }

    /**
     * Get ipacceso
     *
     * @return string
     */
    public function getIpacceso()
    {
        return $this->ipacceso;
    }

    /**
     * Set feacceso
     *
     * @param \DateTime $feacceso
     *
     * @return Sistema.loginlog
     */
    public function setFeacceso($feacceso)
    {
        $this->feacceso = $feacceso;

        return $this;
    }

    /**
     * Get feacceso
     *
     * @return \DateTime
     */
    public function getFeacceso()
    {
        return $this->feacceso;
    }

    /**
     * Set fesalida
     *
     * @param \DateTime $fesalida
     *
     * @return Sistema.loginlog
     */
    public function setFesalida($fesalida)
    {
        $this->fesalida = $fesalida;

        return $this;
    }

    /**
     * Get fesalida
     *
     * @return \DateTime
     */
    public function getFesalida()
    {
        return $this->fesalida;
    }
}

