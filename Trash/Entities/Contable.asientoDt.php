<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Contable.asientoDt
 *
 * @ORM\Table(name="contable.asiento_dt", indexes={@ORM\Index(name="fki_idcuenta_asiento_dt", columns={"idcuenta"}), @ORM\Index(name="fki_idasiento_asiento_dt", columns={"idasiento"}), @ORM\Index(name="idx_iduscreacion_asiento_dt", columns={"iduscreacion"}), @ORM\Index(name="idx_idestado_asiento_dt", columns={"idestado"})})
 * @ORM\Entity
 */
class Contable.asientoDt
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contable.asiento_dt_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idcuenta", type="integer", nullable=false)
     */
    private $idcuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="idmovnto", type="string", length=1, nullable=false)
     */
    private $idmovnto = 'D';

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="glosa", type="string", length=100, nullable=false)
     */
    private $glosa = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Contable.asiento
     *
     * @ORM\ManyToOne(targetEntity="Contable.asiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idasiento", referencedColumnName="id")
     * })
     */
    private $idasiento;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idcuenta
     *
     * @param integer $idcuenta
     *
     * @return Contable.asientoDt
     */
    public function setIdcuenta($idcuenta)
    {
        $this->idcuenta = $idcuenta;

        return $this;
    }

    /**
     * Get idcuenta
     *
     * @return integer
     */
    public function getIdcuenta()
    {
        return $this->idcuenta;
    }

    /**
     * Set idmovnto
     *
     * @param string $idmovnto
     *
     * @return Contable.asientoDt
     */
    public function setIdmovnto($idmovnto)
    {
        $this->idmovnto = $idmovnto;

        return $this;
    }

    /**
     * Get idmovnto
     *
     * @return string
     */
    public function getIdmovnto()
    {
        return $this->idmovnto;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Contable.asientoDt
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set glosa
     *
     * @param string $glosa
     *
     * @return Contable.asientoDt
     */
    public function setGlosa($glosa)
    {
        $this->glosa = $glosa;

        return $this;
    }

    /**
     * Get glosa
     *
     * @return string
     */
    public function getGlosa()
    {
        return $this->glosa;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Contable.asientoDt
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return Contable.asientoDt
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return Contable.asientoDt
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Contable.asientoDt
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Contable.asientoDt
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idasiento
     *
     * @param \Contable.asiento $idasiento
     *
     * @return Contable.asientoDt
     */
    public function setIdasiento(\Contable.asiento $idasiento = null)
    {
        $this->idasiento = $idasiento;

        return $this;
    }

    /**
     * Get idasiento
     *
     * @return \Contable.asiento
     */
    public function getIdasiento()
    {
        return $this->idasiento;
    }
}

