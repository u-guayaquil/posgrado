<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * General.sucursalEmision
 *
 * @ORM\Table(name="general.sucursal_emision", indexes={@ORM\Index(name="idx_iduscreacion_sucuremision", columns={"iduscreacion"}), @ORM\Index(name="idx_idestado_sucuremision", columns={"idestado"}), @ORM\Index(name="fki_idtipocomp_sucuremision", columns={"idtipocomp"}), @ORM\Index(name="IDX_209FB6ADCBBD1341", columns={"idemidocs"})})
 * @ORM\Entity
 */
class General.sucursalEmision
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=4, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="general.sucursal_emision_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idsucursal", type="string", length=2, nullable=false)
     */
    private $idsucursal;

    /**
     * @var string
     *
     * @ORM\Column(name="serie1", type="string", length=3, nullable=false)
     */
    private $serie1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="serie2", type="string", length=3, nullable=false)
     */
    private $serie2 = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="sec_inicio", type="integer", nullable=false)
     */
    private $secInicio = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="sec_fin", type="integer", nullable=false)
     */
    private $secFin = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="autorizacion", type="string", length=20, nullable=false)
     */
    private $autorizacion = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fevigencia", type="date", nullable=false)
     */
    private $fevigencia = '1900-01-01';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecaducidad", type="date", nullable=false)
     */
    private $fecaducidad = '1900-01-01';

    /**
     * @var integer
     *
     * @ORM\Column(name="idambiente", type="integer", nullable=false)
     */
    private $idambiente = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="idejecucion", type="integer", nullable=false)
     */
    private $idejecucion = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Impuesto.tipoEmision
     *
     * @ORM\ManyToOne(targetEntity="Impuesto.tipoEmision")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idemidocs", referencedColumnName="id")
     * })
     */
    private $idemidocs;

    /**
     * @var \Impuesto.tipoComprobante
     *
     * @ORM\ManyToOne(targetEntity="Impuesto.tipoComprobante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtipocomp", referencedColumnName="id")
     * })
     */
    private $idtipocomp;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idsucursal
     *
     * @param string $idsucursal
     *
     * @return General.sucursalEmision
     */
    public function setIdsucursal($idsucursal)
    {
        $this->idsucursal = $idsucursal;

        return $this;
    }

    /**
     * Get idsucursal
     *
     * @return string
     */
    public function getIdsucursal()
    {
        return $this->idsucursal;
    }

    /**
     * Set serie1
     *
     * @param string $serie1
     *
     * @return General.sucursalEmision
     */
    public function setSerie1($serie1)
    {
        $this->serie1 = $serie1;

        return $this;
    }

    /**
     * Get serie1
     *
     * @return string
     */
    public function getSerie1()
    {
        return $this->serie1;
    }

    /**
     * Set serie2
     *
     * @param string $serie2
     *
     * @return General.sucursalEmision
     */
    public function setSerie2($serie2)
    {
        $this->serie2 = $serie2;

        return $this;
    }

    /**
     * Get serie2
     *
     * @return string
     */
    public function getSerie2()
    {
        return $this->serie2;
    }

    /**
     * Set secInicio
     *
     * @param integer $secInicio
     *
     * @return General.sucursalEmision
     */
    public function setSecInicio($secInicio)
    {
        $this->secInicio = $secInicio;

        return $this;
    }

    /**
     * Get secInicio
     *
     * @return integer
     */
    public function getSecInicio()
    {
        return $this->secInicio;
    }

    /**
     * Set secFin
     *
     * @param integer $secFin
     *
     * @return General.sucursalEmision
     */
    public function setSecFin($secFin)
    {
        $this->secFin = $secFin;

        return $this;
    }

    /**
     * Get secFin
     *
     * @return integer
     */
    public function getSecFin()
    {
        return $this->secFin;
    }

    /**
     * Set autorizacion
     *
     * @param string $autorizacion
     *
     * @return General.sucursalEmision
     */
    public function setAutorizacion($autorizacion)
    {
        $this->autorizacion = $autorizacion;

        return $this;
    }

    /**
     * Get autorizacion
     *
     * @return string
     */
    public function getAutorizacion()
    {
        return $this->autorizacion;
    }

    /**
     * Set fevigencia
     *
     * @param \DateTime $fevigencia
     *
     * @return General.sucursalEmision
     */
    public function setFevigencia($fevigencia)
    {
        $this->fevigencia = $fevigencia;

        return $this;
    }

    /**
     * Get fevigencia
     *
     * @return \DateTime
     */
    public function getFevigencia()
    {
        return $this->fevigencia;
    }

    /**
     * Set fecaducidad
     *
     * @param \DateTime $fecaducidad
     *
     * @return General.sucursalEmision
     */
    public function setFecaducidad($fecaducidad)
    {
        $this->fecaducidad = $fecaducidad;

        return $this;
    }

    /**
     * Get fecaducidad
     *
     * @return \DateTime
     */
    public function getFecaducidad()
    {
        return $this->fecaducidad;
    }

    /**
     * Set idambiente
     *
     * @param integer $idambiente
     *
     * @return General.sucursalEmision
     */
    public function setIdambiente($idambiente)
    {
        $this->idambiente = $idambiente;

        return $this;
    }

    /**
     * Get idambiente
     *
     * @return integer
     */
    public function getIdambiente()
    {
        return $this->idambiente;
    }

    /**
     * Set idejecucion
     *
     * @param integer $idejecucion
     *
     * @return General.sucursalEmision
     */
    public function setIdejecucion($idejecucion)
    {
        $this->idejecucion = $idejecucion;

        return $this;
    }

    /**
     * Get idejecucion
     *
     * @return integer
     */
    public function getIdejecucion()
    {
        return $this->idejecucion;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return General.sucursalEmision
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return General.sucursalEmision
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return General.sucursalEmision
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return General.sucursalEmision
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return General.sucursalEmision
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idemidocs
     *
     * @param \Impuesto.tipoEmision $idemidocs
     *
     * @return General.sucursalEmision
     */
    public function setIdemidocs(\Impuesto.tipoEmision $idemidocs = null)
    {
        $this->idemidocs = $idemidocs;

        return $this;
    }

    /**
     * Get idemidocs
     *
     * @return \Impuesto.tipoEmision
     */
    public function getIdemidocs()
    {
        return $this->idemidocs;
    }

    /**
     * Set idtipocomp
     *
     * @param \Impuesto.tipoComprobante $idtipocomp
     *
     * @return General.sucursalEmision
     */
    public function setIdtipocomp(\Impuesto.tipoComprobante $idtipocomp = null)
    {
        $this->idtipocomp = $idtipocomp;

        return $this;
    }

    /**
     * Get idtipocomp
     *
     * @return \Impuesto.tipoComprobante
     */
    public function getIdtipocomp()
    {
        return $this->idtipocomp;
    }
}

