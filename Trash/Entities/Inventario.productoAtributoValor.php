<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Inventario.productoAtributoValor
 *
 * @ORM\Table(name="inventario.producto_atributo_valor")
 * @ORM\Entity
 */
class Inventario.productoAtributoValor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="inventario.producto_atributo_valor_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idproducto", type="string", length=10, nullable=false)
     */
    private $idproducto;

    /**
     * @var string
     *
     * @ORM\Column(name="idatributo_valor", type="string", length=4, nullable=false)
     */
    private $idatributoValor;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idproducto
     *
     * @param string $idproducto
     *
     * @return Inventario.productoAtributoValor
     */
    public function setIdproducto($idproducto)
    {
        $this->idproducto = $idproducto;

        return $this;
    }

    /**
     * Get idproducto
     *
     * @return string
     */
    public function getIdproducto()
    {
        return $this->idproducto;
    }

    /**
     * Set idatributoValor
     *
     * @param string $idatributoValor
     *
     * @return Inventario.productoAtributoValor
     */
    public function setIdatributoValor($idatributoValor)
    {
        $this->idatributoValor = $idatributoValor;

        return $this;
    }

    /**
     * Get idatributoValor
     *
     * @return string
     */
    public function getIdatributoValor()
    {
        return $this->idatributoValor;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Inventario.productoAtributoValor
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }
}

