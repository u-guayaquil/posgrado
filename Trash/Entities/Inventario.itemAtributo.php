<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Inventario.itemAtributo
 *
 * @ORM\Table(name="inventario.item_atributo", indexes={@ORM\Index(name="idx_iduscreacion_itematrib", columns={"iduscreacion"}), @ORM\Index(name="idx_idestado_itematrib", columns={"idestado"}), @ORM\Index(name="fki_iditem_itematrib", columns={"iditem"}), @ORM\Index(name="fki_idatributo_itematrib", columns={"idatributo"})})
 * @ORM\Entity
 */
class Inventario.itemAtributo
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=6, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="inventario.item_atributo_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=true)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Inventario.atributo
     *
     * @ORM\ManyToOne(targetEntity="Inventario.atributo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idatributo", referencedColumnName="id")
     * })
     */
    private $idatributo;

    /**
     * @var \Inventario.item
     *
     * @ORM\ManyToOne(targetEntity="Inventario.item")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iditem", referencedColumnName="id")
     * })
     */
    private $iditem;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Inventario.itemAtributo
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return Inventario.itemAtributo
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return Inventario.itemAtributo
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Inventario.itemAtributo
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Inventario.itemAtributo
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idatributo
     *
     * @param \Inventario.atributo $idatributo
     *
     * @return Inventario.itemAtributo
     */
    public function setIdatributo(\Inventario.atributo $idatributo = null)
    {
        $this->idatributo = $idatributo;

        return $this;
    }

    /**
     * Get idatributo
     *
     * @return \Inventario.atributo
     */
    public function getIdatributo()
    {
        return $this->idatributo;
    }

    /**
     * Set iditem
     *
     * @param \Inventario.item $iditem
     *
     * @return Inventario.itemAtributo
     */
    public function setIditem(\Inventario.item $iditem = null)
    {
        $this->iditem = $iditem;

        return $this;
    }

    /**
     * Get iditem
     *
     * @return \Inventario.item
     */
    public function getIditem()
    {
        return $this->iditem;
    }
}

