<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Contable.asiento
 *
 * @ORM\Table(name="contable.asiento", indexes={@ORM\Index(name="idx_iduscreacion_asiento", columns={"iduscreacion"}), @ORM\Index(name="idx_idestado_asiento", columns={"idestado"}), @ORM\Index(name="fki_idtipoasto_asiento", columns={"idtipo_asiento"}), @ORM\Index(name="fki_idsucursal_asiento", columns={"idsucursal"})})
 * @ORM\Entity
 */
class Contable.asiento
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contable.asiento_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nutipo_asiento", type="string", length=6, nullable=false)
     */
    private $nutipoAsiento;

    /**
     * @var string
     *
     * @ORM\Column(name="idsucursal", type="string", length=2, nullable=false)
     */
    private $idsucursal;

    /**
     * @var string
     *
     * @ORM\Column(name="glosa", type="string", length=512, nullable=false)
     */
    private $glosa = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Contable.tipoAsiento
     *
     * @ORM\ManyToOne(targetEntity="Contable.tipoAsiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtipo_asiento", referencedColumnName="id")
     * })
     */
    private $idtipoAsiento;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nutipoAsiento
     *
     * @param string $nutipoAsiento
     *
     * @return Contable.asiento
     */
    public function setNutipoAsiento($nutipoAsiento)
    {
        $this->nutipoAsiento = $nutipoAsiento;

        return $this;
    }

    /**
     * Get nutipoAsiento
     *
     * @return string
     */
    public function getNutipoAsiento()
    {
        return $this->nutipoAsiento;
    }

    /**
     * Set idsucursal
     *
     * @param string $idsucursal
     *
     * @return Contable.asiento
     */
    public function setIdsucursal($idsucursal)
    {
        $this->idsucursal = $idsucursal;

        return $this;
    }

    /**
     * Get idsucursal
     *
     * @return string
     */
    public function getIdsucursal()
    {
        return $this->idsucursal;
    }

    /**
     * Set glosa
     *
     * @param string $glosa
     *
     * @return Contable.asiento
     */
    public function setGlosa($glosa)
    {
        $this->glosa = $glosa;

        return $this;
    }

    /**
     * Get glosa
     *
     * @return string
     */
    public function getGlosa()
    {
        return $this->glosa;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Contable.asiento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Contable.asiento
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return Contable.asiento
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return Contable.asiento
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Contable.asiento
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Contable.asiento
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idtipoAsiento
     *
     * @param \Contable.tipoAsiento $idtipoAsiento
     *
     * @return Contable.asiento
     */
    public function setIdtipoAsiento(\Contable.tipoAsiento $idtipoAsiento = null)
    {
        $this->idtipoAsiento = $idtipoAsiento;

        return $this;
    }

    /**
     * Get idtipoAsiento
     *
     * @return \Contable.tipoAsiento
     */
    public function getIdtipoAsiento()
    {
        return $this->idtipoAsiento;
    }
}

