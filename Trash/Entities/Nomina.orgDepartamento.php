<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Nomina.orgDepartamento
 *
 * @ORM\Table(name="nomina.org_departamento", indexes={@ORM\Index(name="idx_deptpadre_orgdept", columns={"id_departamento_padre"}), @ORM\Index(name="fki_idorg_orgdept", columns={"id_organigrama"}), @ORM\Index(name="fki_iddepart_orgdept", columns={"id_departamento"})})
 * @ORM\Entity
 */
class Nomina.orgDepartamento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="nomina.org_departamento_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_departamento_padre", type="integer", nullable=true)
     */
    private $idDepartamentoPadre;

    /**
     * @var \Nomina.departamento
     *
     * @ORM\ManyToOne(targetEntity="Nomina.departamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_departamento", referencedColumnName="id")
     * })
     */
    private $idDepartamento;

    /**
     * @var \Nomina.organigrama
     *
     * @ORM\ManyToOne(targetEntity="Nomina.organigrama")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_organigrama", referencedColumnName="id")
     * })
     */
    private $idOrganigrama;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDepartamentoPadre
     *
     * @param integer $idDepartamentoPadre
     *
     * @return Nomina.orgDepartamento
     */
    public function setIdDepartamentoPadre($idDepartamentoPadre)
    {
        $this->idDepartamentoPadre = $idDepartamentoPadre;

        return $this;
    }

    /**
     * Get idDepartamentoPadre
     *
     * @return integer
     */
    public function getIdDepartamentoPadre()
    {
        return $this->idDepartamentoPadre;
    }

    /**
     * Set idDepartamento
     *
     * @param \Nomina.departamento $idDepartamento
     *
     * @return Nomina.orgDepartamento
     */
    public function setIdDepartamento(\Nomina.departamento $idDepartamento = null)
    {
        $this->idDepartamento = $idDepartamento;

        return $this;
    }

    /**
     * Get idDepartamento
     *
     * @return \Nomina.departamento
     */
    public function getIdDepartamento()
    {
        return $this->idDepartamento;
    }

    /**
     * Set idOrganigrama
     *
     * @param \Nomina.organigrama $idOrganigrama
     *
     * @return Nomina.orgDepartamento
     */
    public function setIdOrganigrama(\Nomina.organigrama $idOrganigrama = null)
    {
        $this->idOrganigrama = $idOrganigrama;

        return $this;
    }

    /**
     * Get idOrganigrama
     *
     * @return \Nomina.organigrama
     */
    public function getIdOrganigrama()
    {
        return $this->idOrganigrama;
    }
}

