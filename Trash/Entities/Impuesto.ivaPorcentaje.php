<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Impuesto.ivaPorcentaje
 *
 * @ORM\Table(name="impuesto.iva_porcentaje", uniqueConstraints={@ORM\UniqueConstraint(name="idx_descripcion_porcentajeiva", columns={"descripcion"}), @ORM\UniqueConstraint(name="idx_codigo_porcentajeiva", columns={"codigo"})}, indexes={@ORM\Index(name="idx_idestado_porcentajeiva", columns={"idestado"}), @ORM\Index(name="idx_iduscreacion_porcentajeiva", columns={"iduscreacion"}), @ORM\Index(name="fki_idcativa_porcentajeiva", columns={"idcativa"})})
 * @ORM\Entity
 */
class Impuesto.ivaPorcentaje
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="impuesto.iva_porcentaje_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=10, nullable=false)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=true)
     */
    private $valor = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="iduscreacion", type="integer", nullable=false)
     */
    private $iduscreacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idusmodifica", type="integer", nullable=true)
     */
    private $idusmodifica = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=false)
     */
    private $codigo;

    /**
     * @var \Impuesto.ivaCategoria
     *
     * @ORM\ManyToOne(targetEntity="Impuesto.ivaCategoria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcativa", referencedColumnName="id")
     * })
     */
    private $idcativa;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param integer $iduscreacion
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return integer
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param integer $idusmodifica
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return integer
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set idcativa
     *
     * @param \Impuesto.ivaCategoria $idcativa
     *
     * @return Impuesto.ivaPorcentaje
     */
    public function setIdcativa(\Impuesto.ivaCategoria $idcativa = null)
    {
        $this->idcativa = $idcativa;

        return $this;
    }

    /**
     * Get idcativa
     *
     * @return \Impuesto.ivaCategoria
     */
    public function getIdcativa()
    {
        return $this->idcativa;
    }
}

