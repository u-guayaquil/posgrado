<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Impuesto.porRetencion
 *
 * @ORM\Table(name="impuesto.por_retencion", uniqueConstraints={@ORM\UniqueConstraint(name="idx_codigo_retencion", columns={"codigo"}), @ORM\UniqueConstraint(name="idx_descripcion_retencion", columns={"descripcion"})}, indexes={@ORM\Index(name="fki_idimpuesto_retencion", columns={"idimpuesto"}), @ORM\Index(name="idx_idestado_retencion", columns={"idestado"}), @ORM\Index(name="idx_idctadeudora_retencion", columns={"idctadeudora"}), @ORM\Index(name="idx_idctaacreedora_retencion", columns={"idctaacreedora"}), @ORM\Index(name="idx_iduscreacion_retencion", columns={"iduscreacion"})})
 * @ORM\Entity
 */
class Impuesto.porRetencion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="impuesto.por_retencion_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=20, nullable=false)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idctaacreedora", type="integer", nullable=true)
     */
    private $idctaacreedora = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idctadeudora", type="integer", nullable=true)
     */
    private $idctadeudora = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="iduscreacion", type="integer", nullable=false)
     */
    private $iduscreacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idusmodifica", type="integer", nullable=true)
     */
    private $idusmodifica = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Impuesto.tipoImpuesto
     *
     * @ORM\ManyToOne(targetEntity="Impuesto.tipoImpuesto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idimpuesto", referencedColumnName="id")
     * })
     */
    private $idimpuesto;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Impuesto.porRetencion
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Impuesto.porRetencion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Impuesto.porRetencion
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set idctaacreedora
     *
     * @param integer $idctaacreedora
     *
     * @return Impuesto.porRetencion
     */
    public function setIdctaacreedora($idctaacreedora)
    {
        $this->idctaacreedora = $idctaacreedora;

        return $this;
    }

    /**
     * Get idctaacreedora
     *
     * @return integer
     */
    public function getIdctaacreedora()
    {
        return $this->idctaacreedora;
    }

    /**
     * Set idctadeudora
     *
     * @param integer $idctadeudora
     *
     * @return Impuesto.porRetencion
     */
    public function setIdctadeudora($idctadeudora)
    {
        $this->idctadeudora = $idctadeudora;

        return $this;
    }

    /**
     * Get idctadeudora
     *
     * @return integer
     */
    public function getIdctadeudora()
    {
        return $this->idctadeudora;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Impuesto.porRetencion
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param integer $iduscreacion
     *
     * @return Impuesto.porRetencion
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return integer
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param integer $idusmodifica
     *
     * @return Impuesto.porRetencion
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return integer
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Impuesto.porRetencion
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Impuesto.porRetencion
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idimpuesto
     *
     * @param \Impuesto.tipoImpuesto $idimpuesto
     *
     * @return Impuesto.porRetencion
     */
    public function setIdimpuesto(\Impuesto.tipoImpuesto $idimpuesto = null)
    {
        $this->idimpuesto = $idimpuesto;

        return $this;
    }

    /**
     * Get idimpuesto
     *
     * @return \Impuesto.tipoImpuesto
     */
    public function getIdimpuesto()
    {
        return $this->idimpuesto;
    }
}

