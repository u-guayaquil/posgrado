<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Nomina.orgDepartamentoCargo
 *
 * @ORM\Table(name="nomina.org_departamento_cargo", indexes={@ORM\Index(name="fki_idcargo_orgdepcarg", columns={"id_cargo"}), @ORM\Index(name="idx_idcargpadre_orgdepcarg", columns={"id_cargo_padre"}), @ORM\Index(name="fki_idorgdept_orgdepcarg", columns={"id_orgdept"})})
 * @ORM\Entity
 */
class Nomina.orgDepartamentoCargo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="nomina.org_departamento_cargo_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="id_cargo_padre", type="string", length=2, nullable=false)
     */
    private $idCargoPadre = '';

    /**
     * @var \Nomina.cargo
     *
     * @ORM\ManyToOne(targetEntity="Nomina.cargo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cargo", referencedColumnName="id")
     * })
     */
    private $idCargo;

    /**
     * @var \Nomina.orgDepartamento
     *
     * @ORM\ManyToOne(targetEntity="Nomina.orgDepartamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_orgdept", referencedColumnName="id")
     * })
     */
    private $idOrgdept;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCargoPadre
     *
     * @param string $idCargoPadre
     *
     * @return Nomina.orgDepartamentoCargo
     */
    public function setIdCargoPadre($idCargoPadre)
    {
        $this->idCargoPadre = $idCargoPadre;

        return $this;
    }

    /**
     * Get idCargoPadre
     *
     * @return string
     */
    public function getIdCargoPadre()
    {
        return $this->idCargoPadre;
    }

    /**
     * Set idCargo
     *
     * @param \Nomina.cargo $idCargo
     *
     * @return Nomina.orgDepartamentoCargo
     */
    public function setIdCargo(\Nomina.cargo $idCargo = null)
    {
        $this->idCargo = $idCargo;

        return $this;
    }

    /**
     * Get idCargo
     *
     * @return \Nomina.cargo
     */
    public function getIdCargo()
    {
        return $this->idCargo;
    }

    /**
     * Set idOrgdept
     *
     * @param \Nomina.orgDepartamento $idOrgdept
     *
     * @return Nomina.orgDepartamentoCargo
     */
    public function setIdOrgdept(\Nomina.orgDepartamento $idOrgdept = null)
    {
        $this->idOrgdept = $idOrgdept;

        return $this;
    }

    /**
     * Get idOrgdept
     *
     * @return \Nomina.orgDepartamento
     */
    public function getIdOrgdept()
    {
        return $this->idOrgdept;
    }
}

