<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Impuesto.ice
 *
 * @ORM\Table(name="impuesto.ice", uniqueConstraints={@ORM\UniqueConstraint(name="idx_descripcion_impuesto_ice", columns={"descripcion"}), @ORM\UniqueConstraint(name="idx_codigo_impuesto_ice", columns={"codigo"})}, indexes={@ORM\Index(name="idx_idctaicecompra_impuesto_ice", columns={"idctaicecompra"}), @ORM\Index(name="idx_idctaivacompra_impuesto_ice", columns={"idctaivacompra"}), @ORM\Index(name="idx_iduscreacion_impuesto_ice", columns={"iduscreacion"}), @ORM\Index(name="idx_idestado_impuesto_ice", columns={"idestado"}), @ORM\Index(name="idx_idctaivaventa_impuesto_ice", columns={"idctaivaventa"}), @ORM\Index(name="idx_idctaiceventa_impuesto_ice", columns={"idctaiceventa"})})
 * @ORM\Entity
 */
class Impuesto.ice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="impuesto.ice_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=30, nullable=false)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idctaiceventa", type="integer", nullable=true)
     */
    private $idctaiceventa = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idctaicecompra", type="integer", nullable=true)
     */
    private $idctaicecompra = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idctaivaventa", type="integer", nullable=true)
     */
    private $idctaivaventa = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idctaivacompra", type="integer", nullable=true)
     */
    private $idctaivacompra = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="iduscreacion", type="integer", nullable=false)
     */
    private $iduscreacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idusmodifica", type="integer", nullable=true)
     */
    private $idusmodifica = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=true)
     */
    private $fecreacion = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Impuesto.ice
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Impuesto.ice
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Impuesto.ice
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set idctaiceventa
     *
     * @param integer $idctaiceventa
     *
     * @return Impuesto.ice
     */
    public function setIdctaiceventa($idctaiceventa)
    {
        $this->idctaiceventa = $idctaiceventa;

        return $this;
    }

    /**
     * Get idctaiceventa
     *
     * @return integer
     */
    public function getIdctaiceventa()
    {
        return $this->idctaiceventa;
    }

    /**
     * Set idctaicecompra
     *
     * @param integer $idctaicecompra
     *
     * @return Impuesto.ice
     */
    public function setIdctaicecompra($idctaicecompra)
    {
        $this->idctaicecompra = $idctaicecompra;

        return $this;
    }

    /**
     * Get idctaicecompra
     *
     * @return integer
     */
    public function getIdctaicecompra()
    {
        return $this->idctaicecompra;
    }

    /**
     * Set idctaivaventa
     *
     * @param integer $idctaivaventa
     *
     * @return Impuesto.ice
     */
    public function setIdctaivaventa($idctaivaventa)
    {
        $this->idctaivaventa = $idctaivaventa;

        return $this;
    }

    /**
     * Get idctaivaventa
     *
     * @return integer
     */
    public function getIdctaivaventa()
    {
        return $this->idctaivaventa;
    }

    /**
     * Set idctaivacompra
     *
     * @param integer $idctaivacompra
     *
     * @return Impuesto.ice
     */
    public function setIdctaivacompra($idctaivacompra)
    {
        $this->idctaivacompra = $idctaivacompra;

        return $this;
    }

    /**
     * Get idctaivacompra
     *
     * @return integer
     */
    public function getIdctaivacompra()
    {
        return $this->idctaivacompra;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Impuesto.ice
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param integer $iduscreacion
     *
     * @return Impuesto.ice
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return integer
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param integer $idusmodifica
     *
     * @return Impuesto.ice
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return integer
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Impuesto.ice
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Impuesto.ice
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }
}

