<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * General.cliente
 *
 * @ORM\Table(name="general.cliente", uniqueConstraints={@ORM\UniqueConstraint(name="idx_codigo_cliente", columns={"codigo"})}, indexes={@ORM\Index(name="fki_idtermino_cliente", columns={"idtermino"}), @ORM\Index(name="fki_idestadocivil_cliente", columns={"idestadocivil"}), @ORM\Index(name="fki_idciudad_cliente", columns={"idciudad"}), @ORM\Index(name="fki_tipoident_cliente", columns={"tipo_ident"}), @ORM\Index(name="fki_idgrupo_grpcliente", columns={"idgrupo"}), @ORM\Index(name="fki_idgrupo_cliente", columns={"idgrupo"})})
 * @ORM\Entity
 */
class General.cliente
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="general.cliente_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=120, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="num_ident", type="string", length=15, nullable=false)
     */
    private $numIdent;

    /**
     * @var string
     *
     * @ORM\Column(name="idtermino", type="string", length=2, nullable=false)
     */
    private $idtermino;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_vendedor", type="string", length=1, nullable=false)
     */
    private $tipoVendedor = 'E';

    /**
     * @var string
     *
     * @ORM\Column(name="idvendedor", type="string", length=4, nullable=false)
     */
    private $idvendedor = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ctaxcobrar", type="string", length=5, nullable=false)
     */
    private $ctaxcobrar = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ctaanticip", type="string", length=5, nullable=false)
     */
    private $ctaanticip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", length=1, nullable=false)
     */
    private $sexo = 'M';

    /**
     * @var integer
     *
     * @ORM\Column(name="idciudad", type="integer", nullable=false)
     */
    private $idciudad;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=150, nullable=false)
     */
    private $direccion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=10, nullable=false)
     */
    private $telefono = '';

    /**
     * @var string
     *
     * @ORM\Column(name="movil", type="string", length=15, nullable=false)
     */
    private $movil = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=false)
     */
    private $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=500, nullable=false)
     */
    private $comentario = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechanacimiento", type="datetime", nullable=true)
     */
    private $fechanacimiento = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechacliente", type="datetime", nullable=true)
     */
    private $fechacliente = '1900-01-01 00:00:00';

    /**
     * @var \General.estadocivil
     *
     * @ORM\ManyToOne(targetEntity="General.estadocivil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idestadocivil", referencedColumnName="id")
     * })
     */
    private $idestadocivil;

    /**
     * @var \General.grpcliente
     *
     * @ORM\ManyToOne(targetEntity="General.grpcliente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idgrupo", referencedColumnName="id")
     * })
     */
    private $idgrupo;

    /**
     * @var \Impuesto.identificacion
     *
     * @ORM\ManyToOne(targetEntity="Impuesto.identificacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_ident", referencedColumnName="id")
     * })
     */
    private $tipoent;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return General.cliente
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return General.cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set numIdent
     *
     * @param string $numIdent
     *
     * @return General.cliente
     */
    public function setNumIdent($numIdent)
    {
        $this->numIdent = $numIdent;

        return $this;
    }

    /**
     * Get numIdent
     *
     * @return string
     */
    public function getNumIdent()
    {
        return $this->numIdent;
    }

    /**
     * Set idtermino
     *
     * @param string $idtermino
     *
     * @return General.cliente
     */
    public function setIdtermino($idtermino)
    {
        $this->idtermino = $idtermino;

        return $this;
    }

    /**
     * Get idtermino
     *
     * @return string
     */
    public function getIdtermino()
    {
        return $this->idtermino;
    }

    /**
     * Set tipoVendedor
     *
     * @param string $tipoVendedor
     *
     * @return General.cliente
     */
    public function setTipoVendedor($tipoVendedor)
    {
        $this->tipoVendedor = $tipoVendedor;

        return $this;
    }

    /**
     * Get tipoVendedor
     *
     * @return string
     */
    public function getTipoVendedor()
    {
        return $this->tipoVendedor;
    }

    /**
     * Set idvendedor
     *
     * @param string $idvendedor
     *
     * @return General.cliente
     */
    public function setIdvendedor($idvendedor)
    {
        $this->idvendedor = $idvendedor;

        return $this;
    }

    /**
     * Get idvendedor
     *
     * @return string
     */
    public function getIdvendedor()
    {
        return $this->idvendedor;
    }

    /**
     * Set ctaxcobrar
     *
     * @param string $ctaxcobrar
     *
     * @return General.cliente
     */
    public function setCtaxcobrar($ctaxcobrar)
    {
        $this->ctaxcobrar = $ctaxcobrar;

        return $this;
    }

    /**
     * Get ctaxcobrar
     *
     * @return string
     */
    public function getCtaxcobrar()
    {
        return $this->ctaxcobrar;
    }

    /**
     * Set ctaanticip
     *
     * @param string $ctaanticip
     *
     * @return General.cliente
     */
    public function setCtaanticip($ctaanticip)
    {
        $this->ctaanticip = $ctaanticip;

        return $this;
    }

    /**
     * Get ctaanticip
     *
     * @return string
     */
    public function getCtaanticip()
    {
        return $this->ctaanticip;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     *
     * @return General.cliente
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set idciudad
     *
     * @param integer $idciudad
     *
     * @return General.cliente
     */
    public function setIdciudad($idciudad)
    {
        $this->idciudad = $idciudad;

        return $this;
    }

    /**
     * Get idciudad
     *
     * @return integer
     */
    public function getIdciudad()
    {
        return $this->idciudad;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return General.cliente
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return General.cliente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set movil
     *
     * @param string $movil
     *
     * @return General.cliente
     */
    public function setMovil($movil)
    {
        $this->movil = $movil;

        return $this;
    }

    /**
     * Get movil
     *
     * @return string
     */
    public function getMovil()
    {
        return $this->movil;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return General.cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return General.cliente
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set fechanacimiento
     *
     * @param \DateTime $fechanacimiento
     *
     * @return General.cliente
     */
    public function setFechanacimiento($fechanacimiento)
    {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }

    /**
     * Get fechanacimiento
     *
     * @return \DateTime
     */
    public function getFechanacimiento()
    {
        return $this->fechanacimiento;
    }

    /**
     * Set fechacliente
     *
     * @param \DateTime $fechacliente
     *
     * @return General.cliente
     */
    public function setFechacliente($fechacliente)
    {
        $this->fechacliente = $fechacliente;

        return $this;
    }

    /**
     * Get fechacliente
     *
     * @return \DateTime
     */
    public function getFechacliente()
    {
        return $this->fechacliente;
    }

    /**
     * Set idestadocivil
     *
     * @param \General.estadocivil $idestadocivil
     *
     * @return General.cliente
     */
    public function setIdestadocivil(\General.estadocivil $idestadocivil = null)
    {
        $this->idestadocivil = $idestadocivil;

        return $this;
    }

    /**
     * Get idestadocivil
     *
     * @return \General.estadocivil
     */
    public function getIdestadocivil()
    {
        return $this->idestadocivil;
    }

    /**
     * Set idgrupo
     *
     * @param \General.grpcliente $idgrupo
     *
     * @return General.cliente
     */
    public function setIdgrupo(\General.grpcliente $idgrupo = null)
    {
        $this->idgrupo = $idgrupo;

        return $this;
    }

    /**
     * Get idgrupo
     *
     * @return \General.grpcliente
     */
    public function getIdgrupo()
    {
        return $this->idgrupo;
    }

    /**
     * Set tipoent
     *
     * @param \Impuesto.identificacion $tipoent
     *
     * @return General.cliente
     */
    public function setTipoent(\Impuesto.identificacion $tipoent = null)
    {
        $this->tipoent = $tipoent;

        return $this;
    }

    /**
     * Get tipoent
     *
     * @return \Impuesto.identificacion
     */
    public function getTipoent()
    {
        return $this->tipoent;
    }
}

