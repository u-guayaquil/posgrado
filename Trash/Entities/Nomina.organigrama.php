<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Nomina.organigrama
 *
 * @ORM\Table(name="nomina.organigrama", indexes={@ORM\Index(name="idx_idestado_organigrama", columns={"idestado"}), @ORM\Index(name="idx_iduscreacion_organigrama", columns={"iduscreacion"})})
 * @ORM\Entity
 */
class Nomina.organigrama
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="nomina.organigrama_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fedesde", type="datetime", nullable=true)
     */
    private $fedesde;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fehasta", type="datetime", nullable=true)
     */
    private $fehasta;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=10, nullable=false)
     */
    private $version = '1900-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=512, nullable=false)
     */
    private $comentario;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado;

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fedesde
     *
     * @param \DateTime $fedesde
     *
     * @return Nomina.organigrama
     */
    public function setFedesde($fedesde)
    {
        $this->fedesde = $fedesde;

        return $this;
    }

    /**
     * Get fedesde
     *
     * @return \DateTime
     */
    public function getFedesde()
    {
        return $this->fedesde;
    }

    /**
     * Set fehasta
     *
     * @param \DateTime $fehasta
     *
     * @return Nomina.organigrama
     */
    public function setFehasta($fehasta)
    {
        $this->fehasta = $fehasta;

        return $this;
    }

    /**
     * Get fehasta
     *
     * @return \DateTime
     */
    public function getFehasta()
    {
        return $this->fehasta;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return Nomina.organigrama
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return Nomina.organigrama
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Nomina.organigrama
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return Nomina.organigrama
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return Nomina.organigrama
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Nomina.organigrama
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Nomina.organigrama
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }
}

