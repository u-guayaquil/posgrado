<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Sistema.usuarioperfil
 *
 * @ORM\Table(name="sistema.usuarioperfil", uniqueConstraints={@ORM\UniqueConstraint(name="idx_idperfil_idusuario_usuarioperfil", columns={"idusuario", "idperfil"})}, indexes={@ORM\Index(name="IDX_98BA35DDF2D8DBEB", columns={"idperfil"}), @ORM\Index(name="IDX_98BA35DDFD61E233", columns={"idusuario"})})
 * @ORM\Entity
 */
class Sistema.usuarioperfil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="sistema.usuarioperfil_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fedesde", type="datetime", nullable=false)
     */
    private $fedesde = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fehasta", type="datetime", nullable=true)
     */
    private $fehasta = '1900-01-01 00:00:00';

    /**
     * @var \Sistema.perfil
     *
     * @ORM\ManyToOne(targetEntity="Sistema.perfil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idperfil", referencedColumnName="id")
     * })
     */
    private $idperfil;

    /**
     * @var \Sistema.usuario
     *
     * @ORM\ManyToOne(targetEntity="Sistema.usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuario", referencedColumnName="id")
     * })
     */
    private $idusuario;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fedesde
     *
     * @param \DateTime $fedesde
     *
     * @return Sistema.usuarioperfil
     */
    public function setFedesde($fedesde)
    {
        $this->fedesde = $fedesde;

        return $this;
    }

    /**
     * Get fedesde
     *
     * @return \DateTime
     */
    public function getFedesde()
    {
        return $this->fedesde;
    }

    /**
     * Set fehasta
     *
     * @param \DateTime $fehasta
     *
     * @return Sistema.usuarioperfil
     */
    public function setFehasta($fehasta)
    {
        $this->fehasta = $fehasta;

        return $this;
    }

    /**
     * Get fehasta
     *
     * @return \DateTime
     */
    public function getFehasta()
    {
        return $this->fehasta;
    }

    /**
     * Set idperfil
     *
     * @param \Sistema.perfil $idperfil
     *
     * @return Sistema.usuarioperfil
     */
    public function setIdperfil(\Sistema.perfil $idperfil = null)
    {
        $this->idperfil = $idperfil;

        return $this;
    }

    /**
     * Get idperfil
     *
     * @return \Sistema.perfil
     */
    public function getIdperfil()
    {
        return $this->idperfil;
    }

    /**
     * Set idusuario
     *
     * @param \Sistema.usuario $idusuario
     *
     * @return Sistema.usuarioperfil
     */
    public function setIdusuario(\Sistema.usuario $idusuario = null)
    {
        $this->idusuario = $idusuario;

        return $this;
    }

    /**
     * Get idusuario
     *
     * @return \Sistema.usuario
     */
    public function getIdusuario()
    {
        return $this->idusuario;
    }
}

