<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * General.acreedor
 *
 * @ORM\Table(name="general.acreedor", uniqueConstraints={@ORM\UniqueConstraint(name="idx_codigo_acreedor", columns={"codigo"})}, indexes={@ORM\Index(name="fki_tipo_ident_acreedor", columns={"tipo_ident"}), @ORM\Index(name="idx_idestado_acreedor", columns={"idestado"}), @ORM\Index(name="fki_idclase_acreedor", columns={"idclase"}), @ORM\Index(name="idx_iduscreacion_acreedor", columns={"iduscreacion"}), @ORM\Index(name="fk_idciudad_acreedor", columns={"idciudad"})})
 * @ORM\Entity
 */
class General.acreedor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="general.acreedor_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=80, nullable=false)
     */
    private $nombre = '';

    /**
     * @var string
     *
     * @ORM\Column(name="num_ident", type="string", length=15, nullable=false)
     */
    private $numIdent;

    /**
     * @var integer
     *
     * @ORM\Column(name="idciudad", type="integer", nullable=false)
     */
    private $idciudad;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=150, nullable=false)
     */
    private $direccion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=20, nullable=false)
     */
    private $telefono = '';

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=20, nullable=false)
     */
    private $fax = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=false)
     */
    private $email = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="idctaxpagar", type="integer", nullable=false)
     */
    private $idctaxpagar;

    /**
     * @var integer
     *
     * @ORM\Column(name="idctanticip", type="integer", nullable=false)
     */
    private $idctanticip;

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=true)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \General.claseacreedor
     *
     * @ORM\ManyToOne(targetEntity="General.claseacreedor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idclase", referencedColumnName="id")
     * })
     */
    private $idclase;

    /**
     * @var \Impuesto.identificacion
     *
     * @ORM\ManyToOne(targetEntity="Impuesto.identificacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_ident", referencedColumnName="id")
     * })
     */
    private $tipoent;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return General.acreedor
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return General.acreedor
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set numIdent
     *
     * @param string $numIdent
     *
     * @return General.acreedor
     */
    public function setNumIdent($numIdent)
    {
        $this->numIdent = $numIdent;

        return $this;
    }

    /**
     * Get numIdent
     *
     * @return string
     */
    public function getNumIdent()
    {
        return $this->numIdent;
    }

    /**
     * Set idciudad
     *
     * @param integer $idciudad
     *
     * @return General.acreedor
     */
    public function setIdciudad($idciudad)
    {
        $this->idciudad = $idciudad;

        return $this;
    }

    /**
     * Get idciudad
     *
     * @return integer
     */
    public function getIdciudad()
    {
        return $this->idciudad;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return General.acreedor
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return General.acreedor
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return General.acreedor
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return General.acreedor
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set idctaxpagar
     *
     * @param integer $idctaxpagar
     *
     * @return General.acreedor
     */
    public function setIdctaxpagar($idctaxpagar)
    {
        $this->idctaxpagar = $idctaxpagar;

        return $this;
    }

    /**
     * Get idctaxpagar
     *
     * @return integer
     */
    public function getIdctaxpagar()
    {
        return $this->idctaxpagar;
    }

    /**
     * Set idctanticip
     *
     * @param integer $idctanticip
     *
     * @return General.acreedor
     */
    public function setIdctanticip($idctanticip)
    {
        $this->idctanticip = $idctanticip;

        return $this;
    }

    /**
     * Get idctanticip
     *
     * @return integer
     */
    public function getIdctanticip()
    {
        return $this->idctanticip;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return General.acreedor
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return General.acreedor
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return General.acreedor
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return General.acreedor
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return General.acreedor
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idclase
     *
     * @param \General.claseacreedor $idclase
     *
     * @return General.acreedor
     */
    public function setIdclase(\General.claseacreedor $idclase = null)
    {
        $this->idclase = $idclase;

        return $this;
    }

    /**
     * Get idclase
     *
     * @return \General.claseacreedor
     */
    public function getIdclase()
    {
        return $this->idclase;
    }

    /**
     * Set tipoent
     *
     * @param \Impuesto.identificacion $tipoent
     *
     * @return General.acreedor
     */
    public function setTipoent(\Impuesto.identificacion $tipoent = null)
    {
        $this->tipoent = $tipoent;

        return $this;
    }

    /**
     * Get tipoent
     *
     * @return \Impuesto.identificacion
     */
    public function getTipoent()
    {
        return $this->tipoent;
    }
}

