<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * General.empresa
 *
 * @ORM\Table(name="general.empresa", indexes={@ORM\Index(name="fki_idmoneda_empresa", columns={"idmoneda"}), @ORM\Index(name="fki_idsucursal_empresa", columns={"idsucursal"})})
 * @ORM\Entity
 */
class General.empresa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="general.empresa_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comercial", type="string", length=100, nullable=false)
     */
    private $comercial = '';

    /**
     * @var string
     *
     * @ORM\Column(name="razonsocial", type="string", length=100, nullable=false)
     */
    private $razonsocial = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_ident_razon", type="integer", nullable=true)
     */
    private $tipoIdentRazon = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="num_ident_razon", type="string", length=15, nullable=true)
     */
    private $numIdentRazon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="representante", type="string", length=80, nullable=true)
     */
    private $representante = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_ident_repres", type="integer", nullable=true)
     */
    private $tipoIdentRepres = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="num_ident_repres", type="string", length=15, nullable=true)
     */
    private $numIdentRepres = '';

    /**
     * @var string
     *
     * @ORM\Column(name="contador", type="string", length=80, nullable=true)
     */
    private $contador = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_ident_contador", type="integer", nullable=true)
     */
    private $tipoIdentContador = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="num_ident_contador", type="string", length=15, nullable=true)
     */
    private $numIdentContador = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="esp_contribuyente", type="integer", nullable=true)
     */
    private $espContribuyente = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="obl_contabilidad", type="integer", nullable=true)
     */
    private $oblContabilidad = '0';

    /**
     * @var \General.moneda
     *
     * @ORM\ManyToOne(targetEntity="General.moneda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idmoneda", referencedColumnName="id")
     * })
     */
    private $idmoneda;

    /**
     * @var \General.sucursal
     *
     * @ORM\ManyToOne(targetEntity="General.sucursal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idsucursal", referencedColumnName="id")
     * })
     */
    private $idsucursal;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comercial
     *
     * @param string $comercial
     *
     * @return General.empresa
     */
    public function setComercial($comercial)
    {
        $this->comercial = $comercial;

        return $this;
    }

    /**
     * Get comercial
     *
     * @return string
     */
    public function getComercial()
    {
        return $this->comercial;
    }

    /**
     * Set razonsocial
     *
     * @param string $razonsocial
     *
     * @return General.empresa
     */
    public function setRazonsocial($razonsocial)
    {
        $this->razonsocial = $razonsocial;

        return $this;
    }

    /**
     * Get razonsocial
     *
     * @return string
     */
    public function getRazonsocial()
    {
        return $this->razonsocial;
    }

    /**
     * Set tipoIdentRazon
     *
     * @param integer $tipoIdentRazon
     *
     * @return General.empresa
     */
    public function setTipoIdentRazon($tipoIdentRazon)
    {
        $this->tipoIdentRazon = $tipoIdentRazon;

        return $this;
    }

    /**
     * Get tipoIdentRazon
     *
     * @return integer
     */
    public function getTipoIdentRazon()
    {
        return $this->tipoIdentRazon;
    }

    /**
     * Set numIdentRazon
     *
     * @param string $numIdentRazon
     *
     * @return General.empresa
     */
    public function setNumIdentRazon($numIdentRazon)
    {
        $this->numIdentRazon = $numIdentRazon;

        return $this;
    }

    /**
     * Get numIdentRazon
     *
     * @return string
     */
    public function getNumIdentRazon()
    {
        return $this->numIdentRazon;
    }

    /**
     * Set representante
     *
     * @param string $representante
     *
     * @return General.empresa
     */
    public function setRepresentante($representante)
    {
        $this->representante = $representante;

        return $this;
    }

    /**
     * Get representante
     *
     * @return string
     */
    public function getRepresentante()
    {
        return $this->representante;
    }

    /**
     * Set tipoIdentRepres
     *
     * @param integer $tipoIdentRepres
     *
     * @return General.empresa
     */
    public function setTipoIdentRepres($tipoIdentRepres)
    {
        $this->tipoIdentRepres = $tipoIdentRepres;

        return $this;
    }

    /**
     * Get tipoIdentRepres
     *
     * @return integer
     */
    public function getTipoIdentRepres()
    {
        return $this->tipoIdentRepres;
    }

    /**
     * Set numIdentRepres
     *
     * @param string $numIdentRepres
     *
     * @return General.empresa
     */
    public function setNumIdentRepres($numIdentRepres)
    {
        $this->numIdentRepres = $numIdentRepres;

        return $this;
    }

    /**
     * Get numIdentRepres
     *
     * @return string
     */
    public function getNumIdentRepres()
    {
        return $this->numIdentRepres;
    }

    /**
     * Set contador
     *
     * @param string $contador
     *
     * @return General.empresa
     */
    public function setContador($contador)
    {
        $this->contador = $contador;

        return $this;
    }

    /**
     * Get contador
     *
     * @return string
     */
    public function getContador()
    {
        return $this->contador;
    }

    /**
     * Set tipoIdentContador
     *
     * @param integer $tipoIdentContador
     *
     * @return General.empresa
     */
    public function setTipoIdentContador($tipoIdentContador)
    {
        $this->tipoIdentContador = $tipoIdentContador;

        return $this;
    }

    /**
     * Get tipoIdentContador
     *
     * @return integer
     */
    public function getTipoIdentContador()
    {
        return $this->tipoIdentContador;
    }

    /**
     * Set numIdentContador
     *
     * @param string $numIdentContador
     *
     * @return General.empresa
     */
    public function setNumIdentContador($numIdentContador)
    {
        $this->numIdentContador = $numIdentContador;

        return $this;
    }

    /**
     * Get numIdentContador
     *
     * @return string
     */
    public function getNumIdentContador()
    {
        return $this->numIdentContador;
    }

    /**
     * Set espContribuyente
     *
     * @param integer $espContribuyente
     *
     * @return General.empresa
     */
    public function setEspContribuyente($espContribuyente)
    {
        $this->espContribuyente = $espContribuyente;

        return $this;
    }

    /**
     * Get espContribuyente
     *
     * @return integer
     */
    public function getEspContribuyente()
    {
        return $this->espContribuyente;
    }

    /**
     * Set oblContabilidad
     *
     * @param integer $oblContabilidad
     *
     * @return General.empresa
     */
    public function setOblContabilidad($oblContabilidad)
    {
        $this->oblContabilidad = $oblContabilidad;

        return $this;
    }

    /**
     * Get oblContabilidad
     *
     * @return integer
     */
    public function getOblContabilidad()
    {
        return $this->oblContabilidad;
    }

    /**
     * Set idmoneda
     *
     * @param \General.moneda $idmoneda
     *
     * @return General.empresa
     */
    public function setIdmoneda(\General.moneda $idmoneda = null)
    {
        $this->idmoneda = $idmoneda;

        return $this;
    }

    /**
     * Get idmoneda
     *
     * @return \General.moneda
     */
    public function getIdmoneda()
    {
        return $this->idmoneda;
    }

    /**
     * Set idsucursal
     *
     * @param \General.sucursal $idsucursal
     *
     * @return General.empresa
     */
    public function setIdsucursal(\General.sucursal $idsucursal = null)
    {
        $this->idsucursal = $idsucursal;

        return $this;
    }

    /**
     * Get idsucursal
     *
     * @return \General.sucursal
     */
    public function getIdsucursal()
    {
        return $this->idsucursal;
    }
}

