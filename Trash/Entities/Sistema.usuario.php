<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Sistema.usuario
 *
 * @ORM\Table(name="sistema.usuario", uniqueConstraints={@ORM\UniqueConstraint(name="idx_usuario_usuario", columns={"usuario"})}, indexes={@ORM\Index(name="idx_idempleado_usuario", columns={"idempleado"}), @ORM\Index(name="idx_iduscreacion_usuario", columns={"iduscreacion"}), @ORM\Index(name="fki_idpregunta_usuario", columns={"idpregunta"}), @ORM\Index(name="idx_idestado_usuario", columns={"idestado"})})
 * @ORM\Entity
 */
class Sistema.usuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="sistema.usuario_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=80, nullable=false)
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="idempleado", type="integer", nullable=true)
     */
    private $idempleado = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="string", length=20, nullable=true)
     */
    private $respuesta = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="iduscreacion", type="integer", nullable=false)
     */
    private $iduscreacion = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="idusmodifica", type="integer", nullable=true)
     */
    private $idusmodifica = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecaducidad", type="datetime", nullable=true)
     */
    private $fecaducidad = '1900-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=true)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="horadesde", type="string", length=8, nullable=true)
     */
    private $horadesde = '';

    /**
     * @var string
     *
     * @ORM\Column(name="horahasta", type="string", length=8, nullable=true)
     */
    private $horahasta = '';

    /**
     * @var \Sistema.pregunta
     *
     * @ORM\ManyToOne(targetEntity="Sistema.pregunta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpregunta", referencedColumnName="id")
     * })
     */
    private $idpregunta;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return Sistema.usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Sistema.usuario
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set idempleado
     *
     * @param integer $idempleado
     *
     * @return Sistema.usuario
     */
    public function setIdempleado($idempleado)
    {
        $this->idempleado = $idempleado;

        return $this;
    }

    /**
     * Get idempleado
     *
     * @return integer
     */
    public function getIdempleado()
    {
        return $this->idempleado;
    }

    /**
     * Set respuesta
     *
     * @param string $respuesta
     *
     * @return Sistema.usuario
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return string
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Sistema.usuario
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param integer $iduscreacion
     *
     * @return Sistema.usuario
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return integer
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param integer $idusmodifica
     *
     * @return Sistema.usuario
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return integer
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecaducidad
     *
     * @param \DateTime $fecaducidad
     *
     * @return Sistema.usuario
     */
    public function setFecaducidad($fecaducidad)
    {
        $this->fecaducidad = $fecaducidad;

        return $this;
    }

    /**
     * Get fecaducidad
     *
     * @return \DateTime
     */
    public function getFecaducidad()
    {
        return $this->fecaducidad;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Sistema.usuario
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Sistema.usuario
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set horadesde
     *
     * @param string $horadesde
     *
     * @return Sistema.usuario
     */
    public function setHoradesde($horadesde)
    {
        $this->horadesde = $horadesde;

        return $this;
    }

    /**
     * Get horadesde
     *
     * @return string
     */
    public function getHoradesde()
    {
        return $this->horadesde;
    }

    /**
     * Set horahasta
     *
     * @param string $horahasta
     *
     * @return Sistema.usuario
     */
    public function setHorahasta($horahasta)
    {
        $this->horahasta = $horahasta;

        return $this;
    }

    /**
     * Get horahasta
     *
     * @return string
     */
    public function getHorahasta()
    {
        return $this->horahasta;
    }

    /**
     * Set idpregunta
     *
     * @param \Sistema.pregunta $idpregunta
     *
     * @return Sistema.usuario
     */
    public function setIdpregunta(\Sistema.pregunta $idpregunta = null)
    {
        $this->idpregunta = $idpregunta;

        return $this;
    }

    /**
     * Get idpregunta
     *
     * @return \Sistema.pregunta
     */
    public function getIdpregunta()
    {
        return $this->idpregunta;
    }
}

