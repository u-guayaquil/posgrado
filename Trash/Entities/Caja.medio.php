<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Caja.medio
 *
 * @ORM\Table(name="caja.medio", indexes={@ORM\Index(name="idx_idestado_cajamedios", columns={"idestado"}), @ORM\Index(name="idx_iduscreacion_cajamedios", columns={"iduscreacion"}), @ORM\Index(name="IDX_88B8A690205A055B", columns={"idcajaoper"}), @ORM\Index(name="IDX_88B8A690E006C7F7", columns={"idcajatipo"})})
 * @ORM\Entity
 */
class Caja.medio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="caja.medio_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=20, nullable=false)
     */
    private $descripcion = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="idestado", type="integer", nullable=false)
     */
    private $idestado = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="iduscreacion", type="string", length=4, nullable=false)
     */
    private $iduscreacion;

    /**
     * @var string
     *
     * @ORM\Column(name="idusmodifica", type="string", length=4, nullable=false)
     */
    private $idusmodifica = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecreacion", type="datetime", nullable=false)
     */
    private $fecreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="femodificacion", type="datetime", nullable=false)
     */
    private $femodificacion = '1900-01-01 00:00:00';

    /**
     * @var \Caja.operacion
     *
     * @ORM\ManyToOne(targetEntity="Caja.operacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcajaoper", referencedColumnName="id")
     * })
     */
    private $idcajaoper;

    /**
     * @var \Caja.tipo
     *
     * @ORM\ManyToOne(targetEntity="Caja.tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcajatipo", referencedColumnName="id")
     * })
     */
    private $idcajatipo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Caja.medio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idestado
     *
     * @param integer $idestado
     *
     * @return Caja.medio
     */
    public function setIdestado($idestado)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return integer
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set iduscreacion
     *
     * @param string $iduscreacion
     *
     * @return Caja.medio
     */
    public function setIduscreacion($iduscreacion)
    {
        $this->iduscreacion = $iduscreacion;

        return $this;
    }

    /**
     * Get iduscreacion
     *
     * @return string
     */
    public function getIduscreacion()
    {
        return $this->iduscreacion;
    }

    /**
     * Set idusmodifica
     *
     * @param string $idusmodifica
     *
     * @return Caja.medio
     */
    public function setIdusmodifica($idusmodifica)
    {
        $this->idusmodifica = $idusmodifica;

        return $this;
    }

    /**
     * Get idusmodifica
     *
     * @return string
     */
    public function getIdusmodifica()
    {
        return $this->idusmodifica;
    }

    /**
     * Set fecreacion
     *
     * @param \DateTime $fecreacion
     *
     * @return Caja.medio
     */
    public function setFecreacion($fecreacion)
    {
        $this->fecreacion = $fecreacion;

        return $this;
    }

    /**
     * Get fecreacion
     *
     * @return \DateTime
     */
    public function getFecreacion()
    {
        return $this->fecreacion;
    }

    /**
     * Set femodificacion
     *
     * @param \DateTime $femodificacion
     *
     * @return Caja.medio
     */
    public function setFemodificacion($femodificacion)
    {
        $this->femodificacion = $femodificacion;

        return $this;
    }

    /**
     * Get femodificacion
     *
     * @return \DateTime
     */
    public function getFemodificacion()
    {
        return $this->femodificacion;
    }

    /**
     * Set idcajaoper
     *
     * @param \Caja.operacion $idcajaoper
     *
     * @return Caja.medio
     */
    public function setIdcajaoper(\Caja.operacion $idcajaoper = null)
    {
        $this->idcajaoper = $idcajaoper;

        return $this;
    }

    /**
     * Get idcajaoper
     *
     * @return \Caja.operacion
     */
    public function getIdcajaoper()
    {
        return $this->idcajaoper;
    }

    /**
     * Set idcajatipo
     *
     * @param \Caja.tipo $idcajatipo
     *
     * @return Caja.medio
     */
    public function setIdcajatipo(\Caja.tipo $idcajatipo = null)
    {
        $this->idcajatipo = $idcajatipo;

        return $this;
    }

    /**
     * Get idcajatipo
     *
     * @return \Caja.tipo
     */
    public function getIdcajatipo()
    {
        return $this->idcajatipo;
    }
}

