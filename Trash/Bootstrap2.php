<?php
        require_once ("../../../../../vendor/autoload.php");

        use Doctrine\ORM\Tools\Setup;
        use Doctrine\ORM\EntityManager;

        $paths = array(__DIR__."/src");
        $Configuracion = Setup::createAnnotationMetadataConfiguration($paths, true);
        $Conexion = array(
                          'user' => 'postgres',
                          'password' => '123456',
                          'port' => 5432,
                          'host' => 'localhost',
                          'dbname' => 'MAGICNEGSA',
                          'driver' => 'pdo_pgsql');

        $entityManager = EntityManager::create($Conexion, $Configuracion);
?>

